

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_fwiz.inc'),ONCE

                     MAP
                       INCLUDE('SBB01016.INC'),ONCE        !Local module procedure declarations
                     END


Pick_Other_Location PROCEDURE (f_type,f_ref_number)   !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGFLAG          BYTE(0)
DASBRW::6:TAGMOUSE         BYTE(0)
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer                       LIKE(GLO:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
save_stm_id          USHORT,AUTO
save_stm_ali_id      USHORT,AUTO
sav:Purchase_Cost    REAL
sav:Sale_Cost        REAL
sav:Retail_Cost      REAL
sav:Minimum_Level    REAL
sav:Reorder_Level    REAL
save_loc_id          USHORT,AUTO
save_sto_ali_id      USHORT,AUTO
save_sto_id          USHORT,AUTO
no_temp              STRING('NO')
tag_temp             STRING(1)
levels_temp          STRING('0')
costs_temp           BYTE(0)
all_temp             BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                       PROJECT(loc:Main_Store)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
loc:Main_Store         LIKE(loc:Main_Store)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse The Location File'),AT(,,345,215),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('Pick_Other_Location'),SYSTEM,GRAY,DOUBLE
                       PROMPT('Replicate Main Store Stock'),AT(8,8),USE(?Title),FONT(,12,,FONT:bold)
                       SHEET,AT(4,4,336,180),USE(?CurrentTab),BELOW,SPREAD
                         TAB('Tab 3'),USE(?Tab3)
                           PROMPT('General:'),AT(80,28),USE(?Prompt7),FONT(,,,FONT:bold)
                           PROMPT('This facility will replicate all the Main Store Stock Items into other locations'),AT(80,44,244,16),USE(?General1),FONT(,8,,)
                           PROMPT('Note: If the same part already exists in another location, it''s details will be ' &|
   'overwritten.'),AT(80,64,256,24),USE(?General2)
                           PROMPT('Click ''Next'' To Continue...'),AT(124,124),USE(?Prompt4)
                         END
                         TAB('By Location'),USE(?Tab:2)
                           PROMPT('Tag the locations that you wish to replicate to, then click ''Next'''),AT(80,44,104,32),USE(?Prompt5)
                           LIST,AT(192,24,144,132),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@80L(2)|M~Location~@s30@'),FROM(Queue:Browse:1)
                           PROMPT('Select Locations:'),AT(80,28),USE(?Prompt8),FONT(,,,FONT:bold)
                           BUTTON('&Rev tags'),AT(196,72,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(192,100,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON('&Tag'),AT(192,160,48,16),USE(?DASTAG),LEFT,ICON('tag.gif')
                           BUTTON('T&ag All'),AT(240,160,48,16),USE(?DASTAGAll),LEFT,ICON('tagall.gif')
                           BUTTON('&Untag All'),AT(288,160,48,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                         END
                         TAB('Tab 4'),USE(?Tab4)
                           PROMPT('Replication Details'),AT(77,28),USE(?Prompt9),FONT(,,,FONT:bold)
                           PROMPT('The Stock Item''s General Details will be replicated. Please tick the box if you ' &|
   'also wish to replicate the Cost Structure of the parts.'),AT(80,44,240,20),USE(?Prompt6)
                           PROMPT('(Note: Stock Quantity will NOT be replicated)'),AT(80,64),USE(?Prompt11)
                           CHECK('Minimum Stock And Re-Order Levels'),AT(132,84),USE(levels_temp),HIDE,VALUE('1','0')
                           CHECK('Cost Structure'),AT(132,104),USE(costs_temp),VALUE('1','0')
                           CHECK('All Details'),AT(132,124),USE(all_temp),HIDE,VALUE('1','0')
                           PROMPT('Click ''Finish'' when you are ready to begin. (Warning! this may take a long time ' &|
   'to complete)'),AT(28,152),USE(?Prompt10)
                         END
                       END
                       BUTTON('Close'),AT(280,192,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                       IMAGE('WRPT5256.GIF'),AT(8,56,64,68),USE(?Image1)
                       BUTTON('&Finish'),AT(224,192,56,16),USE(?Ok),LEFT,ICON('thumbs.gif')
                       BUTTON('&Next'),AT(64,192,56,16),USE(?VSNextButton),LEFT,ICON(ICON:VCRfastforward)
                       PANEL,AT(4,188,336,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Back'),AT(8,192,56,16),USE(?VSBackButton),LEFT,ICON(ICON:VCRrewind)
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
Wizard2         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Title{prop:FontColor} = -1
    ?Title{prop:Color} = 15066597
    ?CurrentTab{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    ?General1{prop:FontColor} = -1
    ?General1{prop:Color} = 15066597
    ?General2{prop:FontColor} = -1
    ?General2{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    ?Tab4{prop:Color} = 15066597
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?Prompt11{prop:FontColor} = -1
    ?Prompt11{prop:Color} = 15066597
    ?levels_temp{prop:Font,3} = -1
    ?levels_temp{prop:Color} = 15066597
    ?levels_temp{prop:Trn} = 0
    ?costs_temp{prop:Font,3} = -1
    ?costs_temp{prop:Color} = 15066597
    ?costs_temp{prop:Trn} = 0
    ?all_temp{prop:Font,3} = -1
    ?all_temp{prop:Color} = 15066597
    ?all_temp{prop:Trn} = 0
    ?Prompt10{prop:FontColor} = -1
    ?Prompt10{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   GLO:Queue.Pointer = loc:Location
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = loc:Location
     ADD(GLO:Queue,GLO:Queue.Pointer)
    tag_temp = '*'
  ELSE
    DELETE(GLO:Queue)
    tag_temp = ''
  END
    Queue:Browse:1.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse:1.tag_temp_Icon = 2
  ELSE
    Queue:Browse:1.tag_temp_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::6:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = loc:Location
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::6:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::6:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::6:QUEUE = GLO:Queue
    ADD(DASBRW::6:QUEUE)
  END
  FREE(GLO:Queue)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer = loc:Location
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = loc:Location
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Pick_Other_Location',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Pick_Other_Location',1)
    SolaceViewVars('save_stm_id',save_stm_id,'Pick_Other_Location',1)
    SolaceViewVars('save_stm_ali_id',save_stm_ali_id,'Pick_Other_Location',1)
    SolaceViewVars('sav:Purchase_Cost',sav:Purchase_Cost,'Pick_Other_Location',1)
    SolaceViewVars('sav:Sale_Cost',sav:Sale_Cost,'Pick_Other_Location',1)
    SolaceViewVars('sav:Retail_Cost',sav:Retail_Cost,'Pick_Other_Location',1)
    SolaceViewVars('sav:Minimum_Level',sav:Minimum_Level,'Pick_Other_Location',1)
    SolaceViewVars('sav:Reorder_Level',sav:Reorder_Level,'Pick_Other_Location',1)
    SolaceViewVars('save_loc_id',save_loc_id,'Pick_Other_Location',1)
    SolaceViewVars('save_sto_ali_id',save_sto_ali_id,'Pick_Other_Location',1)
    SolaceViewVars('save_sto_id',save_sto_id,'Pick_Other_Location',1)
    SolaceViewVars('no_temp',no_temp,'Pick_Other_Location',1)
    SolaceViewVars('tag_temp',tag_temp,'Pick_Other_Location',1)
    SolaceViewVars('levels_temp',levels_temp,'Pick_Other_Location',1)
    SolaceViewVars('costs_temp',costs_temp,'Pick_Other_Location',1)
    SolaceViewVars('all_temp',all_temp,'Pick_Other_Location',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Title;  SolaceCtrlName = '?Title';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?General1;  SolaceCtrlName = '?General1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?General2;  SolaceCtrlName = '?General2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt9;  SolaceCtrlName = '?Prompt9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt11;  SolaceCtrlName = '?Prompt11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?levels_temp;  SolaceCtrlName = '?levels_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?costs_temp;  SolaceCtrlName = '?costs_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?all_temp;  SolaceCtrlName = '?all_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt10;  SolaceCtrlName = '?Prompt10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1;  SolaceCtrlName = '?Image1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ok;  SolaceCtrlName = '?Ok';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VSNextButton;  SolaceCtrlName = '?VSNextButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VSBackButton;  SolaceCtrlName = '?VSBackButton';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Pick_Other_Location')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Pick_Other_Location')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Title
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOCATION.Open
  Relate:STOCK_ALIAS.Open
  Relate:STOMODEL_ALIAS.Open
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:STOMODEL.UseFile
  Access:LOCSHELF.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:LOCATION,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
    Wizard2.Init(?CurrentTab, |                       ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Ok, |                           ! OK button
                     ?Close, |                        ! Cancel button
                     1, |                             ! Skip hidden tabs
                     1, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,loc:Main_Store_Key)
  BRW1.AddRange(loc:Main_Store,no_temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,loc:Location,1,BRW1)
  BIND('tag_temp',tag_temp)
  ?Browse:1{PROP:IconList,1} = '~notick1.ico'
  ?Browse:1{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(tag_temp,BRW1.Q.tag_temp)
  BRW1.AddField(loc:Location,BRW1.Q.loc:Location)
  BRW1.AddField(loc:RecordNumber,BRW1.Q.loc:RecordNumber)
  BRW1.AddField(loc:Main_Store,BRW1.Q.loc:Main_Store)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?all_temp{Prop:Checked} = True
    levels_temp = 1
    costs_temp = 1
  END
  IF ?all_temp{Prop:Checked} = False
    costs_temp = 0
    levels_temp = 0
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:LOCATION.Close
    Relate:STOCK_ALIAS.Close
    Relate:STOMODEL_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Pick_Other_Location',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard2.Validate()
        DISABLE(Wizard2.NextControl())
     ELSE
        ENABLE(Wizard2.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?VSNextButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VSNextButton, Accepted)
      Case Choice(?CurrentTab)
          Of 2
              If ~Records(glo:Queue)
                  Case MessageEx('You select at lease ONE Site Location.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  Cycle
              End!If ~Records(glo:Queue)
      End!Case Choice(?CurrentTab)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VSNextButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?all_temp
      IF ?all_temp{Prop:Checked} = True
        levels_temp = 1
        costs_temp = 1
      END
      IF ?all_temp{Prop:Checked} = False
        costs_temp = 0
        levels_temp = 0
      END
      ThisWindow.Reset
    OF ?Ok
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Ok, Accepted)
      If ~Records(glo:Queue)
          Case MessageEx('You must select at least one location.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If ~Records(glo:Queue)
          Case MessageEx('Click ''OK'' to begin.','ServiceBase 2000',|
                         'Styles\warn.ico','|&OK|&Cancel',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
                  If f_type = 'ALL'
      
                      recordspercycle     = 25
                      recordsprocessed    = 0
                      percentprogress     = 0
                      open(progresswindow)
                      progress:thermometer    = 0
                      ?progress:pcttext{prop:text} = '0% Completed'
      
                      recordstoprocess    = Records(Stock)
      
                      setcursor(cursor:wait)                                                          !Loop through the main stores
                      save_loc_id = access:location.savefile()
                      access:location.clearkey(loc:main_store_key)
                      loc:main_store = 'YES'
                      set(loc:main_store_key,loc:main_store_key)
                      loop
                          if access:location.next()
                             break
                          end !if
                          if loc:main_store <> 'YES'      |
                              then break.  ! end if
                          save_sto_ali_id = access:stock_alias.savefile()                             !Loop through the stock for the main
                          access:stock_alias.clearkey(sto_ali:location_part_description_key)          !stores
                          sto_ali:location    = loc:location
                          set(sto_ali:location_part_description_key,sto_ali:location_part_description_key)
                          loop
                              if access:stock_alias.next()
                                 break
                              end !if
                              if sto_ali:location    <> loc:location     |
                                  then break.  ! end if
                              Do GetNextRecord2
                              Loop x# = 1 To Records(glo:Queue)                                    !Loop through the selected locations
                                  Get(glo:Queue,x#)
                                  access:stock.clearkey(sto:location_part_description_key)
                                  sto:location    = glo:pointer                                    !Try and see if the part already exists
                                  sto:part_number = sto_ali:part_number
                                  sto:description = sto_ali:description
                                  if access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                                      sav:minimum_level = sto:minimum_level
                                      sav:reorder_level = sto:reorder_level
                                      sav:purchase_cost  = sto:purchase_cost
                                      sav:sale_cost      = sto:sale_cost
                                      sav:retail_cost    = sto:retail_cost
                                      ref_number# = sto:ref_number
                                      quantity_stock# = sto:quantity_stock
                                      sto:record :=: sto_ali:record
                                      sto:location    = glo:pointer
                                      sto:part_number = sto_ali:part_number
                                      sto:description = sto_ali:description
                                      sto:ref_number  = ref_number#
                                      sto:quantity_stock = quantity_stock#
                                      If costs_temp = 0
                                          sto:sale_Cost       = sav:sale_cost
                                          sto:retail_cost     = sav:retail_cost
                                          sto:purchase_cost   = sav:purchase_cost
                                      End!If costs_temp = 0
                                      If levels_temp = 0
                                          sto:minimum_level   = sav:minimum_level
                                          sto:reorder_level   = sav:reorder_level
                                      End!If levels_temp = 0
                                      access:stock.update()
      
                                      save_stm_id = access:stomodel.savefile()
                                      access:stomodel.clearkey(stm:mode_number_only_key)
                                      stm:ref_number   = sto:ref_number
                                      set(stm:mode_number_only_key,stm:mode_number_only_key)
                                      loop
                                          if access:stomodel.next()
                                             break
                                          end !if
                                          if stm:ref_number   <> sto:ref_number  |
                                              then break.  ! end if
                                          Delete(stomodel)
                                      end !loop
                                      access:stomodel.restorefile(save_stm_id)
      
                                      save_stm_ali_id = access:stomodel_alias.savefile()
                                      access:stomodel_alias.clearkey(stm_ali:model_number_key)
                                      stm_ali:ref_number   = sto_ali:ref_number
                                      stm_ali:manufacturer = sto_ali:manufacturer
                                      set(stm_ali:model_number_key,stm_ali:model_number_key)
                                      loop
                                          if access:stomodel_alias.next()
                                             break
                                          end !if
                                          if stm_ali:ref_number   <> sto_ali:ref_number      |
                                          or stm_ali:manufacturer <> sto_ali:manufacturer      |
                                              then break.  ! end if
                                          get(stomodel,0)
                                          if access:stomodel.primerecord() = Level:Benign
                                              stm:record  :=: stm_ali:record
                                              stm:ref_number  = sto:ref_number
                                              stm:location    = sto:location
                                              if access:stomodel.insert()
                                                 access:stomodel.cancelautoinc()
                                              end
                                          end!if access:stomodel.primerecord() = Level:Benign
      
                                      end !loop
                                      access:stomodel_alias.restorefile(save_stm_ali_id)
      
                                      access:locshelf.clearkey(los:shelf_location_key)        !See if the shelf location
                                      los:site_location  = glo:pointer                     !exists under the new
                                      los:shelf_location = sto:shelf_location                 !location.
                                      if access:locshelf.tryfetch(los:shelf_location_key)
                                          get(locshelf,0)
                                          if access:locshelf.primerecord() = Level:Benign
                                              los:site_location  = glo:pointer
                                              los:shelf_location = sto:shelf_location
                                              if access:locshelf.tryinsert()
                                                 access:locshelf.cancelautoinc()
                                              end
                                          End!if access:locshelf.primerecord() = Level:Benign
      
                                      end
      
                                  Else!if access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                                      get(stock,0)                                                    !If part doesn't exist. Add the part
                                      if access:stock.primerecord() = Level:Benign                    !into the new location. Make a direct
                                          ref_number# = sto:ref_number                                !copy of the alias part, except for the
                                          sto:record :=: sto_ali:record                               !ref number and location.
                                          sto:ref_number  = ref_number#
                                          sto:location    = glo:pointer
                                          sto:quantity_stock  = 0
                                          If costs_temp = 0
                                              sto:sale_cost   = 0
                                              sto:purchase_cost = 0
                                              sto:retail_cost = 0
                                          End!If costs_temp = 0
                                          If levels_temp = 0
                                              sto:minimum_level = 0
                                              sto:reorder_level = 0
                                          End!If levels_temp = 0
                                          if access:stock.insert()
                                             access:stock.cancelautoinc()
                                          Else
                                              access:locshelf.clearkey(los:shelf_location_key)        !See if the shelf location
                                              los:site_location  = glo:pointer                     !exists under the new
                                              los:shelf_location = sto:shelf_location                 !location.
                                              if access:locshelf.tryfetch(los:shelf_location_key)
                                                  get(locshelf,0)
                                                  if access:locshelf.primerecord() = Level:Benign
                                                      los:site_location  = glo:pointer
                                                      los:shelf_location = sto:shelf_location
                                                      if access:locshelf.tryinsert()
                                                         access:locshelf.cancelautoinc()
                                                      end
                                                  End!if access:locshelf.primerecord() = Level:Benign
      
                                              end
                                              If sto_ali:manufacturer <> ''
                                                  save_stm_ali_id = access:stomodel_alias.savefile()
                                                  access:stomodel_alias.clearkey(stm_ali:model_number_key)
                                                  stm_ali:ref_number   = sto_ali:ref_number
                                                  stm_ali:manufacturer = sto_ali:manufacturer
                                                  set(stm_ali:model_number_key,stm_ali:model_number_key)
                                                  loop
                                                      if access:stomodel_alias.next()
                                                         break
                                                      end !if
                                                      if stm_ali:ref_number   <> sto_ali:ref_number      |
                                                      or stm_ali:manufacturer <> sto_ali:manufacturer      |
                                                          then break.  ! end if
                                                      get(stomodel,0)
                                                      if access:stomodel.primerecord() = Level:Benign
                                                          stm:record  :=: stm_ali:record
                                                          stm:ref_number  = sto:ref_number
                                                          stm:location    = sto:location
                                                          if access:stomodel.insert()
                                                             access:stomodel.cancelautoinc()
                                                          end
                                                      end!if access:stomodel.primerecord() = Level:Benign
      
                                                  end !loop
                                                  access:stomodel_alias.restorefile(save_stm_ali_id)
                                              End!If sto_ali:manufacturer <> ''
      
                                              get(stohist,0)
                                              if access:stohist.primerecord() = level:benign
                                                  shi:ref_number           = sto:ref_number
                                                  access:users.clearkey(use:password_key)
                                                  use:password              =glo:password
                                                  access:users.fetch(use:password_key)
                                                  shi:user                  = use:user_code    
                                                  shi:date                 = today()
                                                  shi:transaction_type     = 'ADD'
                                                  shi:despatch_note_number = ''
                                                  shi:job_number           = ''
                                                  shi:quantity             = 0
                                                  shi:purchase_cost        = sto:purchase_Cost
                                                  shi:sale_cost            = sto:sale_cost
                                                  shi:retail_cost          = sto:retail_cost
                                                  shi:information          = ''
                                                  shi:notes                = 'REPLICATED FROM LOCATION: ' & Clip(loc:location)
                                                  if access:stohist.insert()
                                                     access:stohist.cancelautoinc()
                                                  end
                                              end!if access:stohist.primerecord() = level:benign
                                          end
                                      End!if access:stock.primerecord() = Level:Benign
                                  End!if access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                              End!Loop x# = 1 To Records(glo:Queue)
                          end !loop
                          access:stock_alias.restorefile(save_sto_ali_id)
                      end !loop
                      access:location.restorefile(save_loc_id)
                      setcursor()
                      close(progresswindow)
                      Case MessageEx('Process Completed.','ServiceBase 2000',|
                                     'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
      
                  Else!If f_type = 'ALL'
      
                      recordspercycle     = 25
                      recordsprocessed    = 0
                      percentprogress     = 0
                      open(progresswindow)
                      progress:thermometer    = 0
                      ?progress:pcttext{prop:text} = '0% Completed'
      
                      recordstoprocess    = Records(glo:Queue)
      
                      access:stock_alias.clearkey(sto_ali:ref_number_key)                         !Get the orignal part
                      sto_ali:ref_number  = f_ref_number
                      IF access:stock_alias.tryfetch(sto_ali:ref_number_key) = Level:Benign
                          Loop x# = 1 To Records(glo:Queue)                                    !Loop through the selected locations
                              Get(glo:Queue,x#)
                              Do GetNextRecord2
                              If glo:pointer = sto_ali:location
                                  Cycle
                              End!If glo:pointer = sto_ali:location
                              access:stock.clearkey(sto:location_part_description_key)
                              sto:location    = glo:pointer                                    !Try and see if the part already exists
                              sto:part_number = sto_ali:part_number
                              sto:description = sto_ali:description
                              if access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                                  sav:minimum_level = sto:minimum_level
                                  sav:reorder_level = sto:reorder_level
                                  sav:purchase_cost  = sto:purchase_cost
                                  sav:sale_cost      = sto:sale_cost
                                  sav:retail_cost    = sto:retail_cost
                                  ref_number# = sto:ref_number
                                  quantity_stock# = sto:quantity_stock
                                  sto:record :=: sto_ali:record
                                  sto:location    = glo:pointer
                                  sto:part_number = sto_ali:part_number
                                  sto:description = sto_ali:description
                                  sto:ref_number  = ref_number#
                                  sto:quantity_stock = quantity_stock#
                                  If costs_temp = 0
                                      sto:sale_Cost       = sav:sale_cost
                                      sto:retail_cost     = sav:retail_cost
                                      sto:purchase_cost   = sav:purchase_cost
                                  End!If costs_temp = 0
                                  If levels_temp = 0
                                      sto:minimum_level   = sav:minimum_level
                                      sto:reorder_level   = sav:reorder_level
                                  End!If levels_temp = 0
                                  access:stock.update()
                                  save_stm_id = access:stomodel.savefile()
                                  access:stomodel.clearkey(stm:mode_number_only_key)
                                  stm:ref_number   = sto:ref_number
                                  set(stm:mode_number_only_key,stm:mode_number_only_key)
                                  loop
                                      if access:stomodel.next()
                                         break
                                      end !if
                                      if stm:ref_number   <> sto:ref_number  |
                                          then break.  ! end if
                                      Delete(stomodel)
                                  end !loop
                                  access:stomodel.restorefile(save_stm_id)
      
                                  save_stm_ali_id = access:stomodel_alias.savefile()
                                  access:stomodel_alias.clearkey(stm_ali:model_number_key)
                                  stm_ali:ref_number   = sto_ali:ref_number
                                  stm_ali:manufacturer = sto_ali:manufacturer
                                  set(stm_ali:model_number_key,stm_ali:model_number_key)
                                  loop
                                      if access:stomodel_alias.next()
                                         break
                                      end !if
                                      if stm_ali:ref_number   <> sto_ali:ref_number      |
                                      or stm_ali:manufacturer <> sto_ali:manufacturer      |
                                          then break.  ! end if
                                      get(stomodel,0)
                                      if access:stomodel.primerecord() = Level:Benign
                                          stm:record  :=: stm_ali:record
                                          stm:ref_number  = sto:ref_number
                                          stm:location    = sto:location
                                          if access:stomodel.insert()
                                             access:stomodel.cancelautoinc()
                                          end
                                      end!if access:stomodel.primerecord() = Level:Benign
      
                                  end !loop
                                  access:stomodel_alias.restorefile(save_stm_ali_id)
      
                                  access:locshelf.clearkey(los:shelf_location_key)        !See if the shelf location
                                  los:site_location  = glo:pointer                     !exists under the new
                                  los:shelf_location = sto:shelf_location                 !location.
                                  if access:locshelf.tryfetch(los:shelf_location_key)
                                      get(locshelf,0)
                                      if access:locshelf.primerecord() = Level:Benign
                                          los:site_location  = glo:pointer
                                          los:shelf_location = sto:shelf_location
                                          if access:locshelf.tryinsert()
                                             access:locshelf.cancelautoinc()
                                          end
                                      End!if access:locshelf.primerecord() = Level:Benign
      
                                  end
      
                              Else!if access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                                  get(stock,0)                                                    !If part doesn't exist. Add the part
                                  if access:stock.primerecord() = Level:Benign                    !into the new location. Make a direct
                                      ref_number# = sto:ref_number                                !copy of the alias part, except for the
                                      sto:record :=: sto_ali:record                               !ref number and location.
                                      sto:ref_number  = ref_number#
                                      sto:location    = glo:pointer
                                      sto:quantity_stock  = 0
                                      If costs_temp = 0
                                          sto:sale_cost   = 0
                                          sto:purchase_cost = 0
                                          sto:retail_cost = 0
                                      End!If costs_temp = 0
                                      If levels_temp = 0
                                          sto:minimum_level = 0
                                          sto:reorder_level = 0
                                      End!If levels_temp = 0
                                      if access:stock.insert()
                                         access:stock.cancelautoinc()
                                      Else
                                          access:locshelf.clearkey(los:shelf_location_key)        !See if the shelf location
                                          los:site_location  = glo:pointer                     !exists under the new
                                          los:shelf_location = sto:shelf_location                 !location.
                                          if access:locshelf.tryfetch(los:shelf_location_key)
                                              get(locshelf,0)
                                              if access:locshelf.primerecord() = Level:Benign
                                                  los:site_location  = glo:pointer
                                                  los:shelf_location = sto:shelf_location
                                                  if access:locshelf.tryinsert()
                                                     access:locshelf.cancelautoinc()
                                                  end
                                              End!if access:locshelf.primerecord() = Level:Benign
      
                                          end
      
                                          If sto_ali:manufacturer <> ''
                                              save_stm_ali_id = access:stomodel_alias.savefile()
                                              access:stomodel_alias.clearkey(stm_ali:model_number_key)
                                              stm_ali:ref_number   = sto_ali:ref_number
                                              stm_ali:manufacturer = sto_ali:manufacturer
                                              set(stm_ali:model_number_key,stm_ali:model_number_key)
                                              loop
                                                  if access:stomodel_alias.next()
                                                     break
                                                  end !if
                                                  if stm_ali:ref_number   <> sto_ali:ref_number      |
                                                  or stm_ali:manufacturer <> sto_ali:manufacturer      |
                                                      then break.  ! end if
                                                  get(stomodel,0)
                                                  if access:stomodel.primerecord() = Level:Benign
                                                      stm:record  :=: stm_ali:record
                                                      stm:ref_number  = sto:ref_number
                                                      stm:location    = sto:location
                                                      if access:stomodel.insert()
                                                         access:stomodel.cancelautoinc()
                                                      end
                                                  end!if access:stomodel.primerecord() = Level:Benign
      
                                              end !loop
                                              access:stomodel_alias.restorefile(save_stm_ali_id)
                                          End!If sto_ali:manufacturer <> ''
      
                                          get(stohist,0)
                                          if access:stohist.primerecord() = level:benign
                                              shi:ref_number           = sto:ref_number
                                              access:users.clearkey(use:password_key)
                                              use:password              =glo:password
                                              access:users.fetch(use:password_key)
                                              shi:user                  = use:user_code    
                                              shi:date                 = today()
                                              shi:transaction_type     = 'ADD'
                                              shi:despatch_note_number = ''
                                              shi:job_number           = ''
                                              shi:quantity             = 0
                                              shi:purchase_cost        = sto:purchase_Cost
                                              shi:sale_cost            = sto:sale_cost
                                              shi:retail_cost          = sto:retail_cost
                                              shi:information          = ''
                                              shi:notes                = 'REPLICATED FROM LOCATION: ' & Clip(sto_ali:location)
                                              if access:stohist.insert()
                                                 access:stohist.cancelautoinc()
                                              end
                                          end!if access:stohist.primerecord() = level:benign
                                      end
                                  End!if access:stock.primerecord() = Level:Benign
                              End!if access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                          End!Loop x# = 1 To Records(glo:Queue)
      
                      End!IF access:stock_alias.tryfetch(sto_ali:ref_number_key) = Level:Benign
      
                      setcursor()
                      close(progresswindow)
                      Case MessageEx('Process Completed.','ServiceBase 2000',|
                                     'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
      
      
                      Post(Event:closewindow)
                  End!If f_type = 'ALL'
      
              Of 2 ! &Cancel Button
          End!Case MessageEx
      
      End!If ~Records(glo:Queue)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Ok, Accepted)
    OF ?VSNextButton
      ThisWindow.Update
         Wizard2.TakeAccepted()
    OF ?VSBackButton
      ThisWindow.Update
         Wizard2.TakeAccepted()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Pick_Other_Location')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?Browse:1{PROPLIST:MouseDownRow} > 0) 
        CASE ?Browse:1{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::6:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?Browse:1{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::6:DASUNTAGALL
      Select(?CurrentTab,1)
      Case f_Type
          Of 'ALL'
          Of 'SINGLE'
              ?title{prop:text} = 'Replicate Stock Item'
              ?general1{prop:text} = 'This facility will replicate the Stock Item you have just inserted into other locations.'
      End!Case f_Type
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = loc:Location
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = loc:Location
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Wizard2.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()


Wizard2.TakeBackEmbed PROCEDURE
   CODE

Wizard2.TakeNextEmbed PROCEDURE
   CODE

Wizard2.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
