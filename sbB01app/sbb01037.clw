

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01037.INC'),ONCE        !Local module procedure declarations
                     END


BrowseMinimumStock PROCEDURE                          !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:MinimumLevel     LONG
tmp:QuantityToOrder  LONG
tmp:QuantityOnOrder  LONG
tmp:Usage            LONG
save_sto_id          USHORT,AUTO
save_orp_id          USHORT,AUTO
save_ope_id          USHORT,AUTO
save_shi_id          USHORT,AUTO
save_smin_id         USHORT,AUTO
tmp:Location         STRING(30)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:Location
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(STOCKMIN)
                       PROJECT(smin:Description)
                       PROJECT(smin:PartNumber)
                       PROJECT(smin:Supplier)
                       PROJECT(smin:MinLevel)
                       PROJECT(smin:QuantityOnOrder)
                       PROJECT(smin:QuantityInStock)
                       PROJECT(smin:QuantityRequired)
                       PROJECT(smin:RecordNumber)
                       PROJECT(smin:Location)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
smin:Description       LIKE(smin:Description)         !List box control field - type derived from field
smin:PartNumber        LIKE(smin:PartNumber)          !List box control field - type derived from field
smin:Supplier          LIKE(smin:Supplier)            !List box control field - type derived from field
smin:MinLevel          LIKE(smin:MinLevel)            !List box control field - type derived from field
smin:QuantityOnOrder   LIKE(smin:QuantityOnOrder)     !List box control field - type derived from field
smin:QuantityInStock   LIKE(smin:QuantityInStock)     !List box control field - type derived from field
smin:QuantityRequired  LIKE(smin:QuantityRequired)    !List box control field - type derived from field
smin:RecordNumber      LIKE(smin:RecordNumber)        !Primary key field - type derived from field
smin:Location          LIKE(smin:Location)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK9::smin:Location        LIKE(smin:Location)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB8::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
QuickWindow          WINDOW('Minimum Stock Routine'),AT(,,631,279),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('BrowseMinimumStock'),ALRT(F9Key),ALRT(F10Key),SYSTEM,GRAY,DOUBLE,MDI
                       PANEL,AT(4,4,540,20),USE(?Panel1),FILL(COLOR:Silver)
                       PROMPT('Site Location'),AT(8,8),USE(?Prompt1),FONT(,,,,CHARSET:ANSI)
                       COMBO(@s20),AT(84,9,124,10),USE(tmp:Location),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                       BUTTON('&Insert'),AT(337,9,42,12),USE(?Insert),HIDE
                       BUTTON('&Process Item [F9]'),AT(552,44,76,20),USE(?Change),LEFT,ICON('desp_sm.gif')
                       BUTTON('Process &All Items [F10]'),AT(552,76,76,20),USE(?ProcessAllItems),LEFT,ICON('desp_sm.gif')
                       BUTTON('Cancel'),AT(552,256,76,20),USE(?Cancel),LEFT,ICON('cancel.ico')
                       BUTTON('&Delete'),AT(552,220,76,20),USE(?Delete),LEFT,ICON('delete.ico')
                       LIST,AT(8,60,532,212),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~Description~@s30@120L(2)|M~Part Number~@s30@120L(2)|M~Supplier~@s30@38' &|
   'L(2)|M~Min Level~@s8@38L(2)|M~On Order~@s8@38L(2)|M~In Stock~@s8@38L(2)|M~Requir' &|
   'ed~@s8@'),FROM(Queue:Browse:1)
                       SHEET,AT(4,28,540,248),USE(?CurrentTab),SPREAD
                         TAB('By Description'),USE(?Tab:2)
                           ENTRY(@s30),AT(8,44,124,10),USE(smin:Description),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Descriptoin'),TIP('Descriptoin'),UPR
                         END
                         TAB('By Part Number'),USE(?Tab2)
                           ENTRY(@s30),AT(8,44,124,10),USE(smin:PartNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Part Number'),TIP('Part Number'),UPR
                         END
                       END
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Panel1{prop:Fill} = 15066597

    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?tmp:Location{prop:ReadOnly} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 15066597
    Elsif ?tmp:Location{prop:Req} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 8454143
    Else ! If ?tmp:Location{prop:Req} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 16777215
    End ! If ?tmp:Location{prop:Req} = True
    ?tmp:Location{prop:Trn} = 0
    ?tmp:Location{prop:FontStyle} = font:Bold
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?smin:Description{prop:ReadOnly} = True
        ?smin:Description{prop:FontColor} = 65793
        ?smin:Description{prop:Color} = 15066597
    Elsif ?smin:Description{prop:Req} = True
        ?smin:Description{prop:FontColor} = 65793
        ?smin:Description{prop:Color} = 8454143
    Else ! If ?smin:Description{prop:Req} = True
        ?smin:Description{prop:FontColor} = 65793
        ?smin:Description{prop:Color} = 16777215
    End ! If ?smin:Description{prop:Req} = True
    ?smin:Description{prop:Trn} = 0
    ?smin:Description{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    If ?smin:PartNumber{prop:ReadOnly} = True
        ?smin:PartNumber{prop:FontColor} = 65793
        ?smin:PartNumber{prop:Color} = 15066597
    Elsif ?smin:PartNumber{prop:Req} = True
        ?smin:PartNumber{prop:FontColor} = 65793
        ?smin:PartNumber{prop:Color} = 8454143
    Else ! If ?smin:PartNumber{prop:Req} = True
        ?smin:PartNumber{prop:FontColor} = 65793
        ?smin:PartNumber{prop:Color} = 16777215
    End ! If ?smin:PartNumber{prop:Req} = True
    ?smin:PartNumber{prop:Trn} = 0
    ?smin:PartNumber{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'BrowseMinimumStock',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'BrowseMinimumStock',1)
    SolaceViewVars('tmp:MinimumLevel',tmp:MinimumLevel,'BrowseMinimumStock',1)
    SolaceViewVars('tmp:QuantityToOrder',tmp:QuantityToOrder,'BrowseMinimumStock',1)
    SolaceViewVars('tmp:QuantityOnOrder',tmp:QuantityOnOrder,'BrowseMinimumStock',1)
    SolaceViewVars('tmp:Usage',tmp:Usage,'BrowseMinimumStock',1)
    SolaceViewVars('save_sto_id',save_sto_id,'BrowseMinimumStock',1)
    SolaceViewVars('save_orp_id',save_orp_id,'BrowseMinimumStock',1)
    SolaceViewVars('save_ope_id',save_ope_id,'BrowseMinimumStock',1)
    SolaceViewVars('save_shi_id',save_shi_id,'BrowseMinimumStock',1)
    SolaceViewVars('save_smin_id',save_smin_id,'BrowseMinimumStock',1)
    SolaceViewVars('tmp:Location',tmp:Location,'BrowseMinimumStock',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Location;  SolaceCtrlName = '?tmp:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ProcessAllItems;  SolaceCtrlName = '?ProcessAllItems';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?smin:Description;  SolaceCtrlName = '?smin:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?smin:PartNumber;  SolaceCtrlName = '?smin:PartNumber';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowseMinimumStock')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'BrowseMinimumStock')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:LOCATION.Open
  Relate:STOCKMIN.Open
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:ORDPARTS.UseFile
  Access:ORDPEND.UseFile
  Access:ORDERS.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:SUPPLIER.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOCKMIN,SELF)
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  Case MessageEx('Minimum Stock Routine.'&|
    '<13,10>'&|
    '<13,10>Warning! This routine may take a long time to run. Are you sure you want to continue?','ServiceBase 2000',|
                 'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
      Of 1 ! &Yes Button
  
          recordspercycle         = 25
          recordsprocessed        = 0
          percentprogress         = 0
          progress:thermometer    = 0
  !        thiswindow.reset(1)
          open(progresswindow)
  
          ?progress:userstring{prop:text} = 'Running...'
          ?progress:pcttext{prop:text} = '0% Completed'
  
          recordstoprocess    = Records(STOCK)
  
          Save_sto_ID = Access:STOCK.SaveFile()
          Access:STOCK.ClearKey(sto:Ref_Number_Key)
          sto:Ref_Number = 1
          Set(sto:Ref_Number_Key,sto:Ref_Number_Key)
          Loop
              If Access:STOCK.NEXT()
                 Break
              End !If
              Do GetNextRecord2
              cancelcheck# += 1
              If cancelcheck# > (RecordsToProcess/100)
                  Do cancelcheck
                  If tmp:cancel = 1
                      Break
                  End!If tmp:cancel = 1
                  cancelcheck# = 0
              End!If cancelcheck# > 50
  
  
              !Work Out Supplier Usage Value
              Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
              sup:Company_Name = sto:Supplier
              If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                  !Found
  
              Else!If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
  
              !Work Out Minimum Level
              tmp:Usage   = 0
              tmp:MinimumLevel    = 0
              Save_shi_ID = Access:STOHIST.SaveFile()
              Access:STOHIST.ClearKey(shi:Transaction_Type_Key)
              shi:Ref_Number       = sto:Ref_Number
              shi:Transaction_Type = 'DEC'
              shi:Date             = Today() - sup:HistoryUsage
              Set(shi:Transaction_Type_Key,shi:Transaction_Type_Key)
              Loop
                  If Access:STOHIST.NEXT()
                     Break
                  End !If
                  If shi:Ref_Number       <> sto:Ref_Number      |
                  Or shi:Transaction_Type <> 'DEC'      |
                  Or shi:Date             > Today()     |
                      Then Break.  ! End If
                  tmp:Usage   += shi:Quantity
  
              End !Loop
              Access:STOHIST.RestoreFile(Save_shi_ID)
  
              tmp:MinimumLevel    = ABS(tmp:Usage * sup:Factor/100)
              !Work Out Quantity On Order
              If def:SummaryOrders !New Order Process
  
                  If tmp:MinimumLevel - sto:QuantityRequested - sto:Quantity_Stock > 1
                      If Access:STOCKMIN.PrimeRecord() = Level:Benign
                          smin:Location            = sto:Location
                          smin:PartNumber          = sto:Part_Number
                          smin:Description         = sto:Description
                          smin:Supplier            = sto:Supplier
                          smin:Usage               = tmp:Usage
                          smin:MinLevel            = tmp:MinimumLevel
                          smin:QuantityOnOrder     = sto:QuantityRequested
                          smin:QuantityInStock     = sto:Quantity_Stock
                          smin:QuantityRequired    = (tmp:MinimumLevel - sto:QuantityRequested - sto:Quantity_Stock)
                          smin:PartRefNumber       = sto:Ref_number
                          smin:QuantityJobsPending = ''
  
                          If Access:STOCKMIN.TryInsert() = Level:Benign
                              !Insert Successful
                          Else !If Access:STOCKMIN.TryInsert() = Level:Benign
                              !Insert Failed
                          End !If Access:STOCKMIN.TryInsert() = Level:Benign
                      End !If Access:STOCKMIN.PrimeRecord() = Level:Benign
                  End !If sto:Minimum_Stock - sto:QuantityRequested - sto:Quantity_Stock > 1
              Else !If def:SummaryOrders
                  tmp:QuantityToOrder = 0
                  save_ope_id = access:ordpend.savefile()                                                 !awaiting order
                  access:ordpend.clearkey(ope:part_ref_number_key)
                  ope:part_ref_number =  sto:ref_number
                  set(ope:part_ref_number_key,ope:part_ref_number_key)
                  loop
                      if access:ordpend.next()
                         break
                      end !if
                      if ope:part_ref_number <> sto:ref_number      |
                          then break.  ! end if
                      yldcnt# += 1
                      if yldcnt# > 25
                         yield() ; yldcnt# = 0
                      end !if
                      tmp:QuantityToOrder += ope:quantity
                  end !loop
                  access:ordpend.restorefile(save_ope_id)
  
                  tmp:QuantityOnOrder = 0
                  setcursor(cursor:wait)                                                                  !file. But check with the order
                  save_orp_id = access:ordparts.savefile()                                                !file first to see if the whole
                  access:ordparts.clearkey(orp:ref_number_key)                                            !order has been received. This
                  orp:part_ref_number = sto:ref_number                                                    !should hopefully speed things
                  set(orp:ref_number_key,orp:ref_number_key)                                              !up.
                  loop
                      if access:ordparts.next()
                         break
                      end !if
                      if orp:part_ref_number <> sto:ref_number      |
                          then break.  ! end if
                      If orp:order_number <> ''
                          access:orders.clearkey(ord:order_number_key)
                          ord:order_number = orp:order_number
                          if access:orders.fetch(ord:order_number_key) = Level:Benign
                              If ord:all_received = 'NO'
                                  If orp:all_received = 'NO'
                                      tmp:QuantityOnOrder += orp:quantity
                                  End!If orp:all_received <> 'YES'
                              End!If ord:all_received <> 'YES'
                          end!if access:orders.fetch(ord:order_number_key) = Level:Benign
                      End!If orp:order_number <> ''
                  end !loop
                  access:ordparts.restorefile(save_orp_id)
  
                  If tmp:MinimumLevel - (tmp:QuantityOnOrder + tmp:QuantityToOrder) - sto:Quantity_Stock > 1
                      If Access:STOCKMIN.PrimeRecord() = Level:Benign
                          smin:Location            = sto:Location
                          smin:PartNumber          = sto:Part_Number
                          smin:Description         = sto:Description
                          smin:Supplier            = sto:Supplier
                          smin:Usage               = tmp:Usage
                          smin:MinLevel            = tmp:MinimumLevel
                          smin:QuantityOnOrder     = (tmp:QuantityOnOrder + tmp:QuantityToOrder)
                          smin:QuantityInStock     = sto:Quantity_Stock
                          smin:QuantityRequired    = tmp:MinimumLevel - (tmp:QuantityOnOrder + tmp:QuantityToOrder) - sto:Quantity_Stock
                          smin:PartRefNumber       = sto:Ref_number
                          smin:QuantityJobsPending = ''
  
                          If Access:STOCKMIN.TryInsert() = Level:Benign
                              !Insert Successful
                          Else !If Access:STOCKMIN.TryInsert() = Level:Benign
                              !Insert Failed
                          End !If Access:STOCKMIN.TryInsert() = Level:Benign
                      End !If Access:STOCKMIN.PrimeRecord() = Level:Benign
                  End !If tmp:MinimumLevel - (tmp:QuantityOnOrder + tmp:QuantityToOrder) - sto:Quantity_Stock > 1
  
              End !If def:SummaryOrders
  
          End !Loop
          Access:STOCK.RestoreFile(Save_sto_ID)
          Do EndPrintRun
          close(progresswindow)
  
      Of 2 ! &No Button
          Post(Event:CloseWindow)
  End!Case MessageEx
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,smin:LocationPartNoKey)
  BRW1.AddRange(smin:Location)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?smin:PartNumber,smin:PartNumber,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,smin:LocationDescriptionKey)
  BRW1.AddRange(smin:Location)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?smin:Description,smin:Description,1,BRW1)
  BRW1.AddField(smin:Description,BRW1.Q.smin:Description)
  BRW1.AddField(smin:PartNumber,BRW1.Q.smin:PartNumber)
  BRW1.AddField(smin:Supplier,BRW1.Q.smin:Supplier)
  BRW1.AddField(smin:MinLevel,BRW1.Q.smin:MinLevel)
  BRW1.AddField(smin:QuantityOnOrder,BRW1.Q.smin:QuantityOnOrder)
  BRW1.AddField(smin:QuantityInStock,BRW1.Q.smin:QuantityInStock)
  BRW1.AddField(smin:QuantityRequired,BRW1.Q.smin:QuantityRequired)
  BRW1.AddField(smin:RecordNumber,BRW1.Q.smin:RecordNumber)
  BRW1.AddField(smin:Location,BRW1.Q.smin:Location)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  FDCB8.Init(tmp:Location,?tmp:Location,Queue:FileDropCombo.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo
  FDCB8.AddSortOrder(loc:Location_Key)
  FDCB8.AddField(loc:Location,FDCB8.Q.loc:Location)
  FDCB8.AddField(loc:RecordNumber,FDCB8.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  SELF.SetAlerts()
  brw1.InsertControl = 0
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:LOCATION.Close
    Relate:STOCKMIN.Close
  Remove(STOCKMIN)
  If Error()
      Access:STOCKMIN.Close()
      Remove(STOCKMIN)
  End !Error
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'BrowseMinimumStock',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  glo:DeleteStock = 0
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMinimumStock
    ReturnValue = GlobalResponse
  END
  If glo:DeleteStock = 1
      Delete(STOCKMIN)
  End !If glo:DeleteStock = 1
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:Location
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?ProcessAllItems
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessAllItems, Accepted)
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      
      Case MessageEx('Process All Items.'&|
        '<13,10>'&|
        '<13,10>If you continue all the parts displayed in the Minimum Stock Browse will be made ready for ordering.'&|
        '<13,10>'&|
        '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
          
              recordspercycle         = 25
              recordsprocessed        = 0
              percentprogress         = 0
              progress:thermometer    = 0
              RecordsToProcess        = 0
      
              Setcursor(Cursor:Wait)
              Save_smin_ID = Access:STOCKMIN.SaveFile()
              Access:STOCKMIN.ClearKey(smin:LocationPartNoKey)
              smin:Location   = tmp:Location
              Set(smin:LocationPartNoKey,smin:LocationPartNoKey)
              Loop
                  If Access:STOCKMIN.NEXT()
                     Break
                  End !If
                  If smin:Location   <> tmp:Location      |
                      Then Break.  ! End If
                  RecordsToProcess    += 1
              End !Loop
              Access:STOCKMIN.RestoreFile(Save_smin_ID)
              Setcursor()
      
              thiswindow.reset(1)
              open(progresswindow)
      
              ?progress:userstring{prop:text} = 'Running...'
              ?progress:pcttext{prop:text} = '0% Completed'
      
      
              Save_smin_ID = Access:STOCKMIN.SaveFile()
              Access:STOCKMIN.ClearKey(smin:LocationPartNoKey)
              smin:Location   = tmp:Location
              Set(smin:LocationPartNoKey,smin:LocationPartNoKey)
              Loop
                  If Access:STOCKMIN.NEXT()
                     Break
                  End !If
                  If smin:Location   <> tmp:Location      |
                      Then Break.  ! End If
      
                  Do GetNextRecord2
                  cancelcheck# += 1
                  If cancelcheck# > (RecordsToProcess/100)
                      Do cancelcheck
                      If tmp:cancel = 1
                          Break
                      End!If tmp:cancel = 1
                      cancelcheck# = 0
                  End!If cancelcheck# > 50
      
                  Access:STOCK.Clearkey(sto:Ref_Number_Key)
                  sto:Ref_Number  = smin:PartRefNumber
                  If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      !Found
      
                  Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                  Case def:SummaryOrders
                      Of 0 !Old Fashioned ORders
                          If access:ordpend.primerecord() = Level:Benign
                              ope:part_ref_number = sto:ref_number
                              ope:part_type       = 'STO'
                              ope:supplier        = smin:Supplier
                              ope:part_number     = sto:part_number
                              ope:Description     = sto:description
                              ope:job_number      = ''
                              ope:quantity        = smin:QuantityRequired
                              access:ordpend.insert()
                          End !If access:ordpend.primerecord() = Level:Benign
      
                      Of 1 !New ORders
                          MakePartsRequest(smin:Supplier,sto:Part_Number,sto:Description,smin:QuantityRequired)
                          sto:QuantityRequested += smin:QuantityRequired
                          Access:STOCK.Update()
      
                  End!Case def:SummaryOrders
                  Delete(STOCKMIN)
              End !Loop
              Access:STOCKMIN.RestoreFile(Save_smin_ID)
              Do EndPrintRun
              close(progresswindow)
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessAllItems, Accepted)
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Post(Event:CloseWindow)
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'BrowseMinimumStock')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?tmp:Location
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?smin:Description
      Select(?Browse:1)
    OF ?smin:PartNumber
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F9Key
              Post(Event:Accepted,?Change)
          Of F10Key
              Post(Event:Accepted,?ProcessAllItems)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

