

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('SBB01017.INC'),ONCE        !Local module procedure declarations
                     END





Browse_Stock_By_Model PROCEDURE                       !Generated from procedure template - Window

CurrentTab           STRING(80)
request              BYTE
tmp:partnumber       STRING(30)
tmp:description      STRING(30)
tmp:location         STRING(30)
save_sto_id          USHORT,AUTO
save_stm_id          USHORT,AUTO
save_loc_id          USHORT,AUTO
model_number_temp    STRING(30)
LocalRequest         LONG
FilesOpened          BYTE
location_temp        STRING(30)
no_temp              STRING('NO')
manufacturer_temp    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?location_temp
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?model_number_temp
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?manufacturer_temp
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(STOMODEL)
                       PROJECT(stm:Part_Number)
                       PROJECT(stm:Description)
                       PROJECT(stm:Ref_Number)
                       PROJECT(stm:Manufacturer)
                       PROJECT(stm:Model_Number)
                       PROJECT(stm:Location)
                       JOIN(sto:Ref_Part_Description_Key,stm:Location,stm:Ref_Number,stm:Part_Number,stm:Description)
                         PROJECT(sto:Purchase_Cost)
                         PROJECT(sto:Sale_Cost)
                         PROJECT(sto:Retail_Cost)
                         PROJECT(sto:Quantity_Stock)
                         PROJECT(sto:Shelf_Location)
                         PROJECT(sto:Second_Location)
                         PROJECT(sto:Location)
                         PROJECT(sto:Ref_Number)
                         PROJECT(sto:Part_Number)
                         PROJECT(sto:Description)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
stm:Part_Number        LIKE(stm:Part_Number)          !List box control field - type derived from field
stm:Description        LIKE(stm:Description)          !List box control field - type derived from field
sto:Purchase_Cost      LIKE(sto:Purchase_Cost)        !List box control field - type derived from field
sto:Sale_Cost          LIKE(sto:Sale_Cost)            !List box control field - type derived from field
sto:Retail_Cost        LIKE(sto:Retail_Cost)          !List box control field - type derived from field
sto:Quantity_Stock     LIKE(sto:Quantity_Stock)       !List box control field - type derived from field
sto:Shelf_Location     LIKE(sto:Shelf_Location)       !List box control field - type derived from field
sto:Second_Location    LIKE(sto:Second_Location)      !List box control field - type derived from field
stm:Ref_Number         LIKE(stm:Ref_Number)           !List box control field - type derived from field
stm:Manufacturer       LIKE(stm:Manufacturer)         !Primary key field - type derived from field
stm:Model_Number       LIKE(stm:Model_Number)         !Primary key field - type derived from field
stm:Location           LIKE(stm:Location)             !Browse key field - type derived from field
sto:Location           LIKE(sto:Location)             !Related join file key field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !Related join file key field - type derived from field
sto:Part_Number        LIKE(sto:Part_Number)          !Related join file key field - type derived from field
sto:Description        LIKE(sto:Description)          !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK18::stm:Location        LIKE(stm:Location)
HK18::stm:Model_Number    LIKE(stm:Model_Number)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB6::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
FDCB16::View:FileDropCombo VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                     END
FDCB17::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,538770663)          !Xplore
XploreMask11          DECIMAL(10,0,0)                 !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
QuickWindow          WINDOW('Browse the Stock File'),AT(,,527,326),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Model_Stock'),SYSTEM,GRAY,MAX,RESIZE
                       PANEL,AT(4,4,436,20),USE(?Panel1),FILL(COLOR:Silver)
                       PROMPT('Site Location'),AT(8,8),USE(?Prompt1)
                       COMBO(@s30),AT(64,8,124,10),USE(location_temp),VSCROLL,FONT('Tahoma',8,,FONT:bold),FORMAT('120L(2)*@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                       COMBO(@s30),AT(8,56,124,10),USE(model_number_temp),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:1)
                       COMBO(@s30),AT(8,44,124,10),USE(manufacturer_temp),IMM,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:2)
                       LIST,AT(8,88,428,212),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('124L(2)|M~Part Number~@s30@124L(2)|M~Description~@s30@[38R(2)|M~Purchase~L@n7.2@' &|
   '38R(2)|M~Trade~L@n7.2@38R(2)|M~Retail~L@n7.2@]|M~Costs~47R(2)|M~Qty In Stock~L@N' &|
   '8@120R(2)|M~Shelf Location~L@s30@120R(2)|M~Second Location~L@s30@32R(2)|M~Ref Nu' &|
   'mber~L@s8@'),FROM(Queue:Browse:1)
                       BUTTON,AT(100,304,16,16),USE(?DepleteStock),LEFT,ICON('delete.ICO')
                       PROMPT('Deplete Stock'),AT(120,308),USE(?Prompt6:2),FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                       BUTTON,AT(364,304,16,16),USE(?TransferStock),LEFT,ICON('arrow.ico')
                       PROMPT('Transfer Stock'),AT(384,308),USE(?Prompt6:3),FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                       BUTTON,AT(8,304,16,16),USE(?AddStock),LEFT,ICON('INSERT.ICO')
                       BUTTON('&Insert'),AT(448,228,76,20),USE(?Insert),LEFT,ICON('insert.ico')
                       BUTTON('&Change'),AT(448,252,76,20),USE(?Change),LEFT,ICON('edit.ico')
                       BUTTON('Delete'),AT(448,276,76,20),USE(?Delete),LEFT,ICON('delete.ico')
                       PROMPT('Shelf Location'),AT(260,44),USE(?STO:Shelf_Location:Prompt),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                       ENTRY(@s30),AT(312,44,124,10),USE(sto:Shelf_Location),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Yellow),UPR,READONLY
                       BUTTON('&Select'),AT(448,32,76,20),USE(?Select:2),LEFT,ICON('select.ico')
                       PROMPT('2nd Location'),AT(260,60),USE(?STO:Second_Location:Prompt),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                       ENTRY(@s30),AT(312,60,124,10),USE(sto:Second_Location),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                       SHEET,AT(4,28,436,296),USE(?CurrentTab),SPREAD
                         TAB('By Part Number'),USE(?Tab:6)
                           PROMPT('Manufacturer'),AT(140,44),USE(?Prompt2),FONT(,,COLOR:White,,CHARSET:ANSI)
                           PROMPT('Model Number'),AT(140,56),USE(?Prompt2:2),FONT(,,COLOR:White,,CHARSET:ANSI)
                           ENTRY(@s30),AT(8,72,124,10),USE(stm:Part_Number),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Add Stock'),AT(28,308),USE(?Prompt6),FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                           PROMPT('Stock History'),AT(208,308,44,8),USE(?StockHistory:2),FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                           PROMPT('Order Stock'),AT(292,308,44,8),USE(?StockHistory:3),FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                           BUTTON,AT(272,304,16,16),USE(?OrderStock),LEFT,ICON('phone.ico')
                           BUTTON,AT(188,304,16,16),USE(?StockHistory),LEFT,ICON('book.ico')
                         END
                         TAB('By Description'),USE(?Tab2)
                           ENTRY(@s30),AT(8,72,124,10),USE(stm:Description),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       BUTTON('Close'),AT(448,304,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
BRW1::Sort1:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

FDCB16               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB17               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Panel1{prop:Fill} = 15066597

    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?location_temp{prop:ReadOnly} = True
        ?location_temp{prop:FontColor} = 65793
        ?location_temp{prop:Color} = 15066597
    Elsif ?location_temp{prop:Req} = True
        ?location_temp{prop:FontColor} = 65793
        ?location_temp{prop:Color} = 8454143
    Else ! If ?location_temp{prop:Req} = True
        ?location_temp{prop:FontColor} = 65793
        ?location_temp{prop:Color} = 16777215
    End ! If ?location_temp{prop:Req} = True
    ?location_temp{prop:Trn} = 0
    ?location_temp{prop:FontStyle} = font:Bold
    If ?model_number_temp{prop:ReadOnly} = True
        ?model_number_temp{prop:FontColor} = 65793
        ?model_number_temp{prop:Color} = 15066597
    Elsif ?model_number_temp{prop:Req} = True
        ?model_number_temp{prop:FontColor} = 65793
        ?model_number_temp{prop:Color} = 8454143
    Else ! If ?model_number_temp{prop:Req} = True
        ?model_number_temp{prop:FontColor} = 65793
        ?model_number_temp{prop:Color} = 16777215
    End ! If ?model_number_temp{prop:Req} = True
    ?model_number_temp{prop:Trn} = 0
    ?model_number_temp{prop:FontStyle} = font:Bold
    If ?manufacturer_temp{prop:ReadOnly} = True
        ?manufacturer_temp{prop:FontColor} = 65793
        ?manufacturer_temp{prop:Color} = 15066597
    Elsif ?manufacturer_temp{prop:Req} = True
        ?manufacturer_temp{prop:FontColor} = 65793
        ?manufacturer_temp{prop:Color} = 8454143
    Else ! If ?manufacturer_temp{prop:Req} = True
        ?manufacturer_temp{prop:FontColor} = 65793
        ?manufacturer_temp{prop:Color} = 16777215
    End ! If ?manufacturer_temp{prop:Req} = True
    ?manufacturer_temp{prop:Trn} = 0
    ?manufacturer_temp{prop:FontStyle} = font:Bold
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?Prompt6:2{prop:FontColor} = -1
    ?Prompt6:2{prop:Color} = 15066597
    ?Prompt6:3{prop:FontColor} = -1
    ?Prompt6:3{prop:Color} = 15066597
    ?STO:Shelf_Location:Prompt{prop:FontColor} = -1
    ?STO:Shelf_Location:Prompt{prop:Color} = 15066597
    If ?sto:Shelf_Location{prop:ReadOnly} = True
        ?sto:Shelf_Location{prop:FontColor} = 65793
        ?sto:Shelf_Location{prop:Color} = 15066597
    Elsif ?sto:Shelf_Location{prop:Req} = True
        ?sto:Shelf_Location{prop:FontColor} = 65793
        ?sto:Shelf_Location{prop:Color} = 8454143
    Else ! If ?sto:Shelf_Location{prop:Req} = True
        ?sto:Shelf_Location{prop:FontColor} = 65793
        ?sto:Shelf_Location{prop:Color} = 16777215
    End ! If ?sto:Shelf_Location{prop:Req} = True
    ?sto:Shelf_Location{prop:Trn} = 0
    ?sto:Shelf_Location{prop:FontStyle} = font:Bold
    ?STO:Second_Location:Prompt{prop:FontColor} = -1
    ?STO:Second_Location:Prompt{prop:Color} = 15066597
    If ?sto:Second_Location{prop:ReadOnly} = True
        ?sto:Second_Location{prop:FontColor} = 65793
        ?sto:Second_Location{prop:Color} = 15066597
    Elsif ?sto:Second_Location{prop:Req} = True
        ?sto:Second_Location{prop:FontColor} = 65793
        ?sto:Second_Location{prop:Color} = 8454143
    Else ! If ?sto:Second_Location{prop:Req} = True
        ?sto:Second_Location{prop:FontColor} = 65793
        ?sto:Second_Location{prop:Color} = 16777215
    End ! If ?sto:Second_Location{prop:Req} = True
    ?sto:Second_Location{prop:Trn} = 0
    ?sto:Second_Location{prop:FontStyle} = font:Bold
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:6{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?Prompt2:2{prop:FontColor} = -1
    ?Prompt2:2{prop:Color} = 15066597
    If ?stm:Part_Number{prop:ReadOnly} = True
        ?stm:Part_Number{prop:FontColor} = 65793
        ?stm:Part_Number{prop:Color} = 15066597
    Elsif ?stm:Part_Number{prop:Req} = True
        ?stm:Part_Number{prop:FontColor} = 65793
        ?stm:Part_Number{prop:Color} = 8454143
    Else ! If ?stm:Part_Number{prop:Req} = True
        ?stm:Part_Number{prop:FontColor} = 65793
        ?stm:Part_Number{prop:Color} = 16777215
    End ! If ?stm:Part_Number{prop:Req} = True
    ?stm:Part_Number{prop:Trn} = 0
    ?stm:Part_Number{prop:FontStyle} = font:Bold
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?StockHistory:2{prop:FontColor} = -1
    ?StockHistory:2{prop:Color} = 15066597
    ?StockHistory:3{prop:FontColor} = -1
    ?StockHistory:3{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    If ?stm:Description{prop:ReadOnly} = True
        ?stm:Description{prop:FontColor} = 65793
        ?stm:Description{prop:Color} = 15066597
    Elsif ?stm:Description{prop:Req} = True
        ?stm:Description{prop:FontColor} = 65793
        ?stm:Description{prop:Color} = 8454143
    Else ! If ?stm:Description{prop:Req} = True
        ?stm:Description{prop:FontColor} = 65793
        ?stm:Description{prop:Color} = 16777215
    End ! If ?stm:Description{prop:Req} = True
    ?stm:Description{prop:Trn} = 0
    ?stm:Description{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Stock_By_Model',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Stock_By_Model',1)
    SolaceViewVars('request',request,'Browse_Stock_By_Model',1)
    SolaceViewVars('tmp:partnumber',tmp:partnumber,'Browse_Stock_By_Model',1)
    SolaceViewVars('tmp:description',tmp:description,'Browse_Stock_By_Model',1)
    SolaceViewVars('tmp:location',tmp:location,'Browse_Stock_By_Model',1)
    SolaceViewVars('save_sto_id',save_sto_id,'Browse_Stock_By_Model',1)
    SolaceViewVars('save_stm_id',save_stm_id,'Browse_Stock_By_Model',1)
    SolaceViewVars('save_loc_id',save_loc_id,'Browse_Stock_By_Model',1)
    SolaceViewVars('model_number_temp',model_number_temp,'Browse_Stock_By_Model',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Browse_Stock_By_Model',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Stock_By_Model',1)
    SolaceViewVars('location_temp',location_temp,'Browse_Stock_By_Model',1)
    SolaceViewVars('no_temp',no_temp,'Browse_Stock_By_Model',1)
    SolaceViewVars('manufacturer_temp',manufacturer_temp,'Browse_Stock_By_Model',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?location_temp;  SolaceCtrlName = '?location_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?model_number_temp;  SolaceCtrlName = '?model_number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?manufacturer_temp;  SolaceCtrlName = '?manufacturer_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DepleteStock;  SolaceCtrlName = '?DepleteStock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6:2;  SolaceCtrlName = '?Prompt6:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TransferStock;  SolaceCtrlName = '?TransferStock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6:3;  SolaceCtrlName = '?Prompt6:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AddStock;  SolaceCtrlName = '?AddStock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Shelf_Location:Prompt;  SolaceCtrlName = '?STO:Shelf_Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Shelf_Location;  SolaceCtrlName = '?sto:Shelf_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select:2;  SolaceCtrlName = '?Select:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Second_Location:Prompt;  SolaceCtrlName = '?STO:Second_Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Second_Location;  SolaceCtrlName = '?sto:Second_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:6;  SolaceCtrlName = '?Tab:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2:2;  SolaceCtrlName = '?Prompt2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:Part_Number;  SolaceCtrlName = '?stm:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StockHistory:2;  SolaceCtrlName = '?StockHistory:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StockHistory:3;  SolaceCtrlName = '?StockHistory:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OrderStock;  SolaceCtrlName = '?OrderStock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StockHistory;  SolaceCtrlName = '?StockHistory';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:Description;  SolaceCtrlName = '?stm:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('Browse_Stock_By_Model','?Browse:1') !Xplore
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Stock_By_Model')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Stock_By_Model')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  Relate:LOCATION.Open
  Relate:USERS_ALIAS.Open
  Access:MODELNUM.UseFile
  Access:STOCK.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOMODEL,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse:1,QuickWindow,?Browse:1,'sbb01app.INI','>Header',0,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,stm:Location_Part_Number_Key)
  BRW1.AddRange(stm:Location,location_temp)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?STM:Part_Number,stm:Part_Number,1,BRW1)
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,stm:Location_Description_Key)
  BRW1.AddRange(stm:Location,location_temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?STM:Description,stm:Description,1,BRW1)
  BRW1.AddResetField(location_temp)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,stm:Location_Part_Number_Key)
  BRW1.AddRange(stm:Location,location_temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?STM:Part_Number,stm:Part_Number,1,BRW1)
  BRW1.AddResetField(location_temp)
  BRW1.AddField(stm:Part_Number,BRW1.Q.stm:Part_Number)
  BRW1.AddField(stm:Description,BRW1.Q.stm:Description)
  BRW1.AddField(sto:Purchase_Cost,BRW1.Q.sto:Purchase_Cost)
  BRW1.AddField(sto:Sale_Cost,BRW1.Q.sto:Sale_Cost)
  BRW1.AddField(sto:Retail_Cost,BRW1.Q.sto:Retail_Cost)
  BRW1.AddField(sto:Quantity_Stock,BRW1.Q.sto:Quantity_Stock)
  BRW1.AddField(sto:Shelf_Location,BRW1.Q.sto:Shelf_Location)
  BRW1.AddField(sto:Second_Location,BRW1.Q.sto:Second_Location)
  BRW1.AddField(stm:Ref_Number,BRW1.Q.stm:Ref_Number)
  BRW1.AddField(stm:Manufacturer,BRW1.Q.stm:Manufacturer)
  BRW1.AddField(stm:Model_Number,BRW1.Q.stm:Model_Number)
  BRW1.AddField(stm:Location,BRW1.Q.stm:Location)
  BRW1.AddField(sto:Location,BRW1.Q.sto:Location)
  BRW1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  BRW1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  BRW1.AddField(sto:Description,BRW1.Q.sto:Description)
  QuickWindow{PROP:MinWidth}=584
  QuickWindow{PROP:MinHeight}=210
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  FDCB6.Init(location_temp,?location_temp,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(loc:Location_Key)
  FDCB6.AddField(loc:Location,FDCB6.Q.loc:Location)
  FDCB6.AddField(loc:RecordNumber,FDCB6.Q.loc:RecordNumber)
  FDCB6.AddUpdateField(loc:Location,location_temp)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  FDCB16.Init(model_number_temp,?model_number_temp,Queue:FileDropCombo:1.ViewPosition,FDCB16::View:FileDropCombo,Queue:FileDropCombo:1,Relate:MODELNUM,ThisWindow,GlobalErrors,0,1,0)
  FDCB16.Q &= Queue:FileDropCombo:1
  FDCB16.AddSortOrder(mod:Manufacturer_Key)
  FDCB16.AddRange(mod:Manufacturer,manufacturer_temp)
  FDCB16.AddField(mod:Model_Number,FDCB16.Q.mod:Model_Number)
  ThisWindow.AddItem(FDCB16.WindowComponent)
  FDCB16.DefaultFill = 0
  FDCB17.Init(manufacturer_temp,?manufacturer_temp,Queue:FileDropCombo:2.ViewPosition,FDCB17::View:FileDropCombo,Queue:FileDropCombo:2,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB17.Q &= Queue:FileDropCombo:2
  FDCB17.AddSortOrder(man:Manufacturer_Key)
  FDCB17.AddField(man:Manufacturer,FDCB17.Q.man:Manufacturer)
  FDCB17.AddField(man:RecordNumber,FDCB17.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB17.WindowComponent)
  FDCB17.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:6{PROP:TEXT} = 'By Part Number'
    ?Tab2{PROP:TEXT} = 'By Description'
    ?Browse:1{PROP:FORMAT} ='124L(2)|M~Part Number~@s30@#1#124L(2)|M~Description~@s30@#2#38R(2)|M~Purchase~L@n7.2@#3#38R(2)|M~Trade~L@n7.2@#4#38R(2)|M~Retail~L@n7.2@#5#47R(2)|M~Qty In Stock~L@N8@#6#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
    Relate:LOCATION.Close
    Relate:USERS_ALIAS.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('Browse_Stock_By_Model','?Browse:1',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Stock_By_Model',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(stm:Part_Number,BRW1.Q.stm:Part_Number)
  Xplore1.AddField(stm:Description,BRW1.Q.stm:Description)
  Xplore1.AddField(sto:Purchase_Cost,BRW1.Q.sto:Purchase_Cost)
  Xplore1.AddField(sto:Sale_Cost,BRW1.Q.sto:Sale_Cost)
  Xplore1.AddField(sto:Retail_Cost,BRW1.Q.sto:Retail_Cost)
  Xplore1.AddField(sto:Quantity_Stock,BRW1.Q.sto:Quantity_Stock)
  Xplore1.AddField(sto:Shelf_Location,BRW1.Q.sto:Shelf_Location)
  Xplore1.AddField(sto:Second_Location,BRW1.Q.sto:Second_Location)
  Xplore1.AddField(stm:Ref_Number,BRW1.Q.stm:Ref_Number)
  Xplore1.AddField(stm:Manufacturer,BRW1.Q.stm:Manufacturer)
  Xplore1.AddField(stm:Model_Number,BRW1.Q.stm:Model_Number)
  Xplore1.AddField(stm:Location,BRW1.Q.stm:Location)
  Xplore1.AddField(sto:Location,BRW1.Q.sto:Location)
  Xplore1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  Xplore1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  Xplore1.AddField(sto:Description,BRW1.Q.sto:Description)
  BRW1.FileOrderNbr = BRW1.AddSortOrder(,stm:Location_Part_Number_Key) !Xplore Sort Order for File Sequence
  BRW1.AddRange(stm:Location,location_temp)           !Xplore
  BRW1.SetOrder('')                                   !Xplore
  BRW1.ViewOrder = True                               !Xplore
  Xplore1.AddAllColumnSortOrders(1)                   !Xplore
  BRW1.ViewOrder = False                              !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?location_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?location_temp, Accepted)
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      !Free(BRW1.ListQueue)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?location_temp, Accepted)
    OF ?model_number_temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      !Free(BRW1.ListQueue)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?manufacturer_temp)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?manufacturer_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?manufacturer_temp, Accepted)
      FDCB16.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?manufacturer_temp, Accepted)
    OF ?DepleteStock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DepleteStock, Accepted)
      If SecurityCheck('STOCK - DEPLETE STOCK')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!if x" = false
      
          Thiswindow.reset
          error# = 0
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = brw1.q.stm:ref_number
          if access:stock.fetch(sto:ref_number_key)
              error# = 1
          Else!if access:stock.fetch(sto:ref_number_key)
              If sto:sundry_item = 'YES'
                  Case MessageEx('Cannot Deplete Stock! This item has been marked as a Sundry Item.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End!If sto:sundry_item = 'YES'
          end!if access:stock.fetch(sto:ref_number_key)
      
          If error# = 0
              Deplete_Stock(brw1.q.stm:ref_number)
          End!If error# = 0
      
      End!!if x" = false
      
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DepleteStock, Accepted)
    OF ?TransferStock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TransferStock, Accepted)
      If SecurityCheck('STOCK - STOCK TRANSFER')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!if x" = false
      
          Thiswindow.reset
          glo:select1 = location_temp
          glo:select2 = brw1.q.STm:Ref_Number
       
      Transfer_Stock()
          glo:select1 = ''
          glo:select2 = ''
      
      End!Else!if x" = false
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TransferStock, Accepted)
    OF ?AddStock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AddStock, Accepted)
      If SecurityCheck('STOCK - ADD STOCK')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!if x" = false
      
          Thiswindow.reset
          error# = 0
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = brw1.q.stm:ref_number
          if access:stock.fetch(sto:ref_number_key)
              error# = 1
          Else!if access:stock.fetch(sto:ref_number_key)
              If sto:sundry_item = 'YES'
                  Case MessageEx('Cannot Add Stock. This part has been marked as a Sundry Item.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End!If sto:sundry_item = 'YES'
          end!if access:stock.fetch(sto:ref_number_key)
      
          If error# = 0
              Add_Stock(brw1.q.stm:ref_number)
          End!If error# = 0
      
      end!if x" = false
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AddStock, Accepted)
    OF ?Insert
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert, Accepted)
      thiswindow.reset(1)
      Globalrequest   = Insertrecord
      Request = Globalrequest
      access:stock.primerecord()
      Include('StockUpd.inc','BeforeUpdate')
      Updatestock
      Include('StockUpd.inc','AfterUpdate')
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert, Accepted)
    OF ?Change
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change, Accepted)
      thiswindow.reset(1)
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number  = brw1.q.stm:ref_number
      If access:stock.fetch(sto:ref_number_key) = Level:Benign
          Globalrequest   = ChangeRecord
          request = Globalrequest
          Include('StockUpd.inc','BeforeUpdate')
          Updatestock
          Include('StockUpd.inc','AfterUpdate')
      End!If access:stock.fetch(sto:ref_number_key) = Level:Benign
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change, Accepted)
    OF ?Delete
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Delete, Accepted)
      thiswindow.reset(1)
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number  = brw1.q.stm:ref_number
      If access:stock.fetch(sto:ref_number_key) = Level:Benign
          Globalrequest   = DeleteRecord
          request = Globalrequest
          Include('StockUpd.inc','BeforeUpdate')
          Updatestock
          Include('StockUpd.inc','AfterUpdate')
      End!If access:stock.fetch(sto:ref_number_key) = Level:Benign
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Delete, Accepted)
    OF ?OrderStock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OrderStock, Accepted)
      If SecurityCheck('STOCK - ORDER STOCK')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!if x" = false
          Thiswindow.reset
          error# = 0
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = brw1.q.stm:ref_number
          if access:stock.fetch(sto:ref_number_key)
              error# = 1
          Else!if access:stock.fetch(sto:ref_number_key)
              If sto:sundry_item = 'YES'
                  Case MessageEx('Cannot Order! This item has been marked as a Sundry Item.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              Else
                  If sto:Suspend
                      error# = 1
                      Case MessageEx('This part cannot be Ordered. It has been suspended.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  End !If sto:Suspend
              End!If sto:sundry_item = 'YES'
          end!if access:stock.fetch(sto:ref_number_key)
      
          If error# = 0
              glo:select1 = sto:ref_number
              Stock_Order
              glo:select1 = ''
          End!If error# = 0
      
      End!Else!if x" = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OrderStock, Accepted)
    OF ?StockHistory
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StockHistory, Accepted)
      If SecurityCheck('STOCK - STOCK HISTORY')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!if x" = false
      
          Thiswindow.reset
          error# = 0
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = brw1.q.stm:ref_number
          if access:stock.fetch(sto:ref_number_key) = Level:Benign
              glo:select1 = STO:Ref_Number
              Browse_Stock_History
              glo:select1 = ''
          End!if access:stock.fetch(sto:ref_number_key) = Level:Benign
          
      End!Else!if x" = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StockHistory, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Stock_By_Model')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      If Keycode() = MouseLeft2
          Post(Event:accepted,?Change)
      End!If Keycode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?CurrentTab
        SELECT(?Browse:1)                   ! Reselect list box after tab change
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='124L(2)|M~Part Number~@s30@#1#124L(2)|M~Description~@s30@#2#38R(2)|M~Purchase~L@n7.2@#3#38R(2)|M~Trade~L@n7.2@#4#38R(2)|M~Retail~L@n7.2@#5#47R(2)|M~Qty In Stock~L@N8@#6#'
          ?Tab:6{PROP:TEXT} = 'By Part Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='124L(2)|M~Description~@s30@#2#124L(2)|M~Part Number~@s30@#1#38R(2)|M~Purchase~L@n7.2@#3#38R(2)|M~Trade~L@n7.2@#4#38R(2)|M~Retail~L@n7.2@#5#47R(2)|M~Qty In Stock~L@N8@#6#'
          ?Tab2{PROP:TEXT} = 'By Description'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Set(defstock)
      Access:defstock.next()
      If dst:use_site_location = 'YES'
          location_temp = dst:site_location
      Else
          location_temp   = ''
      End
      Display()
      Select(?Browse:1)
      BRW1.ResetSort(1)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      FDCB6.ResetQueue(1)
      FDCB17.ResetQueue(1)
      FDCB16.ResetQueue(1)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    OF EVENT:Sized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Sized',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
    OF EVENT:Iconized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Iconized',PRIORITY(9000)
      OF xpEVENT:Xplore + 1                           !Xplore
        Xplore1.GetColumnInfo()                       !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = model_number_temp
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = location_temp
     end
  ELSIF Choice(?CurrentTab) = 1
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = model_number_temp
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = location_temp
     end
  ELSE
  END
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF Choice(?CurrentTab) = 1
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?Browse:1                              !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?Browse:1                         !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue LONG
SaveCopies  LONG
  CODE
  !CPCS Previewer (c) copyright Creative PC Solutions, 1995-97
  ReturnValue                  = PrintPreview(PQ,0,,CPCS,0,,,,)
  CASE ReturnValue
  OF RequestCompleted !1
     SaveCopies                = PRINTER{PROPPRINT:Copies}
     PRINTER{PROPPRINT:Copies} = CPCS{PROPPRINT:Copies}
     HandleCopies(PQ,CPCS{PROPPRINT:Copies})
     CPCS{PROP:FlushPreview}   = True
     PRINTER{PROPPRINT:Copies} = SaveCopies
  OF RequestCancelled !2
  END
  RETURN (False)
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !stm:Part_Number
  OF 2 !stm:Description
  OF 3 !sto:Purchase_Cost
  OF 4 !sto:Sale_Cost
  OF 5 !sto:Retail_Cost
  OF 6 !sto:Quantity_Stock
  OF 7 !sto:Shelf_Location
  OF 8 !sto:Second_Location
  OF 9 !stm:Ref_Number
  OF 10 !stm:Manufacturer
  OF 11 !stm:Model_Number
  OF 12 !stm:Location
  OF 13 !sto:Location
  OF 14 !sto:Ref_Number
  OF 15 !sto:Part_Number
  OF 16 !sto:Description
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(STOMODEL)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('stm:Part_Number')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Part Number')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Part Number')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('stm:Description')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Description')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Description')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Purchase_Cost')          !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Purchase')                   !Header
                PSTRING('@n7.2')                      !Picture
                PSTRING('Purchase')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Sale_Cost')              !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Trade')                      !Header
                PSTRING('@n7.2')                      !Picture
                PSTRING('Trade')                      !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Retail_Cost')            !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Retail')                     !Header
                PSTRING('@n7.2')                      !Picture
                PSTRING('Retail')                     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Quantity_Stock')         !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Qty In Stock')               !Header
                PSTRING('@N8')                        !Picture
                PSTRING('Qty In Stock')               !Description
                STRING('D')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Shelf_Location')         !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Shelf Location')             !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Shelf Location')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Second_Location')        !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Second Location')            !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Second Location')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('stm:Ref_Number')             !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Ref Number')                 !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Ref Number')                 !Description
                STRING('R')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('stm:Manufacturer')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Manufacturer')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Manufacturer')               !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('stm:Model_Number')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Model Number')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Model Number')               !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('stm:Location')               !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Location')                   !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Location')                   !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Location')               !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Location')                   !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Location')                   !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Ref_Number')             !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Reference Number')           !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Reference Number')           !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Part_Number')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Part Number')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Part Number')                !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Description')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Description')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Description')                !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                END
XpDim           SHORT(16)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Panel1, Resize:FixLeft+Resize:FixTop, Resize:LockHeight)
  SELF.SetStrategy(?location_temp, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?STM:Part_Number, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?STM:Description, Resize:FixLeft+Resize:FixTop, Resize:LockSize)


FDCB6.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (loc:Main_Store = 'YES')
    SELF.Q.loc:Location_NormalFG = 255
    SELF.Q.loc:Location_NormalBG = 16777215
    SELF.Q.loc:Location_SelectedFG = 16777215
    SELF.Q.loc:Location_SelectedBG = 255
  ELSIF (loc:Active = 0)
    SELF.Q.loc:Location_NormalFG = 8421504
    SELF.Q.loc:Location_NormalBG = 16777215
    SELF.Q.loc:Location_SelectedFG = 16777215
    SELF.Q.loc:Location_SelectedBG = 8421504
  ELSE
    SELF.Q.loc:Location_NormalFG = -1
    SELF.Q.loc:Location_NormalBG = -1
    SELF.Q.loc:Location_SelectedFG = -1
    SELF.Q.loc:Location_SelectedBG = -1
  END

