

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01011.INC'),ONCE        !Local module procedure declarations
                     END


UpdateSUPPLIER PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::sup:Record  LIKE(sup:RECORD),STATIC
!! ** Bryan Harrison (c)1998 **
temp_string         String(255)
QuickWindow          WINDOW('Update the SUPPLIER File'),AT(,,219,331),FONT('Tahoma',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('UpdateSUPPLIER'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,296),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Company Name'),AT(8,20),USE(?SUP:Company_Name:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(sup:Company_Name),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Account Number'),AT(8,36),USE(?SUP:Account_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,36,64,10),USE(sup:Account_Number),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Postcode'),AT(8,52),USE(?SUP:Postcode:Prompt),TRN
                           ENTRY(@s10),AT(84,52,64,10),USE(sup:Postcode),FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON('Clear'),AT(152,52,56,10),USE(?Clear_Address),SKIP,LEFT,ICON(ICON:Cut)
                           PROMPT('Address'),AT(8,68),USE(?SUP:Address_Line1:Prompt),TRN
                           ENTRY(@s30),AT(84,64,124,10),USE(sup:Address_Line1),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(84,76,124,10),USE(sup:Address_Line2),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(84,88,124,10),USE(sup:Address_Line3),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Telephone Number'),AT(8,104),USE(?SUP:Telephone_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,104,64,10),USE(sup:Telephone_Number),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fax Number'),AT(8,120),USE(?SUP:Fax_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,120,64,10),USE(sup:Fax_Number),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Email Address'),AT(8,136),USE(?sup:EmailAddress:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(84,136,124,10),USE(sup:EmailAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Address'),TIP('Email Address')
                           PROMPT('Contact Name'),AT(8,152),USE(?SUP:Contact_Name:Prompt),TRN
                           ENTRY(@s60),AT(84,152,124,10),USE(sup:Contact_Name),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Notes'),AT(8,168),USE(?SUP:Notes:Prompt),TRN
                           TEXT,AT(84,168,124,52),USE(sup:Notes),FONT('Tahoma',8,,FONT:bold),UPR
                           GROUP('Stock Order'),AT(8,220,200,76),USE(?Group1),BOXED
                             PROMPT('History Usage'),AT(12,232),USE(?sup:HistoryUsage:Prompt),TRN
                             SPIN(@n2),AT(84,232,64,10),USE(sup:HistoryUsage),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('History Usage'),TIP('History Usage'),UPR,STEP(1)
                             PROMPT('Multiply By Factor Of'),AT(12,248),USE(?sup:Factor:Prompt),TRN
                             ENTRY(@n14.2),AT(84,248,64,10),USE(sup:Factor),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Multiply By Factor Of'),TIP('Multiply By Factor Of'),UPR
                             PROMPT('Normal Supply Period'),AT(12,264),USE(?SUP:Normal_Supply_Period:Prompt),TRN
                             ENTRY(@n3b),AT(84,264,64,10),USE(sup:Normal_Supply_Period),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Days'),AT(152,264),USE(?Prompt10)
                             ENTRY(@n14.2),AT(84,280,64,10),USE(sup:Minimum_Order_Value),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Minimum Order Value'),AT(12,280),USE(?SUP:Minimum_Order_Value:Prompt),TRN
                             PROMPT('Days'),AT(152,232),USE(?Prompt10:2)
                             PROMPT('%'),AT(152,248),USE(?Percent)
                           END
                         END
                       END
                       BUTTON('Replicate Manufacturer'),AT(8,308,72,16),USE(?Replicate_Manufacturer),LEFT,ICON('arrow.ico')
                       BUTTON('&OK'),AT(100,308,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(156,308,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                       PANEL,AT(4,304,212,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?SUP:Company_Name:Prompt{prop:FontColor} = -1
    ?SUP:Company_Name:Prompt{prop:Color} = 15066597
    If ?sup:Company_Name{prop:ReadOnly} = True
        ?sup:Company_Name{prop:FontColor} = 65793
        ?sup:Company_Name{prop:Color} = 15066597
    Elsif ?sup:Company_Name{prop:Req} = True
        ?sup:Company_Name{prop:FontColor} = 65793
        ?sup:Company_Name{prop:Color} = 8454143
    Else ! If ?sup:Company_Name{prop:Req} = True
        ?sup:Company_Name{prop:FontColor} = 65793
        ?sup:Company_Name{prop:Color} = 16777215
    End ! If ?sup:Company_Name{prop:Req} = True
    ?sup:Company_Name{prop:Trn} = 0
    ?sup:Company_Name{prop:FontStyle} = font:Bold
    ?SUP:Account_Number:Prompt{prop:FontColor} = -1
    ?SUP:Account_Number:Prompt{prop:Color} = 15066597
    If ?sup:Account_Number{prop:ReadOnly} = True
        ?sup:Account_Number{prop:FontColor} = 65793
        ?sup:Account_Number{prop:Color} = 15066597
    Elsif ?sup:Account_Number{prop:Req} = True
        ?sup:Account_Number{prop:FontColor} = 65793
        ?sup:Account_Number{prop:Color} = 8454143
    Else ! If ?sup:Account_Number{prop:Req} = True
        ?sup:Account_Number{prop:FontColor} = 65793
        ?sup:Account_Number{prop:Color} = 16777215
    End ! If ?sup:Account_Number{prop:Req} = True
    ?sup:Account_Number{prop:Trn} = 0
    ?sup:Account_Number{prop:FontStyle} = font:Bold
    ?SUP:Postcode:Prompt{prop:FontColor} = -1
    ?SUP:Postcode:Prompt{prop:Color} = 15066597
    If ?sup:Postcode{prop:ReadOnly} = True
        ?sup:Postcode{prop:FontColor} = 65793
        ?sup:Postcode{prop:Color} = 15066597
    Elsif ?sup:Postcode{prop:Req} = True
        ?sup:Postcode{prop:FontColor} = 65793
        ?sup:Postcode{prop:Color} = 8454143
    Else ! If ?sup:Postcode{prop:Req} = True
        ?sup:Postcode{prop:FontColor} = 65793
        ?sup:Postcode{prop:Color} = 16777215
    End ! If ?sup:Postcode{prop:Req} = True
    ?sup:Postcode{prop:Trn} = 0
    ?sup:Postcode{prop:FontStyle} = font:Bold
    ?SUP:Address_Line1:Prompt{prop:FontColor} = -1
    ?SUP:Address_Line1:Prompt{prop:Color} = 15066597
    If ?sup:Address_Line1{prop:ReadOnly} = True
        ?sup:Address_Line1{prop:FontColor} = 65793
        ?sup:Address_Line1{prop:Color} = 15066597
    Elsif ?sup:Address_Line1{prop:Req} = True
        ?sup:Address_Line1{prop:FontColor} = 65793
        ?sup:Address_Line1{prop:Color} = 8454143
    Else ! If ?sup:Address_Line1{prop:Req} = True
        ?sup:Address_Line1{prop:FontColor} = 65793
        ?sup:Address_Line1{prop:Color} = 16777215
    End ! If ?sup:Address_Line1{prop:Req} = True
    ?sup:Address_Line1{prop:Trn} = 0
    ?sup:Address_Line1{prop:FontStyle} = font:Bold
    If ?sup:Address_Line2{prop:ReadOnly} = True
        ?sup:Address_Line2{prop:FontColor} = 65793
        ?sup:Address_Line2{prop:Color} = 15066597
    Elsif ?sup:Address_Line2{prop:Req} = True
        ?sup:Address_Line2{prop:FontColor} = 65793
        ?sup:Address_Line2{prop:Color} = 8454143
    Else ! If ?sup:Address_Line2{prop:Req} = True
        ?sup:Address_Line2{prop:FontColor} = 65793
        ?sup:Address_Line2{prop:Color} = 16777215
    End ! If ?sup:Address_Line2{prop:Req} = True
    ?sup:Address_Line2{prop:Trn} = 0
    ?sup:Address_Line2{prop:FontStyle} = font:Bold
    If ?sup:Address_Line3{prop:ReadOnly} = True
        ?sup:Address_Line3{prop:FontColor} = 65793
        ?sup:Address_Line3{prop:Color} = 15066597
    Elsif ?sup:Address_Line3{prop:Req} = True
        ?sup:Address_Line3{prop:FontColor} = 65793
        ?sup:Address_Line3{prop:Color} = 8454143
    Else ! If ?sup:Address_Line3{prop:Req} = True
        ?sup:Address_Line3{prop:FontColor} = 65793
        ?sup:Address_Line3{prop:Color} = 16777215
    End ! If ?sup:Address_Line3{prop:Req} = True
    ?sup:Address_Line3{prop:Trn} = 0
    ?sup:Address_Line3{prop:FontStyle} = font:Bold
    ?SUP:Telephone_Number:Prompt{prop:FontColor} = -1
    ?SUP:Telephone_Number:Prompt{prop:Color} = 15066597
    If ?sup:Telephone_Number{prop:ReadOnly} = True
        ?sup:Telephone_Number{prop:FontColor} = 65793
        ?sup:Telephone_Number{prop:Color} = 15066597
    Elsif ?sup:Telephone_Number{prop:Req} = True
        ?sup:Telephone_Number{prop:FontColor} = 65793
        ?sup:Telephone_Number{prop:Color} = 8454143
    Else ! If ?sup:Telephone_Number{prop:Req} = True
        ?sup:Telephone_Number{prop:FontColor} = 65793
        ?sup:Telephone_Number{prop:Color} = 16777215
    End ! If ?sup:Telephone_Number{prop:Req} = True
    ?sup:Telephone_Number{prop:Trn} = 0
    ?sup:Telephone_Number{prop:FontStyle} = font:Bold
    ?SUP:Fax_Number:Prompt{prop:FontColor} = -1
    ?SUP:Fax_Number:Prompt{prop:Color} = 15066597
    If ?sup:Fax_Number{prop:ReadOnly} = True
        ?sup:Fax_Number{prop:FontColor} = 65793
        ?sup:Fax_Number{prop:Color} = 15066597
    Elsif ?sup:Fax_Number{prop:Req} = True
        ?sup:Fax_Number{prop:FontColor} = 65793
        ?sup:Fax_Number{prop:Color} = 8454143
    Else ! If ?sup:Fax_Number{prop:Req} = True
        ?sup:Fax_Number{prop:FontColor} = 65793
        ?sup:Fax_Number{prop:Color} = 16777215
    End ! If ?sup:Fax_Number{prop:Req} = True
    ?sup:Fax_Number{prop:Trn} = 0
    ?sup:Fax_Number{prop:FontStyle} = font:Bold
    ?sup:EmailAddress:Prompt{prop:FontColor} = -1
    ?sup:EmailAddress:Prompt{prop:Color} = 15066597
    If ?sup:EmailAddress{prop:ReadOnly} = True
        ?sup:EmailAddress{prop:FontColor} = 65793
        ?sup:EmailAddress{prop:Color} = 15066597
    Elsif ?sup:EmailAddress{prop:Req} = True
        ?sup:EmailAddress{prop:FontColor} = 65793
        ?sup:EmailAddress{prop:Color} = 8454143
    Else ! If ?sup:EmailAddress{prop:Req} = True
        ?sup:EmailAddress{prop:FontColor} = 65793
        ?sup:EmailAddress{prop:Color} = 16777215
    End ! If ?sup:EmailAddress{prop:Req} = True
    ?sup:EmailAddress{prop:Trn} = 0
    ?sup:EmailAddress{prop:FontStyle} = font:Bold
    ?SUP:Contact_Name:Prompt{prop:FontColor} = -1
    ?SUP:Contact_Name:Prompt{prop:Color} = 15066597
    If ?sup:Contact_Name{prop:ReadOnly} = True
        ?sup:Contact_Name{prop:FontColor} = 65793
        ?sup:Contact_Name{prop:Color} = 15066597
    Elsif ?sup:Contact_Name{prop:Req} = True
        ?sup:Contact_Name{prop:FontColor} = 65793
        ?sup:Contact_Name{prop:Color} = 8454143
    Else ! If ?sup:Contact_Name{prop:Req} = True
        ?sup:Contact_Name{prop:FontColor} = 65793
        ?sup:Contact_Name{prop:Color} = 16777215
    End ! If ?sup:Contact_Name{prop:Req} = True
    ?sup:Contact_Name{prop:Trn} = 0
    ?sup:Contact_Name{prop:FontStyle} = font:Bold
    ?SUP:Notes:Prompt{prop:FontColor} = -1
    ?SUP:Notes:Prompt{prop:Color} = 15066597
    If ?sup:Notes{prop:ReadOnly} = True
        ?sup:Notes{prop:FontColor} = 65793
        ?sup:Notes{prop:Color} = 15066597
    Elsif ?sup:Notes{prop:Req} = True
        ?sup:Notes{prop:FontColor} = 65793
        ?sup:Notes{prop:Color} = 8454143
    Else ! If ?sup:Notes{prop:Req} = True
        ?sup:Notes{prop:FontColor} = 65793
        ?sup:Notes{prop:Color} = 16777215
    End ! If ?sup:Notes{prop:Req} = True
    ?sup:Notes{prop:Trn} = 0
    ?sup:Notes{prop:FontStyle} = font:Bold
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?sup:HistoryUsage:Prompt{prop:FontColor} = -1
    ?sup:HistoryUsage:Prompt{prop:Color} = 15066597
    If ?sup:HistoryUsage{prop:ReadOnly} = True
        ?sup:HistoryUsage{prop:FontColor} = 65793
        ?sup:HistoryUsage{prop:Color} = 15066597
    Elsif ?sup:HistoryUsage{prop:Req} = True
        ?sup:HistoryUsage{prop:FontColor} = 65793
        ?sup:HistoryUsage{prop:Color} = 8454143
    Else ! If ?sup:HistoryUsage{prop:Req} = True
        ?sup:HistoryUsage{prop:FontColor} = 65793
        ?sup:HistoryUsage{prop:Color} = 16777215
    End ! If ?sup:HistoryUsage{prop:Req} = True
    ?sup:HistoryUsage{prop:Trn} = 0
    ?sup:HistoryUsage{prop:FontStyle} = font:Bold
    ?sup:Factor:Prompt{prop:FontColor} = -1
    ?sup:Factor:Prompt{prop:Color} = 15066597
    If ?sup:Factor{prop:ReadOnly} = True
        ?sup:Factor{prop:FontColor} = 65793
        ?sup:Factor{prop:Color} = 15066597
    Elsif ?sup:Factor{prop:Req} = True
        ?sup:Factor{prop:FontColor} = 65793
        ?sup:Factor{prop:Color} = 8454143
    Else ! If ?sup:Factor{prop:Req} = True
        ?sup:Factor{prop:FontColor} = 65793
        ?sup:Factor{prop:Color} = 16777215
    End ! If ?sup:Factor{prop:Req} = True
    ?sup:Factor{prop:Trn} = 0
    ?sup:Factor{prop:FontStyle} = font:Bold
    ?SUP:Normal_Supply_Period:Prompt{prop:FontColor} = -1
    ?SUP:Normal_Supply_Period:Prompt{prop:Color} = 15066597
    If ?sup:Normal_Supply_Period{prop:ReadOnly} = True
        ?sup:Normal_Supply_Period{prop:FontColor} = 65793
        ?sup:Normal_Supply_Period{prop:Color} = 15066597
    Elsif ?sup:Normal_Supply_Period{prop:Req} = True
        ?sup:Normal_Supply_Period{prop:FontColor} = 65793
        ?sup:Normal_Supply_Period{prop:Color} = 8454143
    Else ! If ?sup:Normal_Supply_Period{prop:Req} = True
        ?sup:Normal_Supply_Period{prop:FontColor} = 65793
        ?sup:Normal_Supply_Period{prop:Color} = 16777215
    End ! If ?sup:Normal_Supply_Period{prop:Req} = True
    ?sup:Normal_Supply_Period{prop:Trn} = 0
    ?sup:Normal_Supply_Period{prop:FontStyle} = font:Bold
    ?Prompt10{prop:FontColor} = -1
    ?Prompt10{prop:Color} = 15066597
    If ?sup:Minimum_Order_Value{prop:ReadOnly} = True
        ?sup:Minimum_Order_Value{prop:FontColor} = 65793
        ?sup:Minimum_Order_Value{prop:Color} = 15066597
    Elsif ?sup:Minimum_Order_Value{prop:Req} = True
        ?sup:Minimum_Order_Value{prop:FontColor} = 65793
        ?sup:Minimum_Order_Value{prop:Color} = 8454143
    Else ! If ?sup:Minimum_Order_Value{prop:Req} = True
        ?sup:Minimum_Order_Value{prop:FontColor} = 65793
        ?sup:Minimum_Order_Value{prop:Color} = 16777215
    End ! If ?sup:Minimum_Order_Value{prop:Req} = True
    ?sup:Minimum_Order_Value{prop:Trn} = 0
    ?sup:Minimum_Order_Value{prop:FontStyle} = font:Bold
    ?SUP:Minimum_Order_Value:Prompt{prop:FontColor} = -1
    ?SUP:Minimum_Order_Value:Prompt{prop:Color} = 15066597
    ?Prompt10:2{prop:FontColor} = -1
    ?Prompt10:2{prop:Color} = 15066597
    ?Percent{prop:FontColor} = -1
    ?Percent{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateSUPPLIER',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateSUPPLIER',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateSUPPLIER',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateSUPPLIER',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateSUPPLIER',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateSUPPLIER',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUP:Company_Name:Prompt;  SolaceCtrlName = '?SUP:Company_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:Company_Name;  SolaceCtrlName = '?sup:Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUP:Account_Number:Prompt;  SolaceCtrlName = '?SUP:Account_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:Account_Number;  SolaceCtrlName = '?sup:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUP:Postcode:Prompt;  SolaceCtrlName = '?SUP:Postcode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:Postcode;  SolaceCtrlName = '?sup:Postcode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Clear_Address;  SolaceCtrlName = '?Clear_Address';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUP:Address_Line1:Prompt;  SolaceCtrlName = '?SUP:Address_Line1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:Address_Line1;  SolaceCtrlName = '?sup:Address_Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:Address_Line2;  SolaceCtrlName = '?sup:Address_Line2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:Address_Line3;  SolaceCtrlName = '?sup:Address_Line3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUP:Telephone_Number:Prompt;  SolaceCtrlName = '?SUP:Telephone_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:Telephone_Number;  SolaceCtrlName = '?sup:Telephone_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUP:Fax_Number:Prompt;  SolaceCtrlName = '?SUP:Fax_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:Fax_Number;  SolaceCtrlName = '?sup:Fax_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:EmailAddress:Prompt;  SolaceCtrlName = '?sup:EmailAddress:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:EmailAddress;  SolaceCtrlName = '?sup:EmailAddress';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUP:Contact_Name:Prompt;  SolaceCtrlName = '?SUP:Contact_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:Contact_Name;  SolaceCtrlName = '?sup:Contact_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUP:Notes:Prompt;  SolaceCtrlName = '?SUP:Notes:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:Notes;  SolaceCtrlName = '?sup:Notes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group1;  SolaceCtrlName = '?Group1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:HistoryUsage:Prompt;  SolaceCtrlName = '?sup:HistoryUsage:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:HistoryUsage;  SolaceCtrlName = '?sup:HistoryUsage';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:Factor:Prompt;  SolaceCtrlName = '?sup:Factor:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:Factor;  SolaceCtrlName = '?sup:Factor';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUP:Normal_Supply_Period:Prompt;  SolaceCtrlName = '?SUP:Normal_Supply_Period:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:Normal_Supply_Period;  SolaceCtrlName = '?sup:Normal_Supply_Period';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt10;  SolaceCtrlName = '?Prompt10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:Minimum_Order_Value;  SolaceCtrlName = '?sup:Minimum_Order_Value';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUP:Minimum_Order_Value:Prompt;  SolaceCtrlName = '?SUP:Minimum_Order_Value:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt10:2;  SolaceCtrlName = '?Prompt10:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Percent;  SolaceCtrlName = '?Percent';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Replicate_Manufacturer;  SolaceCtrlName = '?Replicate_Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Supplier'
  OF ChangeRecord
    ActionMessage = 'Changing A Supplier'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateSUPPLIER')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateSUPPLIER')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?SUP:Company_Name:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(sup:Record,History::sup:Record)
  SELF.AddHistoryField(?sup:Company_Name,2)
  SELF.AddHistoryField(?sup:Account_Number,11)
  SELF.AddHistoryField(?sup:Postcode,3)
  SELF.AddHistoryField(?sup:Address_Line1,4)
  SELF.AddHistoryField(?sup:Address_Line2,5)
  SELF.AddHistoryField(?sup:Address_Line3,6)
  SELF.AddHistoryField(?sup:Telephone_Number,7)
  SELF.AddHistoryField(?sup:Fax_Number,8)
  SELF.AddHistoryField(?sup:EmailAddress,9)
  SELF.AddHistoryField(?sup:Contact_Name,10)
  SELF.AddHistoryField(?sup:Notes,17)
  SELF.AddHistoryField(?sup:HistoryUsage,15)
  SELF.AddHistoryField(?sup:Factor,16)
  SELF.AddHistoryField(?sup:Normal_Supply_Period,13)
  SELF.AddHistoryField(?sup:Minimum_Order_Value,14)
  SELF.AddUpdateFile(Access:SUPPLIER)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:MANUFACT.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:SUPPLIER
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:MANUFACT.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateSUPPLIER',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?sup:Postcode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sup:Postcode, Accepted)
      If ~quickwindow{prop:acceptall}
          Postcode_Routine (sup:postcode,sup:address_line1,sup:address_line2,sup:address_line3)
          Select(?sup:address_line1,1)
          Display()
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sup:Postcode, Accepted)
    OF ?Clear_Address
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Clear_Address, Accepted)
      Case MessageEx('Are you sure you want to clear the Address?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,500) 
          Of 1 ! &Yes Button
      
              SUP:Postcode=''
              SUP:Address_Line1=''
              SUP:Address_Line2=''
              SUP:Address_Line3=''
      
              Display()
              Select(?sup:postcode)
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Clear_Address, Accepted)
    OF ?sup:Telephone_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sup:Telephone_Number, Accepted)
      
          temp_string = Clip(left(sup:Telephone_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          sup:Telephone_Number    = temp_string
          Display(?sup:Telephone_Number)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sup:Telephone_Number, Accepted)
    OF ?sup:Fax_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sup:Fax_Number, Accepted)
      
          temp_string = Clip(left(sup:Fax_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          sup:Fax_Number    = temp_string
          Display(?sup:Fax_Number)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sup:Fax_Number, Accepted)
    OF ?Replicate_Manufacturer
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Replicate_Manufacturer, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      select_manufacturer
      if globalresponse = requestcompleted
          Case MessageEx('Are you sure you want to replicate the Manufacturer''s Details to this supplier?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,500) 
              Of 1 ! &Yes Button
                  sup:company_name    = man:manufacturer
                  access:manufact.clearkey(man:manufacturer_key)
                  man:manufacturer = sup:company_name
                  if access:manufact.fetch(man:manufacturer_key) = Level:Benign
                      sup:company_name     = man:manufacturer
                      SUP:Postcode        = man:Postcode
                      SUP:Address_Line1   = man:Address_Line1
                      SUP:Address_Line2   = man:Address_Line2
                      SUP:Address_Line3   = man:Address_Line3
                      sup:telephone_number = man:telephone_number
                      sup:fax_number       = man:fax_number
                      sup:contact_name     = man:contact_name1
                      sup:account_number   = man:account_number
                  end !if access:manufact.fetch(man:manufacturer_key) = Level:Benign
                  Display()
              Of 2 ! &No Button
          End!Case MessageEx
      end
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Replicate_Manufacturer, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateSUPPLIER')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

