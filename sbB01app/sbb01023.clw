

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01023.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Pending_Orders PROCEDURE                       !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::7:TAGFLAG          BYTE(0)
DASBRW::7:TAGMOUSE         BYTE(0)
DASBRW::7:TAGDISPSTATUS    BYTE(0)
DASBRW::7:QUEUE           QUEUE
Pointer                       LIKE(GLO:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
InStockQty           LONG
IsExchange           STRING(3)
TradeAccountName     STRING(30)
save_ord_id          USHORT,AUTO
sav:file_name        STRING(255)
FilesOpened          BYTE
tag_temp             STRING(1)
order_type_temp      STRING(8)
cost_temp            REAL
save_wpr_id          USHORT,AUTO
save_ope_id          USHORT,AUTO
save_par_id          USHORT,AUTO
type_temp            STRING(3)
no_temp              STRING('NO')
yes_temp             STRING('YES')
tmp:TotalCost        REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(ORDPEND)
                       PROJECT(ope:Description)
                       PROJECT(ope:Part_Number)
                       PROJECT(ope:Quantity)
                       PROJECT(ope:Ref_Number)
                       PROJECT(ope:Supplier)
                       PROJECT(ope:Job_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
ope:Description        LIKE(ope:Description)          !List box control field - type derived from field
ope:Part_Number        LIKE(ope:Part_Number)          !List box control field - type derived from field
ope:Quantity           LIKE(ope:Quantity)             !List box control field - type derived from field
order_type_temp        LIKE(order_type_temp)          !List box control field - type derived from local data
InStockQty             LIKE(InStockQty)               !List box control field - type derived from local data
IsExchange             LIKE(IsExchange)               !List box control field - type derived from local data
TradeAccountName       LIKE(TradeAccountName)         !List box control field - type derived from local data
ope:Ref_Number         LIKE(ope:Ref_Number)           !Primary key field - type derived from field
ope:Supplier           LIKE(ope:Supplier)             !Browse key field - type derived from field
ope:Job_Number         LIKE(ope:Job_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(SUPPTEMP)
                       PROJECT(suptmp:Company_Name)
                       PROJECT(suptmp:Minimum_Order_Value)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
suptmp:Company_Name    LIKE(suptmp:Company_Name)      !List box control field - type derived from field
suptmp:Minimum_Order_Value LIKE(suptmp:Minimum_Order_Value) !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse The Pending Orders File'),AT(,,627,232),FONT('Tahoma',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('Browse_Pending_Orders'),ALRT(F5Key),ALRT(F6Key),ALRT(F7Key),ALRT(F8Key),ALRT(F10Key),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,208,224),USE(?Sheet2),SPREAD
                         TAB('By Supplier'),USE(?Tab5)
                           ENTRY(@s30),AT(8,20,124,10),USE(suptmp:Company_Name),FONT('Tahoma',8,,FONT:regular),UPR
                           LIST,AT(8,36,200,164),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Company Name~@s30@56L(2)|M~Min Order Value~@n14.2@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(36,80,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(44,100,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON('&Tag  [F5]'),AT(8,208,56,16),USE(?DASTAG),LEFT,ICON('tag.gif')
                           BUTTON('Tag &All [F6]'),AT(64,208,56,16),USE(?DASTAGAll),LEFT,ICON('tagall.gif')
                           BUTTON('Untag &All [F7]'),AT(120,208,56,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                         END
                       END
                       LIST,AT(224,52,304,148),USE(?Browse:1),IMM,HSCROLL,MSG('Browsing Records'),FORMAT('77L(2)|M~Description~@s30@65L(2)|M~Part Number~@s30@37L(2)|M~Quantity~@p<<<<<<<<<<<<<<#' &|
   'p@36L(2)|M~Job No~@s8@36R(2)|M~Stock Qty~L@p<<<<<<<<<<<<<<#p@17L(2)|M~Exchange~@s3@120L' &|
   '(2)|M~Trade Account~@s30@'),FROM(Queue:Browse:1)
                       BUTTON('Allocate From Main Store'),AT(292,208,76,16),USE(?AllocateFromMainStoreButton),LEFT,ICON('order.gif')
                       SHEET,AT(216,4,324,224),USE(?CurrentTab),SPREAD
                         TAB('Parts Awaiting Order'),USE(?Tab:5)
                           SHEET,AT(220,24,312,180),USE(?Sheet3),SPREAD
                             TAB('By Description'),USE(?Tab6)
                               ENTRY(@s30),AT(224,40,124,10),USE(ope:Description),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             END
                             TAB('By Part Number'),USE(?Tab4)
                               ENTRY(@s30),AT(224,40,124,10),USE(ope:Part_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             END
                             TAB('By Job Number'),USE(?JobTab)
                               ENTRY(@s8),AT(224,40,124,10),USE(ope:Job_Number),LEFT,UPR
                             END
                           END
                           BUTTON('Amend Details'),AT(416,208,56,16),USE(?Amend_Details),LEFT,ICON('edit.gif')
                           BUTTON('Delete Part'),AT(476,208,56,16),USE(?Delete_Part),LEFT,ICON('delete.gif')
                           BUTTON('Valuate Order [F8]'),AT(220,208,68,16),USE(?Valuate_Order),LEFT,ICON('money_sm.gif')
                         END
                         TAB('Parts Awaiting Stock Delivery'),USE(?Tab3),HIDE
                         END
                       END
                       BUTTON('Make Tagged Orders [F10]'),AT(548,20,76,20),USE(?Create_Order),LEFT,ICON('order.gif')
                       BUTTON('Cancel'),AT(548,208,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort4:Locator  EntryLocatorClass                !Conditional Locator - Choice(?Sheet3) = 1
BRW1::Sort3:Locator  EntryLocatorClass                !Conditional Locator - Choice(?Sheet3) = 2
BRW1::Sort5:Locator  EntryLocatorClass                !Conditional Locator - Choice(?Sheet3) = 3
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
BRW6                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW6::Sort0:Locator  IncrementalLocatorClass          !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet2{prop:Color} = 15066597
    ?Tab5{prop:Color} = 15066597
    If ?suptmp:Company_Name{prop:ReadOnly} = True
        ?suptmp:Company_Name{prop:FontColor} = 65793
        ?suptmp:Company_Name{prop:Color} = 15066597
    Elsif ?suptmp:Company_Name{prop:Req} = True
        ?suptmp:Company_Name{prop:FontColor} = 65793
        ?suptmp:Company_Name{prop:Color} = 8454143
    Else ! If ?suptmp:Company_Name{prop:Req} = True
        ?suptmp:Company_Name{prop:FontColor} = 65793
        ?suptmp:Company_Name{prop:Color} = 16777215
    End ! If ?suptmp:Company_Name{prop:Req} = True
    ?suptmp:Company_Name{prop:Trn} = 0
    ?suptmp:Company_Name{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:5{prop:Color} = 15066597
    ?Sheet3{prop:Color} = 15066597
    ?Tab6{prop:Color} = 15066597
    If ?ope:Description{prop:ReadOnly} = True
        ?ope:Description{prop:FontColor} = 65793
        ?ope:Description{prop:Color} = 15066597
    Elsif ?ope:Description{prop:Req} = True
        ?ope:Description{prop:FontColor} = 65793
        ?ope:Description{prop:Color} = 8454143
    Else ! If ?ope:Description{prop:Req} = True
        ?ope:Description{prop:FontColor} = 65793
        ?ope:Description{prop:Color} = 16777215
    End ! If ?ope:Description{prop:Req} = True
    ?ope:Description{prop:Trn} = 0
    ?ope:Description{prop:FontStyle} = font:Bold
    ?Tab4{prop:Color} = 15066597
    If ?ope:Part_Number{prop:ReadOnly} = True
        ?ope:Part_Number{prop:FontColor} = 65793
        ?ope:Part_Number{prop:Color} = 15066597
    Elsif ?ope:Part_Number{prop:Req} = True
        ?ope:Part_Number{prop:FontColor} = 65793
        ?ope:Part_Number{prop:Color} = 8454143
    Else ! If ?ope:Part_Number{prop:Req} = True
        ?ope:Part_Number{prop:FontColor} = 65793
        ?ope:Part_Number{prop:Color} = 16777215
    End ! If ?ope:Part_Number{prop:Req} = True
    ?ope:Part_Number{prop:Trn} = 0
    ?ope:Part_Number{prop:FontStyle} = font:Bold
    ?JobTab{prop:Color} = 15066597
    If ?ope:Job_Number{prop:ReadOnly} = True
        ?ope:Job_Number{prop:FontColor} = 65793
        ?ope:Job_Number{prop:Color} = 15066597
    Elsif ?ope:Job_Number{prop:Req} = True
        ?ope:Job_Number{prop:FontColor} = 65793
        ?ope:Job_Number{prop:Color} = 8454143
    Else ! If ?ope:Job_Number{prop:Req} = True
        ?ope:Job_Number{prop:FontColor} = 65793
        ?ope:Job_Number{prop:Color} = 16777215
    End ! If ?ope:Job_Number{prop:Req} = True
    ?ope:Job_Number{prop:Trn} = 0
    ?ope:Job_Number{prop:FontStyle} = font:Bold
    ?Tab3{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::7:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW6.UpdateBuffer
   GLO:Queue.Pointer = suptmp:Company_Name
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = suptmp:Company_Name
     ADD(GLO:Queue,GLO:Queue.Pointer)
    tag_temp = '*'
  ELSE
    DELETE(GLO:Queue)
    tag_temp = ''
  END
    Queue:Browse.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse.tag_temp_Icon = 2
  ELSE
    Queue:Browse.tag_temp_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW6.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = suptmp:Company_Name
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW6.Reset
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::7:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::7:QUEUE = GLO:Queue
    ADD(DASBRW::7:QUEUE)
  END
  FREE(GLO:Queue)
  BRW6.Reset
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::7:QUEUE.Pointer = suptmp:Company_Name
     GET(DASBRW::7:QUEUE,DASBRW::7:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = suptmp:Company_Name
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASSHOWTAG Routine
   CASE DASBRW::7:TAGDISPSTATUS
   OF 0
      DASBRW::7:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::7:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::7:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW6.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Pending_Orders',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Pending_Orders',1)
    SolaceViewVars('InStockQty',InStockQty,'Browse_Pending_Orders',1)
    SolaceViewVars('IsExchange',IsExchange,'Browse_Pending_Orders',1)
    SolaceViewVars('TradeAccountName',TradeAccountName,'Browse_Pending_Orders',1)
    SolaceViewVars('save_ord_id',save_ord_id,'Browse_Pending_Orders',1)
    SolaceViewVars('sav:file_name',sav:file_name,'Browse_Pending_Orders',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Pending_Orders',1)
    SolaceViewVars('tag_temp',tag_temp,'Browse_Pending_Orders',1)
    SolaceViewVars('order_type_temp',order_type_temp,'Browse_Pending_Orders',1)
    SolaceViewVars('cost_temp',cost_temp,'Browse_Pending_Orders',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Browse_Pending_Orders',1)
    SolaceViewVars('save_ope_id',save_ope_id,'Browse_Pending_Orders',1)
    SolaceViewVars('save_par_id',save_par_id,'Browse_Pending_Orders',1)
    SolaceViewVars('type_temp',type_temp,'Browse_Pending_Orders',1)
    SolaceViewVars('no_temp',no_temp,'Browse_Pending_Orders',1)
    SolaceViewVars('yes_temp',yes_temp,'Browse_Pending_Orders',1)
    SolaceViewVars('tmp:TotalCost',tmp:TotalCost,'Browse_Pending_Orders',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?suptmp:Company_Name;  SolaceCtrlName = '?suptmp:Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AllocateFromMainStoreButton;  SolaceCtrlName = '?AllocateFromMainStoreButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:5;  SolaceCtrlName = '?Tab:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet3;  SolaceCtrlName = '?Sheet3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab6;  SolaceCtrlName = '?Tab6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ope:Description;  SolaceCtrlName = '?ope:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ope:Part_Number;  SolaceCtrlName = '?ope:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JobTab;  SolaceCtrlName = '?JobTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ope:Job_Number;  SolaceCtrlName = '?ope:Job_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Amend_Details;  SolaceCtrlName = '?Amend_Details';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete_Part;  SolaceCtrlName = '?Delete_Part';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Valuate_Order;  SolaceCtrlName = '?Valuate_Order';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Create_Order;  SolaceCtrlName = '?Create_Order';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Pending_Orders')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Pending_Orders')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?suptmp:Company_Name
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:IEQUIP.Open
  Relate:JOBS.Open
  Relate:LOCATION_ALIAS.Open
  Relate:PICKDET.Open
  Relate:PICKNOTE.Open
  Relate:RETSTOCK.Open
  Relate:STATUS.Open
  Relate:SUPPTEMP.Open
  Access:SUPPLIER.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:STOCK.UseFile
  Access:ORDERS.UseFile
  Access:ORDPARTS.UseFile
  Access:JOBSTAGE.UseFile
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ORDPEND,SELF)
  BRW6.Init(?List,Queue:Browse.ViewPosition,BRW6::View:Browse,Queue:Browse,Relate:SUPPTEMP,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,ope:DescriptionKey)
  BRW1.AddRange(ope:Supplier,suptmp:Company_Name)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?ope:Description,ope:Description,1,BRW1)
  BRW1.AddSortOrder(,ope:Supplier_Key)
  BRW1.AddRange(ope:Supplier,suptmp:Company_Name)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?ope:Part_Number,ope:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,ope:Supplier_Job_Key)
  BRW1.AddRange(ope:Supplier,suptmp:Company_Name)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?ope:Job_Number,ope:Job_Number,1,BRW1)
  BRW1.SetFilter('(ope:job_number <<> '''')')
  BRW1.AddSortOrder(,ope:Supplier_Key)
  BRW1.AddRange(ope:Supplier,suptmp:Company_Name)
  BIND('order_type_temp',order_type_temp)
  BIND('InStockQty',InStockQty)
  BIND('IsExchange',IsExchange)
  BIND('TradeAccountName',TradeAccountName)
  BRW1.AddField(ope:Description,BRW1.Q.ope:Description)
  BRW1.AddField(ope:Part_Number,BRW1.Q.ope:Part_Number)
  BRW1.AddField(ope:Quantity,BRW1.Q.ope:Quantity)
  BRW1.AddField(order_type_temp,BRW1.Q.order_type_temp)
  BRW1.AddField(InStockQty,BRW1.Q.InStockQty)
  BRW1.AddField(IsExchange,BRW1.Q.IsExchange)
  BRW1.AddField(TradeAccountName,BRW1.Q.TradeAccountName)
  BRW1.AddField(ope:Ref_Number,BRW1.Q.ope:Ref_Number)
  BRW1.AddField(ope:Supplier,BRW1.Q.ope:Supplier)
  BRW1.AddField(ope:Job_Number,BRW1.Q.ope:Job_Number)
  BRW6.Q &= Queue:Browse
  BRW6.AddSortOrder(,suptmp:Company_Name_Key)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(?SUPTMP:Company_Name,suptmp:Company_Name,1,BRW6)
  BIND('tag_temp',tag_temp)
  BIND('cost_temp',cost_temp)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW6.AddField(tag_temp,BRW6.Q.tag_temp)
  BRW6.AddField(suptmp:Company_Name,BRW6.Q.suptmp:Company_Name)
  BRW6.AddField(suptmp:Minimum_Order_Value,BRW6.Q.suptmp:Minimum_Order_Value)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab6{PROP:TEXT} = 'By Description'
    ?Tab4{PROP:TEXT} = 'By Part Number'
    ?JobTab{PROP:TEXT} = 'By Job Number'
    ?Browse:1{PROP:FORMAT} ='77L(2)|M~Description~@s30@#1#65L(2)|M~Part Number~@s30@#2#40L(2)|M~Quantity~@p<<<<<<<#p@#3#40L(2)|M~Job No~@s8@#4#40R(2)|M~Stock Qty~L@p<<<<<<<#p@#5#12L(2)|M~Exchange~@s3@#6#120L(2)|M~Trade Account~@s30@#7#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:DEFAULTS.Close
    Relate:IEQUIP.Close
    Relate:JOBS.Close
    Relate:LOCATION_ALIAS.Close
    Relate:PICKDET.Close
    Relate:PICKNOTE.Close
    Relate:RETSTOCK.Close
    Relate:STATUS.Close
    Relate:SUPPTEMP.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Pending_Orders',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?List
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?AllocateFromMainStoreButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AllocateFromMainStoreButton, Accepted)
      ! Start Change 4699 BE(20/09/2004)
      check_access('PENDING ORDERS - ALLOCATE PART',x")
      IF (x" = false) THEN
          MessageEx('You do not have access to this option.','ServiceBase 2000',|
                    'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,|
                    charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
      ELSIF (BRW1.Q.order_type_temp = 'Stock') THEN
          MessageEx('This is not a pending order for a job.','ServiceBase 2000',|
                    'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,|
                    charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
      ELSE
          access:stock.clearkey(sto:location_key)
          sto:location = MainStoreLocation()
          sto:part_number = ope:part_number
          IF (access:stock.fetch(sto:location_key) = Level:Benign) THEN
              IF (ope:Quantity > sto:Quantity_stock) THEN
                      MessageEx('There is insufficient stock of this part to fulfill the job.','ServiceBase 2000',|
                    'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,|
                    charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
              ELSE
                  CASE ope:part_type
                  OF 'JOB'
                      access:parts.clearkey(par:PendingRefNoKey)
                      par:ref_number  = ope:job_number
                      par:pending_ref_number  = ope:ref_number
                      IF (access:parts.fetch(par:PendingRefNoKey) = Level:Benign) THEN
                          sto:Quantity_Stock -= par:Quantity
                          IF (sto:Quantity_Stock < 0) THEN
                              sto:Quantity_Stock = 0
                          END
                          access:stock.update()
      
                          par:pending_ref_number  = ''
                          access:parts.update()
      
                          access:jobs.clearkey(job:ref_number_key)
                          job:ref_number  = par:ref_number
                          IF (access:jobs.fetch(job:ref_number_key) = Level:Benign) THEN
                              CASE CheckParts('C')
                                  OF 0        ! Spares Received
                                      !Start - Make sure to check the Warranty Parts if the job is split - TrkBs: 5273 (DBH: 18-01-2005)
                                      If job:Warranty_Job = 'YES'
                                          Case CheckParts('W')
                                              Of 0 !Spares Received
                                                  GetStatus(345,0,'JOB')
                                              Of 1 !Parts On Order
                                                  GetStatus(330,0,'JOB')
                                              Of 2 !Parts Pending
                                                  GetStatus(335,0,'JOB')
                                          End ! Case CheckParts('W')
                                      Else
                                          GetStatus(345,0,'JOB') !Spares Received
                                      End ! End ! If job:Warranty_Job = 'YES'
                                      !End   - Make sure to check the Warranty Parts if the job is split - TrkBs: 5273 (DBH: 18-01-2005)
                                  OF 1        ! Parts On Order
                                      GetStatus(330,0,'JOB')
                                  OF 2        ! Perts Pending
                                      GetStatus(335,0,'JOB')
                              END
                              access:jobs.update()
                          END
      
                          access:users.clearkey(use:password_key)
                          use:password            = glo:password
                          access:users.fetch(use:password_key)
      
                          IF (access:stohist.primerecord() = Level:Benign) THEN
                              shi:information         = ''
                              shi:ref_number          = sto:ref_number
                              shi:transaction_type    = 'DEC'
                              shi:despatch_note_number= ''
                              shi:quantity            = par:Quantity
                              shi:date                = TODAY()
                              shi:purchase_cost       = sto:purchase_cost
                              shi:sale_cost           = sto:sale_cost
                              shi:retail_cost         = sto:retail_cost
                              shi:job_number          = job:ref_number
                              shi:user                = use:user_code
                              shi:notes               = 'STOCK DECREMENTED'
                              IF (access:stohist.insert() <> Level:Benign) THEN
                                  access:stohist.cancelautoinc()
                              END
                          END
      
                          ! Start Change 4801 BE(12/10/2004)
                          !ReprintPartReceivedPickNote(job:ref_number, sto:ref_number)
                          ! Start Change 4805 BE913/10/2004)
                          !ReprintPartReceivedPickNote(job:ref_number, sto:ref_number, false)
                          ReprintPartReceivedPickNote(job:ref_number, ope:Part_Ref_Number, false)
                          ! End Change 4805 BE913/10/2004)
                          ! End Change 4801 BE(12/10/2004)
      
                      END
                      DELETE(ordpend)
                      BRW1.ResetSort(1)
      
                      save_ope_id = access:ordpend.savefile()
                      access:ordpend.clearkey(ope:supplier_name_key)
                      ope:supplier = supptemp:company_name
                      IF (access:ordpend.fetch(ope:supplier_name_key) <> Level:Benign) THEN
                          ! If no pending orders delete tempoarary
                          ! supplier record
                          DELETE(SUPPTEMP)
                          BRW6.ResetSort(1)
                      END
                      access:ordpend.restorefile(save_ope_id)
      
                  OF 'WAR'
                      access:parts.clearkey(wpr:PendingRefNoKey)
                      wpr:ref_number  = ope:job_number
                      wpr:pending_ref_number  = ope:ref_number
                      IF (access:warparts.fetch(wpr:PendingRefNoKey) = Level:Benign) THEN
      
                          sto:Quantity_Stock -= wpr:Quantity
                          IF (sto:Quantity_Stock < 0) THEN
                              sto:Quantity_Stock = 0
                          END
                          access:stock.update()
      
                          wpr:pending_ref_number  = ''
                          access:warparts.update()
      
                          access:jobs.clearkey(job:ref_number_key)
                          job:ref_number  = wpr:ref_number
                          IF (access:jobs.fetch(job:ref_number_key) = Level:Benign) THEN
                              CASE CheckParts('W')
                                  OF 0        ! Spares Received
                                      !Start - Make sure to check the Chargeable parts if the job is split - TrkBs: 5273 (DBH: 18-01-2005)
                                      If job:Chargeable_Job = 'YES'
                                          Case CheckParts('C')
                                              Of 0 !Spares Received
                                                  GetStatus(345,0,'JOB')
                                              Of 1 !Parts On Order
                                                  GetStatus(330,0,'JOB')
                                              Of 2 !Parts Pending
                                                  GetStatus(335,0,'JOB')
                                          End ! Case CheckParts('C')
                                      Else ! If job:Chargeable_Job = 'YES'
                                          GetStatus(345,0,'JOB')
                                      End ! If job:Chargeable_Job = 'YES'
                                      !End   - Make sure to check the Chargeable parts if the job is split - TrkBs: 5273 (DBH: 18-01-2005)
                                  OF 1        ! Parts On Order
                                      GetStatus(330,0,'JOB')
                                  OF 2        ! Perts Pending
                                      GetStatus(335,0,'JOB')
                              END
                              access:jobs.update()
                          END
      
                          access:users.clearkey(use:password_key)
                          use:password            = glo:password
                          access:users.fetch(use:password_key)
      
                          IF (access:stohist.primerecord() = Level:Benign) THEN
                              shi:information         = ''
                              shi:ref_number          = sto:ref_number
                              shi:transaction_type    = 'DEC'
                              shi:despatch_note_number= ''
                              shi:quantity            = wpr:Quantity
                              shi:date                = TODAY()
                              shi:purchase_cost       = sto:purchase_cost
                              shi:sale_cost           = sto:sale_cost
                              shi:retail_cost         = sto:retail_cost
                              shi:job_number          = job:ref_number
                              shi:user                = use:user_code
                              shi:notes               = 'STOCK DECREMENTED'
                              IF (access:stohist.insert() <> Level:Benign) THEN
                                  access:stohist.cancelautoinc()
                              END
                          END
      
                          ! Start Change 4801 BE(12/10/2004)
                          !ReprintPartReceivedPickNote(job:ref_number, sto:ref_number)
                          ! Start Change 4805 BE913/10/2004)
                          !ReprintPartReceivedPickNote(job:ref_number, sto:ref_number, false)
                          ReprintPartReceivedPickNote(job:ref_number, ope:Part_Ref_Number, false)
                          ! End Change 4805 BE913/10/2004)
                          ! End Change 4801 BE(12/10/2004)
                      END
                      DELETE(ordpend)
                      BRW1.ResetSort(1)
      
                      save_ope_id = access:ordpend.savefile()
                      access:ordpend.clearkey(ope:supplier_name_key)
                      ope:supplier = supptemp:company_name
                      IF (access:ordpend.fetch(ope:supplier_name_key) <> Level:Benign) THEN
                          ! If no pending orders delete tempoarary
                          ! supplier record
                          DELETE(SUPPTEMP)
                          BRW6.ResetSort(1)
                      END
                      access:ordpend.restorefile(save_ope_id)
                  END
              END
          END
      END
      ! End Change 4699 BE(20/09/2004)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AllocateFromMainStoreButton, Accepted)
    OF ?Amend_Details
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Amend_Details, Accepted)
      thiswindow.reset(1)
      glo:select1  = ope:ref_number
      Amend_Pending_Part
      glo:select1 = ''
      
      BRW1.ResetSort(1)
      BRW6.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Amend_Details, Accepted)
    OF ?Delete_Part
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Delete_Part, Accepted)
    Case MessageEx('Are you sure you want to remove this part from the Order Generation?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
        Of 1 ! &Yes Button
          Brw1.UpdateViewRecord()
          Case ope:part_type
              Of 'JOB'
                  setcursor(cursor:wait)
                  save_par_id = access:parts.savefile()
                  access:parts.clearkey(par:PendingRefNoKey)
                  par:ref_number         = ope:job_number
                  par:pending_ref_number = ope:ref_number
                  set(par:PendingRefNoKey,par:PendingRefNoKey)
                  loop
                      if access:parts.next()
                         break
                      end !if
                      if par:ref_number         <> ope:job_number      |
                      or par:pending_ref_number <> ope:ref_number      |
                          then break.  ! end if
                      If par:part_number  = ope:part_number
                          Delete(parts)
                      End!If par:part_number  = ope:part_number
                  end !loop
                  access:parts.restorefile(save_par_id)
                  setcursor()
              Of 'WAR'
                  setcursor(cursor:wait)
                  save_wpr_id = access:warparts.savefile()
                  access:warparts.clearkey(wpr:PendingRefNoKey)
                  wpr:ref_number         = ope:job_number
                  wpr:pending_ref_number = ope:ref_number
                  set(wpr:PendingRefNoKey,wpr:PendingRefNoKey)
                  loop
                      if access:warparts.next()
                         break
                      end !if
                      if wpr:ref_number         <> ope:job_number      |
                      or wpr:pending_ref_number <> ope:ref_number      |
                          then break.  ! end if
                      If wpr:part_number  = ope:part_number
                          Delete(warparts)
                      End!If wpr:part_number  = ope:part_number
                  end !loop
                  access:warparts.restorefile(save_wpr_id)
                  setcursor()
              Of 'STO'
              Of 'RET'
!                  setcursor(cursor:wait)
!                  save_wpr_id = access:warparts.savefile()
!                  access:warparts.clearkey(wpr:PendingRefNoKey)
!                  wpr:ref_number         = ope:job_number
!                  wpr:pending_ref_number = ope:ref_number
!                  set(wpr:PendingRefNoKey,wpr:PendingRefNoKey)
!                  loop
!                      if access:warparts.next()
!                         break
!                      end !if
!                      if wpr:ref_number         <> ope:job_number      |
!                      or wpr:pending_ref_number <> ope:ref_number      |
!                          then break.  ! end if
!                      If wpr:part_number  = ope:part_number
!                          Delete(warparts)
!                      End!If wpr:part_number  = ope:part_number
!                  end !loop
!                  access:warparts.restorefile(save_wpr_id)
!                  setcursor()

          End!Case ope:ref_number
          Delete(ordpend)
          If error()
              stop(error())
          End!If error()
        Of 2 ! &No Button
    End!Cas
      BRW1.ResetSort(1)
      BRW6.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Delete_Part, Accepted)
    OF ?Valuate_Order
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Valuate_Order, Accepted)
      cost_Temp = 0
      setcursor(cursor:Wait)
      save_Ope_Id = Access:ORDPEND.savefile()
      access:Ordpend.Clearkey(ope:Supplier_Key)
      ope:Supplier    = suptmp:Company_Name
      set(ope:Supplier_Key,ope:Supplier_Key)
      loop
          If access:Ordpend.next()
             break
          end !if
          if ope:Supplier    <> suptmp:Company_Name      |
              then break.  ! end if
          Case ope:Part_Type
              Of 'JOB'
                  save_Par_Id = access:Parts.savefile()
                  access:Parts.clearkey(par:PendingRefNoKey)
                  par:Ref_Number         = ope:Job_Number
                  par:Pending_Ref_Number = ope:Ref_Number
                  set(par:PendingRefNoKey,par:PendingRefNoKey)
                  loop
                      if access:Parts.next()
                         break
                      end !if
                      if par:Ref_Number         <> ope:Job_Number      |
                      or par:Pending_Ref_Number <> ope:Ref_Number      |
                          then break.  ! end if
                      If par:Part_Number  = ope:Part_Number
                          cost_Temp   += (par:Purchase_Cost * ope:Quantity)
                          Break
                      End!If par:Part_Number  = ope:Part_Number
                  end !loop
                  access:Parts.restorefile(save_Par_Id)
              Of 'WAR'
                  save_Wpr_Id = access:Warparts.savefile()
                  access:Warparts.clearkey(wpr:PendingRefNoKey)
                  wpr:Ref_Number         = ope:Job_Number
                  wpr:Pending_Ref_Number = ope:Ref_Number
                  set(wpr:PendingRefNoKey,wpr:PendingRefNoKey)
                  loop
                      if access:Warparts.next()
                         break
                      end !if
                      if wpr:Ref_Number         <> ope:Job_Number      |
                      or wpr:Pending_Ref_Number <> ope:Ref_Number      |
                          then break.  ! end if
                      If wpr:Part_Number = ope:Part_Number
                          cost_Temp   += wpr:Purchase_Cost * ope:Quantity
                          Break
                      End!If wpr:Part_Number = ope:Part_Number
                  end !loop
                  access:Warparts.restorefile(save_Wpr_Id)
              Of 'STO'
                  access:Stock.clearkey(sto:Ref_Number_Key)
                  sto:Ref_Number = ope:Part_Ref_Number
                  if access:Stock.Fetch(sto:Ref_Number_Key) = Level:Benign
                      cost_Temp   += sto:Purchase_Cost * ope:Quantity
                  End!if access:Stock.fetch(sto:Ref_Number_Key) = Level:Benign
          End!Case ope:Part_Type
      end !loop
      access:Ordpend.restorefile(save_Ope_Id)
      setcursor()
      Case MessageEx('The value of this order is: '&Clip(Format(cost_Temp,@n14.2)),'ServiceBase 2000',|
                     'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:Systemexclamation,msgex:Samewidths,84,26,0) 
          Of 1 ! &OK Button
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Valuate_Order, Accepted)
    OF ?Create_Order
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Create_Order, Accepted)
          Set(DEFAULTS)
          Access:DEFAULTS.Next()
          If ~Records(glo:Queue)
              Case MessageEx('You have not tagged any Suppliers.','ServiceBase 2000','styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,0+2+0+0,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
      
          Else!If ~Records(glo:Queue)
              Case MessageEx('Are you sure you want to Generate Ordes for all the Tagged Suppliers?','ServiceBase 2000','styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemquestion,0+2+0+0,84,26,0) 
                  Of 1 ! &Yes Button
                      Clear(glo:Q_PartsOrder)
                      Free(glo:Q_PartsOrder)
                      recordspercycle     = 25
                      recordsprocessed    = 0
                      percentprogress     = 0
                      setcursor(cursor:wait)
                      open(progresswindow)
                      progress:thermometer    = 0
                      ?progress:pcttext{prop:text} = '0% Completed'
          
                      recordstoprocess    = Records(glo:Queue)
                      Loop x# = 1 To Records(glo:Queue)
                          Get(glo:Queue,x#)
                          Do GetNextRecord2
                          get(orders,0)
                          if access:orders.primerecord() = level:benign
                              ord:supplier     = glo:pointer
                              ord:date         = Today()
                              ord:printed      = 'NO'
                              ord:all_received = 'NO'
                              access:users.clearkey(use:password_key)
                              use:password    =glo:password
                              access:users.fetch(use:password_key)
                              ord:user        = use:user_code
                              access:orders.insert()
                          End!if access:orders.primerecord() = level:benign
      
                          save_ope_id = access:ordpend.savefile()
                          access:ordpend.clearkey(ope:supplier_name_key)
                          ope:supplier = glo:pointer
                          set(ope:supplier_name_key,ope:supplier_name_key)
                          loop
                              if access:ordpend.next()
                                 break
                              end !if
                              if ope:supplier <> glo:pointer      |
                                  then break.  ! end if
                              yldcnt# += 1
                              if yldcnt# > 25
                                 yield() ; yldcnt# = 0
                             end !if
                              get(ordparts,0)
                              if access:ordparts.primerecord() = level:benign
                                  orp:order_number    = ord:order_number
                                  Case def:SummaryOrders
                                      Of 0! Old Way
      
                                          Case ope:part_type
                                              Of 'JOB'
                                                  orp:job_number  = ope:job_number
                                                  access:parts.clearkey(par:PendingRefNoKey)
                                                  par:ref_number  = ope:job_number
                                                  par:pending_ref_number  = ope:ref_number
                                                  If access:parts.fetch(par:PendingRefNoKey)
                                                      Delete(ordpend)
                                                      Cycle
                                                  Else!If access:parts.fetch(par:PendingRefNoKey)
                                                      par:order_number    = ord:order_number
                                                      par:pending_ref_number  = ''
                                                      par:date_ordered    = Today()
                                                      par:order_part_number   = orp:record_number
                                                      access:parts.update()
                  
                                                      orp:quantity    = ope:quantity
                                                      orp:part_number = par:part_number
                                                      orp:description = par:description
                                                      orp:purchase_cost   = par:purchase_cost
                                                      orp:sale_cost   = par:sale_cost
                                                      orp:part_Type   = 'JOB'
                                                      orp:part_ref_number = par:part_ref_number
                  
                                                      access:jobs.clearkey(job:ref_number_key)
                                                      job:ref_number  = par:ref_number
                                                      If access:jobs.fetch(job:ref_number_key) = Level:Benign
                                                          !call the status routine
                                                          GetStatus(335,1,'JOB') !spares ordered
      
                                                          access:jobs.update()
                                                      End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
                                                  End!If access:parts.fetch(par:PendingRefNoKey)
                                                  
                                              Of 'WAR'
                                                  orp:job_number  = ope:job_number
                                                  access:warparts.clearkey(wpr:PendingRefNoKey)
                                                  wpr:ref_number  = ope:job_number
                                                  wpr:pending_ref_number  = ope:ref_number
                                                  If access:warparts.fetch(wpr:PendingRefNoKey)
                                                      Delete(ordpend)
                                                      Cycle
                                                  Else!If access:warparts.fetch(wpr:PendingRefNoKey)
                                                      wpr:order_number    = ord:order_number
                                                      wpr:pending_ref_number  = ''
                                                      wpr:date_ordered    = Today()
                                                      wpr:order_part_number   = orp:record_number
                                                      access:warparts.update()
                  
                                                      orp:quantity    = ope:quantity
                                                      orp:part_number = wpr:part_number
                                                      orp:description = wpr:description
                                                      orp:purchase_cost   = wpr:purchase_cost
                                                      orp:sale_cost       = wpr:sale_cost
                                                      orp:part_type       = 'WAR'
                                                      orp:part_ref_number = wpr:part_ref_number
                  
                                                      access:jobs.clearkey(job:ref_number_key)
                                                      job:ref_number  = wpr:ref_number
                                                      If access:jobs.fetch(job:ref_number_key) = Level:Benign
                                                          !call the status routine
                                                          GetStatus(335,0,'JOB') !spares ordered
      
                                                          access:jobs.update()
                                                      End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
                                                  End!If access:warparts.fetch(wpr:PendingRefNoKey)
                                              Of 'STO'
                                                  !The new way of ordering retail parts is always a summary order
                                                  !It will therefore not have a Part_Ref_Number filled in.
                                                  !So need to try and get the costs from the Main Store
                                                  orp:Reason          = ope:Reason
                                                  orp:part_ref_number = ope:part_ref_number
                                                  orp:Quantity        = ope:Quantity
                                                  orp:Part_Number     = ope:Part_Number
                                                  orp:Description     = ope:Description
                                                  orp:Part_Type       = 'STO'
      
                                                  Access:STOCK.ClearKey(sto:Ref_Number_Key)
                                                  sto:Ref_Number  = ope:Part_Ref_Number
                                                  If Access:STOCK.Fetch(sto:Ref_Number_Key)
                                                      Access:STOCK.ClearKey(sto:Location_Key)
                                                      sto:Location    = MainStoreLocation()
                                                      sto:Part_Number = ope:Part_Number
                                                      If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                                                          orp:Purchase_Cost      = sto:Purchase_Cost
                                                          orp:Sale_Cost          = sto:Sale_Cost
                                                          orp:Retail_Cost        = sto:Retail_Cost
                                                      End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                                                  Else!If access:stock.fetch(sto:ref_number_key)
                                                      orp:quantity    = ope:quantity
                                                      orp:part_number = sto:part_number
                                                      orp:description = sto:description
                                                      orp:purchase_cost   = sto:purchase_cost
                                                      orp:sale_cost   = sto:sale_cost
                                                      orp:part_type   = 'STO'
                                                  End!If access:stock.fetch(sto:ref_number_key)
                  
                                              Of 'RET'
                                                  orp:job_number  = ope:job_number
                                                  ! Start Change BE022 BE(12/12/03)
                                                  !access:retstock.clearkey(res:part_number_key)
                                                  access:retstock.clearkey(res:pending_ref_number_key)
                                                  ! End Change BE022 BE(12/12/03)
                                                  res:ref_number  = ope:job_number
                                                  res:pending_ref_number = ope:ref_number
                                                  if access:retstock.tryfetch(res:pending_ref_number_key)
                                                      Delete(ordpend)
                                                      Cycle
                                                  Else!if access:retstock.tryfetch(res:pending_ref_number_key)
                                                      res:order_number    = ord:order_number
                                                      res:pending_ref_number  = ''
                                                      res:date_ordered    = Today()
                                                      res:order_part_number  = orp:record_number
                                                      ! Start Change BE022 BE(12/12/03)
                                                      !res:despatched    = 'ORD'
                                                      ! End Change BE022 BE(12/12/03)
                                                      access:retstock.update()
                  
                                                      orp:quantity    = res:quantity
                                                      orp:part_number = res:part_number
                                                      orp:description = res:description
                                                      orp:purchase_cost   = res:purchase_cost
                                                      orp:sale_cost   = res:sale_cost
                                                      orp:retail_cost = res:retail_cost
                                                      orp:part_type   = 'RET'
                                                      orp:part_ref_number = res:part_ref_number
                                                      orp:account_number  = ope:account_number
                                                      orp:allocated_to_sale   = 'NO'
                                                  End!if access:retstock.tryfetch(res:pending_ref_number_key)
      
                                              OF 'INS'
                                                  orp:Job_Number  = ope:Job_Number
                                                  Access:IEQUIP.ClearKey(iequ:RefPendRefKey)
                                                  iequ:RefNumber        = ope:Job_Number
                                                  iequ:PendingRefNumber = ope:Ref_Number
                                                  If Access:IEQUIP.TryFetch(iequ:RefPendRefKey) = Level:Benign
                                                      iequ:OrderNumber        = ord:Order_Number
                                                      iequ:PendingRefNumber   = ''
                                                      iequ:DateOrdered        = Today()
                                                      iequ:OrderPartNUmber    = orp:Record_Number
                                                      Access:IEQUIP.Update()
      
                                                      orp:Quantity            = iequ:Quantity
                                                      orp:Part_Number         = iequ:PartNumber
                                                      orp:Description         = iequ:Description
                                                      orp:Purchase_Cost       = iequ:PurchaseCost
                                                      orp:Sale_Cost           = iequ:SaleCost
                                                      orp:Part_Type           = 'INS'
                                                      orp:Part_Ref_Number       = iequ:PartRefNumber
                                                      orp:Account_Number      = ope:Account_Number
                                                      orp:Allocated_To_Sale   = 'NO'
                                                  Else!If Access:IEQUIP.TryFetch(iequ:RefPendRefKey) = Level:Benign
                                                      Delete(ORDPEND)
                                                      Cycle
                                                  End!If Access:IEQUIP.TryFetch(iequ:RefPendRefKey) = Level:Benign
                                          End!Case ope:part_type
                                      Of 1! New Way
                                          If ope:Part_Type = 'RET'
                                              orp:job_number  = ope:job_number
                                              ! Start Change BE022 BE(12/12/03)
                                              !access:retstock.clearkey(res:part_number_key)
                                              access:retstock.clearkey(res:pending_ref_number_key)
                                              ! End Change BE022 BE(12/12/03)
                                              res:ref_number  = ope:job_number
                                              res:pending_ref_number = ope:ref_number
                                              if access:retstock.tryfetch(res:pending_ref_number_key)
                                                  Delete(ordpend)
                                                  Cycle
                                              Else!if access:retstock.tryfetch(res:pending_ref_number_key)
                                                  res:order_number    = ord:order_number
                                                  res:pending_ref_number  = ''
                                                  res:date_ordered    = Today()
                                                  res:order_part_number  = orp:record_number
                                                  ! Start Change BE022 BE(12/12/03)
                                                  !res:despatched    = 'ORD'
                                                  ! End Change BE022 BE(12/12/03)
                                                  access:retstock.update()
              
                                                  orp:quantity    = ope:quantity
                                                  orp:part_number = res:part_number
                                                  orp:description = res:description
                                                  orp:purchase_cost   = res:purchase_cost
                                                  orp:sale_cost   = res:sale_cost
                                                  orp:retail_cost = res:retail_cost
                                                  orp:part_type   = 'RET'
                                                  orp:part_ref_number = res:part_ref_number
                                                  orp:account_number  = ope:account_number
                                                  orp:allocated_to_sale   = 'NO'
                                              End!if access:retstock.tryfetch(res:pending_ref_number_key)
                                          Else !If ope:Part_Type = 'RET'
                                              orp:Part_Ref_Number    = ''
                                              orp:Quantity           = ope:Quantity
                                              orp:Part_Number        = ope:Part_Number
                                              orp:Description        = ope:Description
                                              orp:part_type   = 'STO'
      
                                              !Dunno the costs, so try and find it in Main Store
                                              Access:LOCATION.ClearKey(loc:Main_Store_Key)
                                              loc:Main_Store = 'YES'
                                              Set(loc:Main_Store_Key,loc:Main_Store_Key)
                                              If Access:LOCATION.Next() = Level:Benign
                                                  If loc:Main_Store = 'YES'
                                                      Access:STOCK.ClearKey(sto:Location_Key)
                                                      sto:Location    = loc:Location
                                                      sto:Part_Number = ope:Part_Number
                                                      If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                                                          orp:Purchase_Cost      = sto:Purchase_Cost
                                                          orp:Sale_Cost          = sto:Sale_Cost
                                                          orp:Retail_Cost        = sto:Retail_Cost
                                                      End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                                                  End!If loc:Main_Store = 'YES'
                                              End!If Access:LOCATION.Next() = Level:Benign
                                          End !If ope:Part_Type = 'RET'
      
                                  End!Case def:SummaryOrders
      
                                  orp:all_received    = 'NO'
                                  access:ordparts.insert()
                              End!if access:ordparts.primerecord() = level:benign
              !Now put the order into a memory table, so the report will be a summary
                          Sort(glo:Q_PartsOrder,glo:q_order_number,glo:q_part_number,glo:q_description,glo:q_purchase_cost)
                          glo:q_order_number  = orp:order_number
                          glo:q_part_number   = orp:part_number
                          glo:q_description   = orp:description
                          glo:q_supplier      = ope:supplier
                          glo:q_purchase_cost = orp:purchase_cost
                          glo:q_sale_cost     = orp:sale_cost
          
                          Get(glo:Q_PartsOrder,glo:q_order_number,glo:q_part_number,glo:q_description,glo:q_purchase_cost)
                          If error()
                              glo:q_order_number  = orp:order_number
                              glo:q_part_number   = orp:part_number
                              glo:q_description   = orp:description
                              glo:q_supplier      = ope:supplier
                              glo:q_purchase_cost = orp:purchase_cost
                              glo:q_sale_cost     = orp:sale_cost
                              glo:q_quantity      = orp:quantity
                              Add(glo:Q_PartsOrder)
                          Else!If error()
                              glo:q_quantity      += orp:quantity
                              Put(glo:Q_PartsOrder)
                          End!If error()
          
                          Delete(ordpend)
          
                          end !loop
                          access:ordpend.restorefile(save_ope_id)
                      End!Loop x# = 1 To Records(glo:Queue)
                      setcursor()
                      close(progresswindow)
      
                      Set(defaults)
                      access:defaults.next()
                      If INSTRING('COMMUNICAID',def:User_Name,1,1)
                          Case MessageEx('The current Euro exchange rate is set to : '&Clip(FORMAT(GETINI('EDI','PartsOrderEuroRate',,CLIP(PATH())&'\SB2KDEF.INI'),@n23.11))&|
                            '<13,10>','ServiceBase 2000',|
                                         'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          sav:file_name   = glo:file_name
                          UNTCount# = 0
                          UNTCount# += 1
                          setcursor(cursor:wait)
                          save_ord_id = access:orders.savefile()
                          access:orders.clearkey(ord:supplier_printed_key)
                          ord:printed      = 'NO'
                          set(ord:supplier_printed_key,ord:supplier_printed_key)
                          loop
                              if access:orders.next()
                                 break
                              end !if
                              if ord:printed      <> 'NO'      |
                                  then break.  ! end if
                              first# = 1        
                              line# = 1
                              Sort(glo:q_partsorder,glo:q_supplier,glo:q_order_number,glo:q_part_number)
                              Loop x# = 1 To Records(glo:Q_PartsOrder)    
                                  Get(glo:Q_PartsOrder,x#)
                                  If glo:q_supplier <> ord:supplier Then Cycle.
                                  If first# = 1
                                      glo:file_name = Clip(def:exportpath) & '\OD' & Format(ord:order_number,@n06) & '.TXT'
      
                                      access:expgen.open()
                                      access:expgen.usefile()
                                      UNTCount# += 8
      
                                      EdiHeader()
      
                                  first# = 0        
                                  End!If first# = 1
                              UNTCount# += 5
                              EDIBody(line#)
      
      
                              line# += 1
      
                              End!Loop x# = 1 To Records(glo:Q_PartsOrder)
                              If line# > 1
                                  UNTCount# += 2
      
                                  EDIFooter(UNTCount#,Line#)
      
                                  access:expgen.close()
                              End!If line# > 1
                          end !loop
                          access:orders.restorefile(save_ord_id)
                          setcursor()
      
                          glo:file_name   = sav:file_name
                          Case MessageEx('Order(s) and EDI File(s) have been created.<13,10><13,10>The order will now be printed.<13,10><13,10>(This message will close automatically)','ServiceBase 2000',|
                                         'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,500) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                      Else!If INSTRING('COMMUNICAID',def:User_Name,1,1)
                          Case MessageEx('Orders have been created. They will now be printed.<13,10><13,10>(This message will close automatically)','ServiceBase 2000','Styles\idea.ico','|&Close',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,500)
                              Of 1 ! &Close Button
                          End!Case MessageEx
      
                      End!If INSTRING('COMMUNICAID',def:User_Name,1,1)
      
                      glo:select1  = ''
                      Parts_Order
                      Post(Event:CloseWindow)
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ~Records(Qu
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Create_Order, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Pending_Orders')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?List
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, NewSelection)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, NewSelection)
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::7:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?Sheet3
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?Sheet3)
        OF 1
          ?Browse:1{PROP:FORMAT} ='77L(2)|M~Description~@s30@#1#65L(2)|M~Part Number~@s30@#2#40L(2)|M~Quantity~@p<<<<<<<#p@#3#40L(2)|M~Job No~@s8@#4#40R(2)|M~Stock Qty~L@p<<<<<<<#p@#5#12L(2)|M~Exchange~@s3@#6#120L(2)|M~Trade Account~@s30@#7#'
          ?Tab6{PROP:TEXT} = 'By Description'
        OF 2
          ?Browse:1{PROP:FORMAT} ='65L(2)|M~Part Number~@s30@#2#77L(2)|M~Description~@s30@#1#40L(2)|M~Quantity~@p<<<<<<<#p@#3#40L(2)|M~Job No~@s8@#4#40R(2)|M~Stock Qty~L@p<<<<<<<#p@#5#12L(2)|M~Exchange~@s3@#6#120L(2)|M~Trade Account~@s30@#7#'
          ?Tab4{PROP:TEXT} = 'By Part Number'
        OF 3
          ?Browse:1{PROP:FORMAT} ='36L(2)|M~Job No~@s8@#4#65L(2)|M~Part Number~@s30@#2#77L(2)|M~Description~@s30@#1#37L(2)|M~Quantity~@p<<<<<<<#p@#3#36R(2)|M~Stock Qty~L@p<<<<<<<#p@#5#17L(2)|M~Exchange~@s3@#6#120L(2)|M~Trade Account~@s30@#7#'
          ?JobTab{PROP:TEXT} = 'By Job Number'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F5Key
              Post(Event:Accepted,?Dastag)
          Of F6Key
              Post(Event:Accepted,?DasTagall)
          Of F7Key
              Post(Event:Accepted,?DasUntagAll)
          Of F8Key
              Post(Event:Accepted,?Valuate_Order)
          Of F10Key
              Post(Event:Accepted,?Create_Order)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::7:DASUNTAGALL
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet3) = 1
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?Sheet3) = 2
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?Sheet3) = 3
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())
  ! Start Change 4699 BE(20/09/2004)
  access:stock.clearkey(sto:location_key)
  sto:location = MainStoreLocation()
  sto:part_number = ope:part_number
  IF (access:stock.fetch(sto:location_key) = Level:Benign) THEN
    InStockQty = sto:Quantity_Stock
  ELSE
    InStockQty = 0
  END
  IF (ope:Job_Number = '') THEN
    IsExchange = 'N/A'
  ELSE
      IsExchange = ''
      TradeAccountName = ''
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number  = ope:Job_Number
      IF (access:jobs.fetch(job:ref_number_key) = Level:Benign) THEN
        IF (job:Exchange_Unit_Number = '') THEN
            IsExchange = 'N'
        ELSE
            IsExchange = 'Y'
        END
        ! Start Change xxxx BE(04/10/2004)
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        IF (access:subtracc.fetch(sub:account_number_key) = Level:Benign) THEN
          TradeAccountName = sub:company_name
        END
        ! End Change xxxx BE(04/10/2004)
      END
  END
  ! End Change 4699 BE(20/09/2004)
  IF (ope:Job_Number <> '')
    order_type_temp = ope:Job_Number
  ELSE
    order_type_temp = 'Stock'
  END
  CASE (ope:Part_Type)
  OF 'JOB'
    type_temp = 'CHA'
  ELSE
    type_temp = ope:Part_Type
  END
  PARENT.SetQueueRecord
  SELF.Q.order_type_temp = order_type_temp            !Assign formula result to display queue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = suptmp:Company_Name
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW6::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW6::RecordStatus=ReturnValue
  IF BRW6::RecordStatus NOT=Record:OK THEN RETURN BRW6::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = suptmp:Company_Name
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::7:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW6::RecordStatus
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

