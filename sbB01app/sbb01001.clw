

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01001.INC'),ONCE        !Local module procedure declarations
                     END


Transfer_Stock PROCEDURE                              !Generated from procedure template - Window

LocalRequest         LONG
FilesOpened          BYTE
current_site_location_temp STRING(30)
transfer_site_location_temp STRING(30)
quantity_temp        LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ReqNo                STRING(20)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?transfer_site_location_temp
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB1::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
window               WINDOW('Stock Transfer'),AT(,,220,116),FONT('Tahoma',8,,),CENTER,TILED,GRAY,DOUBLE
                       SHEET,AT(4,4,212,80),USE(?Sheet1),SPREAD
                         TAB('Stock Transfer'),USE(?Tab1)
                           PROMPT('Current Site Location'),AT(9,20),USE(?Prompt1)
                           ENTRY(@s30),AT(89,20,124,10),USE(GLO:Select1),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Transfer Stock To'),AT(9,36),USE(?Prompt2)
                           COMBO(@s30),AT(89,36,124,10),USE(transfer_site_location_temp),VSCROLL,FONT('Tahoma',8,,FONT:bold),UPR,FORMAT('120L(2)*@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Quantity To Transfer'),AT(9,52),USE(?Prompt3)
                           SPIN(@p<<<<<#p),AT(89,52,64,10),USE(quantity_temp),RIGHT,FONT('Tahoma',8,,FONT:bold),RANGE(1,999999)
                           PROMPT('Requisition Number'),AT(8,68),USE(?ReqNo:Prompt),HIDE
                           ENTRY(@s20),AT(88,68,124,10),USE(ReqNo),HIDE,FONT(,,,FONT:bold),UPR
                         END
                       END
                       PANEL,AT(4,88,212,24),USE(?Panel2),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,92,56,16),USE(?OkButton),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(156,92,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB1                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_sto_id   ushort,auto
save_stm_ali_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?GLO:Select1{prop:ReadOnly} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 15066597
    Elsif ?GLO:Select1{prop:Req} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 8454143
    Else ! If ?GLO:Select1{prop:Req} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 16777215
    End ! If ?GLO:Select1{prop:Req} = True
    ?GLO:Select1{prop:Trn} = 0
    ?GLO:Select1{prop:FontStyle} = font:Bold
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?transfer_site_location_temp{prop:ReadOnly} = True
        ?transfer_site_location_temp{prop:FontColor} = 65793
        ?transfer_site_location_temp{prop:Color} = 15066597
    Elsif ?transfer_site_location_temp{prop:Req} = True
        ?transfer_site_location_temp{prop:FontColor} = 65793
        ?transfer_site_location_temp{prop:Color} = 8454143
    Else ! If ?transfer_site_location_temp{prop:Req} = True
        ?transfer_site_location_temp{prop:FontColor} = 65793
        ?transfer_site_location_temp{prop:Color} = 16777215
    End ! If ?transfer_site_location_temp{prop:Req} = True
    ?transfer_site_location_temp{prop:Trn} = 0
    ?transfer_site_location_temp{prop:FontStyle} = font:Bold
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    If ?quantity_temp{prop:ReadOnly} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 15066597
    Elsif ?quantity_temp{prop:Req} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 8454143
    Else ! If ?quantity_temp{prop:Req} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 16777215
    End ! If ?quantity_temp{prop:Req} = True
    ?quantity_temp{prop:Trn} = 0
    ?quantity_temp{prop:FontStyle} = font:Bold
    ?ReqNo:Prompt{prop:FontColor} = -1
    ?ReqNo:Prompt{prop:Color} = 15066597
    If ?ReqNo{prop:ReadOnly} = True
        ?ReqNo{prop:FontColor} = 65793
        ?ReqNo{prop:Color} = 15066597
    Elsif ?ReqNo{prop:Req} = True
        ?ReqNo{prop:FontColor} = 65793
        ?ReqNo{prop:Color} = 8454143
    Else ! If ?ReqNo{prop:Req} = True
        ?ReqNo{prop:FontColor} = 65793
        ?ReqNo{prop:Color} = 16777215
    End ! If ?ReqNo{prop:Req} = True
    ?ReqNo{prop:Trn} = 0
    ?ReqNo{prop:FontStyle} = font:Bold
    ?Panel2{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Transfer_Stock',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('LocalRequest',LocalRequest,'Transfer_Stock',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Transfer_Stock',1)
    SolaceViewVars('current_site_location_temp',current_site_location_temp,'Transfer_Stock',1)
    SolaceViewVars('transfer_site_location_temp',transfer_site_location_temp,'Transfer_Stock',1)
    SolaceViewVars('quantity_temp',quantity_temp,'Transfer_Stock',1)
    SolaceViewVars('ReqNo',ReqNo,'Transfer_Stock',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select1;  SolaceCtrlName = '?GLO:Select1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?transfer_site_location_temp;  SolaceCtrlName = '?transfer_site_location_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?quantity_temp;  SolaceCtrlName = '?quantity_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReqNo:Prompt;  SolaceCtrlName = '?ReqNo:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReqNo;  SolaceCtrlName = '?ReqNo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel2;  SolaceCtrlName = '?Panel2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Transfer_Stock')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Transfer_Stock')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULT2.Open
  Relate:LOCATION.Open
  Relate:STOCK_ALIAS.Open
  Relate:STOMODEL_ALIAS.Open
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:STOMODEL.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  SET(Default2)
  Access:Default2.Next()
  !Get location!
  Access:Location.ClearKey(loc:Location_Key)
  loc:Location = GLO:Select1
  IF Access:Location.Fetch(loc:Location_Key)
    !ERROR
  ELSE
    IF loc:Main_Store = 'YES'
      IF de2:UseRequisitionNumber = 1
        UNHIDE(?ReqNo)
        UNHIDE(?ReqNo:Prompt)
        ?ReqNo{PROP:REQ} = TRUE
      ELSE
        ?ReqNo{PROP:REQ} = FALSE
      END
      DISPLAY()
      UPDATE()
    END
  END
  Do RecolourWindow
  ! support for CPCS
  FDCB1.Init(transfer_site_location_temp,?transfer_site_location_temp,Queue:FileDropCombo.ViewPosition,FDCB1::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB1.Q &= Queue:FileDropCombo
  FDCB1.AddSortOrder(loc:Location_Key)
  FDCB1.AddField(loc:Location,FDCB1.Q.loc:Location)
  FDCB1.AddField(loc:RecordNumber,FDCB1.Q.loc:RecordNumber)
  FDCB1.AddUpdateField(loc:Location,transfer_site_location_temp)
  ThisWindow.AddItem(FDCB1.WindowComponent)
  FDCB1.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:LOCATION.Close
    Relate:STOCK_ALIAS.Close
    Relate:STOMODEL_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Transfer_Stock',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      !Transfer
      finish# = 1
      If transfer_site_location_temp = ''
          Select(?transfer_site_location_temp)
          finish# = 0
      End
      If ?ReqNo{PROP:REQ} = TRUE AND ReqNo = ''
          Select(?ReqNo)
          finish# = 0
      End
      If finish# = 1
          If quantity_temp = ''
              Select(?quantity_temp)
              finish# = 0
          End
      End
      If finish# = 1
          If transfer_site_location_temp  = glo:select1
              Case MessageEx('You cannot transfer stock to the same location.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              Select(?transfer_site_location_temp)
              finish# = 0
          End
      End
      If finish# = 1
          access:stock_alias.clearkey(sto_ali:ref_number_key)
          sto_ali:ref_number = glo:select2
          If access:stock_alias.fetch(sto_ali:ref_number_key)
              Return Level:Fatal
          Else
              finish2# = 1
              If sto_ali:quantity_stock - quantity_temp < 0
                  Case MessageEx('You cannot exceed the quantity in stock.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  Select(?quantity_temp)
                  finish2# = 0
      
              End
              If finish2# = 1
                  found# = 0
      
                  Save_sto_ID = Access:STOCK.SaveFile()
                  Access:STOCK.ClearKey(sto:Location_Key)
                  sto:Location    = Transfer_Site_Location_Temp
                  sto:Part_Number = sto_ali:Part_Number
                  Set(sto:Location_Key,sto:Location_Key)
                  Loop
                      If Access:STOCK.NEXT()
                         Break
                      End !If
                      If sto:Location    <> Transfer_Site_Location_Temp      |
                      Or sto:Part_Number <> sto_ali:Part_Number      |
                          Then Break.  ! End If
                      If sto:Ref_Number <> sto_Ali:Ref_Number
                          !Found a part in the location
                          If sto:Manufacturer <> sto_ali:Manufacturer Or |
                              sto:Description <> sto_ali:Description Or |
                              sto:Supplier <> sto_ali:Supplier
      
                              Case MessageEx('This part number already exists in the selected location.'&|
                                '<13,10>But it''s details are different.'&|
                                '<13,10>'&|
                                '<13,10>Part Currently Transferring:-'&|
                                '<13,10>Description: ' & Clip(sto_ali:Description) & |
                                '<13,10>Manufacturer: ' & Clip(sto_ali:Manufacturer) & |
                                '<13,10>Supplier: ' & Clip(sto_ali:Supplier) & |
                                '<13,10>'&|
                                '<13,10>Existing Part:-'&|
                                '<13,10>Description: ' & Clip(sto:Description) & |
                                '<13,10>Manufacturer: ' & Clip(sto:Manufacturer) & |
                                '<13,10>Supplier: ' & Clip(sto:Supplier) & |
                                '<13,10>'&|
                                '<13,10>Do you want to increase this part''s quantity?'&|
                                '<13,10>','ServiceBase 2000',|
                                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                  Of 1 ! &Yes Button
                                      ref_number$ = sto:ref_number
                                      found# = 1
                                      Break
                                  Of 2 ! &No Button
                              End!Case MessageEx
                          Else
                              Ref_Number$ = sto:Ref_Number
                              Found# = 1
                          End !sto:Supplier <> sto_ali:Supplier
                      End !If sto:Ref_Number <> sto_Ali:Ref_Number
      
                  End !Loop
                  Access:STOCK.RestoreFile(Save_sto_ID)
      
                  error# = 0
                  If found# = 0
                      Case MessageEx('Do you wish to create a NEW part in the NEW location?','ServiceBase 2000',|
                                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
      
                              If access:stock.primerecord() = Level:Benign
                                  ref_number$                = sto:ref_number
                                  sto:record                :=: sto_ali:record
                                  sto:ref_number             = ref_number$
                                  sto:location               = transfer_site_location_temp
                                  sto:quantity_stock         = quantity_temp
                                  access:stock.insert()
                              End!If access:stock.primerecord() = Level:Benign
                              setcursor(cursor:wait)
                              
                              save_stm_ali_id = access:stomodel_alias.savefile()
                              access:stomodel_alias.clearkey(stm_ali:model_number_key)
                              stm_ali:ref_number   = sto_ali:ref_number
                              set(stm_ali:model_number_key,stm_ali:model_number_key)
                              loop
                                  if access:stomodel_alias.next()
                                     break
                                  end !if
                                  if stm_ali:ref_number   <> sto_ali:ref_number      |
                                      then break.  ! end if
                                  yldcnt# += 1
                                  if yldcnt# > 25
                                     yield() ; yldcnt# = 0
                                  end !if
                                  get(stomodel,0)
                                  if access:stomodel.primerecord() = Level:Benign
                                      stm:record      :=: stm_ali:record
                                      stm:ref_number   = sto:ref_number
                                      stm:location     = transfer_site_location_temp
                                      access:stomodel.insert()
                                  end!if access:stomodel.primerecord() = Level:Benign
      
                              end !loop
                              access:stomodel_alias.restorefile(save_stm_ali_id)
                              setcursor()
      
                              shi:ref_number           = sto:ref_number
                              shi:transaction_type     = 'ADD'
                              shi:despatch_note_number = ''
                              shi:quantity             = quantity_temp
                              shi:date                 = Today()
                              shi:purchase_cost        = sto:purchase_cost
                              shi:sale_cost            = sto:sale_cost
                              shi:retail_cost          = sto:retail_cost
                              shi:information          = 'FROM LOCATION: ' & Clip(glo:select1)
                              shi:job_number           = ''
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              shi:user                 = use:user_code
                              IF ReqNo = ''
                                shi:notes                = 'STOCK TRANSFERRED'
                              ELSE
                                shi:notes                = 'STOCK TRANSFERRED ON REQ. NO. '&CLIP(ReqNo)
                              END
                              access:stohist.insert()
                          Of 2 ! &No Button
                              error# = 1
                      End!Case MessageEx
      
                  Else
                      access:stock.clearkey(sto:ref_number_key)
                      sto:ref_number = ref_number$
                      if access:stock.fetch(sto:ref_number_key)
                          Return Level:Fatal
                      Else
                          sto:quantity_stock += quantity_temp
                          If access:Stock.update() = Level:Benign
                              shi:ref_number           = sto:ref_number
                              shi:transaction_type     = 'ADD'
                              shi:despatch_note_number = ''
                              shi:quantity             = quantity_temp
                              shi:date                 = Today()
                              shi:purchase_cost        = sto:purchase_cost
                              shi:sale_cost            = sto:sale_cost
                              shi:retail_cost          = sto:retail_cost
                              shi:information          = 'FROM LOCATION: ' & Clip(glo:select1)
                              shi:job_number           = ''
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              shi:user                 = use:user_code
                              IF ReqNo = ''
                                shi:notes                = 'STOCK TRANSFERRED'
                              ELSE
                                shi:notes                = 'STOCK TRANSFERRED ON REQ. NO. '&CLIP(ReqNo)
                              END
                              access:stohist.insert()
      
                          End !If access:Stock.update() = Level:Benign
                      end !if
                  End
                  If error# = 0
                      access:stock.clearkey(sto:ref_number_key)
                      sto:ref_number = glo:select2
                      If access:stock.fetch(sto:ref_number_key)
                          Return Level:Fatal
                      Else
                          sto:quantity_stock -= quantity_temp
                          If access:stock.update() = Level:Benign
                              shi:ref_number           = sto:ref_number
                              shi:transaction_type     = 'DEC'
                              shi:despatch_note_number = ''
                              shi:quantity             = quantity_temp
                              shi:date                 = Today()
                              shi:purchase_cost        = sto:purchase_cost
                              shi:sale_cost            = sto:sale_cost
                              shi:retail_cost          = sto:retail_cost
                              shi:information          = 'TO LOCATION: ' & Clip(transfer_site_location_temp)
                              shi:job_number           = ''
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              shi:user                 = use:user_code
                              IF ReqNo = ''
                                shi:notes                = 'STOCK TRANSFERRED'
                              ELSE
                                shi:notes                = 'STOCK TRANSFERRED ON REQ. NO. '&CLIP(ReqNo)
                              END
                              access:stohist.insert()
                          End !If access:stock.update() = Level:Benign
                      end !if
                  End !If error# = 0
              End !If finish2# = 1
          end !if
          Post(event:closewindow)
      End !If finish# = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Transfer_Stock')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      quantity_temp = 1
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


FDCB1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1

