

   MEMBER('sbrvodex.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('SBRVO003.INC'),ONCE        !Local module procedure declarations
                     END


B2BExport PROCEDURE (func:StartDate,func:EndDate)     !Generated from procedure template - Process

Progress:Thermometer BYTE
tmp:Job_Type         STRING(30)
tmp:CustomerFault    STRING(255)
tmp:Fault            STRING(255)
tmp:DepartureDate    DATE
tmp:StartDate        DATE
tmp:EndDate          DATE
SheetDesc            CSTRING(41)
tmp:ExchangeIMEI     STRING(30)
Acc_Count            SHORT
Process:View         VIEW(JOBS)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,148,60),FONT('Tahoma',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(4,4,140,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(4,30,140,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(44,40,56,16),USE(?Progress:Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                    !Progress Manager
?F1SS                EQUATE(1000)
!# SW2 Extension            STRING(3)
f1Action             BYTE
f1FileName           CSTRING(256)
RowNumber            USHORT
StartRow             LONG
StartCol             LONG
Template             CSTRING(129)
f1Disabled           BYTE(0)
ssFieldQ              QUEUE(tqField).
CQ                    QUEUE(tqField).
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Progress:UserString{prop:FontColor} = -1
    ?Progress:UserString{prop:Color} = 15066597
    ?Progress:PctText{prop:FontColor} = -1
    ?Progress:PctText{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!---------------------
ssInit         ROUTINE
!---------------------
  DO ssBuildQueue                                     ! Initialize f1 field queue
  IF NOT UsBrowse(SsFieldQ,'VodafoneB2BExport',,,,CQ,SheetDesc,StartRow,StartCol,Template)
    ThisWindow.Kill
    RETURN
  END

!---------------------
ssBuildQueue   ROUTINE
!---------------------
    ssFieldQ.Name = 'job:Ref_Number'
    ssFieldQ.Desc = 'Job No'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = '''B2B'''
    ssFieldQ.Desc = 'Product Type'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Manufacturer'
    ssFieldQ.Desc = 'Make'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Model_Number'
    ssFieldQ.Desc = 'Model No'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Chr(39) & job:esn'
    ssFieldQ.Desc = 'Serial No'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'FORMAT(job:Date_Completed,@d07-)'
    ssFieldQ.Desc = 'Completed'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:Job_Type'
    ssFieldQ.Desc = 'Job Type'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Format(job:date_booked,@d07-)'
    ssFieldQ.Desc = 'Date Booked'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Order_Number'
    ssFieldQ.Desc = 'Trade Ord'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Order_Number'
    ssFieldQ.Desc = 'Process Code'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:CustomerFault'
    ssFieldQ.Desc = 'Customer Fault'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:Fault'
    ssFieldQ.Desc = 'Fault'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Acc_Count'
    ssFieldQ.Desc = 'No. Of Accessories'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'FORMAT(job:DOP,@d6)'
    ssFieldQ.Desc = 'Date Of Purchase'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Consignment_Number'
    ssFieldQ.Desc = 'Outgoing Consignment Number'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Incoming_Consignment_Number'
    ssFieldQ.Desc = 'Incoming Consignment Number'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'CHR(39)&CLIP(tmp:ExchangeIMEI)'
    ssFieldQ.Desc = 'Exchange IMEI'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
  SORT(ssFieldQ,+ssFieldQ.Desc)


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('B2BExport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  tmp:StartDate   = func:StartDate
  tmp:EndDate     = func:EndDate
    BIND('Acc_Count',Acc_Count)
    BIND('tmp:CustomerFault',tmp:CustomerFault)
    BIND('tmp:DepartureDate',tmp:DepartureDate)
    BIND('tmp:ExchangeIMEI',tmp:ExchangeIMEI)
    BIND('tmp:Fault',tmp:Fault)
    BIND('tmp:Job_Type',tmp:Job_Type)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  DO ssInit
  Relate:EXCHANGE.Open
  Relate:JOBACC.Open
  Access:MANFAULO.UseFile
  Access:JOBNOTES.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  Do RecolourWindow
  IF ThisWindow.Response <> RequestCancelled
    UsInitDoc(CQ,?F1ss,StartRow,StartCol,Template)
    ?F1SS{'Sheets("Sheet1").Name'} = SheetDesc
    IF Template = ''
      RowNumber = StartRow + 1
    ELSE
      RowNumber = StartRow
    END
  END
  ! support for CPCS
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:date_booked)
  ThisProcess.AddSortOrder(job:Date_Booked_Key)
  ThisProcess.AddRange(job:date_booked,tmp:StartDate,tmp:EndDate)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(JOBS,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  Do RecolourWindow


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:JOBACC.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Progress:Cancel
                                                      ! SW2 This section changed
      IF Message('Would you like to view the partially created spreadsheet?','Process Cancelled',ICON:Question,Button:Yes+Button:No,Button:No)|
      = Button:Yes
        UsDeInit(f1FileName, ?F1SS, 1)
      END
      ReturnValue = Level:Fatal
      RETURN ReturnValue
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ThisWindow.Response = RequestCompleted
    f1FileName = 'VodafoneB2BExport'
    f1Action = 2
  
    IF UsGetFileName( f1FileName, 1, f1Action )
      UsDeInit( f1FileName, ?F1SS, f1Action )
    END
    ?F1SS{'quit()'}
  ELSE
    IF RowNumber THEN ?F1SS{'quit()'}.
  END
  ReturnValue = PARENT.TakeCloseEvent()
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Neil!
  Access:SubTracc.ClearKey(sub:Account_Number_Key)
  sub:Account_Number = job:Account_Number
  IF Access:SubTracc.Fetch(sub:Account_Number_Key)
    Return Level:User
  ELSE
    IF CLIP(UPPER(sub:Main_Account_Number)) <> 'VODA B2B'
      Return Level:User
    END
  END
  
  IF job:Warranty_Job = 'YES' AND job:Chargeable_Job = 'NO'
    tmp:Job_Type = 'WARRANTY'
  ELSE
    tmp:Job_Type = job:Charge_Type
  END
  !!include Warranty Jobs Only
  !If job:warranty_job <> 'YES'
  !    Return Level:User
  !End!If job:warranty_job <> 'YES'
  !If job:Manufacturer <> 'MITSUBISHI'
  !    Return Level:User
  !End!If job:Manufacturer <> 'MITSUBISHI'
  !
  
  !Get Notes
  Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
  jbn:RefNumber = job:Ref_Number
  Access:JOBNOTES.TryFetch(jbn:RefNumberKey)
  
  tmp:CustomerFault = jbn:Fault_Description
  tmp:Fault = jbn:Invoice_Text
  
  !Count Accessories!
  Acc_Count = 0
  Access:JobAcc.ClearKey(jac:Ref_Number_Key)
  jac:Ref_Number = job:Ref_Number
  SET(jac:Ref_Number_Key,jac:Ref_Number_Key)
  LOOP
    IF Access:JobAcc.Next()
      BREAK
    END
    IF jac:Ref_Number <> job:Ref_Number
      BREAK
    END
    Acc_Count+=1
  END
  
  !
  !!Has it got an Exchange Unit?
  tmp:ExchangeIMEI = ''
  tmp:DepartureDate = job:Date_Completed
  If job:Exchange_unit_Number <> 0
      Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
      xch:Ref_Number = job:Exchange_Unit_Number
      If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
          tmp:ExchangeIMEI    = xch:ESN
          If job:Exchange_Despatched <> ''
              tmp:DepartureDate   = job:Exchange_Despatched
          End!If job:Exchange_Despatched <> ''
      End!If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
  End!If job:Exchange_unit_Number <> 0
  !
  !!Get Faults
  !access:manfaulo.clearkey(mfo:field_key)
  !mfo:manufacturer = 'MITSUBISHI'
  !mfo:field_number = 9
  !mfo:field        = job:fault_code9
  !if access:manfaulo.tryfetch(mfo:field_key) = Level:Benign
  !    tmp:CustomerFault = mfo:description
  !End!if access:manfpalo.tryfetch(mfp:field_key) = Level:Benign
  !
  !access:manfaulo.clearkey(mfo:field_key)
  !mfo:manufacturer = 'MITSUBISHI'
  !mfo:field_number = 10
  !mfo:field        = job:fault_code10
  !if access:manfaulo.tryfetch(mfo:field_key) = Level:Benign
  !    tmp:Fault = mfo:description
  !End!if access:manfpalo.tryfetch(mfp:field_key) = Level:Benign
  ReturnValue = PARENT.TakeRecord()
  UsFillRow(CQ,?F1SS,RowNumber,StartCol)
  RETURN ReturnValue

