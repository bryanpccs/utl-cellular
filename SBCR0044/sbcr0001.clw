

   MEMBER('sbcr0044.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('NetSimp.inc'),ONCE

                     MAP
                       INCLUDE('SBCR0001.INC'),ONCE        !Local module procedure declarations
                     END


CRC_Invoice_Export PROCEDURE                          !Generated from procedure template - Window

batch_seq_no         LONG
SparesCost           REAL
SparesVAT            REAL
AccessCost           REAL
AccessVAT            REAL
LabourCost           REAL
LabourVAT            REAL
PartsCost            REAL
PartsVAT             REAL
Parameter_Group      GROUP,PRE(param)
InvoiceNumber        LONG
STARTInvoiceNumber   LONG
ENDInvoiceNumber     LONG
MessageID            STRING(30)
EmailServer          STRING(80)
EmailPort            USHORT
EmailFrom            STRING(100)
EmailTo              STRING(100)
MessageNumber        LONG
                     END
Local_Group          GROUP,PRE(LOC)
Account_Name         STRING(100)
AccountNumber        STRING(15)
ApplicationName      STRING('ServiceBase 2000')
CommentText          STRING(100)
DefaultCurrencyID    STRING('GBP')
DesktopPath          STRING(255)
EmailSubject         STRING(100)
FileName             STRING(255)
HTMLFilename         STRING(255)
LastInvoiceNumber    LONG
MaxHTMLSize          LONG
MessageExTimeout     LONG(500)
Path                 STRING(255)
ProgramName          STRING(100)
SectionName          STRING(100)
Text                 STRING(255)
UserCode             STRING(3)
UserName             STRING(100)
                     END
Excel_Group          GROUP,PRE()
Excel                SIGNED !OLE Automation holder
excel:ColumnName     STRING(100)
excel:ColumnWidth    REAL
excel:DateFormat     STRING('dd mmm yyyy')
excel:OperatingSystem REAL
Excel:Visible        BYTE(0)
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG
Count                LONG
                     END
Misc_Group           GROUP,PRE()
OPTION1              SHORT
RecordCount          LONG
Result               BYTE
AccountValue         STRING(15)
AccountTag           STRING(1)
AccountCount         LONG
AccountChange        BYTE
Save_Job_ID          ULONG,AUTO
SAVEPATH             STRING(255)
TotalCharacters      LONG
                     END
Progress_Group       GROUP,PRE()
progress:Text        STRING(100)
progress:NextCancelCheck TIME
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(1)
HeadLastCol          STRING('E')
DataLastCol          STRING('S')
HeadSummaryRow       LONG(9)
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(2048)
                     END
HTML_Queue           QUEUE,PRE(html)
LINE                 STRING(1024)
                     END
VATCode_Queue        QUEUE,PRE(vatq)
VATCode              STRING(2)
VAT_Rate             REAL
                     END
Email_Group          GROUP,PRE()
EmailServer          STRING(80)
EmailPort            USHORT
EmailFrom            STRING(255)
EmailTo              STRING(1024)
EmailSubject         STRING(255)
EmailCC              STRING(1024)
EmailBCC             STRING(1024)
EmailFileList        STRING(1024)
EmailMessageText     STRING(16384)
EmailMessageHTML     STRING(1048576)
                     END
GeneralLedgerCode    GROUP,PRE(gl)
CompanyNumber        STRING('03')
Filler1              STRING('-')
CostCentre           STRING('0120')
Filler2              STRING('-')
AccountCode          STRING(4)
                     END
InvoiceAccount_Queue QUEUE,PRE(invaccQ)
AccountNumber        STRING(15)
AccountType          STRING(3)
CountryCode          STRING('03')
EuroApplies          BYTE
LabourVATCode        STRING(2)
LabourVATRate        REAL
PartsVATCode         STRING(2)
PartsVATRate         REAL
RetailVATCode        STRING(2)
RetailVATRate        REAL
CourierVATCode       STRING(2)
CourierVATRate       REAL(0.0)
                     END
window               WINDOW('Report'),AT(,,225,97),FONT('Tahoma',8,,),CENTER,ICON('PC.ICO'),CENTERED,GRAY,DOUBLE,IMM
                       SHEET,AT(3,2,217,62),USE(?Sheet1),SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           STRING('0 = To Last Invoice'),AT(156,36),USE(?String8)
                           STRING('From  Invoice Number'),AT(8,20,84,12),USE(?String4),LEFT
                           SPIN(@n-14),AT(96,20,56,12),USE(param:STARTInvoiceNumber),TIP('Select the first Invoice Number to export.'),REQ,RANGE(1,999999999),STEP(1)
                           STRING('To Invoice Number'),AT(8,36,84,12),USE(?String4:2),LEFT
                           SPIN(@n-14),AT(96,36,56,12),USE(param:ENDInvoiceNumber),TIP('Select the Invoice Number to be the last on the export<13,10>   or set to 0 (Zero) to' &|
   ' use all Invoices<13,10>   from "From Invoice Number" onwards.<13,10>'),REQ,RANGE(0,999999999),STEP(1)
                           CHECK('Show Excel'),AT(164,52,52,12),USE(Excel:Visible),HIDE,LEFT,FONT(,,,FONT:bold),TIP('Tick To Show Excel During Report Creation.<13,10>Only Tick if you are experiencing pr' &|
   'oblems <13,10>with Excel reports.'),VALUE('1','0')
                         END
                       END
                       PANEL,AT(4,68,216,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Create Data File'),AT(100,72,56,16),USE(?OkButton),FLAT,LEFT,TIP('Click to create export'),ICON(ICON:Tick),DEFAULT
                       BUTTON('Close'),AT(160,72,56,16),USE(?CancelButton),FLAT,LEFT,TIP('Press to close criteria form.'),ICON(ICON:Cross)
                     END

RecordExport     File,Driver('BASIC','/COMMA=9 /ALWAYSQUOTE=OFF'),Pre(export),Name(LOCAL:File_Name),Create,Bindable,Thread
Record                  Record
!-- Header ---------------------------------------
Voucher_Number      STRING(17)
Document_Type       STRING(2)
Description         STRING(30)
Batch_ID            STRING(15)
Document_Date       STRING(10)
Creditor_ID         STRING(15)
Due_Date            STRING(10)
Currency_ID         STRING(15)
PO_Number           STRING(20)
Purchases           STRING(10)
Tax                 STRING(10)
!-- Receivables -----------------------------------
GL_Account              STRING(12)
Distribution_Type       STRING(2)
Debit_Amount            STRING(10)
Credit_Amount           STRING(10)
Distribution_Reference  STRING(30)
!-- VAT ------------------------------------------
Tax_Detail_ID           STRING(15)
Amount                  STRING(10)
Tax_Amount              STRING(10)
F20_Unused              STRING(20)
GP_Customer_Code        STRING(20)
                        End
                    End
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Local Data Classes
FTPData              CLASS(NetFTPClientData)          !Generated by NetTalk Extension (Class Definition)

                     END

!Local Data Classes
FTP                  CLASS(NetFTPClientControl)       !Generated by NetTalk Extension (Class Definition)

                     END

    MAP
AppendString    PROCEDURE(STRING, STRING, STRING), STRING
DateToString PROCEDURE(DATE), STRING
GetCurrencyID   PROCEDURE(), STRING
GetCustomerName PROCEDURE(), STRING
GetExpectedShipDate PROCEDURE(), STRING!(2)
GetTransactionCode PROCEDURE(), STRING
GetInvoiceNumber      PROCEDURE(), LONG !

GetVATRate  PROCEDURE( STRING ), REAL
GetEmailServer    PROCEDURE(), STRING
GetEmailPort      PROCEDURE(), LONG
GetEmailFrom      PROCEDURE(), STRING
GetEmailTo        PROCEDURE(), STRING
SetInvoiceNumber    PROCEDURE()
SetEmailServer    PROCEDURE()! 
SetEmailPort      PROCEDURE()! 
SetEmailFrom      PROCEDURE()! 
SetEmailTo        PROCEDURE()! 
IsMessageHTMLFull   PROCEDURE(), LONG ! BOOL
LoadESTPARTS        PROCEDURE( LONG, LONG=True ), LONG ! BOOL
LoadJOBS        PROCEDURE( LONG ), LONG ! BOOL
LoadJOBSE        PROCEDURE( LONG ), LONG ! BOOL
LoadJOBSFromInvoice        PROCEDURE( LONG, LONG = True ), LONG ! BOOL
LoadPARTS        PROCEDURE( LONG, LONG = True ), LONG ! BOOL
LoadSTOCK        PROCEDURE( LONG ), LONG ! BOOL
LoadSUBTRACC    PROCEDURE( STRING ), LONG ! BOOL
LoadTRADEACC    PROCEDURE( STRING ), LONG ! BOOL
LoadRETSALES        PROCEDURE( LONG, LONG = True ), LONG ! BOOL
LoadRETSTOCK        PROCEDURE( LONG, LONG ), LONG ! BOOL
LoadWARPARTS        PROCEDURE( LONG, LONG = True ), LONG ! BOOL
NumberToBool    PROCEDURE(LONG), STRING
SetInvoiceAccount   PROCEDURE( STRING, STRING )
WriteTABLE PROCEDURE(LONG)
WriteTD PROCEDURE(STRING)
WriteTH PROCEDURE(STRING)
WriteTR PROCEDURE(LONG)
WriteDebug  PROCEDURE( STRING )
GetVatCode  PROCEDURE(Real),String
    END ! MAP
! ProgressWindow Declarations
 
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
 
! Excel EQUATES

!----------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic  EQUATE(0FFFFEFF7h) ! Constants.
xlSolid      EQUATE(        1 ) ! Constants.
xlLeft       EQUATE(0FFFFEFDDh) ! Constants.
xlRight      EQUATE(0FFFFEFC8h) ! Constants.
xlLastCell   EQUATE(       11 ) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!----------------------------------------------------
! Office EQUATES

!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?String8{prop:FontColor} = -1
    ?String8{prop:Color} = 15066597
    ?String4{prop:FontColor} = -1
    ?String4{prop:Color} = 15066597
    If ?param:STARTInvoiceNumber{prop:ReadOnly} = True
        ?param:STARTInvoiceNumber{prop:FontColor} = 65793
        ?param:STARTInvoiceNumber{prop:Color} = 15066597
    Elsif ?param:STARTInvoiceNumber{prop:Req} = True
        ?param:STARTInvoiceNumber{prop:FontColor} = 65793
        ?param:STARTInvoiceNumber{prop:Color} = 8454143
    Else ! If ?param:STARTInvoiceNumber{prop:Req} = True
        ?param:STARTInvoiceNumber{prop:FontColor} = 65793
        ?param:STARTInvoiceNumber{prop:Color} = 16777215
    End ! If ?param:STARTInvoiceNumber{prop:Req} = True
    ?param:STARTInvoiceNumber{prop:Trn} = 0
    ?param:STARTInvoiceNumber{prop:FontStyle} = font:Bold
    ?String4:2{prop:FontColor} = -1
    ?String4:2{prop:Color} = 15066597
    If ?param:ENDInvoiceNumber{prop:ReadOnly} = True
        ?param:ENDInvoiceNumber{prop:FontColor} = 65793
        ?param:ENDInvoiceNumber{prop:Color} = 15066597
    Elsif ?param:ENDInvoiceNumber{prop:Req} = True
        ?param:ENDInvoiceNumber{prop:FontColor} = 65793
        ?param:ENDInvoiceNumber{prop:Color} = 8454143
    Else ! If ?param:ENDInvoiceNumber{prop:Req} = True
        ?param:ENDInvoiceNumber{prop:FontColor} = 65793
        ?param:ENDInvoiceNumber{prop:Color} = 16777215
    End ! If ?param:ENDInvoiceNumber{prop:Req} = True
    ?param:ENDInvoiceNumber{prop:Trn} = 0
    ?param:ENDInvoiceNumber{prop:FontStyle} = font:Bold
    ?Excel:Visible{prop:Font,3} = -1
    ?Excel:Visible{prop:Color} = 15066597
    ?Excel:Visible{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!-----------------------------------------------
OKButton_Pressed                     ROUTINE
    DATA
    CODE
        !------------------------------------------------------------------
        IF (param:ENDInvoiceNumber > 0) AND (param:ENDInvoiceNumber < param:STARTInvoiceNumber)
            Case MessageEx('"To Invoice Number" Must be greater or equal to "From Invoice Number".', LOC:ApplicationName,|
                           'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LOC:MessageExTimeout)
                Of 1 ! &OK Button
            End!Case MessageEx

            EXIT
        END!If                                           
        !------------------------------------------------------------------
        LOC:Filename = ''
        DO LoadFileDialog

        IF CLIP(LOC:Filename) = ''
            EXIT
        END!If                                           

        LOCAL:File_Name = LOC:Filename
        !------------------------------------------------------------------
        !Found
        Remove(RecordExport)

        Open(RecordExport)
        If Error()
            Create(RecordExport)
            Open(RecordExport)
            If Error()
                Case MessageEx('Cannot create export file.', LOC:ApplicationName,|
                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LOC:MessageExTimeout)
                    Of 1 ! &OK Button
                End!Case MessageEx

                EXIT
            End!If Error()

        End!If Error()
        !##################################################################
        recordspercycle         = 25
        recordsprocessed        = 0
        percentprogress         = 0
        progress:thermometer    = 0
        Progress:HighWaterMark  = 10

        recordstoprocess        = 10 ! 1000 * (tmp:EndDate - tmp:StartDate) + 1

        thiswindow.reset(1)
        open(progresswindow)

        ?progress:userstring{prop:text} = 'Exporting Invoices...'
        ?progress:pcttext{prop:text}    = '0% Completed'
        !------------------------------------------------------------------
        !---Before Routine
        !DO WriteHeaderHTML

        CompCount# = 0
        !------------------------------------------------------------------
        ! Ref_Number_Key KEY( job:Ref_Number ),NOCASE,PRIMARY
        !
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = param:STARTInvoiceNumber
        Set(inv:Invoice_Number_Key, inv:Invoice_Number_Key)

        Loop UNTIL Access:INVOICE.NEXT() <> Level:Benign
            !--------------------------------------------------------------
            ?progress:userstring{prop:Text} = 'Done : ' & CompCount# & ' Found: ' & Count#
            Display()

            CompCount# += 1

            Do GetNextRecord2
            Do cancelcheck
            If tmp:cancel = 1
                GOTO Finalize
            End!If tmp:cancel = 1
            !--------------------------------------------------------------
            IF (param:ENDInvoiceNumber > 0) AND (inv:Invoice_Number > param:ENDInvoiceNumber)
                BREAK
            END !IF
            !==============================================================
            IF (inv:Total = 0.0) THEN
                CYCLE
            END
            param:InvoiceNumber = inv:Invoice_Number

            SetInvoiceAccount(inv:Account_Number, inv:Invoice_Type)
            !!-- Header ---------------------------------------
            ! Start Change 2052 BE(03/10/03)
            !export:Voucher_Number      = inv:Invoice_Number               !STRING(17)
            !export:Document_Type       = 0 !9=Sales 8=Other               !LONG
            !export:Description         = inv:Invoice_Type                 !STRING(30)
            !export:Batch_ID            = inv:Batch_Number                 !STRING(30)
            !export:Document_Date       = DateToString(TODAY())            !STRING(10)
            !export:Creditor_ID         = invaccQ:AccountNumber            !STRING(15)
            !!export:Payment_Terms
            !export:Due_Date            = DateToString(TODAY())            !STRING(10)
            !export:Currency_ID         = GetCurrencyID()                  !STRING(15)
            !export:Document_Number     = inv:Invoice_Type                 !STRING(20)
            !export:PO_Number           = inv:Invoice_Number               !STRING(20)
            !!Tax_Schedule_ID
            !export:Purchases           = inv:Total                        !REAL
            !!Trade_Discount
            !!Freight
            !!Miscellaneous
            !export:Tax                 = 0                                !REAL
            ! End Change 2052 BE(03/10/03)
            !--------------------------------------------------
            gl:CompanyNumber = invaccQ:CountryCode

            CASE inv:Invoice_Type
            OF 'CHA'
                DO Invoice_CHA ! Multiple Chargeable
            OF 'WAR'
                DO Invoice_WAR ! Warranty
            OF 'RET'
                DO Invoice_RET ! Retail
            OF 'SIN'
                DO Invoice_SIN ! Single Chargeable
            ELSE
                !DO Invoice_CHA
            END !CASE
            !--------------------------------------------------------------
            Count#     += 1
            !--------------------------------------------------------------
        End !Loop

        ?progress:userstring{prop:Text} = 'Done : ' & CompCount# & ' Found: ' & Count#
        Display()
        !##################################################################
        !---After Routine
        !DO SendMessage
        !DO WriteFooterHTML
        !DO SendFTP
        !------------------------------------------------------------------
Finalize Setcursor()
        !------------------------------------------------------------------
        Do EndPrintRun

        close(progresswindow)
        Close(RecordExport)
        !------------------------------------------------------------------
        SetInvoiceNumber()
        !------------------------------------------------------------------
        Case MessageEx('Export Complete.',LOC:ApplicationName,|
                       'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,LOC:MessageExTimeout)
            Of 1 ! &OK Button
        End!Case MessageEx
        !------------------------------------------------------------------
        post(event:closewindow)
        !------------------------------------------------------------------
    EXIT

!-----------------------------------------------
Invoice_CHA         ROUTINE ! Multiple Chargeable
    DATA
    CODE

    PartsCost = 0.0
    PartsVAT = 0.0
    LabourCost = 0.0
    LabourVAT = 0.0

    Access:JOBS.ClearKey(job:InvoiceNumberKey)
    job:Invoice_Number = inv:Invoice_Number
    SET(job:InvoiceNumberKey, job:InvoiceNumberKey)
    LOOP
        IF ((Access:JOBS.TryNext() <> Level:Benign) OR (job:Invoice_Number <> inv:Invoice_Number)) THEN
            BREAK
        END

        IF (job:Chargeable_Job <> 'YES') THEN
            CYCLE
        END

        DO Transaction
    END
    DO Receivables
    EXIT

Invoice_RET         ROUTINE ! Retail
    DATA
TotalCost   REAL
TotalVAT    REAL
    CODE

    SparesCost = 0.0
    SparesVAT = 0.0
    AccessCost = 0.0
    AccessVAT = 0.0
    LabourCost = 0.0
    LabourVAT = 0.0

    Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
    ret:Invoice_Number = inv:Invoice_Number
    SET(ret:Invoice_Number_Key, ret:Invoice_Number_Key)
    LOOP
        IF ((Access:RETSALES.Next() <> Level:Benign) OR (ret:Invoice_Number <> inv:Invoice_Number)) THEN
            BREAK
        END
        LabourCost += ret:Courier_Cost
        DO RetStock_VAT
    END

    LabourVAT +=  ROUND((LabourCost * inv:vat_rate_retail)/100.0, 0.01)

    TotalCost = SparesCost + AccessCost + LabourCost
    TotalVAT = SparesVAT + AccessVAT + LabourVAT

    IF (TotalCost <> 0.0) THEN
        !--DEBUG ---------------------------------------------------------------------------------
        IF (TotalCost <> inv:Total) THEN
            MESSAGE('Invoice: ' & inv:invoice_number & ' Costs: ' & TotalCost & ' - ' & inv:Total)
        END
        !--DEBUG ---------------------------------------------------------------------------------

        !--Header--------------------------------------------
        export:Voucher_Number      = inv:Invoice_Number
        export:Document_Type       = 1
        export:Description         = ''
        export:Batch_ID            = FORMAT(TODAY(), @d12) & '-' & batch_seq_no
        export:Document_Date       = LEFT(FORMAT(inv:Date_Created, @D06))
        export:Creditor_ID         = invaccQ:AccountNumber
        export:Due_Date            = LEFT(FORMAT(inv:Date_Created+30, @D06))
        export:Currency_ID         = GetCurrencyID()
        export:PO_Number           = ''
        export:Purchases           = FORMAT(inv:Total, @n10.02)
        export:Tax                 = FORMAT(TotalVAT, @n10.02)

        IF (SparesCost <> 0.0) THEN
            !--Receivables-----------------------------------
            gl:CostCentre = '0120' ! Spares
            gl:AccountCode = '1115' ! Spares
            export:GL_Account = GeneralLedgerCode
            export:Distribution_Type       = 9
            export:Debit_Amount            = 0
            export:Credit_Amount           = FORMAT(SparesCost, @n10.02)
            export:Distribution_Reference  = ''
            !-- VAT ------------------------------------------
            export:Tax_Detail_ID = ''
            export:Amount        = ''
            export:Tax_Amount = ''
            export:F20_Unused = ''
            export:GP_Customer_Code = ''
            !--Write Record------------------------------------
            Add(RecordExport)
        END

        IF (AccessCost <> 0.0) THEN
            !--Receivables-----------------------------------
            gl:CostCentre = '0150' ! Accessories
            gl:AccountCode = '1120' ! Accessories
            export:GL_Account = GeneralLedgerCode
            export:Distribution_Type       = 9
            export:Debit_Amount            = 0
            export:Credit_Amount           = FORMAT(AccessCost, @n10.02)
            export:Distribution_Reference  = ''
            !-- VAT ------------------------------------------
            export:Tax_Detail_ID = ''
            export:Amount        = ''
            export:Tax_Amount = ''
            export:F20_Unused = ''
            export:GP_Customer_Code = ''
            !--Write Record------------------------------------
            Add(RecordExport)
        END

        IF (LabourCost <> 0.0) THEN
            !--Receivables-----------------------------------
            gl:CostCentre = '0000' ! Courier
            gl:AccountCode = '1135' ! Courier
            export:GL_Account = GeneralLedgerCode
            export:Distribution_Type       = 9
            export:Debit_Amount            = 0
            export:Credit_Amount           = FORMAT(LabourCost, @n10.02)
            export:Distribution_Reference  = ''
            !-- VAT ------------------------------------------
            export:Tax_Detail_ID = ''
            export:Amount        = ''
            export:Tax_Amount = ''
            export:F20_Unused = ''
            export:GP_Customer_Code = ''
            !--Write Record------------------------------------
            Add(RecordExport)
        END

        !--Receivables-----------------------------------
        gl:CostCentre = '0000' ! Receivables
        gl:AccountCode = '6210' ! Receivables
        export:GL_Account = GeneralLedgerCode
        export:Distribution_Type       = 3
        export:Debit_Amount            = FORMAT(TotalCost + TotalVAT, @n10.02)
        export:Credit_Amount           = 0
        export:Distribution_Reference  = ''
        !-- VAT ------------------------------------------
        export:Tax_Detail_ID = ''
        export:Amount        = ''
        export:Tax_Amount = ''
        export:F20_Unused = ''
        export:GP_Customer_Code = ''
        !--Write Record------------------------------------
        Add(RecordExport)

        !--Receivables-----------------------------------
        gl:CostCentre = '0000' ! VAT
        gl:AccountCode = '6540' ! VAT
        export:GL_Account = GeneralLedgerCode
        export:Distribution_Type       = 13
        export:Debit_Amount            = 0
        export:Credit_Amount           = FORMAT(TotalVAT, @n10.02)
        export:Distribution_Reference  = ''
        !-- VAT ------------------------------------------
        export:Tax_Detail_ID = GetVatCode(inv:vat_rate_retail)
        export:Amount        = FORMAT(TotalCost, @n10.02)
        export:Tax_Amount = FORMAT(TotalVAT, @n10.02)
        export:F20_Unused = ''
        export:GP_Customer_Code = ''
        !--Write Record------------------------------------
        Add(RecordExport)
    END

    EXIT
Invoice_SIN         ROUTINE ! Single Chargeable
    DATA
    CODE

    PartsCost = 0.0
    PartsVAT = 0.0
    LabourCost = 0.0
    LabourVAT = 0.0

    Access:JOBS.ClearKey(job:InvoiceNumberKey)
    job:Invoice_Number = inv:invoice_number
    IF (access:JOBS.fetch(job:InvoiceNumberKey) = Level:Benign) THEN
        DO Transaction
        DO Receivables
    END

    EXIT


Invoice_WAR         ROUTINE ! Warranty
    DATA
    CODE

    PartsCost = 0.0
    PartsVAT = 0.0
    LabourCost = 0.0
    LabourVAT = 0.0

    Access:JOBS.ClearKey(job:InvoiceNumberKey)
    job:Invoice_Number = inv:Invoice_Number
    SET(job:InvoiceNumberKey, job:InvoiceNumberKey)
    LOOP
        IF ((Access:JOBS.TryNext() <> Level:Benign) OR (job:Invoice_Number <> inv:Invoice_Number)) THEN
            BREAK
        END
        IF (job:Warranty_Job <> 'YES') THEN
            CYCLE
        END
        DO Transaction
    END
    DO Receivables
    EXIT


!-----------------------------------------------
Transaction     ROUTINE
    DATA
    CODE

    LabourCost += job:Invoice_Labour_Cost
    LabourVAT +=  ROUND((job:Invoice_Labour_Cost * inv:vat_rate_labour)/100.0, 0.01)

    PartsCost += job:Invoice_Parts_Cost
    PartsVAT += ROUND((job:Invoice_Parts_Cost * inv:vat_rate_parts)/100.0, 0.01)

    LabourCost += job:Invoice_Courier_Cost
    LabourVAT +=  ROUND((job:Invoice_Courier_Cost * inv:vat_rate_labour)/100.0, 0.01)
    EXIT
!Calc_VAT       ROUTINE
!    DATA
!First LONG(True)
!    CODE
!        !------------------------------------------------------------------
!        ! 0120    1115        Retail
!        ! 0150    1120        Accessories
!        !
!        ! 0100    1100        Repair Labour
!        ! 0100    1110        Repair Parts
!        !
!        !------------------------------------------------------------------
!        gl:CostCentre        = '0100' ! Repair
!        gl:AccountCode       = '1110' ! Parts
!        export:GL_Account    = GeneralLedgerCode
!
!        export:Tax_Detail_ID = invaccQ:PartsVATCode                            ! STRING(20)
!        export:Amount        = job:Invoice_Parts_Cost                          ! REAL
!        export:Tax_Amount    = job:Invoice_Parts_Cost * invaccQ:PartsVATRate   ! REAL
!
!        DO WriteDataCSV
!        !--------------------------------------------------------------
!        gl:CostCentre        = '0100' ! Repair
!        gl:AccountCode       = '1110' ! Labour
!        export:GL_Account    = GeneralLedgerCode
!
!        export:Tax_Detail_ID = invaccQ:LabourVATCode                           ! STRING(20)
!        export:Amount        = job:Invoice_Labour_Cost                         ! REAL
!        export:Tax_Amount    = job:Invoice_Labour_Cost * invaccQ:LabourVATRate ! REAL
!
!        DO WriteDataCSV
!        !--------------------------------------------------------------
!        IF job:Invoice_Courier_Cost <> 0
!            gl:CostCentre        = '0100' ! Repair
!            gl:AccountCode       = '1110' ! Labour
!            export:GL_Account    = GeneralLedgerCode
!
!            export:Tax_Detail_ID = ''                             ! STRING(20)
!            export:Amount        = job:Invoice_Courier_Cost       ! REAL
!            export:Tax_Amount    = job:Invoice_Courier_Cost * 0.0 ! REAL
!
!            DO WriteDataCSV
!        END !IF
!        !--------------------------------------------------------------
!    EXIT
!
!
!        LOOP WHILE LoadPARTS(job:Ref_Number, First)
!            First = False
!            !!-- VAT ------------------------------------------
!            export:Tax_Detail_ID = ''                            ! STRING(20)
!            export:Amount        = par:Sale_Cost * par:Quantity  ! REAL
!            export:Tax_Amount    = 0                             ! REAL
!
!            IF LoadSTOCK(par:Part_Ref_Number)
!                IF sto:Use_VAT_Code = 'YES'
!                    !sto:Service_VAT_Code
!                    export:Tax_Detail_ID = sto:Retail_VAT_Code           ! STRING(20)
!                    export:Tax_Amount = export:Amount * GetVATRate(sto:Retail_VAT_Code)
!
!                END !IF
!
!                IF sto:Accessory = 'YES'
!                    gl:CostCentre  = '0150'
!                    gl:AccountCode = '1120'
!                ELSE
!                    gl:CostCentre  = '0120'
!                    gl:AccountCode = '1115'
!                END !IF
!
!                export:GL_Account = GeneralLedgerCode
!                DO WriteDataCSV
!
!                CYCLE
!            END !IF
!
!            DO WriteDataCSV
!        END !LOOP
RetStock_VAT    ROUTINE
    DATA
    CODE
        access:RETSTOCK.clearkey(res:part_number_key)
        res:ref_number = ret:ref_number
        SET(res:part_number_key, res:part_number_key)
        LOOP
            IF ((Access:RETSTOCK.Next() <> Level:Benign) OR (res:Ref_Number <> ret:Ref_Number)) THEN
                BREAK
            END
            IF (res:despatched <> 'YES') THEN
                CYCLE
            END
            Access:STOCK.ClearKey(sto:Ref_Number_Key)
            sto:Ref_Number = res:Part_Ref_Number
            IF (access:STOCK.fetch(sto:ref_number_key) = Level:Benign) THEN
                IF sto:Accessory = 'YES'
                    AccessCost += res:Item_Cost * res:Quantity
                    AccessVAT +=  ROUND((res:Item_Cost * res:Quantity * inv:vat_rate_retail)/100.0, 0.01)
                ELSE
                    SparesCost += res:Item_Cost * res:Quantity
                    SparesVAT +=  ROUND((res:Item_Cost * res:Quantity * inv:vat_rate_retail)/100.0, 0.01)
                END
             ELSE
                SparesCost += res:Item_Cost * res:Quantity
                SparesVAT +=  ROUND((res:Item_Cost * res:Quantity * inv:vat_rate_retail)/100.0, 0.01)
            END
        END
    EXIT

!WarParts_VAT    ROUTINE
!    DATA
!First LONG(True)
!    CODE
!        !------------------------------------------------------------------
!        ! 0120    1115        Retail
!        ! 0150    1120        Accessories
!        !                
!        ! 0100    1100        Repair Labour
!        ! 0100    1110        Repair Parts
!        !
!        IF job:Warranty_Job = 'YES'
!            !--------------------------------------------------------------
!            gl:CostCentre        = '0100' ! Repair
!            gl:AccountCode       = '1110' ! Parts
!            export:GL_Account    = GeneralLedgerCode
!
!            export:Tax_Detail_ID = invaccQ:PartsVATCode                             ! STRING(20)
!            export:Amount        = job:Parts_Cost_Warranty                          ! REAL
!            export:Tax_Amount    = job:Parts_Cost_Warranty * invaccQ:PartsVATRate   ! REAL
!
!            DO WriteDataCSV
!            !--------------------------------------------------------------
!            gl:CostCentre        = '0100' ! Repair
!            gl:AccountCode       = '1110' ! Labour
!            export:GL_Account    = GeneralLedgerCode
!
!            export:Tax_Detail_ID = invaccQ:LabourVATCode                            ! STRING(20)
!            export:Amount        = job:Labour_Cost_Warranty                         ! REAL
!            export:Tax_Amount    = job:Labour_Cost_Warranty * invaccQ:LabourVATRate ! REAL
!
!            DO WriteDataCSV
!            !--------------------------------------------------------------
!            IF job:Courier_Cost_Warranty <> 0
!                gl:CostCentre        = '0100' ! Repair
!                gl:AccountCode       = '1110' ! Labour
!                export:GL_Account    = GeneralLedgerCode
!
!                export:Tax_Detail_ID = ''                              ! STRING(20)
!                export:Amount        = job:Courier_Cost_Warranty       ! REAL
!                export:Tax_Amount    = job:Courier_Cost_Warranty * 0.0 ! REAL
!
!                DO WriteDataCSV
!            END !IF
!            !--------------------------------------------------------------
!        END !IF
!        !------------------------------------------------------------------
!    EXIT
!
!
!
!!        LOOP WHILE LoadWARPARTS(job:Ref_Number, First)
!!            First = False
!!            !!-- VAT ------------------------------------------
!!            export:Tax_Detail_ID = ''                                   ! STRING(20)
!!            export:Amount        = wpr:Sale_Cost * wpr:Quantity         ! REAL
!!            export:Tax_Amount    = export:Amount * invaccQ:PartsVATRate ! REAL
!!
!!            IF LoadSTOCK(wpr:Part_Ref_Number)
!!                IF sto:Use_VAT_Code = 'YES'
!!                    !sto:Service_VAT_Code
!!                    export:Tax_Detail_ID = sto:Retail_VAT_Code                          ! STRING(20)
!!                    export:Tax_Amount = export:Amount * GetVATRate(sto:Retail_VAT_Code)
!!
!!                END !IF
!!
!!                !IF sto:Accessory = 'YES'
!!                !    gl:CostCentre  = '0150'
!!                !    gl:AccountCode = '1120'
!!                !ELSE
!!                    gl:CostCentre  = '0100' ! Repair
!!                    gl:AccountCode = '1110' ! Parts
!!                !END !IF
!!
!!                export:GL_Account = GeneralLedgerCode
!!                DO WriteDataCSV
!!                CYCLE
!!            END !IF
!!
!!            DO WriteDataCSV
!!        END !LOOP
!Courier_VAT ROUTINE
!    DATA
!    CODE
!        !------------------------------------------------------------------
!        !------------------------------------------------------------------
!    EXIT
!Labour_VAT ROUTINE
!    DATA
!    CODE
!        !------------------------------------------------------------------
!        !------------------------------------------------------------------
!    EXIT
!Estimate_VAT            ROUTINE
!    DATA
!    CODE
!        !------------------------------------------------------------------
!        ! 0120    1115        Retail
!        ! 0150    1120        Accessories
!        !                
!        ! 0100    1100        Repair Labour
!        ! 0100    1110        Repair Parts
!        !
!        !------------------------------------------------------------------
!        gl:CostCentre        = '0100'
!        gl:AccountCode       = '1100'
!        export:GL_Account    = GeneralLedgerCode
!
!        export:Tax_Detail_ID = invaccQ:LabourVATCode                            ! STRING(20)
!        export:Amount        = job:Labour_Cost_Estimate                         ! REAL
!        export:Tax_Amount    = job:Labour_Cost_Estimate * invaccQ:LabourVATRate ! REAL
!
!        DO WriteDataCSV
!        !------------------------------------------------------------------
!        gl:CostCentre        = '0100'
!        gl:AccountCode       = '1110'
!        export:GL_Account    = GeneralLedgerCode
!
!        export:Tax_Detail_ID = invaccQ:LabourVATCode                            ! STRING(20)
!        export:Amount        = job:Parts_Cost_Estimate                          ! REAL
!        export:Tax_Amount    = job:Parts_Cost_Estimate * invaccQ:LabourVATRate  ! REAL
!
!        DO WriteDataCSV
!        !------------------------------------------------------------------
!    EXIT
!-----------------------------------------------
!SendMessage     ROUTINE
!    DATA
!    CODE
!        !------------------------------------------------------------------
!        IF CLIP(LOC:EmailSubject) = ''
!            LOC:EmailSubject = CLIP(LOC:ProgramName) & ' (' & FORMAT(TODAY(), @D8) & ' ' & FORMAT(CLOCK(), @T6) & ') '
!        END !IF
!
!        param:MessageNumber += 1
!        !------------------------------------------------------------------
!        EmailServer      = param:EmailServer
!        EmailPort        = param:EmailPort
!        EmailFrom        = param:EmailFrom
!        EmailTo          = param:EmailTo
!        EmailSubject     = CLIP(LOC:EmailSubject) & '[' & param:MessageNumber & ']'
!        EmailCC          = ''
!        EmailBCC         = ''
!        EmailFileList    = '' ! Could be 'c:\test.txt'
!        EmailMessageText = ''
!        EmailMessageHTML = ''
!        !------------------------------------------------------------------
!        DO WriteFooterHTML
!
!        LOOP x# = 1 TO RECORDS(HTML_Queue)
!            GET(HTML_Queue, x#)
!            EmailMessageHTML = CLIP(EmailMessageHTML) & CLIP(html:LINE)
!        END !LOOP
!
!        !SETCLIPBOARD(EmailMessageHTML)
!        !------------------------------------------------------------------
!        !         pEmailServer, pEmailPort, pEmailFrom, pEmailTo, pEmailSubject, pEmailCC, pEmailBcc, pEmailFileList, pEmailMessageText, pEmailMessageHTML
!       ! SendEMail( EmailServer,  EmailPort,  EmailFrom,  EmailTo,  EmailSubject,  EmailCC,  EmailBCC,  EmailFileList,  EmailMessageText,  EmailMessageHTML)
!        !------------------------------------------------------------------
!        FREE(HTML_Queue)
!        TotalCharacters = 0
!
!        DO WriteHeaderHTML
!        !------------------------------------------------------------------
!!        setcursor (CURSOR:WAIT)
!        !------------------------------------------------------------------
!
!SendFTP     ROUTINE
!    DATA
!    CODE
!        !------------------------------------------------------------------
!!        setcursor (CURSOR:WAIT)
!            FTP.ServerHost               = 'ftp.crc-group.com'
!            FTP.User                     = 'anonymous'
!            FTP.Password                 = 'anonymous@hotmail.com'
!            FTP.BinaryTransfer           = 1
!            FTP.ChangeDir('/fromSB/crc')    !          = 'FromSB' ! RemoteFTP.CurrentDirectory
!            FTP.ControlPort              = 21
!           FTP.OpenNewControlConnection = 1
!            FTP.PutFile('FROM name', 'TO name')
!!        setcursor
!        !------------------------------------------------------------------
!-----------------------------------------------
!WriteDataCSV                     ROUTINE
!        !------------------------------------------------------------------
!        Add(RecordExport)
!        !DO WriteDataHTML
!        !------------------------------------------------------------------
!-----------------------------------------------
!WriteHeaderHTML         ROUTINE
!        !------------------------------------------------------------------
!        FREE(HTML_Queue)
!        TotalCharacters = 0
!
!        ADD(HTML_Queue)
!            CLEAR(HTML_Queue)
!
!            WriteTABLE(False)
!                WriteTR(False)
!                    !-- Header --------------------------------------------
!                    WriteTH( 'Voucher Number'  )
!                    WriteTH( 'Document Type'   )
!                    WriteTH( 'Description'     )
!                    WriteTH( 'Batch ID'        )
!                    WriteTH( 'Document Date'   )
!                    WriteTH( 'Creditor ID'     )
!                    WriteTH( 'Due Date'        )
!                    WriteTH( 'Currency ID'     )
!                    WriteTH( 'Document Number' )
!                    WriteTH( 'PO Number'       )
!                    !WriteTH( 'Tax Schedule ID' )
!                    WriteTH( 'Purchases'       )
!                    WriteTH( 'Tax'             )
!                    !-- Transaction ---------------------------------------
!                    WriteTH( 'GL Account'             )
!                    WriteTH( 'Distribution Type'      )
!                    WriteTH( 'Debit Amount'           )
!                    WriteTH( 'Credit Amount'          )
!                    WriteTH( 'Distribution Reference' )
!                    !-- VAT -----------------------------------------------
!                    WriteTH( 'Tax Detail ID' )
!                    WriteTH( 'Amount'        )
!                    WriteTH( 'Tax Amount'    )
!                    !------------------------------------------------------
!                WriteTR(True)
!        PUT(HTML_Queue)
!
!        TotalCharacters += LEN(CLIP(html:LINE))
!        !------------------------------------------------------------------
!
!WriteDataHTML         ROUTINE
!        !------------------------------------------------------------------
!        ADD(HTML_Queue)
!            CLEAR(HTML_Queue)
!
!            WriteTR(False)
!                !-- Header ------------------------------------------------
!                WriteTD( export:Voucher_Number  )
!                WriteTD( export:Document_Type   )
!                WriteTD( export:Description     )
!                WriteTD( export:Batch_ID        )
!                WriteTD( export:Document_Date   )
!                WriteTD( export:Creditor_ID     )
!                WriteTD( export:Due_Date        )
!                WriteTD( export:Currency_ID     )
!                ! Start Change 2052 BE(03/10/03)
!                !WriteTD( export:Document_Number )
!                ! End Change 2052 BE(03/10/03)
!                WriteTD( export:PO_Number       )
!                WriteTD( export:Purchases       )
!                WriteTD( export:Tax             )
!                !-- Transacton --------------------------------------------
!                WriteTD( export:GL_Account              )
!                WriteTD( export:Distribution_Type       )
!                WriteTD( export:Debit_Amount            )
!                WriteTD( export:Credit_Amount           )
!                WriteTD( export:Distribution_Reference  )
!                !-- VAT ---------------------------------------------------
!                WriteTD( export:Tax_Detail_ID           )
!                WriteTD( export:Amount                  )
!                WriteTD( export:Tax_Amount              )
!                ! Start Change 2052 BE(03/10/03)
!                WriteTD( export:F20_Unused      )
!                WriteTD( export:GP_Customer_Code )
!                ! End Change 2052 BE(03/10/03)
!                !----------------------------------------------------------
!            WriteTR(True)
!        PUT(HTML_Queue)
!
!        TotalCharacters += LEN(CLIP(html:LINE))
!        !------------------------------------------------------------------
!        IF IsMessageHTMLFull()
!            DO SendMessage
!        END !IF
!        !------------------------------------------------------------------
!
!WriteFooterHTML         ROUTINE
!        !------------------------------------------------------------------
!        ADD(HTML_Queue)
!            CLEAR(HTML_Queue)
!
!            WriteTABLE(True)
!        PUT(HTML_Queue)
!
!        TotalCharacters += LEN(CLIP(html:LINE))
!        !------------------------------------------------------------------
!-----------------------------------------------
CancelCheck                     routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()

        Case MessageEx('Are you sure you want to cancel?',LOC:ApplicationName,|
                       'Styles\question.ico','|&Yes|&No',1,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,LOC:MessageExTimeout)
            Of 1 ! &Yes Button
                tmp:cancel = 1
                CancelPressed = True
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess) * 100

      if percentprogress > 100
        percentprogress = 100
      elsIF percentprogress > 80
        recordstoprocess = 3 * recordstoprocess
      end
    end

    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
    end

    Display()

endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    IF window{Prop:AcceptAll} THEN EXIT.
    DISPLAY()
    !ForceRefresh = False
!-----------------------------------------------
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',1,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,LOC:MessageExTimeout)
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case MessageEx('Attempting to use Workshop Performance Report<10,13>'            & |
                '   without using ' & LOC:ApplicationName & '.<10,13>'                       & |
                '   Start ' & LOC:ApplicationName & ' and run the report from there.<10,13>',  |
                LOC:ApplicationName,                                                           |
                'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LOC:MessageExTimeout)
            Of 1 ! &OK Button
            END!Case MessageEx

            DO SetUpReportDetails ! 16 Apr 2003 John

           POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            !Assert(0,'<13,10>Fetch Error<13,10>')
            Case MessageEx('Unable to find your logged in user details.', |
                    LOC:ApplicationName,                                   |
                    'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LOC:MessageExTimeout)
                Of 1 ! &OK Button
            End!Case MessageEx

            POST(Event:CloseWindow)

           EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = use:Forename

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END !IF

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF
        !-----------------------------------------------
    EXIT
SetUpReportDetails          ROUTINE ! 16 Apr 2003 John
    DATA
RepType STRING('EXPORT REPORTS')
EXEName STRING('SBCR0044')
    CODE
        !-----------------------------------------------
        Access:REPEXTtp.Clearkey(rpt:ReportTypeKey)
            rpt:ReportType = RepType
        IF Access:REPEXTtp.Tryfetch(rpt:ReportTypeKey) <> Level:Benign
            !-------------------------------------------
            Access:REPEXTtp.Clearkey(rpt:ReportTypeKey)
                rpt:ReportType = RepType
            IF Access:REPEXTtp.TryInsert() <> Level:Benign
                Access:REPEXTtp.Close()

                EXIT
            END !IF
            !-------------------------------------------
        END !IF
        !-----------------------------------------------
        Access:REPEXTrp.Clearkey(rex:ReportNameKey)
            rex:ReportType = RepType
            rex:ReportName = CLIP(LOC:ProgramName) & '    (' & CLIP(EXEName) & ')'
        IF Access:REPEXTrp.Tryfetch(rex:ReportNameKey) <> Level:Benign
            !-------------------------------------------
            Access:REPEXTrp.Clearkey(rpt:ReportTypeKey)
                rex:ReportType = RepType
                rex:ReportName = CLIP(LOC:ProgramName) & '    (' & CLIP(EXEName) & ')'
                rex:EXEName    = EXEName & '.EXE'
            IF Access:REPEXTrp.TryInsert() <> Level:Benign
                Access:REPEXTrp.Close()

                EXIT
            END !IF
            !-------------------------------------------
        END !IF
        !-----------------------------------------------
        Access:REPEXTtp.Close()
        Access:REPEXTrp.Close()
        !-----------------------------------------------
    EXIT
GetFileName                             ROUTINE ! Generate default file name
    DATA
Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)
    CODE
        !-----------------------------------------------
        ! Generate default file name
        !
        !-----------------------------------------------
        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
        !
        IF CLIP(LOC:FileName) <> ''
            ! Already generated 
            EXIT
        END !IF

        DeskTopExists = False

        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        LOC:DesktopPath = Desktop
        !-----------------------------------------------
        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
        IF EXISTS(CLIP(ApplicationPath))
            LOC:Path      = CLIP(ApplicationPath) & '\'
            DeskTopExists = True

        ELSE
            Desktop  = CLIP(ApplicationPath)

            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!            ELSE
!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
            END !IF

            IF EXISTS(CLIP(ApplicationPath))
                LOC:Path      = CLIP(ApplicationPath) & '\'
                DeskTopExists = True
            END !IF

        END !IF
        !-----------------------------------------------
        IF DeskTopExists
            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
            Desktop         = CLIP(ApplicationPath)

            IF NOT EXISTS(CLIP(ApplicationPath))
                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!                ELSE
!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
                END !IF
            END !IF

            IF EXISTS(CLIP(ApplicationPath))
                LOC:Path = CLIP(ApplicationPath) & '\'
            END !IF
        END !IF
        !-----------------------------------------------
        !LOC:FileName     = SHORTPATH(CLIP(LOC:Path)) & CLIP(LOC:ProgramName) & ' ' & LEFT(FORMAT(TODAY(), @D12))& ' ' & LEFT(FORMAT(CLOCK(), @T5)) & '.tab'
        !LOC:HTMLFilename = SHORTPATH(CLIP(LOC:Path)) & CLIP(LOC:ProgramName) & ' ' & LEFT(FORMAT(TODAY(), @D12))& ' ' & LEFT(FORMAT(CLOCK(), @T5)) & '.html'
        LOC:FileName =  SHORTPATH(CLIP(LOC:Path)) & 'XYZrm' & FORMAT(batch_seq_no, @n04) & '.tab'
        !-----------------------------------------------
    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

        IF UPPER(glo:Password) <> 'SCHEDULER'
            OriginalPath = PATH()
            SETPATH(LOC:Path) ! Required for Win98
                !IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
                IF NOT FILEDIALOG('Save File', LOC:Filename, 'Text File (*.tab)|*.tab', |
                    FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)

                    LOC:Filename = ''
                END !IF
            SETPATH(OriginalPath)
        END !IF

        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT
SetButtons              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF (param:ENDInvoiceNumber > 0) AND (param:ENDInvoiceNumber < param:STARTInvoiceNumber)
            DISABLE(?OKButton)
        ELSE
            ENABLE(?OKButton)
        END!If
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
!PassViaClipboard                                            ROUTINE
!    DATA
!!ActiveWorkSheet CSTRING(20)
!!ActiveCell      CSTRING(20)
!    CODE
!        !-----------------------------------------------
!        IF clip:Saved = False
!            clip:OriginalValue = CLIPBOARD()
!            clip:Saved         = True
!        END !IF
!        !-----------------------------------------------
!        !clip:Value = 'abc' ! <09>def<09>xyz'
!        SETCLIPBOARD(CLIP(clip:Value))
!
!        Excel{'ActiveSheet.Paste'} ! Special'}
!        !-----------------------------------------------
!    EXIT
!ResetClipboard                                            ROUTINE
!    DATA
!    CODE
!        !-----------------------------------------------
!        IF clip:Saved = False
!            EXIT
!        END !IF
!
!        SETCLIPBOARD(clip:OriginalValue)
!        !-----------------------------------------------
!    EXIT
!-----------------------------------------------
Receivables ROUTINE
    DATA
TotalCost   REAL
TotalVAT    REAL
    CODE
        TotalCost = LabourCost + PartsCost
        TotalVAT = LabourVAT + PartsVAT

        IF (TotalCost <> 0.0 ) THEN

            !--DEBUG ---------------------------------------------------------------------------------
            IF (TotalCost <> inv:Total) THEN
                MESSAGE('Invoice: ' & inv:invoice_number & ' Costs: ' & TotalCost & ' - ' & inv:Total)
            END
            !--DEBUG ---------------------------------------------------------------------------------

            !--Header--------------------------------------------
            export:Voucher_Number      = inv:Invoice_Number
            export:Document_Type       = 1
            export:Description         = ''
            export:Batch_ID            = FORMAT(TODAY(), @d12) & '-' & batch_seq_no
            export:Document_Date       = LEFT(FORMAT(inv:Date_Created, @D06))
            export:Creditor_ID         = invaccQ:AccountNumber
            export:Due_Date            = LEFT(FORMAT(inv:Date_Created+30, @D06))
            export:Currency_ID         = GetCurrencyID()
            export:PO_Number           = ''
            export:Purchases           = FORMAT(inv:Total, @n10.02)
            export:Tax                 = FORMAT(TotalVAT, @n10.02)

            IF (PartsCost <> 0.0) THEN
                !--Receivables-----------------------------------
                gl:CostCentre = '0100' ! Parts
                gl:AccountCode = '1110' ! Parts
                export:GL_Account = GeneralLedgerCode
                export:Distribution_Type       = 9
                export:Debit_Amount            = 0
                export:Credit_Amount           = FORMAT(PartsCost, @n10.02)
                export:Distribution_Reference  = ''
                !-- VAT ------------------------------------------
                export:Tax_Detail_ID = ''
                export:Amount        = ''
                export:Tax_Amount = ''
                export:F20_Unused = ''
                export:GP_Customer_Code = ''
                !--Write Record------------------------------------
                Add(RecordExport)
            END

            IF (labourCost <> 0.0) THEN
                !--Receivables-----------------------------------
                gl:CostCentre = '0100' ! Labour
                gl:AccountCode = '1100' ! Labour
                export:GL_Account = GeneralLedgerCode
                export:Distribution_Type       = 9
                export:Debit_Amount            = 0
                export:Credit_Amount           = FORMAT(LabourCost, @n10.02)
                export:Distribution_Reference  = ''
                !-- VAT ------------------------------------------
                export:Tax_Detail_ID = ''
                export:Amount        = ''
                export:Tax_Amount = ''
                export:F20_Unused = ''
                export:GP_Customer_Code = ''
                !--Write Record------------------------------------
                Add(RecordExport)
            END

            !--Receivables-----------------------------------
            gl:CostCentre = '0000' ! Receivables
            gl:AccountCode = '6210' ! Receivables
            export:GL_Account = GeneralLedgerCode
            export:Distribution_Type       = 3
            export:Debit_Amount            = FORMAT(TotalCost + TotalVAT, @n10.02)
            export:Credit_Amount           = 0
            export:Distribution_Reference  = ''
            !-- VAT ------------------------------------------
            export:Tax_Detail_ID = ''
            export:Amount        = ''
            export:Tax_Amount = ''
            export:F20_Unused = ''
            export:GP_Customer_Code = ''
            !--Write Record------------------------------------
            Add(RecordExport)

            IF (PartsCost <> 0.0) THEN
                !--Receivables-----------------------------------
                gl:CostCentre = '0000' ! VAT
                gl:AccountCode = '6540' ! VAT
                export:GL_Account = GeneralLedgerCode
                export:Distribution_Type       = 13
                export:Debit_Amount            = 0
                export:Credit_Amount           = FORMAT(PartsVAT, @n10.02)
                export:Distribution_Reference  = ''
                !-- VAT ------------------------------------------
                export:Tax_Detail_ID = GetVatCode(inv:vat_rate_parts)
                export:Amount        = FORMAT(PartsCost, @n10.02)
                export:Tax_Amount = FORMAT(PartsVAT, @n10.02)
                export:F20_Unused = ''
                export:GP_Customer_Code = ''
                !--Write Record------------------------------------
                Add(RecordExport)
            END

            IF (LabourCost <> 0.0) THEN
                !--Receivables-----------------------------------
                gl:CostCentre = '0000' ! VAT
                gl:AccountCode = '6540' ! VAT
                export:GL_Account = GeneralLedgerCode
                export:Distribution_Type       = 13
                export:Debit_Amount            = 0
                export:Credit_Amount           = FORMAT(LabourVAT, @n10.02)
                export:Distribution_Reference  = ''
                !-- VAT ------------------------------------------
                export:Tax_Detail_ID = GetVatCode(inv:vat_rate_labour)
                export:Amount        = FORMAT(LabourCost, @n10.02)
                export:Tax_Amount = FORMAT(LabourVAT, @n10.02)
                export:F20_Unused = ''
                export:GP_Customer_Code = ''
                !--Write Record------------------------------------
                Add(RecordExport)
            END
        END
        EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('CRC_Invoice_Export')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?String8
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?CancelButton,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:ESTPARTS.Open
  Relate:REPEXTRP.Open
  Relate:RETSALES.Open
  Relate:VATCODE.Open
  Access:USERS.UseFile
  Access:SUBTRACC.UseFile
  Access:INVOICE.UseFile
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:JOBNOTES.UseFile
  Access:WARPARTS.UseFile
  Access:PARTS.UseFile
  Access:RETSTOCK.UseFile
  Access:STOCK.UseFile
  Access:TRADEACC.UseFile
  Access:REPEXTTP.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
      LOC:ProgramName   = 'CRC Invoice Export'               ! Job=2052      Cust=    Date=9 Sep 2002 John
      window{PROP:Text} = CLIP(LOC:ProgramName)
      ?Tab1{PROP:Text}  = CLIP(LOC:ProgramName) & ' Criteria'
      !--------------------------------------------------------------
      Excel:Visible     = False ! INTEC = ON
      !--------------------------------------------------------------
      LOC:LastInvoiceNumber     = GetInvoiceNumber()
      param:InvoiceNumber       = LOC:LastInvoiceNumber
  
      param:STARTInvoiceNumber  = (LOC:LastInvoiceNumber + 1)
      param:ENDInvoiceNumber    = 0
  
      DO SetButtons
      !--------------------------------------------------------------
      DO GetUserName
      
      IF UPPER(glo:Password) = 'SCHEDULER'
          DISABLE(?OKButton)
          LOC:MessageExTimeout = 50
          DO OKButton_Pressed
      ELSE
          LOC:MessageExTimeout = 0
      END !IF
      !--------------------------------------------------------------
      batch_seq_no = GETINI('Receivables','BatchNumber',0,CLIP(PATH())&'\SBCR0044.INI')
      batch_seq_no += 1
      PUTINI('Receivables','BatchNumber',batch_seq_no,CLIP(PATH())&'\SBCR0044.INI')
  Do RecolourWindow
                                               ! Generated by NetTalk Extension (Start)
  FTPData.ControlConnection &= FTP ! FTP needs _2_ objects. Check the settings on the "Settings" Tab
  FTPData.init(NET:SimpleServer)
  if FTPData.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
                                               ! Generated by NetTalk Extension (Start)
  FTP.DataConnection &= FTPData ! FTP needs _2_ objects. Check the settings on the "Settings" Tab
  FTP.init(NET:SimpleClient)
  if FTP.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
  ! support for CPCS
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  FTPData.Kill()                              ! Generated by NetTalk Extension
  FTP.Kill()                              ! Generated by NetTalk Extension
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:ESTPARTS.Close
    Relate:REPEXTRP.Close
    Relate:RETSALES.Close
    Relate:VATCODE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      DO OKButton_Pressed
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    FTPData.TakeEvent()                 ! Generated by NetTalk Extension
    FTP.TakeEvent()                 ! Generated by NetTalk Extension
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

AppendString    PROCEDURE(IN:First, IN:Second, IN:Separator)!STRING
    CODE
        !------------------------------------------------------------------
        IF CLIP(IN:First) = ''
            RETURN IN:Second

        ELSIF CLIP(IN:Second) = ''
            RETURN IN:First
        END ! IF

        RETURN CLIP(IN:First) & IN:Separator & CLIP(IN:Second)
        !------------------------------------------------------------------
DateToString PROCEDURE(IN:Date)!STRING
    CODE
        !------------------------------------------------------------------
        ! Start Change 2052 BE(03/10/03)
        !RETURN LEFT(FORMAT(IN:Date, @D8))
        RETURN LEFT(FORMAT(IN:Date, @D06))
        ! End Change 2052 BE(03/10/03)
        !------------------------------------------------------------------
GetCurrencyID   PROCEDURE()!STRING
DefaultCurrencyID STRING('GBP')
    CODE
        !------------------------------------------------------------------
        CASE inv:AccountType
        OF 'MAI'
            IF NOT LoadTRADEACC(inv:Account_Number)
                ! NULL
            ELSIF tra:EuroApplies
                RETURN 'EUR'
            END !IF
        OF 'SUB'
            IF NOT LoadSUBTRACC(inv:Account_Number)
                ! NULL
            ELSIF sub:EuroApplies
                RETURN 'EUR'
            END !IF
        ELSE
            ! NULL
        END !IF

        RETURN LOC:DefaultCurrencyID
        !------------------------------------------------------------------
GetCustomerName PROCEDURE()!STRING
TEMP STRING(100)
    CODE
        !------------------------------------------------------------------
        ! STRING(30)!(job:Title+job:Initials+job:Surname) / job:Company_Name
        !------------------------------------------------------------------
        TEMP = AppendString( job:Title, job:Initial, ' ')
        TEMP = AppendString(      TEMP, job:Surname, ' ')

        IF CLIP(TEMP) = ''
            RETURN job:Company_Name
        END !IF

        RETURN CLIP(TEMP)
        !------------------------------------------------------------------
GetExpectedShipDate PROCEDURE()!STRING(2)
    CODE
        !------------------------------------------------------------------
        ! STRING('Delivery Not Known')!put "Delivery Not Known" if job:Current_Status IN("Awaiting Spares", "Spares Requested")
        !
        CASE job:Current_Status
        OF 'Awaiting Spares' OROF 'Spares Requested'
            RETURN 'Delivery Not Known'
        ELSE
            RETURN ''
        END !CASE
        !------------------------------------------------------------------
GetTransactionCode PROCEDURE()!STRING(2)
    CODE
        !------------------------------------------------------------------
        CASE job:Current_Status
        OF 'EXCHNGE UNIT SENT'      ! 'EXCH. UNIT DESPATCHED'
            RETURN 'PR'
        OF 'ORIGINAL UNIT RETURNED' !
            RETURN 'RC'
        END !CASE

        IF      job:Unit_Type <> 'ACCESSORY'
            RETURN ''
        ELSIF job:Charge_Type <> 'EXCHANGE'
            RETURN ''
        ELSE
            RETURN 'AC'
        END !IF
        !------------------------------------------------------------------
GetInvoiceNumber        PROCEDURE()! STRING
    CODE
        !------------------------------------------------------------------
        RETURN GETINI( 'Parameters',  'InvoiceNumber',                                  '0', '.\sbcr0043.ini')
        !------------------------------------------------------------------

GetEmailServer            PROCEDURE()! STRING
    CODE
        !------------------------------------------------------------------
        RETURN GETINI( 'Parameters',   'EmailServer',                      '192.168.0.229', '.\sbcr0043.ini')
        !------------------------------------------------------------------

GetEmailPort              PROCEDURE()! LONG
    CODE
        !------------------------------------------------------------------
        RETURN GETINI( 'Parameters',     'EmailPort',                                 '25', '.\sbcr0043.ini')
        !------------------------------------------------------------------

GetEmailFrom              PROCEDURE()! STRING
    CODE
        !------------------------------------------------------------------
        RETURN GETINI( 'Parameters',     'EmailFrom',   'j.griffiths@pccontrolsystems.com', '.\sbcr0043.ini')
        !------------------------------------------------------------------

GetEmailTo                PROCEDURE()! STRING
    CODE
        !------------------------------------------------------------------
        RETURN GETINI( 'Parameters',       'EmailTo',   'j.griffiths@pccontrolsystems.com', '.\sbcr0043.ini')
        !------------------------------------------------------------------

GetVATRate  PROCEDURE(IN:VATCode)! REAL
    CODE
        vatq:VATCode = IN:VATCode
        GET(VATCode_Queue, +vatq:VATCode)
        CASE ERRORCODE()
        OF 00
            RETURN vatq:VAT_Rate
        OF 30
            Access:VATCODE.ClearKey(vat:Vat_code_Key)
                vat:VAT_Code = IN:VATCode
            SET(vat:Vat_code_Key, vat:Vat_code_Key)

            IF Access:VATCODE.NEXT() = Level:Benign
                vatq:VAT_Rate = vat:VAT_Rate / 100
            ELSE
                vatq:VAT_Rate = 0.0
            END !IF

            vatq:VATCode = IN:VATCode
            ADD(VATCode_Queue, +vatq:VATCode)

            RETURN vatq:VAT_Rate
        ELSE
            CancelPressed = True

            RETURN 0.0
        END !CASE
IsMessageHTMLFull   PROCEDURE()! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        IF LOC:MaxHTMLSize = 0
           LOC:MaxHTMLSize = (SIZE(EmailMessageHTML) * 0.95)
           !message('(SIZE(EmailMessageHTML) * 0.8)="' & LOC:MaxHTMLSize & '"')
           !LOC:MaxHTMLSize = (16384 * 0.8)
        END !IF

        IF TotalCharacters > LOC:MaxHTMLSize
            RETURN True
        ELSE
            RETURN False
        END !IF
        !------------------------------------------------------------------
LoadESTPARTS        PROCEDURE(IN:JobNumber, IN:Init)! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        IF IN:Init = True
            Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
                epr:Ref_Number = IN:JobNumber
            SET(epr:Part_Number_Key, epr:Part_Number_Key)
        END !IF

        IF Access:ESTPARTS.TryNext() <> Level:Benign
            epr:Ref_Number = ''
            RETURN False
        END !IF

        IF NOT par:Ref_Number = IN:JobNumber
            epr:Ref_Number = ''
            RETURN False
        END !IF

        RETURN True
        !------------------------------------------------------------------
LoadJOBS        PROCEDURE(IN:JobNumber)! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = IN:JobNumber
        SET(job:Ref_Number_Key, job:Ref_Number_Key)

        IF Access:JOBS.TryNext() <> Level:Benign
            job:Ref_Number = ''
            RETURN False
        END !IF

        IF NOT job:Ref_Number = IN:JobNumber
            job:Ref_Number = ''
            RETURN False
        END !IF

        RETURN True
        !------------------------------------------------------------------

LoadJOBSFromInvoice        PROCEDURE(IN:InvoiceNumber, IN:Init)! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        IF IN:Init = True
            ! InvoiceNumberKey         KEY(job:Invoice_Number),DUP,NOCASE
            !
            Access:JOBS.ClearKey(job:InvoiceNumberKey)
                job:Invoice_Number = IN:InvoiceNumber
            SET(job:InvoiceNumberKey, job:InvoiceNumberKey)
        END !IF

        IF Access:JOBS.TryNext() <> Level:Benign
            job:Ref_Number = ''
            RETURN False
        END !IF

        IF NOT job:Invoice_Number = IN:InvoiceNumber
            job:Ref_Number = ''
            RETURN False
        END !IF

        RETURN True
        !------------------------------------------------------------------

LoadJOBSE        PROCEDURE(IN:JobNumber)! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
            jbn:RefNumber = IN:JobNumber
        SET(jbn:RefNumberKey, jbn:RefNumberKey)

        IF Access:JOBNOTES.TryNext() <> Level:Benign
            jbn:RefNumber = ''
            RETURN False
        END !IF

        IF NOT jbn:RefNumber = IN:JobNumber
            jbn:RefNumber = ''
            RETURN False
        END !IF

        RETURN True
        !------------------------------------------------------------------

LoadPARTS        PROCEDURE(IN:JobNumber, IN:Init)! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        IF IN:Init = True
            Access:PARTS.ClearKey(par:Part_Number_Key)
                par:Ref_Number = IN:JobNumber
            SET(par:Part_Number_Key, par:Part_Number_Key)
        END !IF

        IF Access:PARTS.TryNext() <> Level:Benign
            par:Ref_Number = ''
            RETURN False
        END !IF

        IF NOT par:Ref_Number = IN:JobNumber
            par:Ref_Number = ''
            RETURN False
        END !IF

        RETURN True
        !------------------------------------------------------------------
LoadSUBTRACC    PROCEDURE(IN:AccountNumber)! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
!        IF sub:Account_Number = IN:AccountNumber
!            RETURN True
!        END !IF

        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
            sub:Account_Number = IN:AccountNumber
        SET(sub:Account_Number_Key, sub:Account_Number_Key)

        IF Access:SUBTRACC.TryNext() <> Level:Benign
            sub:Account_Number = ''
            RETURN False
        END !IF

        IF NOT sub:Account_Number = IN:AccountNumber
            sub:Account_Number = ''
            RETURN False
        END !IF

        RETURN True
        !------------------------------------------------------------------

LoadTRADEACC    PROCEDURE(IN:AccountNumber)! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
!        IF tra:Account_Number = IN:AccountNumber
!            RETURN True
!        END !IF

        Access:TRADEACC.ClearKey(sub:Account_Number_Key)
            tra:Account_Number = IN:AccountNumber
        SET(sub:Account_Number_Key, sub:Account_Number_Key)

        IF Access:TRADEACC.TryNext() <> Level:Benign
            tra:Account_Number = ''
            RETURN False
        END !IF

        IF NOT tra:Account_Number = IN:AccountNumber
            tra:Account_Number = ''
            RETURN False
        END !IF

        RETURN True
        !------------------------------------------------------------------

LoadRETSALES                   PROCEDURE(IN:InvoiceNumber, IN:Init)! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        IF IN:Init = True
            ! Invoice_Number_Key       KEY(ret:Invoice_Number),DUP,NOCASE
            !
            Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
                ret:Invoice_Number = IN:InvoiceNumber
            SET(ret:Invoice_Number_Key, ret:Invoice_Number_Key)
        END !IF

        IF Access:RETSALES.TryNext() <> Level:Benign
            ret:Ref_Number = ''
            RETURN False
        END !IF

        IF NOT ret:Invoice_Number = IN:InvoiceNumber
            ret:Ref_Number = ''
            RETURN False
        END !IF

        RETURN True
        !------------------------------------------------------------------


LoadRETSTOCK        PROCEDURE(IN:SalesNumber, IN:Init)! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        IF IN:Init = True
            ! Despatched_Only_Key      KEY( res:Ref_Number, res:Despatched 
            !
            Access:RETSTOCK.ClearKey(res:Despatched_Only_Key)
                res:Ref_Number = IN:SalesNumber
            SET(res:Despatched_Only_Key, res:Despatched_Only_Key)
        END !IF

        IF Access:RETSTOCK.TryNext() <> Level:Benign
            res:Ref_Number = ''
            RETURN False
        END !IF

        IF NOT res:Ref_Number = IN:SalesNumber
            res:Ref_Number = ''
            RETURN False
        END !IF

        RETURN True
        !------------------------------------------------------------------


LoadSTOCK        PROCEDURE(IN:PartRefNumber)! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        ! Ref_Number_Key KEY( sto:Ref_Number ),NOCASE,PRIMARY
        !
        Access:STOCK.ClearKey(sto:Ref_Number_Key)
            sto:Ref_Number = IN:PartRefNumber
        SET(sto:Ref_Number_Key, sto:Ref_Number_Key)

        IF Access:STOCK.TryNext() <> Level:Benign
            sto:Ref_Number = ''
            RETURN False
        END !IF

        IF NOT sto:Ref_Number = IN:PartRefNumber
            sto:Ref_Number = ''
            RETURN False
        END !IF

        RETURN True
        !------------------------------------------------------------------


LoadWARPARTS        PROCEDURE(IN:JobNumber, IN:Init)! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        IF IN:Init = True
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                wpr:Ref_Number = IN:JobNumber
            SET(wpr:Part_Number_Key, wpr:Part_Number_Key)
        END !IF

        IF Access:WARPARTS.TryNext() <> Level:Benign
            wpr:Ref_Number = ''
            RETURN False
        END !IF

        IF NOT wpr:Ref_Number = IN:JobNumber
            wpr:Ref_Number = ''
            RETURN False
        END !IF

        RETURN True
        !------------------------------------------------------------------
NumberToBool    PROCEDURE(IN:Number)! STRING
    CODE
        !------------------------------------------------------------------
        IF IN:Number = True
            RETURN '1' ! True'
        ELSE
            RETURN '0' ! False'
        END !IF
        !------------------------------------------------------------------
SetInvoiceAccount   PROCEDURE(IN:AccountNumber, IN:AccountType)
    CODE
        !------------------------------------------------------------------
        invaccQ:AccountNumber = IN:AccountNumber
        GET(InvoiceAccount_Queue, +invaccQ:AccountNumber)
        CASE ERRORCODE()
        OF 00
            IF    invaccQ:AccountType = 'MAI' AND IN:AccountType <> 'MAI'
                invaccQ:AccountType = 'BOT'
                PUT(InvoiceAccount_Queue)

            ELSIF invaccQ:AccountType = 'SUB' AND IN:AccountType <> 'SUB'
                invaccQ:AccountType = 'BOT'
                PUT(InvoiceAccount_Queue)

            END !IF
        OF 30
            IF    IN:AccountType = 'MAI' AND LoadTRADEACC(IN:AccountNumber)
                invaccQ:AccountNumber = IN:AccountNumber
                invaccQ:AccountType   = IN:AccountType
                invaccQ:CountryCode   = '03'
                invaccQ:EuroApplies   = tra:EuroApplies

                invaccQ:LabourVATCode = tra:Labour_VAT_Code
                invaccQ:PartsVATCode  = tra:Parts_VAT_Code
                invaccQ:RetailVATCode = tra:Retail_VAT_Code

                invaccQ:LabourVATRate = GetVATRate(tra:Labour_VAT_Code)
                invaccQ:PartsVATRate  = GetVATRate(tra:Parts_VAT_Code)
                invaccQ:RetailVATRate = GetVATRate(tra:Retail_VAT_Code)

            ELSIF IN:AccountType = 'SUB' AND LoadSUBTRACC(IN:AccountNumber)
                invaccQ:AccountNumber = IN:AccountNumber
                invaccQ:AccountType   = IN:AccountType
                invaccQ:CountryCode   = '03'

                invaccQ:EuroApplies   = sub:EuroApplies
                invaccQ:LabourVATCode = sub:Labour_VAT_Code
                invaccQ:PartsVATCode  = sub:Parts_VAT_Code

                invaccQ:RetailVATCode = sub:Retail_VAT_Code
                invaccQ:LabourVATRate = GetVATRate(sub:Labour_VAT_Code)
                invaccQ:PartsVATRate  = GetVATRate(sub:Parts_VAT_Code)
                invaccQ:RetailVATRate = GetVATRate(sub:Retail_VAT_Code)

            ELSE
                invaccQ:AccountNumber = IN:AccountNumber
                invaccQ:AccountType   = IN:AccountType
                invaccQ:CountryCode   = '03'
                invaccQ:EuroApplies   = False

                invaccQ:LabourVATCode = def:VAT_Rate_Labour
                invaccQ:PartsVATCode  = def:VAT_Rate_Parts
                invaccQ:RetailVATCode = '' ! def:VAT_Rate_Retail

                invaccQ:LabourVATRate = GetVATRate(def:VAT_Rate_Labour)
                invaccQ:PartsVATRate  = GetVATRate(def:VAT_Rate_Parts)
                invaccQ:RetailVATRate = 0.0 ! GetVATRate(def:VAT_Rate_Retail)

            END !IF
            !--------------------------------------------------------------
            ! No real meathod to differentiate
            ! This is a bug awaiting to BITE.
            !
            IF invaccQ:EuroApplies
                invaccQ:CountryCode   = '02'
            END !IF
            !--------------------------------------------------------------
            ADD(InvoiceAccount_Queue, +invaccQ:AccountNumber)
        ELSE
            CancelPressed = True

            RETURN
        END !CASE
        !------------------------------------------------------------------
SetInvoiceNumber    PROCEDURE()
    CODE
        !------------------------------------------------------------------
        IF    (param:STARTInvoiceNumber > LOC:LastInvoiceNumber+1)
            ! Attempt to skip some invoices, ignore InvoiceNumber update
            RETURN
        ELSIF (param:InvoiceNumber < LOC:LastInvoiceNumber+1)
            ! Failed to print any new invoices, ignore InvoiceNumber update
            RETURN
        END !IF

        PUTINI( 'Parameters', 'InvoiceNumber', param:InvoiceNumber, '.\sbcr0043.ini')
        !------------------------------------------------------------------

SetEmailServer    PROCEDURE()!
    CODE
        !------------------------------------------------------------------
        PUTINI( 'Parameters',  'EmailServer',     param:EmailServer, '.\sbcr0043.ini')
        !------------------------------------------------------------------

SetEmailPort      PROCEDURE()!
    CODE
        !------------------------------------------------------------------
        PUTINI( 'Parameters',    'EmailPort',       param:EmailPort, '.\sbcr0043.ini')
        !------------------------------------------------------------------

SetEmailFrom      PROCEDURE()!
    CODE
        !------------------------------------------------------------------
        PUTINI( 'Parameters',    'EmailFrom',       param:EmailFrom, '.\sbcr0043.ini')
        !------------------------------------------------------------------

SetEmailTo        PROCEDURE()!
    CODE
        !------------------------------------------------------------------
        PUTINI( 'Parameters',      'EmailTo',         param:EmailTo, '.\sbcr0043.ini')
        !------------------------------------------------------------------
WriteTABLE PROCEDURE(IN:Close)
    CODE
        !------------------------------------------------------------------
        IF IN:Close = True
            html:LINE = CLIP(html:LINE) & ('</TABLE><13,10>')

            LinePrint('</TABLE>', LOC:HTMLFilename)
        ELSE
            html:LINE = CLIP(html:LINE) & ('<TABLE BORDER="1"><13,10>')

            LinePrint('<TABLE BORDER="1">', LOC:HTMLFilename)
        END !IF
        !------------------------------------------------------------------
WriteTD PROCEDURE(IN:Value)
    CODE
        !------------------------------------------------------------------
        html:LINE = CLIP(html:LINE) & '<TD>' & CLIP(IN:Value) & '</TD>'

        LinePrint('<TD>' & CLIP(IN:Value) & '</TD>', LOC:HTMLFilename)
        !------------------------------------------------------------------
WriteTH PROCEDURE(IN:Value)
    CODE
        !------------------------------------------------------------------
        html:LINE = CLIP(html:LINE) & '<TH>' & CLIP(IN:Value) & '</TH>'

        LinePrint('<TH>' & CLIP(IN:Value) & '</TH>', LOC:HTMLFilename)
        !------------------------------------------------------------------
WriteTR PROCEDURE(IN:Close)
    CODE
        !------------------------------------------------------------------
        IF IN:Close = True
            html:LINE = CLIP(html:LINE) & '</TR><13,10>'

            LinePrint('</TR>', LOC:HTMLFilename)
        ELSE
            html:LINE = CLIP(html:LINE) & '<TR>'

            LinePrint('<TR>', LOC:HTMLFilename)
        END !IF
        !------------------------------------------------------------------
WriteDebug  PROCEDURE( IN:Message )
    CODE
        !------------------------------------------------------------------
        IF debug:Active = False
            RETURN
        END !IF

        debug:Count += 1

        PUTINI(CLIP(LOC:ProgramName), debug:Count, IN:Message, 'C:\Debug.ini')
        !------------------------------------------------------------------
GetVatCode  PROCEDURE(vatrate)
vatstring       STRING(15)
countrystring   STRING(3)
    CODE
    IF (gl:CompanyNumber = '03') THEN
        countrystring = 'UKS'
    ELSE
        countrystring = 'IRS'
    END
    IF (vatrate = 0.0) THEN
        vatstring = countrystring & 'ZERO'
    ELSE
        vatstring = countrystring & CLIP(LEFT(vatrate)) & '%'
    END
    RETURN vatstring
