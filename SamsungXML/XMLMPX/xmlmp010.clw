

   MEMBER('xmlmpx.clw')                               ! This is a MEMBER module

                     MAP
                       INCLUDE('XMLMP010.INC'),ONCE        !Local module procedure declarations
                     END


ConvertTrueFalse     PROCEDURE  (string argValue)     ! Declare Procedure
  CODE
    if (upper(argValue) = 'TRUE') then
        return true
    end

    return false
