

   MEMBER('xmlmpx.clw')                               ! This is a MEMBER module

                     MAP
                       INCLUDE('XMLMP002.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('XMLMP005.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('XMLMP014.INC'),ONCE        !Req'd for module callout resolution
                     END


ImportJobs           PROCEDURE  (string aDirectory, string aUniqueId) ! Declare Procedure
ImportPathname       string(FILE:MaxFilePath)
ImportJobs    file,driver('BASIC','/COMMA=124 /ALWAYSQUOTE=OFF'),pre(imp)
Record           record
ID               string(16)     ! Order Number
CustCTN          string(50)     ! Contact Number
QuoteValue       string(16)     ! Job Fault Code 3
ModelID          string(50)     ! Model
QuoteQ1          string(5)      ! Job Fault Code 4
QuoteQ2          string(5)      ! Job Fault Code 5
QuoteQ3          string(5)      ! Job Fault Code 6
QuoteACC1        string(5)      ! Job Fault Code 8
QuoteACC2        string(5)      ! Job Fault Code 9
QuoteACC3        string(5)      ! Job Fault Code 10
QuoteACC4        string(5)      ! Job Fault Code 11
QuoteACC5        string(5)      ! Job Fault Code 12
CustTitle        string(50)     ! Title
CustFirstName    string(50)     ! Initials
CustLastName     string(50)     ! Surname
CustAdd1         string(50)     ! Customer Address Line 1
CustAdd2         string(50)     ! Customer Address Line 2
CustAdd3         string(50)     ! Customer Address Line 3
CustAdd4         string(50)     ! Customer Address Line 4
CustPostcode     string(50)     ! Customer Postcode
CustRef          string(50)     ! Insurer ref Number
CustNetwork      string(50)     ! Trade Account
EnvLetter        string(50)     ! Job Fault Code 13
InspType         string(50)     ! Job Turnaround
EnvType          string(50)     ! Job Fault Code 14
EnvCount         string(16)     ! No Imported
                 end
              end
  CODE
    ! Initialise Variables
    ImportPathname = clip(aDirectory) & '\IMP' & clip(aUniqueId) & '.csv'




    ! Run Import program to create csv file
    code# =  RunDOS(clip(longpath()) & '\webmpx.exe -IMPORT "' & clip(ImportPathname) & '"', true)


    ! Copy jobs to XML D/B
    ImportJobs{prop:name} = ImportPathname
    open(ImportJobs, ReadOnly)
    if not errorcode() then
        set(ImportJobs,0)
        loop
            next(ImportJobs)
            if errorcode()
                Break
            end

            ! check for duplicate MPX ID
            mpxid# = deformat(imp:ID)
            envcount# = deformat(imp:EnvCount)
            dup# = false

            access:mpxjobs.clearkey(mxj:IDKey)
            mxj:ID = mpxid#
            if access:mpxjobs.fetch(mxj:IDKey) = level:benign then
               dup# = true
               if envcount# > 0 then
                    mxj:ImportDate        = today()
                    mxj:ImportTime        = clock()

                    mxj:Record_State      = RS_NEW_IMPORT
                    mxj:ReportCode        = MPX_NONE
                    mxj:Exception_Code    = XC_NONE
                    mxj:Exception_desc    = ''

                    mxj:CustCTN           = upper(imp:CustCTN)
                    mxj:QuoteValue        = deformat(imp:QuoteValue)
                    mxj:ModelID           = upper(imp:ModelID)
                    mxj:QuoteQ1           = CompareStrings(imp:QuoteQ1, 'TRUE')
                    mxj:QuoteQ2           = CompareStrings(imp:QuoteQ2, 'TRUE')
                    mxj:QuoteQ3           = CompareStrings(imp:QuoteQ3, 'TRUE')
                    mxj:QuoteACC1         = CompareStrings(imp:QuoteACC1, 'TRUE')
                    mxj:QuoteACC2         = CompareStrings(imp:QuoteACC2, 'TRUE')
                    mxj:QuoteACC3         = CompareStrings(imp:QuoteACC3, 'TRUE')
                    mxj:QuoteACC4         = CompareStrings(imp:QuoteACC4, 'TRUE')
                    mxj:QuoteACC5         = CompareStrings(imp:QuoteACC5, 'TRUE')
                    mxj:CustTitle         = upper(imp:CustTitle)
                    mxj:CustFirstName     = upper(imp:CustFirstName)
                    mxj:CustLastName      = upper(imp:CustLastName)
                    mxj:CustAdd1          = upper(imp:CustAdd1)
                    mxj:CustAdd2          = upper(imp:CustAdd2)
                    mxj:CustAdd3          = upper(imp:CustAdd3)
                    mxj:CustAdd4          = upper(imp:CustAdd4)
                    mxj:CustPostcode      = upper(imp:CustPostcode)
                    mxj:CustRef           = upper(imp:CustRef)
                    mxj:CustNetwork       = upper(imp:CustNetwork)
                    mxj:EnvLetter         = upper(imp:EnvLetter)
                    mxj:InspType          = upper(imp:InspType)
                    mxj:EnvType           = upper(imp:EnvType)
                    mxj:EnvCount          = envcount#

                    access:mpxjobs.update()
                end
             end

            if (dup# = false) or (envcount# = 0) then

                ! Create New Job Record
                if access:mpxjobs.primerecord() = level:benign then

                    mxj:ImportDate        = today()
                    mxj:ImportTime        = clock()

                    if dup# then
                        mxj:Record_State      = RS_EXCEPTION
                        mxj:Exception_Code    = XC_DUPLICATE
                    else
                        mxj:Record_State      = RS_NEW_IMPORT
                        mxj:Exception_Code    = XC_NONE
                    end
                    mxj:Exception_desc    = ''

                    mxj:ReportCode        = MPX_NONE

                    mxj:ID                = deformat(imp:ID)
                    mxj:CustCTN           = upper(imp:CustCTN)
                    mxj:QuoteValue        = deformat(imp:QuoteValue)
                    mxj:ModelID           = upper(imp:ModelID)
                    mxj:QuoteQ1           = CompareStrings(imp:QuoteQ1, 'TRUE')
                    mxj:QuoteQ2           = CompareStrings(imp:QuoteQ2, 'TRUE')
                    mxj:QuoteQ3           = CompareStrings(imp:QuoteQ3, 'TRUE')
                    mxj:QuoteACC1         = CompareStrings(imp:QuoteACC1, 'TRUE')
                    mxj:QuoteACC2         = CompareStrings(imp:QuoteACC2, 'TRUE')
                    mxj:QuoteACC3         = CompareStrings(imp:QuoteACC3, 'TRUE')
                    mxj:QuoteACC4         = CompareStrings(imp:QuoteACC4, 'TRUE')
                    mxj:QuoteACC5         = CompareStrings(imp:QuoteACC5, 'TRUE')
                    mxj:CustTitle         = upper(imp:CustTitle)
                    mxj:CustFirstName     = upper(imp:CustFirstName)
                    mxj:CustLastName      = upper(imp:CustLastName)
                    mxj:CustAdd1          = upper(imp:CustAdd1)
                    mxj:CustAdd2          = upper(imp:CustAdd2)
                    mxj:CustAdd3          = upper(imp:CustAdd3)
                    mxj:CustAdd4          = upper(imp:CustAdd4)
                    mxj:CustPostcode      = upper(imp:CustPostcode)
                    mxj:CustRef           = upper(imp:CustRef)
                    mxj:CustNetwork       = upper(imp:CustNetwork)
                    mxj:EnvLetter         = upper(imp:EnvLetter)
                    mxj:InspType          = upper(imp:InspType)
                    mxj:EnvType           = upper(imp:EnvType)
                    mxj:EnvCount          = envcount#

!                    mxj:CustTitle         = 'MR'          ! ****************** DEBUG / TEST / REMOVE ****************
!                    mxj:CustFirstName     = 'John'       ! ****************** DEBUG / TEST / REMOVE ****************
!                    mxj:CustLastName      = 'Doe'       ! ****************** DEBUG / TEST / REMOVE ****************
!                    mxj:CustNetwork       = '4NET'                        ! ****************** DEBUG / TEST / REMOVE ****************
!                    mxj:InspType          = '10 SAME DAY'     ! ****************** DEBUG / TEST / REMOVE ****************

                    if access:mpxjobs.insert() <> level:benign
                        access:mpxjobs.cancelautoinc()
                    end
                 end
             end

        end

        close(ImportJobs)

        remove(ImportPathname) 

    end







    ! import jobs to ServiceBase
    access:mpxjobs.clearkey(mxj:StateJobNoKey)
    mxj:Record_State = RS_NEW_IMPORT
    set(mxj:StateJobNoKey, mxj:StateJobNoKey)
    loop while access:mpxjobs.next() = level:benign

        if mxj:Record_State <> RS_NEW_IMPORT then  
            break
        end

        ! validate record
        if mxj:CustLastName = '' then
           mxj:Record_State = RS_EXCEPTION
           mxj:Exception_Code = BOR(mxj:Exception_Code, XC_MISSING_FIELDS)
           if (mxj:Exception_Desc <> '') then
               mxj:Exception_Desc = clip(mxj:Exception_Desc) & '<13,10>'
           end
           mxj:Exception_Desc = clip(mxj:Exception_Desc) & 'Name field Missing'
        end

        if mxj:CustAdd1 = '' and mxj:CustAdd2 = '' and mxj:CustAdd3 = '' and mxj:CustAdd4 = '' then
           mxj:Record_State = RS_EXCEPTION
           mxj:Exception_Code = BOR(mxj:Exception_Code, XC_MISSING_FIELDS)
           if (mxj:Exception_Desc <> '') then
               mxj:Exception_Desc = clip(mxj:Exception_Desc) & '<13,10>'
           end
           mxj:Exception_Desc = clip(mxj:Exception_Desc) & 'Address field Missing'
        end

        if mxj:ID = '' then
           mxj:Record_State = RS_EXCEPTION
           mxj:Exception_Code = BOR(mxj:Exception_Code, XC_MISSING_FIELDS)
           if (mxj:Exception_Desc <> '') then
               mxj:Exception_Desc = clip(mxj:Exception_Desc) & '<13,10>'
           end
           mxj:Exception_Desc = clip(mxj:Exception_Desc) & 'MPX ID field Missing'
        end

        ! Validate Model No - XC_MODEL
        access:modelnum.clearkey(mod:Manufacturer_key)
        mod:manufacturer = 'MPX'
        mod:Model_Number = mxj:ModelId
        if access:modelnum.fetch(mod:Manufacturer_key) <> Level:Benign then
            mxj:Record_State = RS_EXCEPTION
            mxj:Exception_Code = BOR(mxj:Exception_Code, XC_MODEL)
        end

        ! Validate Inspection Type (job turnaround) - XC_INSPECTION_TYPE
        access:turnarnd.clearkey(tur:turnaround_time_key)
        tur:turnaround_time = mxj:InspType
        IF (access:turnarnd.fetch(tur:turnaround_time_key) <> Level:Benign) THEN
            mxj:Record_State = RS_EXCEPTION
            mxj:Exception_Code = BOR(mxj:Exception_Code, XC_INSPECTION_TYPE)
        END

        ! Validate Trade Account - XC_ACCOUNT_NO
        access:mpxdefs.clearkey(mxd:AccountNoKey)
        mxd:AccountNo = mxj:CustNetwork
        if access:mpxdefs.fetch(mxd:AccountNoKey) <> level:benign then
            mxj:Record_State = RS_EXCEPTION
            mxj:Exception_Code = BOR(mxj:Exception_Code, XC_ACCOUNT_NO)
        end

        if mxj:Record_State = RS_NEW_IMPORT

            ! Check if record exists
            access:jobs.clearkey(job:AccOrdNoKey)
            job:Account_Number = mxj:CustNetwork
            job:Order_Number = mxj:ID
            if access:jobs.fetch(job:AccOrdNoKey) = level:benign then
                if mxj:EnvCount > 0 then
                    ! Envelope Count update
                    GetStatus(mxd:EnvCountStatus[1 : 3], 1, 'JOB')
                    access:jobs.update();
                    mxj:Record_State = RS_ACTIVE_JOB
                else
                    mxj:Record_State = RS_EXCEPTION
                    mxj:Exception_Code = BOR(mxj:Exception_Code, XC_DUPLICATE)
                end

            else

                if access:jobs.primerecord() = level:benign then

                    job:date_booked = today()
                    job:time_booked = clock()
                    job:who_booked = mxd:UserCode
                    job:Location = mxd:Location
                    job:EDI = 'EDI'
                    GetStatus(mxd:Status[1 : 3], 1, 'JOB')

                    access:trantype.clearkey(trt:Transit_Type_Key)
                    trt:Transit_Type = mxd:TransitType
                    if Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign then
                        job:Exchange_Status = trt:ExchangeStatus
                        job:Loan_Status     = trt:LoanStatus
                        if (trt:InternalLocation <> '') then
                            job:Location    = trt:InternalLocation
                        end
                    end

                    if mxd:IsChargeable then
                        job:Chargeable_Job = 'YES'
                        job:Charge_Type = mxd:ChargeType
                    else
                        job:Chargeable_Job = 'NO'
                    end

                    if mxd:IsWarranty then
                        job:Warranty_Job = 'YES'
                        job:Warranty_Charge_Type = mxd:WarrantyType
                    else
                        job:Warranty_Job = 'NO'
                    end

                    job:order_number = mxj:ID
                    job:Account_Number = mxj:CustNetwork
                    job:telephone_number = mxj:CustCTN
                    job:Fault_Code3 = mxj:QuoteValue

                    job:Model_Number = mod:Model_Number
                    job:Manufacturer = mod:Manufacturer
                    IF (mod:Specify_Unit_Type = 'YES') THEN
                        job:Unit_Type   = mod:Unit_Type
                    ELSE
                        job:unit_type = mxd:UnitType
                    END

                    job:turnaround_time = tur:turnaround_time
                    Turnaround_Routine(tur:days, tur:hours, end_date", end_time")
                    job:turnaround_end_date = end_date"
                    job:turnaround_end_time = end_time"

                    if mxj:QuoteQ1 then
                        job:Fault_Code4 = FC_WORKING_ORDER
                    else
                        job:Fault_Code4 = FC_NOT_WORKING_ORDER
                    end

                    if mxj:QuoteQ2 then
                        job:Fault_Code5 = FC_DAMAGED_DISPLAY
                    else
                        job:Fault_Code5 = FC_NO_DAMAGED_DISPLAY
                    end

                    if mxj:QuoteQ3 then
                        job:Fault_Code6 = FC_WATER_DAMAGE
                    else
                        job:Fault_Code6 = FC_NO_WATER_DAMAGE
                    end

                    if mxj:QuoteACC1 then
                        job:Fault_Code8 = FC_CHARGER
                    else
                        job:Fault_Code8 = FC_NO_CHARGER
                    end

                    if mxj:QuoteACC2 then
                        job:Fault_Code9 = FC_BATTERY
                    else
                        job:Fault_Code9 = FC_NO_BATTERY
                    end

                    if mxj:QuoteACC3 then
                        job:Fault_Code10 = FC_MEMORY_CARD
                    else
                        job:Fault_Code10 = FC_NO_MEMORY_CARD
                    end

                    if mxj:QuoteACC4 then
                        job:Fault_Code11 = FC_MANUAL
                    else
                        job:Fault_Code11 = FC_NO_MANUAL
                    end

                    if mxj:QuoteACC5 then
                        job:Fault_Code12 = FC_BOX
                    else
                        job:Fault_Code12 = FC_NO_BOX
                    end

                    job:title = mxj:CustTitle
                    job:initial = mxj:CustFirstName
                    job:surname = mxj:CustLastName
                    job:address_line1 = mxj:CustAdd1
                    job:address_line2 = mxj:CustAdd2
                    job:address_line3 = clip(mxj:CustAdd3) & mxj:CustAdd4
                    job:Postcode = mxj:CustPostcode
                    job:Insurance_reference_Number = mxj:CustRef

                    if access:jobs.insert() = level:benign
                       if access:jobse.primerecord() then
                           jobe:RefNumber = job:Ref_Number
                           jobe:FaultCode13 = mxj:EnvLetter
                           jobe:FaultCode14 = mxj:EnvType
                           if access:jobse.insert() <> level:benign then
                               access:jobse.cancelautoinc()
                           end
                       end

                       if access:jobnotes.primerecord() then
                           jbn:RefNumber = job:Ref_Number
                           jbn:Delivery_Text = mxd:DeliveryText
                           if access:jobnotes.insert() <> level:benign then
                               access:jobnotes.cancelautoinc()
                           end
                       end

                        ! Add Audit Record ***********************************
                        IF access:audit.primerecord() = Level:Benign THEN
                            aud:notes         = ''
                            aud:ref_number    = job:ref_number
                            aud:date          = Today()
                            aud:time          = Clock()
                            aud:user          = mxd:UserCode
                            aud:action        = 'JOB DETAILS RECEIVED FROM MPX'
                            IF (access:audit.insert() <> Level:Benign) THEN
                                access:audit.cancelautoinc()
                            END
                        END

                       ! Save Job No in XML file Record
                       mxj:InspACRJobNo = job:Ref_Number

                       ! Update Record State
                       mxj:Record_State = RS_ACTIVE_JOB
                    else
                        access:jobs.cancelautoinc()
                    end

                end

            end
        end

        access:mpxjobs.update()
    end
