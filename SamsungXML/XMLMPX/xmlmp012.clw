

   MEMBER('xmlmpx.clw')                               ! This is a MEMBER module

                     MAP
                       INCLUDE('XMLMP012.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('XMLMP006.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('XMLMP015.INC'),ONCE        !Req'd for module callout resolution
                     END


CreateCSVFile        PROCEDURE  (string aPathname, long aReportType) ! Declare Procedure
JobCount long
ExportStatus    file,driver('ASCII'),pre(exp),create,bindable,thread
Record           record
Line                string(3000)
                 end
              end
  CODE
    JobCount = 0
    ExportStatus{prop:name} = aPathname
    create(ExportStatus)
    open(ExportStatus, WriteOnly)
    if errorcode() then
        LogError('Unable To Create File : ' & clip(aPathname) & '<13,10>Error: ' & errorcode() & ' ' & error())
    else
        access:mpxjobs.clearkey(mxj:StateReportCodeKey)
        mxj:record_state = RS_EXPORT
        mxj:ReportCode = aReportType
        set(mxj:StateReportCodeKey,mxj:StateReportCodeKey)
        loop while access:mpxjobs.next() = Level:Benign
            if mxj:record_state <> RS_EXPORT or mxj:ReportCode <> aReportType then
                break
            end

            clear(exp:Line)

            case aReportType
            of MPX_NEW_JOB
                exp:Line = '#JOB'                        & '|'  & |
                           clip(mxj:InspACRJobNo)        & '|'  & |
                           format(mxj:EnvDateSent, @d06) & '|'  & |
                           clip(mxj:ID)                  & '|'  & |
                           clip(mxj:InspACRStatus)       & '|'

            of MPX_UNIT_RECEIVED
                exp:Line = '#JOB'                          & '|'  & |
                           clip(mxj:ID)                    & '|'  & |
                           clip(mxj:InspACRJobNo)          & '|'  & |
                           format(mxj:InspDateRecd, @d06)  & '|'  & |
                           clip(mxj:InspModelID)           & '|'  & |
                           clip(mxj:InspACRStatus)         & '|'  & |
                           Bool2String(mxj:InspFlag)       & '|'  & |
                           clip(mxj:InspAttention)         & '|'  & |
                           clip(mxj:InspQty)               & '|'  & |
                           clip(mxj:InspPackageCond)       & '|'

            of MPX_UNIT_INSPECTED
                exp:Line = '#JOB'                          & '|'  & |
                           clip(mxj:ID)                    & '|'  & |
                           clip(mxj:InspACRJobNo)          & '|'  & |
                           clip(mxj:InspModelID)           & '|'  & |
                           Bool2String(mxj:InspQ1)         & '|'  & |
                           Bool2String(mxj:InspQ2)         & '|'  & |
                           Bool2String(mxj:InspQ3)         & '|'  & |
                           Bool2String(mxj:InspACC1)       & '|'  & |
                           Bool2String(mxj:InspACC2)       & '|'  & |
                           Bool2String(mxj:InspACC3)       & '|'  & |
                           Bool2String(mxj:InspACC4)       & '|'  & |
                           Bool2String(mxj:InspACC5)       & '|'  & |
                           clip(mxj:InspGrade)             & '|'  & |
                           clip(mxj:InspACRStatus)         & '|'  & |
                           clip(mxj:InspIMEI)              & '|'  & |
                           clip(mxj:InspNotes)             & '|'  & |
                           format(mxj:InspDate, @d06)      & '|'  & |
                           clip(mxj:InspBrand)             & '|'  & |
                           Bool2String(mxj:InspInWarranty) & '|'

            of MPX_UNIT_DESPATCHED
                exp:Line = '#JOB'                         & '|'  & |
                           clip(mxj:ID)                   & '|'  & |
                           clip(mxj:InspACRJobNo)         & '|'  & |
                           clip(mxj:InspACRStatus)        & '|'  & |
                           clip(mxj:DespatchConsignment)  & '|'  & |
                           format(mxj:Despatchdate, @d06) & '|'

            end

            add(ExportStatus)
            JobCount += 1

            if JobCount > 100 then
                break
            end

        end

        close(ExportStatus)

        if JobCount = 0 then
            remove(ExportStatus)
        end

    end
    return JobCount
