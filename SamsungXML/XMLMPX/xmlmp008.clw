

   MEMBER('xmlmpx.clw')                               ! This is a MEMBER module

                     MAP
                       INCLUDE('XMLMP008.INC'),ONCE        !Local module procedure declarations
                     END


CloseFiles           PROCEDURE                        ! Declare Procedure
  CODE
    relate:mpxdefs.close()
    relate:mpxjobs.close()
    relate:mpxstat.close()
    relate:jobs.close()
    relate:subtracc.close()
    relate:jobse.close()
    relate:jobnotes.close()
    relate:modelnum.close()
    relate:audit.close()
    relate:trantype.close()
    relate:turnarnd.close()
    relate:defaults.close()
    relate:jobstage.close()
    relate:status.close()
    relate:stahead.close()
