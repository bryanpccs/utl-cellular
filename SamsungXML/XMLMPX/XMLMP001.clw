

   MEMBER('xmlmpx.clw')                               ! This is a MEMBER module

                     MAP
                       INCLUDE('XMLMP001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('XMLMP002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('XMLMP004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('XMLMP006.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('XMLMP007.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('XMLMP008.INC'),ONCE        !Req'd for module callout resolution
                     END


Main                 PROCEDURE                        ! Declare Procedure
SemaphoreName        cstring('XMLMPX.EXE<0>')
UniqueId             string(32)
ExportPathname       string(FILE:MaxFilePath)
ImportPathname       string(FILE:MaxFilePath)
szDirectory          cstring(FILE:MaxFilePath)
nCode                long
JobCount             long
FileSeqNo            long

  CODE
! Check One Instance
IF CreateSemaphore(,0,1,SemaphoreName) <> 0 THEN
    ! Error : File Aleady Exists = 183
    IF GetLastError() = 183 THEN
        ! Stop if another instance already running
        HALT
    END
END
    ! Open Database Files
    OpenFiles()

    ! make sure sub-directory exists
    szDirectory = clip(longpath()) & '\webmpx'
    nCode = MkDir(szDirectory)

    ! construct a unique id for filenames
    UniqueId = format(today(), @d11) & format(clock(), @n07)

    ! Stage 1 - Import New Jobs from MPX
    ImportJobs(szDirectory, UniqueId)

    ! Stage 2 - Export Status Updates to MPX
    ExportStatus(szDirectory, UniqueId)

    ! Close Database Files
    CloseFiles()
