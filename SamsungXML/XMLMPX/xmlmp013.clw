

   MEMBER('xmlmpx.clw')                               ! This is a MEMBER module

                     MAP
                       INCLUDE('XMLMP013.INC'),ONCE        !Local module procedure declarations
                     END


MarkProcessedJobs    PROCEDURE  (string aPathname)    ! Declare Procedure
ResponseFile file,driver('BASIC','/COMMA=124 /ALWAYSQUOTE=OFF'),pre(rsp)
Record        record
ID               string(16)       ! Order Number
Status           string(50)       ! Status
Message          string(255)      ! Error Message
                 end
              end
  CODE
    ! Update Job Status in XML D/B
    ResponseFile{prop:name} = aPathname
    open(ResponseFile, ReadOnly)
    if not errorcode() then
        set(ResponseFile,0)
        loop
            next(ResponseFile)
            if errorcode()
                Break
            end

            ResponseId# = deformat(rsp:ID)

            access:mpxjobs.clearkey(mxj:IDKey)
            mxj:ID = ResponseId#
            set(mxj:IDKey, mxj:IDKey)
            ! Allow for the case where MPX ID is not unique !!!!
            ! - shouldn't happen but if it does would cause a message loop
            ! so make sure here that ALL candidates are marked as processed....
            loop while access:mpxjobs.next() = level:benign
                if mxj:ID <> ResponseId# then
                    break
                end
                if mxj:Record_State = RS_EXPORT then
                    if upper(rsp:Status) = 'SUCCESS' then
                        mxj:Record_State      = RS_ACTIVE_JOB
                        ! Note: Do not reset ReportCode here so
                        ! record is not a candidate for export until
                        ! ServiceBase sattus changes to next level....
                        mxj:Exception_Code    = XC_NONE
                        mxj:Exception_desc    = ''
                    else
                        mxj:Record_State      = RS_EXCEPTION
                        ! Note: Reset ReportCode here so that record is an export candidate
                        ! when exception is cleared....
                        mxj:ReportCode        = MPX_NONE
                        mxj:Exception_Code    = XC_EXPORT_FAIL
                        mxj:Exception_desc    = rsp:Message
                    end
                end

                access:mpxjobs.update()
            end
         end

        close(ResponseFile)

        !remove(aPathname)  ********** debug only ***** remember to uncomment this !!!!!!!

    end







