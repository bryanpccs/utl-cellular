

   MEMBER('xmlmpx.clw')                               ! This is a MEMBER module

                     MAP
                       INCLUDE('XMLMP015.INC'),ONCE        !Local module procedure declarations
                     END


Bool2String          PROCEDURE  (byte b1)             ! Declare Procedure
  CODE
    if b1 then
        return 'TRUE'
    end
    return 'FALSE'
