

   MEMBER('xmlmpx.clw')                               ! This is a MEMBER module

                     MAP
                       INCLUDE('XMLMP007.INC'),ONCE        !Local module procedure declarations
                     END


OpenFiles            PROCEDURE                        ! Declare Procedure
  CODE
    relate:mpxdefs.open()
    relate:mpxjobs.open()
    relate:mpxstat.open()
    relate:jobs.open()
    relate:subtracc.open()
    relate:jobse.open()
    relate:jobnotes.open()
    relate:modelnum.open()
    relate:audit.open()
    relate:trantype.open()
    relate:turnarnd.open()
    relate:defaults.open()
    relate:jobstage.open()
    relate:status.open()
    relate:stahead.open()
