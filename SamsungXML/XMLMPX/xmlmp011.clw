

   MEMBER('xmlmpx.clw')                               ! This is a MEMBER module

                     MAP
                       INCLUDE('XMLMP011.INC'),ONCE        !Local module procedure declarations
                     END


CheckMPXStatus       PROCEDURE  (string aJobStatus)   ! Declare Procedure
  CODE
    access:mpxstat.clearkey(mxs:SBStatusKey)
    mxs:SB_STATUS = aJobStatus
    if access:mpxstat.fetch(mxs:SBStatusKey) = level:benign then
        return mxs:mpx_status
    end
    return MPX_NONE
