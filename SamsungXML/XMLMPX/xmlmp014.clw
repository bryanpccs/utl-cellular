

   MEMBER('xmlmpx.clw')                               ! This is a MEMBER module

                     MAP
                       INCLUDE('XMLMP014.INC'),ONCE        !Local module procedure declarations
                     END


CompareStrings       PROCEDURE  (string s1, string s2) ! Declare Procedure
  CODE
    if upper(s1) = upper(s2) then
        return true
    end
    return false
