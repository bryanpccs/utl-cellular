

   MEMBER('xmlmpx.clw')                               ! This is a MEMBER module

                     MAP
                       INCLUDE('XMLMP004.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('XMLMP005.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('XMLMP006.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('XMLMP011.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('XMLMP012.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('XMLMP013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('XMLMP014.INC'),ONCE        !Req'd for module callout resolution
                     END


ExportStatus         PROCEDURE  (string aDirectory, string aUniqueId) ! Declare Procedure
StatusPathname         string(FILE:MaxFilePath)
ResponsePathname       string(FILE:MaxFilePath)

MPXStatus       long
JobCount        long
FileSeqNo       long
  CODE
    ! Look for New Jobs in ServiceBase
    access:jobs.clearkey(job:Date_Booked_Key)
    job:date_booked = today()
    set(job:Date_Booked_Key,job:Date_Booked_Key)
    loop
        ! Note: search file from start date to end of file
        if access:jobs.next() <> Level:Benign then
            break
        end
        access:subtracc.clearkey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        if access:subtracc.fetch(sub:Account_Number_Key) <> Level:Benign then
            cycle
        end

        access:mpxdefs.clearkey(mxd:AccountNoKey)
        mxd:AccountNo = job:Account_Number
        if access:mpxdefs.fetch(mxd:AccountNoKey) <> Level:Benign then
            cycle
        end

        ! Check if job already exported
        access:mpxjobs.clearkey(mxj:JobNoKey)
        mxj:InspACRJobNo = job:Ref_Number
        if access:mpxjobs.fetch(mxj:JobNoKey) = Level:Benign then
            cycle
        end

        if access:mpxjobs.primerecord() = Level:Benign then

            mxj:RECORD_STATE = RS_ACTIVE_JOB
            mxj:EXCEPTION_CODE = XC_NONE
            mxj:ReportCode    = MPX_NONE

            mxj:InspACRJobNo = job:Ref_Number
            mxj:EnvDateSent = job:date_booked
            mxj:ID = job:order_number
            mxj:InspACRStatus = job:Current_Status

            if (job:surname <> '') then
                mxj:CustTitle = job:title
                mxj:CustFirstName = job:initial
                mxj:CustLastName = job:surname
            else
                mxj:CustLastName = job:company_name
            end

            mxj:CustAdd1 = job:address_line1
            mxj:CustAdd2 = job:address_line2
            mxj:CustAdd3 = job:address_line3
            mxj:CustPostcode = job:postcode
            mxj:CustCTN = job:telephone_number
            mxj:QuoteValue = job:Fault_Code3
            mxj:ModelID = job:Model_Number
            mxj:QuoteQ1 = CompareStrings(job:Fault_Code4, FC_WORKING_ORDER)
            mxj:QuoteQ2 = CompareStrings(job:Fault_Code5, FC_DAMAGED_DISPLAY)
            mxj:QuoteQ3 = CompareStrings(job:Fault_Code6, FC_WATER_DAMAGE)
            mxj:QuoteACC1 = CompareStrings(job:Fault_Code8, FC_CHARGER)
            mxj:QuoteACC2 = CompareStrings(job:Fault_Code9, FC_BATTERY)
            mxj:QuoteACC3 = CompareStrings(job:Fault_Code10, FC_MEMORY_CARD)
            mxj:QuoteACC4 = CompareStrings(job:Fault_Code11, FC_MANUAL)
            mxj:QuoteACC5 = CompareStrings(job:Fault_Code12, FC_BOX)
            mxj:CustRef = job:Insurance_Reference_Number
            mxj:CustNetwork = job:Account_Number
            mxj:InspType = job:Turnaround_Time
            mxj:EnvCount = 0

            ! Check if job already exported
            access:jobse.clearkey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            if access:jobse.fetch(jobe:RefNumberKey) = Level:Benign then
                mxj:EnvLetter = jobe:FaultCode13
                mxj:EnvType = jobe:FaultCode14
            end

            if access:mpxjobs.insert() = Level:Benign then
                ! Add Audit Record ***********************************
                if access:audit.primerecord() = Level:Benign then
                    aud:notes         = ''
                    aud:ref_number    = job:ref_number
                    aud:date          = Today()
                    aud:time          = Clock()
                    aud:user          = mxd:UserCode
                    aud:action        = 'JOB DETAILS SENT TO MPX'
                    if access:audit.insert() <> Level:Benign then
                        access:audit.cancelautoinc()
                    end
                end
            else
                access:mpxjobs.cancelautoinc()
            end
        end
    end
    ! Update XML Job Status
    access:mpxjobs.clearkey(mxj:StateJobNoKey)
    mxj:record_state = RS_ACTIVE_JOB
    set(mxj:StateJobNoKey,mxj:StateJobNoKey)
    loop while access:mpxjobs.next() = Level:Benign
        if mxj:record_state <> RS_ACTIVE_JOB then
            break
        end
        ! Check if Status Has Changed
        access:jobs.clearkey(job:ref_number_key)
        job:ref_number = mxj:InspACRJobNo
        if access:jobs.fetch(job:ref_number_key) = Level:Benign then
            MPXStatus =  CheckMPXStatus(job:Current_Status)
            if MPXStatus <> mxj:ReportCode then

                case MPXStatus
                of MPX_NEW_JOB
                    mxj:record_state = RS_EXPORT
                    mxj:ReportCode = MPX_NEW_JOB

                    mxj:InspACRJobNo = job:ref_number
                    mxj:EnvDateSent = job:date_booked
                    mxj:ID = job:order_number
                    mxj:InspACRStatus = job:current_status

                    access:mpxjobs.update()

                of MPX_UNIT_RECEIVED
                    mxj:record_state = RS_EXPORT
                    mxj:ReportCode = MPX_UNIT_RECEIVED

                    mxj:ID = job:order_number
                    mxj:InspACRJobNo = job:ref_number
                    mxj:InspModelID = job:Model_Number
                    mxj:InspACRStatus = job:current_status
                    mxj:InspPackageCond = job:Fault_Code2

                    access:jobse.clearkey(jobe:RefNumberKey)
                    jobe:RefNumber = job:ref_number
                    if access:jobse.fetch(jobe:RefNumberKey) = level:benign
                         mxj:InspDateRecd = jobe:InWorkshopDate
                         mxj:InspFlag = CompareStrings(jobe:FaultCode15, FC_HIGHLIGHT_ISSUE)
                         mxj:InspQty = jobe:FaultCode16
                    end
                    
                    access:jobnotes.clearkey(jbn:RefNumberKey)
                    jbn:RefNumber = job:ref_number
                    if access:jobnotes.fetch(jbn:RefNumberKey) = level:benign
                        mxj:InspAttention = jbn:Fault_Description
                    end

                    access:mpxjobs.update()

                of MPX_UNIT_INSPECTED
                    mxj:record_state = RS_EXPORT
                    mxj:ReportCode = MPX_UNIT_INSPECTED

                    mxj:ID = job:order_number
                    mxj:InspACRJobNo = job:ref_number
                    mxj:InspModelID = job:Model_Number
                    mxj:InspACRStatus = job:current_status

                    mxj:InspQ1 = CompareStrings(job:Fault_Code4, FC_WORKING_ORDER)
                    mxj:InspQ2 = CompareStrings(job:Fault_Code5, FC_DAMAGED_DISPLAY)
                    mxj:InspQ3 = CompareStrings(job:Fault_Code6, FC_WATER_DAMAGE)
                    mxj:InspACC1 = CompareStrings(job:Fault_Code8, FC_CHARGER)
                    mxj:InspACC2 = CompareStrings(job:Fault_Code9, FC_BATTERY)
                    mxj:InspACC3 = CompareStrings(job:Fault_Code10, FC_MEMORY_CARD)
                    mxj:InspACC4 = CompareStrings(job:Fault_Code11, FC_MANUAL)
                    mxj:InspACC5 = CompareStrings(job:Fault_Code12, FC_BOX)
                    mxj:InspGrade = job:Fault_Code7
                    mxj:InspIMEI = job:ESN
                    mxj:InspDate = job:Date_Completed
                    mxj:InspBrand = job:Fault_Code1
                    mxj:InspInWarranty = CompareStrings(job:Warranty_Job, 'YES')

                    access:jobnotes.clearkey(jbn:RefNumberKey)
                    jbn:RefNumber = job:ref_number
                    if access:jobnotes.fetch(jbn:RefNumberKey) = level:benign
                        mxj:InspNotes = jbn:Engineers_Notes
                    end

                    access:mpxjobs.update()

                of MPX_UNIT_DESPATCHED
                    mxj:record_state = RS_EXPORT
                    mxj:ReportCode = MPX_UNIT_DESPATCHED

                    mxj:ID = job:order_number
                    mxj:InspACRJobNo = job:ref_number
                    mxj:InspACRStatus = job:current_status
                    mxj:DespatchConsignment = job:Consignment_Number
                    mxj:DespatchDate = job:Date_Despatched

                    access:mpxjobs.update()

                end
            end

        end
    end



    ! Export Confirm Envelope
    FileSeqNo = 0
    JobCount = 0

    loop
        FileSeqNo += 1
        StatusPathname = clip(aDirectory) & '\EN1_' & clip(aUniqueId) & '_' & format(FileSeqNo, @n03) & '.csv'
        ResponsePathname = clip(aDirectory) & '\EN2_' & clip(aUniqueId) & '_' & format(FileSeqNo, @n03) & '.csv'

        JobCount = CreateCSVFile(StatusPathname, MPX_NEW_JOB)

        if JobCount > 0 then
            code# =  RunDOS(clip(longpath()) & '\webmpx.exe -ENVELOPE "' & clip(StatusPathname) & '" "' & clip(ResponsePathname) & '"', true)
            if code# then
                MarkProcessedJobs(ResponsePathname)
            else
                LogError('Unexpected Eror running webmpx.exe')
                JobCount = 0
            end
        end

    while JobCount > 0
    ! Export Receipt Of Phone
    FileSeqNo = 0
    JobCount = 0

    loop
        FileSeqNo += 1
        StatusPathname = clip(aDirectory) & '\RC1_' & clip(aUniqueId) &  '_' & format(FileSeqNo, @n03) & '.csv'
        ResponsePathname = clip(aDirectory) & '\RC2_' & clip(aUniqueId) &  '_' & format(FileSeqNo, @n03) & '.csv'

        JobCount = CreateCSVFile(StatusPathname, MPX_UNIT_RECEIVED)

        if JobCount > 0 then
            code# =  RunDOS(clip(longpath()) & '\webmpx.exe -RECEIPT "' & clip(StatusPathname) & '" "' & clip(ResponsePathname) & '"', true)
            if code# then
                MarkProcessedJobs(ResponsePathname)
            else
                LogError('Unexpected Eror running webmpx.exe')
                JobCount = 0
            end
        end

    while JobCount > 0
    ! Export Inspection Status
    FileSeqNo = 0
    JobCount = 0

    loop
        FileSeqNo += 1
        StatusPathname = clip(aDirectory) & '\IN1_' & clip(aUniqueId) &  '_' & format(FileSeqNo, @n03) & '.csv'
        ResponsePathname = clip(aDirectory) & '\IN2_' & clip(aUniqueId) &  '_' & format(FileSeqNo, @n03) & '.csv'

        JobCount = CreateCSVFile(StatusPathname, MPX_UNIT_INSPECTED)

        if JobCount > 0 then
            code# =  RunDOS(clip(longpath()) & '\webmpx.exe -INSPECTION "' & clip(StatusPathname) & '" "' & clip(ResponsePathname) & '"', true)
            if code# then
                MarkProcessedJobs(ResponsePathname)
            else
                LogError('Unexpected Eror running webmpx.exe')
                JobCount = 0
            end
        end

    while JobCount > 0
    ! Export Despatch Status
    FileSeqNo = 0
    JobCount = 0

    loop
        FileSeqNo += 1
        StatusPathname = clip(aDirectory) & '\DE1_' & clip(aUniqueId) &  '_' & format(FileSeqNo, @n03) & '.csv'
        ResponsePathname = clip(aDirectory) & '\DE2_' & clip(aUniqueId) &  '_' & format(FileSeqNo, @n03) & '.csv'

        JobCount = CreateCSVFile(StatusPathname, MPX_UNIT_DESPATCHED)

        if JobCount > 0 then
            code# =  RunDOS(clip(longpath()) & '\webmpx.exe -DESPATCH "' & clip(StatusPathname) & '" "' & clip(ResponsePathname) & '"', true)
            if code# then
                MarkProcessedJobs(ResponsePathname)
            else
                LogError('Unexpected Eror running webmpx.exe')
                JobCount = 0
            end
        end

    while JobCount > 0
