

   MEMBER('xmlmpx.clw')                               ! This is a MEMBER module

                     MAP
                       INCLUDE('XMLMP006.INC'),ONCE        !Local module procedure declarations
                     END


LogError             PROCEDURE  (string aErrorMessage) ! Declare Procedure
  CODE
    !message(clip(aErrorMessage))
    lineprint(format(today(), @d06) & ' ' & format(clock(), @t04) & ' ' & clip(aErrorMessage), clip(longpath()) & '\xmlmpx.log')
