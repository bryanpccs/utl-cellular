

   MEMBER('XMLTEST.clw')                              ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('XMLTE001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

XmlDocPath           STRING(256)
BRW1::View:Browse    VIEW(XMLDOCS)
                       PROJECT(XMD:RECORD_NO)
                       PROJECT(XMD:ENTRY_DATE)
                       PROJECT(XMD:ENTRY_TIME)
                       PROJECT(XMD:TYPE)
                       PROJECT(XMD:PATHNAME)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
XMD:RECORD_NO          LIKE(XMD:RECORD_NO)            !List box control field - type derived from field
XMD:ENTRY_DATE         LIKE(XMD:ENTRY_DATE)           !List box control field - type derived from field
XMD:ENTRY_TIME         LIKE(XMD:ENTRY_TIME)           !List box control field - type derived from field
XMD:TYPE               LIKE(XMD:TYPE)                 !List box control field - type derived from field
XMD:PATHNAME           LIKE(XMD:PATHNAME)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('XML Test Functions'),AT(,,286,189),GRAY,DOUBLE
                       BUTTON('Import XML Docs'),AT(152,164,80,16),USE(?ImportButton)
                       BUTTON('Exit'),AT(236,164,40,16),USE(?ExitButton)
                       STRING(@s255),AT(4,4,276,12),USE(XmlDocPath)
                       LIST,AT(4,16,276,144),USE(?List),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('56R|M~RECORD NO~L(2)@n-14@40R|M~ENTRY DATE~L(2)@d17@20R|M~ENTRY TIME~L(2)@t7@12R' &|
   '|M~TYPE~L(2)@n3@1020R|M~PATHNAME~L(2)@s255@'),FROM(Queue:Browse)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
                     END

FileQueue queue(File:Queue),pre(fq)
          end
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ImportButton
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  XmlDocPath = CLIP(LONGPATH(PATH())) & '\XMLDOCS\IMPORT\'
  Relate:XMLDOCS.Open
  SELF.FilesOpened = True
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW1::View:Browse,Queue:Browse,Relate:XMLDOCS,SELF)
  OPEN(window)
  SELF.Opened=True
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,)
  BRW1.AddField(XMD:RECORD_NO,BRW1.Q.XMD:RECORD_NO)
  BRW1.AddField(XMD:ENTRY_DATE,BRW1.Q.XMD:ENTRY_DATE)
  BRW1.AddField(XMD:ENTRY_TIME,BRW1.Q.XMD:ENTRY_TIME)
  BRW1.AddField(XMD:TYPE,BRW1.Q.XMD:TYPE)
  BRW1.AddField(XMD:PATHNAME,BRW1.Q.XMD:PATHNAME)
  BRW1.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:XMLDOCS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ImportButton
      set(xmldocs)
      loop
          next(xmldocs)
          if errorcode() then break.
          delete(xmldocs)
      end
      
      directory(FileQueue, clip(XmlDocPath) & '*.xml', ff_:Normal)
      recount# = records(FileQueue)
      loop ix# = recount# to 1 by -1
          get(FileQueue, ix#)
          !message(clip(XmlDocPath) & clip(fq:name))
      
          ! Add entry to DB Queue
          access:xmldocs.primerecord()
          xmd:ENTRY_DATE = TODAY()
          xmd:ENTRY_TIME = CLOCK()
          xmd:TYPE = DT_IMPORT
          xmd:PATHNAME = clip(XmlDocPath) & clip(fq:name)
          IF (access:xmldocs.insert() <> Level:Benign)
              access:xmldocs.cancelautoinc()
          END
      
      end
      
      message('Imported ' & recount# & ' records')
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ExitButton
      ThisWindow.Update
      Post(event:closewindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

