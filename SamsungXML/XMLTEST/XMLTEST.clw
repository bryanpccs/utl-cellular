   PROGRAM



   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFILE.INC'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('EQUATES.CLW'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE
INCLUDE('SAMXML.INC')   ! SAMSUNG/HAYS XML MESSAGING Constants

   MAP
     MODULE('Windows API')
SystemParametersInfo PROCEDURE (LONG uAction, LONG uParam, *? lpvParam, LONG fuWinIni),LONG,RAW,PROC,PASCAL,DLL(TRUE),NAME('SystemParametersInfoA')
     END
     MODULE('XMLTEBC.CLW')
DctInit     PROCEDURE
DctKill     PROCEDURE
     END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('XMLTE001.CLW')
Main                   PROCEDURE   !
     END
   END

SilentRunning        BYTE(0)                         !Set true when application is running in silent mode

XMLDOCS              FILE,DRIVER('Btrieve'),OEM,NAME('XMLDOCS'),PRE(XMD),CREATE,BINDABLE,THREAD
RecordNoKey              KEY(XMD:RECORD_NO),NOCASE,PRIMARY
TypeKey                  KEY(XMD:TYPE),DUP,NOCASE
Record                   RECORD,PRE()
RECORD_NO                   LONG
ENTRY_DATE                  DATE
ENTRY_TIME                  TIME
TYPE                        BYTE
PATHNAME                    STRING(255)
                         END
                     END                       



glo:ErrorText        STRING(1000)
glo:owner            STRING(20)
glo:DateModify       DATE
glo:TimeModify       TIME
glo:Notes_Global     STRING(1000)
glo:Q_Invoice        QUEUE,PRE(glo)
Invoice_Number         REAL
Account_Number         STRING(15)
                     END
glo:afe_path         STRING(255)
glo:Password         STRING(20)
glo:PassAccount      STRING(30)
glo:Preview          STRING('True')
glo:q_JobNumber      QUEUE,PRE(GLO)
Job_Number_Pointer     REAL
                     END
glo:Q_PartsOrder     QUEUE,PRE(GLO)
Q_Order_Number         REAL
Q_Part_Number          STRING(30)
Q_Description          STRING(30)
Q_Supplier             STRING(30)
Q_Quantity             LONG
Q_Purchase_Cost        REAL
Q_Sale_Cost            REAL
                     END
glo:G_Select1        GROUP,PRE(GLO)
Select1                STRING(30)
Select2                STRING(40)
Select3                STRING(40)
Select4                STRING(40)
Select5                STRING(40)
Select6                STRING(40)
Select7                STRING(40)
Select8                STRING(40)
Select9                STRING(40)
Select10               STRING(40)
Select11               STRING(40)
Select12               STRING(40)
Select13               STRING(40)
Select14               STRING(40)
Select15               STRING(40)
Select16               STRING(40)
Select17               STRING(40)
Select18               STRING(40)
Select19               STRING(40)
Select20               STRING(40)
Select21               STRING(40)
Select22               STRING(40)
Select23               STRING(40)
Select24               STRING(40)
Select25               STRING(40)
Select26               STRING(40)
Select27               STRING(40)
Select28               STRING(40)
Select29               STRING(40)
Select30               STRING(40)
Select31               STRING(40)
Select32               STRING(40)
Select33               STRING(40)
Select34               STRING(40)
Select35               STRING(40)
Select36               STRING(40)
Select37               STRING(40)
Select38               STRING(40)
Select39               STRING(40)
Select40               STRING(40)
Select41               STRING(40)
Select42               STRING(40)
Select43               STRING(40)
Select44               STRING(40)
Select45               STRING(40)
Select46               STRING(40)
Select47               STRING(40)
Select48               STRING(40)
Select49               STRING(40)
Select50               STRING(40)
                     END
glo:q_RepairType     QUEUE,PRE(GLO)
Repair_Type_Pointer    STRING(30)
                     END
glo:Q_UnitType       QUEUE,PRE(GLO)
Unit_Type_Pointer      STRING(30)
                     END
glo:Q_ChargeType     QUEUE,PRE(GLO)
Charge_Type_Pointer    STRING(30)
                     END
glo:Q_ModelNumber    QUEUE,PRE(GLO)
Model_Number_Pointer   STRING(30)
                     END
glo:Q_Accessory      QUEUE,PRE(GLO)
Accessory_Pointer      STRING(30)
                     END
glo:Q_AccessLevel    QUEUE,PRE(GLO)
Access_Level_Pointer   STRING(30)
                     END
glo:Q_AccessAreas    QUEUE,PRE(GLO)
Access_Areas_Pointer   STRING(30)
                     END
glo:Q_Notes          QUEUE,PRE(GLO)
notes_pointer          STRING(80)
                     END
glo:EnableFlag       STRING(3)
glo:TimeLogged       TIME
glo:GLO:ApplicationName STRING(8)
glo:GLO:ApplicationCreationDate LONG
glo:GLO:ApplicationCreationTime LONG
glo:GLO:ApplicationChangeDate LONG
glo:GLO:ApplicationChangeTime LONG
glo:GLO:Compiled32   BYTE
glo:GLO:AppINIFile   STRING(255)
glo:Queue            QUEUE,PRE(GLO)
Pointer                STRING(40)
                     END
glo:Queue2           QUEUE,PRE(GLO)
Pointer2               STRING(40)
                     END
glo:Queue3           QUEUE,PRE(GLO)
Pointer3               STRING(40)
                     END
glo:Queue4           QUEUE,PRE(GLO)
Pointer4               STRING(40)
                     END
glo:Queue5           QUEUE,PRE(GLO)
Pointer5               STRING(40)
                     END
glo:Queue6           QUEUE,PRE(GLO)
Pointer6               STRING(40)
                     END
glo:Queue7           QUEUE,PRE(GLO)
Pointer7               STRING(40)
                     END
glo:Queue8           QUEUE,PRE(GLO)
Pointer8               STRING(40)
                     END
glo:Queue9           QUEUE,PRE(GLO)
Pointer9               STRING(40)
                     END
glo:Queue10          QUEUE,PRE(GLO)
Pointer10              STRING(40)
                     END
glo:Queue11          QUEUE,PRE(GLO)
Pointer11              STRING(40)
                     END
glo:Queue12          QUEUE,PRE(GLO)
Pointer12              STRING(40)
                     END
glo:Queue13          QUEUE,PRE(GLO)
Pointer13              STRING(40)
                     END
glo:Queue14          QUEUE,PRE(GLO)
Pointer14              STRING(40)
                     END
glo:Queue15          QUEUE,PRE(GLO)
Pointer15              STRING(40)
                     END
glo:Queue16          QUEUE,PRE(GLO)
Pointer16              STRING(40)
                     END
glo:Queue17          QUEUE,PRE(GLO)
Pointer17              STRING(40)
                     END
glo:Queue18          QUEUE,PRE(GLO)
Pointer18              STRING(40)
                     END
glo:Queue19          QUEUE,PRE(GLO)
Pointer19              STRING(40)
                     END
glo:Queue20          QUEUE,PRE(GLO)
Pointer20              STRING(40)
                     END
Access:XMLDOCS       &FileManager
Relate:XMLDOCS       &RelationManager
FuzzyMatcher         FuzzyClass
GlobalErrors         ErrorClass
INIMgr               INIClass
GlobalRequest        BYTE(0),THREAD
GlobalResponse       BYTE(0),THREAD
VCRRequest           LONG(0),THREAD
lCurrentFDSetting    LONG
lAdjFDSetting        LONG

  CODE
  GlobalErrors.Init
  DctInit
  FuzzyMatcher.Init
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)
  INIMgr.Init('XMLTEST.INI')
  SystemParametersInfo (38, 0, lCurrentFDSetting, 0)
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 0, lAdjFDSetting, 3)
  END
  Main
  INIMgr.Update
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 1, lAdjFDSetting, 3)
  END
  INIMgr.Kill
  FuzzyMatcher.Kill
  DctKill
  GlobalErrors.Kill


