

   MEMBER('XMLSVC.clw')                                    ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('XMLSV001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                             ! Generated from procedure template - Window

Window               WINDOW('Samsung/ACR Logistics XML Messaging - XML Service'),AT(,,214,137),SYSTEM,GRAY
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
! Data Declarations
MEM_RECORD LIKE(xjb:Record),PRE(memxjb)

MEM_EXPORT_RESPONSE_GROUP GROUP,PRE(memexp)
RETURN_TYPE     STRING(1)
JOB_NO          STRING(30)
BP_NO           STRING(30)
ERR_MSG         STRING(100)
    END
    MAP
ImportJobs                   PROCEDURE()
ExportJobsResponse           PROCEDURE()
copyXmlValuesToFields        PROCEDURE(XMLExchange xch, XMLNavigator nav, STRING parentNodePath),BYTE
UpdateExportJobs             PROCEDURE(XMLExchange xch, XMLNavigator nav, STRING parentNodePath),BYTE
getNodeValue                 PROCEDURE(XMLExchange xch, XMLNavigator nav),STRING
WriteError                   PROCEDURE(STRING arg:s)
    END
! Logfile Declarations
LOGFILE               FILE,DRIVER('ASCII'),OEM,PRE(log),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
recbuff                     STRING(256)
                         END
                     END

               MAP
OpenLog                  PROCEDURE(STRING f, BYTE newflag=0),BYTE
CloseLog                 PROCEDURE()
WriteLog                 PROCEDURE(STRING s, BYTE logflag=0)
              END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It's called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  Relate:XMLDOCS.Open                                      ! File XMLDOCS used by this procedure, so make sure it's RelationManager is open
  Relate:XMLJOBS.Open                                      ! File XMLJOBS used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  !WriteError('Start Run')
  IF COMMAND('/IMPORT') THEN
      ! Process getServiceRequest
      !WriteError('Start Import Jobs')
      ImportJobs()
      !WriteError('End Import Jobs')
  ELSIF COMMAND('/EXPORT') THEN
      ! Process putServiceRequest Response
      !WriteError('Start Export Jobs Response')
      ExportJobsResponse()
      !WriteError('End Export Jobs Response')
  END
  !WriteError('End Run')
  ! Start Change xxx BE(21/10/2004)
  ! Call Close file methods
  Relate:XMLDOCS.Close
  Relate:XMLJOBS.Close
  ! Start Change xxx BE(21/10/2004)
  HALT()
  
  
  
  OPEN(Window)                                             ! Open window
  SELF.Opened=True
  Do DefineListboxStyle
  INIMgr.Fetch('Main',Window)                              ! Restore window settings from non-volatile store
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:XMLDOCS.Close
    Relate:XMLJOBS.Close
  END
  IF SELF.Opened
    INIMgr.Update('Main',Window)                           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

ImportJobs      PROCEDURE()
ResultCode  BYTE
xmlExch     XMLExchange
xmlNav      &XMLNavigator
success     LONG
    CODE

    ! Process getServiceRequest Response Documents

    access:xmldocs.clearkey(xmd:TypeKey)
    xmd:Type = DT_IMPORT
    SET(xmd:TypeKey,xmd:TypeKey)
    LOOP
        IF ((access:xmldocs.next() <> Level:benign) OR |
            (xmd:Type <> DT_IMPORT)) THEN
            BREAK
        END
        success = false;
        ResultCode = xmlExch.open(CLIP(xmd:Pathname))
        IF (ResultCode = CPXMLErr:NoError) THEN
            xmlExch.setStyle(XMLStyle:ADO_Net)
            xmlNav &= xmlExch.getNavigator()
            
            IF ((xmlNav.goToRoot() = Level:Benign) AND |
                (xmlNav.getNodeName() = 'ASREQUEST_CIC_ASC')) THEN  
                !LOGOUT(600, XMLJOBS)
                IF copyXmlValuesToFields(xmlExch, xmlNav, 'ASREQUEST_CIC_ASC')  THEN
                    !COMMIT
                    !WriteError(CLIP(xmd:Pathname) & ' Parsed OK')
                    success = true
                ELSE
                    !ROLLBACK
                    WriteError(CLIP(xmd:Pathname) & ' Parse Error')
                END
            ELSE
                WriteError(CLIP(xmd:Pathname) & ' Unable to Read Root')
            END

            xmlExch.close()

        ELSE
            WriteError(CLIP(xmd:Pathname) & ' ERROR = ' & ResultCode)
        END
        
        IF success THEN
            IF (COMMAND('/TEST')) THEN
                RENAME(CLIP(xmd:Pathname), CLIP(xmd:Pathname) & '.OK')
            ELSE
                REMOVE(CLIP(xmd:Pathname))
            END
        ELSE
            RENAME(CLIP(xmd:Pathname), CLIP(xmd:Pathname) & '.ERROR')
        END
        access:xmldocs.deleterecord(0)
    END

    RETURN

ExportJobsResponse           PROCEDURE()
ResultCode  BYTE
xmlExch     XMLExchange
xmlNav      &XMLNavigator
success     LONG
    CODE

    ! Process putServiceRequest Response Documents

    access:xmldocs.clearkey(xmd:TypeKey)
    xmd:Type = DT_EXPORT_RESPONSE
    SET(xmd:TypeKey,xmd:TypeKey)
    LOOP
        IF ((access:xmldocs.next() <> Level:benign) OR |
            (xmd:Type <> DT_EXPORT_RESPONSE)) THEN
            BREAK
        END
        success = false;
        ResultCode = xmlExch.open(CLIP(xmd:Pathname))
        IF (ResultCode = CPXMLErr:NoError) THEN
            xmlExch.setStyle(XMLStyle:ADO_Net)
            xmlNav &= xmlExch.getNavigator()
            
            IF ((xmlNav.goToRoot() = Level:Benign) AND |
                (xmlNav.getNodeName() = 'ASREQUEST_CIC_ASC_RESPONSE')) THEN
                !LOGOUT(600, XMLJOBS)
                IF UpdateExportJobs(xmlExch, xmlNav, 'ASREQUEST_CIC_ASC_RESPONSE')  THEN
                    !COMMIT
                    !WriteError(CLIP(xmd:Pathname) & ' Parsed OK')
                    success = true
                ELSE
                    !ROLLBACK
                    WriteError(CLIP(xmd:Pathname) & ' Parse Error')
                END
            ELSE
                WriteError(CLIP(xmd:Pathname) & ' Unable to Read Root')
            END

            xmlExch.close()

        ELSE
            WriteError(CLIP(xmd:Pathname) & ' ERROR = ' & ResultCode)
        END
        
        IF success THEN
            IF (COMMAND('/TEST')) THEN
                RENAME(CLIP(xmd:Pathname), CLIP(xmd:Pathname) & '.OK')
            ELSE
                REMOVE(CLIP(xmd:Pathname))
            END
        ELSE
            RENAME(CLIP(xmd:Pathname), CLIP(xmd:Pathname) & '.ERROR')
        END
        access:xmldocs.deleterecord(0)
    END

    CloseLog()
    RETURN
copyXmlValuesToFields        PROCEDURE(XMLExchange xch, XMLNavigator nav, STRING parentNodePath)
navErr   LONG
thisNodePath STRING(256)
    CODE
    navErr = nav.goToFirstChild()
    LOOP WHILE navErr = Level:Benign
        thisNodePath  = CLIP(parentNodePath) & '/' & CLIP(nav.getNodeName())
        CASE thisNodePath
            OF  'ASREQUEST_CIC_ASC/AS_REQUEST'
                    CLEAR(MEM_RECORD)
                    IF (NOT copyXmlValuesToFields(xch, nav, CLIP(thisNodePath))) THEN
                        access:xmljobs.cancelautoinc()
                        RETURN false
                    END
                    ! Ignore Duplicate TR_NO Records (Have already been received)
                    access:xmljobs.clearkey(xjb:tr_no_key)
                    xjb:tr_no = memxjb:tr_no
                    IF (access:xmljobs.fetch(xjb:tr_no_key) <> Level:Benign) THEN
                        IF (access:xmljobs.primerecord() = Level:Benign) THEN
                            memxjb:Record_Number = xjb:Record_Number
                            memxjb:RECORD_STATE = RS_NEW_IMPORT
                            memxjb:EXCEPTION_CODE = XC_NONE
                            xjb:Record :=: MEM_RECORD
                            IF (access:xmljobs.insert() <> Level:Benign) THEN
                                access:xmljobs.cancelautoinc()
                                RETURN false
                            END
                        ELSE
                            RETURN false
                        END
                    END
            OF   'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO'
            OROF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/PRODUCT'
            OROF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/PRODUCT/PRODUCTSHORT'
            OROF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/PRODUCT/SYMPTOM'
            OROF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/SERVICE'
            OROF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/TRACKING'
            OROF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/TRREASON'
            OROF 'ASREQUEST_CIC_ASC/AS_REQUEST/CONSUMER_INFO'
            OROF 'ASREQUEST_CIC_ASC/AS_REQUEST/CONSUMER_INFO/CUSTNAME'
            OROF 'ASREQUEST_CIC_ASC/AS_REQUEST/CONSUMER_INFO/PHONE'
            OROF 'ASREQUEST_CIC_ASC/AS_REQUEST/CONSUMER_INFO/ADDRESS'
            OROF 'ASREQUEST_CIC_ASC/AS_REQUEST/DEALER_INFO'
            OROF 'ASREQUEST_CIC_ASC/AS_REQUEST/DEALER_INFO/CUSTNAME'
            OROF 'ASREQUEST_CIC_ASC/AS_REQUEST/DEALER_INFO/PHONE'
            OROF 'ASREQUEST_CIC_ASC/AS_REQUEST/DEALER_INFO/ADDRESS'
                IF (NOT copyXmlValuesToFields(xch, nav, CLIP(thisNodePath))) THEN
                    return false
                END
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/COMPANY'
                memxjb:COMPANY = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/JOB_NO'
                memxjb:TR_NO = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/ASC_CODE'
                memxjb:ASC_CODE = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/PRODUCT/PRODUCTSHORT/MODEL_CODE'
                memxjb:MODEL_CODE = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/PRODUCT/PRODUCTSHORT/SERIAL_NO'
                memxjb:SERIAL_NO = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/PRODUCT/PRODUCTSHORT/PURCHASE_DATE'
                memxjb:PURCHASE_DATE = DEFORMAT(getNodeValue(xch, nav),@D12)
            ! Start Change (01/10/2004)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/PRODUCT/SYMPTOM/SYMPTOM1_DESC'
                memxjb:SYMPTOM1_DESC = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/PRODUCT/SYMPTOM/SYMPTOM2_DESC'
                memxjb:SYMPTOM2_DESC = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/PRODUCT/SYMPTOM/SYMPTOM3_DESC'
                memxjb:SYMPTOM3_DESC = getNodeValue(xch, nav)
            ! End Change (01/10/2004)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/SERVICE/SERVICE_TYPE'
                memxjb:SERVICE_TYPE = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/IN_OUT_WARRANTY'
                memxjb:IN_OUT_WARRANTY = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/REQUEST_DATE'
                memxjb:REQUEST_DATE = DEFORMAT(getNodeValue(xch, nav),@D12)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/REQUEST_TIME'
                memxjb:REQUEST_TIME = DEFORMAT(getNodeValue(xch, nav),@T5)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/REPAIR_ETD_DATE'
                memxjb:REPAIR_ETD_DATE = DEFORMAT(getNodeValue(xch, nav),@D12)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/REPAIR_ETD_TIME'
                memxjb:REPAIR_ETD_TIME = DEFORMAT(getNodeValue(xch, nav),@T5)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/TRACKING/TR_STATUS'
                memxjb:TR_STATUS = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/TRACKING/TR_REASON'
                memxjb:TR_REASON = getNodeValue(xch, nav)
            ! Start Change 6133 BE(19/09/2005)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/ASREQUEST_INFO/INQUIRY_TEXT'
                memxjb:INQUIRY_TEXT = getNodeValue(xch, nav)
            ! End Change 6133 BE(19/09/2005)
            ! Start Change (01/10/2004) **************
            !OF 'ASREQUEST_CIC_ASC/AS_REQUEST/CONSUMER_INFO/BP_NO'
            !    memxjb:BP_NO = getNodeValue(xch, nav)
            ! End Change (01/10/2004)  ***************
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/CONSUMER_INFO/CUSTNAME/TITLE'
                memxjb:CONSUMER_TITLE = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/CONSUMER_INFO/CUSTNAME/FIRST_NAME'
                memxjb:CONSUMER_FIRST_NAME = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/CONSUMER_INFO/CUSTNAME/LAST_NAME'
                memxjb:CONSUMER_LAST_NAME = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/CONSUMER_INFO/PHONE/TEL_NUMBER1'
                memxjb:CONSUMER_TEL_NUMBER1 = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/CONSUMER_INFO/PHONE/FAX_NUMBER'
                memxjb:CONSUMER_FAX_NUMBER = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/CONSUMER_INFO/PHONE/E_MAIL'
                memxjb:CONSUMER_EMAIL = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/CONSUMER_INFO/ADDRESS/COUNTRY'
                memxjb:CONSUMER_COUNTRY = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/CONSUMER_INFO/ADDRESS/REGION'
                memxjb:CONSUMER_REGION = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/CONSUMER_INFO/ADDRESS/POST_CODE'
                memxjb:CONSUMER_POSTCODE = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/CONSUMER_INFO/ADDRESS/CITY'
                memxjb:CONSUMER_CITY = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/CONSUMER_INFO/ADDRESS/STREET'
                memxjb:CONSUMER_STREET = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC/AS_REQUEST/CONSUMER_INFO/ADDRESS/HOUSE_NUMBER'
                memxjb:CONSUMER_HOUSE_NUMBER = getNodeValue(xch, nav)
        END
        navErr = nav.goToNextSibling()
    END
    navErr = nav.goToParent()
    RETURN true


UpdateExportJobs        PROCEDURE(XMLExchange xch, XMLNavigator nav, STRING parentNodePath)
navErr   LONG
thisNodePath STRING(256)
    CODE
    navErr = nav.goToFirstChild()
    LOOP WHILE navErr = Level:Benign
        thisNodePath  = CLIP(parentNodePath) & '/' & CLIP(nav.getNodeName())
        CASE thisNodePath
            OF  'ASREQUEST_CIC_ASC_RESPONSE/ASREQUEST_RESPONSE'
                    CLEAR(MEM_EXPORT_RESPONSE_GROUP)
                    IF (NOT UpdateExportJobs(xch, nav, CLIP(thisNodePath))) THEN
                        RETURN false
                    END
                    ! Parse Job No from ERR_MSG
                    max# = LEN(CLIP(memexp:ERR_MSG))
                    end# = max#
                    LOOP ix# = 1 TO max#
                        IF (memexp:ERR_MSG[ix# : ix#] = ' ') THEN
                            end# = ix#-1
                            BREAK
                        END
                    END
                    IF (memexp:RETURN_TYPE = 'S') THEN
                        ! Update the Job Record with Samsung Ref No.
                        access:xmljobs.clearkey(xjb:State_Job_Number_Key)
                        xjb:RECORD_STATE = RS_EXPORT
                        xjb:JOB_NO = memexp:ERR_MSG[1 : end#]
                        IF (access:xmljobs.fetch(xjb:JobNumberKey) = Level:Benign) THEN
                            xjb:TR_NO = memexp:JOB_NO
                            xjb:RECORD_STATE = RS_ACTIVE_JOB
                            access:xmljobs.update()
                        END
                    ELSE
                        ! Mark the Job Record as Export Failure
                        access:xmljobs.clearkey(xjb:State_Job_Number_Key)
                        xjb:RECORD_STATE = RS_EXPORT
                        xjb:JOB_NO = memexp:ERR_MSG[1 : end#]
                        IF (access:xmljobs.fetch(xjb:State_Job_Number_Key) = Level:Benign) THEN
                            xjb:RECORD_STATE = RS_EXCEPTION
                            xjb:EXCEPTION_CODE = XC_EXPORT_FAIL
                            xjb:EXCEPTION_DESC = CLIP(memexp:ERR_MSG)
                            access:xmljobs.update()
                        END
                    END
            OF 'ASREQUEST_CIC_ASC_RESPONSE/ASREQUEST_RESPONSE/RETURN_TYPE'
                memexp:RETURN_TYPE = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC_RESPONSE/ASREQUEST_RESPONSE/JOB_NO'
                memexp:JOB_NO = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC_RESPONSE/ASREQUEST_RESPONSE/BP'
                memexp:BP_NO = getNodeValue(xch, nav)
            OF 'ASREQUEST_CIC_ASC_RESPONSE/ASREQUEST_RESPONSE/ERR_MSG'
                memexp:ERR_MSG = getNodeValue(xch, nav)
        END
        navErr = nav.goToNextSibling()
    END
    navErr = nav.goToParent()
    RETURN true
getNodeValue        PROCEDURE(XMLExchange xch, XMLNavigator nav)
ANode   &Element
TNode   &Text
    CODE
    nav.setXMLExchangeNode()
    ANode &= xch.getNode()
    TNode &= ADDRESS(ANode.getFirstChild())
    IF TNode &= NULL THEN
        RETURN ''
    ELSE
        RETURN CLIP(TNode.getData())
    END
WriteError      PROCEDURE(STRING arg:s)
    CODE
    IF (OpenLog(CLIP(PATH()) & '\xmlsvc.log')) THEN
        WriteLog(CLIP(arg:s), true)
        CloseLog()
    END
    RETURN
! Logfile Procedures
OpenLog     PROCEDURE(arg:f, arg:newflag)
    CODE
    LOGFILE{PROP:NAME} = CLIP(arg:f)

    IF (EXISTS(arg:f)) THEN
        IF (arg:newflag) THEN
            REMOVE(LOGFILE)
            CREATE(LOGFILE)
        END
    ELSE
        CREATE(LOGFILE)
    END

    OPEN(LOGFILE)
    IF (ERRORCODE()) THEN
        RETURN false
    END
    RETURN true

CloseLog     PROCEDURE()
    CODE
    CLOSE(LOGFILE)
    RETURN


WriteLog     PROCEDURE(arg:s, arg:logflag)
    CODE
    CLEAR(LOGFILE)
    IF (arg:logflag) THEN
        log:recbuff = FORMAT(TODAY(), @d06) & ' ' & FORMAT(CLOCK(), @t04) & ' ' & CLIP(arg:s)
    ELSE
        log:recbuff = CLIP(arg:s)
    END
    ADD(LOGFILE)
    RETURN
