   PROGRAM



   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFILE.INC'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE
INCLUDE('xmlclass.inc')
INCLUDE('..\..\INC\SAMXML.INC')

   MAP
     MODULE('Windows API')
SystemParametersInfo PROCEDURE (LONG uAction, LONG uParam, *? lpvParam, LONG fuWinIni),LONG,RAW,PROC,PASCAL,DLL(TRUE),NAME('SystemParametersInfoA')
     END
     MODULE('XMLSVBC.CLW')
DctInit     PROCEDURE                                      ! Initializes the dictionary definition module
DctKill     PROCEDURE                                      ! Kills the dictionary definition module
     END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('XMLSV001.CLW')
Main                   PROCEDURE   !
     END
   END

SilentRunning        BYTE(0)                               ! Set true when application is running in 'silent mode'

XMLJOBS              FILE,DRIVER('Btrieve'),OEM,NAME('XMLJOBS.DAT'),PRE(XJB),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(XJB:RECORD_NUMBER),NOCASE,PRIMARY
TR_NO_KEY                KEY(XJB:TR_NO),DUP,NOCASE
JobNumberKey             KEY(XJB:JOB_NO),DUP,NOCASE
State_Job_Number_Key     KEY(XJB:RECORD_STATE,XJB:JOB_NO),DUP,NOCASE
State_Surname_Key        KEY(XJB:RECORD_STATE,XJB:CONSUMER_LAST_NAME),DUP,NOCASE
State_Postcode_Key       KEY(XJB:RECORD_STATE,XJB:CONSUMER_POSTCODE),DUP,NOCASE
State_Model_Key          KEY(XJB:RECORD_STATE,XJB:MODEL_CODE),DUP,NOCASE
State_Serial_No_Key      KEY(XJB:RECORD_STATE,XJB:SERIAL_NO),DUP,NOCASE
Record                   RECORD,PRE()
RECORD_NUMBER               LONG
RECORD_STATE                BYTE
EXCEPTION_CODE              LONG
EXCEPTION_DESC              STRING(255)
JOB_NO                      LONG
COMPANY                     STRING(4)
TR_NO                       STRING(30)
ASC_CODE                    STRING(10)
MODEL_CODE                  STRING(40)
SERIAL_NO                   STRING(16)
PURCHASE_DATE               DATE
SERVICE_TYPE                STRING(2)
IN_OUT_WARRANTY             STRING(1)
REQUEST_DATE                DATE
REQUEST_TIME                TIME
REPAIR_ETD_DATE             DATE
REPAIR_ETD_TIME             TIME
TR_STATUS                   STRING(2)
TR_REASON                   STRING(2)
CONSUMER_TITLE              STRING(20)
CONSUMER_FIRST_NAME         STRING(40)
CONSUMER_LAST_NAME          STRING(40)
CONSUMER_TEL_NUMBER1        STRING(30)
CONSUMER_FAX_NUMBER         STRING(30)
CONSUMER_EMAIL              STRING(241)
CONSUMER_COUNTRY            STRING(20)
CONSUMER_REGION             STRING(20)
CONSUMER_POSTCODE           STRING(10)
CONSUMER_CITY               STRING(40)
CONSUMER_STREET             STRING(60)
CONSUMER_HOUSE_NUMBER       STRING(20)
REPAIR_COMPLETE_DATE        DATE
REPAIR_COMPLETE_TIME        TIME
GOODS_DELIVERY_DATE         DATE
GOODS_DELIVERY_TIME         TIME
SYMPTOM1_DESC               STRING(40)
SYMPTOM2_DESC               STRING(40)
SYMPTOM3_DESC               STRING(40)
BP_NO                       STRING(10)
INQUIRY_TEXT                STRING(254)
                         END
                     END                       

XMLDOCS              FILE,DRIVER('Btrieve'),OEM,NAME('XMLDOCS'),PRE(XMD),CREATE,BINDABLE,THREAD
RecordNoKey              KEY(XMD:RECORD_NO),NOCASE,PRIMARY
TypeKey                  KEY(XMD:TYPE),DUP,NOCASE
Record                   RECORD,PRE()
RECORD_NO                   LONG
ENTRY_DATE                  DATE
ENTRY_TIME                  TIME
TYPE                        BYTE
PATHNAME                    STRING(255)
                         END
                     END                       



glo:ErrorText        STRING(1000)
glo:owner            STRING(20)
glo:DateModify       DATE
glo:TimeModify       TIME
glo:Notes_Global     STRING(1000)
glo:Q_Invoice        QUEUE,PRE(glo)
Invoice_Number         REAL
Account_Number         STRING(15)
                     END
glo:afe_path         STRING(255)
glo:Password         STRING(20)
glo:PassAccount      STRING(30)
glo:Preview          STRING('True')
glo:q_JobNumber      QUEUE,PRE(GLO)
Job_Number_Pointer     REAL
                     END
glo:Q_PartsOrder     QUEUE,PRE(GLO)
Q_Order_Number         REAL
Q_Part_Number          STRING(30)
Q_Description          STRING(30)
Q_Supplier             STRING(30)
Q_Quantity             LONG
Q_Purchase_Cost        REAL
Q_Sale_Cost            REAL
                     END
glo:G_Select1        GROUP,PRE(GLO)
Select1                STRING(30)
Select2                STRING(40)
Select3                STRING(40)
Select4                STRING(40)
Select5                STRING(40)
Select6                STRING(40)
Select7                STRING(40)
Select8                STRING(40)
Select9                STRING(40)
Select10               STRING(40)
Select11               STRING(40)
Select12               STRING(40)
Select13               STRING(40)
Select14               STRING(40)
Select15               STRING(40)
Select16               STRING(40)
Select17               STRING(40)
Select18               STRING(40)
Select19               STRING(40)
Select20               STRING(40)
Select21               STRING(40)
Select22               STRING(40)
Select23               STRING(40)
Select24               STRING(40)
Select25               STRING(40)
Select26               STRING(40)
Select27               STRING(40)
Select28               STRING(40)
Select29               STRING(40)
Select30               STRING(40)
Select31               STRING(40)
Select32               STRING(40)
Select33               STRING(40)
Select34               STRING(40)
Select35               STRING(40)
Select36               STRING(40)
Select37               STRING(40)
Select38               STRING(40)
Select39               STRING(40)
Select40               STRING(40)
Select41               STRING(40)
Select42               STRING(40)
Select43               STRING(40)
Select44               STRING(40)
Select45               STRING(40)
Select46               STRING(40)
Select47               STRING(40)
Select48               STRING(40)
Select49               STRING(40)
Select50               STRING(40)
                     END
glo:q_RepairType     QUEUE,PRE(GLO)
Repair_Type_Pointer    STRING(30)
                     END
glo:Q_UnitType       QUEUE,PRE(GLO)
Unit_Type_Pointer      STRING(30)
                     END
glo:Q_ChargeType     QUEUE,PRE(GLO)
Charge_Type_Pointer    STRING(30)
                     END
glo:Q_ModelNumber    QUEUE,PRE(glo)
Model_Number_Pointer   STRING(30)
                     END
glo:Q_Accessory      QUEUE,PRE(GLO)
Accessory_Pointer      STRING(30)
                     END
glo:Q_AccessLevel    QUEUE,PRE(GLO)
Access_Level_Pointer   STRING(30)
                     END
glo:Q_AccessAreas    QUEUE,PRE(GLO)
Access_Areas_Pointer   STRING(30)
                     END
glo:Q_Notes          QUEUE,PRE(GLO)
notes_pointer          STRING(80)
                     END
glo:EnableFlag       STRING(3)
glo:TimeLogged       TIME
glo:GLO:ApplicationName STRING(8)
glo:GLO:ApplicationCreationDate LONG
glo:GLO:ApplicationCreationTime LONG
glo:GLO:ApplicationChangeDate LONG
glo:GLO:ApplicationChangeTime LONG
glo:GLO:Compiled32   BYTE
glo:GLO:AppINIFile   STRING(255)
glo:Queue            QUEUE,PRE(GLO)
Pointer                STRING(40)
                     END
glo:Queue2           QUEUE,PRE(GLO)
Pointer2               STRING(40)
                     END
glo:Queue3           QUEUE,PRE(GLO)
Pointer3               STRING(40)
                     END
glo:Queue4           QUEUE,PRE(GLO)
Pointer4               STRING(40)
                     END
glo:Queue5           QUEUE,PRE(GLO)
Pointer5               STRING(40)
                     END
glo:Queue6           QUEUE,PRE(GLO)
Pointer6               STRING(40)
                     END
glo:Queue7           QUEUE,PRE(GLO)
Pointer7               STRING(40)
                     END
glo:Queue8           QUEUE,PRE(GLO)
Pointer8               STRING(40)
                     END
glo:Queue9           QUEUE,PRE(GLO)
Pointer9               STRING(40)
                     END
glo:Queue10          QUEUE,PRE(GLO)
Pointer10              STRING(40)
                     END
glo:Queue11          QUEUE,PRE(GLO)
Pointer11              STRING(40)
                     END
glo:Queue12          QUEUE,PRE(GLO)
Pointer12              STRING(40)
                     END
glo:Queue13          QUEUE,PRE(GLO)
Pointer13              STRING(40)
                     END
glo:Queue14          QUEUE,PRE(GLO)
Pointer14              STRING(40)
                     END
glo:Queue15          QUEUE,PRE(GLO)
Pointer15              STRING(40)
                     END
glo:Queue16          QUEUE,PRE(GLO)
Pointer16              STRING(40)
                     END
glo:Queue17          QUEUE,PRE(GLO)
Pointer17              STRING(40)
                     END
glo:Queue18          QUEUE,PRE(GLO)
Pointer18              STRING(40)
                     END
glo:Queue19          QUEUE,PRE(GLO)
Pointer19              STRING(40)
                     END
glo:Queue20          QUEUE,PRE(GLO)
Pointer20              STRING(40)
                     END
glo:FaultCode1       STRING(30)
glo:FaultCode2       STRING(30)
glo:FaultCode3       STRING(30)
glo:FaultCode4       STRING(30)
glo:FaultCode5       STRING(30)
glo:FaultCode6       STRING(30)
glo:FaultCode7       STRING(30)
glo:FaultCode8       STRING(30)
glo:FaultCode9       STRING(30)
glo:FaultCode10      STRING(255)
glo:FaultCode11      STRING(255)
glo:FaultCode12      STRING(255)
glo:FaultCode13      STRING(30)
glo:FaultCode14      STRING(30)
glo:FaultCode15      STRING(30)
glo:FaultCode16      STRING(30)
glo:FaultCode17      STRING(30)
glo:FaultCode18      STRING(30)
glo:FaultCode19      STRING(30)
glo:FaultCode20      STRING(30)
glo:TradeFaultCode1  STRING(30)
glo:TradeFaultCode2  STRING(30)
glo:TradeFaultCode3  STRING(30)
glo:TradeFaultCode4  STRING(30)
glo:TradeFaultCode5  STRING(30)
glo:TradeFaultCode6  STRING(30)
glo:TradeFaultCode7  STRING(30)
glo:TradeFaultCode8  STRING(30)
glo:TradeFaultCode9  STRING(30)
glo:TradeFaultCode10 STRING(30)
glo:TradeFaultCode11 STRING(30)
glo:TradeFaultCode12 STRING(30)
Access:XMLJOBS       &FileManager,THREAD                   ! FileManager for XMLJOBS
Relate:XMLJOBS       &RelationManager,THREAD               ! RelationManager for XMLJOBS
Access:XMLDOCS       &FileManager,THREAD                   ! FileManager for XMLDOCS
Relate:XMLDOCS       &RelationManager,THREAD               ! RelationManager for XMLDOCS

FuzzyMatcher         FuzzyClass                            ! Global fuzzy matcher
GlobalErrors         ErrorClass,THREAD                     ! Global error manager
INIMgr               INIClass                              ! Global non-volatile storage manager
GlobalRequest        BYTE(0),THREAD                        ! Set when a browse calls a form, to let it know action to perform
GlobalResponse       BYTE(0),THREAD                        ! Set to the response from the form
VCRRequest           LONG(0),THREAD                        ! Set to the request from the VCR buttons

Dictionary           CLASS,THREAD
Construct              PROCEDURE
Destruct               PROCEDURE
                     END

lCurrentFDSetting    LONG                                  ! Used by window frame dragging
lAdjFDSetting        LONG                                  ! ditto

  CODE
  FuzzyMatcher.Init                                        ! Initilaize the browse 'fuzzy matcher'
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)            ! Configure case matching
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)          ! Configure 'word only' matching
  INIMgr.Init('XMLSVC.INI', NVD_INI)                       ! Configure INIManager to use INI file
  SystemParametersInfo (38, 0, lCurrentFDSetting, 0)       ! Configure frame dragging
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 0, lAdjFDSetting, 3)
  END
  Main
  INIMgr.Update
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 1, lAdjFDSetting, 3)
  END
  INIMgr.Kill                                              ! Destroy INI manager
  FuzzyMatcher.Kill                                        ! Destroy fuzzy matcher


Dictionary.Construct PROCEDURE

  CODE
  DctInit()


Dictionary.Destruct PROCEDURE

  CODE
  DctKill()

