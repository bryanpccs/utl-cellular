

   MEMBER('samxml.clw')                               ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SAMXM006.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SAMXM007.INC'),ONCE        !Req'd for module callout resolution
                     END


UpdateSamsungStatus PROCEDURE                         !Generated from procedure template - Window

ActionMessage        CSTRING(40)
SaveStatusCode       STRING(4)
BRW7::View:Browse    VIEW(XMLSB)
                       PROJECT(XSB:SBStatus)
                       PROJECT(XSB:FKStatusCode)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
XSB:SBStatus           LIKE(XSB:SBStatus)             !List box control field - type derived from field
XSB:FKStatusCode       LIKE(XSB:FKStatusCode)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::XSA:Record  LIKE(XSA:RECORD),STATIC
FormWindow           WINDOW('Samsung Update Status'),AT(,,287,188),FONT('Arial',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,280,152),USE(?Sheet1),SPREAD
                         TAB('Update Status'),USE(?Tab1)
                           ENTRY(@s4),AT(72,20,124,10),USE(XSA:StatusCode),REQ,UPR
                           LIST,AT(8,36,188,116),USE(?List),IMM,MSG('Browsing Records'),FORMAT('120L|M~SB Status~L(2)@s30@'),FROM(Queue:Browse)
                           BUTTON('&Insert'),AT(204,36,76,20),USE(?Insert),TRN,LEFT,ICON('insert.ico')
                           BUTTON('&Edit'),AT(204,60,76,20),USE(?Change),LEFT,ICON('edit.ico')
                           BUTTON('&Delete'),AT(204,84,76,20),USE(?Delete),TRN,LEFT,ICON('delete.ico')
                           PROMPT('Samsung Status'),AT(8,20),USE(?Prompt1),TRN
                         END
                       END
                       PANEL,AT(4,160,280,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('OK'),AT(168,164,56,16),USE(?OK),TRN,LEFT,ICON('ok.gif'),DEFAULT,REQ
                       BUTTON('Cancel'),AT(224,164,56,16),USE(?Cancel),TRN,LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
BRW7                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    If ?XSA:StatusCode{prop:ReadOnly} = True
        ?XSA:StatusCode{prop:FontColor} = 65793
        ?XSA:StatusCode{prop:Color} = 15066597
    Elsif ?XSA:StatusCode{prop:Req} = True
        ?XSA:StatusCode{prop:FontColor} = 65793
        ?XSA:StatusCode{prop:Color} = 8454143
    Else ! If ?XSA:StatusCode{prop:Req} = True
        ?XSA:StatusCode{prop:FontColor} = 65793
        ?XSA:StatusCode{prop:Color} = 16777215
    End ! If ?XSA:StatusCode{prop:Req} = True
    ?XSA:StatusCode{prop:Trn} = 0
    ?XSA:StatusCode{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateSamsungStatus')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?XSA:StatusCode
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(XSA:Record,History::XSA:Record)
  SELF.AddHistoryField(?XSA:StatusCode,1)
  SELF.AddUpdateFile(Access:XMLSAM)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:XMLSAM.Open
  Relate:XMLSAM_ALIAS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:XMLSAM
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SaveStatusCode = xsa:StatusCode
  BRW7.Init(?List,Queue:Browse.ViewPosition,BRW7::View:Browse,Queue:Browse,Relate:XMLSB,SELF)
  OPEN(FormWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  BRW7.Q &= Queue:Browse
  BRW7.AddSortOrder(,XSB:FKStatusCodeKey)
  BRW7.AddRange(XSB:FKStatusCode,XSA:StatusCode)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,XSB:FKStatusCode,1,BRW7)
  BRW7.AddField(XSB:SBStatus,BRW7.Q.XSB:SBStatus)
  BRW7.AddField(XSB:FKStatusCode,BRW7.Q.XSB:FKStatusCode)
  SELF.AddItem(ToolbarForm)
  BRW7.AskProcedure = 1
  BRW7.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:XMLSAM.Close
    Relate:XMLSAM_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateSBStatus(XSA:StatusCode)
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?XSA:StatusCode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?XSA:StatusCode, Accepted)
      IF (xsa:StatusCode <> SaveStatusCode) THEN
      !    ! Check Code is valid
      !    IF (xsa:StatusCode = '01') OR |
      !       (xsa:StatusCode = '02') OR |
      !       (xsa:StatusCode = '03') OR |
      !       (xsa:StatusCode = '04') OR |
      !       (xsa:StatusCode = '0501') OR |
      !       (xsa:StatusCode = '0502') OR |
      !       (xsa:StatusCode = '0503') OR |
      !       (xsa:StatusCode = '0504') OR |
      !       (xsa:StatusCode = '0505') OR |
      !       (xsa:StatusCode = '08') THEN
      !        ! check if key already exists
      !        access:xmlsam_alias.clearkey(xsa_ali:StatusCodeKey)
      !        xsa_ali:StatusCode = xsa:StatusCode
      !        IF (access:xmlsam_alias.fetch(xsa_ali:StatusCodeKey) = Level:Benign) THEN
      !            MESSAGE('Status Code ' & CLIP(xsa:StatusCode) & ' already exists')
      !            xsa:StatusCode = SaveStatusCode
      !            DISPLAY(?xsa:StatusCode)
      !            SELECT(?xsa:StatusCode)
      !            CYCLE
      !        END
      !    ELSE
      !        MESSAGE('Status Code ' & CLIP(xsa:StatusCode) & ' is not valid')
      !        xsa:StatusCode = SaveStatusCode
      !        DISPLAY(?xsa:StatusCode)
      !        SELECT(?xsa:StatusCode)
      !        CYCLE
      !    END
      !
      ! 20/09/2004 - Remove validation check on Samsung Status code
      !              because any status is valid.
      !
          ! check if key already exists
          access:xmlsam_alias.clearkey(xsa_ali:StatusCodeKey)
          xsa_ali:StatusCode = xsa:StatusCode
          IF (access:xmlsam_alias.fetch(xsa_ali:StatusCodeKey) = Level:Benign) THEN
              MESSAGE('Status Code ' & CLIP(xsa:StatusCode) & ' already exists')
              xsa:StatusCode = SaveStatusCode
              DISPLAY(?xsa:StatusCode)
              SELECT(?xsa:StatusCode)
              CYCLE
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?XSA:StatusCode, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW7.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

