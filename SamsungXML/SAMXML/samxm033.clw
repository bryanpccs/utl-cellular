

   MEMBER('samxml.clw')                               ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SAMXM033.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SAMXM031.INC'),ONCE        !Req'd for module callout resolution
                     END


MPXBrowseStatus PROCEDURE                             !Generated from procedure template - Window

ThisThreadActive BYTE
ReportQueue          QUEUE,PRE(rpq)
MpxReportName        STRING(30)
MpxReportCode        LONG
                     END
CurrentMpxReportCode LONG
CurrentMpxReportName STRING(30)
BRW1::View:Browse    VIEW(MPXSTAT)
                       PROJECT(MXS:SB_STATUS)
                       PROJECT(MXS:RecordNo)
                       PROJECT(MXS:MPX_STATUS)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
MXS:SB_STATUS          LIKE(MXS:SB_STATUS)            !List box control field - type derived from field
MXS:RecordNo           LIKE(MXS:RecordNo)             !Primary key field - type derived from field
MXS:MPX_STATUS         LIKE(MXS:MPX_STATUS)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Browse
mo:SelectedField  Long
BrowseWindow         WINDOW('MPX Browse Report Status'),AT(,,271,229),FONT('Arial',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,180,220),USE(?Sheet1),SPREAD
                         TAB,USE(?Tab1)
                           PROMPT('MPX Report'),AT(8,20),USE(?Prompt1)
                           COMBO(@s30),AT(52,20,124,10),USE(CurrentMpxReportName),UPR,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(ReportQueue)
                           LIST,AT(8,36,168,184),USE(?List),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~ServiceBase Status~@s30@'),FROM(Queue:Browse)
                         END
                       END
                       BUTTON('&Insert'),AT(192,36,76,20),USE(?Insert),LEFT,KEY(InsertKey),ICON('insert.ico')
                       BUTTON('&Edit'),AT(192,60,76,20),USE(?Change),LEFT,KEY(CtrlEnter),ICON('edit.ico'),DEFAULT
                       BUTTON('&Delete'),AT(192,84,76,20),USE(?Delete),LEFT,KEY(DeleteKey),ICON('delete.ico')
                       BUTTON('&Select'),AT(192,4,76,20),USE(?Select),LEFT,KEY(EnterKey),ICON('select.ico')
                       BUTTON('&Close'),AT(192,204,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('MPXBrowseStatus')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='MPXBrowseStatus'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  ! Initialise MPX Report Code Table
  !
  rpq:MpxReportCode = MPX_NEW_JOB
  rpq:MpxReportName = 'Envelope Sent'
  add(ReportQueue)
  clear(ReportQueue)
  rpq:MpxReportCode = MPX_UNIT_RECEIVED
  rpq:MpxReportName = 'Unit Received'
  add(ReportQueue)
  clear(ReportQueue)
  rpq:MpxReportCode = MPX_UNIT_INSPECTED
  rpq:MpxReportName = 'Unit Inspected'
  add(ReportQueue)
  clear(ReportQueue)
  rpq:MpxReportCode = MPX_UNIT_DESPATCHED
  rpq:MpxReportName = 'Unit Despatched'
  add(ReportQueue)
  
  get(ReportQueue, 1)
  CurrentMpxReportCode = rpq:MpxReportCode
  CurrentMpxReportName = rpq:MpxReportName
  
  
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MPXSTAT.Open
  SELF.FilesOpened = True
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW1::View:Browse,Queue:Browse,Relate:MPXSTAT,SELF)
  OPEN(BrowseWindow)
  SELF.Opened=True
  ?List{prop:vcr} = TRUE
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,MXS:MpxStatusKey)
  BRW1.AddRange(MXS:MPX_STATUS,rpq:MpxReportCode)
  BRW1.AddField(MXS:SB_STATUS,BRW1.Q.MXS:SB_STATUS)
  BRW1.AddField(MXS:RecordNo,BRW1.Q.MXS:RecordNo)
  BRW1.AddField(MXS:MPX_STATUS,BRW1.Q.MXS:MPX_STATUS)
  ThisMakeover.SetWindow(Win:Browse)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Browse,mo:SelectedTab,?Sheet1)
  BrowseWindow{prop:buffer} = 1
  BRW1.AskProcedure = 1
  BRW1.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
    ThisMakeover.Refresh()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MPXSTAT.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='MPXBrowseStatus'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    MPXUpdateStatus(CurrentMpxReportCode, CurrentMpxReportName)
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisMakeover.TakeEvent(Win:Browse,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Browse,mo:SelectedTab,Field())
    OF ?CurrentMpxReportName
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentMpxReportName, NewSelection)
      clear(ReportQueue)
      rpq:MpxReportName = CurrentMpxReportName
      get(ReportQueue, rpq:MpxReportName)
      CurrentMpxReportCode = rpq:MpxReportCode
      
      brw1.Reset(true)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentMpxReportName, NewSelection)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:GainFocus
      IF BrowseWindow{PROP:Iconize}=TRUE
        BrowseWindow{PROP:Iconize}=FALSE
        IF BrowseWindow{PROP:Active}<>TRUE
           BrowseWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end

