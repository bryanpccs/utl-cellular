

   MEMBER('samxml.clw')                               ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SAMXM008.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SAMXM009.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseTradeAccounts PROCEDURE                         !Generated from procedure template - Window

ThisThreadActive BYTE
LocalCompanyName     STRING(30)
LocalTelephoneNumber STRING(15)
LocalFaxNumber       STRING(15)
BRW1::View:Browse    VIEW(XMLTRADE)
                       PROJECT(XTR:AccountNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
XTR:AccountNumber      LIKE(XTR:AccountNumber)        !List box control field - type derived from field
LocalCompanyName       LIKE(LocalCompanyName)         !List box control field - type derived from local data
LocalTelephoneNumber   LIKE(LocalTelephoneNumber)     !List box control field - type derived from local data
LocalFaxNumber         LIKE(LocalFaxNumber)           !List box control field - type derived from local data
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BrowseWindow         WINDOW('Browse the Samsung Trade Account File'),AT(,,415,206),FONT('Arial',8,,),CENTER,GRAY,DOUBLE
                       BUTTON('&Insert'),AT(336,112,76,20),USE(?Insert),LEFT,KEY(InsertKey),ICON('insert.ico')
                       BUTTON('&Edit'),AT(336,136,76,20),USE(?Change),LEFT,KEY(CtrlEnter),ICON('edit.ico'),DEFAULT
                       BUTTON('&Delete'),AT(336,160,76,20),USE(?Delete),LEFT,KEY(DeleteKey),ICON('delete.ico')
                       BUTTON('&Select'),AT(336,88,76,20),USE(?Select),LEFT,KEY(EnterKey),ICON('select.ico')
                       BUTTON('Close'),AT(336,184,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                       SHEET,AT(4,4,328,200),USE(?Sheet1),SPREAD
                         TAB('By Account Number'),USE(?Tab1)
                           ENTRY(@s15),AT(8,16,124,10),USE(XTR:AccountNumber),UPR
                           LIST,AT(8,32,316,168),USE(?List),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('60L|M~Account Number~L(2)@s15@120L|M~Company Name~L(2)@s30@60L|M~Telephone Numbe' &|
   'r~L(2)@s15@60L|M~Fax Number~L(2)@s15@'),FROM(Queue:Browse)
                         END
                       END
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    If ?XTR:AccountNumber{prop:ReadOnly} = True
        ?XTR:AccountNumber{prop:FontColor} = 65793
        ?XTR:AccountNumber{prop:Color} = 15066597
    Elsif ?XTR:AccountNumber{prop:Req} = True
        ?XTR:AccountNumber{prop:FontColor} = 65793
        ?XTR:AccountNumber{prop:Color} = 8454143
    Else ! If ?XTR:AccountNumber{prop:Req} = True
        ?XTR:AccountNumber{prop:FontColor} = 65793
        ?XTR:AccountNumber{prop:Color} = 16777215
    End ! If ?XTR:AccountNumber{prop:Req} = True
    ?XTR:AccountNumber{prop:Trn} = 0
    ?XTR:AccountNumber{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseTradeAccounts')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Insert
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='BrowseTradeAccounts'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:TRADEACC.Open
  Relate:XMLTRADE.Open
  SELF.FilesOpened = True
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW1::View:Browse,Queue:Browse,Relate:XMLTRADE,SELF)
  OPEN(BrowseWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,XTR:AccountNumberKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?XTR:AccountNumber,XTR:AccountNumber,1,BRW1)
  BIND('LocalCompanyName',LocalCompanyName)
  BIND('LocalTelephoneNumber',LocalTelephoneNumber)
  BIND('LocalFaxNumber',LocalFaxNumber)
  BRW1.AddField(XTR:AccountNumber,BRW1.Q.XTR:AccountNumber)
  BRW1.AddField(LocalCompanyName,BRW1.Q.LocalCompanyName)
  BRW1.AddField(LocalTelephoneNumber,BRW1.Q.LocalTelephoneNumber)
  BRW1.AddField(LocalFaxNumber,BRW1.Q.LocalFaxNumber)
  BRW1.AskProcedure = 1
  BRW1.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TRADEACC.Close
    Relate:XMLTRADE.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='BrowseTradeAccounts'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateTradeAccount
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:GainFocus
      IF BrowseWindow{PROP:Iconize}=TRUE
        BrowseWindow{PROP:Iconize}=FALSE
        IF BrowseWindow{PROP:Active}<>TRUE
           BrowseWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())
  PARENT.SetQueueRecord
  ! Lookup fields  from Trade Account File
  access:tradeacc.clearkey(sub:Account_Number_Key)
  tra:Account_Number = BRW1.Q.XTR:AccountNumber
  IF (access:tradeacc.fetch(tra:Account_Number_Key) = Level:Benign) THEN
      BRW1.Q.LocalCompanyName      = tra:Company_Name
      BRW1.Q.LocalTelephoneNumber  = tra:Telephone_Number
      BRW1.Q.LocalFaxNumber        = tra:Fax_Number
  END
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

