

   MEMBER('samxml.clw')                               ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SAMXM010.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SAMXM011.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseExceptions PROCEDURE                            !Generated from procedure template - Window

ThisThreadActive BYTE
LocalAddress         STRING(30)
ExceptionState       BYTE
BRW1::View:Browse    VIEW(XMLJOBS)
                       PROJECT(XJB:CONSUMER_LAST_NAME)
                       PROJECT(XJB:CONSUMER_POSTCODE)
                       PROJECT(XJB:MODEL_CODE)
                       PROJECT(XJB:SERIAL_NO)
                       PROJECT(XJB:REQUEST_DATE)
                       PROJECT(XJB:REQUEST_TIME)
                       PROJECT(XJB:RECORD_NUMBER)
                       PROJECT(XJB:RECORD_STATE)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
XJB:CONSUMER_LAST_NAME LIKE(XJB:CONSUMER_LAST_NAME)   !List box control field - type derived from field
XJB:CONSUMER_POSTCODE  LIKE(XJB:CONSUMER_POSTCODE)    !List box control field - type derived from field
XJB:MODEL_CODE         LIKE(XJB:MODEL_CODE)           !List box control field - type derived from field
XJB:SERIAL_NO          LIKE(XJB:SERIAL_NO)            !List box control field - type derived from field
XJB:REQUEST_DATE       LIKE(XJB:REQUEST_DATE)         !List box control field - type derived from field
XJB:REQUEST_TIME       LIKE(XJB:REQUEST_TIME)         !List box control field - type derived from field
LocalAddress           LIKE(LocalAddress)             !List box control field - type derived from local data
XJB:RECORD_NUMBER      LIKE(XJB:RECORD_NUMBER)        !Primary key field - type derived from field
XJB:RECORD_STATE       LIKE(XJB:RECORD_STATE)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BrowseWindow         WINDOW('Browse Samsung Exceptions'),AT(,,528,251),FONT('Arial',8,,),CENTER,GRAY,DOUBLE
                       BUTTON('&Insert'),AT(448,180,76,20),USE(?Insert),TRN,LEFT,KEY(InsertKey),ICON('insert.ico')
                       BUTTON('Amend Detail'),AT(448,40,76,20),USE(?Change),TRN,LEFT,KEY(CtrlEnter),ICON('edit.ico'),DEFAULT
                       BUTTON('Cancel Job'),AT(448,64,76,20),USE(?CancelJobButton),TRN,LEFT,ICON('Stop.ico')
                       BUTTON('Duplicated Job'),AT(448,112,76,20),USE(?DuplicateJobButton),TRN,HIDE,LEFT,ICON('FailClam.ico')
                       BUTTON('Reprocess All Jobs'),AT(448,88,76,20),USE(?ReprocessAllButton),LEFT,ICON('ok.ico')
                       BUTTON('&Delete'),AT(448,204,76,20),USE(?Delete),TRN,LEFT,KEY(DeleteKey),ICON('delete.ico')
                       LIST,AT(8,36,428,208),USE(?List),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('100L|M~Surname~L(2)@s30@40L|M~Postcode~L(2)@s10@59L|M~Model Number~L(2)@s30@53L|' &|
   'M~MSN~L(2)@s20@40L|M~Date~L(2)@d6b@20L|M~Time~L(2)@t1@120L|M~Address~L(2)@s30@'),FROM(Queue:Browse)
                       SHEET,AT(4,4,436,244),USE(?CurrentTab),SPREAD
                         TAB('By Surname'),USE(?Tab1)
                           ENTRY(@s30),AT(8,20,124,10),USE(XJB:CONSUMER_LAST_NAME),UPR
                         END
                         TAB('By Postcode'),USE(?Tab2)
                           ENTRY(@s10),AT(8,20,124,10),USE(XJB:CONSUMER_POSTCODE),UPR
                         END
                         TAB('By Model'),USE(?Tab3)
                           ENTRY(@s30),AT(8,20,124,10),USE(XJB:MODEL_CODE),UPR
                         END
                         TAB('By MSN'),USE(?Tab4)
                           ENTRY(@s20),AT(8,20,124,10),USE(XJB:SERIAL_NO),UPR
                         END
                       END
                       BUTTON('&Select'),AT(448,156,76,20),USE(?Select),TRN,HIDE,LEFT,KEY(EnterKey),ICON('select.ico')
                       BUTTON('Close'),AT(448,228,76,20),USE(?Close),TRN,LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab)=1
BRW1::Sort2:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab)=2
BRW1::Sort3:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab)=3
BRW1::Sort4:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab)=4
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    If ?XJB:CONSUMER_LAST_NAME{prop:ReadOnly} = True
        ?XJB:CONSUMER_LAST_NAME{prop:FontColor} = 65793
        ?XJB:CONSUMER_LAST_NAME{prop:Color} = 15066597
    Elsif ?XJB:CONSUMER_LAST_NAME{prop:Req} = True
        ?XJB:CONSUMER_LAST_NAME{prop:FontColor} = 65793
        ?XJB:CONSUMER_LAST_NAME{prop:Color} = 8454143
    Else ! If ?XJB:CONSUMER_LAST_NAME{prop:Req} = True
        ?XJB:CONSUMER_LAST_NAME{prop:FontColor} = 65793
        ?XJB:CONSUMER_LAST_NAME{prop:Color} = 16777215
    End ! If ?XJB:CONSUMER_LAST_NAME{prop:Req} = True
    ?XJB:CONSUMER_LAST_NAME{prop:Trn} = 0
    ?XJB:CONSUMER_LAST_NAME{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    If ?XJB:CONSUMER_POSTCODE{prop:ReadOnly} = True
        ?XJB:CONSUMER_POSTCODE{prop:FontColor} = 65793
        ?XJB:CONSUMER_POSTCODE{prop:Color} = 15066597
    Elsif ?XJB:CONSUMER_POSTCODE{prop:Req} = True
        ?XJB:CONSUMER_POSTCODE{prop:FontColor} = 65793
        ?XJB:CONSUMER_POSTCODE{prop:Color} = 8454143
    Else ! If ?XJB:CONSUMER_POSTCODE{prop:Req} = True
        ?XJB:CONSUMER_POSTCODE{prop:FontColor} = 65793
        ?XJB:CONSUMER_POSTCODE{prop:Color} = 16777215
    End ! If ?XJB:CONSUMER_POSTCODE{prop:Req} = True
    ?XJB:CONSUMER_POSTCODE{prop:Trn} = 0
    ?XJB:CONSUMER_POSTCODE{prop:FontStyle} = font:Bold
    ?Tab3{prop:Color} = 15066597
    If ?XJB:MODEL_CODE{prop:ReadOnly} = True
        ?XJB:MODEL_CODE{prop:FontColor} = 65793
        ?XJB:MODEL_CODE{prop:Color} = 15066597
    Elsif ?XJB:MODEL_CODE{prop:Req} = True
        ?XJB:MODEL_CODE{prop:FontColor} = 65793
        ?XJB:MODEL_CODE{prop:Color} = 8454143
    Else ! If ?XJB:MODEL_CODE{prop:Req} = True
        ?XJB:MODEL_CODE{prop:FontColor} = 65793
        ?XJB:MODEL_CODE{prop:Color} = 16777215
    End ! If ?XJB:MODEL_CODE{prop:Req} = True
    ?XJB:MODEL_CODE{prop:Trn} = 0
    ?XJB:MODEL_CODE{prop:FontStyle} = font:Bold
    ?Tab4{prop:Color} = 15066597
    If ?XJB:SERIAL_NO{prop:ReadOnly} = True
        ?XJB:SERIAL_NO{prop:FontColor} = 65793
        ?XJB:SERIAL_NO{prop:Color} = 15066597
    Elsif ?XJB:SERIAL_NO{prop:Req} = True
        ?XJB:SERIAL_NO{prop:FontColor} = 65793
        ?XJB:SERIAL_NO{prop:Color} = 8454143
    Else ! If ?XJB:SERIAL_NO{prop:Req} = True
        ?XJB:SERIAL_NO{prop:FontColor} = 65793
        ?XJB:SERIAL_NO{prop:Color} = 16777215
    End ! If ?XJB:SERIAL_NO{prop:Req} = True
    ?XJB:SERIAL_NO{prop:Trn} = 0
    ?XJB:SERIAL_NO{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowseExceptions')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Insert
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='BrowseExceptions'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:XMLJOBS.Open
  Relate:XMLJOBS_ALIAS.Open
  SELF.FilesOpened = True
  ExceptionState = RS_EXCEPTION
  
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW1::View:Browse,Queue:Browse,Relate:XMLJOBS,SELF)
  OPEN(BrowseWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,XJB:State_Surname_Key)
  BRW1.AddRange(XJB:RECORD_STATE,ExceptionState)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?XJB:CONSUMER_LAST_NAME,XJB:CONSUMER_LAST_NAME,1,BRW1)
  BRW1.AddSortOrder(,XJB:State_Postcode_Key)
  BRW1.AddRange(XJB:RECORD_STATE,ExceptionState)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?XJB:CONSUMER_POSTCODE,XJB:CONSUMER_POSTCODE,1,BRW1)
  BRW1.AddSortOrder(,XJB:State_Model_Key)
  BRW1.AddRange(XJB:RECORD_STATE,ExceptionState)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?XJB:MODEL_CODE,XJB:MODEL_CODE,1,BRW1)
  BRW1.AddSortOrder(,XJB:State_Serial_No_Key)
  BRW1.AddRange(XJB:RECORD_STATE,ExceptionState)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?XJB:SERIAL_NO,XJB:SERIAL_NO,1,BRW1)
  BRW1.AddSortOrder(,XJB:State_Surname_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,XJB:RECORD_STATE,1,BRW1)
  BIND('LocalAddress',LocalAddress)
  BRW1.AddField(XJB:CONSUMER_LAST_NAME,BRW1.Q.XJB:CONSUMER_LAST_NAME)
  BRW1.AddField(XJB:CONSUMER_POSTCODE,BRW1.Q.XJB:CONSUMER_POSTCODE)
  BRW1.AddField(XJB:MODEL_CODE,BRW1.Q.XJB:MODEL_CODE)
  BRW1.AddField(XJB:SERIAL_NO,BRW1.Q.XJB:SERIAL_NO)
  BRW1.AddField(XJB:REQUEST_DATE,BRW1.Q.XJB:REQUEST_DATE)
  BRW1.AddField(XJB:REQUEST_TIME,BRW1.Q.XJB:REQUEST_TIME)
  BRW1.AddField(LocalAddress,BRW1.Q.LocalAddress)
  BRW1.AddField(XJB:RECORD_NUMBER,BRW1.Q.XJB:RECORD_NUMBER)
  BRW1.AddField(XJB:RECORD_STATE,BRW1.Q.XJB:RECORD_STATE)
  BRW1.AskProcedure = 1
  BRW1.AddToolbarTarget(Toolbar)
  HIDE(?Insert)
  HIDE(?Delete)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:XMLJOBS.Close
    Relate:XMLJOBS_ALIAS.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='BrowseExceptions'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateException
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CancelJobButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelJobButton, Accepted)
      if records(brw1.q) > 0 then
          IF (xjb:EXCEPTION_CODE = XC_EXPORT_FAIL) THEN
              ! Export Failure:
              ! Job does not have a TR_NO so status change will be rejected anyway
              ! so just mark job as completed
              xjb:RECORD_STATE = RS_COMPLETED
          ELSE
              ! Import Failure:
              ! Send notification of cancellation to Samsung
              xjb:RECORD_STATE = RS_CANCELLED
          END
      
          xjb:EXCEPTION_CODE = XC_NONE
          xjb:EXCEPTION_DESC = ''
      
          access:xmljobs.update()
          brw1.resetsort(1)
      end
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelJobButton, Accepted)
    OF ?ReprocessAllButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReprocessAllButton, Accepted)
      access:xmljobs_alias.clearkey(xjb_ali:State_Job_Number_Key)
      xjb_ali:RECORD_STATE = RS_EXCEPTION
      SET(xjb_ali:State_Job_Number_Key,xjb_ali:State_Job_Number_Key)
      LOOP
          IF ((access:xmljobs_alias.next() <> Level:Benign) OR |
              (xjb_ali:RECORD_STATE <> RS_EXCEPTION)) THEN
              BREAK
          END
          IF (xjb_ali:EXCEPTION_CODE = XC_EXPORT_FAIL) THEN
              xjb_ali:RECORD_STATE = RS_NEW_EXPORT
          ELSE
              xjb_ali:RECORD_STATE = RS_NEW_IMPORT
          END
          xjb_ali:EXCEPTION_CODE = XC_NONE
          xjb_ali:EXCEPTION_DESC = ''
          access:xmljobs_alias.update()
      END
      
      brw1.resetsort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReprocessAllButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:GainFocus
      IF BrowseWindow{PROP:Iconize}=TRUE
        BrowseWindow{PROP:Iconize}=FALSE
        IF BrowseWindow{PROP:Active}<>TRUE
           BrowseWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, Init, (SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM))
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END
  SELF.InsertControl = 0
  SELF.DeleteControl = 0
  
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, Init, (SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM))


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab)=1
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab)=2
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab)=3
    RETURN SELF.SetSort(3,Force)
  ELSIF CHOICE(?CurrentTab)=4
    RETURN SELF.SetSort(4,Force)
  ELSE
    RETURN SELF.SetSort(5,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())
  PARENT.SetQueueRecord
  IF (CLIP(xjb:Consumer_House_Number) = '') THEN
      brw1.q.LocalAddress = CLIP(xjb:Consumer_Street)
  ELSE
      brw1.q.LocalAddress = CLIP(xjb:Consumer_House_Number) & ' ' & CLIP(xjb:Consumer_Street)
  END
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

