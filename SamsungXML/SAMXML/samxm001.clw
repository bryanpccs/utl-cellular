

   MEMBER('samxml.clw')                               ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SAMXM001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SAMXM002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('SAMXM004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('SAMXM005.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('SAMXM008.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('SAMXM010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('SAMXM012.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('SAMXM013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('SAMXM016.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('SAMXM017.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('SAMXM018.INC'),ONCE        !Req'd for module callout resolution
                     END


Main PROCEDURE                                        !Generated from procedure template - Frame

AllowUserToExit      BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
AppFrame             APPLICATION('ServiceBase 2000 Cellular ACR Logistics XML Messaging'),AT(,,647,319),FONT('Arial',8,,),CENTER,IMM,HVSCROLL,ICON('Pc.ico'),ALRT(F12Key),STATUS(-1,-1,0),SYSTEM,MAX,MAXIMIZE,RESIZE
                       MENUBAR
                         MENU('File'),USE(?File)
                           ITEM('&Print Setup ...'),USE(?PrintSetup),MSG('Setup printer'),STD(STD:PrintSetup)
                           ITEM,SEPARATOR
                           ITEM('&Re-Login'),USE(?ReLogin)
                           ITEM,SEPARATOR
                           ITEM('E&xit'),USE(?Exit),MSG('Exit this application'),STD(STD:Close)
                         END
                         MENU('&Edit')
                           ITEM('Cu&t'),USE(?Cut),MSG('Remove item to Windows Clipboard'),STD(STD:Cut)
                           ITEM('&Copy'),USE(?Copy),MSG('Copy item to Windows Clipboard'),STD(STD:Copy)
                           ITEM('&Paste'),USE(?Paste),MSG('Paste contents of Windows Clipboard'),STD(STD:Paste)
                         END
                         MENU('System Administration'),USE(?SystemAdministration)
                           MENU('Samsung'),USE(?SystemAdministrationSamsung)
                             ITEM('Import Defaults'),USE(?ImportDefaults)
                             ITEM('Trade Accounts'),USE(?SystemAdministrationTradeAccounts)
                             ITEM('Status Codes'),USE(?StatusCodes)
                           END
                           MENU('MPX'),USE(?SystemAdministrationMPX)
                             ITEM('Trade Account Defaults'),USE(?MPXTradeAccounts)
                             ITEM('Status Codes'),USE(?MPXStatusCodes)
                           END
                         END
                         MENU('&Window'),MSG('Create and Arrange windows'),STD(STD:WindowList)
                           ITEM('T&ile'),USE(?Tile),MSG('Make all open windows visible'),STD(STD:TileWindow)
                           ITEM('&Cascade'),USE(?Cascade),MSG('Stack all open windows'),STD(STD:CascadeWindow)
                           ITEM('&Arrange Icons'),USE(?Arrange),MSG('Align all window icons'),STD(STD:ArrangeIcons)
                         END
                         MENU('&Help'),USE(?Help),RIGHT
                         END
                       END
                       TOOLBAR,AT(0,0,,32),COLOR(COLOR:Silver),TILED
                         BUTTON('Exit'),AT(568,4,76,24),USE(?Button6),LEFT,ICON('exitcomp.gif'),STD(STD:Close)
                         BUTTON('MPX Browse All'),AT(380,4,84,24),USE(?MPXBrowseAllButton),HIDE
                         BUTTON('Samsung Browse All'),AT(288,4,84,24),USE(?SamsungBrowseAllButton),HIDE
                         BUTTON('Browse MPX Exceptions'),AT(104,4,96,24),USE(?BrowseMPXExceptions),LEFT,ICON('stock_br.gif')
                         BUTTON('Browse Samsung Exceptions'),AT(4,4,96,24),USE(?BrowseExceptions),LEFT,ICON('stock_br.gif')
                       END
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
Login      Routine
    glo:select1 = 'N'
    Insert_Password
    If glo:select1 = 'Y'
        Halt
    Else
        access:users.open
        access:users.usefile
        access:users.clearkey(use:password_key)
        use:password =glo:password
        access:users.fetch(use:password_key)
        set(defaults)
        access:defaults.next()
        0{prop:statustext,2} = 'Current User: ' & Capitalize(Clip(use:forename) & ' ' & Clip(use:surname))
        If glo:password <> 'JOB:ENTER'
            Do Security_Check
        End
        access:users.close
    End
    glo:select1= ''
Log_out      Routine
    access:users.open
    access:users.usefile
    access:logged.open
    access:logged.usefile
    access:users.clearkey(use:password_key)
    use:password =glo:password
    if access:users.fetch(use:password_key) = Level:Benign
        access:logged.clearkey(log:user_code_key)
        log:user_code = use:user_code
        log:time      = glo:timelogged
        if access:logged.fetch(log:user_code_key) = Level:Benign
            delete(logged)
        end !if
    end !if
    access:users.close
    access:logged.close
Security_Check      Routine
    If SecurityCheck('MENU - FILE') = Level:Benign
        enable(?file)
    Else!If SecurityCheck('MENU - FILE') = Level:Benign
        disable(?file)
    End!If SecurityCheck('MENU - FILE') = Level:Benign

    If SecurityCheck('PRINT SETUP') = Level:Benign
        enable(?printsetup)
    Else!If SecurityCheck('PRINT SETUP') = Level:Benign
        disable(?printsetup)
    End!If SecurityCheck('PRINT SETUP') = Level:Benign

    If SecurityCheck('RE-LOGIN') = Level:Benign
        enable(?relogin)
    Else!If SecurityCheck('RE-LOGIN') = Level:Benign
        disable(?relogin)
    End!If SecurityCheck('RE-LOGIN') = Level:Benign
Menu::File ROUTINE                                    !Code for menu items on ?File
  CASE ACCEPTED()
  OF ?ReLogin
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReLogin, Accepted)
    Do Log_out
    Do Login
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReLogin, Accepted)
  END
Menu::SystemAdministration ROUTINE                    !Code for menu items on ?SystemAdministration
Menu::SystemAdministrationSamsung ROUTINE             !Code for menu items on ?SystemAdministrationSamsung
  CASE ACCEPTED()
  OF ?ImportDefaults
    UpdateImportDefaults
  OF ?SystemAdministrationTradeAccounts
    BrowseTradeAccounts
  OF ?StatusCodes
    BrowseSamsungStatus
  END
Menu::SystemAdministrationMPX ROUTINE                 !Code for menu items on ?SystemAdministrationMPX
  CASE ACCEPTED()
  OF ?MPXTradeAccounts
    MPXBrowseTradeAccounts
  OF ?MPXStatusCodes
    MPXBrowseStatus
  END
Menu::Help ROUTINE                                    !Code for menu items on ?Help
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  AppFrame{Prop:StatusText,1}=CLIP('PC Control Systems Limited �2000')
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:LOGGED.Open
  Relate:USERS.Open
  Relate:USERS_ALIAS.Open
  Relate:XMLJOBS.Open
  SELF.FilesOpened = True
  OPEN(AppFrame)
  SELF.Opened=True
  Do RecolourWindow
  ! Start Change 6133 BE(19/09/2005)
  !
  ! Cellular.txt has been moved so this cloned version of start_screen
  ! no longer works.  Removed call for expediency.
  !
  !Start_Screen
  !If glo:select1  <> 'OK'
  !    Halt
  !End!If glo:select1  <> 'OK'
  ! End Change 6133 BE(19/09/2005)
  
  glo:Select1 = ''
  Do Login
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Do log_out
    Relate:ACCAREAS_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:LOGGED.Close
    Relate:USERS.Close
    Relate:USERS_ALIAS.Close
    Relate:XMLJOBS.Close
  END
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    ELSE
      DO Menu::File                                   !Process menu items on ?File menu
      DO Menu::SystemAdministration                   !Process menu items on ?SystemAdministration menu
      DO Menu::SystemAdministrationSamsung            !Process menu items on ?SystemAdministrationSamsung menu
      DO Menu::SystemAdministrationMPX                !Process menu items on ?SystemAdministrationMPX menu
      DO Menu::Help                                   !Process menu items on ?Help menu
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?MPXBrowseAllButton
      MPXBrowseAllRecords
    OF ?SamsungBrowseAllButton
      BrowseAllRecords
    OF ?BrowseMPXExceptions
      MPXBrowseExceptions
    OF ?BrowseExceptions
      BrowseExceptions
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Do Log_out
      Do Login
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
         AppFrame{Prop:Active}=1
    OF Event:Timer
       AppFrame{Prop:StatusText,1}=CLIP('PC Control Systems Limited �2000')
    ELSE
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

