

   MEMBER('samxml.clw')                               ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SAMXM028.INC'),ONCE        !Local module procedure declarations
                     END


MPXUpdateTradeAccount PROCEDURE                       !Generated from procedure template - Window

ActionMessage        CSTRING(40)
SaveAccountNumber    STRING(15)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?MXD:UserCode
use:User_Code          LIKE(use:User_Code)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?MXD:TransitType
trt:Transit_Type       LIKE(trt:Transit_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?MXD:UnitType
uni:Unit_Type          LIKE(uni:Unit_Type)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB7::View:FileDropCombo VIEW(USERS)
                       PROJECT(use:User_Code)
                     END
FDCB8::View:FileDropCombo VIEW(TRANTYPE)
                       PROJECT(trt:Transit_Type)
                     END
FDCB9::View:FileDropCombo VIEW(UNITTYPE)
                       PROJECT(uni:Unit_Type)
                     END
History::MXD:Record  LIKE(MXD:RECORD),STATIC
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Form
mo:SelectedField  Long
FormWindow           WINDOW('MPX Trade Account Defaults Update'),AT(,,255,256),FONT('Arial',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,248,220),USE(?Sheet1),SPREAD
                         TAB,USE(?Tab1)
                           PROMPT('Trade Account'),AT(12,20),USE(?PromptTradeAccount),TRN
                           ENTRY(@s15),AT(100,20,124,10),USE(MXD:AccountNo),UPR
                           BUTTON('...'),AT(228,20,10,10),USE(?LookupTracdeAccount),LEFT,ICON('list3.ico')
                           PROMPT('User'),AT(12,36),USE(?PromptUser),TRN
                           COMBO(@s3),AT(100,36,124,12),USE(MXD:UserCode),IMM,VSCROLL,UPR,FORMAT('12L(2)|M@s3@'),DROP(10),FROM(Queue:FileDropCombo)
                           CHECK('Chargeable Job {15}'),AT(12,52),USE(MXD:IsChargeable),TRN,LEFT,VALUE('1','0')
                           PROMPT('Chargeable Charge Type'),AT(12,68),USE(?PromptChaChargeType),TRN
                           ENTRY(@s30),AT(100,68,124,10),USE(MXD:ChargeType),UPR
                           BUTTON('...'),AT(228,68,10,10),USE(?LookupChargeType),LEFT,ICON('list3.ico')
                           CHECK('Warranty Job {19}'),AT(12,84),USE(MXD:IsWarranty),TRN,LEFT,VALUE('1','0')
                           PROMPT('Warranty Charge Type'),AT(12,100),USE(?PromptWarChargeType),TRN
                           ENTRY(@s30),AT(100,100,124,10),USE(MXD:WarrantyType),UPR
                           BUTTON('...'),AT(228,100,10,10),USE(?LookupWarantyType),LEFT,ICON('list3.ico')
                           PROMPT('Transit Type'),AT(12,116),USE(?PromptTransitType),TRN
                           COMBO(@s30),AT(100,116,124,10),USE(MXD:TransitType),IMM,VSCROLL,UPR,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:1)
                           PROMPT('Unit Type'),AT(12,132),USE(?PromptUnitType),TRN
                           COMBO(@s30),AT(100,132,124,10),USE(MXD:UnitType),IMM,VSCROLL,UPR,FORMAT('120L|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:2)
                           PROMPT('Delivery text'),AT(12,196),USE(?PromptDeliveryText),TRN
                           TEXT,AT(100,196,124,24),USE(MXD:DeliveryText),VSCROLL,UPR
                           PROMPT('Status'),AT(12,148),USE(?PromptStatus),TRN
                           ENTRY(@s30),AT(100,148,124,10),USE(MXD:Status),UPR
                           BUTTON('...'),AT(228,148,10,10),USE(?LookupStatus),LEFT,ICON('list3.ico')
                           PROMPT('Location'),AT(12,164),USE(?PromptLocation),TRN
                           ENTRY(@s30),AT(100,164,124,10),USE(MXD:Location),UPR
                           BUTTON('...'),AT(228,164,10,10),USE(?LookupLocation),LEFT,ICON('list3.ico')
                           PROMPT('Env Count Status'),AT(12,176),USE(?PromptEnvCountStatus),TRN
                           ENTRY(@s30),AT(100,180,124,10),USE(MXD:EnvCountStatus),UPR
                           BUTTON('...'),AT(228,180,10,10),USE(?LookupEnvCountStatus),LEFT,ICON('list3.ico')
                         END
                       END
                       PANEL,AT(4,228,248,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('OK'),AT(132,232,56,16),USE(?OK),TRN,LEFT,ICON('ok.gif'),DEFAULT,REQ
                       BUTTON('Cancel'),AT(188,232,56,16),USE(?Cancel),TRN,LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB9                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
look:MXD:AccountNo                Like(MXD:AccountNo)
look:MXD:ChargeType                Like(MXD:ChargeType)
look:MXD:WarrantyType                Like(MXD:WarrantyType)
look:MXD:Status                Like(MXD:Status)
look:MXD:Location                Like(MXD:Location)
look:MXD:EnvCountStatus                Like(MXD:EnvCountStatus)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('MPXUpdateTradeAccount')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PromptTradeAccount
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(MXD:Record,History::MXD:Record)
  SELF.AddHistoryField(?MXD:AccountNo,1)
  SELF.AddHistoryField(?MXD:UserCode,2)
  SELF.AddHistoryField(?MXD:IsChargeable,3)
  SELF.AddHistoryField(?MXD:ChargeType,4)
  SELF.AddHistoryField(?MXD:IsWarranty,5)
  SELF.AddHistoryField(?MXD:WarrantyType,6)
  SELF.AddHistoryField(?MXD:TransitType,7)
  SELF.AddHistoryField(?MXD:UnitType,8)
  SELF.AddHistoryField(?MXD:DeliveryText,9)
  SELF.AddHistoryField(?MXD:Status,10)
  SELF.AddHistoryField(?MXD:Location,11)
  SELF.AddHistoryField(?MXD:EnvCountStatus,12)
  SELF.AddUpdateFile(Access:MPXDEFS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CHARTYPE.Open
  Relate:MPXDEFS.Open
  Relate:MPXDEFS_ALIAS.Open
  Relate:STATUS.Open
  Relate:TRANTYPE.Open
  Access:LOCINTER.UseFile
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MPXDEFS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SaveAccountNumber = mxd:AccountNo
  OPEN(FormWindow)
  SELF.Opened=True
  IF ?MXD:AccountNo{Prop:Tip} AND ~?LookupTracdeAccount{Prop:Tip}
     ?LookupTracdeAccount{Prop:Tip} = 'Select ' & ?MXD:AccountNo{Prop:Tip}
  END
  IF ?MXD:AccountNo{Prop:Msg} AND ~?LookupTracdeAccount{Prop:Msg}
     ?LookupTracdeAccount{Prop:Msg} = 'Select ' & ?MXD:AccountNo{Prop:Msg}
  END
  IF ?MXD:ChargeType{Prop:Tip} AND ~?LookupChargeType{Prop:Tip}
     ?LookupChargeType{Prop:Tip} = 'Select ' & ?MXD:ChargeType{Prop:Tip}
  END
  IF ?MXD:ChargeType{Prop:Msg} AND ~?LookupChargeType{Prop:Msg}
     ?LookupChargeType{Prop:Msg} = 'Select ' & ?MXD:ChargeType{Prop:Msg}
  END
  IF ?MXD:WarrantyType{Prop:Tip} AND ~?LookupWarantyType{Prop:Tip}
     ?LookupWarantyType{Prop:Tip} = 'Select ' & ?MXD:WarrantyType{Prop:Tip}
  END
  IF ?MXD:WarrantyType{Prop:Msg} AND ~?LookupWarantyType{Prop:Msg}
     ?LookupWarantyType{Prop:Msg} = 'Select ' & ?MXD:WarrantyType{Prop:Msg}
  END
  IF ?MXD:Status{Prop:Tip} AND ~?LookupStatus{Prop:Tip}
     ?LookupStatus{Prop:Tip} = 'Select ' & ?MXD:Status{Prop:Tip}
  END
  IF ?MXD:Status{Prop:Msg} AND ~?LookupStatus{Prop:Msg}
     ?LookupStatus{Prop:Msg} = 'Select ' & ?MXD:Status{Prop:Msg}
  END
  IF ?MXD:Location{Prop:Tip} AND ~?LookupLocation{Prop:Tip}
     ?LookupLocation{Prop:Tip} = 'Select ' & ?MXD:Location{Prop:Tip}
  END
  IF ?MXD:Location{Prop:Msg} AND ~?LookupLocation{Prop:Msg}
     ?LookupLocation{Prop:Msg} = 'Select ' & ?MXD:Location{Prop:Msg}
  END
  IF ?MXD:EnvCountStatus{Prop:Tip} AND ~?LookupEnvCountStatus{Prop:Tip}
     ?LookupEnvCountStatus{Prop:Tip} = 'Select ' & ?MXD:EnvCountStatus{Prop:Tip}
  END
  IF ?MXD:EnvCountStatus{Prop:Msg} AND ~?LookupEnvCountStatus{Prop:Msg}
     ?LookupEnvCountStatus{Prop:Msg} = 'Select ' & ?MXD:EnvCountStatus{Prop:Msg}
  END
  ThisMakeover.SetWindow(Win:Form)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,?Sheet1)
  FormWindow{prop:buffer} = 1
  SELF.AddItem(ToolbarForm)
  FDCB7.Init(MXD:UserCode,?MXD:UserCode,Queue:FileDropCombo.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo,Relate:USERS,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo
  FDCB7.AddSortOrder(use:User_Code_Key)
  FDCB7.AddField(use:User_Code,FDCB7.Q.use:User_Code)
  FDCB7.AddUpdateField(use:User_Code,MXD:UserCode)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB8.Init(MXD:TransitType,?MXD:TransitType,Queue:FileDropCombo:1.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo:1,Relate:TRANTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo:1
  FDCB8.AddSortOrder(trt:Transit_Type_Key)
  FDCB8.AddField(trt:Transit_Type,FDCB8.Q.trt:Transit_Type)
  FDCB8.AddUpdateField(trt:Transit_Type,MXD:TransitType)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB9.Init(MXD:UnitType,?MXD:UnitType,Queue:FileDropCombo:2.ViewPosition,FDCB9::View:FileDropCombo,Queue:FileDropCombo:2,Relate:UNITTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB9.Q &= Queue:FileDropCombo:2
  FDCB9.AddSortOrder(uni:Unit_Type_Key)
  FDCB9.AddField(uni:Unit_Type,FDCB9.Q.uni:Unit_Type)
  FDCB9.AddUpdateField(uni:Unit_Type,MXD:UnitType)
  ThisWindow.AddItem(FDCB9.WindowComponent)
  SELF.SetAlerts()
    ThisMakeover.Refresh()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:MPXDEFS.Close
    Relate:MPXDEFS_ALIAS.Close
    Relate:STATUS.Close
    Relate:TRANTYPE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickSubAccounts
      PickChargeableChargeTypes
      PickWarrantyChargeTypes
      Browse_Status
      Browse_Locations
      Browse_Status
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?MXD:AccountNo
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MXD:AccountNo, Accepted)
      IF MXD:AccountNo OR ?MXD:AccountNo{Prop:Req}
        sub:Account_Number = MXD:AccountNo
        !Save Lookup Field Incase Of error
        look:MXD:AccountNo        = MXD:AccountNo
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            MXD:AccountNo = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            MXD:AccountNo = look:MXD:AccountNo
            SELECT(?MXD:AccountNo)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      IF (mxd:AccountNo <> SaveAccountNumber) THEN
          ! check if key already exists
          access:mpxdefs_alias.clearkey(mxd_ali:AccountNoKey)
          mxd_ali:AccountNo = mxd:AccountNo
          IF (access:mpxdefs_alias.fetch(mxd_ali:AccountNoKey) = Level:Benign) THEN
              MESSAGE('Account Number ' & CLIP(mxd:AccountNo) & ' already exists')
              mxd:AccountNo = SaveAccountNumber
              DISPLAY(?mxd:AccountNo)
              SELECT(?mxd:AccountNo)
              CYCLE
          END
          SaveAccountNumber = mxd:AccountNo
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MXD:AccountNo, Accepted)
    OF ?LookupTracdeAccount
      ThisWindow.Update
      sub:Account_Number = MXD:AccountNo
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          MXD:AccountNo = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?MXD:AccountNo)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?MXD:AccountNo)
    OF ?MXD:ChargeType
      IF MXD:ChargeType OR ?MXD:ChargeType{Prop:Req}
        cha:Charge_Type = MXD:ChargeType
        cha:Warranty = 'NO'
        !Save Lookup Field Incase Of error
        look:MXD:ChargeType        = MXD:ChargeType
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            MXD:ChargeType = cha:Charge_Type
          ELSE
            CLEAR(cha:Warranty)
            !Restore Lookup On Error
            MXD:ChargeType = look:MXD:ChargeType
            SELECT(?MXD:ChargeType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupChargeType
      ThisWindow.Update
      cha:Charge_Type = MXD:ChargeType
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          MXD:ChargeType = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?MXD:ChargeType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?MXD:ChargeType)
    OF ?MXD:WarrantyType
      IF MXD:WarrantyType OR ?MXD:WarrantyType{Prop:Req}
        cha:Charge_Type = MXD:WarrantyType
        cha:Warranty = 'YES'
        !Save Lookup Field Incase Of error
        look:MXD:WarrantyType        = MXD:WarrantyType
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            MXD:WarrantyType = cha:Charge_Type
          ELSE
            CLEAR(cha:Warranty)
            !Restore Lookup On Error
            MXD:WarrantyType = look:MXD:WarrantyType
            SELECT(?MXD:WarrantyType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupWarantyType
      ThisWindow.Update
      cha:Charge_Type = MXD:WarrantyType
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          MXD:WarrantyType = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?MXD:WarrantyType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?MXD:WarrantyType)
    OF ?MXD:Status
      IF MXD:Status OR ?MXD:Status{Prop:Req}
        sts:Status = MXD:Status
        !Save Lookup Field Incase Of error
        look:MXD:Status        = MXD:Status
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            MXD:Status = sts:Status
          ELSE
            !Restore Lookup On Error
            MXD:Status = look:MXD:Status
            SELECT(?MXD:Status)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupStatus
      ThisWindow.Update
      sts:Status = MXD:Status
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          MXD:Status = sts:Status
          Select(?+1)
      ELSE
          Select(?MXD:Status)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?MXD:Status)
    OF ?MXD:Location
      IF MXD:Location OR ?MXD:Location{Prop:Req}
        loi:Location = MXD:Location
        loi:Location_Available = 'YES'
        !Save Lookup Field Incase Of error
        look:MXD:Location        = MXD:Location
        IF Access:LOCINTER.TryFetch(loi:Location_Available_Key)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            MXD:Location = loi:Location
          ELSE
            CLEAR(loi:Location_Available)
            !Restore Lookup On Error
            MXD:Location = look:MXD:Location
            SELECT(?MXD:Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupLocation
      ThisWindow.Update
      loi:Location = MXD:Location
      
      IF SELF.RUN(5,Selectrecord)  = RequestCompleted
          MXD:Location = loi:Location
          Select(?+1)
      ELSE
          Select(?MXD:Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?MXD:Location)
    OF ?MXD:EnvCountStatus
      IF MXD:EnvCountStatus OR ?MXD:EnvCountStatus{Prop:Req}
        sts:Status = MXD:EnvCountStatus
        !Save Lookup Field Incase Of error
        look:MXD:EnvCountStatus        = MXD:EnvCountStatus
        IF Access:STATUS.TryFetch(sts:Status_Key)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            MXD:EnvCountStatus = sts:Status
          ELSE
            !Restore Lookup On Error
            MXD:EnvCountStatus = look:MXD:EnvCountStatus
            SELECT(?MXD:EnvCountStatus)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupEnvCountStatus
      ThisWindow.Update
      sts:Status = MXD:EnvCountStatus
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          MXD:EnvCountStatus = sts:Status
          Select(?+1)
      ELSE
          Select(?MXD:EnvCountStatus)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?MXD:EnvCountStatus)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisMakeover.TakeEvent(Win:Form,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()

