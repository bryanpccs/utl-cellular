

   MEMBER('samxml.clw')                               ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SAMXM005.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SAMXM006.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseSamsungStatus PROCEDURE                         !Generated from procedure template - Window

ThisThreadActive BYTE
BRW1::View:Browse    VIEW(XMLSAM)
                       PROJECT(XSA:StatusCode)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
XSA:StatusCode         LIKE(XSA:StatusCode)           !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BrowseWindow         WINDOW('Browse Samsung Status'),AT(,,259,229),FONT('Arial',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,172,220),USE(?Sheet1),SPREAD
                         TAB('Browse Status'),USE(?Tab1)
                           ENTRY(@s4),AT(8,16,124,10),USE(XSA:StatusCode),UPR
                           LIST,AT(8,32,164,188),USE(?List),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('23L(2)|M~Samsung Status~@s4@'),FROM(Queue:Browse)
                         END
                       END
                       BUTTON('&Insert'),AT(180,36,76,20),USE(?Insert),LEFT,KEY(InsertKey),ICON('insert.ico')
                       BUTTON('&Edit'),AT(180,60,76,20),USE(?Change),LEFT,KEY(CtrlEnter),ICON('edit.ico'),DEFAULT
                       BUTTON('&Delete'),AT(180,84,76,20),USE(?Delete),LEFT,KEY(DeleteKey),ICON('delete.ico')
                       BUTTON('&Select'),AT(180,4,76,20),USE(?Select),LEFT,KEY(EnterKey),ICON('select.ico')
                       BUTTON('&Close'),AT(180,204,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    If ?XSA:StatusCode{prop:ReadOnly} = True
        ?XSA:StatusCode{prop:FontColor} = 65793
        ?XSA:StatusCode{prop:Color} = 15066597
    Elsif ?XSA:StatusCode{prop:Req} = True
        ?XSA:StatusCode{prop:FontColor} = 65793
        ?XSA:StatusCode{prop:Color} = 8454143
    Else ! If ?XSA:StatusCode{prop:Req} = True
        ?XSA:StatusCode{prop:FontColor} = 65793
        ?XSA:StatusCode{prop:Color} = 16777215
    End ! If ?XSA:StatusCode{prop:Req} = True
    ?XSA:StatusCode{prop:Trn} = 0
    ?XSA:StatusCode{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseSamsungStatus')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?XSA:StatusCode
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='BrowseSamsungStatus'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:XMLSAM.Open
  SELF.FilesOpened = True
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW1::View:Browse,Queue:Browse,Relate:XMLSAM,SELF)
  OPEN(BrowseWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,XSA:StatusCodeKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?XSA:StatusCode,XSA:StatusCode,1,BRW1)
  BRW1.AddField(XSA:StatusCode,BRW1.Q.XSA:StatusCode)
  BRW1.AskProcedure = 1
  BRW1.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:XMLSAM.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='BrowseSamsungStatus'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateSamsungStatus
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:GainFocus
      IF BrowseWindow{PROP:Iconize}=TRUE
        BrowseWindow{PROP:Iconize}=FALSE
        IF BrowseWindow{PROP:Active}<>TRUE
           BrowseWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

