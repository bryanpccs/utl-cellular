

   MEMBER('samxml.clw')                               ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SAMXM002.INC'),ONCE        !Local module procedure declarations
                     END


Insert_Password PROCEDURE                             !Generated from procedure template - Window

LocalRequest         LONG
tries_temp           BYTE
FilesOpened          BYTE
Version_Temp         STRING(20)
tmp:OSVersion        STRING(25)
tmp:makedate         DATE
tmp:maketime         TIME
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW,AT(,,320,212),FONT('Arial',8,,),COLOR(COLOR:White),CENTER,ICON('Pc.ico'),TILED,GRAY,DOUBLE,IMM
                       IMAGE('monhand.gif'),AT(4,80,120,116),USE(?Image1)
                       IMAGE('celsam2.gif'),AT(8,4,302,91),USE(?Image3)
                       BOX,AT(0,0,320,212),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(2)
                       STRING(@s20),AT(132,100),USE(Version_Temp),TRN,CENTER,FONT('Comic Sans MS',14,COLOR:Maroon,),COLOR(COLOR:Silver)
                       STRING('Insert Password And Click ''OK'''),AT(144,124),USE(?String4),TRN,FONT(,12,COLOR:Navy,FONT:bold),COLOR(COLOR:Silver)
                       STRING('Password'),AT(148,156),USE(?String1),TRN
                       ENTRY(@s20),AT(192,156,108,10),USE(glo:Password),FONT(,,,FONT:bold),COLOR(COLOR:White),REQ,UPR,PASSWORD
                       BUTTON('&OK'),AT(148,180,56,16),USE(?OkButton),FLAT,LEFT,ICON('OK.gif'),DEFAULT
                       BUTTON('Cancel'),AT(244,180,56,16),USE(?CancelButton),FLAT,LEFT,ICON('Cancel.gif')
                       STRING(@s25),AT(20,200,92,8),USE(tmp:OSVersion),FONT(,7,,FONT:bold)
                       STRING(@d6),AT(176,200),USE(tmp:makedate),FONT(,7,,FONT:bold,CHARSET:ANSI)
                       STRING(@t1),AT(292,200),USE(tmp:maketime),FONT(,7,,FONT:bold,CHARSET:ANSI)
                       STRING('OS:'),AT(4,200),USE(?String7),FONT(,7,,,CHARSET:ANSI)
                       STRING('Date Created:'),AT(132,200),USE(?String7:2),FONT(,7,,,CHARSET:ANSI)
                       STRING('Time Created:'),AT(248,200),USE(?String7:3),FONT(,7,,,CHARSET:ANSI)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Version_Temp{prop:FontColor} = -1
    ?Version_Temp{prop:Color} = 15066597
    ?String4{prop:FontColor} = -1
    ?String4{prop:Color} = 15066597
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    If ?glo:Password{prop:ReadOnly} = True
        ?glo:Password{prop:FontColor} = 65793
        ?glo:Password{prop:Color} = 15066597
    Elsif ?glo:Password{prop:Req} = True
        ?glo:Password{prop:FontColor} = 65793
        ?glo:Password{prop:Color} = 8454143
    Else ! If ?glo:Password{prop:Req} = True
        ?glo:Password{prop:FontColor} = 65793
        ?glo:Password{prop:Color} = 16777215
    End ! If ?glo:Password{prop:Req} = True
    ?glo:Password{prop:Trn} = 0
    ?glo:Password{prop:FontStyle} = font:Bold
    ?tmp:OSVersion{prop:FontColor} = -1
    ?tmp:OSVersion{prop:Color} = 15066597
    ?tmp:makedate{prop:FontColor} = -1
    ?tmp:makedate{prop:Color} = 15066597
    ?tmp:maketime{prop:FontColor} = -1
    ?tmp:maketime{prop:Color} = 15066597
    ?String7{prop:FontColor} = -1
    ?String7{prop:Color} = 15066597
    ?String7:2{prop:FontColor} = -1
    ?String7:2{prop:Color} = 15066597
    ?String7:3{prop:FontColor} = -1
    ?String7:3{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Add_Login       Routine
    Clear(log:record)
    log:user_code   = use:user_code
    log:surname     = use:surname
    log:forename    = use:forename
    log:date        = Today()
    log:time        = Clock()
    access:logged.Insert()
    glo:timelogged = log:time
    glo:select1 = 'N'


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Insert_Password')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:LOGGED.Open
  Relate:USERS.Open
  SELF.FilesOpened = True
  ! Before Embed Point: %BeforeWindowOpening) DESC(Legacy: Before Opening the Window) ARG()
  glo:select1 = 'Y'
  glo:password = ''
  ! After Embed Point: %BeforeWindowOpening) DESC(Legacy: Before Opening the Window) ARG()
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! Before Embed Point: %AfterWindowOpening) DESC(Legacy: After Opening the Window) ARG()
  Set(defaults)
  access:defaults.next()
  version_temp    = 'Version: ' & Clip(def:version_number)
  tries_temp = 0
  
  
  
  
  ! After Embed Point: %AfterWindowOpening) DESC(Legacy: After Opening the Window) ARG()
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:LOGGED.Close
    Relate:USERS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      close_window# = 1
      glo:select1 = 'Y'
      If glo:password = 'JOB:ENTER' Or glo:password = INT((Today() / 2.5) * 9.7)
          glo:select1 = 'N'
      Else!If password = 'JOB:ENTER'
          access:users.clearkey(use:password_key)
          use:password =glo:password
          if access:users.fetch(use:password_key)
             glo:password = ''
              Display()
              Case MessageEx('Invalid Password!','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              Select(?glo:password)
              tries_temp += 1
              If tries_temp = 3
                  close_window# = 2
              Else!If tries# = 3
                  close_window# = 0
              End!If tries# = 3
          Else !if access:users.fetch(use:password_key)
              If use:restrict_logins = 'YES'
                  count_logins# = 1
                  setcursor(cursor:wait)
                  access:logged.clearkey(log:user_code_key)
                  log:user_code = use:user_code
                  set(log:user_code_key,log:user_code_key)
                  loop
                      if access:logged.next()
                         break
                      end !if
                      if log:user_code <> use:user_code      |
                          then break.  ! end if
                      count_logins# += 1
                  end !loop
                  setcursor()
                  If count_logins# > use:concurrent_logins
                      Case MessageEx('You have exceeded your maximum number of logins. <13,10><13,10>Please contact your Systems Administrator.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      Select(?glo:password)
                      tries_temp += 1
                      If tries_temp = 3
                          close_window# = 2
                      Else!If tries# = 3
                          close_window# = 0
                      End!If tries# = 3
                  Else !If count_logins# > use:concurrent_logins
                      Do Add_Login
                  End !If count_logins# > use:concurrent_logins
              Else !If use:restrict_logins = 'YES'
                  Do Add_Login
              End !If use:restrict_logins = 'YES'
          End !if access:users.fetch(use:password_key)
      End !If password <> 'JOB:ENTER'
      If close_window# <> 0
          If close_window# = 2
              Case MessageEx('You have unsuccessfully logged in 3 times.<13,10><13,10>The program will now quit.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              glo:select1 = 'Y'
          End
          Post(event:closewindow)
      End!If close_window# = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      glo:select1 = 'Y'
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
       tmp:makedate  = 75514
       tmp:maketime  = 4984577
      tmp:OSVersion = OSVersion()
         window{Prop:Active}=1
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

