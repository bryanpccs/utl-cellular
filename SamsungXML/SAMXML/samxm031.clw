

   MEMBER('samxml.clw')                               ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SAMXM031.INC'),ONCE        !Local module procedure declarations
                     END


MPXUpdateStatus PROCEDURE (arg:MpxReportCode, arg:MPXReportName) !Generated from procedure template - Window

ActionMessage        CSTRING(40)
MPXReportCode        LONG
MPXReportName        STRING(30)
SaveSBStatus         STRING(30)
History::MXS:Record  LIKE(MXS:RECORD),STATIC
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Form
mo:SelectedField  Long
FormWindow           WINDOW('MPX Report Status'),AT(,,232,88),FONT('Arial',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,224,52),USE(?Sheet1),SPREAD
                         TAB('Update Status'),USE(?Tab1)
                           PROMPT('MPX Report Code'),AT(8,20),USE(?Prompt1)
                           ENTRY(@s30),AT(84,20,124,10),USE(MPXReportName),COLOR(COLOR:GRAYTEXT),UPR,READONLY
                           PROMPT('ServiceBase Status'),AT(8,36),USE(?Prompt2)
                           ENTRY(@s30),AT(84,36,124,10),USE(MXS:SB_STATUS),UPR
                           BUTTON('...'),AT(212,36,10,10),USE(?SBStatusLookup),LEFT,ICON('list3.ico')
                         END
                       END
                       PANEL,AT(4,60,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('OK'),AT(108,64,56,16),USE(?OK),TRN,LEFT,ICON('ok.gif'),DEFAULT,REQ
                       BUTTON('Cancel'),AT(164,64,56,16),USE(?Cancel),TRN,LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
!Save Entry Fields Incase Of Lookup
look:MXS:SB_STATUS                Like(MXS:SB_STATUS)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('MPXUpdateStatus')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(MXS:Record,History::MXS:Record)
  SELF.AddHistoryField(?MXS:SB_STATUS,3)
  SELF.AddUpdateFile(Access:MPXSTAT)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MPXSTAT.Open
  Relate:MPXSTAT_ALIAS.Open
  Relate:STATUS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MPXSTAT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  MPXReportCode = arg:MpxReportCode
  MPXReportName = arg:MpxReportName
  SaveSBStatus = mxs:SB_Status
  
  OPEN(FormWindow)
  SELF.Opened=True
  IF ?MXS:SB_STATUS{Prop:Tip} AND ~?SBStatusLookup{Prop:Tip}
     ?SBStatusLookup{Prop:Tip} = 'Select ' & ?MXS:SB_STATUS{Prop:Tip}
  END
  IF ?MXS:SB_STATUS{Prop:Msg} AND ~?SBStatusLookup{Prop:Msg}
     ?SBStatusLookup{Prop:Msg} = 'Select ' & ?MXS:SB_STATUS{Prop:Msg}
  END
  ThisMakeover.SetWindow(Win:Form)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,?Sheet1)
  FormWindow{prop:buffer} = 1
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
    ThisMakeover.Refresh()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MPXSTAT.Close
    Relate:MPXSTAT_ALIAS.Close
    Relate:STATUS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Browse_Status
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?MXS:SB_STATUS
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MXS:SB_STATUS, Accepted)
      IF MXS:SB_STATUS OR ?MXS:SB_STATUS{Prop:Req}
        sts:Status = MXS:SB_STATUS
        !Save Lookup Field Incase Of error
        look:MXS:SB_STATUS        = MXS:SB_STATUS
        IF Access:STATUS.TryFetch(sts:Status_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            MXS:SB_STATUS = sts:Status
          ELSE
            !Restore Lookup On Error
            MXS:SB_STATUS = look:MXS:SB_STATUS
            SELECT(?MXS:SB_STATUS)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      IF (mxs:SB_Status <> SaveSBStatus) THEN
          ! check if key already exists
          access:mpxstat_alias.clearkey(mxs_ali:SBStatusKey)
          mxs_ali:SB_Status = mxs:SB_Status
          IF (access:mpxstat_alias.fetch(mxs_ali:SBStatusKey) = Level:Benign) THEN
              MESSAGE('Status Code ' & CLIP(mxs:SB_Status) & ' already exists')
              mxs:SB_Status = SaveSBStatus
              DISPLAY(?mxs:SB_Status)
              SELECT(?mxs:SB_Status)
              CYCLE
          END
          SaveSBStatus = mxs:SB_Status
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MXS:SB_STATUS, Accepted)
    OF ?SBStatusLookup
      ThisWindow.Update
      sts:Status = MXS:SB_STATUS
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          MXS:SB_STATUS = sts:Status
          Select(?+1)
      ELSE
          Select(?MXS:SB_STATUS)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?MXS:SB_STATUS)
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      mxs:MPX_Status = MPXReportCode
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisMakeover.TakeEvent(Win:Form,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()

