

   MEMBER('samxml.clw')                               ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SAMXM021.INC'),ONCE        !Local module procedure declarations
                     END


UpdateSBStatus PROCEDURE (arg:SamsungStatus)          !Generated from procedure template - Window

ActionMessage        CSTRING(40)
SamsungStatus        STRING(4)
SaveSBStatus         STRING(30)
History::XSB:Record  LIKE(XSB:RECORD),STATIC
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Form
mo:SelectedField  Long
FormWindow           WINDOW('Samsung ServiceBase Status'),AT(,,232,88),FONT('Arial',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,224,52),USE(?Sheet1),SPREAD
                         TAB('Update Status'),USE(?Tab1)
                           PROMPT('Samsung Status'),AT(8,20),USE(?Prompt1)
                           ENTRY(@s2),AT(84,20,124,10),USE(SamsungStatus),COLOR(COLOR:GRAYTEXT),UPR,READONLY
                           PROMPT('ServiceBase Status'),AT(8,36),USE(?Prompt2)
                           ENTRY(@s30),AT(84,36,124,10),USE(XSB:SBStatus),REQ,UPR
                           BUTTON('...'),AT(212,36,10,10),USE(?SBStatusLookup),LEFT,ICON('list3.ico')
                         END
                       END
                       PANEL,AT(4,60,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('OK'),AT(108,64,56,16),USE(?OK),TRN,LEFT,ICON('ok.gif'),DEFAULT,REQ
                       BUTTON('Cancel'),AT(164,64,56,16),USE(?Cancel),TRN,LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
!Save Entry Fields Incase Of Lookup
look:XSB:SBStatus                Like(XSB:SBStatus)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateSBStatus')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(XSB:Record,History::XSB:Record)
  SELF.AddHistoryField(?XSB:SBStatus,1)
  SELF.AddUpdateFile(Access:XMLSB)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:STATUS.Open
  Relate:XMLSB.Open
  Relate:XMLSB_ALIAS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:XMLSB
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SamsungStatus = arg:SamsungStatus
  SaveSBStatus = xsb:SBStatus
  OPEN(FormWindow)
  SELF.Opened=True
  IF ?XSB:SBStatus{Prop:Tip} AND ~?SBStatusLookup{Prop:Tip}
     ?SBStatusLookup{Prop:Tip} = 'Select ' & ?XSB:SBStatus{Prop:Tip}
  END
  IF ?XSB:SBStatus{Prop:Msg} AND ~?SBStatusLookup{Prop:Msg}
     ?SBStatusLookup{Prop:Msg} = 'Select ' & ?XSB:SBStatus{Prop:Msg}
  END
  ThisMakeover.SetWindow(Win:Form)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,?Sheet1)
  FormWindow{prop:buffer} = 1
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
    ThisMakeover.Refresh()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STATUS.Close
    Relate:XMLSB.Close
    Relate:XMLSB_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Browse_Status
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?XSB:SBStatus
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?XSB:SBStatus, Accepted)
      IF (xsb:SBStatus <> SaveSBStatus) THEN
          ! check if key already exists
          access:xmlsb_alias.clearkey(xsb_ali:SBStatusKey)
          xsb_ali:SBStatus = xsb:SBStatus
          IF (access:xmlsb_alias.fetch(xsb_ali:SBStatusKey) = Level:Benign) THEN
              MESSAGE('Status Code ' & CLIP(xsb:SBStatus) & ' already exists')
              xsb:SBStatus = SaveSBStatus
              DISPLAY(?xsb:SBStatus)
              SELECT(?xsb:SBStatus)
              CYCLE
          END
      END
      IF XSB:SBStatus OR ?XSB:SBStatus{Prop:Req}
        sts:Status = XSB:SBStatus
        sts:Job = 'YES'
        GLO:Select1 = 'JOB'
        !Save Lookup Field Incase Of error
        look:XSB:SBStatus        = XSB:SBStatus
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            XSB:SBStatus = sts:Status
          ELSE
            CLEAR(sts:Job)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            XSB:SBStatus = look:XSB:SBStatus
            SELECT(?XSB:SBStatus)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?XSB:SBStatus, Accepted)
    OF ?SBStatusLookup
      ThisWindow.Update
      sts:Status = XSB:SBStatus
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          XSB:SBStatus = sts:Status
          Select(?+1)
      ELSE
          Select(?XSB:SBStatus)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?XSB:SBStatus)
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      XSB:FKStatusCode = SamsungStatus
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisMakeover.TakeEvent(Win:Form,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()

