

   MEMBER('samxml.clw')                               ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SAMXM017.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SAMXM019.INC'),ONCE        !Req'd for module callout resolution
                     END


MPXBrowseExceptions PROCEDURE                         !Generated from procedure template - Window

ThisThreadActive BYTE
LocalAddress         STRING(30)
ExceptionState       BYTE
BRW1::View:Browse    VIEW(MPXJOBS)
                       PROJECT(MXJ:CustLastName)
                       PROJECT(MXJ:CustPostCode)
                       PROJECT(MXJ:ModelID)
                       PROJECT(MXJ:ImportDate)
                       PROJECT(MXJ:ImportTime)
                       PROJECT(MXJ:RecordNo)
                       PROJECT(MXJ:Record_State)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
MXJ:CustLastName       LIKE(MXJ:CustLastName)         !List box control field - type derived from field
MXJ:CustPostCode       LIKE(MXJ:CustPostCode)         !List box control field - type derived from field
MXJ:ModelID            LIKE(MXJ:ModelID)              !List box control field - type derived from field
MXJ:ImportDate         LIKE(MXJ:ImportDate)           !List box control field - type derived from field
MXJ:ImportTime         LIKE(MXJ:ImportTime)           !List box control field - type derived from field
LocalAddress           LIKE(LocalAddress)             !List box control field - type derived from local data
MXJ:RecordNo           LIKE(MXJ:RecordNo)             !Primary key field - type derived from field
MXJ:Record_State       LIKE(MXJ:Record_State)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BrowseWindow         WINDOW('Browse MPX Exceptions'),AT(,,528,251),FONT('Arial',8,,),CENTER,GRAY,DOUBLE
                       BUTTON('&Insert'),AT(448,180,76,20),USE(?Insert),TRN,LEFT,KEY(InsertKey),ICON('insert.ico')
                       BUTTON('Amend Detail'),AT(448,40,76,20),USE(?Change),TRN,LEFT,KEY(CtrlEnter),ICON('edit.ico'),DEFAULT
                       BUTTON('Cancel Job'),AT(448,64,76,20),USE(?CancelJobButton),TRN,LEFT,ICON('Stop.ico')
                       BUTTON('Duplicated Job'),AT(448,112,76,20),USE(?DuplicateJobButton),TRN,HIDE,LEFT,ICON('FailClam.ico')
                       BUTTON('Reprocess All Jobs'),AT(448,88,76,20),USE(?ReprocessAllButton),LEFT,ICON('ok.ico')
                       BUTTON('&Delete'),AT(448,204,76,20),USE(?Delete),TRN,LEFT,KEY(DeleteKey),ICON('delete.ico')
                       LIST,AT(8,36,428,208),USE(?List),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('106L(2)|M~Surname~@s50@43L(2)|M~Postcode~@s50@94L(2)|M~Model~@s50@49L(2)|M~Date~' &|
   '@d17@27L(2)|M~Time~@t7@120L(2)|M~Address~@s30@'),FROM(Queue:Browse)
                       SHEET,AT(4,4,436,244),USE(?CurrentTab),SPREAD
                         TAB('By Surname'),USE(?Tab1)
                           ENTRY(@s50),AT(8,20,124,10),USE(MXJ:CustLastName)
                         END
                         TAB('By Postcode'),USE(?Tab2)
                           ENTRY(@s50),AT(8,20,124,10),USE(MXJ:CustPostCode)
                         END
                         TAB('By Model'),USE(?Tab3)
                           ENTRY(@s50),AT(8,20,124,10),USE(MXJ:ModelID)
                         END
                       END
                       BUTTON('&Select'),AT(448,156,76,20),USE(?Select),TRN,HIDE,LEFT,KEY(EnterKey),ICON('select.ico')
                       BUTTON('Close'),AT(448,228,76,20),USE(?Close),TRN,LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab)=1
BRW1::Sort2:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab)=2
BRW1::Sort3:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab)=3
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    If ?MXJ:CustLastName{prop:ReadOnly} = True
        ?MXJ:CustLastName{prop:FontColor} = 65793
        ?MXJ:CustLastName{prop:Color} = 15066597
    Elsif ?MXJ:CustLastName{prop:Req} = True
        ?MXJ:CustLastName{prop:FontColor} = 65793
        ?MXJ:CustLastName{prop:Color} = 8454143
    Else ! If ?MXJ:CustLastName{prop:Req} = True
        ?MXJ:CustLastName{prop:FontColor} = 65793
        ?MXJ:CustLastName{prop:Color} = 16777215
    End ! If ?MXJ:CustLastName{prop:Req} = True
    ?MXJ:CustLastName{prop:Trn} = 0
    ?MXJ:CustLastName{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    If ?MXJ:CustPostCode{prop:ReadOnly} = True
        ?MXJ:CustPostCode{prop:FontColor} = 65793
        ?MXJ:CustPostCode{prop:Color} = 15066597
    Elsif ?MXJ:CustPostCode{prop:Req} = True
        ?MXJ:CustPostCode{prop:FontColor} = 65793
        ?MXJ:CustPostCode{prop:Color} = 8454143
    Else ! If ?MXJ:CustPostCode{prop:Req} = True
        ?MXJ:CustPostCode{prop:FontColor} = 65793
        ?MXJ:CustPostCode{prop:Color} = 16777215
    End ! If ?MXJ:CustPostCode{prop:Req} = True
    ?MXJ:CustPostCode{prop:Trn} = 0
    ?MXJ:CustPostCode{prop:FontStyle} = font:Bold
    ?Tab3{prop:Color} = 15066597
    If ?MXJ:ModelID{prop:ReadOnly} = True
        ?MXJ:ModelID{prop:FontColor} = 65793
        ?MXJ:ModelID{prop:Color} = 15066597
    Elsif ?MXJ:ModelID{prop:Req} = True
        ?MXJ:ModelID{prop:FontColor} = 65793
        ?MXJ:ModelID{prop:Color} = 8454143
    Else ! If ?MXJ:ModelID{prop:Req} = True
        ?MXJ:ModelID{prop:FontColor} = 65793
        ?MXJ:ModelID{prop:Color} = 16777215
    End ! If ?MXJ:ModelID{prop:Req} = True
    ?MXJ:ModelID{prop:Trn} = 0
    ?MXJ:ModelID{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('MPXBrowseExceptions')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Insert
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='MPXBrowseExceptions'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MPXJOBS.Open
  Relate:MPXJOBS_ALIAS.Open
  SELF.FilesOpened = True
  ExceptionState = RS_EXCEPTION
  
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW1::View:Browse,Queue:Browse,Relate:MPXJOBS,SELF)
  OPEN(BrowseWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,MXJ:StateSurnameKey)
  BRW1.AddRange(MXJ:Record_State,ExceptionState)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?MXJ:CustLastName,MXJ:CustLastName,1,BRW1)
  BRW1.AddSortOrder(,MXJ:StatePostcodeKey)
  BRW1.AddRange(MXJ:Record_State,ExceptionState)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?MXJ:CustPostCode,MXJ:CustPostCode,1,BRW1)
  BRW1.AddSortOrder(,MXJ:StateModelKey)
  BRW1.AddRange(MXJ:Record_State,ExceptionState)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?MXJ:ModelID,MXJ:ModelID,1,BRW1)
  BRW1.AddSortOrder(,MXJ:RecordNoKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,MXJ:RecordNo,1,BRW1)
  BIND('LocalAddress',LocalAddress)
  BRW1.AddField(MXJ:CustLastName,BRW1.Q.MXJ:CustLastName)
  BRW1.AddField(MXJ:CustPostCode,BRW1.Q.MXJ:CustPostCode)
  BRW1.AddField(MXJ:ModelID,BRW1.Q.MXJ:ModelID)
  BRW1.AddField(MXJ:ImportDate,BRW1.Q.MXJ:ImportDate)
  BRW1.AddField(MXJ:ImportTime,BRW1.Q.MXJ:ImportTime)
  BRW1.AddField(LocalAddress,BRW1.Q.LocalAddress)
  BRW1.AddField(MXJ:RecordNo,BRW1.Q.MXJ:RecordNo)
  BRW1.AddField(MXJ:Record_State,BRW1.Q.MXJ:Record_State)
  BRW1.AskProcedure = 1
  BRW1.AddToolbarTarget(Toolbar)
  HIDE(?Insert)
  HIDE(?Delete)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MPXJOBS.Close
    Relate:MPXJOBS_ALIAS.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='MPXBrowseExceptions'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    MPXUpdateException
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CancelJobButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelJobButton, Accepted)
      if records(brw1.q) > 0 then
          mxj:RECORD_STATE = RS_CANCELLED
          mxj:EXCEPTION_CODE = XC_NONE
          mxj:EXCEPTION_DESC = ''
      
          access:mpxjobs.update()
          brw1.resetsort(1)
      end
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelJobButton, Accepted)
    OF ?ReprocessAllButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReprocessAllButton, Accepted)
      access:mpxjobs_alias.clearkey(mxj_ali:StateJobNoKey)
      mxj_ali:RECORD_STATE = RS_EXCEPTION
      SET(mxj_ali:StateJobNoKey,mxj_ali:StateJobNoKey)
      LOOP
          IF ((access:mpxjobs_alias.next() <> Level:Benign) OR |
              (mxj_ali:RECORD_STATE <> RS_EXCEPTION)) THEN
              BREAK
          END
          if mxj_ali:EXCEPTION_CODE = XC_EXPORT_FAIL
              mxj_ali:RECORD_STATE = RS_ACTIVE_JOB
          else
              mxj_ali:RECORD_STATE = RS_NEW_IMPORT
          end
          mxj_ali:EXCEPTION_CODE = XC_NONE
          mxj_ali:EXCEPTION_DESC = ''
          access:mpxjobs_alias.update()
      END
      
      brw1.resetsort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReprocessAllButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:GainFocus
      IF BrowseWindow{PROP:Iconize}=TRUE
        BrowseWindow{PROP:Iconize}=FALSE
        IF BrowseWindow{PROP:Active}<>TRUE
           BrowseWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, Init, (SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM))
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END
  SELF.InsertControl = 0
  SELF.DeleteControl = 0
  
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, Init, (SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM))


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab)=1
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab)=2
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab)=3
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())
  PARENT.SetQueueRecord
  brw1.q.LocalAddress = CLIP(mxj:CustAdd1) & ' ' & CLIP(mxj:CustAdd2) & ' ' & CLIP(mxj:CustAdd3) & ' ' & CLIP(mxj:CustAdd4)
  
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

