

   MEMBER('samxml.clw')                               ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SAMXM023.INC'),ONCE        !Local module procedure declarations
                     END


UpdateTradeAccount PROCEDURE                          !Generated from procedure template - Window

ActionMessage        CSTRING(40)
SaveAccountNumber    STRING(15)
History::XTR:Record  LIKE(XTR:RECORD),STATIC
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Form
mo:SelectedField  Long
FormWindow           WINDOW('Samsung Trade Account Update'),AT(,,216,76),FONT('Arial',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,208,40),USE(?Sheet1),SPREAD
                         TAB('Update Allowable Trade Accounts'),USE(?Tab1)
                           PROMPT('Trade Account'),AT(8,24),USE(?Prompt1)
                           ENTRY(@s15),AT(68,24,124,10),USE(XTR:AccountNumber)
                           BUTTON('...'),AT(196,24,10,10),USE(?CallLookup),TRN,LEFT,ICON('list3.ico')
                         END
                       END
                       PANEL,AT(4,48,208,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('OK'),AT(96,52,56,16),USE(?OK),TRN,LEFT,ICON('ok.gif'),DEFAULT,REQ
                       BUTTON('Cancel'),AT(152,52,56,16),USE(?Cancel),TRN,LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
!Save Entry Fields Incase Of Lookup
look:XTR:AccountNumber                Like(XTR:AccountNumber)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateTradeAccount')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(XTR:Record,History::XTR:Record)
  SELF.AddHistoryField(?XTR:AccountNumber,1)
  SELF.AddUpdateFile(Access:XMLTRADE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:TRADEACC.Open
  Relate:XMLTRADE.Open
  Relate:XMLTRADE_ALIAS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:XMLTRADE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SaveAccountNumber = xtr:AccountNumber
  OPEN(FormWindow)
  SELF.Opened=True
  IF ?XTR:AccountNumber{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?XTR:AccountNumber{Prop:Tip}
  END
  IF ?XTR:AccountNumber{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?XTR:AccountNumber{Prop:Msg}
  END
  ThisMakeover.SetWindow(Win:Form)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,?Sheet1)
  FormWindow{prop:buffer} = 1
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
    ThisMakeover.Refresh()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TRADEACC.Close
    Relate:XMLTRADE.Close
    Relate:XMLTRADE_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    BrowseMainAccount
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?XTR:AccountNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?XTR:AccountNumber, Accepted)
      IF (xtr:AccountNumber <> SaveAccountNumber) THEN
          ! check if key already exists
          access:xmltrade_alias.clearkey(xtr_ali:AccountNumberKey)
          xtr_ali:AccountNumber = xtr:AccountNumber
          IF (access:xmltrade_alias.fetch(xtr_ali:AccountNumberKey) = Level:Benign) THEN
              MESSAGE('Account Number ' & CLIP(xtr:AccountNumber) & ' already exists')
              xtr:AccountNumber = SaveAccountNumber
              DISPLAY(?xtr:AccountNumber)
              SELECT(?xtr:AccountNumber)
              CYCLE
          END
      END
      IF XTR:AccountNumber OR ?XTR:AccountNumber{Prop:Req}
        tra:Account_Number = XTR:AccountNumber
        !Save Lookup Field Incase Of error
        look:XTR:AccountNumber        = XTR:AccountNumber
        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            XTR:AccountNumber = tra:Account_Number
          ELSE
            !Restore Lookup On Error
            XTR:AccountNumber = look:XTR:AccountNumber
            SELECT(?XTR:AccountNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?XTR:AccountNumber, Accepted)
    OF ?CallLookup
      ThisWindow.Update
      tra:Account_Number = XTR:AccountNumber
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          XTR:AccountNumber = tra:Account_Number
          Select(?+1)
      ELSE
          Select(?XTR:AccountNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?XTR:AccountNumber)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisMakeover.TakeEvent(Win:Form,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()

