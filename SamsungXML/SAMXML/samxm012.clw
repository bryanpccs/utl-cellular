

   MEMBER('samxml.clw')                               ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SAMXM012.INC'),ONCE        !Local module procedure declarations
                     END


BrowseAllRecords PROCEDURE                            !Generated from procedure template - Window

ThisThreadActive BYTE
BRW3::View:Browse    VIEW(XMLJOBS)
                       PROJECT(XJB:RECORD_NUMBER)
                       PROJECT(XJB:RECORD_STATE)
                       PROJECT(XJB:EXCEPTION_CODE)
                       PROJECT(XJB:EXCEPTION_DESC)
                       PROJECT(XJB:JOB_NO)
                       PROJECT(XJB:COMPANY)
                       PROJECT(XJB:TR_NO)
                       PROJECT(XJB:ASC_CODE)
                       PROJECT(XJB:MODEL_CODE)
                       PROJECT(XJB:SERIAL_NO)
                       PROJECT(XJB:PURCHASE_DATE)
                       PROJECT(XJB:SERVICE_TYPE)
                       PROJECT(XJB:IN_OUT_WARRANTY)
                       PROJECT(XJB:REQUEST_DATE)
                       PROJECT(XJB:REQUEST_TIME)
                       PROJECT(XJB:REPAIR_ETD_DATE)
                       PROJECT(XJB:REPAIR_ETD_TIME)
                       PROJECT(XJB:TR_STATUS)
                       PROJECT(XJB:TR_REASON)
                       PROJECT(XJB:CONSUMER_TITLE)
                       PROJECT(XJB:CONSUMER_FIRST_NAME)
                       PROJECT(XJB:CONSUMER_LAST_NAME)
                       PROJECT(XJB:CONSUMER_TEL_NUMBER1)
                       PROJECT(XJB:CONSUMER_FAX_NUMBER)
                       PROJECT(XJB:CONSUMER_EMAIL)
                       PROJECT(XJB:CONSUMER_COUNTRY)
                       PROJECT(XJB:CONSUMER_REGION)
                       PROJECT(XJB:CONSUMER_POSTCODE)
                       PROJECT(XJB:CONSUMER_CITY)
                       PROJECT(XJB:CONSUMER_STREET)
                       PROJECT(XJB:CONSUMER_HOUSE_NUMBER)
                       PROJECT(XJB:REPAIR_COMPLETE_DATE)
                       PROJECT(XJB:REPAIR_COMPLETE_TIME)
                       PROJECT(XJB:GOODS_DELIVERY_DATE)
                       PROJECT(XJB:GOODS_DELIVERY_TIME)
                       PROJECT(XJB:SYMPTOM1_DESC)
                       PROJECT(XJB:SYMPTOM2_DESC)
                       PROJECT(XJB:SYMPTOM3_DESC)
                       PROJECT(XJB:BP_NO)
                       PROJECT(XJB:INQUIRY_TEXT)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
XJB:RECORD_NUMBER      LIKE(XJB:RECORD_NUMBER)        !List box control field - type derived from field
XJB:RECORD_STATE       LIKE(XJB:RECORD_STATE)         !List box control field - type derived from field
XJB:EXCEPTION_CODE     LIKE(XJB:EXCEPTION_CODE)       !List box control field - type derived from field
XJB:EXCEPTION_DESC     LIKE(XJB:EXCEPTION_DESC)       !List box control field - type derived from field
XJB:JOB_NO             LIKE(XJB:JOB_NO)               !List box control field - type derived from field
XJB:COMPANY            LIKE(XJB:COMPANY)              !List box control field - type derived from field
XJB:TR_NO              LIKE(XJB:TR_NO)                !List box control field - type derived from field
XJB:ASC_CODE           LIKE(XJB:ASC_CODE)             !List box control field - type derived from field
XJB:MODEL_CODE         LIKE(XJB:MODEL_CODE)           !List box control field - type derived from field
XJB:SERIAL_NO          LIKE(XJB:SERIAL_NO)            !List box control field - type derived from field
XJB:PURCHASE_DATE      LIKE(XJB:PURCHASE_DATE)        !List box control field - type derived from field
XJB:SERVICE_TYPE       LIKE(XJB:SERVICE_TYPE)         !List box control field - type derived from field
XJB:IN_OUT_WARRANTY    LIKE(XJB:IN_OUT_WARRANTY)      !List box control field - type derived from field
XJB:REQUEST_DATE       LIKE(XJB:REQUEST_DATE)         !List box control field - type derived from field
XJB:REQUEST_TIME       LIKE(XJB:REQUEST_TIME)         !List box control field - type derived from field
XJB:REPAIR_ETD_DATE    LIKE(XJB:REPAIR_ETD_DATE)      !List box control field - type derived from field
XJB:REPAIR_ETD_TIME    LIKE(XJB:REPAIR_ETD_TIME)      !List box control field - type derived from field
XJB:TR_STATUS          LIKE(XJB:TR_STATUS)            !List box control field - type derived from field
XJB:TR_REASON          LIKE(XJB:TR_REASON)            !List box control field - type derived from field
XJB:CONSUMER_TITLE     LIKE(XJB:CONSUMER_TITLE)       !List box control field - type derived from field
XJB:CONSUMER_FIRST_NAME LIKE(XJB:CONSUMER_FIRST_NAME) !List box control field - type derived from field
XJB:CONSUMER_LAST_NAME LIKE(XJB:CONSUMER_LAST_NAME)   !List box control field - type derived from field
XJB:CONSUMER_TEL_NUMBER1 LIKE(XJB:CONSUMER_TEL_NUMBER1) !List box control field - type derived from field
XJB:CONSUMER_FAX_NUMBER LIKE(XJB:CONSUMER_FAX_NUMBER) !List box control field - type derived from field
XJB:CONSUMER_EMAIL     LIKE(XJB:CONSUMER_EMAIL)       !List box control field - type derived from field
XJB:CONSUMER_COUNTRY   LIKE(XJB:CONSUMER_COUNTRY)     !List box control field - type derived from field
XJB:CONSUMER_REGION    LIKE(XJB:CONSUMER_REGION)      !List box control field - type derived from field
XJB:CONSUMER_POSTCODE  LIKE(XJB:CONSUMER_POSTCODE)    !List box control field - type derived from field
XJB:CONSUMER_CITY      LIKE(XJB:CONSUMER_CITY)        !List box control field - type derived from field
XJB:CONSUMER_STREET    LIKE(XJB:CONSUMER_STREET)      !List box control field - type derived from field
XJB:CONSUMER_HOUSE_NUMBER LIKE(XJB:CONSUMER_HOUSE_NUMBER) !List box control field - type derived from field
XJB:REPAIR_COMPLETE_DATE LIKE(XJB:REPAIR_COMPLETE_DATE) !List box control field - type derived from field
XJB:REPAIR_COMPLETE_TIME LIKE(XJB:REPAIR_COMPLETE_TIME) !List box control field - type derived from field
XJB:GOODS_DELIVERY_DATE LIKE(XJB:GOODS_DELIVERY_DATE) !List box control field - type derived from field
XJB:GOODS_DELIVERY_TIME LIKE(XJB:GOODS_DELIVERY_TIME) !List box control field - type derived from field
XJB:SYMPTOM1_DESC      LIKE(XJB:SYMPTOM1_DESC)        !List box control field - type derived from field
XJB:SYMPTOM2_DESC      LIKE(XJB:SYMPTOM2_DESC)        !List box control field - type derived from field
XJB:SYMPTOM3_DESC      LIKE(XJB:SYMPTOM3_DESC)        !List box control field - type derived from field
XJB:BP_NO              LIKE(XJB:BP_NO)                !List box control field - type derived from field
XJB:INQUIRY_TEXT       LIKE(XJB:INQUIRY_TEXT)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Samsung Browse All Records'),AT(,,382,231),CENTER,SYSTEM,GRAY,MAX,RESIZE,IMM
                       LIST,AT(0,0),USE(?List),IMM,FULL,HVSCROLL,MSG('Browsing Records'),FORMAT('29R|M~RECORD NUMBER~L(2)@n-14@17R|M~RECORD STATE~L(2)@n3@25R|M~EXCEPTION CODE~L(' &|
   '2)@n-14@100R|M~EXCEPTION DESC~L(2)@s255@46R|M~Job Number~L(2)@s8@44R|M~COMPANY~L' &|
   '(2)@s4@45R|M~TR NO~L(2)@s30@47R|M~EDI Account Number~L(2)@s10@54L|M~Model Number' &|
   '~L(2)@s40@24L|M~MSN~L(2)@s16@46L|M~Date Of Purchase~L(2)@d6@26L|M~SERVICE TYPE~L' &|
   '(2)@s2@21L|M~IN OUT WARRANTY~L(2)@s1@40L|M~date booked~L(2)@d6b@20L|M~time booke' &|
   'd~L(2)@t1@40L|M~REPAIR ETD DATE~L(2)@d17@20L|M~REPAIR ETD TIME~L(2)@t7@8L|M~TR S' &|
   'TATUS~L(2)@s2@8L|M~TR REASON~L(2)@s2@70L|M~CONSUMER TITLE~L(2)@s20@160L|M~CONSUM' &|
   'ER FIRST NAME~L(2)@s40@160L|M~Surname~L(2)@s40@120L|M~Telephone Number~L(2)@s30@' &|
   '120L|M~CONSUMER FAX NUMBER~L(2)@s30@964L|M~CONSUMER EMAIL~L(2)@s241@80L|M~CONSUM' &|
   'ER COUNTRY~L(2)@s20@80L|M~CONSUMER REGION~L(2)@s20@40L|M~Postcode~L(2)@s10@160L|' &|
   'M~CONSUMER CITY~L(2)@s40@240L|M~CONSUMER STREET~L(2)@s60@80L|M~CONSUMER HOUSE NU' &|
   'MBER~L(2)@s20@40L|M~REPAIR COMPLETE DATE~L(2)@d17@20L|M~REPAIR COMPLETE TIME~L(2' &|
   ')@t7@40L|M~GOODS DELIVERY DATE~L(2)@d17@20L|M~GOODS DELIVERY TIME~L(2)@t7@160L|M' &|
   '~SYMPTOM 1 DESC~L(2)@s40@160L|M~SYMPTOM 2 DESC~L(2)@s40@160L|M~SYMPTOM 3 DESC~L(' &|
   '2)@s40@40L|M~BP NO~L(2)@s10@1016L|M~INQUIRY TEXT~L(2)@s254@'),FROM(Queue:Browse)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW3                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseAllRecords')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?List
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='BrowseAllRecords'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:XMLJOBS.Open
  SELF.FilesOpened = True
  BRW3.Init(?List,Queue:Browse.ViewPosition,BRW3::View:Browse,Queue:Browse,Relate:XMLJOBS,SELF)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  BRW3.Q &= Queue:Browse
  BRW3.AddSortOrder(,)
  BRW3.AddField(XJB:RECORD_NUMBER,BRW3.Q.XJB:RECORD_NUMBER)
  BRW3.AddField(XJB:RECORD_STATE,BRW3.Q.XJB:RECORD_STATE)
  BRW3.AddField(XJB:EXCEPTION_CODE,BRW3.Q.XJB:EXCEPTION_CODE)
  BRW3.AddField(XJB:EXCEPTION_DESC,BRW3.Q.XJB:EXCEPTION_DESC)
  BRW3.AddField(XJB:JOB_NO,BRW3.Q.XJB:JOB_NO)
  BRW3.AddField(XJB:COMPANY,BRW3.Q.XJB:COMPANY)
  BRW3.AddField(XJB:TR_NO,BRW3.Q.XJB:TR_NO)
  BRW3.AddField(XJB:ASC_CODE,BRW3.Q.XJB:ASC_CODE)
  BRW3.AddField(XJB:MODEL_CODE,BRW3.Q.XJB:MODEL_CODE)
  BRW3.AddField(XJB:SERIAL_NO,BRW3.Q.XJB:SERIAL_NO)
  BRW3.AddField(XJB:PURCHASE_DATE,BRW3.Q.XJB:PURCHASE_DATE)
  BRW3.AddField(XJB:SERVICE_TYPE,BRW3.Q.XJB:SERVICE_TYPE)
  BRW3.AddField(XJB:IN_OUT_WARRANTY,BRW3.Q.XJB:IN_OUT_WARRANTY)
  BRW3.AddField(XJB:REQUEST_DATE,BRW3.Q.XJB:REQUEST_DATE)
  BRW3.AddField(XJB:REQUEST_TIME,BRW3.Q.XJB:REQUEST_TIME)
  BRW3.AddField(XJB:REPAIR_ETD_DATE,BRW3.Q.XJB:REPAIR_ETD_DATE)
  BRW3.AddField(XJB:REPAIR_ETD_TIME,BRW3.Q.XJB:REPAIR_ETD_TIME)
  BRW3.AddField(XJB:TR_STATUS,BRW3.Q.XJB:TR_STATUS)
  BRW3.AddField(XJB:TR_REASON,BRW3.Q.XJB:TR_REASON)
  BRW3.AddField(XJB:CONSUMER_TITLE,BRW3.Q.XJB:CONSUMER_TITLE)
  BRW3.AddField(XJB:CONSUMER_FIRST_NAME,BRW3.Q.XJB:CONSUMER_FIRST_NAME)
  BRW3.AddField(XJB:CONSUMER_LAST_NAME,BRW3.Q.XJB:CONSUMER_LAST_NAME)
  BRW3.AddField(XJB:CONSUMER_TEL_NUMBER1,BRW3.Q.XJB:CONSUMER_TEL_NUMBER1)
  BRW3.AddField(XJB:CONSUMER_FAX_NUMBER,BRW3.Q.XJB:CONSUMER_FAX_NUMBER)
  BRW3.AddField(XJB:CONSUMER_EMAIL,BRW3.Q.XJB:CONSUMER_EMAIL)
  BRW3.AddField(XJB:CONSUMER_COUNTRY,BRW3.Q.XJB:CONSUMER_COUNTRY)
  BRW3.AddField(XJB:CONSUMER_REGION,BRW3.Q.XJB:CONSUMER_REGION)
  BRW3.AddField(XJB:CONSUMER_POSTCODE,BRW3.Q.XJB:CONSUMER_POSTCODE)
  BRW3.AddField(XJB:CONSUMER_CITY,BRW3.Q.XJB:CONSUMER_CITY)
  BRW3.AddField(XJB:CONSUMER_STREET,BRW3.Q.XJB:CONSUMER_STREET)
  BRW3.AddField(XJB:CONSUMER_HOUSE_NUMBER,BRW3.Q.XJB:CONSUMER_HOUSE_NUMBER)
  BRW3.AddField(XJB:REPAIR_COMPLETE_DATE,BRW3.Q.XJB:REPAIR_COMPLETE_DATE)
  BRW3.AddField(XJB:REPAIR_COMPLETE_TIME,BRW3.Q.XJB:REPAIR_COMPLETE_TIME)
  BRW3.AddField(XJB:GOODS_DELIVERY_DATE,BRW3.Q.XJB:GOODS_DELIVERY_DATE)
  BRW3.AddField(XJB:GOODS_DELIVERY_TIME,BRW3.Q.XJB:GOODS_DELIVERY_TIME)
  BRW3.AddField(XJB:SYMPTOM1_DESC,BRW3.Q.XJB:SYMPTOM1_DESC)
  BRW3.AddField(XJB:SYMPTOM2_DESC,BRW3.Q.XJB:SYMPTOM2_DESC)
  BRW3.AddField(XJB:SYMPTOM3_DESC,BRW3.Q.XJB:SYMPTOM3_DESC)
  BRW3.AddField(XJB:BP_NO,BRW3.Q.XJB:BP_NO)
  BRW3.AddField(XJB:INQUIRY_TEXT,BRW3.Q.XJB:INQUIRY_TEXT)
  BRW3.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:XMLJOBS.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='BrowseAllRecords'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:GainFocus
      IF window{PROP:Iconize}=TRUE
        window{PROP:Iconize}=FALSE
        IF window{PROP:Active}<>TRUE
           window{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

