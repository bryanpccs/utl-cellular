

   MEMBER('samxml.clw')                               ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SAMXM019.INC'),ONCE        !Local module procedure declarations
                     END


MPXUpdateException PROCEDURE                          !Generated from procedure template - Window

ActionMessage        CSTRING(40)
CopyPostcode         LONG
CopyModel            LONG
CopyMSN              LONG
CopyMissingFields    LONG
CopyExport           LONG
CopyDuplicate        LONG
CopyAccount          LONG
CopyInspectionType   LONG
FailPostcode         LONG
FailModel            LONG
FailMSN              LONG
FailMissingFields    LONG
FailExport           LONG
FailDuplicate        LONG
FailAccount          LONG
FailInspectionType   LONG
History::MXJ:Record  LIKE(MXJ:RECORD),STATIC
FormWindow           WINDOW('Update MPX Exception'),AT(,,455,222),FONT('Arial',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,448,188),USE(?Sheet1),SPREAD
                         TAB('Update Exception'),USE(?Tab1)
                           PROMPT('Inspection Required'),AT(232,20),USE(?Prompt22)
                           ENTRY(@s50),AT(308,20,124,10),USE(MXJ:InspType),UPR
                           ENTRY(@s50),AT(84,20,124,10),USE(MXJ:CustCTN)
                           PROMPT('Customer Contact'),AT(12,20),USE(?Prompt1)
                           PROMPT('Value'),AT(12,32),USE(?Prompt3)
                           PROMPT('Model'),AT(12,44),USE(?Prompt4)
                           PROMPT('Last Name'),AT(12,80),USE(?Prompt7)
                           ENTRY(@s50),AT(84,68,124,10),USE(MXJ:CustFirstName),UPR
                           ENTRY(@s50),AT(84,176,124,10),USE(MXJ:EnvLetter),UPR
                           PROMPT('Title'),AT(12,56),USE(?Prompt5)
                           PROMPT('Envelope Type'),AT(232,32),USE(?Prompt14)
                           ENTRY(@n-14),AT(84,32,124,10),USE(MXJ:QuoteValue),UPR
                           PROMPT('Address 2'),AT(12,104),USE(?Prompt9)
                           ENTRY(@s50),AT(84,104,124,10),USE(MXJ:CustAdd2),UPR
                           CHECK('Charger Available'),AT(232,100),USE(MXJ:QuoteACC1),VALUE('1','0')
                           ENTRY(@s50),AT(84,164,124,10),USE(MXJ:CustNetwork),UPR
                           ENTRY(@s50),AT(84,92,124,10),USE(MXJ:CustAdd1),UPR
                           CHECK('Display Damage'),AT(232,76),USE(MXJ:QuoteQ2),VALUE('1','0')
                           PROMPT('Address 4'),AT(12,128),USE(?Prompt11)
                           PROMPT('Address 3'),AT(12,116),USE(?Prompt10)
                           ENTRY(@s50),AT(84,116,124,10),USE(MXJ:CustAdd3),UPR
                           CHECK('Water Damage'),AT(232,88),USE(MXJ:QuoteQ3),VALUE('1','0')
                           ENTRY(@s50),AT(84,152,124,10),USE(MXJ:CustRef),UPR
                           CHECK('Memory Card'),AT(232,124),USE(MXJ:QuoteACC3),VALUE('1','0')
                           ENTRY(@s50),AT(84,140,124,10),USE(MXJ:CustPostCode),UPR
                           CHECK('Manual'),AT(232,136),USE(MXJ:QuoteACC4),VALUE('1','0')
                           CHECK('Battery Available'),AT(232,112),USE(MXJ:QuoteACC2),VALUE('1','0')
                           CHECK('Box'),AT(232,148),USE(MXJ:QuoteACC5),VALUE('1','0')
                           ENTRY(@s50),AT(84,80,124,10),USE(MXJ:CustLastName),UPR
                           CHECK('Working Order'),AT(232,64),USE(MXJ:QuoteQ1),VALUE('1','0')
                           ENTRY(@n-14),AT(308,44,124,10),USE(MXJ:EnvCount),UPR
                           ENTRY(@s50),AT(308,32,124,10),USE(MXJ:EnvType),UPR
                           PROMPT('Postcode'),AT(12,140),USE(?Prompt12)
                           ENTRY(@s50),AT(84,128,124,10),USE(MXJ:CustAdd4),UPR
                           PROMPT('First Name'),AT(12,68),USE(?Prompt6)
                           ENTRY(@s50),AT(84,56,124,10),USE(MXJ:CustTitle),UPR
                           ENTRY(@s50),AT(84,44,124,10),USE(MXJ:ModelID),UPR
                           PROMPT('EnvCount'),AT(232,44),USE(?Prompt15)
                           PROMPT('Address 1'),AT(12,92),USE(?Prompt8)
                           PROMPT('Client Ref '),AT(12,152),USE(?Prompt26)
                           PROMPT('Network'),AT(12,164),USE(?Prompt27)
                           PROMPT('Mail Merge Letter'),AT(12,176),USE(?Prompt28)
                         END
                         TAB('Exception Codes'),USE(?Tab2)
                           CHECK('Postcode'),AT(20,44),USE(FailPostcode),TRN,VALUE('1','0')
                           CHECK('Model No.'),AT(20,56),USE(FailModel),TRN,VALUE('1','0')
                           CHECK('MSN'),AT(20,68),USE(FailMSN),TRN,VALUE('1','0')
                           TEXT,AT(120,32,320,152),USE(MXJ:Exception_Desc),VSCROLL,READONLY
                           CHECK('Missing Fields'),AT(20,80),USE(FailMissingFields),TRN,VALUE('1','0')
                           CHECK('Export Failure'),AT(20,92),USE(FailExport),TRN,VALUE('1','0')
                           GROUP('Errors'),AT(8,28,104,116),USE(?Group1),BOXED
                             CHECK('Duplicate'),AT(20,104),USE(FailDuplicate),TRN,VALUE('1','0')
                             CHECK('Invalid Account'),AT(20,116),USE(FailAccount),TRN,VALUE('1','0')
                             CHECK('Invalid Inspection Type'),AT(20,128),USE(FailInspectionType),TRN,VALUE('1','0')
                           END
                           PROMPT('Description'),AT(120,20),USE(?Prompt24),TRN
                         END
                       END
                       PANEL,AT(4,196,452,24),USE(?Panel1),FILL(COLOR:Silver)
                       PROMPT('MPX Ref No.'),AT(12,200),USE(?Prompt25),TRN
                       BUTTON('OK'),AT(340,200,56,16),USE(?OK),TRN,LEFT,ICON('ok.gif'),DEFAULT,REQ
                       BUTTON('Cancel'),AT(396,200,56,16),USE(?Cancel),TRN,LEFT,ICON('cancel.gif')
                       ENTRY(@s15),AT(60,200,124,10),USE(MXJ:ID),COLOR(COLOR:Silver),UPR,READONLY
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt22{prop:FontColor} = -1
    ?Prompt22{prop:Color} = 15066597
    If ?MXJ:InspType{prop:ReadOnly} = True
        ?MXJ:InspType{prop:FontColor} = 65793
        ?MXJ:InspType{prop:Color} = 15066597
    Elsif ?MXJ:InspType{prop:Req} = True
        ?MXJ:InspType{prop:FontColor} = 65793
        ?MXJ:InspType{prop:Color} = 8454143
    Else ! If ?MXJ:InspType{prop:Req} = True
        ?MXJ:InspType{prop:FontColor} = 65793
        ?MXJ:InspType{prop:Color} = 16777215
    End ! If ?MXJ:InspType{prop:Req} = True
    ?MXJ:InspType{prop:Trn} = 0
    ?MXJ:InspType{prop:FontStyle} = font:Bold
    If ?MXJ:CustCTN{prop:ReadOnly} = True
        ?MXJ:CustCTN{prop:FontColor} = 65793
        ?MXJ:CustCTN{prop:Color} = 15066597
    Elsif ?MXJ:CustCTN{prop:Req} = True
        ?MXJ:CustCTN{prop:FontColor} = 65793
        ?MXJ:CustCTN{prop:Color} = 8454143
    Else ! If ?MXJ:CustCTN{prop:Req} = True
        ?MXJ:CustCTN{prop:FontColor} = 65793
        ?MXJ:CustCTN{prop:Color} = 16777215
    End ! If ?MXJ:CustCTN{prop:Req} = True
    ?MXJ:CustCTN{prop:Trn} = 0
    ?MXJ:CustCTN{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    If ?MXJ:CustFirstName{prop:ReadOnly} = True
        ?MXJ:CustFirstName{prop:FontColor} = 65793
        ?MXJ:CustFirstName{prop:Color} = 15066597
    Elsif ?MXJ:CustFirstName{prop:Req} = True
        ?MXJ:CustFirstName{prop:FontColor} = 65793
        ?MXJ:CustFirstName{prop:Color} = 8454143
    Else ! If ?MXJ:CustFirstName{prop:Req} = True
        ?MXJ:CustFirstName{prop:FontColor} = 65793
        ?MXJ:CustFirstName{prop:Color} = 16777215
    End ! If ?MXJ:CustFirstName{prop:Req} = True
    ?MXJ:CustFirstName{prop:Trn} = 0
    ?MXJ:CustFirstName{prop:FontStyle} = font:Bold
    If ?MXJ:EnvLetter{prop:ReadOnly} = True
        ?MXJ:EnvLetter{prop:FontColor} = 65793
        ?MXJ:EnvLetter{prop:Color} = 15066597
    Elsif ?MXJ:EnvLetter{prop:Req} = True
        ?MXJ:EnvLetter{prop:FontColor} = 65793
        ?MXJ:EnvLetter{prop:Color} = 8454143
    Else ! If ?MXJ:EnvLetter{prop:Req} = True
        ?MXJ:EnvLetter{prop:FontColor} = 65793
        ?MXJ:EnvLetter{prop:Color} = 16777215
    End ! If ?MXJ:EnvLetter{prop:Req} = True
    ?MXJ:EnvLetter{prop:Trn} = 0
    ?MXJ:EnvLetter{prop:FontStyle} = font:Bold
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?Prompt14{prop:FontColor} = -1
    ?Prompt14{prop:Color} = 15066597
    If ?MXJ:QuoteValue{prop:ReadOnly} = True
        ?MXJ:QuoteValue{prop:FontColor} = 65793
        ?MXJ:QuoteValue{prop:Color} = 15066597
    Elsif ?MXJ:QuoteValue{prop:Req} = True
        ?MXJ:QuoteValue{prop:FontColor} = 65793
        ?MXJ:QuoteValue{prop:Color} = 8454143
    Else ! If ?MXJ:QuoteValue{prop:Req} = True
        ?MXJ:QuoteValue{prop:FontColor} = 65793
        ?MXJ:QuoteValue{prop:Color} = 16777215
    End ! If ?MXJ:QuoteValue{prop:Req} = True
    ?MXJ:QuoteValue{prop:Trn} = 0
    ?MXJ:QuoteValue{prop:FontStyle} = font:Bold
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    If ?MXJ:CustAdd2{prop:ReadOnly} = True
        ?MXJ:CustAdd2{prop:FontColor} = 65793
        ?MXJ:CustAdd2{prop:Color} = 15066597
    Elsif ?MXJ:CustAdd2{prop:Req} = True
        ?MXJ:CustAdd2{prop:FontColor} = 65793
        ?MXJ:CustAdd2{prop:Color} = 8454143
    Else ! If ?MXJ:CustAdd2{prop:Req} = True
        ?MXJ:CustAdd2{prop:FontColor} = 65793
        ?MXJ:CustAdd2{prop:Color} = 16777215
    End ! If ?MXJ:CustAdd2{prop:Req} = True
    ?MXJ:CustAdd2{prop:Trn} = 0
    ?MXJ:CustAdd2{prop:FontStyle} = font:Bold
    ?MXJ:QuoteACC1{prop:Font,3} = -1
    ?MXJ:QuoteACC1{prop:Color} = 15066597
    ?MXJ:QuoteACC1{prop:Trn} = 0
    If ?MXJ:CustNetwork{prop:ReadOnly} = True
        ?MXJ:CustNetwork{prop:FontColor} = 65793
        ?MXJ:CustNetwork{prop:Color} = 15066597
    Elsif ?MXJ:CustNetwork{prop:Req} = True
        ?MXJ:CustNetwork{prop:FontColor} = 65793
        ?MXJ:CustNetwork{prop:Color} = 8454143
    Else ! If ?MXJ:CustNetwork{prop:Req} = True
        ?MXJ:CustNetwork{prop:FontColor} = 65793
        ?MXJ:CustNetwork{prop:Color} = 16777215
    End ! If ?MXJ:CustNetwork{prop:Req} = True
    ?MXJ:CustNetwork{prop:Trn} = 0
    ?MXJ:CustNetwork{prop:FontStyle} = font:Bold
    If ?MXJ:CustAdd1{prop:ReadOnly} = True
        ?MXJ:CustAdd1{prop:FontColor} = 65793
        ?MXJ:CustAdd1{prop:Color} = 15066597
    Elsif ?MXJ:CustAdd1{prop:Req} = True
        ?MXJ:CustAdd1{prop:FontColor} = 65793
        ?MXJ:CustAdd1{prop:Color} = 8454143
    Else ! If ?MXJ:CustAdd1{prop:Req} = True
        ?MXJ:CustAdd1{prop:FontColor} = 65793
        ?MXJ:CustAdd1{prop:Color} = 16777215
    End ! If ?MXJ:CustAdd1{prop:Req} = True
    ?MXJ:CustAdd1{prop:Trn} = 0
    ?MXJ:CustAdd1{prop:FontStyle} = font:Bold
    ?MXJ:QuoteQ2{prop:Font,3} = -1
    ?MXJ:QuoteQ2{prop:Color} = 15066597
    ?MXJ:QuoteQ2{prop:Trn} = 0
    ?Prompt11{prop:FontColor} = -1
    ?Prompt11{prop:Color} = 15066597
    ?Prompt10{prop:FontColor} = -1
    ?Prompt10{prop:Color} = 15066597
    If ?MXJ:CustAdd3{prop:ReadOnly} = True
        ?MXJ:CustAdd3{prop:FontColor} = 65793
        ?MXJ:CustAdd3{prop:Color} = 15066597
    Elsif ?MXJ:CustAdd3{prop:Req} = True
        ?MXJ:CustAdd3{prop:FontColor} = 65793
        ?MXJ:CustAdd3{prop:Color} = 8454143
    Else ! If ?MXJ:CustAdd3{prop:Req} = True
        ?MXJ:CustAdd3{prop:FontColor} = 65793
        ?MXJ:CustAdd3{prop:Color} = 16777215
    End ! If ?MXJ:CustAdd3{prop:Req} = True
    ?MXJ:CustAdd3{prop:Trn} = 0
    ?MXJ:CustAdd3{prop:FontStyle} = font:Bold
    ?MXJ:QuoteQ3{prop:Font,3} = -1
    ?MXJ:QuoteQ3{prop:Color} = 15066597
    ?MXJ:QuoteQ3{prop:Trn} = 0
    If ?MXJ:CustRef{prop:ReadOnly} = True
        ?MXJ:CustRef{prop:FontColor} = 65793
        ?MXJ:CustRef{prop:Color} = 15066597
    Elsif ?MXJ:CustRef{prop:Req} = True
        ?MXJ:CustRef{prop:FontColor} = 65793
        ?MXJ:CustRef{prop:Color} = 8454143
    Else ! If ?MXJ:CustRef{prop:Req} = True
        ?MXJ:CustRef{prop:FontColor} = 65793
        ?MXJ:CustRef{prop:Color} = 16777215
    End ! If ?MXJ:CustRef{prop:Req} = True
    ?MXJ:CustRef{prop:Trn} = 0
    ?MXJ:CustRef{prop:FontStyle} = font:Bold
    ?MXJ:QuoteACC3{prop:Font,3} = -1
    ?MXJ:QuoteACC3{prop:Color} = 15066597
    ?MXJ:QuoteACC3{prop:Trn} = 0
    If ?MXJ:CustPostCode{prop:ReadOnly} = True
        ?MXJ:CustPostCode{prop:FontColor} = 65793
        ?MXJ:CustPostCode{prop:Color} = 15066597
    Elsif ?MXJ:CustPostCode{prop:Req} = True
        ?MXJ:CustPostCode{prop:FontColor} = 65793
        ?MXJ:CustPostCode{prop:Color} = 8454143
    Else ! If ?MXJ:CustPostCode{prop:Req} = True
        ?MXJ:CustPostCode{prop:FontColor} = 65793
        ?MXJ:CustPostCode{prop:Color} = 16777215
    End ! If ?MXJ:CustPostCode{prop:Req} = True
    ?MXJ:CustPostCode{prop:Trn} = 0
    ?MXJ:CustPostCode{prop:FontStyle} = font:Bold
    ?MXJ:QuoteACC4{prop:Font,3} = -1
    ?MXJ:QuoteACC4{prop:Color} = 15066597
    ?MXJ:QuoteACC4{prop:Trn} = 0
    ?MXJ:QuoteACC2{prop:Font,3} = -1
    ?MXJ:QuoteACC2{prop:Color} = 15066597
    ?MXJ:QuoteACC2{prop:Trn} = 0
    ?MXJ:QuoteACC5{prop:Font,3} = -1
    ?MXJ:QuoteACC5{prop:Color} = 15066597
    ?MXJ:QuoteACC5{prop:Trn} = 0
    If ?MXJ:CustLastName{prop:ReadOnly} = True
        ?MXJ:CustLastName{prop:FontColor} = 65793
        ?MXJ:CustLastName{prop:Color} = 15066597
    Elsif ?MXJ:CustLastName{prop:Req} = True
        ?MXJ:CustLastName{prop:FontColor} = 65793
        ?MXJ:CustLastName{prop:Color} = 8454143
    Else ! If ?MXJ:CustLastName{prop:Req} = True
        ?MXJ:CustLastName{prop:FontColor} = 65793
        ?MXJ:CustLastName{prop:Color} = 16777215
    End ! If ?MXJ:CustLastName{prop:Req} = True
    ?MXJ:CustLastName{prop:Trn} = 0
    ?MXJ:CustLastName{prop:FontStyle} = font:Bold
    ?MXJ:QuoteQ1{prop:Font,3} = -1
    ?MXJ:QuoteQ1{prop:Color} = 15066597
    ?MXJ:QuoteQ1{prop:Trn} = 0
    If ?MXJ:EnvCount{prop:ReadOnly} = True
        ?MXJ:EnvCount{prop:FontColor} = 65793
        ?MXJ:EnvCount{prop:Color} = 15066597
    Elsif ?MXJ:EnvCount{prop:Req} = True
        ?MXJ:EnvCount{prop:FontColor} = 65793
        ?MXJ:EnvCount{prop:Color} = 8454143
    Else ! If ?MXJ:EnvCount{prop:Req} = True
        ?MXJ:EnvCount{prop:FontColor} = 65793
        ?MXJ:EnvCount{prop:Color} = 16777215
    End ! If ?MXJ:EnvCount{prop:Req} = True
    ?MXJ:EnvCount{prop:Trn} = 0
    ?MXJ:EnvCount{prop:FontStyle} = font:Bold
    If ?MXJ:EnvType{prop:ReadOnly} = True
        ?MXJ:EnvType{prop:FontColor} = 65793
        ?MXJ:EnvType{prop:Color} = 15066597
    Elsif ?MXJ:EnvType{prop:Req} = True
        ?MXJ:EnvType{prop:FontColor} = 65793
        ?MXJ:EnvType{prop:Color} = 8454143
    Else ! If ?MXJ:EnvType{prop:Req} = True
        ?MXJ:EnvType{prop:FontColor} = 65793
        ?MXJ:EnvType{prop:Color} = 16777215
    End ! If ?MXJ:EnvType{prop:Req} = True
    ?MXJ:EnvType{prop:Trn} = 0
    ?MXJ:EnvType{prop:FontStyle} = font:Bold
    ?Prompt12{prop:FontColor} = -1
    ?Prompt12{prop:Color} = 15066597
    If ?MXJ:CustAdd4{prop:ReadOnly} = True
        ?MXJ:CustAdd4{prop:FontColor} = 65793
        ?MXJ:CustAdd4{prop:Color} = 15066597
    Elsif ?MXJ:CustAdd4{prop:Req} = True
        ?MXJ:CustAdd4{prop:FontColor} = 65793
        ?MXJ:CustAdd4{prop:Color} = 8454143
    Else ! If ?MXJ:CustAdd4{prop:Req} = True
        ?MXJ:CustAdd4{prop:FontColor} = 65793
        ?MXJ:CustAdd4{prop:Color} = 16777215
    End ! If ?MXJ:CustAdd4{prop:Req} = True
    ?MXJ:CustAdd4{prop:Trn} = 0
    ?MXJ:CustAdd4{prop:FontStyle} = font:Bold
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    If ?MXJ:CustTitle{prop:ReadOnly} = True
        ?MXJ:CustTitle{prop:FontColor} = 65793
        ?MXJ:CustTitle{prop:Color} = 15066597
    Elsif ?MXJ:CustTitle{prop:Req} = True
        ?MXJ:CustTitle{prop:FontColor} = 65793
        ?MXJ:CustTitle{prop:Color} = 8454143
    Else ! If ?MXJ:CustTitle{prop:Req} = True
        ?MXJ:CustTitle{prop:FontColor} = 65793
        ?MXJ:CustTitle{prop:Color} = 16777215
    End ! If ?MXJ:CustTitle{prop:Req} = True
    ?MXJ:CustTitle{prop:Trn} = 0
    ?MXJ:CustTitle{prop:FontStyle} = font:Bold
    If ?MXJ:ModelID{prop:ReadOnly} = True
        ?MXJ:ModelID{prop:FontColor} = 65793
        ?MXJ:ModelID{prop:Color} = 15066597
    Elsif ?MXJ:ModelID{prop:Req} = True
        ?MXJ:ModelID{prop:FontColor} = 65793
        ?MXJ:ModelID{prop:Color} = 8454143
    Else ! If ?MXJ:ModelID{prop:Req} = True
        ?MXJ:ModelID{prop:FontColor} = 65793
        ?MXJ:ModelID{prop:Color} = 16777215
    End ! If ?MXJ:ModelID{prop:Req} = True
    ?MXJ:ModelID{prop:Trn} = 0
    ?MXJ:ModelID{prop:FontStyle} = font:Bold
    ?Prompt15{prop:FontColor} = -1
    ?Prompt15{prop:Color} = 15066597
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    ?Prompt26{prop:FontColor} = -1
    ?Prompt26{prop:Color} = 15066597
    ?Prompt27{prop:FontColor} = -1
    ?Prompt27{prop:Color} = 15066597
    ?Prompt28{prop:FontColor} = -1
    ?Prompt28{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?FailPostcode{prop:Font,3} = -1
    ?FailPostcode{prop:Color} = 15066597
    ?FailPostcode{prop:Trn} = 0
    ?FailModel{prop:Font,3} = -1
    ?FailModel{prop:Color} = 15066597
    ?FailModel{prop:Trn} = 0
    ?FailMSN{prop:Font,3} = -1
    ?FailMSN{prop:Color} = 15066597
    ?FailMSN{prop:Trn} = 0
    If ?MXJ:Exception_Desc{prop:ReadOnly} = True
        ?MXJ:Exception_Desc{prop:FontColor} = 65793
        ?MXJ:Exception_Desc{prop:Color} = 15066597
    Elsif ?MXJ:Exception_Desc{prop:Req} = True
        ?MXJ:Exception_Desc{prop:FontColor} = 65793
        ?MXJ:Exception_Desc{prop:Color} = 8454143
    Else ! If ?MXJ:Exception_Desc{prop:Req} = True
        ?MXJ:Exception_Desc{prop:FontColor} = 65793
        ?MXJ:Exception_Desc{prop:Color} = 16777215
    End ! If ?MXJ:Exception_Desc{prop:Req} = True
    ?MXJ:Exception_Desc{prop:Trn} = 0
    ?MXJ:Exception_Desc{prop:FontStyle} = font:Bold
    ?FailMissingFields{prop:Font,3} = -1
    ?FailMissingFields{prop:Color} = 15066597
    ?FailMissingFields{prop:Trn} = 0
    ?FailExport{prop:Font,3} = -1
    ?FailExport{prop:Color} = 15066597
    ?FailExport{prop:Trn} = 0
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?FailDuplicate{prop:Font,3} = -1
    ?FailDuplicate{prop:Color} = 15066597
    ?FailDuplicate{prop:Trn} = 0
    ?FailAccount{prop:Font,3} = -1
    ?FailAccount{prop:Color} = 15066597
    ?FailAccount{prop:Trn} = 0
    ?FailInspectionType{prop:Font,3} = -1
    ?FailInspectionType{prop:Color} = 15066597
    ?FailInspectionType{prop:Trn} = 0
    ?Prompt24{prop:FontColor} = -1
    ?Prompt24{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597

    ?Prompt25{prop:FontColor} = -1
    ?Prompt25{prop:Color} = 15066597
    If ?MXJ:ID{prop:ReadOnly} = True
        ?MXJ:ID{prop:FontColor} = 65793
        ?MXJ:ID{prop:Color} = 15066597
    Elsif ?MXJ:ID{prop:Req} = True
        ?MXJ:ID{prop:FontColor} = 65793
        ?MXJ:ID{prop:Color} = 8454143
    Else ! If ?MXJ:ID{prop:Req} = True
        ?MXJ:ID{prop:FontColor} = 65793
        ?MXJ:ID{prop:Color} = 16777215
    End ! If ?MXJ:ID{prop:Req} = True
    ?MXJ:ID{prop:Trn} = 0
    ?MXJ:ID{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('MPXUpdateException')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt22
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(MXJ:Record,History::MXJ:Record)
  SELF.AddHistoryField(?MXJ:InspType,56)
  SELF.AddHistoryField(?MXJ:CustCTN,34)
  SELF.AddHistoryField(?MXJ:CustFirstName,46)
  SELF.AddHistoryField(?MXJ:EnvLetter,55)
  SELF.AddHistoryField(?MXJ:QuoteValue,35)
  SELF.AddHistoryField(?MXJ:CustAdd2,49)
  SELF.AddHistoryField(?MXJ:QuoteACC1,40)
  SELF.AddHistoryField(?MXJ:CustNetwork,54)
  SELF.AddHistoryField(?MXJ:CustAdd1,48)
  SELF.AddHistoryField(?MXJ:QuoteQ2,38)
  SELF.AddHistoryField(?MXJ:CustAdd3,50)
  SELF.AddHistoryField(?MXJ:QuoteQ3,39)
  SELF.AddHistoryField(?MXJ:CustRef,53)
  SELF.AddHistoryField(?MXJ:QuoteACC3,42)
  SELF.AddHistoryField(?MXJ:CustPostCode,52)
  SELF.AddHistoryField(?MXJ:QuoteACC4,43)
  SELF.AddHistoryField(?MXJ:QuoteACC2,41)
  SELF.AddHistoryField(?MXJ:QuoteACC5,44)
  SELF.AddHistoryField(?MXJ:CustLastName,47)
  SELF.AddHistoryField(?MXJ:QuoteQ1,37)
  SELF.AddHistoryField(?MXJ:EnvCount,58)
  SELF.AddHistoryField(?MXJ:EnvType,57)
  SELF.AddHistoryField(?MXJ:CustAdd4,51)
  SELF.AddHistoryField(?MXJ:CustTitle,45)
  SELF.AddHistoryField(?MXJ:ModelID,36)
  SELF.AddHistoryField(?MXJ:Exception_Desc,5)
  SELF.AddHistoryField(?MXJ:ID,8)
  SELF.AddUpdateFile(Access:MPXJOBS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MPXJOBS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MPXJOBS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  FailPostcode        = BAND(mxj:EXCEPTION_CODE, XC_POSTCODE)
  FailModel           = BAND(mxj:EXCEPTION_CODE, XC_MODEL)
  FailMSN             = BAND(mxj:EXCEPTION_CODE, XC_MSN)
  FailMissingFields   = BAND(mxj:EXCEPTION_CODE, XC_MISSING_FIELDS)
  FailExport          = BAND(mxj:EXCEPTION_CODE, XC_EXPORT_FAIL)
  FailDuplicate       = BAND(mxj:EXCEPTION_CODE, XC_DUPLICATE)
  FailAccount         = BAND(mxj:EXCEPTION_CODE, XC_ACCOUNT_NO)
  FailInspectionType  = BAND(mxj:EXCEPTION_CODE, XC_INSPECTION_TYPE)
  
  CopyPostcode        = FailPostcode
  CopyModel           = FailModel
  CopyMSN             = FailMSN
  CopyMissingFields   = FailMissingFields
  CopyExport          = FailExport
  CopyDuplicate       = FailDuplicate
  CopyAccount         = FailAccount
  CopyInspectionType  = FailInspectionType
  OPEN(FormWindow)
  SELF.Opened=True
  Do RecolourWindow
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MPXJOBS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?FailPostcode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailPostcode, Accepted)
      FailPostcode = CopyPostcode
      DISPLAY()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailPostcode, Accepted)
    OF ?FailModel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailModel, Accepted)
      FailModel = CopyModel
      DISPLAY()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailModel, Accepted)
    OF ?FailMSN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailMSN, Accepted)
      FailMSN = CopyMSN
      DISPLAY()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailMSN, Accepted)
    OF ?FailMissingFields
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailMissingFields, Accepted)
      FailMissingFields = CopyMissingFields
      DISPLAY()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailMissingFields, Accepted)
    OF ?FailExport
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailExport, Accepted)
      FailExport = CopyExport
      DISPLAY()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailExport, Accepted)
    OF ?FailDuplicate
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailDuplicate, Accepted)
      FailDuplicate = CopyDuplicate
      DISPLAY()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailDuplicate, Accepted)
    OF ?FailAccount
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailAccount, Accepted)
      FailAccount = CopyAccount
      DISPLAY()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailAccount, Accepted)
    OF ?FailInspectionType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailInspectionType, Accepted)
      FailInspectionType = CopyInspectionType
      DISPLAY()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailInspectionType, Accepted)
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      if  mxj:EXCEPTION_CODE = XC_EXPORT_FAIL
          mxj:RECORD_STATE = RS_ACTIVE_JOB
      else
          mxj:RECORD_STATE = RS_NEW_IMPORT
      end
      mxj:EXCEPTION_CODE = XC_NONE
      mxj:EXCEPTION_DESC = ''
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

