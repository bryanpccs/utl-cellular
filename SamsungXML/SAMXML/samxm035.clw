

   MEMBER('samxml.clw')                               ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SAMXM035.INC'),ONCE        !Local module procedure declarations
                     END


MPXBrowseAllRecords PROCEDURE                         !Generated from procedure template - Window

ThisThreadActive BYTE
BRW3::View:Browse    VIEW(MPXJOBS)
                       PROJECT(MXJ:RecordNo)
                       PROJECT(MXJ:Record_State)
                       PROJECT(MXJ:ReportCode)
                       PROJECT(MXJ:Exception_Code)
                       PROJECT(MXJ:Exception_Desc)
                       PROJECT(MXJ:ImportDate)
                       PROJECT(MXJ:ImportTime)
                       PROJECT(MXJ:ID)
                       PROJECT(MXJ:InspACRJobNo)
                       PROJECT(MXJ:EnvDateSent)
                       PROJECT(MXJ:InspACRStatus)
                       PROJECT(MXJ:InspDateRecd)
                       PROJECT(MXJ:InspModelID)
                       PROJECT(MXJ:InspFlag)
                       PROJECT(MXJ:InspAttention)
                       PROJECT(MXJ:InspQty)
                       PROJECT(MXJ:InspPackageCond)
                       PROJECT(MXJ:InspQ1)
                       PROJECT(MXJ:InspQ2)
                       PROJECT(MXJ:InspQ3)
                       PROJECT(MXJ:InspACC1)
                       PROJECT(MXJ:InspACC2)
                       PROJECT(MXJ:InspACC3)
                       PROJECT(MXJ:InspACC4)
                       PROJECT(MXJ:InspACC5)
                       PROJECT(MXJ:InspGrade)
                       PROJECT(MXJ:InspIMEI)
                       PROJECT(MXJ:InspNotes)
                       PROJECT(MXJ:InspDate)
                       PROJECT(MXJ:InspBrand)
                       PROJECT(MXJ:InspInWarranty)
                       PROJECT(MXJ:DespatchConsignment)
                       PROJECT(MXJ:DespatchDate)
                       PROJECT(MXJ:CustCTN)
                       PROJECT(MXJ:QuoteValue)
                       PROJECT(MXJ:ModelID)
                       PROJECT(MXJ:QuoteQ1)
                       PROJECT(MXJ:QuoteQ2)
                       PROJECT(MXJ:QuoteQ3)
                       PROJECT(MXJ:QuoteACC1)
                       PROJECT(MXJ:QuoteACC2)
                       PROJECT(MXJ:QuoteACC3)
                       PROJECT(MXJ:QuoteACC4)
                       PROJECT(MXJ:QuoteACC5)
                       PROJECT(MXJ:CustTitle)
                       PROJECT(MXJ:CustFirstName)
                       PROJECT(MXJ:CustLastName)
                       PROJECT(MXJ:CustAdd1)
                       PROJECT(MXJ:CustAdd2)
                       PROJECT(MXJ:CustAdd3)
                       PROJECT(MXJ:CustAdd4)
                       PROJECT(MXJ:CustPostCode)
                       PROJECT(MXJ:CustRef)
                       PROJECT(MXJ:CustNetwork)
                       PROJECT(MXJ:EnvLetter)
                       PROJECT(MXJ:InspType)
                       PROJECT(MXJ:EnvType)
                       PROJECT(MXJ:EnvCount)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
MXJ:RecordNo           LIKE(MXJ:RecordNo)             !List box control field - type derived from field
MXJ:Record_State       LIKE(MXJ:Record_State)         !List box control field - type derived from field
MXJ:ReportCode         LIKE(MXJ:ReportCode)           !List box control field - type derived from field
MXJ:Exception_Code     LIKE(MXJ:Exception_Code)       !List box control field - type derived from field
MXJ:Exception_Desc     LIKE(MXJ:Exception_Desc)       !List box control field - type derived from field
MXJ:ImportDate         LIKE(MXJ:ImportDate)           !List box control field - type derived from field
MXJ:ImportTime         LIKE(MXJ:ImportTime)           !List box control field - type derived from field
MXJ:ID                 LIKE(MXJ:ID)                   !List box control field - type derived from field
MXJ:InspACRJobNo       LIKE(MXJ:InspACRJobNo)         !List box control field - type derived from field
MXJ:EnvDateSent        LIKE(MXJ:EnvDateSent)          !List box control field - type derived from field
MXJ:InspACRStatus      LIKE(MXJ:InspACRStatus)        !List box control field - type derived from field
MXJ:InspDateRecd       LIKE(MXJ:InspDateRecd)         !List box control field - type derived from field
MXJ:InspModelID        LIKE(MXJ:InspModelID)          !List box control field - type derived from field
MXJ:InspFlag           LIKE(MXJ:InspFlag)             !List box control field - type derived from field
MXJ:InspAttention      LIKE(MXJ:InspAttention)        !List box control field - type derived from field
MXJ:InspQty            LIKE(MXJ:InspQty)              !List box control field - type derived from field
MXJ:InspPackageCond    LIKE(MXJ:InspPackageCond)      !List box control field - type derived from field
MXJ:InspQ1             LIKE(MXJ:InspQ1)               !List box control field - type derived from field
MXJ:InspQ2             LIKE(MXJ:InspQ2)               !List box control field - type derived from field
MXJ:InspQ3             LIKE(MXJ:InspQ3)               !List box control field - type derived from field
MXJ:InspACC1           LIKE(MXJ:InspACC1)             !List box control field - type derived from field
MXJ:InspACC2           LIKE(MXJ:InspACC2)             !List box control field - type derived from field
MXJ:InspACC3           LIKE(MXJ:InspACC3)             !List box control field - type derived from field
MXJ:InspACC4           LIKE(MXJ:InspACC4)             !List box control field - type derived from field
MXJ:InspACC5           LIKE(MXJ:InspACC5)             !List box control field - type derived from field
MXJ:InspGrade          LIKE(MXJ:InspGrade)            !List box control field - type derived from field
MXJ:InspIMEI           LIKE(MXJ:InspIMEI)             !List box control field - type derived from field
MXJ:InspNotes          LIKE(MXJ:InspNotes)            !List box control field - type derived from field
MXJ:InspDate           LIKE(MXJ:InspDate)             !List box control field - type derived from field
MXJ:InspBrand          LIKE(MXJ:InspBrand)            !List box control field - type derived from field
MXJ:InspInWarranty     LIKE(MXJ:InspInWarranty)       !List box control field - type derived from field
MXJ:DespatchConsignment LIKE(MXJ:DespatchConsignment) !List box control field - type derived from field
MXJ:DespatchDate       LIKE(MXJ:DespatchDate)         !List box control field - type derived from field
MXJ:CustCTN            LIKE(MXJ:CustCTN)              !List box control field - type derived from field
MXJ:QuoteValue         LIKE(MXJ:QuoteValue)           !List box control field - type derived from field
MXJ:ModelID            LIKE(MXJ:ModelID)              !List box control field - type derived from field
MXJ:QuoteQ1            LIKE(MXJ:QuoteQ1)              !List box control field - type derived from field
MXJ:QuoteQ2            LIKE(MXJ:QuoteQ2)              !List box control field - type derived from field
MXJ:QuoteQ3            LIKE(MXJ:QuoteQ3)              !List box control field - type derived from field
MXJ:QuoteACC1          LIKE(MXJ:QuoteACC1)            !List box control field - type derived from field
MXJ:QuoteACC2          LIKE(MXJ:QuoteACC2)            !List box control field - type derived from field
MXJ:QuoteACC3          LIKE(MXJ:QuoteACC3)            !List box control field - type derived from field
MXJ:QuoteACC4          LIKE(MXJ:QuoteACC4)            !List box control field - type derived from field
MXJ:QuoteACC5          LIKE(MXJ:QuoteACC5)            !List box control field - type derived from field
MXJ:CustTitle          LIKE(MXJ:CustTitle)            !List box control field - type derived from field
MXJ:CustFirstName      LIKE(MXJ:CustFirstName)        !List box control field - type derived from field
MXJ:CustLastName       LIKE(MXJ:CustLastName)         !List box control field - type derived from field
MXJ:CustAdd1           LIKE(MXJ:CustAdd1)             !List box control field - type derived from field
MXJ:CustAdd2           LIKE(MXJ:CustAdd2)             !List box control field - type derived from field
MXJ:CustAdd3           LIKE(MXJ:CustAdd3)             !List box control field - type derived from field
MXJ:CustAdd4           LIKE(MXJ:CustAdd4)             !List box control field - type derived from field
MXJ:CustPostCode       LIKE(MXJ:CustPostCode)         !List box control field - type derived from field
MXJ:CustRef            LIKE(MXJ:CustRef)              !List box control field - type derived from field
MXJ:CustNetwork        LIKE(MXJ:CustNetwork)          !List box control field - type derived from field
MXJ:EnvLetter          LIKE(MXJ:EnvLetter)            !List box control field - type derived from field
MXJ:InspType           LIKE(MXJ:InspType)             !List box control field - type derived from field
MXJ:EnvType            LIKE(MXJ:EnvType)              !List box control field - type derived from field
MXJ:EnvCount           LIKE(MXJ:EnvCount)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Browse
mo:SelectedField  Long
window               WINDOW('MPX Browse All Records'),AT(,,382,231),CENTER,SYSTEM,GRAY,MAX,RESIZE,IMM
                       BUTTON('Delete All'),AT(312,4,56,16),USE(?Button1)
                       LIST,AT(0,25),USE(?List),IMM,FULL,HVSCROLL,MSG('Browsing Records'),FORMAT('56R|M~Record No~L(2)@n-14@12R|M~Record State~L(2)@n3@56R|M~Report Code~L(2)@n-14' &|
   '@56R|M~Exception Code~L(2)@n-14@1020R|M~Exception Desc~L(2)@s255@40R|M~Import Da' &|
   'te~L(2)@d17@20R|M~Import Time~L(2)@t7@56R|M~ID~L(2)@n-14@32R|M~Job Number~L(2)@s' &|
   '8@40R|M~Env Date Sent~L(2)@d17@200R|M~Insp ACRS tatus~L(2)@s50@40R|M~Insp Date R' &|
   'ecd~L(2)@d17@200R|M~Insp Model ID~L(2)@s50@12R|M~Insp Flag~L(2)@n3@1020R|M~Insp ' &|
   'Attention~L(2)@s255@56R|M~Insp Qty~L(2)@n-14@200R|M~Insp Package Cond~L(2)@s50@1' &|
   '2R|M~Insp Q 1~L(2)@n3@12R|M~Insp Q 2~L(2)@n3@12R|M~Insp Q 3~L(2)@n3@12R|M~Insp A' &|
   'CC 1~L(2)@n3@12R|M~Insp ACC 2~L(2)@n3@12R|M~Insp ACC 3~L(2)@n3@12R|M~Insp ACC 4~' &|
   'L(2)@n3@12R|M~Insp ACC 5~L(2)@n3@200R|M~Insp Grade~L(2)@s50@60R|M~Insp IMEI~L(2)' &|
   '@s15@1020R|M~Insp Notes~L(2)@s255@40R|M~Insp Date~L(2)@d17@200R|M~Insp Brand~L(2' &|
   ')@s50@12R|M~Insp In Warranty~L(2)@n3@200R|M~Despatch Consignment~L(2)@s50@40R|M~' &|
   'Despatch Date~L(2)@d17@200R|M~Cust CTN~L(2)@s50@56R|M~Quote Value~L(2)@n-14@200R' &|
   '|M~Model ID~L(2)@s50@12R|M~Quote Q 1~L(2)@n3@12R|M~Quote Q 2~L(2)@n3@12R|M~Quote' &|
   ' Q 3~L(2)@n3@12R|M~Quote ACC 1~L(2)@n3@12R|M~Quote ACC 2~L(2)@n3@12R|M~Quote ACC' &|
   ' 3~L(2)@n3@12R|M~Quote ACC 4~L(2)@n3@12R|M~Quote ACC 5~L(2)@n3@200R|M~Cust Title' &|
   '~L(2)@s50@200R|M~Cust First Name~L(2)@s50@200R|M~Cust Last Name~L(2)@s50@200R|M~' &|
   'Cust Add 1~L(2)@s50@200R|M~Cust Add 2~L(2)@s50@200R|M~Cust Add 3~L(2)@s50@200R|M' &|
   '~Cust Add 4~L(2)@s50@200R|M~Cust Post Code~L(2)@s50@200R|M~Cust Ref~L(2)@s50@200' &|
   'R|M~Cust Network~L(2)@s50@200R|M~Env Letter~L(2)@s50@200R|M~Insp Type~L(2)@s50@2' &|
   '00R|M~Env Type~L(2)@s50@56R|M~Env Count~L(2)@n-14@'),FROM(Queue:Browse)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

BRW3                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

BRW3::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('MPXBrowseAllRecords')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Button1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='MPXBrowseAllRecords'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:MPXJOBS.Open
  SELF.FilesOpened = True
  BRW3.Init(?List,Queue:Browse.ViewPosition,BRW3::View:Browse,Queue:Browse,Relate:MPXJOBS,SELF)
  OPEN(window)
  SELF.Opened=True
  ?List{prop:vcr} = TRUE
  BRW3.Q &= Queue:Browse
  BRW3.AddSortOrder(,MXJ:RecordNoKey)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(,MXJ:RecordNo,1,BRW3)
  BRW3.AddField(MXJ:RecordNo,BRW3.Q.MXJ:RecordNo)
  BRW3.AddField(MXJ:Record_State,BRW3.Q.MXJ:Record_State)
  BRW3.AddField(MXJ:ReportCode,BRW3.Q.MXJ:ReportCode)
  BRW3.AddField(MXJ:Exception_Code,BRW3.Q.MXJ:Exception_Code)
  BRW3.AddField(MXJ:Exception_Desc,BRW3.Q.MXJ:Exception_Desc)
  BRW3.AddField(MXJ:ImportDate,BRW3.Q.MXJ:ImportDate)
  BRW3.AddField(MXJ:ImportTime,BRW3.Q.MXJ:ImportTime)
  BRW3.AddField(MXJ:ID,BRW3.Q.MXJ:ID)
  BRW3.AddField(MXJ:InspACRJobNo,BRW3.Q.MXJ:InspACRJobNo)
  BRW3.AddField(MXJ:EnvDateSent,BRW3.Q.MXJ:EnvDateSent)
  BRW3.AddField(MXJ:InspACRStatus,BRW3.Q.MXJ:InspACRStatus)
  BRW3.AddField(MXJ:InspDateRecd,BRW3.Q.MXJ:InspDateRecd)
  BRW3.AddField(MXJ:InspModelID,BRW3.Q.MXJ:InspModelID)
  BRW3.AddField(MXJ:InspFlag,BRW3.Q.MXJ:InspFlag)
  BRW3.AddField(MXJ:InspAttention,BRW3.Q.MXJ:InspAttention)
  BRW3.AddField(MXJ:InspQty,BRW3.Q.MXJ:InspQty)
  BRW3.AddField(MXJ:InspPackageCond,BRW3.Q.MXJ:InspPackageCond)
  BRW3.AddField(MXJ:InspQ1,BRW3.Q.MXJ:InspQ1)
  BRW3.AddField(MXJ:InspQ2,BRW3.Q.MXJ:InspQ2)
  BRW3.AddField(MXJ:InspQ3,BRW3.Q.MXJ:InspQ3)
  BRW3.AddField(MXJ:InspACC1,BRW3.Q.MXJ:InspACC1)
  BRW3.AddField(MXJ:InspACC2,BRW3.Q.MXJ:InspACC2)
  BRW3.AddField(MXJ:InspACC3,BRW3.Q.MXJ:InspACC3)
  BRW3.AddField(MXJ:InspACC4,BRW3.Q.MXJ:InspACC4)
  BRW3.AddField(MXJ:InspACC5,BRW3.Q.MXJ:InspACC5)
  BRW3.AddField(MXJ:InspGrade,BRW3.Q.MXJ:InspGrade)
  BRW3.AddField(MXJ:InspIMEI,BRW3.Q.MXJ:InspIMEI)
  BRW3.AddField(MXJ:InspNotes,BRW3.Q.MXJ:InspNotes)
  BRW3.AddField(MXJ:InspDate,BRW3.Q.MXJ:InspDate)
  BRW3.AddField(MXJ:InspBrand,BRW3.Q.MXJ:InspBrand)
  BRW3.AddField(MXJ:InspInWarranty,BRW3.Q.MXJ:InspInWarranty)
  BRW3.AddField(MXJ:DespatchConsignment,BRW3.Q.MXJ:DespatchConsignment)
  BRW3.AddField(MXJ:DespatchDate,BRW3.Q.MXJ:DespatchDate)
  BRW3.AddField(MXJ:CustCTN,BRW3.Q.MXJ:CustCTN)
  BRW3.AddField(MXJ:QuoteValue,BRW3.Q.MXJ:QuoteValue)
  BRW3.AddField(MXJ:ModelID,BRW3.Q.MXJ:ModelID)
  BRW3.AddField(MXJ:QuoteQ1,BRW3.Q.MXJ:QuoteQ1)
  BRW3.AddField(MXJ:QuoteQ2,BRW3.Q.MXJ:QuoteQ2)
  BRW3.AddField(MXJ:QuoteQ3,BRW3.Q.MXJ:QuoteQ3)
  BRW3.AddField(MXJ:QuoteACC1,BRW3.Q.MXJ:QuoteACC1)
  BRW3.AddField(MXJ:QuoteACC2,BRW3.Q.MXJ:QuoteACC2)
  BRW3.AddField(MXJ:QuoteACC3,BRW3.Q.MXJ:QuoteACC3)
  BRW3.AddField(MXJ:QuoteACC4,BRW3.Q.MXJ:QuoteACC4)
  BRW3.AddField(MXJ:QuoteACC5,BRW3.Q.MXJ:QuoteACC5)
  BRW3.AddField(MXJ:CustTitle,BRW3.Q.MXJ:CustTitle)
  BRW3.AddField(MXJ:CustFirstName,BRW3.Q.MXJ:CustFirstName)
  BRW3.AddField(MXJ:CustLastName,BRW3.Q.MXJ:CustLastName)
  BRW3.AddField(MXJ:CustAdd1,BRW3.Q.MXJ:CustAdd1)
  BRW3.AddField(MXJ:CustAdd2,BRW3.Q.MXJ:CustAdd2)
  BRW3.AddField(MXJ:CustAdd3,BRW3.Q.MXJ:CustAdd3)
  BRW3.AddField(MXJ:CustAdd4,BRW3.Q.MXJ:CustAdd4)
  BRW3.AddField(MXJ:CustPostCode,BRW3.Q.MXJ:CustPostCode)
  BRW3.AddField(MXJ:CustRef,BRW3.Q.MXJ:CustRef)
  BRW3.AddField(MXJ:CustNetwork,BRW3.Q.MXJ:CustNetwork)
  BRW3.AddField(MXJ:EnvLetter,BRW3.Q.MXJ:EnvLetter)
  BRW3.AddField(MXJ:InspType,BRW3.Q.MXJ:InspType)
  BRW3.AddField(MXJ:EnvType,BRW3.Q.MXJ:EnvType)
  BRW3.AddField(MXJ:EnvCount,BRW3.Q.MXJ:EnvCount)
  ThisMakeover.SetWindow(Win:Browse)
  window{prop:buffer} = 1
  BRW3.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
    ThisMakeover.Refresh()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MPXJOBS.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='MPXBrowseAllRecords'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button1
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button1, Accepted)
      access:mpxjobs.clearkey(mxj:RecordNoKey)
      set(mxj:RecordNoKey, mxj:RecordNoKey)
      loop while access:mpxjobs.next() = level:benign
          access:mpxjobs.deleterecord(false)
      end
      brw3.ResetQueue(reset:queue)
      brw3.PostNewSelection()
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button1, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisMakeover.TakeEvent(Win:Browse,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:GainFocus
      IF window{PROP:Iconize}=TRUE
        window{PROP:Iconize}=FALSE
        IF window{PROP:Active}<>TRUE
           window{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW3.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end

