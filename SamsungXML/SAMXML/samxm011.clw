

   MEMBER('samxml.clw')                               ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SAMXM011.INC'),ONCE        !Local module procedure declarations
                     END


UpdateException PROCEDURE                             !Generated from procedure template - Window

ActionMessage        CSTRING(40)
CopyPostcode         LONG
CopyModel            LONG
CopyMSN              LONG
CopyMissingFields    LONG
CopyExport           LONG
CopyDuplicate        LONG
FailPostcode         LONG
FailModel            LONG
FailMSN              LONG
FailMissingFields    LONG
FailExport           LONG
FailDuplicate        LONG
History::XJB:Record  LIKE(XJB:RECORD),STATIC
FormWindow           WINDOW('Update Samsung Exception'),AT(,,455,222),FONT('Arial',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,448,188),USE(?Sheet1),SPREAD
                         TAB('Update Exception'),USE(?Tab1)
                           PROMPT('Consumer First Name'),AT(232,20),USE(?Prompt22)
                           ENTRY(@s40),AT(320,20,124,10),USE(XJB:CONSUMER_FIRST_NAME),UPR
                           PROMPT('Company'),AT(12,20),USE(?Prompt1)
                           ENTRY(@s4),AT(96,20,124,10),USE(XJB:COMPANY),UPR
                           PROMPT('ASC Code'),AT(12,32),USE(?Prompt3)
                           PROMPT('Model Code'),AT(12,44),USE(?Prompt4)
                           ENTRY(@s30),AT(96,44,124,10),USE(XJB:MODEL_CODE),UPR
                           ENTRY(@s30),AT(96,32,124,10),USE(XJB:ASC_CODE),UPR
                           PROMPT('TR Status'),AT(232,140),USE(?Prompt13)
                           PROMPT('Service Type'),AT(12,80),USE(?Prompt7)
                           PROMPT('Serial No'),AT(12,56),USE(?Prompt5)
                           PROMPT('Consumer Postcode'),AT(232,116),USE(?Prompt16)
                           ENTRY(@s10),AT(320,116,124,10),USE(XJB:CONSUMER_POSTCODE),UPR
                           PROMPT('Consumer Last Name'),AT(232,32),USE(?Prompt14)
                           PROMPT('Request Date'),AT(12,104),USE(?Prompt9)
                           ENTRY(@d6b),AT(96,104,124,10),USE(XJB:REQUEST_DATE),UPR
                           ENTRY(@s30),AT(320,32,124,10),USE(XJB:CONSUMER_LAST_NAME),UPR
                           ENTRY(@d17),AT(96,128,124,10),USE(XJB:REPAIR_ETD_DATE),UPR
                           ENTRY(@s241),AT(320,128,124,10),USE(XJB:CONSUMER_EMAIL),UPR
                           PROMPT('Consumer Email'),AT(232,128),USE(?Prompt23)
                           ENTRY(@s2),AT(96,80,124,10),USE(XJB:SERVICE_TYPE),UPR
                           PROMPT('Consumer City'),AT(232,104),USE(?Prompt21)
                           ENTRY(@s20),AT(320,92,124,10),USE(XJB:CONSUMER_REGION),UPR
                           PROMPT('Consumer Region'),AT(232,92),USE(?Prompt20)
                           PROMPT('Consumer House No'),AT(232,56),USE(?Prompt17)
                           ENTRY(@s15),AT(320,44,124,10),USE(XJB:CONSUMER_TEL_NUMBER1),UPR
                           ENTRY(@t7),AT(96,140,124,10),USE(XJB:REPAIR_ETD_TIME),UPR
                           PROMPT('Estimated Date Repair'),AT(12,128),USE(?Prompt11)
                           PROMPT('Request Time'),AT(12,116),USE(?Prompt10)
                           ENTRY(@t1),AT(96,116,124,10),USE(XJB:REQUEST_TIME),UPR
                           PROMPT('Estimated Time Repair'),AT(12,140),USE(?Prompt12)
                           PROMPT('Purchase Date'),AT(12,68),USE(?Prompt6)
                           ENTRY(@s20),AT(96,56,124,10),USE(XJB:SERIAL_NO),UPR
                           PROMPT('Consumer Telephone No'),AT(232,44),USE(?Prompt15)
                           PROMPT('In/Out Warranty'),AT(12,92),USE(?Prompt8)
                           ENTRY(@s1),AT(96,92,124,10),USE(XJB:IN_OUT_WARRANTY),UPR
                           ENTRY(@s20),AT(320,104,124,10),USE(XJB:CONSUMER_COUNTRY),UPR
                           PROMPT('Consumer Street'),AT(232,68),USE(?Prompt18)
                           ENTRY(@s2),AT(320,140,124,10),USE(XJB:TR_STATUS),UPR
                           PROMPT('Symptom Desc 1'),AT(12,152),USE(?Prompt26)
                           ENTRY(@s40),AT(96,152,124,10),USE(XJB:SYMPTOM1_DESC),UPR
                           ENTRY(@s40),AT(96,164,124,10),USE(XJB:SYMPTOM2_DESC),UPR
                           PROMPT('Inquiry Text'),AT(232,152),USE(?Prompt29)
                           PROMPT('Symptom Desc 2'),AT(12,164),USE(?Prompt27)
                           ENTRY(@s40),AT(96,176,124,10),USE(XJB:SYMPTOM3_DESC),UPR
                           ENTRY(@s254),AT(320,152,124,10),USE(XJB:INQUIRY_TEXT),UPR
                           PROMPT('Symptom Desc 3'),AT(12,176),USE(?Prompt28)
                           ENTRY(@d6),AT(96,68,124,10),USE(XJB:PURCHASE_DATE),UPR
                           ENTRY(@s40),AT(320,80,124,10),USE(XJB:CONSUMER_CITY),UPR
                           PROMPT('Consumer City'),AT(232,80),USE(?Prompt19)
                           ENTRY(@s60),AT(320,68,124,10),USE(XJB:CONSUMER_STREET),UPR
                           ENTRY(@s20),AT(320,56,124,10),USE(XJB:CONSUMER_HOUSE_NUMBER),UPR
                         END
                         TAB('Exception Codes'),USE(?Tab2)
                           TEXT,AT(120,28,324,160),USE(XJB:EXCEPTION_DESC),VSCROLL,UPR,READONLY
                           CHECK('Postcode'),AT(20,40),USE(FailPostcode),TRN,VALUE('1','0')
                           CHECK('Model No.'),AT(20,52),USE(FailModel),TRN,VALUE('1','0')
                           CHECK('MSN'),AT(20,64),USE(FailMSN),TRN,VALUE('1','0')
                           CHECK('Missing Fields'),AT(20,76),USE(FailMissingFields),TRN,VALUE('1','0')
                           CHECK('Export Failure'),AT(20,88),USE(FailExport),TRN,VALUE('1','0')
                           GROUP('Errors'),AT(8,24,104,96),USE(?Group1),BOXED
                             CHECK('Duplicate'),AT(20,100),USE(FailDuplicate),TRN,VALUE('1','0')
                           END
                           PROMPT('Description'),AT(120,20),USE(?Prompt24),TRN
                         END
                       END
                       PANEL,AT(4,196,452,24),USE(?Panel1),FILL(COLOR:Silver)
                       PROMPT('TR No.'),AT(12,200),USE(?Prompt25),TRN
                       ENTRY(@s30),AT(40,200,124,10),USE(XJB:TR_NO),COLOR(COLOR:Silver),READONLY
                       BUTTON('OK'),AT(340,200,56,16),USE(?OK),TRN,LEFT,ICON('ok.gif'),DEFAULT,REQ
                       BUTTON('Cancel'),AT(396,200,56,16),USE(?Cancel),TRN,LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt22{prop:FontColor} = -1
    ?Prompt22{prop:Color} = 15066597
    If ?XJB:CONSUMER_FIRST_NAME{prop:ReadOnly} = True
        ?XJB:CONSUMER_FIRST_NAME{prop:FontColor} = 65793
        ?XJB:CONSUMER_FIRST_NAME{prop:Color} = 15066597
    Elsif ?XJB:CONSUMER_FIRST_NAME{prop:Req} = True
        ?XJB:CONSUMER_FIRST_NAME{prop:FontColor} = 65793
        ?XJB:CONSUMER_FIRST_NAME{prop:Color} = 8454143
    Else ! If ?XJB:CONSUMER_FIRST_NAME{prop:Req} = True
        ?XJB:CONSUMER_FIRST_NAME{prop:FontColor} = 65793
        ?XJB:CONSUMER_FIRST_NAME{prop:Color} = 16777215
    End ! If ?XJB:CONSUMER_FIRST_NAME{prop:Req} = True
    ?XJB:CONSUMER_FIRST_NAME{prop:Trn} = 0
    ?XJB:CONSUMER_FIRST_NAME{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?XJB:COMPANY{prop:ReadOnly} = True
        ?XJB:COMPANY{prop:FontColor} = 65793
        ?XJB:COMPANY{prop:Color} = 15066597
    Elsif ?XJB:COMPANY{prop:Req} = True
        ?XJB:COMPANY{prop:FontColor} = 65793
        ?XJB:COMPANY{prop:Color} = 8454143
    Else ! If ?XJB:COMPANY{prop:Req} = True
        ?XJB:COMPANY{prop:FontColor} = 65793
        ?XJB:COMPANY{prop:Color} = 16777215
    End ! If ?XJB:COMPANY{prop:Req} = True
    ?XJB:COMPANY{prop:Trn} = 0
    ?XJB:COMPANY{prop:FontStyle} = font:Bold
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    If ?XJB:MODEL_CODE{prop:ReadOnly} = True
        ?XJB:MODEL_CODE{prop:FontColor} = 65793
        ?XJB:MODEL_CODE{prop:Color} = 15066597
    Elsif ?XJB:MODEL_CODE{prop:Req} = True
        ?XJB:MODEL_CODE{prop:FontColor} = 65793
        ?XJB:MODEL_CODE{prop:Color} = 8454143
    Else ! If ?XJB:MODEL_CODE{prop:Req} = True
        ?XJB:MODEL_CODE{prop:FontColor} = 65793
        ?XJB:MODEL_CODE{prop:Color} = 16777215
    End ! If ?XJB:MODEL_CODE{prop:Req} = True
    ?XJB:MODEL_CODE{prop:Trn} = 0
    ?XJB:MODEL_CODE{prop:FontStyle} = font:Bold
    If ?XJB:ASC_CODE{prop:ReadOnly} = True
        ?XJB:ASC_CODE{prop:FontColor} = 65793
        ?XJB:ASC_CODE{prop:Color} = 15066597
    Elsif ?XJB:ASC_CODE{prop:Req} = True
        ?XJB:ASC_CODE{prop:FontColor} = 65793
        ?XJB:ASC_CODE{prop:Color} = 8454143
    Else ! If ?XJB:ASC_CODE{prop:Req} = True
        ?XJB:ASC_CODE{prop:FontColor} = 65793
        ?XJB:ASC_CODE{prop:Color} = 16777215
    End ! If ?XJB:ASC_CODE{prop:Req} = True
    ?XJB:ASC_CODE{prop:Trn} = 0
    ?XJB:ASC_CODE{prop:FontStyle} = font:Bold
    ?Prompt13{prop:FontColor} = -1
    ?Prompt13{prop:Color} = 15066597
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?Prompt16{prop:FontColor} = -1
    ?Prompt16{prop:Color} = 15066597
    If ?XJB:CONSUMER_POSTCODE{prop:ReadOnly} = True
        ?XJB:CONSUMER_POSTCODE{prop:FontColor} = 65793
        ?XJB:CONSUMER_POSTCODE{prop:Color} = 15066597
    Elsif ?XJB:CONSUMER_POSTCODE{prop:Req} = True
        ?XJB:CONSUMER_POSTCODE{prop:FontColor} = 65793
        ?XJB:CONSUMER_POSTCODE{prop:Color} = 8454143
    Else ! If ?XJB:CONSUMER_POSTCODE{prop:Req} = True
        ?XJB:CONSUMER_POSTCODE{prop:FontColor} = 65793
        ?XJB:CONSUMER_POSTCODE{prop:Color} = 16777215
    End ! If ?XJB:CONSUMER_POSTCODE{prop:Req} = True
    ?XJB:CONSUMER_POSTCODE{prop:Trn} = 0
    ?XJB:CONSUMER_POSTCODE{prop:FontStyle} = font:Bold
    ?Prompt14{prop:FontColor} = -1
    ?Prompt14{prop:Color} = 15066597
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    If ?XJB:REQUEST_DATE{prop:ReadOnly} = True
        ?XJB:REQUEST_DATE{prop:FontColor} = 65793
        ?XJB:REQUEST_DATE{prop:Color} = 15066597
    Elsif ?XJB:REQUEST_DATE{prop:Req} = True
        ?XJB:REQUEST_DATE{prop:FontColor} = 65793
        ?XJB:REQUEST_DATE{prop:Color} = 8454143
    Else ! If ?XJB:REQUEST_DATE{prop:Req} = True
        ?XJB:REQUEST_DATE{prop:FontColor} = 65793
        ?XJB:REQUEST_DATE{prop:Color} = 16777215
    End ! If ?XJB:REQUEST_DATE{prop:Req} = True
    ?XJB:REQUEST_DATE{prop:Trn} = 0
    ?XJB:REQUEST_DATE{prop:FontStyle} = font:Bold
    If ?XJB:CONSUMER_LAST_NAME{prop:ReadOnly} = True
        ?XJB:CONSUMER_LAST_NAME{prop:FontColor} = 65793
        ?XJB:CONSUMER_LAST_NAME{prop:Color} = 15066597
    Elsif ?XJB:CONSUMER_LAST_NAME{prop:Req} = True
        ?XJB:CONSUMER_LAST_NAME{prop:FontColor} = 65793
        ?XJB:CONSUMER_LAST_NAME{prop:Color} = 8454143
    Else ! If ?XJB:CONSUMER_LAST_NAME{prop:Req} = True
        ?XJB:CONSUMER_LAST_NAME{prop:FontColor} = 65793
        ?XJB:CONSUMER_LAST_NAME{prop:Color} = 16777215
    End ! If ?XJB:CONSUMER_LAST_NAME{prop:Req} = True
    ?XJB:CONSUMER_LAST_NAME{prop:Trn} = 0
    ?XJB:CONSUMER_LAST_NAME{prop:FontStyle} = font:Bold
    If ?XJB:REPAIR_ETD_DATE{prop:ReadOnly} = True
        ?XJB:REPAIR_ETD_DATE{prop:FontColor} = 65793
        ?XJB:REPAIR_ETD_DATE{prop:Color} = 15066597
    Elsif ?XJB:REPAIR_ETD_DATE{prop:Req} = True
        ?XJB:REPAIR_ETD_DATE{prop:FontColor} = 65793
        ?XJB:REPAIR_ETD_DATE{prop:Color} = 8454143
    Else ! If ?XJB:REPAIR_ETD_DATE{prop:Req} = True
        ?XJB:REPAIR_ETD_DATE{prop:FontColor} = 65793
        ?XJB:REPAIR_ETD_DATE{prop:Color} = 16777215
    End ! If ?XJB:REPAIR_ETD_DATE{prop:Req} = True
    ?XJB:REPAIR_ETD_DATE{prop:Trn} = 0
    ?XJB:REPAIR_ETD_DATE{prop:FontStyle} = font:Bold
    If ?XJB:CONSUMER_EMAIL{prop:ReadOnly} = True
        ?XJB:CONSUMER_EMAIL{prop:FontColor} = 65793
        ?XJB:CONSUMER_EMAIL{prop:Color} = 15066597
    Elsif ?XJB:CONSUMER_EMAIL{prop:Req} = True
        ?XJB:CONSUMER_EMAIL{prop:FontColor} = 65793
        ?XJB:CONSUMER_EMAIL{prop:Color} = 8454143
    Else ! If ?XJB:CONSUMER_EMAIL{prop:Req} = True
        ?XJB:CONSUMER_EMAIL{prop:FontColor} = 65793
        ?XJB:CONSUMER_EMAIL{prop:Color} = 16777215
    End ! If ?XJB:CONSUMER_EMAIL{prop:Req} = True
    ?XJB:CONSUMER_EMAIL{prop:Trn} = 0
    ?XJB:CONSUMER_EMAIL{prop:FontStyle} = font:Bold
    ?Prompt23{prop:FontColor} = -1
    ?Prompt23{prop:Color} = 15066597
    If ?XJB:SERVICE_TYPE{prop:ReadOnly} = True
        ?XJB:SERVICE_TYPE{prop:FontColor} = 65793
        ?XJB:SERVICE_TYPE{prop:Color} = 15066597
    Elsif ?XJB:SERVICE_TYPE{prop:Req} = True
        ?XJB:SERVICE_TYPE{prop:FontColor} = 65793
        ?XJB:SERVICE_TYPE{prop:Color} = 8454143
    Else ! If ?XJB:SERVICE_TYPE{prop:Req} = True
        ?XJB:SERVICE_TYPE{prop:FontColor} = 65793
        ?XJB:SERVICE_TYPE{prop:Color} = 16777215
    End ! If ?XJB:SERVICE_TYPE{prop:Req} = True
    ?XJB:SERVICE_TYPE{prop:Trn} = 0
    ?XJB:SERVICE_TYPE{prop:FontStyle} = font:Bold
    ?Prompt21{prop:FontColor} = -1
    ?Prompt21{prop:Color} = 15066597
    If ?XJB:CONSUMER_REGION{prop:ReadOnly} = True
        ?XJB:CONSUMER_REGION{prop:FontColor} = 65793
        ?XJB:CONSUMER_REGION{prop:Color} = 15066597
    Elsif ?XJB:CONSUMER_REGION{prop:Req} = True
        ?XJB:CONSUMER_REGION{prop:FontColor} = 65793
        ?XJB:CONSUMER_REGION{prop:Color} = 8454143
    Else ! If ?XJB:CONSUMER_REGION{prop:Req} = True
        ?XJB:CONSUMER_REGION{prop:FontColor} = 65793
        ?XJB:CONSUMER_REGION{prop:Color} = 16777215
    End ! If ?XJB:CONSUMER_REGION{prop:Req} = True
    ?XJB:CONSUMER_REGION{prop:Trn} = 0
    ?XJB:CONSUMER_REGION{prop:FontStyle} = font:Bold
    ?Prompt20{prop:FontColor} = -1
    ?Prompt20{prop:Color} = 15066597
    ?Prompt17{prop:FontColor} = -1
    ?Prompt17{prop:Color} = 15066597
    If ?XJB:CONSUMER_TEL_NUMBER1{prop:ReadOnly} = True
        ?XJB:CONSUMER_TEL_NUMBER1{prop:FontColor} = 65793
        ?XJB:CONSUMER_TEL_NUMBER1{prop:Color} = 15066597
    Elsif ?XJB:CONSUMER_TEL_NUMBER1{prop:Req} = True
        ?XJB:CONSUMER_TEL_NUMBER1{prop:FontColor} = 65793
        ?XJB:CONSUMER_TEL_NUMBER1{prop:Color} = 8454143
    Else ! If ?XJB:CONSUMER_TEL_NUMBER1{prop:Req} = True
        ?XJB:CONSUMER_TEL_NUMBER1{prop:FontColor} = 65793
        ?XJB:CONSUMER_TEL_NUMBER1{prop:Color} = 16777215
    End ! If ?XJB:CONSUMER_TEL_NUMBER1{prop:Req} = True
    ?XJB:CONSUMER_TEL_NUMBER1{prop:Trn} = 0
    ?XJB:CONSUMER_TEL_NUMBER1{prop:FontStyle} = font:Bold
    If ?XJB:REPAIR_ETD_TIME{prop:ReadOnly} = True
        ?XJB:REPAIR_ETD_TIME{prop:FontColor} = 65793
        ?XJB:REPAIR_ETD_TIME{prop:Color} = 15066597
    Elsif ?XJB:REPAIR_ETD_TIME{prop:Req} = True
        ?XJB:REPAIR_ETD_TIME{prop:FontColor} = 65793
        ?XJB:REPAIR_ETD_TIME{prop:Color} = 8454143
    Else ! If ?XJB:REPAIR_ETD_TIME{prop:Req} = True
        ?XJB:REPAIR_ETD_TIME{prop:FontColor} = 65793
        ?XJB:REPAIR_ETD_TIME{prop:Color} = 16777215
    End ! If ?XJB:REPAIR_ETD_TIME{prop:Req} = True
    ?XJB:REPAIR_ETD_TIME{prop:Trn} = 0
    ?XJB:REPAIR_ETD_TIME{prop:FontStyle} = font:Bold
    ?Prompt11{prop:FontColor} = -1
    ?Prompt11{prop:Color} = 15066597
    ?Prompt10{prop:FontColor} = -1
    ?Prompt10{prop:Color} = 15066597
    If ?XJB:REQUEST_TIME{prop:ReadOnly} = True
        ?XJB:REQUEST_TIME{prop:FontColor} = 65793
        ?XJB:REQUEST_TIME{prop:Color} = 15066597
    Elsif ?XJB:REQUEST_TIME{prop:Req} = True
        ?XJB:REQUEST_TIME{prop:FontColor} = 65793
        ?XJB:REQUEST_TIME{prop:Color} = 8454143
    Else ! If ?XJB:REQUEST_TIME{prop:Req} = True
        ?XJB:REQUEST_TIME{prop:FontColor} = 65793
        ?XJB:REQUEST_TIME{prop:Color} = 16777215
    End ! If ?XJB:REQUEST_TIME{prop:Req} = True
    ?XJB:REQUEST_TIME{prop:Trn} = 0
    ?XJB:REQUEST_TIME{prop:FontStyle} = font:Bold
    ?Prompt12{prop:FontColor} = -1
    ?Prompt12{prop:Color} = 15066597
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    If ?XJB:SERIAL_NO{prop:ReadOnly} = True
        ?XJB:SERIAL_NO{prop:FontColor} = 65793
        ?XJB:SERIAL_NO{prop:Color} = 15066597
    Elsif ?XJB:SERIAL_NO{prop:Req} = True
        ?XJB:SERIAL_NO{prop:FontColor} = 65793
        ?XJB:SERIAL_NO{prop:Color} = 8454143
    Else ! If ?XJB:SERIAL_NO{prop:Req} = True
        ?XJB:SERIAL_NO{prop:FontColor} = 65793
        ?XJB:SERIAL_NO{prop:Color} = 16777215
    End ! If ?XJB:SERIAL_NO{prop:Req} = True
    ?XJB:SERIAL_NO{prop:Trn} = 0
    ?XJB:SERIAL_NO{prop:FontStyle} = font:Bold
    ?Prompt15{prop:FontColor} = -1
    ?Prompt15{prop:Color} = 15066597
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    If ?XJB:IN_OUT_WARRANTY{prop:ReadOnly} = True
        ?XJB:IN_OUT_WARRANTY{prop:FontColor} = 65793
        ?XJB:IN_OUT_WARRANTY{prop:Color} = 15066597
    Elsif ?XJB:IN_OUT_WARRANTY{prop:Req} = True
        ?XJB:IN_OUT_WARRANTY{prop:FontColor} = 65793
        ?XJB:IN_OUT_WARRANTY{prop:Color} = 8454143
    Else ! If ?XJB:IN_OUT_WARRANTY{prop:Req} = True
        ?XJB:IN_OUT_WARRANTY{prop:FontColor} = 65793
        ?XJB:IN_OUT_WARRANTY{prop:Color} = 16777215
    End ! If ?XJB:IN_OUT_WARRANTY{prop:Req} = True
    ?XJB:IN_OUT_WARRANTY{prop:Trn} = 0
    ?XJB:IN_OUT_WARRANTY{prop:FontStyle} = font:Bold
    If ?XJB:CONSUMER_COUNTRY{prop:ReadOnly} = True
        ?XJB:CONSUMER_COUNTRY{prop:FontColor} = 65793
        ?XJB:CONSUMER_COUNTRY{prop:Color} = 15066597
    Elsif ?XJB:CONSUMER_COUNTRY{prop:Req} = True
        ?XJB:CONSUMER_COUNTRY{prop:FontColor} = 65793
        ?XJB:CONSUMER_COUNTRY{prop:Color} = 8454143
    Else ! If ?XJB:CONSUMER_COUNTRY{prop:Req} = True
        ?XJB:CONSUMER_COUNTRY{prop:FontColor} = 65793
        ?XJB:CONSUMER_COUNTRY{prop:Color} = 16777215
    End ! If ?XJB:CONSUMER_COUNTRY{prop:Req} = True
    ?XJB:CONSUMER_COUNTRY{prop:Trn} = 0
    ?XJB:CONSUMER_COUNTRY{prop:FontStyle} = font:Bold
    ?Prompt18{prop:FontColor} = -1
    ?Prompt18{prop:Color} = 15066597
    If ?XJB:TR_STATUS{prop:ReadOnly} = True
        ?XJB:TR_STATUS{prop:FontColor} = 65793
        ?XJB:TR_STATUS{prop:Color} = 15066597
    Elsif ?XJB:TR_STATUS{prop:Req} = True
        ?XJB:TR_STATUS{prop:FontColor} = 65793
        ?XJB:TR_STATUS{prop:Color} = 8454143
    Else ! If ?XJB:TR_STATUS{prop:Req} = True
        ?XJB:TR_STATUS{prop:FontColor} = 65793
        ?XJB:TR_STATUS{prop:Color} = 16777215
    End ! If ?XJB:TR_STATUS{prop:Req} = True
    ?XJB:TR_STATUS{prop:Trn} = 0
    ?XJB:TR_STATUS{prop:FontStyle} = font:Bold
    ?Prompt26{prop:FontColor} = -1
    ?Prompt26{prop:Color} = 15066597
    If ?XJB:SYMPTOM1_DESC{prop:ReadOnly} = True
        ?XJB:SYMPTOM1_DESC{prop:FontColor} = 65793
        ?XJB:SYMPTOM1_DESC{prop:Color} = 15066597
    Elsif ?XJB:SYMPTOM1_DESC{prop:Req} = True
        ?XJB:SYMPTOM1_DESC{prop:FontColor} = 65793
        ?XJB:SYMPTOM1_DESC{prop:Color} = 8454143
    Else ! If ?XJB:SYMPTOM1_DESC{prop:Req} = True
        ?XJB:SYMPTOM1_DESC{prop:FontColor} = 65793
        ?XJB:SYMPTOM1_DESC{prop:Color} = 16777215
    End ! If ?XJB:SYMPTOM1_DESC{prop:Req} = True
    ?XJB:SYMPTOM1_DESC{prop:Trn} = 0
    ?XJB:SYMPTOM1_DESC{prop:FontStyle} = font:Bold
    If ?XJB:SYMPTOM2_DESC{prop:ReadOnly} = True
        ?XJB:SYMPTOM2_DESC{prop:FontColor} = 65793
        ?XJB:SYMPTOM2_DESC{prop:Color} = 15066597
    Elsif ?XJB:SYMPTOM2_DESC{prop:Req} = True
        ?XJB:SYMPTOM2_DESC{prop:FontColor} = 65793
        ?XJB:SYMPTOM2_DESC{prop:Color} = 8454143
    Else ! If ?XJB:SYMPTOM2_DESC{prop:Req} = True
        ?XJB:SYMPTOM2_DESC{prop:FontColor} = 65793
        ?XJB:SYMPTOM2_DESC{prop:Color} = 16777215
    End ! If ?XJB:SYMPTOM2_DESC{prop:Req} = True
    ?XJB:SYMPTOM2_DESC{prop:Trn} = 0
    ?XJB:SYMPTOM2_DESC{prop:FontStyle} = font:Bold
    ?Prompt29{prop:FontColor} = -1
    ?Prompt29{prop:Color} = 15066597
    ?Prompt27{prop:FontColor} = -1
    ?Prompt27{prop:Color} = 15066597
    If ?XJB:SYMPTOM3_DESC{prop:ReadOnly} = True
        ?XJB:SYMPTOM3_DESC{prop:FontColor} = 65793
        ?XJB:SYMPTOM3_DESC{prop:Color} = 15066597
    Elsif ?XJB:SYMPTOM3_DESC{prop:Req} = True
        ?XJB:SYMPTOM3_DESC{prop:FontColor} = 65793
        ?XJB:SYMPTOM3_DESC{prop:Color} = 8454143
    Else ! If ?XJB:SYMPTOM3_DESC{prop:Req} = True
        ?XJB:SYMPTOM3_DESC{prop:FontColor} = 65793
        ?XJB:SYMPTOM3_DESC{prop:Color} = 16777215
    End ! If ?XJB:SYMPTOM3_DESC{prop:Req} = True
    ?XJB:SYMPTOM3_DESC{prop:Trn} = 0
    ?XJB:SYMPTOM3_DESC{prop:FontStyle} = font:Bold
    If ?XJB:INQUIRY_TEXT{prop:ReadOnly} = True
        ?XJB:INQUIRY_TEXT{prop:FontColor} = 65793
        ?XJB:INQUIRY_TEXT{prop:Color} = 15066597
    Elsif ?XJB:INQUIRY_TEXT{prop:Req} = True
        ?XJB:INQUIRY_TEXT{prop:FontColor} = 65793
        ?XJB:INQUIRY_TEXT{prop:Color} = 8454143
    Else ! If ?XJB:INQUIRY_TEXT{prop:Req} = True
        ?XJB:INQUIRY_TEXT{prop:FontColor} = 65793
        ?XJB:INQUIRY_TEXT{prop:Color} = 16777215
    End ! If ?XJB:INQUIRY_TEXT{prop:Req} = True
    ?XJB:INQUIRY_TEXT{prop:Trn} = 0
    ?XJB:INQUIRY_TEXT{prop:FontStyle} = font:Bold
    ?Prompt28{prop:FontColor} = -1
    ?Prompt28{prop:Color} = 15066597
    If ?XJB:PURCHASE_DATE{prop:ReadOnly} = True
        ?XJB:PURCHASE_DATE{prop:FontColor} = 65793
        ?XJB:PURCHASE_DATE{prop:Color} = 15066597
    Elsif ?XJB:PURCHASE_DATE{prop:Req} = True
        ?XJB:PURCHASE_DATE{prop:FontColor} = 65793
        ?XJB:PURCHASE_DATE{prop:Color} = 8454143
    Else ! If ?XJB:PURCHASE_DATE{prop:Req} = True
        ?XJB:PURCHASE_DATE{prop:FontColor} = 65793
        ?XJB:PURCHASE_DATE{prop:Color} = 16777215
    End ! If ?XJB:PURCHASE_DATE{prop:Req} = True
    ?XJB:PURCHASE_DATE{prop:Trn} = 0
    ?XJB:PURCHASE_DATE{prop:FontStyle} = font:Bold
    If ?XJB:CONSUMER_CITY{prop:ReadOnly} = True
        ?XJB:CONSUMER_CITY{prop:FontColor} = 65793
        ?XJB:CONSUMER_CITY{prop:Color} = 15066597
    Elsif ?XJB:CONSUMER_CITY{prop:Req} = True
        ?XJB:CONSUMER_CITY{prop:FontColor} = 65793
        ?XJB:CONSUMER_CITY{prop:Color} = 8454143
    Else ! If ?XJB:CONSUMER_CITY{prop:Req} = True
        ?XJB:CONSUMER_CITY{prop:FontColor} = 65793
        ?XJB:CONSUMER_CITY{prop:Color} = 16777215
    End ! If ?XJB:CONSUMER_CITY{prop:Req} = True
    ?XJB:CONSUMER_CITY{prop:Trn} = 0
    ?XJB:CONSUMER_CITY{prop:FontStyle} = font:Bold
    ?Prompt19{prop:FontColor} = -1
    ?Prompt19{prop:Color} = 15066597
    If ?XJB:CONSUMER_STREET{prop:ReadOnly} = True
        ?XJB:CONSUMER_STREET{prop:FontColor} = 65793
        ?XJB:CONSUMER_STREET{prop:Color} = 15066597
    Elsif ?XJB:CONSUMER_STREET{prop:Req} = True
        ?XJB:CONSUMER_STREET{prop:FontColor} = 65793
        ?XJB:CONSUMER_STREET{prop:Color} = 8454143
    Else ! If ?XJB:CONSUMER_STREET{prop:Req} = True
        ?XJB:CONSUMER_STREET{prop:FontColor} = 65793
        ?XJB:CONSUMER_STREET{prop:Color} = 16777215
    End ! If ?XJB:CONSUMER_STREET{prop:Req} = True
    ?XJB:CONSUMER_STREET{prop:Trn} = 0
    ?XJB:CONSUMER_STREET{prop:FontStyle} = font:Bold
    If ?XJB:CONSUMER_HOUSE_NUMBER{prop:ReadOnly} = True
        ?XJB:CONSUMER_HOUSE_NUMBER{prop:FontColor} = 65793
        ?XJB:CONSUMER_HOUSE_NUMBER{prop:Color} = 15066597
    Elsif ?XJB:CONSUMER_HOUSE_NUMBER{prop:Req} = True
        ?XJB:CONSUMER_HOUSE_NUMBER{prop:FontColor} = 65793
        ?XJB:CONSUMER_HOUSE_NUMBER{prop:Color} = 8454143
    Else ! If ?XJB:CONSUMER_HOUSE_NUMBER{prop:Req} = True
        ?XJB:CONSUMER_HOUSE_NUMBER{prop:FontColor} = 65793
        ?XJB:CONSUMER_HOUSE_NUMBER{prop:Color} = 16777215
    End ! If ?XJB:CONSUMER_HOUSE_NUMBER{prop:Req} = True
    ?XJB:CONSUMER_HOUSE_NUMBER{prop:Trn} = 0
    ?XJB:CONSUMER_HOUSE_NUMBER{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    If ?XJB:EXCEPTION_DESC{prop:ReadOnly} = True
        ?XJB:EXCEPTION_DESC{prop:FontColor} = 65793
        ?XJB:EXCEPTION_DESC{prop:Color} = 15066597
    Elsif ?XJB:EXCEPTION_DESC{prop:Req} = True
        ?XJB:EXCEPTION_DESC{prop:FontColor} = 65793
        ?XJB:EXCEPTION_DESC{prop:Color} = 8454143
    Else ! If ?XJB:EXCEPTION_DESC{prop:Req} = True
        ?XJB:EXCEPTION_DESC{prop:FontColor} = 65793
        ?XJB:EXCEPTION_DESC{prop:Color} = 16777215
    End ! If ?XJB:EXCEPTION_DESC{prop:Req} = True
    ?XJB:EXCEPTION_DESC{prop:Trn} = 0
    ?XJB:EXCEPTION_DESC{prop:FontStyle} = font:Bold
    ?FailPostcode{prop:Font,3} = -1
    ?FailPostcode{prop:Color} = 15066597
    ?FailPostcode{prop:Trn} = 0
    ?FailModel{prop:Font,3} = -1
    ?FailModel{prop:Color} = 15066597
    ?FailModel{prop:Trn} = 0
    ?FailMSN{prop:Font,3} = -1
    ?FailMSN{prop:Color} = 15066597
    ?FailMSN{prop:Trn} = 0
    ?FailMissingFields{prop:Font,3} = -1
    ?FailMissingFields{prop:Color} = 15066597
    ?FailMissingFields{prop:Trn} = 0
    ?FailExport{prop:Font,3} = -1
    ?FailExport{prop:Color} = 15066597
    ?FailExport{prop:Trn} = 0
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?FailDuplicate{prop:Font,3} = -1
    ?FailDuplicate{prop:Color} = 15066597
    ?FailDuplicate{prop:Trn} = 0
    ?Prompt24{prop:FontColor} = -1
    ?Prompt24{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597

    ?Prompt25{prop:FontColor} = -1
    ?Prompt25{prop:Color} = 15066597
    If ?XJB:TR_NO{prop:ReadOnly} = True
        ?XJB:TR_NO{prop:FontColor} = 65793
        ?XJB:TR_NO{prop:Color} = 15066597
    Elsif ?XJB:TR_NO{prop:Req} = True
        ?XJB:TR_NO{prop:FontColor} = 65793
        ?XJB:TR_NO{prop:Color} = 8454143
    Else ! If ?XJB:TR_NO{prop:Req} = True
        ?XJB:TR_NO{prop:FontColor} = 65793
        ?XJB:TR_NO{prop:Color} = 16777215
    End ! If ?XJB:TR_NO{prop:Req} = True
    ?XJB:TR_NO{prop:Trn} = 0
    ?XJB:TR_NO{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateException')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt22
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(XJB:Record,History::XJB:Record)
  SELF.AddHistoryField(?XJB:CONSUMER_FIRST_NAME,21)
  SELF.AddHistoryField(?XJB:COMPANY,6)
  SELF.AddHistoryField(?XJB:MODEL_CODE,9)
  SELF.AddHistoryField(?XJB:ASC_CODE,8)
  SELF.AddHistoryField(?XJB:CONSUMER_POSTCODE,28)
  SELF.AddHistoryField(?XJB:REQUEST_DATE,14)
  SELF.AddHistoryField(?XJB:CONSUMER_LAST_NAME,22)
  SELF.AddHistoryField(?XJB:REPAIR_ETD_DATE,16)
  SELF.AddHistoryField(?XJB:CONSUMER_EMAIL,25)
  SELF.AddHistoryField(?XJB:SERVICE_TYPE,12)
  SELF.AddHistoryField(?XJB:CONSUMER_REGION,27)
  SELF.AddHistoryField(?XJB:CONSUMER_TEL_NUMBER1,23)
  SELF.AddHistoryField(?XJB:REPAIR_ETD_TIME,17)
  SELF.AddHistoryField(?XJB:REQUEST_TIME,15)
  SELF.AddHistoryField(?XJB:SERIAL_NO,10)
  SELF.AddHistoryField(?XJB:IN_OUT_WARRANTY,13)
  SELF.AddHistoryField(?XJB:CONSUMER_COUNTRY,26)
  SELF.AddHistoryField(?XJB:TR_STATUS,18)
  SELF.AddHistoryField(?XJB:SYMPTOM1_DESC,36)
  SELF.AddHistoryField(?XJB:SYMPTOM2_DESC,37)
  SELF.AddHistoryField(?XJB:SYMPTOM3_DESC,38)
  SELF.AddHistoryField(?XJB:INQUIRY_TEXT,40)
  SELF.AddHistoryField(?XJB:PURCHASE_DATE,11)
  SELF.AddHistoryField(?XJB:CONSUMER_CITY,29)
  SELF.AddHistoryField(?XJB:CONSUMER_STREET,30)
  SELF.AddHistoryField(?XJB:CONSUMER_HOUSE_NUMBER,31)
  SELF.AddHistoryField(?XJB:EXCEPTION_DESC,4)
  SELF.AddHistoryField(?XJB:TR_NO,7)
  SELF.AddUpdateFile(Access:XMLJOBS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:XMLJOBS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:XMLJOBS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  FailPostcode      = BAND(xjb:EXCEPTION_CODE, XC_POSTCODE)
  FailModel         = BAND(xjb:EXCEPTION_CODE, XC_MODEL)
  FailMSN           = BAND(xjb:EXCEPTION_CODE, XC_MSN)
  FailMissingFields = BAND(xjb:EXCEPTION_CODE, XC_MISSING_FIELDS)
  FailExport        = BAND(xjb:EXCEPTION_CODE, XC_EXPORT_FAIL)
  FailDuplicate     = BAND(xjb:EXCEPTION_CODE, XC_DUPLICATE)
  
  CopyPostcode      = FailPostcode
  CopyModel         = FailModel
  CopyMSN           = FailMSN
  CopyMissingFields = FailMissingFields
  CopyExport        = FailExport
  CopyDuplicate     = FailDuplicate
  
  OPEN(FormWindow)
  SELF.Opened=True
  Do RecolourWindow
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:XMLJOBS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?FailPostcode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailPostcode, Accepted)
      FailPostcode = CopyPostcode
      DISPLAY()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailPostcode, Accepted)
    OF ?FailModel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailModel, Accepted)
      FailModel = CopyModel
      DISPLAY()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailModel, Accepted)
    OF ?FailMSN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailMSN, Accepted)
      FailMSN = CopyMSN
      DISPLAY()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailMSN, Accepted)
    OF ?FailMissingFields
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailMissingFields, Accepted)
      FailMissingFields = CopyMissingFields
      DISPLAY()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailMissingFields, Accepted)
    OF ?FailExport
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailExport, Accepted)
      FailExport = CopyExport
      DISPLAY()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailExport, Accepted)
    OF ?FailDuplicate
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailDuplicate, Accepted)
      FailDuplicate = CopyDuplicate
      DISPLAY()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailDuplicate, Accepted)
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      IF (xjb:EXCEPTION_CODE = XC_EXPORT_FAIL) THEN
          xjb:RECORD_STATE = RS_NEW_EXPORT
      ELSE
          xjb:RECORD_STATE = RS_NEW_IMPORT
      END
      xjb:EXCEPTION_CODE = XC_NONE
      xjb:EXCEPTION_DESC = ''
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

