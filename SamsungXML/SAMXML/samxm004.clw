

   MEMBER('samxml.clw')                               ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SAMXM004.INC'),ONCE        !Local module procedure declarations
                     END


UpdateImportDefaults PROCEDURE                        !Generated from procedure template - Window

ActiveTrue           BYTE(1)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?XDF:JobTurnaroundTime
tur:Turnaround_Time    LIKE(tur:Turnaround_Time)      !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?XDF:UnitType
uni:Unit_Type          LIKE(uni:Unit_Type)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?XDF:TransitType
trt:Transit_Type       LIKE(trt:Transit_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:3 QUEUE                           !Queue declaration for browse/combo box using ?XDF:UserCode
use:User_Code          LIKE(use:User_Code)            !List box control field - type derived from field
use:Surname            LIKE(use:Surname)              !List box control field - type derived from field
use:Forename           LIKE(use:Forename)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(TURNARND)
                       PROJECT(tur:Turnaround_Time)
                     END
FDCB7::View:FileDropCombo VIEW(UNITTYPE)
                       PROJECT(uni:Unit_Type)
                     END
FDCB10::View:FileDropCombo VIEW(TRANTYPE)
                       PROJECT(trt:Transit_Type)
                     END
FDCB5::View:FileDropCombo VIEW(USERS)
                       PROJECT(use:User_Code)
                       PROJECT(use:Surname)
                       PROJECT(use:Forename)
                     END
window               WINDOW('Samsung Import Defaults'),AT(,,252,216),FONT('Arial',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,244,180),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           COMBO(@s3),AT(100,20,124,10),USE(XDF:UserCode),IMM,VSCROLL,UPR,FORMAT('26L(2)|M~Code~@s3@80L(2)|M~Surname~@s30@120L(2)|M~Forename~@s30@'),DROP(10,226),FROM(Queue:FileDropCombo:3)
                           PROMPT('User Code'),AT(8,20),USE(?Prompt9)
                           COMBO(@s30),AT(100,68,124,10),USE(XDF:TransitType),IMM,VSCROLL,LEFT,UPR,FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:2)
                           PROMPT('Account Number'),AT(8,32),USE(?Prompt1),TRN
                           ENTRY(@s15),AT(100,32,124,10),USE(XDF:AccountNumber),UPR
                           BUTTON('...'),AT(228,32,10,10),USE(?LookupTradeAccount),SKIP,TRN,LEFT,ICON('list3.ico')
                           PROMPT('Chargeable Charge Type'),AT(8,44),USE(?Prompt2),TRN
                           ENTRY(@s30),AT(100,56,124,10),USE(XDF:WarrantyChargeType),UPR
                           BUTTON('...'),AT(228,56,10,10),USE(?LookupWarrantyChargeType),SKIP,TRN,LEFT,ICON('list3.ico')
                           PROMPT('Warranty Charge Type'),AT(8,56),USE(?Prompt10),TRN
                           ENTRY(@s30),AT(100,44,124,10),USE(XDF:ChargeType)
                           BUTTON('...'),AT(228,44,10,10),USE(?LookupChargeType),LEFT,ICON('list3.ico')
                           PROMPT('Transit Type'),AT(8,68),USE(?Prompt3),TRN
                           PROMPT('Job Turnaround Time'),AT(8,80),USE(?Prompt4),TRN
                           PROMPT('Unit Type'),AT(8,92),USE(?Prompt5),TRN
                           COMBO(@s30),AT(100,80,124,10),USE(XDF:JobTurnaroundTime),IMM,VSCROLL,LEFT,UPR,FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Collection Status'),AT(8,104),USE(?Prompt6),TRN
                           ENTRY(@s30),AT(100,104,124,10),USE(XDF:CollectionStatus),UPR
                           BUTTON('...'),AT(228,104,10,10),USE(?ColectStatusLookup),FLAT,LEFT,ICON('list3.ico')
                           COMBO(@s30),AT(100,92,124,10),USE(XDF:UnitType),IMM,VSCROLL,LEFT,UPR,FORMAT('120L@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                           PROMPT('Drop Off Status'),AT(8,116),USE(?Prompt7),TRN
                           ENTRY(@s30),AT(100,116,124,10),USE(XDF:DropOffStatus)
                           BUTTON('...'),AT(228,116,10,10),USE(?DeliverStatusLookup),FLAT,LEFT,ICON('list3.ico')
                           PROMPT('Delivery Text'),AT(8,128),USE(?Prompt8),TRN
                           TEXT,AT(100,128,140,48),USE(XDF:DeliveryText),VSCROLL,UPR
                         END
                         TAB('Drop Point'),USE(?Tab2)
                           PROMPT('Status'),AT(8,24),USE(?Prompt11)
                           ENTRY(@s30),AT(96,24,124,10),USE(XDF:DropPointStatus)
                           BUTTON('...'),AT(224,24,10,10),USE(?DropPointStatusLookup),LEFT,ICON('list3.ico')
                           PROMPT('Location'),AT(8,36),USE(?Prompt12)
                           ENTRY(@s30),AT(96,36,124,10),USE(XDF:DropPointLocation),UPR
                           BUTTON('...'),AT(224,36,10,10),USE(?DropPointLocationLookup),LEFT,ICON('list3.ico')
                         END
                       END
                       PANEL,AT(4,188,244,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('OK'),AT(132,192,56,16),USE(?OkButton),TRN,LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(188,192,56,16),USE(?CancelButton),TRN,LEFT,ICON('cancel.gif'),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB10               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:3         !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
look:XDF:AccountNumber                Like(XDF:AccountNumber)
look:XDF:WarrantyChargeType                Like(XDF:WarrantyChargeType)
look:XDF:ChargeType                Like(XDF:ChargeType)
look:XDF:CollectionStatus                Like(XDF:CollectionStatus)
look:XDF:DropOffStatus                Like(XDF:DropOffStatus)
look:XDF:DropPointStatus                Like(XDF:DropPointStatus)
look:XDF:DropPointLocation                Like(XDF:DropPointLocation)
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    If ?XDF:UserCode{prop:ReadOnly} = True
        ?XDF:UserCode{prop:FontColor} = 65793
        ?XDF:UserCode{prop:Color} = 15066597
    Elsif ?XDF:UserCode{prop:Req} = True
        ?XDF:UserCode{prop:FontColor} = 65793
        ?XDF:UserCode{prop:Color} = 8454143
    Else ! If ?XDF:UserCode{prop:Req} = True
        ?XDF:UserCode{prop:FontColor} = 65793
        ?XDF:UserCode{prop:Color} = 16777215
    End ! If ?XDF:UserCode{prop:Req} = True
    ?XDF:UserCode{prop:Trn} = 0
    ?XDF:UserCode{prop:FontStyle} = font:Bold
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    If ?XDF:TransitType{prop:ReadOnly} = True
        ?XDF:TransitType{prop:FontColor} = 65793
        ?XDF:TransitType{prop:Color} = 15066597
    Elsif ?XDF:TransitType{prop:Req} = True
        ?XDF:TransitType{prop:FontColor} = 65793
        ?XDF:TransitType{prop:Color} = 8454143
    Else ! If ?XDF:TransitType{prop:Req} = True
        ?XDF:TransitType{prop:FontColor} = 65793
        ?XDF:TransitType{prop:Color} = 16777215
    End ! If ?XDF:TransitType{prop:Req} = True
    ?XDF:TransitType{prop:Trn} = 0
    ?XDF:TransitType{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?XDF:AccountNumber{prop:ReadOnly} = True
        ?XDF:AccountNumber{prop:FontColor} = 65793
        ?XDF:AccountNumber{prop:Color} = 15066597
    Elsif ?XDF:AccountNumber{prop:Req} = True
        ?XDF:AccountNumber{prop:FontColor} = 65793
        ?XDF:AccountNumber{prop:Color} = 8454143
    Else ! If ?XDF:AccountNumber{prop:Req} = True
        ?XDF:AccountNumber{prop:FontColor} = 65793
        ?XDF:AccountNumber{prop:Color} = 16777215
    End ! If ?XDF:AccountNumber{prop:Req} = True
    ?XDF:AccountNumber{prop:Trn} = 0
    ?XDF:AccountNumber{prop:FontStyle} = font:Bold
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?XDF:WarrantyChargeType{prop:ReadOnly} = True
        ?XDF:WarrantyChargeType{prop:FontColor} = 65793
        ?XDF:WarrantyChargeType{prop:Color} = 15066597
    Elsif ?XDF:WarrantyChargeType{prop:Req} = True
        ?XDF:WarrantyChargeType{prop:FontColor} = 65793
        ?XDF:WarrantyChargeType{prop:Color} = 8454143
    Else ! If ?XDF:WarrantyChargeType{prop:Req} = True
        ?XDF:WarrantyChargeType{prop:FontColor} = 65793
        ?XDF:WarrantyChargeType{prop:Color} = 16777215
    End ! If ?XDF:WarrantyChargeType{prop:Req} = True
    ?XDF:WarrantyChargeType{prop:Trn} = 0
    ?XDF:WarrantyChargeType{prop:FontStyle} = font:Bold
    ?Prompt10{prop:FontColor} = -1
    ?Prompt10{prop:Color} = 15066597
    If ?XDF:ChargeType{prop:ReadOnly} = True
        ?XDF:ChargeType{prop:FontColor} = 65793
        ?XDF:ChargeType{prop:Color} = 15066597
    Elsif ?XDF:ChargeType{prop:Req} = True
        ?XDF:ChargeType{prop:FontColor} = 65793
        ?XDF:ChargeType{prop:Color} = 8454143
    Else ! If ?XDF:ChargeType{prop:Req} = True
        ?XDF:ChargeType{prop:FontColor} = 65793
        ?XDF:ChargeType{prop:Color} = 16777215
    End ! If ?XDF:ChargeType{prop:Req} = True
    ?XDF:ChargeType{prop:Trn} = 0
    ?XDF:ChargeType{prop:FontStyle} = font:Bold
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    If ?XDF:JobTurnaroundTime{prop:ReadOnly} = True
        ?XDF:JobTurnaroundTime{prop:FontColor} = 65793
        ?XDF:JobTurnaroundTime{prop:Color} = 15066597
    Elsif ?XDF:JobTurnaroundTime{prop:Req} = True
        ?XDF:JobTurnaroundTime{prop:FontColor} = 65793
        ?XDF:JobTurnaroundTime{prop:Color} = 8454143
    Else ! If ?XDF:JobTurnaroundTime{prop:Req} = True
        ?XDF:JobTurnaroundTime{prop:FontColor} = 65793
        ?XDF:JobTurnaroundTime{prop:Color} = 16777215
    End ! If ?XDF:JobTurnaroundTime{prop:Req} = True
    ?XDF:JobTurnaroundTime{prop:Trn} = 0
    ?XDF:JobTurnaroundTime{prop:FontStyle} = font:Bold
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    If ?XDF:CollectionStatus{prop:ReadOnly} = True
        ?XDF:CollectionStatus{prop:FontColor} = 65793
        ?XDF:CollectionStatus{prop:Color} = 15066597
    Elsif ?XDF:CollectionStatus{prop:Req} = True
        ?XDF:CollectionStatus{prop:FontColor} = 65793
        ?XDF:CollectionStatus{prop:Color} = 8454143
    Else ! If ?XDF:CollectionStatus{prop:Req} = True
        ?XDF:CollectionStatus{prop:FontColor} = 65793
        ?XDF:CollectionStatus{prop:Color} = 16777215
    End ! If ?XDF:CollectionStatus{prop:Req} = True
    ?XDF:CollectionStatus{prop:Trn} = 0
    ?XDF:CollectionStatus{prop:FontStyle} = font:Bold
    If ?XDF:UnitType{prop:ReadOnly} = True
        ?XDF:UnitType{prop:FontColor} = 65793
        ?XDF:UnitType{prop:Color} = 15066597
    Elsif ?XDF:UnitType{prop:Req} = True
        ?XDF:UnitType{prop:FontColor} = 65793
        ?XDF:UnitType{prop:Color} = 8454143
    Else ! If ?XDF:UnitType{prop:Req} = True
        ?XDF:UnitType{prop:FontColor} = 65793
        ?XDF:UnitType{prop:Color} = 16777215
    End ! If ?XDF:UnitType{prop:Req} = True
    ?XDF:UnitType{prop:Trn} = 0
    ?XDF:UnitType{prop:FontStyle} = font:Bold
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    If ?XDF:DropOffStatus{prop:ReadOnly} = True
        ?XDF:DropOffStatus{prop:FontColor} = 65793
        ?XDF:DropOffStatus{prop:Color} = 15066597
    Elsif ?XDF:DropOffStatus{prop:Req} = True
        ?XDF:DropOffStatus{prop:FontColor} = 65793
        ?XDF:DropOffStatus{prop:Color} = 8454143
    Else ! If ?XDF:DropOffStatus{prop:Req} = True
        ?XDF:DropOffStatus{prop:FontColor} = 65793
        ?XDF:DropOffStatus{prop:Color} = 16777215
    End ! If ?XDF:DropOffStatus{prop:Req} = True
    ?XDF:DropOffStatus{prop:Trn} = 0
    ?XDF:DropOffStatus{prop:FontStyle} = font:Bold
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    If ?XDF:DeliveryText{prop:ReadOnly} = True
        ?XDF:DeliveryText{prop:FontColor} = 65793
        ?XDF:DeliveryText{prop:Color} = 15066597
    Elsif ?XDF:DeliveryText{prop:Req} = True
        ?XDF:DeliveryText{prop:FontColor} = 65793
        ?XDF:DeliveryText{prop:Color} = 8454143
    Else ! If ?XDF:DeliveryText{prop:Req} = True
        ?XDF:DeliveryText{prop:FontColor} = 65793
        ?XDF:DeliveryText{prop:Color} = 16777215
    End ! If ?XDF:DeliveryText{prop:Req} = True
    ?XDF:DeliveryText{prop:Trn} = 0
    ?XDF:DeliveryText{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    ?Prompt11{prop:FontColor} = -1
    ?Prompt11{prop:Color} = 15066597
    If ?XDF:DropPointStatus{prop:ReadOnly} = True
        ?XDF:DropPointStatus{prop:FontColor} = 65793
        ?XDF:DropPointStatus{prop:Color} = 15066597
    Elsif ?XDF:DropPointStatus{prop:Req} = True
        ?XDF:DropPointStatus{prop:FontColor} = 65793
        ?XDF:DropPointStatus{prop:Color} = 8454143
    Else ! If ?XDF:DropPointStatus{prop:Req} = True
        ?XDF:DropPointStatus{prop:FontColor} = 65793
        ?XDF:DropPointStatus{prop:Color} = 16777215
    End ! If ?XDF:DropPointStatus{prop:Req} = True
    ?XDF:DropPointStatus{prop:Trn} = 0
    ?XDF:DropPointStatus{prop:FontStyle} = font:Bold
    ?Prompt12{prop:FontColor} = -1
    ?Prompt12{prop:Color} = 15066597
    If ?XDF:DropPointLocation{prop:ReadOnly} = True
        ?XDF:DropPointLocation{prop:FontColor} = 65793
        ?XDF:DropPointLocation{prop:Color} = 15066597
    Elsif ?XDF:DropPointLocation{prop:Req} = True
        ?XDF:DropPointLocation{prop:FontColor} = 65793
        ?XDF:DropPointLocation{prop:Color} = 8454143
    Else ! If ?XDF:DropPointLocation{prop:Req} = True
        ?XDF:DropPointLocation{prop:FontColor} = 65793
        ?XDF:DropPointLocation{prop:Color} = 16777215
    End ! If ?XDF:DropPointLocation{prop:Req} = True
    ?XDF:DropPointLocation{prop:Trn} = 0
    ?XDF:DropPointLocation{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateImportDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?XDF:UserCode
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:STATUS.Open
  Relate:TRANTYPE.Open
  Relate:TURNARND.Open
  Relate:XMLDEFS.Open
  Access:SUBTRACC.UseFile
  Access:LOCINTER.UseFile
  SELF.FilesOpened = True
  SET(XMLDEFS)
  IF (access:xmldefs.next() <> Level:Benign) THEN
      IF (access:xmldefs.primerecord() = level:benign) THEN
          IF (access:xmldefs.insert() <> Level:Benign) THEN
              access:xmldefs.cancelautoinc()
          END
      END
  END
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  IF ?XDF:AccountNumber{Prop:Tip} AND ~?LookupTradeAccount{Prop:Tip}
     ?LookupTradeAccount{Prop:Tip} = 'Select ' & ?XDF:AccountNumber{Prop:Tip}
  END
  IF ?XDF:AccountNumber{Prop:Msg} AND ~?LookupTradeAccount{Prop:Msg}
     ?LookupTradeAccount{Prop:Msg} = 'Select ' & ?XDF:AccountNumber{Prop:Msg}
  END
  IF ?XDF:WarrantyChargeType{Prop:Tip} AND ~?LookupWarrantyChargeType{Prop:Tip}
     ?LookupWarrantyChargeType{Prop:Tip} = 'Select ' & ?XDF:WarrantyChargeType{Prop:Tip}
  END
  IF ?XDF:WarrantyChargeType{Prop:Msg} AND ~?LookupWarrantyChargeType{Prop:Msg}
     ?LookupWarrantyChargeType{Prop:Msg} = 'Select ' & ?XDF:WarrantyChargeType{Prop:Msg}
  END
  IF ?XDF:CollectionStatus{Prop:Tip} AND ~?ColectStatusLookup{Prop:Tip}
     ?ColectStatusLookup{Prop:Tip} = 'Select ' & ?XDF:CollectionStatus{Prop:Tip}
  END
  IF ?XDF:CollectionStatus{Prop:Msg} AND ~?ColectStatusLookup{Prop:Msg}
     ?ColectStatusLookup{Prop:Msg} = 'Select ' & ?XDF:CollectionStatus{Prop:Msg}
  END
  IF ?XDF:DropOffStatus{Prop:Tip} AND ~?DeliverStatusLookup{Prop:Tip}
     ?DeliverStatusLookup{Prop:Tip} = 'Select ' & ?XDF:DropOffStatus{Prop:Tip}
  END
  IF ?XDF:DropOffStatus{Prop:Msg} AND ~?DeliverStatusLookup{Prop:Msg}
     ?DeliverStatusLookup{Prop:Msg} = 'Select ' & ?XDF:DropOffStatus{Prop:Msg}
  END
  IF ?XDF:ChargeType{Prop:Tip} AND ~?LookupChargeType{Prop:Tip}
     ?LookupChargeType{Prop:Tip} = 'Select ' & ?XDF:ChargeType{Prop:Tip}
  END
  IF ?XDF:ChargeType{Prop:Msg} AND ~?LookupChargeType{Prop:Msg}
     ?LookupChargeType{Prop:Msg} = 'Select ' & ?XDF:ChargeType{Prop:Msg}
  END
  IF ?XDF:DropPointStatus{Prop:Tip} AND ~?DropPointStatusLookup{Prop:Tip}
     ?DropPointStatusLookup{Prop:Tip} = 'Select ' & ?XDF:DropPointStatus{Prop:Tip}
  END
  IF ?XDF:DropPointStatus{Prop:Msg} AND ~?DropPointStatusLookup{Prop:Msg}
     ?DropPointStatusLookup{Prop:Msg} = 'Select ' & ?XDF:DropPointStatus{Prop:Msg}
  END
  IF ?XDF:DropPointLocation{Prop:Tip} AND ~?DropPointLocationLookup{Prop:Tip}
     ?DropPointLocationLookup{Prop:Tip} = 'Select ' & ?XDF:DropPointLocation{Prop:Tip}
  END
  IF ?XDF:DropPointLocation{Prop:Msg} AND ~?DropPointLocationLookup{Prop:Msg}
     ?DropPointLocationLookup{Prop:Msg} = 'Select ' & ?XDF:DropPointLocation{Prop:Msg}
  END
  FDCB6.Init(XDF:JobTurnaroundTime,?XDF:JobTurnaroundTime,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:TURNARND,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(tur:Turnaround_Time_Key)
  FDCB6.AddField(tur:Turnaround_Time,FDCB6.Q.tur:Turnaround_Time)
  FDCB6.AddUpdateField(tur:Turnaround_Time,XDF:JobTurnaroundTime)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB7.Init(XDF:UnitType,?XDF:UnitType,Queue:FileDropCombo:1.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo:1,Relate:UNITTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo:1
  FDCB7.AddSortOrder(uni:ActiveKey)
  FDCB7.AddRange(uni:Active,ActiveTrue)
  FDCB7.AddField(uni:Unit_Type,FDCB7.Q.uni:Unit_Type)
  FDCB7.AddUpdateField(uni:Unit_Type,XDF:UnitType)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB10.Init(XDF:TransitType,?XDF:TransitType,Queue:FileDropCombo:2.ViewPosition,FDCB10::View:FileDropCombo,Queue:FileDropCombo:2,Relate:TRANTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB10.Q &= Queue:FileDropCombo:2
  FDCB10.AddSortOrder(trt:Transit_Type_Key)
  FDCB10.AddField(trt:Transit_Type,FDCB10.Q.trt:Transit_Type)
  FDCB10.AddUpdateField(trt:Transit_Type,XDF:TransitType)
  ThisWindow.AddItem(FDCB10.WindowComponent)
  FDCB5.Init(XDF:UserCode,?XDF:UserCode,Queue:FileDropCombo:3.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo:3,Relate:USERS,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo:3
  FDCB5.AddSortOrder(use:User_Code_Key)
  FDCB5.AddField(use:User_Code,FDCB5.Q.use:User_Code)
  FDCB5.AddField(use:Surname,FDCB5.Q.use:Surname)
  FDCB5.AddField(use:Forename,FDCB5.Q.use:Forename)
  FDCB5.AddUpdateField(use:User_Code,XDF:UserCode)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:STATUS.Close
    Relate:TRANTYPE.Close
    Relate:TURNARND.Close
    Relate:XMLDEFS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickSubAccounts
      PickWarrantyChargeTypes
      PickChargeableChargeTypes
      Browse_Status
      Browse_Status
      Browse_Status
      Browse_Locations
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?XDF:AccountNumber
      IF XDF:AccountNumber OR ?XDF:AccountNumber{Prop:Req}
        sub:Account_Number = XDF:AccountNumber
        !Save Lookup Field Incase Of error
        look:XDF:AccountNumber        = XDF:AccountNumber
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            XDF:AccountNumber = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            XDF:AccountNumber = look:XDF:AccountNumber
            SELECT(?XDF:AccountNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupTradeAccount
      ThisWindow.Update
      sub:Account_Number = XDF:AccountNumber
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          XDF:AccountNumber = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?XDF:AccountNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?XDF:AccountNumber)
    OF ?XDF:WarrantyChargeType
      IF XDF:WarrantyChargeType OR ?XDF:WarrantyChargeType{Prop:Req}
        cha:Charge_Type = XDF:WarrantyChargeType
        cha:Warranty = 'YES'
        !Save Lookup Field Incase Of error
        look:XDF:WarrantyChargeType        = XDF:WarrantyChargeType
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            XDF:WarrantyChargeType = cha:Charge_Type
          ELSE
            CLEAR(cha:Warranty)
            !Restore Lookup On Error
            XDF:WarrantyChargeType = look:XDF:WarrantyChargeType
            SELECT(?XDF:WarrantyChargeType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupWarrantyChargeType
      ThisWindow.Update
      cha:Charge_Type = XDF:WarrantyChargeType
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          XDF:WarrantyChargeType = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?XDF:WarrantyChargeType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?XDF:WarrantyChargeType)
    OF ?XDF:ChargeType
      IF XDF:ChargeType OR ?XDF:ChargeType{Prop:Req}
        cha:Charge_Type = XDF:ChargeType
        cha:Warranty = 'NO'
        !Save Lookup Field Incase Of error
        look:XDF:ChargeType        = XDF:ChargeType
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            XDF:ChargeType = cha:Charge_Type
          ELSE
            CLEAR(cha:Warranty)
            !Restore Lookup On Error
            XDF:ChargeType = look:XDF:ChargeType
            SELECT(?XDF:ChargeType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupChargeType
      ThisWindow.Update
      cha:Charge_Type = XDF:ChargeType
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          XDF:ChargeType = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?XDF:ChargeType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?XDF:ChargeType)
    OF ?XDF:CollectionStatus
      IF XDF:CollectionStatus OR ?XDF:CollectionStatus{Prop:Req}
        sts:Status = XDF:CollectionStatus
        sts:Job = 'YES'
        GLO:Select1 = 'JOB'
        !Save Lookup Field Incase Of error
        look:XDF:CollectionStatus        = XDF:CollectionStatus
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            XDF:CollectionStatus = sts:Status
          ELSE
            CLEAR(sts:Job)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            XDF:CollectionStatus = look:XDF:CollectionStatus
            SELECT(?XDF:CollectionStatus)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?ColectStatusLookup
      ThisWindow.Update
      sts:Status = XDF:CollectionStatus
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          XDF:CollectionStatus = sts:Status
          Select(?+1)
      ELSE
          Select(?XDF:CollectionStatus)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?XDF:CollectionStatus)
    OF ?XDF:DropOffStatus
      IF XDF:DropOffStatus OR ?XDF:DropOffStatus{Prop:Req}
        sts:Status = XDF:DropOffStatus
        sts:Job = 'YES'
        GLO:Select1 = 'JOB'
        !Save Lookup Field Incase Of error
        look:XDF:DropOffStatus        = XDF:DropOffStatus
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            XDF:DropOffStatus = sts:Status
          ELSE
            CLEAR(sts:Job)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            XDF:DropOffStatus = look:XDF:DropOffStatus
            SELECT(?XDF:DropOffStatus)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?DeliverStatusLookup
      ThisWindow.Update
      sts:Status = XDF:DropOffStatus
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          XDF:DropOffStatus = sts:Status
          Select(?+1)
      ELSE
          Select(?XDF:DropOffStatus)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?XDF:DropOffStatus)
    OF ?XDF:DropPointStatus
      IF XDF:DropPointStatus OR ?XDF:DropPointStatus{Prop:Req}
        sts:Status = XDF:DropPointStatus
        sts:Job = 'YES'
        GLO:Select1 = 'JOB'
        !Save Lookup Field Incase Of error
        look:XDF:DropPointStatus        = XDF:DropPointStatus
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            XDF:DropPointStatus = sts:Status
          ELSE
            CLEAR(sts:Job)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            XDF:DropPointStatus = look:XDF:DropPointStatus
            SELECT(?XDF:DropPointStatus)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?DropPointStatusLookup
      ThisWindow.Update
      sts:Status = XDF:DropPointStatus
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          XDF:DropPointStatus = sts:Status
          Select(?+1)
      ELSE
          Select(?XDF:DropPointStatus)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?XDF:DropPointStatus)
    OF ?XDF:DropPointLocation
      IF XDF:DropPointLocation OR ?XDF:DropPointLocation{Prop:Req}
        loi:Location = XDF:DropPointLocation
        loi:Location_Available = 'YES'
        !Save Lookup Field Incase Of error
        look:XDF:DropPointLocation        = XDF:DropPointLocation
        IF Access:LOCINTER.TryFetch(loi:Location_Available_Key)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            XDF:DropPointLocation = loi:Location
          ELSE
            CLEAR(loi:Location_Available)
            !Restore Lookup On Error
            XDF:DropPointLocation = look:XDF:DropPointLocation
            SELECT(?XDF:DropPointLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?DropPointLocationLookup
      ThisWindow.Update
      loi:Location = XDF:DropPointLocation
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          XDF:DropPointLocation = loi:Location
          Select(?+1)
      ELSE
          Select(?XDF:DropPointLocation)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?XDF:DropPointLocation)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      access:xmldefs.update()
      POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

