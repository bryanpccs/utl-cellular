

   MEMBER('xmlmod.clw')                               ! This is a MEMBER module

                     MAP
                       INCLUDE('XMLMO002.INC'),ONCE        !Local module procedure declarations
                     END


ValidatePostcode2    PROCEDURE  (arg:postcode)        ! Declare Procedure
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
szAddress   CSTRING(255)
szPath      CSTRING(255)
szPostcode  CSTRING(20)
tmp:OS      STRING(20)
tmp:High    BYTE
AddressPath STRING(255)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    SET(Defaults)
    access:defaults.next()

    ! start change xxxx 03/11/2005 BE
    ! Hard code the pathname!!!!!!
    AddressPath = '\\csg2kapp02\sbase\apps\address2\address'
    ! end change xxxx 03/11/2005 BE
 
    IF (def:use_postcode = 'YES') THEN
        IF (def:postcodedll <> 'YES') THEN
            SETCLIPBOARD(arg:postcode)
            tmp:os  = GetVersion()
            tmp:high    = BSHIFT(tmp:os,-8)
            IF (tmp:high = 0) THEN
                ! start change xxxx 03/11/2005 BE
                !RUN(CLIP(def:postcode_path) & '\addressc.exe',0)
                RUN(CLIP(AddressPath) & '\addressc.exe',0)
                ! end change xxxx 03/11/2005 BE
            ELSE
                ! start change xxxx 03/11/2005 BE
                !RUN(CLIP(def:postcode_path) & '\addressc.exe',1)
                RUN(CLIP(AddressPath) & '\addressc.exe',1)
                ! end change xxxx 03/11/2005 BE
            END
            IF (ERRORCODE()) THEN
                RETURN Level:Fatal
            ElSIF SUB(CLIPBOARD(),1,1) = '#'
                RETURN Level:Fatal
            ELSE
                RETURN Level:Benign
            END
        ELSE
            ! start change xxxx 03/11/2005 BE
            !szpath  = CLIP(def:postcode_path)
            szpath  = CLIP(AddressPath)
            ! end change xxxx 03/11/2005 BE
            IF (Initaddress(szpath)) THEN
                szpostcode  = arg:postcode
                IF (getaddress(szpostcode,szaddress)) THEN
                    RETURN Level:Benign
                ELSE
                    RETURN Level:Fatal
                END
            ELSE
                RETURN Level:Fatal
            END
        END
    END
    RETURN Level:Benign

! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
