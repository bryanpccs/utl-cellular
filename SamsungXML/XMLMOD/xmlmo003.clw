

   MEMBER('xmlmod.clw')                               ! This is a MEMBER module

                     MAP
                       INCLUDE('XMLMO003.INC'),ONCE        !Local module procedure declarations
                     END


StripChars           PROCEDURE  (in_string)           ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    in_string = CLIP(LEFT(in_string))

    STR_LEN#  = LEN(in_string)
    STR_POS#  = 1

    in_string = UPPER(SUB(in_string,STR_POS#,1)) & SUB(in_string,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

      IF SUB(in_string,STR_POS#,1) = ','
        in_string = SUB(in_string,1,STR_POS#-1) & ' ' & SUB(in_string,STR_POS#+1,STR_LEN#-1)
      END

      IF SUB(in_string,STR_POS#,1) = '&'
        in_string = SUB(in_string,1,STR_POS#-1) & ' ' & SUB(in_string,STR_POS#+1,STR_LEN#-1)
      END

      IF SUB(in_string,STR_POS#,1) = '('
        in_string = SUB(in_string,1,STR_POS#-1) & ' ' & SUB(in_string,STR_POS#+1,STR_LEN#-1)
      END

      IF SUB(in_string,STR_POS#,1) = ')'
        in_string = SUB(in_string,1,STR_POS#-1) & ' ' & SUB(in_string,STR_POS#+1,STR_LEN#-1)
      END

    END

    RETURN(in_string)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
