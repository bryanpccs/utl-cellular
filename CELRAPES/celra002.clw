

   MEMBER('celrapes.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA002.INC'),ONCE        !Local module procedure declarations
                     END


RapidEstimateProcedure PROCEDURE                      !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
JobNumberQueue       QUEUE,PRE(jobque)
JobNumber            LONG
Order                LONG
                     END
tmp:RepairType       STRING(30)
tmp:Location         STRING(30)
tmp:InvoiceText      STRING(255)
JobDetailsGroup      GROUP,PRE()
tmp:LabourCost       REAL
tmp:CourierCost      REAL
tmp:PartsCost        REAL
tmp:SubTotal         REAL
tmp:VAT              REAL
tmp:Total            REAL
                     END
tmp:JobNumber        LONG
tmp:IMEINumber       STRING(30)
tmp:LabourVATRate    REAL
tmp:PartsVATRate     REAL
window               WINDOW('Rapid Estimate Procedure'),AT(,,447,327),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,284,100),USE(?Sheet1),SPREAD
                         TAB('Estimate Details'),USE(?EstimateDetails)
                           PROMPT('Repair Type'),AT(8,20),USE(?tmp:RepairType:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,20,124,10),USE(tmp:RepairType),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),MSG('Repair Type'),TIP('Repair Type'),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),ALRT(DownKey),REQ,UPR
                           BUTTON,AT(212,20,10,10),USE(?LookupRepairType),SKIP,ICON('List3.ico')
                           PROMPT('Location'),AT(8,36),USE(?tmp:Location:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,36,124,10),USE(tmp:Location),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),MSG('Location'),TIP('Location'),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),ALRT(DownKey),UPR
                           BUTTON,AT(212,36,10,10),USE(?LookupLocation),SKIP,ICON('List3.ico')
                           PROMPT('Invoice Text'),AT(8,52),USE(?tmp:InvoiceText:prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           TEXT,AT(84,52,125,46),USE(tmp:InvoiceText),VSCROLL,FONT(,,,FONT:bold),UPR
                           BUTTON,AT(212,52,10,10),USE(?LookupInvoiceText),SKIP,ICON('List3.ico')
                         END
                       END
                       SHEET,AT(292,4,152,292),USE(?Sheet4),SPREAD
                         TAB('Updated Jobs'),USE(?Tab4)
                           LIST,AT(296,20,144,272),USE(?List1),SKIP,FORMAT('32L(2)|M~Job Number~@s8@'),FROM(JobNumberQueue)
                         END
                       END
                       SHEET,AT(4,108,284,132),USE(?Sheet2),SPREAD
                         TAB('Job Details'),USE(?Tab2)
                           GROUP('Estimate Charge'),AT(148,120,136,112),USE(?Group1),BOXED
                             PROMPT('Labour Cost'),AT(156,132),USE(?Prompt8)
                             STRING(@n14.2),AT(204,132),USE(tmp:LabourCost),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                             STRING(@n14.2),AT(204,148),USE(tmp:PartsCost),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                             PROMPT('Parts Cost'),AT(156,148),USE(?Prompt8:2)
                             STRING(@n14.2),AT(204,164),USE(tmp:CourierCost),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                             STRING(@n14.2),AT(204,180),USE(tmp:SubTotal),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                             LINE,AT(220,216,55,0),USE(?Line1),COLOR(COLOR:Black)
                             LINE,AT(220,196,55,0),USE(?Line1:2),COLOR(COLOR:Black)
                             STRING(@n14.2),AT(204,200),USE(tmp:VAT),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                             STRING(@n14.2),AT(204,220),USE(tmp:Total),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                             PROMPT('Courier Cost'),AT(156,164),USE(?Prompt8:3)
                             PROMPT('Sub Total'),AT(156,180),USE(?Prompt8:4)
                             PROMPT('V.A.T.'),AT(156,200),USE(?Prompt8:5)
                             PROMPT('Total'),AT(156,220),USE(?Prompt8:6)
                           END
                           STRING(@s30),AT(8,124),USE(job:Company_Name),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(8,132),USE(job:Address_Line1),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(8,140),USE(job:Address_Line2),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(8,148),USE(job:Address_Line3),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(8,176),USE(job:Charge_Type),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(8,192),USE(job:Manufacturer),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(8,208),USE(job:Model_Number),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s20),AT(8,224),USE(job:ESN),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s10),AT(8,156),USE(job:Postcode),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Charge Type'),AT(7,168),USE(?Prompt4),FONT(,7,,,CHARSET:ANSI)
                           PROMPT('Manufacturer'),AT(8,184),USE(?Prompt4:2),FONT(,7,,,CHARSET:ANSI)
                           PROMPT('Model Number'),AT(8,200),USE(?Prompt4:3),FONT(,7,,,CHARSET:ANSI)
                           PROMPT('I.M.E.I. Number'),AT(8,216),USE(?Prompt4:4),FONT(,7,,,CHARSET:ANSI)
                         END
                       END
                       SHEET,AT(4,244,284,52),USE(?Sheet3),SPREAD
                         TAB('Job Number'),USE(?Tab3)
                           ENTRY(@s8),AT(84,260,64,10),USE(tmp:JobNumber),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),MSG('Job Number'),TIP('Job Number'),REQ,UPR
                           PROMPT('I.M.E.I. Number'),AT(8,276),USE(?tmp:IMEINumber:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,276,124,10),USE(tmp:IMEINumber),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),MSG('I.M.E.I. Number'),TIP('I.M.E.I. Number'),REQ,UPR
                           PROMPT('Job Number'),AT(8,260),USE(?tmp:jobNumber:prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         END
                       END
                       PANEL,AT(4,300,440,24),USE(?ButtonPanel),FILL(COLOR:Silver)
                       BUTTON('Process Estimate [F10]'),AT(8,304,104,16),USE(?ProcessEstimate),DISABLE,LEFT,ICON('process.gif')
                       BUTTON('&Finish'),AT(384,304,56,16),USE(?Finish),LEFT,ICON('thumbs.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:RepairType                Like(tmp:RepairType)
look:tmp:Location                Like(tmp:Location)
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?EstimateDetails{prop:Color} = 15066597
    ?tmp:RepairType:Prompt{prop:FontColor} = -1
    ?tmp:RepairType:Prompt{prop:Color} = 15066597
    If ?tmp:RepairType{prop:ReadOnly} = True
        ?tmp:RepairType{prop:FontColor} = 65793
        ?tmp:RepairType{prop:Color} = 15066597
    Elsif ?tmp:RepairType{prop:Req} = True
        ?tmp:RepairType{prop:FontColor} = 65793
        ?tmp:RepairType{prop:Color} = 8454143
    Else ! If ?tmp:RepairType{prop:Req} = True
        ?tmp:RepairType{prop:FontColor} = 65793
        ?tmp:RepairType{prop:Color} = 16777215
    End ! If ?tmp:RepairType{prop:Req} = True
    ?tmp:RepairType{prop:Trn} = 0
    ?tmp:RepairType{prop:FontStyle} = font:Bold
    ?tmp:Location:Prompt{prop:FontColor} = -1
    ?tmp:Location:Prompt{prop:Color} = 15066597
    If ?tmp:Location{prop:ReadOnly} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 15066597
    Elsif ?tmp:Location{prop:Req} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 8454143
    Else ! If ?tmp:Location{prop:Req} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 16777215
    End ! If ?tmp:Location{prop:Req} = True
    ?tmp:Location{prop:Trn} = 0
    ?tmp:Location{prop:FontStyle} = font:Bold
    ?tmp:InvoiceText:prompt{prop:FontColor} = -1
    ?tmp:InvoiceText:prompt{prop:Color} = 15066597
    If ?tmp:InvoiceText{prop:ReadOnly} = True
        ?tmp:InvoiceText{prop:FontColor} = 65793
        ?tmp:InvoiceText{prop:Color} = 15066597
    Elsif ?tmp:InvoiceText{prop:Req} = True
        ?tmp:InvoiceText{prop:FontColor} = 65793
        ?tmp:InvoiceText{prop:Color} = 8454143
    Else ! If ?tmp:InvoiceText{prop:Req} = True
        ?tmp:InvoiceText{prop:FontColor} = 65793
        ?tmp:InvoiceText{prop:Color} = 16777215
    End ! If ?tmp:InvoiceText{prop:Req} = True
    ?tmp:InvoiceText{prop:Trn} = 0
    ?tmp:InvoiceText{prop:FontStyle} = font:Bold
    ?Sheet4{prop:Color} = 15066597
    ?Tab4{prop:Color} = 15066597
    ?List1{prop:FontColor} = 65793
    ?List1{prop:Color}= 16777215
    ?List1{prop:Color,2} = 16777215
    ?List1{prop:Color,3} = 12937777
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    ?tmp:LabourCost{prop:FontColor} = -1
    ?tmp:LabourCost{prop:Color} = 15066597
    ?tmp:PartsCost{prop:FontColor} = -1
    ?tmp:PartsCost{prop:Color} = 15066597
    ?Prompt8:2{prop:FontColor} = -1
    ?Prompt8:2{prop:Color} = 15066597
    ?tmp:CourierCost{prop:FontColor} = -1
    ?tmp:CourierCost{prop:Color} = 15066597
    ?tmp:SubTotal{prop:FontColor} = -1
    ?tmp:SubTotal{prop:Color} = 15066597
    ?tmp:VAT{prop:FontColor} = -1
    ?tmp:VAT{prop:Color} = 15066597
    ?tmp:Total{prop:FontColor} = -1
    ?tmp:Total{prop:Color} = 15066597
    ?Prompt8:3{prop:FontColor} = -1
    ?Prompt8:3{prop:Color} = 15066597
    ?Prompt8:4{prop:FontColor} = -1
    ?Prompt8:4{prop:Color} = 15066597
    ?Prompt8:5{prop:FontColor} = -1
    ?Prompt8:5{prop:Color} = 15066597
    ?Prompt8:6{prop:FontColor} = -1
    ?Prompt8:6{prop:Color} = 15066597
    ?job:Company_Name{prop:FontColor} = -1
    ?job:Company_Name{prop:Color} = 15066597
    ?job:Address_Line1{prop:FontColor} = -1
    ?job:Address_Line1{prop:Color} = 15066597
    ?job:Address_Line2{prop:FontColor} = -1
    ?job:Address_Line2{prop:Color} = 15066597
    ?job:Address_Line3{prop:FontColor} = -1
    ?job:Address_Line3{prop:Color} = 15066597
    ?job:Charge_Type{prop:FontColor} = -1
    ?job:Charge_Type{prop:Color} = 15066597
    ?job:Manufacturer{prop:FontColor} = -1
    ?job:Manufacturer{prop:Color} = 15066597
    ?job:Model_Number{prop:FontColor} = -1
    ?job:Model_Number{prop:Color} = 15066597
    ?job:ESN{prop:FontColor} = -1
    ?job:ESN{prop:Color} = 15066597
    ?job:Postcode{prop:FontColor} = -1
    ?job:Postcode{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?Prompt4:2{prop:FontColor} = -1
    ?Prompt4:2{prop:Color} = 15066597
    ?Prompt4:3{prop:FontColor} = -1
    ?Prompt4:3{prop:Color} = 15066597
    ?Prompt4:4{prop:FontColor} = -1
    ?Prompt4:4{prop:Color} = 15066597
    ?Sheet3{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    If ?tmp:JobNumber{prop:ReadOnly} = True
        ?tmp:JobNumber{prop:FontColor} = 65793
        ?tmp:JobNumber{prop:Color} = 15066597
    Elsif ?tmp:JobNumber{prop:Req} = True
        ?tmp:JobNumber{prop:FontColor} = 65793
        ?tmp:JobNumber{prop:Color} = 8454143
    Else ! If ?tmp:JobNumber{prop:Req} = True
        ?tmp:JobNumber{prop:FontColor} = 65793
        ?tmp:JobNumber{prop:Color} = 16777215
    End ! If ?tmp:JobNumber{prop:Req} = True
    ?tmp:JobNumber{prop:Trn} = 0
    ?tmp:JobNumber{prop:FontStyle} = font:Bold
    ?tmp:IMEINumber:Prompt{prop:FontColor} = -1
    ?tmp:IMEINumber:Prompt{prop:Color} = 15066597
    If ?tmp:IMEINumber{prop:ReadOnly} = True
        ?tmp:IMEINumber{prop:FontColor} = 65793
        ?tmp:IMEINumber{prop:Color} = 15066597
    Elsif ?tmp:IMEINumber{prop:Req} = True
        ?tmp:IMEINumber{prop:FontColor} = 65793
        ?tmp:IMEINumber{prop:Color} = 8454143
    Else ! If ?tmp:IMEINumber{prop:Req} = True
        ?tmp:IMEINumber{prop:FontColor} = 65793
        ?tmp:IMEINumber{prop:Color} = 16777215
    End ! If ?tmp:IMEINumber{prop:Req} = True
    ?tmp:IMEINumber{prop:Trn} = 0
    ?tmp:IMEINumber{prop:FontStyle} = font:Bold
    ?tmp:jobNumber:prompt{prop:FontColor} = -1
    ?tmp:jobNumber:prompt{prop:Color} = 15066597
    ?ButtonPanel{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
ClearWindow     Routine
    Clear(job:Record)
    Clear(JobDetailsGroup)
    tmp:JobNumber = ''
    tmp:IMEINumber = ''
    Select(?tmp:JobNumber)
    Display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'RapidEstimateProcedure',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('JobNumberQueue:JobNumber',JobNumberQueue:JobNumber,'RapidEstimateProcedure',1)
    SolaceViewVars('JobNumberQueue:Order',JobNumberQueue:Order,'RapidEstimateProcedure',1)
    SolaceViewVars('tmp:RepairType',tmp:RepairType,'RapidEstimateProcedure',1)
    SolaceViewVars('tmp:Location',tmp:Location,'RapidEstimateProcedure',1)
    SolaceViewVars('tmp:InvoiceText',tmp:InvoiceText,'RapidEstimateProcedure',1)
    SolaceViewVars('JobDetailsGroup:tmp:LabourCost',JobDetailsGroup:tmp:LabourCost,'RapidEstimateProcedure',1)
    SolaceViewVars('JobDetailsGroup:tmp:CourierCost',JobDetailsGroup:tmp:CourierCost,'RapidEstimateProcedure',1)
    SolaceViewVars('JobDetailsGroup:tmp:PartsCost',JobDetailsGroup:tmp:PartsCost,'RapidEstimateProcedure',1)
    SolaceViewVars('JobDetailsGroup:tmp:SubTotal',JobDetailsGroup:tmp:SubTotal,'RapidEstimateProcedure',1)
    SolaceViewVars('JobDetailsGroup:tmp:VAT',JobDetailsGroup:tmp:VAT,'RapidEstimateProcedure',1)
    SolaceViewVars('JobDetailsGroup:tmp:Total',JobDetailsGroup:tmp:Total,'RapidEstimateProcedure',1)
    SolaceViewVars('tmp:JobNumber',tmp:JobNumber,'RapidEstimateProcedure',1)
    SolaceViewVars('tmp:IMEINumber',tmp:IMEINumber,'RapidEstimateProcedure',1)
    SolaceViewVars('tmp:LabourVATRate',tmp:LabourVATRate,'RapidEstimateProcedure',1)
    SolaceViewVars('tmp:PartsVATRate',tmp:PartsVATRate,'RapidEstimateProcedure',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EstimateDetails;  SolaceCtrlName = '?EstimateDetails';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:RepairType:Prompt;  SolaceCtrlName = '?tmp:RepairType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:RepairType;  SolaceCtrlName = '?tmp:RepairType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupRepairType;  SolaceCtrlName = '?LookupRepairType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Location:Prompt;  SolaceCtrlName = '?tmp:Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Location;  SolaceCtrlName = '?tmp:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupLocation;  SolaceCtrlName = '?LookupLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:InvoiceText:prompt;  SolaceCtrlName = '?tmp:InvoiceText:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:InvoiceText;  SolaceCtrlName = '?tmp:InvoiceText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupInvoiceText;  SolaceCtrlName = '?LookupInvoiceText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet4;  SolaceCtrlName = '?Sheet4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List1;  SolaceCtrlName = '?List1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group1;  SolaceCtrlName = '?Group1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LabourCost;  SolaceCtrlName = '?tmp:LabourCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:PartsCost;  SolaceCtrlName = '?tmp:PartsCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8:2;  SolaceCtrlName = '?Prompt8:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CourierCost;  SolaceCtrlName = '?tmp:CourierCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SubTotal;  SolaceCtrlName = '?tmp:SubTotal';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1;  SolaceCtrlName = '?Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1:2;  SolaceCtrlName = '?Line1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:VAT;  SolaceCtrlName = '?tmp:VAT';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Total;  SolaceCtrlName = '?tmp:Total';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8:3;  SolaceCtrlName = '?Prompt8:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8:4;  SolaceCtrlName = '?Prompt8:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8:5;  SolaceCtrlName = '?Prompt8:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8:6;  SolaceCtrlName = '?Prompt8:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Company_Name;  SolaceCtrlName = '?job:Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line1;  SolaceCtrlName = '?job:Address_Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line2;  SolaceCtrlName = '?job:Address_Line2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line3;  SolaceCtrlName = '?job:Address_Line3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Charge_Type;  SolaceCtrlName = '?job:Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Manufacturer;  SolaceCtrlName = '?job:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Model_Number;  SolaceCtrlName = '?job:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:ESN;  SolaceCtrlName = '?job:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Postcode;  SolaceCtrlName = '?job:Postcode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4:2;  SolaceCtrlName = '?Prompt4:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4:3;  SolaceCtrlName = '?Prompt4:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4:4;  SolaceCtrlName = '?Prompt4:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet3;  SolaceCtrlName = '?Sheet3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobNumber;  SolaceCtrlName = '?tmp:JobNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:IMEINumber:Prompt;  SolaceCtrlName = '?tmp:IMEINumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:IMEINumber;  SolaceCtrlName = '?tmp:IMEINumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:jobNumber:prompt;  SolaceCtrlName = '?tmp:jobNumber:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ButtonPanel;  SolaceCtrlName = '?ButtonPanel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ProcessEstimate;  SolaceCtrlName = '?ProcessEstimate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Finish;  SolaceCtrlName = '?Finish';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('RapidEstimateProcedure')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'RapidEstimateProcedure')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:RepairType:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Finish,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:REPTYDEF.Open
  Relate:STATUS.Open
  Relate:VATCODE.Open
  Access:JOBS.UseFile
  Access:LOCINTER.UseFile
  Access:CHARTYPE.UseFile
  Access:ESTPARTS.UseFile
  Access:PARTS.UseFile
  Access:STDCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:TRADEACC.UseFile
  Access:WARPARTS.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBSTAGE.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List1{prop:vcr} = TRUE
  IF ?tmp:RepairType{Prop:Tip} AND ~?LookupRepairType{Prop:Tip}
     ?LookupRepairType{Prop:Tip} = 'Select ' & ?tmp:RepairType{Prop:Tip}
  END
  IF ?tmp:RepairType{Prop:Msg} AND ~?LookupRepairType{Prop:Msg}
     ?LookupRepairType{Prop:Msg} = 'Select ' & ?tmp:RepairType{Prop:Msg}
  END
  IF ?tmp:Location{Prop:Tip} AND ~?LookupLocation{Prop:Tip}
     ?LookupLocation{Prop:Tip} = 'Select ' & ?tmp:Location{Prop:Tip}
  END
  IF ?tmp:Location{Prop:Msg} AND ~?LookupLocation{Prop:Msg}
     ?LookupLocation{Prop:Msg} = 'Select ' & ?tmp:Location{Prop:Msg}
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:REPTYDEF.Close
    Relate:STATUS.Close
    Relate:VATCODE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'RapidEstimateProcedure',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickChargeableRepairTypes
      Browse_Locations
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?LookupInvoiceText
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupInvoiceText, Accepted)
      glo:Select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupInvoiceText, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:RepairType
      IF tmp:RepairType OR ?tmp:RepairType{Prop:Req}
        rtd:Repair_Type = tmp:RepairType
        !Save Lookup Field Incase Of error
        look:tmp:RepairType        = tmp:RepairType
        IF Access:REPTYDEF.TryFetch(rtd:Chargeable_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:RepairType = rtd:Repair_Type
          ELSE
            !Restore Lookup On Error
            tmp:RepairType = look:tmp:RepairType
            SELECT(?tmp:RepairType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupRepairType
      ThisWindow.Update
      rtd:Repair_Type = tmp:RepairType
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:RepairType = rtd:Repair_Type
          Select(?+1)
      ELSE
          Select(?tmp:RepairType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:RepairType)
    OF ?tmp:Location
      IF tmp:Location OR ?tmp:Location{Prop:Req}
        loi:Location = tmp:Location
        loi:Location_Available = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:Location        = tmp:Location
        IF Access:LOCINTER.TryFetch(loi:Location_Available_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:Location = loi:Location
          ELSE
            CLEAR(loi:Location_Available)
            !Restore Lookup On Error
            tmp:Location = look:tmp:Location
            SELECT(?tmp:Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupLocation
      ThisWindow.Update
      loi:Location = tmp:Location
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:Location = loi:Location
          Select(?+1)
      ELSE
          Select(?tmp:Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Location)
    OF ?LookupInvoiceText
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Invoice_Text
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupInvoiceText, Accepted)
      If Records(glo:Q_Notes) <> ''
          Loop x$ = 1 To Records(glo:Q_Notes)
              Get(glo:Q_Notes,x$)
              If tmp:InvoiceText = ''
                  tmp:InvoiceText = Clip(glo:notes_pointer)
              Else !If job:engineers_notes = ''
                  tmp:InvoiceText = Clip(tmp:InvoiceText) & '. ' & Clip(glo:notes_pointer)
              End !If job:engineers_notes = ''
          End
          Display()
      End
      Clear(glo:Q_Notes)
      Free(glo:Q_Notes)
      glo:select1 = ''
      
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupInvoiceText, Accepted)
    OF ?tmp:IMEINumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:IMEINumber, Accepted)
      !Get the job
      ?ProcessEstimate{prop:Disable} = 1
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = tmp:JobNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          Error# = 0
          !Is the job already in the list
          Sort(JobNumberQueue,jobque:JobNumber)
          jobque:JobNumber = job:Ref_Number
          Get(JobNumberQueue,jobque:JobNumber)
          If ~Error()
              Case MessageEx('The selected job has already been updated.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              Error# = 1
          End !If ~Error()
      
          If Error# = 0
              If job:Estimate <> 'YES'
                  Case MessageEx('The selected job is not an estimate.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  Error# = 1
              Else !If job:Estimate <> 'YES'
                  If job:Estimate_Ready = 'YES'
                      Case MessageEx('The selected job has already been marked as "Estimate Ready".','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      Error# = 1
                  End !If job:Estimate_Ready = 'YES'
              End !If job:Estimate <> 'YES'
          End !If Error# = 0.
      
          If Error# = 0
              !Is the IMEI number correct?
      
              If job:ESN <> tmp:IMEINumber
                  Case MessageEx('The selected I.M.E.I. Number does not match the selected Job Number.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  Error# = 1
              End !If job:ESN <> tmp:IMEINumber
      
          End !If Error# = 0
      
          If Error# = 0
      
              !Get the VAT RATE from the Trade Account
              tmp:LabourVATRate = 0
              tmp:PartsVATRate = 0
      
              If InvoiceSubAccounts(job:Account_Number)
                  Access:VATCODE.Clearkey(vat:VAT_Code_Key)
                  vat:VAT_Code    = sub:Labour_VAT_Code
                  If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
                      !Found
                      tmp:LabourVATRate   = vat:VAT_Rate
                  Else! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
      
                  Access:VATCODE.Clearkey(vat:VAT_Code_Key)
                  vat:VAT_Code    = sub:Parts_VAT_Code
                  If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
                      !Found
                      tmp:PartsVATRate    = vat:VAT_Rate
                  Else! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
                  
                  
              Else !If InvoiceSubAccounts(job:Account_Number)
                  Access:VATCODE.Clearkey(vat:VAT_Code_Key)
                  vat:VAT_Code    = tra:Labour_VAT_Code
                  If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
                      !Found
                      tmp:LabourVATRate   = vat:VAT_Rate
                  Else! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
      
                  Access:VATCODE.Clearkey(vat:VAT_Code_Key)
                  vat:VAT_Code    = tra:Parts_VAT_Code
                  If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
                      !Found
                      tmp:PartsVATRate    = vat:VAT_Rate
                  Else! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
              End !If InvoiceSubAccounts(job:Account_Number)
      
              job:Repair_Type = tmp:RepairType
      
              Pricing_Routine('E',LabourCost",PartsCost",pass",pass")
              tmp:LabourCost    = LabourCost"
              tmp:PartsCost     = PartsCost"
              tmp:CourierCost    = job:Courier_Cost_Estimate
              tmp:SubTotal    = tmp:LabourCost + tmp:PartsCost + tmp:CourierCost
              tmp:vat         = (tmp:LabourCost * tmp:LabourVATRate/100) + |
                                  (tmp:PartsCost * tmp:PartsVATRate/100) + |
                                  (tmp:CourierCost * tmp:LabourVATRate/100)
              tmp:Total       = tmp:SubTotal + tmp:vat
      
              ?ProcessEstimate{prop:Disable} = 0
          Else
              ?ProcessEstimate{prop:Disable} = 1
              Do ClearWindow
          End !If Error# = 0
          ThisWindow.Reset(1)
      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          Case MessageEx('Cannot find selected Job Number.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      
          Do ClearWindow
      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:IMEINumber, Accepted)
    OF ?ProcessEstimate
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessEstimate, Accepted)
      !Reget the job, just in case
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = tmp:JobNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          If tmp:Location <> ''
              job:Location    = tmp:Location
          End !If tmp:Location <> ''
          job:Repair_Type     = tmp:RepairType
          Pricing_Routine('E',LabourCost",PartsCost",Pass",x")
          If job:Ignore_Estimate_Charges <> 'YES'
              If Pass" = 1
                  job:Labour_Cost_Estimate = LabourCost"
                  job:Parts_Cost_Estimate = PartsCost"
                  job:Sub_Total_Estimate  = job:Labour_Cost_Estimate + job:Parts_Cost_Estimate + job:Courier_Cost_Estimate
              End !If Pass"
          End !If est:Ignore_Estimate_Charges <> 'YES'
      
          Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
          jbn:RefNumber   = job:Ref_Number
          If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
              !Found
              If jbn:Invoice_Text = ''
                  jbn:Invoice_Text    = tmp:InvoiceText
              Else !If jbn:Invoice_Text = ''
                  jbn:Invoice_Text    = Clip(jbn:Invoice_Text) & tmp:InvoiceText
              End !If jbn:Invoice_Text = ''
              Access:JOBNOTES.Update()
          Else! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
      
          job:Estimate_Ready = 'YES'
          GetStatus(520,1,'JOB')
      
          If Access:JOBS.TryUpdate() = Level:Benign
              If Access:AUDIT.PrimeRecord() = Level:Benign
                  aud:Notes         = 'REPAIR TYPE: ' & Clip(tmp:RepairType)
                  If tmp:Location <> ''
                      aud:Notes       = Clip(aud:Notes) & '<13,10>LOCATION: ' & Clip(tmp:Location)
                  End !If tmp:Location <> ''
                  aud:Ref_Number    = job:ref_number
                  aud:Date          = Today()
                  aud:Time          = Clock()
                  aud:Type          = 'JOB'
                  Access:USERS.ClearKey(use:Password_Key)
                  use:Password      = glo:Password
                  Access:USERS.Fetch(use:Password_Key)
                  aud:User          = use:User_Code
                  aud:Action        = 'RAPID ESTIMATE PROCEDURE: ESTIMATE READY'
                  Access:AUDIT.Insert()
              End!If Access:AUDIT.PrimeRecord() = Level:Benign
          End !If Access:JOBS.TryUpdate() = Level:Benign
      
          !Add to the Job Number Queue
          Clear(JobNumberQueue)
          jobque:JobNumber    = job:Ref_Number
          jobque:Order        += 1
          Add(JobNumberQueue)
          Sort(JobNumberQueue,-jobque:Order)
          ?ProcessEstimate{prop:Disable} = 1
      
          Do ClearWindow
      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessEstimate, Accepted)
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      If Records(JobNumberQueue)
          Case MessageEx('Do you wish to print Estimates for the Update Jobs?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
                  Loop x# = 1 To Records(JobNumberQueue)
                      Get(JobNumberQueue,x#)
                      glo:Select1 = jobque:JobNumber
                      Estimate
                      glo:Select1 = ''
                  End !Loop x# = 1 To Records(JobNumberQueue)
              Of 2 ! &No Button
          End!Case MessageEx
      End !Records(JobNumberQueue)
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'RapidEstimateProcedure')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?tmp:RepairType
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:RepairType, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Repair Type')
             Post(Event:Accepted,?LookupRepairType)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupRepairType)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:RepairType, AlertKey)
    END
  OF ?tmp:Location
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Location, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Location')
             Post(Event:Accepted,?LookupLocation)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupLocation)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Location, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

