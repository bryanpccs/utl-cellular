   PROGRAM



   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFILE.INC'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('EQUATES.CLW'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE
INCLUDE('MSGEX.INC')

   MAP
     MODULE('Windows API')
SystemParametersInfo PROCEDURE (LONG uAction, LONG uParam, *? lpvParam, LONG fuWinIni),LONG,RAW,PROC,PASCAL,DLL(TRUE),NAME('SystemParametersInfoA')
     END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('SBRAC002.CLW')
AccessExport           PROCEDURE   !
     END
          MODULE('MsgEx Runtime Library')
            MessageEx(<STRING TheText>,      |
                      <STRING TheTitel>,     |
                      <STRING TheImage>,     |
                      <STRING TheButtons>,   |
                       SIGNED DefButton=0,   |
                       SIGNED EscButton=0,   |
                      <STRING CheckText>,    |
                      <*?     CheckVar>,     |
                      <STRING Fontname>,     |
                      SIGNED  Fontsize=8,    |
                      LONG    Fontcolor=0,   |
                      SIGNED  Fontstyle=0,   |
                      LONG    Charset=0,     |
                      LONG    BackColor=COLOR:NONE, |
                      <STRING Wallpaper>,    |
                      LONG    WallMode=0,    |
                      <STRING Sound>,        |
                      LONG    Options=0,     |
                      SIGNED  MinWidth=0,    |
                      SIGNED  MinHeight=0,   |
                      LONG    TimeOut=0      ),SIGNED,PROC,NAME('MESSAGEEX')
          END
     MODULE('SWiz255.lib')
       SSWizInit(<string>),DLL(dll_mode)
       SpreadWizard(QUEUE,<BYTE CalledFrom>,<LONG SysID>,<STRING ViewClass>,<tqField>,<*CSTRING Sort>,<QUEUE KeyQ>,tqField),LONG,PROC,DLL(dll_mode)
       SpreadStyle(*GROUP StyleSettings),DLL(dll_mode)
       UsBrowse(tqField,STRING ViewClass,<tqField>,<*CSTRING Sort>,<QUEUE KeyQ>,tqField,<*CSTRING SheetDesc>,<*LONG StartRow>,<*LONG StartCol>,<*CSTRING Template>),LONG,PROC,DLL(dll_mode)
       UsGetFileName(*CSTRING f1FileName, BYTE CallFileDialog, BYTE f1Action),BYTE,DLL(dll_mode)
       UsSaveType(*CSTRING f1FileName,LONG F1SS),BYTE,DLL(dll_mode)
       UsInitDoc(tqField,LONG,LONG,LONG,STRING),DLL(dll_mode)
       UsFillRow(tqField,LONG,*USHORT,LONG),DLL(dll_mode)
       BuildSQ(string,tqField,tqField),DLL(dll_mode)
       UsDesigner(LONG F1SS),BYTE,DLL(dll_mode)
       UsDeInit(*CSTRING f1FileName,LONG F1SS, BYTE f1Action),DLL(dll_mode)
     END
     !--------------------------------------------------------------------------
     ! -----------Tintools-------------
     !--------------------------------------------------------------------------
      MODULE('sbrac_VW.CLW')    !REPORT VIEWER
         TINCALENDARSTYLE1(<LONG>),LONG
         TINCALENDARSTYLE2(<STRING>,<LONG>),LONG
         TINCALENDARSTYLE3(<STRING>,<LONG>),LONG
         TINCALENDARSTYLE4(<STRING>,<LONG>),LONG
         TINCALENDARmonyear(<LONG>),LONG
         TINCALENDARmonth(<LONG>),LONG
      END
     !--------------------------------------------------------------------------
     ! -----------Tintools-------------
     !--------------------------------------------------------------------------
        MODULE('CELLMAIN.DLL')
cellmain:Init          PROCEDURE(<ErrorClass curGlobalErrors>, <INIClass curINIMgr>),DLL
cellmain:Kill          PROCEDURE,DLL
        END
   END

SilentRunning        BYTE(0)                         !Set true when application is running in silent mode

USELEVEL             FILE,DRIVER('Btrieve'),PRE(lev),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
User_Level_Key           KEY(lev:User_Level),NOCASE,PRIMARY
Record                   RECORD,PRE()
User_Level                  STRING(30)
                         END
                     END                       

ALLLEVEL             FILE,DRIVER('Btrieve'),PRE(all),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Access_Level_Key         KEY(all:Access_Level),NOCASE,PRIMARY
Record                   RECORD,PRE()
Access_Level                STRING(30)
Description                 STRING(500)
                         END
                     END                       

ACCAREAS             FILE,DRIVER('Btrieve'),PRE(acc),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Access_level_key         KEY(acc:User_Level,acc:Access_Area),NOCASE,PRIMARY
AccessOnlyKey            KEY(acc:Access_Area),DUP,NOCASE
Record                   RECORD,PRE()
Access_Area                 STRING(30)
User_Level                  STRING(30)
DummyField                  STRING(1)
                         END
                     END                       



glo:ErrorText        STRING(1000),EXTERNAL,DLL(_ABCDllMode_)
glo:owner            STRING(20),EXTERNAL,DLL(_ABCDllMode_)
glo:DateModify       DATE,EXTERNAL,DLL(_ABCDllMode_)
glo:TimeModify       TIME,EXTERNAL,DLL(_ABCDllMode_)
glo:Notes_Global     STRING(1000),EXTERNAL,DLL(_ABCDllMode_)
glo:Q_Invoice        QUEUE,PRE(glo),EXTERNAL,DLL(_ABCDllMode_)
Invoice_Number         REAL
Account_Number         STRING(15)
                     END
glo:afe_path         STRING(255),EXTERNAL,DLL(_ABCDllMode_)
glo:Password         STRING(20),EXTERNAL,DLL(_ABCDllMode_)
glo:PassAccount      STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:Preview          STRING('True'),EXTERNAL,DLL(_ABCDllMode_)
glo:q_JobNumber      QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Job_Number_Pointer     REAL
                     END
glo:Q_PartsOrder     QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Q_Order_Number         REAL
Q_Part_Number          STRING(30)
Q_Description          STRING(30)
Q_Supplier             STRING(30)
Q_Quantity             LONG
Q_Purchase_Cost        REAL
Q_Sale_Cost            REAL
                     END
glo:G_Select1        GROUP,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Select1                STRING(30)
Select2                STRING(40)
Select3                STRING(40)
Select4                STRING(40)
Select5                STRING(40)
Select6                STRING(40)
Select7                STRING(40)
Select8                STRING(40)
Select9                STRING(40)
Select10               STRING(40)
Select11               STRING(40)
Select12               STRING(40)
Select13               STRING(40)
Select14               STRING(40)
Select15               STRING(40)
Select16               STRING(40)
Select17               STRING(40)
Select18               STRING(40)
Select19               STRING(40)
Select20               STRING(40)
Select21               STRING(40)
Select22               STRING(40)
Select23               STRING(40)
Select24               STRING(40)
Select25               STRING(40)
Select26               STRING(40)
Select27               STRING(40)
Select28               STRING(40)
Select29               STRING(40)
Select30               STRING(40)
Select31               STRING(40)
Select32               STRING(40)
Select33               STRING(40)
Select34               STRING(40)
Select35               STRING(40)
Select36               STRING(40)
Select37               STRING(40)
Select38               STRING(40)
Select39               STRING(40)
Select40               STRING(40)
Select41               STRING(40)
Select42               STRING(40)
Select43               STRING(40)
Select44               STRING(40)
Select45               STRING(40)
Select46               STRING(40)
Select47               STRING(40)
Select48               STRING(40)
Select49               STRING(40)
Select50               STRING(40)
                     END
glo:q_RepairType     QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Repair_Type_Pointer    STRING(30)
                     END
glo:Q_UnitType       QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Unit_Type_Pointer      STRING(30)
                     END
glo:Q_ChargeType     QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Charge_Type_Pointer    STRING(30)
                     END
glo:Q_ModelNumber    QUEUE,PRE(glo),EXTERNAL,DLL(_ABCDllMode_)
Model_Number_Pointer   STRING(30)
                     END
glo:Q_Accessory      QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Accessory_Pointer      STRING(30)
                     END
glo:Q_AccessLevel    QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Access_Level_Pointer   STRING(30)
                     END
glo:Q_AccessAreas    QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Access_Areas_Pointer   STRING(30)
                     END
glo:Q_Notes          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
notes_pointer          STRING(80)
                     END
glo:EnableFlag       STRING(3),EXTERNAL,DLL(_ABCDllMode_)
glo:TimeLogged       TIME,EXTERNAL,DLL(_ABCDllMode_)
glo:GLO:ApplicationName STRING(8),EXTERNAL,DLL(_ABCDllMode_)
glo:GLO:ApplicationCreationDate LONG,EXTERNAL,DLL(_ABCDllMode_)
glo:GLO:ApplicationCreationTime LONG,EXTERNAL,DLL(_ABCDllMode_)
glo:GLO:ApplicationChangeDate LONG,EXTERNAL,DLL(_ABCDllMode_)
glo:GLO:ApplicationChangeTime LONG,EXTERNAL,DLL(_ABCDllMode_)
glo:GLO:Compiled32   BYTE,EXTERNAL,DLL(_ABCDllMode_)
glo:GLO:AppINIFile   STRING(255),EXTERNAL,DLL(_ABCDllMode_)
glo:Queue            QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer                STRING(40)
                     END
glo:Queue2           QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer2               STRING(40)
                     END
glo:Queue3           QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer3               STRING(40)
                     END
glo:Queue4           QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer4               STRING(40)
                     END
glo:Queue5           QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer5               STRING(40)
                     END
glo:Queue6           QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer6               STRING(40)
                     END
glo:Queue7           QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer7               STRING(40)
                     END
glo:Queue8           QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer8               STRING(40)
                     END
glo:Queue9           QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer9               STRING(40)
                     END
glo:Queue10          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer10              STRING(40)
                     END
glo:Queue11          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer11              STRING(40)
                     END
glo:Queue12          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer12              STRING(40)
                     END
glo:Queue13          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer13              STRING(40)
                     END
glo:Queue14          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer14              STRING(40)
                     END
glo:Queue15          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer15              STRING(40)
                     END
glo:Queue16          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer16              STRING(40)
                     END
glo:Queue17          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer17              STRING(40)
                     END
glo:Queue18          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer18              STRING(40)
                     END
glo:Queue19          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer19              STRING(40)
                     END
glo:Queue20          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer20              STRING(40)
                     END
glo:FaultCode1       STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode2       STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode3       STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode4       STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode5       STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode6       STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode7       STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode8       STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode9       STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode10      STRING(255),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode11      STRING(255),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode12      STRING(255),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode13      STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode14      STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode15      STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode16      STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode17      STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode18      STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode19      STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode20      STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode1  STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode2  STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode3  STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode4  STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode5  STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode6  STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode7  STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode8  STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode9  STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode10 STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode11 STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode12 STRING(30),EXTERNAL,DLL(_ABCDllMode_)
tqField         QUEUE,TYPE
Desc              CSTRING(30)
Name              CSTRING(40)
IsString          BYTE
                END
!--------------------------------------------------------------------------
! -----------Tintools-------------
!--------------------------------------------------------------------------
TinToolsViewRecord  EQUATE(15)
TinToolsCopyRecord  EQUATE(16)
!--------------------------------------------------------------------------
! -----------Tintools-------------
!--------------------------------------------------------------------------
Access:USELEVEL      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:USELEVEL      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:ALLLEVEL      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:ALLLEVEL      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:ACCAREAS      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:ACCAREAS      &RelationManager,EXTERNAL,DLL(dll_mode)
FuzzyMatcher         FuzzyClass
GlobalErrors         ErrorClass
INIMgr               INIClass
GlobalRequest        BYTE,EXTERNAL,DLL(dll_mode)
GlobalResponse       BYTE,EXTERNAL,DLL(dll_mode)
VCRRequest           LONG,EXTERNAL,DLL(dll_mode)
lCurrentFDSetting    LONG
lAdjFDSetting        LONG

  CODE
  GlobalErrors.Init
  FuzzyMatcher.Init
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)
  INIMgr.Init('sbraccex.INI')
  SSWizInit('sbraccex.tps')
  SystemParametersInfo (38, 0, lCurrentFDSetting, 0)
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 0, lAdjFDSetting, 3)
  END
  cellmain:Init(GlobalErrors, INIMgr)                 ! Initialise dll (ABC)
  AccessExport
  INIMgr.Update
  cellmain:Kill()
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 1, lAdjFDSetting, 3)
  END
  INIMgr.Kill
  FuzzyMatcher.Kill
  GlobalErrors.Kill


