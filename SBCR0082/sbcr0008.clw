

   MEMBER('sbcr0082.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBCR0008.INC'),ONCE        !Local module procedure declarations
                     END









JobReport PROCEDURE(Date_Start,Date_End)
Total                LONG
Job_Type_String      STRING(50)
temp_string          STRING(255)
x                    SHORT
y                    SHORT
z                    SHORT
sub_total            LONG
Date_Temp            DATE
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:Current_Status)
                       PROJECT(job:ESN)
                       PROJECT(job:Mobile_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:date_booked)
                       JOIN(aud:Ref_Number_Key,job:Ref_Number)
                       END
                     END
Report               REPORT,AT(396,521,7521,10240),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
Detail                 DETAIL,AT(,,,167),USE(?unnamed:5)
                         STRING(@s8),AT(104,0,573,208),USE(job:Ref_Number),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@d17),AT(646,0,781,208),USE(job:date_booked),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s25),AT(1406,0,1667,208),USE(Job_Type_String),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s13),AT(3115,0,781,208),USE(job:Mobile_Number),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s16),AT(4010,0,1094,208),USE(job:ESN),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(5167,0,2292,208),USE(job:Current_Status),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                       END
Warrenty_Title         DETAIL,AT(,,,313),USE(?W_Title)
                         LINE,AT(73,21,7083,0),USE(?Line1:6),COLOR(COLOR:Black)
                         STRING('Job No'),AT(104,63,573,208),USE(?Jno_String),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Booked'),AT(646,63,781,208),USE(?DB_String),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Job Type'),AT(1417,63,677,208),USE(?Type_String),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Mobile No'),AT(3115,63,781,208),USE(?MN_String),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('IMEI Number'),AT(4010,63,1094,208),USE(?I_String),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Current Status'),AT(5167,63,1510,208),USE(?S_String),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         LINE,AT(73,240,7083,0),USE(?Line1:5),COLOR(COLOR:Black)
                       END
Final_Total            DETAIL,AT(,,,552),USE(?unnamed:6)
                         STRING('Total Jobs Booked:'),AT(3240,125,1719,208),USE(?String38),TRN,RIGHT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@n-14),AT(5052,125,1563,156),USE(Total),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         LINE,AT(115,365,7083,0),USE(?Line1:3),COLOR(COLOR:Black),LINEWIDTH(4)
                         LINE,AT(125,42,7083,0),USE(?Line1:4),COLOR(COLOR:Black),LINEWIDTH(4)
                       END
FrontPageHead          DETAIL,AT(,,,1979),USE(?unnamed:7)
                         BOX,AT(3250,521,3958,1094),USE(?Box1),COLOR(COLOR:Black),FILL(0DEDEDEH)
                         STRING(@s30),AT(146,10),USE(def:User_Name),TRN,FONT('Arial',16,,FONT:bold+FONT:underline,CHARSET:ANSI)
                         STRING('STATUS REPORT'),AT(4698,10,2646,240),USE(?String46),TRN,RIGHT,FONT('Arial',16,,FONT:bold+FONT:underline,CHARSET:ANSI)
                         STRING(@s30),AT(156,344),USE(def:Address_Line1),TRN,FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                         STRING('Customer: '),AT(3865,573),USE(?String47),TRN,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(4604,583),USE(sub:Company_Name),TRN,FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(156,563),USE(def:Address_Line2),TRN,FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                         STRING('Jobs Booked From:'),AT(3354,1042),USE(?String49),TRN,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING(@D17),AT(4604,1042),USE(date_start),TRN,FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(156,802),USE(def:Address_Line3),TRN,FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                         STRING('Jobs Booked To:'),AT(3385,1250,1146,208),USE(?String50),TRN,RIGHT,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING(@D17),AT(4604,1250),USE(date_end),TRN,FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(156,1042),USE(def:Postcode),TRN,FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                         STRING('Report Date:'),AT(3771,823),USE(?String51),TRN,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING(@D17),AT(4604,823),USE(ReportRunDate,,?ReportRunDate:2),TRN,FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                         STRING(@T7),AT(5604,823),USE(ReportRunTime,,?ReportRunTime:2),TRN,FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(604,1271),USE(def:Telephone_Number),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax: '),AT(156,1490,302,188),USE(?String44),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(604,1490),USE(def:Fax_Number),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s255),AT(604,1719,6667,208),USE(def:EmailAddress),TRN,FONT('Arial',8,,FONT:bold)
                         STRING('E-mail:'),AT(156,1708,469,208),USE(?String35),TRN,FONT('Arial',8,,)
                         STRING('Tel: '),AT(156,1271,260,188),USE(?String43),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                       END
                       FORM,AT(396,479,7521,10802),USE(?unnamed)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('JobReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  PreviewReq = True
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Access:AUDIT.UseFile
  Access:JOBSE.UseFile
  Access:SUBTRACC.UseFile
  
  
  RecordsToProcess = BYTES(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      DO OpenReportRoutine
    OF Event:Timer
        SET(defaults)
        access:defaults.next()
        
        total = 0
        sub_total = 0
        PRINT(rpt:FrontPageHead)
        PRINT(RPT:Warrenty_Title)
        
        access:jobs.clearkey(job:Date_Booked_Key)
        job:date_booked = Date_Start
        SET(job:Date_Booked_Key,job:Date_Booked_Key)
        LOOP UNTIL access:jobs.next() <> Level:Benign
            IF (job:date_booked > date_end) THEN
                BREAK
            END
            IF (job:Account_Number <> sub:account_number) THEN
                CYCLE
            END
            IF (job:Warranty_Job = 'YES') THEN
                Job_type_string = 'WARRANTY'
            END
            IF (job:Chargeable_Job = 'YES') THEN
                Job_type_String = 'CHARGEABLE'
            END
            IF (job:Insurance = 'YES') THEN
                job_type_String = 'INSURANCE'
            END
            IF (job:Warranty_Job = 'YES' and job:Chargeable_Job ='YES') THEN
                job_type_string = 'WARRANTY/CHARGEABLE'
            END
            IF (job:Insurance = 'YES' and job:Chargeable_Job = 'YES') THEN
                job_type_string = 'INSURANCE/CHARGEABLE'
            END
            Access:jobse.clearkey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            access:jobse.fetch(jobe:RefNumberKey)
            IF (jobe:JobReceived = false) THEN
                print(RPT:detail) !Print records
                total = total + 1
            END
        END
        
        PRINT(RPT:Final_Total) !Print records
        
        !final lines
        LocalResponse = RequestCompleted
        BREAK
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='JobReport'
  END
  Report{Prop:Preview} = PrintPreviewImage







