

   MEMBER('sbcr0082.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBCR0001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SBCR0002.INC'),ONCE        !Req'd for module callout resolution
                     END


JobStatusReport PROCEDURE                             !Generated from procedure template - Window

StartDate            DATE
EndDate              DATE
CountBack            LONG
TempDate             DATE
SavePath             STRING(255)
RecordCount          LONG
job_type_string      STRING(50)
LOC_GROUP            GROUP,PRE(loc)
ApplicationName      STRING(30)
ProgramName          STRING(30)
UserName             STRING(61)
Filename             STRING(255)
                     END
FoundCount           LONG
Account_Number       STRING(15)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?sub:Account_Number
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:RecordNumber)
                     END
CSVFILE     FILE,DRIVER('BASIC','/ALWAYSQUOTE=ON'),PRE(csv),NAME(Out_Filename),CREATE,BINDABLE,THREAD
Record          RECORD
job_no                  STRING(50)       !
date_booked             STRING(50)       !
job_type                STRING(50)       !
mobile_no               STRING(50)       !
imei_number             STRING(50)       !
current_status          STRING(50)       !
                END
              END

    MAP
ExportCSV       PROCEDURE(Date, Date),Long
CancelCheck     PROCEDURE(),Long
    END
window               WINDOW('Jobs Status Report'),AT(,,316,226),FONT('Tahoma',8,,),CENTER,ICON('pc.ico'),TIMER(1),GRAY,DOUBLE,IMM
                       SHEET,AT(4,2,308,190),USE(?Sheet1),WIZARD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Account'),AT(16,16),USE(?Prompt3)
                           COMBO(@s15),AT(52,16,124,10),USE(sub:Account_Number),IMM,FORMAT('60L|M~Account Number~L(2)@s15@'),DROP(5),FROM(Queue:FileDropCombo)
                           GROUP('Start Date'),AT(52,36,244,60),USE(?Group1),BOXED
                             BUTTON('Today'),AT(176,44,108,8),USE(?StartTodayButton)
                             BUTTON('Back'),AT(176,56,36,8),USE(?StartBack7)
                             PROMPT('7 days'),AT(214,56),USE(?Prompt4),TRN,FONT(,,,FONT:bold)
                             BUTTON('Forward'),AT(248,56,36,8),USE(?StartFor7Button)
                             ENTRY(@D7),AT(64,76,90,10),USE(StartDate),COLOR(080FFFFH)
                             BUTTON('Back'),AT(176,68,36,8),USE(?StartBack30Button)
                             PROMPT('30 days'),AT(214,68),USE(?Prompt5),TRN,FONT(,,,FONT:bold)
                             BUTTON('Forward'),AT(248,68,36,8),USE(?StartFor30Button)
                             BUTTON('Back'),AT(176,80,36,8),USE(?StartBack365Button)
                             PROMPT('365 days'),AT(214,80),USE(?Prompt6),TRN,FONT(,,,FONT:bold)
                             BUTTON('Forward'),AT(248,80,36,8),USE(?StartFor365Button)
                           END
                           GROUP('End Date'),AT(52,104,244,60),USE(?Group2),BOXED
                             BUTTON('Today'),AT(176,112,108,8),USE(?EndTodayButton)
                             BUTTON('Back'),AT(176,124,36,8),USE(?EndBack7Button)
                             PROMPT('7 Days'),AT(214,124),USE(?Prompt7),TRN,FONT(,,,FONT:bold)
                             BUTTON('Forward'),AT(248,124,36,8),USE(?EndFor7Button)
                             BUTTON('Back'),AT(176,136,36,8),USE(?EndBack30Button)
                             PROMPT('30 days'),AT(214,136),USE(?Prompt8),TRN,FONT(,,,FONT:bold)
                             BUTTON('Forward'),AT(248,136,36,8),USE(?EndFor30Button)
                             ENTRY(@D7),AT(64,144,90,10),USE(EndDate),COLOR(080FFFFH)
                             BUTTON('Back'),AT(176,148,36,8),USE(?EndBack365Button)
                             PROMPT('365 days'),AT(214,148),USE(?Prompt9),TRN,FONT(,,,FONT:bold)
                             BUTTON('Forward'),AT(248,148,36,8),USE(?EndFor365Button)
                           END
                           PROMPT('Record No.'),AT(20,172),USE(?RecordCountPrompt),TRN,HIDE
                           STRING(@n8),AT(64,172,32,),USE(RecordCount),TRN,HIDE,RIGHT
                         END
                       END
                       PANEL,AT(4,196,308,28),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('E&xport'),AT(188,200,56,16),USE(?ExportButton),FLAT,LEFT,ICON('ok.ico'),DEFAULT
                       BUTTON('&Cancel'),AT(248,200,56,16),USE(?CancelButton),FLAT,LEFT,ICON('cancel.ico')
                       BUTTON('&Print'),AT(128,200,56,16),USE(?PrintButton),FLAT,LEFT,ICON(ICON:Print)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Open                   PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    If ?sub:Account_Number{prop:ReadOnly} = True
        ?sub:Account_Number{prop:FontColor} = 65793
        ?sub:Account_Number{prop:Color} = 15066597
    Elsif ?sub:Account_Number{prop:Req} = True
        ?sub:Account_Number{prop:FontColor} = 65793
        ?sub:Account_Number{prop:Color} = 8454143
    Else ! If ?sub:Account_Number{prop:Req} = True
        ?sub:Account_Number{prop:FontColor} = 65793
        ?sub:Account_Number{prop:Color} = 16777215
    End ! If ?sub:Account_Number{prop:Req} = True
    ?sub:Account_Number{prop:Trn} = 0
    ?sub:Account_Number{prop:FontStyle} = font:Bold
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    If ?StartDate{prop:ReadOnly} = True
        ?StartDate{prop:FontColor} = 65793
        ?StartDate{prop:Color} = 15066597
    Elsif ?StartDate{prop:Req} = True
        ?StartDate{prop:FontColor} = 65793
        ?StartDate{prop:Color} = 8454143
    Else ! If ?StartDate{prop:Req} = True
        ?StartDate{prop:FontColor} = 65793
        ?StartDate{prop:Color} = 16777215
    End ! If ?StartDate{prop:Req} = True
    ?StartDate{prop:Trn} = 0
    ?StartDate{prop:FontStyle} = font:Bold
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?Group2{prop:Font,3} = -1
    ?Group2{prop:Color} = 15066597
    ?Group2{prop:Trn} = 0
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    If ?EndDate{prop:ReadOnly} = True
        ?EndDate{prop:FontColor} = 65793
        ?EndDate{prop:Color} = 15066597
    Elsif ?EndDate{prop:Req} = True
        ?EndDate{prop:FontColor} = 65793
        ?EndDate{prop:Color} = 8454143
    Else ! If ?EndDate{prop:Req} = True
        ?EndDate{prop:FontColor} = 65793
        ?EndDate{prop:Color} = 16777215
    End ! If ?EndDate{prop:Req} = True
    ?EndDate{prop:Trn} = 0
    ?EndDate{prop:FontStyle} = font:Bold
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    ?RecordCountPrompt{prop:FontColor} = -1
    ?RecordCountPrompt{prop:Color} = 15066597
    ?RecordCount{prop:FontColor} = -1
    ?RecordCount{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE

    CommandLine = CLIP(COMMAND(''))

    tmpPos = INSTRING('%', CommandLine)
    IF (NOT tmpPos) THEN
        MessageEx('Attempting to use ' & CLIP(LOC:ProgramName) & '<10,13>'           & |
            '   without using ' & CLIP(LOC:ApplicationName) & '.<10,13>'                        & |
            '   Start ' & CLIP(LOC:ApplicationName) & ' and run the report from there.<10,13>',   |
            CLIP(LOC:ApplicationName),                                                            |
            'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
       POST(Event:CloseWindow)
       EXIT
    END

    SET(USERS)
    Access:USERS.Clearkey(use:Password_Key)
    glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
    Access:USERS.Clearkey(use:Password_Key)
    use:Password = glo:Password
    IF (Access:USERS.Tryfetch(use:Password_Key) <> Level:benign) THEN
        MessageEx('Unable to find your logged in user details.', |
                CLIP(LOC:ApplicationName),                                  |
                'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
        POST(Event:CloseWindow)
        EXIT
    END

    IF (CLIP(use:Forename) = '') THEN
        LOC:UserName = use:Surname
    ELSE
        LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname
    END

    IF (CLIP(LOC:UserName) = '') THEN
        LOC:UserName = '<' & use:User_Code & '>'
    END

    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('JobStatusReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt3
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:JOBS.Open
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  FDCB2.Init(sub:Account_Number,?sub:Account_Number,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:SUBTRACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder()
  FDCB2.AddField(sub:Account_Number,FDCB2.Q.sub:Account_Number)
  FDCB2.AddField(sub:RecordNumber,FDCB2.Q.sub:RecordNumber)
  FDCB2.AddUpdateField(sub:Account_Number,Account_Number)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Open PROCEDURE

  CODE
  SET(defaults)
  access:defaults.next()
  
  EndDate = TODAY()
  StartDate = EndDate - 30
  
  LOC:ApplicationName = 'ServiceBase 2000'
  LOC:ProgramName = 'Replaced Parts Data Export'
  LOC:UserName = ''
  
  DO GetUserName
    
  DISPLAY
  PARENT.Open


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?StartTodayButton
      ThisWindow.Update
      StartDate=today()
      DISPLAY(?StartDate)
    OF ?StartBack7
      ThisWindow.Update
      IF (StartDate>0) THEN
          StartDate -= 7
          DISPLAY(?StartDate)
      END
    OF ?StartFor7Button
      ThisWindow.Update
      IF (StartDate>0) THEN
          StartDate +=7
          DISPLAY(?StartDate)
      END
    OF ?StartBack30Button
      ThisWindow.Update
      IF (StartDate>0) THEN
          StartDate -= 30
          DISPLAY(?StartDate)
      END
    OF ?StartFor30Button
      ThisWindow.Update
      IF (StartDate>0) THEN
          StartDate += 30
          DISPLAY(?StartDate)
      END
    OF ?StartBack365Button
      ThisWindow.Update
      IF (StartDate>0) THEN
          StartDate -= 365
          DISPLAY(?StartDate)
      END
    OF ?StartFor365Button
      ThisWindow.Update
      IF (StartDate>0) THEN
          StartDate += 365
          DISPLAY(?StartDate)
      END
    OF ?EndTodayButton
      ThisWindow.Update
      EndDate=today()
      DISPLAY(?EndDate)
    OF ?EndBack7Button
      ThisWindow.Update
      IF (EndDate>0) THEN
          EndDate -= 7
          DISPLAY(?EndDate)
      END
    OF ?EndFor7Button
      ThisWindow.Update
      IF (EndDate>0) THEN
          EndDate += 7
          DISPLAY(?EndDate)
      END
    OF ?EndBack30Button
      ThisWindow.Update
      IF (EndDate>0) THEN
          EndDate -= 30
          DISPLAY(?EndDate)
      END
    OF ?EndFor30Button
      ThisWindow.Update
      IF (EndDate>0) THEN
          EndDate += 30
          DISPLAY(?EndDate)
      END
    OF ?EndBack365Button
      ThisWindow.Update
      IF (EndDate>0) THEN
          EndDate -= 365
          DISPLAY(?EndDate)
      END
    OF ?EndFor365Button
      ThisWindow.Update
      IF (EndDate>0) THEN
          EndDate += 365
          DISPLAY(?EndDate)
      END
    OF ?ExportButton
      ThisWindow.Update
      savepath = PATH()
      IF (EndDate < StartDate) THEN
          MessageEx('End date is less than start date.', LOC:ApplicationName,|
                          'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                          beep:systemhand,msgex:samewidths,84,26,0)
      ELSE
          Out_Filename =  Clip(def:exportpath) & '\jobstatus.CSV'
      
          IF (NOT FILEDIALOG('Choose File',Out_Filename,'CSV Files|*.csv|All Files|*.*', |
                                          file:keepdir + file:save + file:longname)) THEN
              SETPATH(savepath)
          ELSE
              REMOVE(CSVFile)
              CREATE(CSVFile)
              IF (ERRORCODE() <> Level:benign) THEN
                  MessageEx('Cannot create CSV export file.', LOC:ApplicationName,|
                          'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                          beep:systemhand,msgex:samewidths,84,26,0)
              ELSE
                  OPEN(CSVFile)
                  SETCURSOR(cursor:wait)
                  IF (ExportCSV(StartDate, EndDate) = 1) THEN
                      SETCURSOR()
                      CLOSE(CSVFILE)
                      REMOVE(CSVFILE)
                  ELSE
                      SETCURSOR()
                      CLOSE(CSVFILE)
                      MESSAGE('End Of Report')
                      POST(Event:CloseWindow)
                  END
              END
          END
      END
      
      
      
    OF ?CancelButton
      ThisWindow.Update
      POST(Event:CloseWindow)
    OF ?PrintButton
      ThisWindow.Update
      savepath = PATH()
      IF (EndDate < StartDate) THEN
          MessageEx('End date is less than start date.', LOC:ApplicationName,|
                          'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                          beep:systemhand,msgex:samewidths,84,26,0)
      ELSE
          JobReport(StartDate, EndDate)
          POST(Event:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

ExportCSV   PROCEDURE(IN:StartDate, IN:EndDate)
    CODE
    CLEAR(CSVFILE)
    csv:job_no = CLIP(sub:company_name)
    csv:date_booked = CLIP(sub:account_number)
    csv:job_type = FORMAT(TODAY(), @d6)
    csv:mobile_no = FORMAT(CLOCK(), @t1)
    csv:imei_number = FORMAT(IN:StartDate, @d6)
    csv:current_status = FORMAT(IN:EndDate, @d6)
    ADD(CSVFILE)

    CLEAR(CSVFILE)
    csv:job_no = 'Job No'
    csv:date_booked = 'Date Booked'
    csv:job_type = 'Job Type'
    csv:mobile_no = 'Mobile No'
    csv:imei_number = 'IMEI Number'
    csv:current_status = 'Current Status'
    ADD(CSVFILE)

    RecordCount = 0
    tempCount# = 0
    UNHIDE(?RecordCountPrompt)
    UNHIDE(?RecordCount)
    DISPLAY()

    access:jobs.clearkey(job:Date_Booked_Key)
    job:date_booked = IN:StartDate
    SET(job:Date_Booked_Key,job:Date_Booked_Key)
    LOOP UNTIL access:jobs.next() <> Level:Benign
        IF (job:date_booked > IN:EndDate) THEN
            BREAK
        END
        TempCount# += 1
        IF (TempCount# > 30) THEN
            RecordCount += TempCount#
            TempCount# = 0
            DISPLAY(?RecordCount)
            IF (CancelCheck() <> 0) THEN
                HIDE(?RecordCountPrompt)
                HIDE(?RecordCount)
                DISPLAY()
                RETURN(1)
            END
        END
        IF (job:Account_Number <> sub:account_number) THEN
            CYCLE
        END
        IF (job:Warranty_Job = 'YES') THEN
            Job_type_string = 'WARRANTY'
        END
        IF (job:Chargeable_Job = 'YES') THEN
            Job_type_String = 'CHARGEABLE'
        END
        IF (job:Insurance = 'YES') THEN
            job_type_String = 'INSURANCE'
        END
        IF (job:Warranty_Job = 'YES' and job:Chargeable_Job ='YES') THEN
            job_type_string = 'WARRANTY/CHARGEABLE'
        END
        IF (job:Insurance = 'YES' and job:Chargeable_Job = 'YES') THEN
            job_type_string = 'INSURANCE/CHARGEABLE'
        END
        Access:jobse.clearkey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        access:jobse.fetch(jobe:RefNumberKey)
        IF (jobe:JobReceived = false) THEN
            CLEAR(CSVFILE)
            csv:job_no = CLIP(job:ref_number)
            csv:date_booked = CLIP(job:date_booked)
            csv:job_type = CLIP(job_type_string)
            csv:mobile_no = CLIP(job:mobile_number)
            csv:imei_number = CLIP(job:esn)
            csv:current_status = CLIP(job:current_status)
            ADD(CSVFILE)
        END
    END

    RETURN(0)
CancelCheck                    PROCEDURE
    CODE
    cancel# = 0
    ACCEPT
        CASE Event()
            OF Event:Timer
                Break
            OF Event:CloseWindow
                cancel# = 1
                Break
            OF Event:accepted
                IF (Field() = ?CancelButton) THEN
                    cancel# = 1
                    BREAK
                END
        END
    END

    IF (cancel# = 1) THEN
        !BEEP(BEEP:SystemAsterisk)  ;  YIELD()
        SETCURSOR()
        CASE MessageEx('Are you sure you want to cancel?',LOC:ApplicationName,|
                       'Styles\question.ico','&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                       beep:systemquestion,msgex:samewidths,84,26,0)
            OF 1 ! &Yes Button
                RETURN(1)
            OF 2 ! &No Button
                SETCURSOR(CURSOR:Wait)
        END
    END

    RETURN(0)
