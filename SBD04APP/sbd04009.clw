

   MEMBER('sbd04app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD04009.INC'),ONCE        !Local module procedure declarations
                     END








SDSLabelInd_Thermal_9X PROCEDURE(func:JobNumber)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:CompanyName      STRING(30)
tmp:AddressLine1     STRING(30)
tmp:AddressLine2     STRING(30)
tmp:AddressLine3     STRING(30)
tmp:Postcode         STRING(30)
tmp:TelephoneNumber  STRING(30)
tmp:FaxNumber        STRING(30)
tmp:JobNumberList    STRING(255)
tmp:printedby        STRING(60)
tmp:DefaultTelephone STRING(30)
tmp:DefaultFax       STRING(30)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS_ALIAS)
                     END
REPORT               REPORT,AT(500,250,6802,3750),PAPER(PAPER:USER,7300,4000),PRE(RPT),FONT('Arial',10,,),THOUS
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(10,10,1854,3115),USE(?Label)
                           STRING(@s30),AT(52,198,1771,208),USE(def:User_Name),FONT(,8,,,CHARSET:ANSI)
                           STRING(@s30),AT(63,354,1771,208),USE(def:Address_Line1),FONT(,8,,,CHARSET:ANSI)
                           STRING(@s30),AT(63,510,1771,208),USE(def:Address_Line2),FONT(,8,,,CHARSET:ANSI)
                           STRING(@s30),AT(63,667,1771,208),USE(def:Address_Line3),FONT(,8,,,CHARSET:ANSI)
                           STRING(@s15),AT(63,823),USE(def:Postcode),FONT(,8,,,CHARSET:ANSI)
                           STRING('TO'),AT(52,1188),USE(?String12),TRN,FONT(,,,FONT:bold)
                           STRING(@s30),AT(63,1354,1771,208),USE(tmp:CompanyName),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(63,1510,1771,208),USE(tmp:AddressLine1),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(63,1667,1771,208),USE(tmp:AddressLine2),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(63,1823,1771,208),USE(tmp:AddressLine3),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(63,1979,1771,208),USE(tmp:Postcode),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           TEXT,AT(52,2188,1771,885),USE(tmp:JobNumberList),TRN,FONT('Arial',9,,)
                           STRING('FROM'),AT(52,10),USE(?String11),TRN,FONT(,,,FONT:bold)
                         END
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('SDSLabelInd_Thermal_9X')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:STANTEXT.Open
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS_ALIAS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS_ALIAS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      DO OpenReportRoutine
    OF Event:Timer
        Access:JOBS_ALIAS.Clearkey(job_ali:Ref_Number_Key)
        job_ali:Ref_Number  = func:JobNumber
        If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
            !Found
            tmp:JobNumberList = Clip(job_ali:Ref_Number)
            tmp:CompanyName    = job_ali:Company_Name_Delivery
            tmp:AddressLine1    = job_ali:Address_Line1_Delivery
            tmp:AddressLine2    = job_ali:Address_Line2_Delivery
            tmp:AddressLine3    = job_ali:Address_Line3_Delivery
            tmp:Postcode        = job_ali:Postcode_Delivery
            tmp:TelephoneNumber = job_ali:Telephone_Delivery
        Else! If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
        
        Print(rpt:Detail)
        
        
        LocalResponse = RequestCompleted
        BREAK
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS_ALIAS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(REPORT)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        REPORT{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),REPORT,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = REPORT{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,REPORT{PROPPRINT:Copies})
          REPORT{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND REPORT{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR REPORT{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(REPORT)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = REPORT{PROPPRINT:Copies}
        IF REPORT{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,REPORT{PROPPRINT:Copies})
        END
        REPORT{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(REPORT)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBS_ALIAS.Close
    Relate:STANTEXT.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','LABEL - SDS')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'LABEL - SDS'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(REPORT)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> REPORT{PROPPRINT:Copies}
    REPORT{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = REPORT{PROPPRINT:Copies}
  IF REPORT{PROPPRINT:COLLATE}=True
    REPORT{PROPPRINT:Copies} = 1
  END
  REPORT$?Label{PROP:MinHeight} = REPORT$?Label{PROP:Height}
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'LABEL - SDS'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  IF REPORT{PROP:TEXT}=''
    REPORT{PROP:TEXT}='SDSLabelInd_Thermal_9X'
  END
  IF PreviewReq = True OR (REPORT{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR REPORT{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    REPORT{Prop:Preview} = PrintPreviewImage
  END







