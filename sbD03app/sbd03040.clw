

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03040.INC'),ONCE        !Local module procedure declarations
                     END


Stock_Check_Criteria PROCEDURE                        !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::4:TAGFLAG          BYTE(0)
DASBRW::4:TAGMOUSE         BYTE(0)
DASBRW::4:TAGDISPSTATUS    BYTE(0)
DASBRW::4:QUEUE           QUEUE
Pointer                       LIKE(GLO:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
Location_Temp        STRING(30)
tag_temp             STRING(1)
tmp:showquantity     BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select1
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?GLO:Select4
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
FDCB8::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
BRW3::View:Browse    VIEW(LOCSHELF)
                       PROJECT(los:Shelf_Location)
                       PROJECT(los:Site_Location)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
los:Shelf_Location     LIKE(los:Shelf_Location)       !List box control field - type derived from field
los:Site_Location      LIKE(los:Site_Location)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Stock Check Report'),AT(,,215,371),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,208,336),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Site Location'),AT(8,20),USE(?Prompt1)
                           COMBO(@s30),AT(84,20,124,10),USE(GLO:Select1),IMM,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)|M*@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Manufacturer'),AT(8,196),USE(?Prompt1:2)
                           LIST,AT(32,40,148,128),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Shelf Location~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Tag'),AT(32,172,48,16),USE(?DASTAG),LEFT,ICON('tag.gif')
                           BUTTON('&Rev tags'),AT(88,92,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('Tag &All'),AT(80,172,48,16),USE(?DASTAGAll),LEFT,ICON('tagall.gif')
                           BUTTON('sho&W tags'),AT(88,108,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON('Untag &All'),AT(128,172,48,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                           COMBO(@s40),AT(84,196,124,10),USE(GLO:Select4),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                           CHECK('Include Accessories In Report'),AT(84,212),USE(GLO:Select2),VALUE('YES','NO')
                           CHECK('Suppress Zeros'),AT(84,224),USE(GLO:Select3),VALUE('YES','NO')
                           CHECK('Show Stock Quantity'),AT(84,236),USE(GLO:Select6),VALUE('1','0')
                           GROUP('Usage Date Range'),AT(8,252,200,44),USE(?Group1),BOXED
                             PROMPT('Start Date'),AT(16,264),USE(?GLO:Select7:Prompt)
                             ENTRY(@d6b),AT(84,264,64,10),USE(GLO:Select7),FONT(,,,FONT:bold,CHARSET:ANSI)
                             BUTTON,AT(152,264,10,10),USE(?LookupStartDate),SKIP,ICON('List3.ico')
                             PROMPT('End Date'),AT(16,280),USE(?GLO:Select8:Prompt)
                             ENTRY(@d6b),AT(84,280,64,10),USE(GLO:Select8),FONT(,,,FONT:bold,CHARSET:ANSI)
                             BUTTON,AT(152,280,10,10),USE(?LookupEndDate),SKIP,ICON('List3.ico')
                           END
                           OPTION('Report Order'),AT(8,304,200,28),USE(GLO:Select5),BOXED
                             RADIO('By Location'),AT(34,316),USE(?glo:select5:Radio1),VALUE('1')
                             RADIO('By Description'),AT(118,316),USE(?glo:select5:Radio2),VALUE('2')
                           END
                         END
                       END
                       PANEL,AT(4,344,208,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(96,348,56,16),USE(?OkButton),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Close'),AT(152,348,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

BRW3                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW3::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?GLO:Select1{prop:ReadOnly} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 15066597
    Elsif ?GLO:Select1{prop:Req} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 8454143
    Else ! If ?GLO:Select1{prop:Req} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 16777215
    End ! If ?GLO:Select1{prop:Req} = True
    ?GLO:Select1{prop:Trn} = 0
    ?GLO:Select1{prop:FontStyle} = font:Bold
    ?Prompt1:2{prop:FontColor} = -1
    ?Prompt1:2{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    If ?GLO:Select4{prop:ReadOnly} = True
        ?GLO:Select4{prop:FontColor} = 65793
        ?GLO:Select4{prop:Color} = 15066597
    Elsif ?GLO:Select4{prop:Req} = True
        ?GLO:Select4{prop:FontColor} = 65793
        ?GLO:Select4{prop:Color} = 8454143
    Else ! If ?GLO:Select4{prop:Req} = True
        ?GLO:Select4{prop:FontColor} = 65793
        ?GLO:Select4{prop:Color} = 16777215
    End ! If ?GLO:Select4{prop:Req} = True
    ?GLO:Select4{prop:Trn} = 0
    ?GLO:Select4{prop:FontStyle} = font:Bold
    ?GLO:Select2{prop:Font,3} = -1
    ?GLO:Select2{prop:Color} = 15066597
    ?GLO:Select2{prop:Trn} = 0
    ?GLO:Select3{prop:Font,3} = -1
    ?GLO:Select3{prop:Color} = 15066597
    ?GLO:Select3{prop:Trn} = 0
    ?GLO:Select6{prop:Font,3} = -1
    ?GLO:Select6{prop:Color} = 15066597
    ?GLO:Select6{prop:Trn} = 0
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?GLO:Select7:Prompt{prop:FontColor} = -1
    ?GLO:Select7:Prompt{prop:Color} = 15066597
    If ?GLO:Select7{prop:ReadOnly} = True
        ?GLO:Select7{prop:FontColor} = 65793
        ?GLO:Select7{prop:Color} = 15066597
    Elsif ?GLO:Select7{prop:Req} = True
        ?GLO:Select7{prop:FontColor} = 65793
        ?GLO:Select7{prop:Color} = 8454143
    Else ! If ?GLO:Select7{prop:Req} = True
        ?GLO:Select7{prop:FontColor} = 65793
        ?GLO:Select7{prop:Color} = 16777215
    End ! If ?GLO:Select7{prop:Req} = True
    ?GLO:Select7{prop:Trn} = 0
    ?GLO:Select7{prop:FontStyle} = font:Bold
    ?GLO:Select8:Prompt{prop:FontColor} = -1
    ?GLO:Select8:Prompt{prop:Color} = 15066597
    If ?GLO:Select8{prop:ReadOnly} = True
        ?GLO:Select8{prop:FontColor} = 65793
        ?GLO:Select8{prop:Color} = 15066597
    Elsif ?GLO:Select8{prop:Req} = True
        ?GLO:Select8{prop:FontColor} = 65793
        ?GLO:Select8{prop:Color} = 8454143
    Else ! If ?GLO:Select8{prop:Req} = True
        ?GLO:Select8{prop:FontColor} = 65793
        ?GLO:Select8{prop:Color} = 16777215
    End ! If ?GLO:Select8{prop:Req} = True
    ?GLO:Select8{prop:Trn} = 0
    ?GLO:Select8{prop:FontStyle} = font:Bold
    ?GLO:Select5{prop:Font,3} = -1
    ?GLO:Select5{prop:Color} = 15066597
    ?GLO:Select5{prop:Trn} = 0
    ?glo:select5:Radio1{prop:Font,3} = -1
    ?glo:select5:Radio1{prop:Color} = 15066597
    ?glo:select5:Radio1{prop:Trn} = 0
    ?glo:select5:Radio2{prop:Font,3} = -1
    ?glo:select5:Radio2{prop:Color} = 15066597
    ?glo:select5:Radio2{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::4:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW3.UpdateBuffer
   GLO:Queue.Pointer = los:Shelf_Location
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = los:Shelf_Location
     ADD(GLO:Queue,GLO:Queue.Pointer)
    tag_temp = '*'
  ELSE
    DELETE(GLO:Queue)
    tag_temp = ''
  END
    Queue:Browse.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse.tag_temp_Icon = 2
  ELSE
    Queue:Browse.tag_temp_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW3.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = los:Shelf_Location
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW3.Reset
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::4:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::4:QUEUE = GLO:Queue
    ADD(DASBRW::4:QUEUE)
  END
  FREE(GLO:Queue)
  BRW3.Reset
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::4:QUEUE.Pointer = los:Shelf_Location
     GET(DASBRW::4:QUEUE,DASBRW::4:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = los:Shelf_Location
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASSHOWTAG Routine
   CASE DASBRW::4:TAGDISPSTATUS
   OF 0
      DASBRW::4:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::4:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::4:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW3.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Stock_Check_Criteria',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Location_Temp',Location_Temp,'Stock_Check_Criteria',1)
    SolaceViewVars('tag_temp',tag_temp,'Stock_Check_Criteria',1)
    SolaceViewVars('tmp:showquantity',tmp:showquantity,'Stock_Check_Criteria',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select1;  SolaceCtrlName = '?GLO:Select1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:2;  SolaceCtrlName = '?Prompt1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select4;  SolaceCtrlName = '?GLO:Select4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select2;  SolaceCtrlName = '?GLO:Select2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select3;  SolaceCtrlName = '?GLO:Select3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select6;  SolaceCtrlName = '?GLO:Select6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group1;  SolaceCtrlName = '?Group1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select7:Prompt;  SolaceCtrlName = '?GLO:Select7:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select7;  SolaceCtrlName = '?GLO:Select7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupStartDate;  SolaceCtrlName = '?LookupStartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select8:Prompt;  SolaceCtrlName = '?GLO:Select8:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select8;  SolaceCtrlName = '?GLO:Select8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupEndDate;  SolaceCtrlName = '?LookupEndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select5;  SolaceCtrlName = '?GLO:Select5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select5:Radio1;  SolaceCtrlName = '?glo:select5:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select5:Radio2;  SolaceCtrlName = '?glo:select5:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Stock_Check_Criteria')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Stock_Check_Criteria')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOCATION.Open
  SELF.FilesOpened = True
  glo:select1 = ''
  glo:select2 = 'NO'
  glo:select3 = 'NO'
  glo:select4 = ''
  glo:select5 = 1
  glo:select6 = 0
  glo:select7 = Deformat('1/1/1990',@d6)
  glo:select8 = Today()
  BRW3.Init(?List,Queue:Browse.ViewPosition,BRW3::View:Browse,Queue:Browse,Relate:LOCSHELF,SELF)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?GLO:Select7{Prop:Alrt,255} = MouseLeft2
  ?GLO:Select8{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  BRW3.Q &= Queue:Browse
  BRW3.RetainRow = 0
  BRW3.AddSortOrder(,los:Shelf_Location_Key)
  BRW3.AddRange(los:Site_Location,GLO:Select1)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(,los:Shelf_Location,1,BRW3)
  BIND('tag_temp',tag_temp)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW3.AddField(tag_temp,BRW3.Q.tag_temp)
  BRW3.AddField(los:Shelf_Location,BRW3.Q.los:Shelf_Location)
  BRW3.AddField(los:Site_Location,BRW3.Q.los:Site_Location)
  FDCB2.Init(GLO:Select1,?GLO:Select1,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(loc:Location_Key)
  FDCB2.AddField(loc:Location,FDCB2.Q.loc:Location)
  FDCB2.AddField(loc:RecordNumber,FDCB2.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  FDCB8.Init(GLO:Select4,?GLO:Select4,Queue:FileDropCombo:1.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo:1,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo:1
  FDCB8.AddSortOrder(man:Manufacturer_Key)
  FDCB8.AddField(man:Manufacturer,FDCB8.Q.man:Manufacturer)
  FDCB8.AddField(man:RecordNumber,FDCB8.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW3.AskProcedure = 0
      CLEAR(BRW3.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  glo:select1 = ''
  glo:select2 = ''
  glo:select3 = ''
  glo:select4 = ''
  glo:select5 = ''
  glo:Select6 = ''
  glo:select7 = ''
  glo:select8 = ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:LOCATION.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Stock_Check_Criteria',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?GLO:Select1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select1, Accepted)
      BRW3.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select1, Accepted)
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select7 = TINCALENDARStyle1(GLO:Select7)
          Display(?GLO:Select7)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select8 = TINCALENDARStyle1(GLO:Select8)
          Display(?GLO:Select8)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      error# = 0
      If glo:select1 = ''
          Select(?glo:select1)
          error# = 1
      End!If glo:select1 = ''
      If error# = 0 And ~Records(glo:Queue)
          Case MessageEx('You must select at least one shelf location.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          error# = 1
      End!If error# = 0 And ~Records(glo:Queue)
      If error# = 0
          Stock_Check_Report
      End!If error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Stock_Check_Criteria')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?GLO:Select7
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?GLO:Select8
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::4:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::4:DASUNTAGALL
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


FDCB2.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1


BRW3.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = los:Shelf_Location
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW3.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW3::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW3::RecordStatus=ReturnValue
  IF BRW3::RecordStatus NOT=Record:OK THEN RETURN BRW3::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = los:Shelf_Location
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::4:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW3::RecordStatus
  RETURN ReturnValue

