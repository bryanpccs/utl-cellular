

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03016.INC'),ONCE        !Local module procedure declarations
                     END


Stock_Usage_Criteria PROCEDURE                        !Generated from procedure template - Window

FilesOpened          BYTE
site_location_temp   STRING('ALL')
supplier_temp        STRING('ALL')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select3
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?GLO:Select4
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
FDCB3::View:FileDropCombo VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:RecordNumber)
                     END
window               WINDOW('Stock Usage Report Criteria'),AT(,,224,203),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,216,168),USE(?Sheet1),SPREAD
                         TAB('Stock Usage Criteria'),USE(?Tab1)
                           OPTION('Site Location'),AT(8,20,208,32),USE(site_location_temp),BOXED
                             RADIO('All'),AT(12,32),USE(?Option1:Radio1),VALUE('ALL')
                             RADIO('Individual'),AT(40,32),USE(?Option1:Radio2),VALUE('IND')
                           END
                           COMBO(@s30),AT(88,32,124,10),USE(GLO:Select3),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M*@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           OPTION('Supplier'),AT(8,56,208,32),USE(supplier_temp),BOXED
                             RADIO('All'),AT(12,68),USE(?Option2:Radio1),VALUE('ALL')
                             RADIO('Individual'),AT(40,68),USE(?Option2:Radio2),VALUE('IND')
                           END
                           COMBO(@s30),AT(88,68,124,10),USE(GLO:Select4),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                           CHECK('Suppress Zeros'),AT(88,92),USE(GLO:Select5),VALUE('YES','NO')
                           STRING('Sort Order'),AT(8,108),USE(?String1)
                           OPTION('Option 3'),AT(88,108,136,12),USE(GLO:Select6)
                             RADIO('Part Number'),AT(88,108),USE(?Option3:Radio1),VALUE('P')
                             RADIO('Shelf Location'),AT(153,108),USE(?Option3:Radio2),VALUE('S')
                           END
                           CHECK('Transfers From Main Store Only'),AT(87,124),USE(GLO:Select7),HIDE,VALUE('1','0')
                           BUTTON,AT(156,140,10,10),USE(?PopCalendar),ICON('Calenda2.ico')
                           PROMPT('Start Date'),AT(8,140),USE(?glo:select1:Prompt)
                           ENTRY(@d6b),AT(88,140,64,10),USE(GLO:Select1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('End Date'),AT(8,156),USE(?glo:select2:Prompt)
                           ENTRY(@d6b),AT(88,156,64,10),USE(GLO:Select2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(156,156,10,10),USE(?PopCalendar:2),ICON('Calenda2.ico')
                         END
                       END
                       BUTTON('&OK'),AT(104,180,56,16),USE(?OkButton),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(160,180,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,176,216,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

FDCB3                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?site_location_temp{prop:Font,3} = -1
    ?site_location_temp{prop:Color} = 15066597
    ?site_location_temp{prop:Trn} = 0
    ?Option1:Radio1{prop:Font,3} = -1
    ?Option1:Radio1{prop:Color} = 15066597
    ?Option1:Radio1{prop:Trn} = 0
    ?Option1:Radio2{prop:Font,3} = -1
    ?Option1:Radio2{prop:Color} = 15066597
    ?Option1:Radio2{prop:Trn} = 0
    If ?GLO:Select3{prop:ReadOnly} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 15066597
    Elsif ?GLO:Select3{prop:Req} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 8454143
    Else ! If ?GLO:Select3{prop:Req} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 16777215
    End ! If ?GLO:Select3{prop:Req} = True
    ?GLO:Select3{prop:Trn} = 0
    ?GLO:Select3{prop:FontStyle} = font:Bold
    ?supplier_temp{prop:Font,3} = -1
    ?supplier_temp{prop:Color} = 15066597
    ?supplier_temp{prop:Trn} = 0
    ?Option2:Radio1{prop:Font,3} = -1
    ?Option2:Radio1{prop:Color} = 15066597
    ?Option2:Radio1{prop:Trn} = 0
    ?Option2:Radio2{prop:Font,3} = -1
    ?Option2:Radio2{prop:Color} = 15066597
    ?Option2:Radio2{prop:Trn} = 0
    If ?GLO:Select4{prop:ReadOnly} = True
        ?GLO:Select4{prop:FontColor} = 65793
        ?GLO:Select4{prop:Color} = 15066597
    Elsif ?GLO:Select4{prop:Req} = True
        ?GLO:Select4{prop:FontColor} = 65793
        ?GLO:Select4{prop:Color} = 8454143
    Else ! If ?GLO:Select4{prop:Req} = True
        ?GLO:Select4{prop:FontColor} = 65793
        ?GLO:Select4{prop:Color} = 16777215
    End ! If ?GLO:Select4{prop:Req} = True
    ?GLO:Select4{prop:Trn} = 0
    ?GLO:Select4{prop:FontStyle} = font:Bold
    ?GLO:Select5{prop:Font,3} = -1
    ?GLO:Select5{prop:Color} = 15066597
    ?GLO:Select5{prop:Trn} = 0
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    ?GLO:Select6{prop:Font,3} = -1
    ?GLO:Select6{prop:Color} = 15066597
    ?GLO:Select6{prop:Trn} = 0
    ?Option3:Radio1{prop:Font,3} = -1
    ?Option3:Radio1{prop:Color} = 15066597
    ?Option3:Radio1{prop:Trn} = 0
    ?Option3:Radio2{prop:Font,3} = -1
    ?Option3:Radio2{prop:Color} = 15066597
    ?Option3:Radio2{prop:Trn} = 0
    ?GLO:Select7{prop:Font,3} = -1
    ?GLO:Select7{prop:Color} = 15066597
    ?GLO:Select7{prop:Trn} = 0
    ?glo:select1:Prompt{prop:FontColor} = -1
    ?glo:select1:Prompt{prop:Color} = 15066597
    If ?GLO:Select1{prop:ReadOnly} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 15066597
    Elsif ?GLO:Select1{prop:Req} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 8454143
    Else ! If ?GLO:Select1{prop:Req} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 16777215
    End ! If ?GLO:Select1{prop:Req} = True
    ?GLO:Select1{prop:Trn} = 0
    ?GLO:Select1{prop:FontStyle} = font:Bold
    ?glo:select2:Prompt{prop:FontColor} = -1
    ?glo:select2:Prompt{prop:Color} = 15066597
    If ?GLO:Select2{prop:ReadOnly} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 15066597
    Elsif ?GLO:Select2{prop:Req} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 8454143
    Else ! If ?GLO:Select2{prop:Req} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 16777215
    End ! If ?GLO:Select2{prop:Req} = True
    ?GLO:Select2{prop:Trn} = 0
    ?GLO:Select2{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
show_hide        Routine
    Case site_location_temp
        Of 'ALL'
            Disable(?glo:select3)    
        Of 'IND'
            Enable(?glo:select3)
    End!Case glo:select3
    
    Case supplier_temp
        Of 'ALL'
            Disable(?glo:select4)
        Of 'IND'
            Enable(?glo:select4)
    End!Case supplier_temp
    Display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Stock_Usage_Criteria',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Stock_Usage_Criteria',1)
    SolaceViewVars('site_location_temp',site_location_temp,'Stock_Usage_Criteria',1)
    SolaceViewVars('supplier_temp',supplier_temp,'Stock_Usage_Criteria',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?site_location_temp;  SolaceCtrlName = '?site_location_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio1;  SolaceCtrlName = '?Option1:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio2;  SolaceCtrlName = '?Option1:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select3;  SolaceCtrlName = '?GLO:Select3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?supplier_temp;  SolaceCtrlName = '?supplier_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option2:Radio1;  SolaceCtrlName = '?Option2:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option2:Radio2;  SolaceCtrlName = '?Option2:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select4;  SolaceCtrlName = '?GLO:Select4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select5;  SolaceCtrlName = '?GLO:Select5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1;  SolaceCtrlName = '?String1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select6;  SolaceCtrlName = '?GLO:Select6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option3:Radio1;  SolaceCtrlName = '?Option3:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option3:Radio2;  SolaceCtrlName = '?Option3:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select7;  SolaceCtrlName = '?GLO:Select7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select1:Prompt;  SolaceCtrlName = '?glo:select1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select1;  SolaceCtrlName = '?GLO:Select1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select2:Prompt;  SolaceCtrlName = '?glo:select2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select2;  SolaceCtrlName = '?GLO:Select2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Stock_Usage_Criteria')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Stock_Usage_Criteria')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Option1:Radio1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:LOCATION.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do show_hide
  glo:select1 = Deformat('1/1/1990',@d6)
  glo:select2 = Today()
  glo:select3 = ''
  glo:select4 = ''
  glo:select5 = 'NO'
  glo:select6 = 'P'
  glo:Select7 = 0
  Do RecolourWindow
  ?glo:select1{Prop:Alrt,255} = MouseLeft2
  ?glo:select2{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  FDCB2.Init(GLO:Select3,?GLO:Select3,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(loc:Location_Key)
  FDCB2.AddField(loc:Location,FDCB2.Q.loc:Location)
  FDCB2.AddField(loc:RecordNumber,FDCB2.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  FDCB3.Init(GLO:Select4,?GLO:Select4,Queue:FileDropCombo:1.ViewPosition,FDCB3::View:FileDropCombo,Queue:FileDropCombo:1,Relate:SUPPLIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB3.Q &= Queue:FileDropCombo:1
  FDCB3.AddSortOrder(sup:Company_Name_Key)
  FDCB3.AddField(sup:Company_Name,FDCB3.Q.sup:Company_Name)
  FDCB3.AddField(sup:RecordNumber,FDCB3.Q.sup:RecordNumber)
  ThisWindow.AddItem(FDCB3.WindowComponent)
  FDCB3.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  Clear(glo:G_Select1)
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Stock_Usage_Criteria',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?site_location_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?site_location_temp, Accepted)
      Do show_hide
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?site_location_temp, Accepted)
    OF ?supplier_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?supplier_temp, Accepted)
      Do show_hide
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?supplier_temp, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select1 = TINCALENDARStyle1(GLO:Select1)
          Display(?glo:select1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select2 = TINCALENDARStyle1(GLO:Select2)
          Display(?glo:select2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      error# = 0
      If glo:select1 = ''
          error# = 1
          Select(?glo:select1)
      End!If glo:select1 = ''
      If glo:select2 = ''
          error# = 1
          Select(?glo:select2)
      End
      If site_location_temp = 'ALL'
          glo:select3 = ''
      End!If site_location_temp = 'ALL'
      If supplier_temp = 'ALL'
          glo:select4 = ''
      End!If site_location_temp = 'ALL'
      If error# = 0
          stock_usage_report
      End!If error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      Clear(glo:G_Select1)
      Post(event:closewindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Stock_Usage_Criteria')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?GLO:Select1
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?GLO:Select2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


FDCB2.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1

