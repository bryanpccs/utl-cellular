

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03073.INC'),ONCE        !Local module procedure declarations
                     END


SelectMainSubAccount PROCEDURE                        !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
CurrentTab           STRING(80)
save_sub_id          USHORT,AUTO
save_tra_id          USHORT,AUTO
LocalRequest         LONG
FilesOpened          BYTE
main_account_temp    STRING(15)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(TRADETMP)
                       PROJECT(tratmp:Account_Number)
                       PROJECT(tratmp:Company_Name)
                       PROJECT(tratmp:Type)
                       PROJECT(tratmp:RefNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tratmp:Account_Number  LIKE(tratmp:Account_Number)    !List box control field - type derived from field
tratmp:Company_Name    LIKE(tratmp:Company_Name)      !List box control field - type derived from field
tratmp:Type            LIKE(tratmp:Type)              !List box control field - type derived from field
tratmp:RefNumber       LIKE(tratmp:RefNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Sub Account File'),AT(,,440,188),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Sub_Accounts'),SYSTEM,GRAY,DOUBLE
                       LIST,AT(8,36,340,144),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),ALRT(InsertKey),FORMAT('66L(2)|M~Account Number~@s15@128L(2)|M~Company Name~@s30@12L(2)|M~Type~@s3@'),FROM(Queue:Browse:1)
                       BUTTON('&Select'),AT(360,20,76,20),USE(?Select:2),LEFT,ICON('select.ico')
                       SHEET,AT(4,4,348,180),USE(?CurrentTab),SPREAD
                         TAB('By Account Number'),USE(?Tab:4)
                           ENTRY(@s15),AT(8,20,64,10),USE(tratmp:Account_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('By Company Name'),USE(?Tab3)
                           ENTRY(@s30),AT(8,20,124,10),USE(tratmp:Company_Name),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       BUTTON('Close'),AT(360,164,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                       BUTTON('DELETE THIS'),AT(276,200,76,20),USE(?Help),STD(STD:Help)
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:4{prop:Color} = 15066597
    If ?tratmp:Account_Number{prop:ReadOnly} = True
        ?tratmp:Account_Number{prop:FontColor} = 65793
        ?tratmp:Account_Number{prop:Color} = 15066597
    Elsif ?tratmp:Account_Number{prop:Req} = True
        ?tratmp:Account_Number{prop:FontColor} = 65793
        ?tratmp:Account_Number{prop:Color} = 8454143
    Else ! If ?tratmp:Account_Number{prop:Req} = True
        ?tratmp:Account_Number{prop:FontColor} = 65793
        ?tratmp:Account_Number{prop:Color} = 16777215
    End ! If ?tratmp:Account_Number{prop:Req} = True
    ?tratmp:Account_Number{prop:Trn} = 0
    ?tratmp:Account_Number{prop:FontStyle} = font:Bold
    ?Tab3{prop:Color} = 15066597
    If ?tratmp:Company_Name{prop:ReadOnly} = True
        ?tratmp:Company_Name{prop:FontColor} = 65793
        ?tratmp:Company_Name{prop:Color} = 15066597
    Elsif ?tratmp:Company_Name{prop:Req} = True
        ?tratmp:Company_Name{prop:FontColor} = 65793
        ?tratmp:Company_Name{prop:Color} = 8454143
    Else ! If ?tratmp:Company_Name{prop:Req} = True
        ?tratmp:Company_Name{prop:FontColor} = 65793
        ?tratmp:Company_Name{prop:Color} = 16777215
    End ! If ?tratmp:Company_Name{prop:Req} = True
    ?tratmp:Company_Name{prop:Trn} = 0
    ?tratmp:Company_Name{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'SelectMainSubAccount',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'SelectMainSubAccount',1)
    SolaceViewVars('save_sub_id',save_sub_id,'SelectMainSubAccount',1)
    SolaceViewVars('save_tra_id',save_tra_id,'SelectMainSubAccount',1)
    SolaceViewVars('LocalRequest',LocalRequest,'SelectMainSubAccount',1)
    SolaceViewVars('FilesOpened',FilesOpened,'SelectMainSubAccount',1)
    SolaceViewVars('main_account_temp',main_account_temp,'SelectMainSubAccount',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select:2;  SolaceCtrlName = '?Select:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:4;  SolaceCtrlName = '?Tab:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tratmp:Account_Number;  SolaceCtrlName = '?tratmp:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tratmp:Company_Name;  SolaceCtrlName = '?tratmp:Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectMainSubAccount')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'SelectMainSubAccount')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:TRADEACC.Open
  Relate:TRADETMP.Open
  Relate:USELEVEL.Open
  Access:USERS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:TRADETMP,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,tratmp:CompanyNameKey)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?tratmp:Company_Name,tratmp:Company_Name,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,tratmp:AccountNoKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?tratmp:Account_Number,tratmp:Account_Number,1,BRW1)
  BRW1.AddField(tratmp:Account_Number,BRW1.Q.tratmp:Account_Number)
  BRW1.AddField(tratmp:Company_Name,BRW1.Q.tratmp:Company_Name)
  BRW1.AddField(tratmp:Type,BRW1.Q.tratmp:Type)
  BRW1.AddField(tratmp:RefNumber,BRW1.Q.tratmp:RefNumber)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:4{PROP:TEXT} = 'By Account Number'
    ?Tab3{PROP:TEXT} = 'By Company Name'
    ?Browse:1{PROP:FORMAT} ='66L(2)|M~Account Number~@s15@#1#128L(2)|M~Company Name~@s30@#2#12L(2)|M~Type~@s3@#3#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TRADEACC.Close
    Relate:TRADETMP.Close
    Relate:USELEVEL.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'SelectMainSubAccount',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'SelectMainSubAccount')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='66L(2)|M~Account Number~@s15@#1#128L(2)|M~Company Name~@s30@#2#12L(2)|M~Type~@s3@#3#'
          ?Tab:4{PROP:TEXT} = 'By Account Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='128L(2)|M~Company Name~@s30@#2#66L(2)|M~Account Number~@s15@#1#12L(2)|M~Type~@s3@#3#'
          ?Tab3{PROP:TEXT} = 'By Company Name'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?tratmp:Account_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tratmp:Account_Number, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tratmp:Account_Number, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    CASE FIELD()
    OF ?tratmp:Company_Name
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tratmp:Company_Name, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tratmp:Company_Name, Selected)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

