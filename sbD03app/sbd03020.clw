

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03020.INC'),ONCE        !Local module procedure declarations
                     END


Choose_Printer PROCEDURE (f_printer,f_preview,f_copies) !Generated from procedure template - Window

FilesOpened          BYTE
defaultprinter       CSTRING(256)
tmp:printer          CSTRING(256)
printer_name_temp    CSTRING(256)
preview_temp         STRING('YES')
copies_temp          LONG(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Choose Destination Printer'),AT(,,240,103),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       PROMPT('Printer Name'),AT(8,24),USE(?printer_name_temp:Prompt),TRN
                       ENTRY(@s255),AT(84,24,124,10),USE(printer_name_temp),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                       BUTTON,AT(212,20,16,16),USE(?Choose_Printer),LEFT,TIP('Change Selected Printer'),ICON(ICON:Print1)
                       SHEET,AT(4,4,232,68),USE(?Sheet1),SPREAD
                         TAB('Choose Destination Printer'),USE(?Tab1)
                           PROMPT('Number Of Copies'),AT(8,40),USE(?copies_temp:Prompt)
                           SPIN(@n8),AT(84,40,64,10),USE(copies_temp),LEFT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       CHECK('Show Print Preview'),AT(84,56),USE(preview_temp),VALUE('YES','NO')
                       PANEL,AT(4,76,232,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(176,80,56,16),USE(?OkButton),LEFT,ICON('ok.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!EnumPtrcl        EnumPrtClType

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?printer_name_temp:Prompt{prop:FontColor} = -1
    ?printer_name_temp:Prompt{prop:Color} = 15066597
    If ?printer_name_temp{prop:ReadOnly} = True
        ?printer_name_temp{prop:FontColor} = 65793
        ?printer_name_temp{prop:Color} = 15066597
    Elsif ?printer_name_temp{prop:Req} = True
        ?printer_name_temp{prop:FontColor} = 65793
        ?printer_name_temp{prop:Color} = 8454143
    Else ! If ?printer_name_temp{prop:Req} = True
        ?printer_name_temp{prop:FontColor} = 65793
        ?printer_name_temp{prop:Color} = 16777215
    End ! If ?printer_name_temp{prop:Req} = True
    ?printer_name_temp{prop:Trn} = 0
    ?printer_name_temp{prop:FontStyle} = font:Bold
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?copies_temp:Prompt{prop:FontColor} = -1
    ?copies_temp:Prompt{prop:Color} = 15066597
    If ?copies_temp{prop:ReadOnly} = True
        ?copies_temp{prop:FontColor} = 65793
        ?copies_temp{prop:Color} = 15066597
    Elsif ?copies_temp{prop:Req} = True
        ?copies_temp{prop:FontColor} = 65793
        ?copies_temp{prop:Color} = 8454143
    Else ! If ?copies_temp{prop:Req} = True
        ?copies_temp{prop:FontColor} = 65793
        ?copies_temp{prop:Color} = 16777215
    End ! If ?copies_temp{prop:Req} = True
    ?copies_temp{prop:Trn} = 0
    ?copies_temp{prop:FontStyle} = font:Bold
    ?preview_temp{prop:Font,3} = -1
    ?preview_temp{prop:Color} = 15066597
    ?preview_temp{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Choose_Printer',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Choose_Printer',1)
    SolaceViewVars('defaultprinter',defaultprinter,'Choose_Printer',1)
    SolaceViewVars('tmp:printer',tmp:printer,'Choose_Printer',1)
    SolaceViewVars('printer_name_temp',printer_name_temp,'Choose_Printer',1)
    SolaceViewVars('preview_temp',preview_temp,'Choose_Printer',1)
    SolaceViewVars('copies_temp',copies_temp,'Choose_Printer',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?printer_name_temp:Prompt;  SolaceCtrlName = '?printer_name_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?printer_name_temp;  SolaceCtrlName = '?printer_name_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Choose_Printer;  SolaceCtrlName = '?Choose_Printer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?copies_temp:Prompt;  SolaceCtrlName = '?copies_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?copies_temp;  SolaceCtrlName = '?copies_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?preview_temp;  SolaceCtrlName = '?preview_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Choose_Printer')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Choose_Printer')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?printer_name_temp:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  printer_name_temp   = printer{propprint:device}
  Do RecolourWindow
  ! support for CPCS
  !EnumPtrCl.init()
  !EnumPtrCl.GetDefaultPrinter(DefaultPrinter)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  !EnumPtrCl.kill()
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Choose_Printer',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Choose_Printer
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Choose_Printer, Accepted)
!      If EnumPtrCl.SetDefaultPrinter(1)  then
!        Message('error setting default printer')
!      end
!      EnumPtrCl.GetDefaultPrinter(printer_name_temp)
!      !printer_name_temp = Upper(printer_name_temp)
!      display(?printer_name_temp)
!      If EnumPtrCl.SetDefaultPrinter(0,defaultprinter)
!          Stop(error())
!      End!If EnumPtrCl.SetDefaultPrinter(tmp:printer)

    If PrinterDialog('Select printer')
       printer_Name_temp = PRINTER{PROPPRINT:Device}
       Display(?Printer_Name_Temp)
    End

      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Choose_Printer, Accepted)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      f_printer   = printer_name_temp
      f_preview   = preview_temp
      f_copies    = copies_temp
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Choose_Printer')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

