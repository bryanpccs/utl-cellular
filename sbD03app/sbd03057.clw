

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03057.INC'),ONCE        !Local module procedure declarations
                     END


Third_Party_Consignment_Criteria PROCEDURE            !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select1
trd:Company_Name       LIKE(trd:Company_Name)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB3::View:FileDropCombo VIEW(TRDPARTY)
                       PROJECT(trd:Company_Name)
                     END
window               WINDOW('3rd Party Report Criteria'),AT(,,220,215),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,212,180),USE(?Sheet1),SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           COMBO(@s30),AT(84,20,124,10),USE(GLO:Select1),IMM,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Batch Number'),AT(8,36),USE(?Prompt1:2)
                           ENTRY(@s40),AT(84,36,64,10),USE(GLO:Select2),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           CHECK('Mark Batch As Despatched'),AT(84,52),USE(GLO:Select3),VALUE('YES','NO')
                           CHECK('Include All Items On Batch'),AT(84,68),USE(GLO:Select10),DISABLE,VALUE('YES','NO')
                           PROMPT('Despatch Date'),AT(8,84),USE(?glo:select4:Prompt)
                           ENTRY(@d6b),AT(84,84,64,10),USE(GLO:Select4),DISABLE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(152,84,10,10),USE(?PopCalendar),DISABLE,ICON('calenda2.ico')
                           PROMPT('Turnaround Time'),AT(8,100),USE(?GLO:Select5:Prompt)
                           ENTRY(@n3),AT(84,100,64,10),USE(GLO:Select5),DISABLE,FONT(,,,FONT:bold)
                           PROMPT('Days'),AT(152,100),USE(?Prompt5)
                           CHECK('Repair If Required'),AT(8,120),USE(GLO:Select6),VALUE('YES','NO')
                           CHECK('Refurbish All Units'),AT(132,120),USE(GLO:Select11),VALUE('YES','NO')
                           CHECK('Screening'),AT(8,136),USE(GLO:Select7),VALUE('YES','NO')
                           CHECK('Upgrade Software'),AT(132,136),USE(GLO:Select12),VALUE('YES','NO')
                           CHECK('Refurbishment If Required'),AT(8,152),USE(GLO:Select8),VALUE('YES','NO')
                           CHECK('Upgrade If Required'),AT(8,168),USE(GLO:Select9),VALUE('YES','NO')
                           PROMPT('3rd Party Agent'),AT(8,20),USE(?Prompt1)
                         END
                       END
                       PANEL,AT(4,188,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,192,56,16),USE(?OkButton),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Close'),AT(156,192,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB3                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    If ?GLO:Select1{prop:ReadOnly} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 15066597
    Elsif ?GLO:Select1{prop:Req} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 8454143
    Else ! If ?GLO:Select1{prop:Req} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 16777215
    End ! If ?GLO:Select1{prop:Req} = True
    ?GLO:Select1{prop:Trn} = 0
    ?GLO:Select1{prop:FontStyle} = font:Bold
    ?Prompt1:2{prop:FontColor} = -1
    ?Prompt1:2{prop:Color} = 15066597
    If ?GLO:Select2{prop:ReadOnly} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 15066597
    Elsif ?GLO:Select2{prop:Req} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 8454143
    Else ! If ?GLO:Select2{prop:Req} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 16777215
    End ! If ?GLO:Select2{prop:Req} = True
    ?GLO:Select2{prop:Trn} = 0
    ?GLO:Select2{prop:FontStyle} = font:Bold
    ?GLO:Select3{prop:Font,3} = -1
    ?GLO:Select3{prop:Color} = 15066597
    ?GLO:Select3{prop:Trn} = 0
    ?GLO:Select10{prop:Font,3} = -1
    ?GLO:Select10{prop:Color} = 15066597
    ?GLO:Select10{prop:Trn} = 0
    ?glo:select4:Prompt{prop:FontColor} = -1
    ?glo:select4:Prompt{prop:Color} = 15066597
    If ?GLO:Select4{prop:ReadOnly} = True
        ?GLO:Select4{prop:FontColor} = 65793
        ?GLO:Select4{prop:Color} = 15066597
    Elsif ?GLO:Select4{prop:Req} = True
        ?GLO:Select4{prop:FontColor} = 65793
        ?GLO:Select4{prop:Color} = 8454143
    Else ! If ?GLO:Select4{prop:Req} = True
        ?GLO:Select4{prop:FontColor} = 65793
        ?GLO:Select4{prop:Color} = 16777215
    End ! If ?GLO:Select4{prop:Req} = True
    ?GLO:Select4{prop:Trn} = 0
    ?GLO:Select4{prop:FontStyle} = font:Bold
    ?GLO:Select5:Prompt{prop:FontColor} = -1
    ?GLO:Select5:Prompt{prop:Color} = 15066597
    If ?GLO:Select5{prop:ReadOnly} = True
        ?GLO:Select5{prop:FontColor} = 65793
        ?GLO:Select5{prop:Color} = 15066597
    Elsif ?GLO:Select5{prop:Req} = True
        ?GLO:Select5{prop:FontColor} = 65793
        ?GLO:Select5{prop:Color} = 8454143
    Else ! If ?GLO:Select5{prop:Req} = True
        ?GLO:Select5{prop:FontColor} = 65793
        ?GLO:Select5{prop:Color} = 16777215
    End ! If ?GLO:Select5{prop:Req} = True
    ?GLO:Select5{prop:Trn} = 0
    ?GLO:Select5{prop:FontStyle} = font:Bold
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?GLO:Select6{prop:Font,3} = -1
    ?GLO:Select6{prop:Color} = 15066597
    ?GLO:Select6{prop:Trn} = 0
    ?GLO:Select11{prop:Font,3} = -1
    ?GLO:Select11{prop:Color} = 15066597
    ?GLO:Select11{prop:Trn} = 0
    ?GLO:Select7{prop:Font,3} = -1
    ?GLO:Select7{prop:Color} = 15066597
    ?GLO:Select7{prop:Trn} = 0
    ?GLO:Select12{prop:Font,3} = -1
    ?GLO:Select12{prop:Color} = 15066597
    ?GLO:Select12{prop:Trn} = 0
    ?GLO:Select8{prop:Font,3} = -1
    ?GLO:Select8{prop:Color} = 15066597
    ?GLO:Select8{prop:Trn} = 0
    ?GLO:Select9{prop:Font,3} = -1
    ?GLO:Select9{prop:Color} = 15066597
    ?GLO:Select9{prop:Trn} = 0
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Third_Party_Consignment_Criteria',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select1;  SolaceCtrlName = '?GLO:Select1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:2;  SolaceCtrlName = '?Prompt1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select2;  SolaceCtrlName = '?GLO:Select2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select3;  SolaceCtrlName = '?GLO:Select3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select10;  SolaceCtrlName = '?GLO:Select10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select4:Prompt;  SolaceCtrlName = '?glo:select4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select4;  SolaceCtrlName = '?GLO:Select4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select5:Prompt;  SolaceCtrlName = '?GLO:Select5:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select5;  SolaceCtrlName = '?GLO:Select5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select6;  SolaceCtrlName = '?GLO:Select6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select11;  SolaceCtrlName = '?GLO:Select11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select7;  SolaceCtrlName = '?GLO:Select7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select12;  SolaceCtrlName = '?GLO:Select12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select8;  SolaceCtrlName = '?GLO:Select8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select9;  SolaceCtrlName = '?GLO:Select9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Third_Party_Consignment_Criteria')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Third_Party_Consignment_Criteria')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?GLO:Select1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:TRDPARTY.Open
  SELF.FilesOpened = True
  glo:select1  = ''
  glo:select2  = ''
  glo:select3  = 'NO'
  glo:select4  = Today()
  glo:select6     = 'NO'
  glo:select7     = 'NO'
  glo:select8     = 'NO'
  glo:select9     = 'NO'
  glo:Select11    = 'NO'
  glo:Select12    = 'NO'
  
  OPEN(window)
  SELF.Opened=True
  If GETINI('HIDE','ThirdPartyDetails',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      ?glo:Select6{prop:Disable} = 1
      ?glo:Select7{prop:Disable} = 1
      ?glo:Select8{prop:Disable} = 1
      ?glo:Select9{prop:Disable} = 1
      ?glo:Select11{prop:Disable} = 1
      ?glo:Select12{prop:Disable} = 1
  End !GETINI('HIDE','ThirdPartyDetails',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  Do RecolourWindow
  ?glo:select4{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  IF ?GLO:Select3{Prop:Checked} = True
    GLO:Select10 = 'NO'
    ENABLE(?glo:select4)
    ENABLE(?PopCalendar)
    ENABLE(?GLO:Select5)
    DISABLE(?GLO:Select10)
  END
  IF ?GLO:Select3{Prop:Checked} = False
    DISABLE(?glo:select4)
    DISABLE(?PopCalendar)
    ENABLE(?GLO:Select5)
    ENABLE(?GLO:Select10)
  END
  FDCB3.Init(GLO:Select1,?GLO:Select1,Queue:FileDropCombo.ViewPosition,FDCB3::View:FileDropCombo,Queue:FileDropCombo,Relate:TRDPARTY,ThisWindow,GlobalErrors,0,1,0)
  FDCB3.Q &= Queue:FileDropCombo
  FDCB3.AddSortOrder(trd:Company_Name_Key)
  FDCB3.AddField(trd:Company_Name,FDCB3.Q.trd:Company_Name)
  ThisWindow.AddItem(FDCB3.WindowComponent)
  FDCB3.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  glo:select1  = ''
  glo:select2  = ''
  glo:select3  = ''
  glo:select4  = ''
  glo:select6     = ''
  glo:select7     = ''
  glo:select8     = ''
  glo:select9     = ''
  glo:Select11    = ''
  glo:Select12    = ''
  
  
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TRDPARTY.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Third_Party_Consignment_Criteria',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?GLO:Select3
      IF ?GLO:Select3{Prop:Checked} = True
        GLO:Select10 = 'NO'
        ENABLE(?glo:select4)
        ENABLE(?PopCalendar)
        ENABLE(?GLO:Select5)
        DISABLE(?GLO:Select10)
      END
      IF ?GLO:Select3{Prop:Checked} = False
        DISABLE(?glo:select4)
        DISABLE(?PopCalendar)
        ENABLE(?GLO:Select5)
        ENABLE(?GLO:Select10)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select3, Accepted)
      access:trdparty.clearkey(trd:company_name_key)
      trd:company_name = glo:select1
      if access:trdparty.tryfetch(trd:company_name_key) = Level:Benign
          glo:select5 = TRD:Turnaround_Time
      End!if access:trdparty.tryfetch(trd:company_name_key) = Level:Benign
      Display()
         
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select3, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select4 = TINCALENDARStyle1(GLO:Select4)
          Display(?glo:select4)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    error# = 0
    If glo:select1 = ''
        Select(?glo:select1)
        error# = 1
    End!If glo:select1 = ''
    If glo:select2 = '' and error# = 0
        Select(?glo:select2)
        error# = 1
    End!If glo:select2 = '' and error# = 0

    If glo:select3 = 'YES' and error# = 0
        If glo:select4 = ''
            Case MessageEx('You have not selected a Despatch Date.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
            error# = 1
        End!If glo:select4 = ''
        If glo:select5 = '' and error# = 0
            Case MessageEx('You have selected to Despatch the Batch but have not selected a Turnaround Time.<13,10><13,10>Do you wish to enter a Turnaround Time?','ServiceBase 2000',|
                           'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                Of 1 ! &Yes Button
                    error# = 1
                Of 2 ! &No Button
            End!Case MessageEx
        End!If glo:select5 = '' and error# = 0
    End!If glo:select3 = 'YES'
    If error# = 0

        Third_Party_Consignment_Note
    End!If error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Third_Party_Consignment_Criteria')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?GLO:Select4
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

