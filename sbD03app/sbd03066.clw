

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03066.INC'),ONCE        !Local module procedure declarations
                     END


LabelG_Consolidation PROCEDURE                        !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
tmp:StartDate        DATE
sav:filename1        STRING(255)
sav:filename2        STRING(255)
sav:accountno        STRING(20)
sav:postcode         STRING(20)
save_lab_id          USHORT,AUTO
tmp:Sumtotal         STRING(60),DIM(50)
tmp:total            LONG
tmp:accountnumber    STRING(30)
save_job_id          USHORT,AUTO
tmp:Dates            STRING(60),DIM(50)
savepath             STRING(255)
save_tra_id          USHORT,AUTO
save_sub_id          USHORT,AUTO
save_cou_id          USHORT,AUTO
tmp:EndDate          DATE
tmp:SkipZero         BYTE(0)
tmp:UseAccount       BYTE(0)
tmp:SelectAccount    STRING(30)
tmp:ExportType       BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:exportinout      BYTE(0)
window               WINDOW('Label G Consolidation'),AT(,,227,163),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,220,128),USE(?Sheet1),SPREAD
                         TAB('Label G Consolidation'),USE(?Tab1)
                           CHECK('Select Trade Account'),AT(84,20),USE(tmp:UseAccount),VALUE('1','0')
                           OPTION('Consolidation Type'),AT(84,44,124,28),USE(tmp:ExportType),BOXED,COLOR(COLOR:Silver)
                             RADIO('Detailed'),AT(92,56),USE(?tmp:ExportType:Radio1)
                             RADIO('Summary'),AT(156,56),USE(?tmp:ExportType:Radio2)
                           END
                           PROMPT('Account Number'),AT(8,32),USE(?Prompt3),DISABLE
                           ENTRY(@s30),AT(84,32,124,10),USE(tmp:SelectAccount),FONT(,,,FONT:bold),ALRT(EnterKey),ALRT(MouseRight),ALRT(MouseLeft2)
                           BUTTON,AT(212,32,10,10),USE(?LookupAccountNumber),SKIP,DISABLE,ICON('List3.ico')
                           PROMPT('Start Date'),AT(8,76),USE(?tmp:StartDate:Prompt),HIDE,LEFT
                           ENTRY(@d6),AT(84,76,64,10),USE(tmp:StartDate),HIDE,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(152,76,10,10),USE(?PopCalendar),SKIP,HIDE,ICON('calenda2.ico')
                           PROMPT('End Date'),AT(8,92),USE(?tmp:EndDate:Prompt),HIDE,LEFT
                           ENTRY(@d6),AT(84,92,64,10),USE(tmp:EndDate),HIDE,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(152,92,10,10),USE(?PopCalendar:2),SKIP,HIDE,ICON('calenda2.ico')
                           OPTION('Export Type'),AT(84,104,124,24),USE(tmp:exportinout),BOXED
                             RADIO('Outgoing'),AT(92,115),USE(?tmp:exportinout:Radio1),VALUE('0')
                             RADIO('Incoming'),AT(160,115),USE(?tmp:exportinout:Radio2),VALUE('1')
                           END
                         END
                       END
                       PANEL,AT(4,136,220,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Export'),AT(8,140,56,16),USE(?Export),LEFT,ICON('Disk.gif')
                       BUTTON('Close'),AT(164,140,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Arial',8,,font:regular),center,imm,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Arial',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Arial',8,,)
       button('Cancel'),at(54,44,56,16),use(?cancel),left,icon('cancel.gif')
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:SelectAccount                Like(tmp:SelectAccount)
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?tmp:UseAccount{prop:Font,3} = -1
    ?tmp:UseAccount{prop:Color} = 15066597
    ?tmp:UseAccount{prop:Trn} = 0
    ?tmp:ExportType{prop:Font,3} = -1
    ?tmp:ExportType{prop:Color} = 15066597
    ?tmp:ExportType{prop:Trn} = 0
    ?tmp:ExportType:Radio1{prop:Font,3} = -1
    ?tmp:ExportType:Radio1{prop:Color} = 15066597
    ?tmp:ExportType:Radio1{prop:Trn} = 0
    ?tmp:ExportType:Radio2{prop:Font,3} = -1
    ?tmp:ExportType:Radio2{prop:Color} = 15066597
    ?tmp:ExportType:Radio2{prop:Trn} = 0
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    If ?tmp:SelectAccount{prop:ReadOnly} = True
        ?tmp:SelectAccount{prop:FontColor} = 65793
        ?tmp:SelectAccount{prop:Color} = 15066597
    Elsif ?tmp:SelectAccount{prop:Req} = True
        ?tmp:SelectAccount{prop:FontColor} = 65793
        ?tmp:SelectAccount{prop:Color} = 8454143
    Else ! If ?tmp:SelectAccount{prop:Req} = True
        ?tmp:SelectAccount{prop:FontColor} = 65793
        ?tmp:SelectAccount{prop:Color} = 16777215
    End ! If ?tmp:SelectAccount{prop:Req} = True
    ?tmp:SelectAccount{prop:Trn} = 0
    ?tmp:SelectAccount{prop:FontStyle} = font:Bold
    ?tmp:StartDate:Prompt{prop:FontColor} = -1
    ?tmp:StartDate:Prompt{prop:Color} = 15066597
    If ?tmp:StartDate{prop:ReadOnly} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 15066597
    Elsif ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 8454143
    Else ! If ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 16777215
    End ! If ?tmp:StartDate{prop:Req} = True
    ?tmp:StartDate{prop:Trn} = 0
    ?tmp:StartDate{prop:FontStyle} = font:Bold
    ?tmp:EndDate:Prompt{prop:FontColor} = -1
    ?tmp:EndDate:Prompt{prop:Color} = 15066597
    If ?tmp:EndDate{prop:ReadOnly} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 15066597
    Elsif ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 8454143
    Else ! If ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 16777215
    End ! If ?tmp:EndDate{prop:Req} = True
    ?tmp:EndDate{prop:Trn} = 0
    ?tmp:EndDate{prop:FontStyle} = font:Bold
    ?tmp:exportinout{prop:Font,3} = -1
    ?tmp:exportinout{prop:Color} = 15066597
    ?tmp:exportinout{prop:Trn} = 0
    ?tmp:exportinout:Radio1{prop:Font,3} = -1
    ?tmp:exportinout:Radio1{prop:Color} = 15066597
    ?tmp:exportinout:Radio1{prop:Trn} = 0
    ?tmp:exportinout:Radio2{prop:Font,3} = -1
    ?tmp:exportinout:Radio2{prop:Color} = 15066597
    ?tmp:exportinout:Radio2{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
    
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
        display()
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        case event()
            of event:timer
                break
            of event:closewindow
                cancel# = 1
                break
            of event:accepted
                if field() = ?cancel
                    cancel# = 1
                    break
                end!if field() = ?button1
        end!case event()
    end!accept
    if cancel# = 1
        case messageex('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,charset:ansi,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            of 1 ! &yes button
                tmp:cancel = 1
            of 2 ! &no button
        end!case messageex
    end!if cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'LabelG_Consolidation',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:StartDate',tmp:StartDate,'LabelG_Consolidation',1)
    SolaceViewVars('sav:filename1',sav:filename1,'LabelG_Consolidation',1)
    SolaceViewVars('sav:filename2',sav:filename2,'LabelG_Consolidation',1)
    SolaceViewVars('sav:accountno',sav:accountno,'LabelG_Consolidation',1)
    SolaceViewVars('sav:postcode',sav:postcode,'LabelG_Consolidation',1)
    SolaceViewVars('save_lab_id',save_lab_id,'LabelG_Consolidation',1)
    
      Loop SolaceDim1# = 1 to 50
        SolaceFieldName" = 'tmp:Sumtotal' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tmp:Sumtotal[SolaceDim1#],'LabelG_Consolidation',1)
      End
    
    
    SolaceViewVars('tmp:total',tmp:total,'LabelG_Consolidation',1)
    SolaceViewVars('tmp:accountnumber',tmp:accountnumber,'LabelG_Consolidation',1)
    SolaceViewVars('save_job_id',save_job_id,'LabelG_Consolidation',1)
    
      Loop SolaceDim1# = 1 to 50
        SolaceFieldName" = 'tmp:Dates' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tmp:Dates[SolaceDim1#],'LabelG_Consolidation',1)
      End
    
    
    SolaceViewVars('savepath',savepath,'LabelG_Consolidation',1)
    SolaceViewVars('save_tra_id',save_tra_id,'LabelG_Consolidation',1)
    SolaceViewVars('save_sub_id',save_sub_id,'LabelG_Consolidation',1)
    SolaceViewVars('save_cou_id',save_cou_id,'LabelG_Consolidation',1)
    SolaceViewVars('tmp:EndDate',tmp:EndDate,'LabelG_Consolidation',1)
    SolaceViewVars('tmp:SkipZero',tmp:SkipZero,'LabelG_Consolidation',1)
    SolaceViewVars('tmp:UseAccount',tmp:UseAccount,'LabelG_Consolidation',1)
    SolaceViewVars('tmp:SelectAccount',tmp:SelectAccount,'LabelG_Consolidation',1)
    SolaceViewVars('tmp:ExportType',tmp:ExportType,'LabelG_Consolidation',1)
    SolaceViewVars('tmp:exportinout',tmp:exportinout,'LabelG_Consolidation',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:UseAccount;  SolaceCtrlName = '?tmp:UseAccount';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExportType;  SolaceCtrlName = '?tmp:ExportType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExportType:Radio1;  SolaceCtrlName = '?tmp:ExportType:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExportType:Radio2;  SolaceCtrlName = '?tmp:ExportType:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SelectAccount;  SolaceCtrlName = '?tmp:SelectAccount';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupAccountNumber;  SolaceCtrlName = '?LookupAccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate:Prompt;  SolaceCtrlName = '?tmp:StartDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate;  SolaceCtrlName = '?tmp:StartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate:Prompt;  SolaceCtrlName = '?tmp:EndDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate;  SolaceCtrlName = '?tmp:EndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:exportinout;  SolaceCtrlName = '?tmp:exportinout';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:exportinout:Radio1;  SolaceCtrlName = '?tmp:exportinout:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:exportinout:Radio2;  SolaceCtrlName = '?tmp:exportinout:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Export;  SolaceCtrlName = '?Export';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('LabelG_Consolidation')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'LabelG_Consolidation')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:UseAccount
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:SUBTRACC.Open
  SELF.FilesOpened = True
  tmp:StartDate   = Today()
  tmp:EndDate     = Today()
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  IF ?tmp:SelectAccount{Prop:Tip} AND ~?LookupAccountNumber{Prop:Tip}
     ?LookupAccountNumber{Prop:Tip} = 'Select ' & ?tmp:SelectAccount{Prop:Tip}
  END
  IF ?tmp:SelectAccount{Prop:Msg} AND ~?LookupAccountNumber{Prop:Msg}
     ?LookupAccountNumber{Prop:Msg} = 'Select ' & ?tmp:SelectAccount{Prop:Msg}
  END
  IF ?tmp:UseAccount{Prop:Checked} = True
    ENABLE(?tmp:SelectAccount)
    ENABLE(?Prompt3)
    ENABLE(?LookupAccountNumber)
  END
  IF ?tmp:UseAccount{Prop:Checked} = False
    DISABLE(?tmp:SelectAccount)
    DISABLE(?Prompt3)
    DISABLE(?LookupAccountNumber)
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SUBTRACC.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LabelG_Consolidation',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    SelectSubAccount
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:UseAccount
      IF ?tmp:UseAccount{Prop:Checked} = True
        ENABLE(?tmp:SelectAccount)
        ENABLE(?Prompt3)
        ENABLE(?LookupAccountNumber)
      END
      IF ?tmp:UseAccount{Prop:Checked} = False
        DISABLE(?tmp:SelectAccount)
        DISABLE(?Prompt3)
        DISABLE(?LookupAccountNumber)
      END
      ThisWindow.Reset
    OF ?tmp:ExportType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ExportType, Accepted)
      Case tmp:ExportType
          Of 1
              ?tmp:StartDate:prompt{prop:text} = 'Date'
              hide(?tmp:EndDate:prompt)
              hide(?tmp:EndDate)
              hide(?popcalendar:2)
              UnHide(?tmp:StartDate:prompt)
              UnHide(?tmp:StartDate)
              UnHide(?popcalendar)
          Of 2
              ?tmp:StartDate:prompt{prop:text} = ' Start Date'
              Unhide(?tmp:EndDate:prompt)
              Unhide(?tmp:EndDate)
              Unhide(?tmp:StartDate:prompt)
              Unhide(?tmp:StartDate)
              Unhide(?popcalendar)
              Unhide(?popcalendar:2)
      End!Case tmp:ExportType
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ExportType, Accepted)
    OF ?tmp:SelectAccount
      IF tmp:SelectAccount OR ?tmp:SelectAccount{Prop:Req}
        sub:Account_Number = tmp:SelectAccount
        !Save Lookup Field Incase Of error
        look:tmp:SelectAccount        = tmp:SelectAccount
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:SelectAccount = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            tmp:SelectAccount = look:tmp:SelectAccount
            SELECT(?tmp:SelectAccount)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupAccountNumber
      ThisWindow.Update
      sub:Account_Number = tmp:SelectAccount
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:SelectAccount = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?tmp:SelectAccount)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:SelectAccount)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Export
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export, Accepted)
      Case tmp:exportinout
          Of 0
      
              error# = 0
              If tmp:Exporttype <> 1 And tmp:Exporttype <> 2
                  Case MessageEx('You must select a Consolidation Type.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End!If tmp:Exporttype <> 1 And tmp:Exporttype <> 2
              If tmp:EndDate - tmp:startDate > 49 And error# = 0 And tmp:exporttype = 2
                  Case MessageEx('You cannot have a Date Range greater than 49 days.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End!If tmp:EndDate - tmp:startDate > 49
      
              If tmp:exporttype = 2 And error# = 0
                  Case MessageEx('Warning! The Summary will take a LONG time to completed.<13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                      Of 2 ! &No Button
                          error# = 1
                  End!Case MessageEx
              End!If tmp:exporttype = 2 And error# = 0
      
              If error# = 0
                  savepath = path()
      
                  set(defaults)
                  access:defaults.next()
                  If def:exportpath <> ''
                      glo:file_name = Clip(def:exportpath) & '\LBLGCONS.CSV'
                  Else!If def:exportpath <> ''
                      glo:file_name = 'C:\LBLGCONS.CSV'
                  End!If def:exportpath <> ''
      
                  if not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
                              file:keepdir + file:noerror + file:longname)
                      !failed
                      setpath(savepath)
                  else!if not filedialog
                      !found
                      setpath(savepath)
                      thiswindow.reset(1)
      
                              
                      glo:file_name2   = 'L' & Format(Clock(),@n07) & '.TMP'
                      access:lablgtmp.open()
                      access:lablgtmp.usefile()
      
              ! COUNT RECORDS
                      setcursor(cursor:wait)
                      count_records# = 0
                      save_cou_id = access:courier.savefile()
                      access:courier.clearkey(cou:courier_type_key)
                      cou:courier_type = 'LABEL G'
                      set(cou:courier_type_key,cou:courier_type_key)
                      loop
                          if access:courier.next()
                             break
                          end !if
                          if cou:courier_type <> 'LABEL G'      |
                              then break.  ! end if
                          save_job_id = access:jobs.savefile()
                          access:jobs.clearkey(job:DateDespatchKey)
                          job:courier         = cou:courier
                          job:date_despatched = tmp:StartDate
                          set(job:DateDespatchKey,job:DateDespatchKey)
                          loop
                              if access:jobs.next()
                                 break
                              end !if
                              if job:courier         <> cou:courier
                                  Break
                              End!if job:courier         <> cou:courier
                              Case tmp:exporttype
                                  Of 1 !Detail
                                      If job:date_despatched <> tmp:StartDate
                                          Break
                                      End!If job:date_despatched <> tmp:StartDate
                                  Of 2 !Summary
                                      If job:date_despatched > tmp:EndDate
                                          Break
                                      End!If job:date_despatched > tmp:EndDate
                              End!Case tmp:exporttype
                              If job:account_number <> tmp:selectaccount and tmp:useaccount = 1
                                  Cycle
                              End!If job:account_number <> tmp:selectaccount
                              count_records# += 1
                          end !loop
                          access:jobs.restorefile(save_job_id)
      
                          save_job_id = access:jobs.savefile()
                          access:jobs.clearkey(job:DateDespLoaKey)
                          job:loan_courier    = cou:courier
                          job:loan_despatched = tmp:StartDate
                          set(job:DateDespLoaKey,job:DateDespLoaKey)
                          loop
                              if access:jobs.next()
                                 break
                              end !if
                              if job:loan_courier         <> cou:courier
                                  Break
                              End!if job:courier         <> cou:courier
                              Case tmp:exporttype
                                  Of 1 !Detail
                                      If job:loan_despatched <> tmp:StartDate
                                          Break
                                      End!If job:date_despatched <> tmp:StartDate
                                  Of 2 !Summary
                                      If job:loan_despatched > tmp:EndDate
                                          Break
                                      End!If job:date_despatched > tmp:EndDate
                              End!Case tmp:exporttype
                              If job:account_number <> tmp:selectaccount and tmp:useaccount = 1
                                  Cycle
                              End!If job:account_number <> tmp:selectaccount
                              count_records# += 1
      
                          end !loop
                          access:jobs.restorefile(save_job_id)
      
                          save_job_id = access:jobs.savefile()
                          access:jobs.clearkey(job:DateDespExcKey)
                          job:exchange_courier    = cou:courier
                          job:exchange_despatched = tmp:StartDate
                          set(job:DateDespExcKey,job:DateDespExcKey)
                          loop
                              if access:jobs.next()
                                 break
                              end !if
                              if job:exchange_courier         <> cou:courier
                                  Break
                              End!if job:courier         <> cou:courier
                              Case tmp:exporttype
                                  Of 1 !Detail
                                      If job:Exchange_despatched <> tmp:StartDate
                                          Break
                                      End!If job:date_despatched <> tmp:StartDate
                                  Of 2 !Summary
                                      If job:Exchange_despatched > tmp:EndDate
                                          Break
                                      End!If job:date_despatched > tmp:EndDate
                              End!Case tmp:exporttype
                              If job:account_number <> tmp:selectaccount and tmp:useaccount = 1
                                  Cycle
                              End!If job:account_number <> tmp:selectaccount
                              count_records# += 1                
                          end !loop
                          access:jobs.restorefile(save_job_id)
      
                      end !loop
                      access:courier.restorefile(save_cou_id)
                      setcursor()
      
              ! COUNT RECORDS - END
      
                      recordspercycle     = 25
                      recordsprocessed    = 0
                      percentprogress     = 0
                      open(progresswindow)
                      progress:thermometer    = 0
                      ?progress:pcttext{prop:text} = '0% Completed'
                      recordstoprocess    = count_records#
      
                      save_cou_id = access:courier.savefile()
                      access:courier.clearkey(cou:courier_type_key)
                      cou:courier_type = 'LABEL G'
                      set(cou:courier_type_key,cou:courier_type_key)
                      loop
                          if access:courier.next()
                             break
                          end !if
                          if cou:courier_type <> 'LABEL G'      |
                              then break.  ! end if
                          save_job_id = access:jobs.savefile()
                          access:jobs.clearkey(job:DateDespatchKey)
                          job:courier         = cou:courier
                          job:date_despatched = tmp:StartDate
                          set(job:DateDespatchKey,job:DateDespatchKey)
                          loop
                              if access:jobs.next()
                                 break
                              end !if
                              if job:courier         <> cou:courier
                                  Break
                              End!if job:courier         <> cou:courier
                              Case tmp:exporttype
                                  Of 1 !Detail
                                      If job:date_despatched <> tmp:StartDate
                                          Break
                                      End!If job:date_despatched <> tmp:StartDate
                                  Of 2 !Summary
                                      If job:date_despatched > tmp:EndDate
                                          Break
                                      End!If job:date_despatched > tmp:EndDate
                              End!Case tmp:exporttype
                              If job:account_number <> tmp:selectaccount and tmp:useaccount = 1
                                  Cycle
                              End!If job:account_number <> tmp:selectaccount
                              Do GetNextRecord2
                              cancelcheck# += 1
                              If cancelcheck# > (RecordsToProcess/100)
                                  Do cancelcheck
                                  If tmp:cancel = 1
                                      Break
                                  End!If tmp:cancel = 1
                                  cancelcheck# = 0
                              End!If cancelcheck# > 50
      
      
                              do_insert# = 0
                              Case tmp:exporttype
                                  Of 1 !Detailed
                                      do_insert# = 1
                                  Of 2 !Summary
                                      access:lablgtmp.clearkey(lab:datekey)
                                      lab:date      = job:date_despatched
                                      lab:accountno = job:account_number
                                      lab:postcode  = job:postcode_delivery
                                      if access:lablgtmp.tryfetch(lab:datekey) = Level:Benign
                                          lab:count   += 1
                                          access:lablgtmp.update()
                                      Else!if access:lablgtmp.tryfetch(lab:datekey) = Level:Benign
                                          do_insert# = 1
                                      End!if access:lablgtmp.tryfetch(lab:datekey) = Level:Benign
                              End!Case tmp:exporttype
                              If do_insert# = 1
                                  get(lablgtmp,0)
                                  if access:lablgtmp.primerecord() = Level:Benign
                                      lab:accountno   = job:account_number
                                      access:subtracc.clearkey(sub:account_number_key)
                                      sub:account_number = job:account_number
                                      if access:subtracc.fetch(sub:account_number_key) = level:benign
                                          lab:accountname = sub:company_name
                                      end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
                                      lab:postcode    = job:postcode_Delivery
                                      lab:date        = job:date_despatched
                                      lab:jobnumber   = job:ref_number
                                      lab:consignmentno   = job:consignment_number
                                      lab:count           = 1
                                      if access:lablgtmp.insert()
                                         access:lablgtmp.cancelautoinc()
                                      end
                                  End!if access:lablgtmp.primerecord() = Level:Benign
                              End!If do_insert# = 1
      
                          end !loop
                          access:jobs.restorefile(save_job_id)
      
                          save_job_id = access:jobs.savefile()
                          access:jobs.clearkey(job:DateDespLoaKey)
                          job:loan_courier    = cou:courier
                          job:loan_despatched = tmp:StartDate
                          set(job:DateDespLoaKey,job:DateDespLoaKey)
                          loop
                              if access:jobs.next()
                                 break
                              end !if
                              if job:loan_courier         <> cou:courier
                                  Break
                              End!if job:courier         <> cou:courier
                              Case tmp:exporttype
                                  Of 1 !Detail
                                      If job:loan_despatched <> tmp:StartDate
                                          Break
                                      End!If job:date_despatched <> tmp:StartDate
                                  Of 2 !Summary
                                      If job:loan_despatched > tmp:EndDate
                                          Break
                                      End!If job:date_despatched > tmp:EndDate
                              End!Case tmp:exporttype
                              If job:account_number <> tmp:selectaccount and tmp:useaccount = 1
                                  Cycle
                              End!If job:account_number <> tmp:selectaccount
                              Do GetNextRecord2
                              cancelcheck# += 1
                              If cancelcheck# > (RecordsToProcess/100)
                                  Do cancelcheck
                                  If tmp:cancel = 1
                                      Break
                                  End!If tmp:cancel = 1
                                  cancelcheck# = 0
                              End!If cancelcheck# > 50
      
                              do_insert# = 0
                              Case tmp:exporttype
                                  Of 1 !Detailed
                                      do_insert# = 1
                                  Of 2 !Summary
                                      access:lablgtmp.clearkey(lab:datekey)
                                      lab:date      = job:loan_despatched
                                      lab:accountno = job:account_number
                                      lab:postcode  = job:postcode_delivery
                                      if access:lablgtmp.tryfetch(lab:datekey) = Level:Benign
                                          lab:count   += 1
                                          access:lablgtmp.update()
                                      Else!if access:lablgtmp.tryfetch(lab:datekey) = Level:Benign
                                          do_insert# = 1
                                      End!if access:lablgtmp.tryfetch(lab:datekey) = Level:Benign
                              End!Case tmp:exporttype
                              If do_insert# = 1
                                  get(lablgtmp,0)
                                  if access:lablgtmp.primerecord() = Level:Benign
                                      lab:accountno   = job:account_number
                                      access:subtracc.clearkey(sub:account_number_key)
                                      sub:account_number = job:account_number
                                      if access:subtracc.fetch(sub:account_number_key) = level:benign
                                          lab:accountname = sub:company_name
                                      end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
                                      lab:postcode    = job:postcode_Delivery
                                      lab:date        = job:loan_despatched
                                      lab:jobnumber   = job:ref_number
                                      lab:consignmentno   = job:loan_consignment_number
                                      lab:count           = 1
                                      if access:lablgtmp.insert()
                                         access:lablgtmp.cancelautoinc()
                                      end
                                  End!if access:lablgtmp.primerecord() = Level:Benign
                              End!If do_insert# = 1
      
      
                          end !loop
                          access:jobs.restorefile(save_job_id)
      
                          save_job_id = access:jobs.savefile()
                          access:jobs.clearkey(job:DateDespExcKey)
                          job:exchange_courier    = cou:courier
                          job:exchange_despatched = tmp:StartDate
                          set(job:DateDespExcKey,job:DateDespExcKey)
                          loop
                              if access:jobs.next()
                                 break
                              end !if
                              if job:exchange_courier         <> cou:courier
                                  Break
                              End!if job:courier         <> cou:courier
                              Case tmp:exporttype
                                  Of 1 !Detail
                                      If job:Exchange_despatched <> tmp:StartDate
                                          Break
                                      End!If job:date_despatched <> tmp:StartDate
                                  Of 2 !Summary
                                      If job:Exchange_despatched > tmp:EndDate
                                          Break
                                      End!If job:date_despatched > tmp:EndDate
                              End!Case tmp:exporttype
                              If job:account_number <> tmp:selectaccount and tmp:useaccount = 1
                                  Cycle
                              End!If job:account_number <> tmp:selectaccount
                              Do GetNextRecord2
                              cancelcheck# += 1
                              If cancelcheck# > (RecordsToProcess/100)
                                  Do cancelcheck
                                  If tmp:cancel = 1
                                      Break
                                  End!If tmp:cancel = 1
                                  cancelcheck# = 0
                              End!If cancelcheck# > 50
      
                              do_insert# = 0
                              Case tmp:exporttype
                                  Of 1 !Detailed
                                      do_insert# = 1
                                  Of 2 !Summary
                                      access:lablgtmp.clearkey(lab:datekey)
                                      lab:date      = job:exchange_despatched
                                      lab:accountno = job:account_number
                                      lab:postcode  = job:postcode_delivery
                                      if access:lablgtmp.tryfetch(lab:datekey) = Level:Benign
                                          lab:count   += 1
                                          access:lablgtmp.update()
                                      Else!if access:lablgtmp.tryfetch(lab:datekey) = Level:Benign
                                          do_insert# = 1
                                      End!if access:lablgtmp.tryfetch(lab:datekey) = Level:Benign
                              End!Case tmp:exporttype
                              If do_insert# = 1
                                  get(lablgtmp,0)
                                  if access:lablgtmp.primerecord() = Level:Benign
                                      lab:accountno   = job:account_number
                                      access:subtracc.clearkey(sub:account_number_key)
                                      sub:account_number = job:account_number
                                      if access:subtracc.fetch(sub:account_number_key) = level:benign
                                          lab:accountname = sub:company_name
                                      end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
                                      lab:postcode    = job:postcode_Delivery
                                      lab:date        = job:Exchange_despatched
                                      lab:jobnumber   = job:ref_number
                                      lab:consignmentno   = job:exchange_consignment_number
                                      lab:count           = 1
                                      if access:lablgtmp.insert()
                                         access:lablgtmp.cancelautoinc()
                                      end
                                  End!if access:lablgtmp.primerecord() = Level:Benign
                              End!If do_insert# = 1
                          end !loop
                          access:jobs.restorefile(save_job_id)
      
                      end !loop
                      access:courier.restorefile(save_cou_id)
      
                      Remove(expgendm)
                      If error()
                          access:expgendm.close()
                          Remove(expgendm)
                      End!If error()
                      access:expgendm.open()
                      access:expgendm.usefile()
      
                      RecordsToProcess    = Records(lablgtmp)
      
                      Case tmp:ExportType
                          Of 1 !Detailed
                              Clear(exp:record)
                              exp:line1[1]    = 'Account Number'
                              exp:line1[2]    = 'Account Name'
                              exp:line1[3]    = 'Date'
                              exp:line1[4]    = 'Job Number'
                              exp:line1[5]    = 'Consignment Number'
                              exp:line1[6]    = 'Postcode'
                              access:expgendm.insert()
                              tmp:total   = 0
                              save_lab_id = access:lablgtmp.savefile()
                              access:lablgtmp.clearkey(lab:datekey)
                              set(lab:datekey)
                              loop
                                  if access:lablgtmp.next()
                                     break
                                  end !if
                                  do getnextrecord2
                                  cancelcheck# += 1
                                  if cancelcheck# > (recordstoprocess/100)
                                      do cancelcheck
                                      if tmp:cancel = 1
                                          break
                                      end!if tmp:cancel = 1
                                      cancelcheck# = 0
                                  end!if cancelcheck# > 50
      
                                  exp:line1[1]    = lab:accountno
                                  exp:line1[2]    = lab:accountname
                                  exp:line1[3]    = Format(lab:date,@d6)
                                  exp:line1[4]    = lab:jobnumber
                                  exp:line1[5]    = lab:consignmentno
                                  exp:line1[6]    = lab:postcode
                                  access:expgendm.insert()
                                  tmp:total       += 1
                              end !loop
                              access:lablgtmp.restorefile(save_lab_id)
      
                              Clear(exp:record)
                              exp:line1[1]    = 'Total:'
                              exp:line1[2]    = tmp:total
                              access:expgendm.insert()
      
                          Of 2 !Summary
                              RecordsToProcess    = tmp:EndDate - tmp:StartDate
      
                              Clear(exp:record)
                              y# = 3
                              exp:line1[1]    = 'Account Number'
                              exp:line1[2]    = 'Account Name'
                              exp:line1[3]    = 'Postcode'
                              Loop x# = tmp:StartDate  To tmp:EndDate
                                  y# += 1
                                  exp:line1[y#]   = Format(x#,@d6)
                                  tmp:Dates[y#]   = x#
                              End!Loop x# = 2 To (tmp:EndDate - tmp:StartDate) + 1
                              access:expgendm.insert()
                              tmp:total   = 0
                              Clear(exp:record)
                              Loop x# = 4 To (tmp:EndDate - tmp:StartDate) + 4
                                  do getnextrecord2
                                  cancelcheck# += 1
                                  if cancelcheck# > (recordstoprocess/100)
                                      do cancelcheck
                                      if tmp:cancel = 1
                                          break
                                      end!if tmp:cancel = 1
                                      cancelcheck# = 0
                                  end!if cancelcheck# > 50
      
                                  sav:accountno   = ''
                                  sav:postcode    = ''
                                  save_lab_id = access:lablgtmp.savefile()
                                  access:lablgtmp.clearkey(lab:datekey)
                                  lab:date      = tmp:StartDate + x# - 4
                                  set(lab:datekey,lab:datekey)
                                  loop
                                      if access:lablgtmp.next()
                                         break
                                      end !if
                                      If lab:date      <> tmp:StartDate + x# - 4      |
                                          then break.  ! end if
                                      Clear(exp:record)
                                      exp:line1[1]    = lab:accountno
                                      exp:line1[2]    = lab:accountname
                                      exp:line1[3]    = lab:postcode
                                      exp:line1[x#]   = lab:count
                                      access:expgendm.insert()
                                      tmp:total       += 1
                                      tmp:sumtotal[x#]    += lab:count
                                  end !loop
                                  access:lablgtmp.restorefile(save_lab_id)
      
                              End!Loop x# = 3 To (tmp:EndDate - tmp:StartDate) + 3
                              clear(exp:record)
                              exp:line1[1] = 'Totals:'
                              Loop x# = 4 To (tmp:EndDate - tmp:StartDate) + 4
                                  exp:line1[x#]    = tmp:sumtotal[x#]
                              End!Loop x# = 4 To (tmp:EndDate - tmp:StartDate) + 4
                              access:expgendm.insert()
      
                      End!Case tmp:ExportType
      
                      access:lablgtmp.close()
                      access:expgendm.close()
                      Remove(lablgtmp)
                      IF error()
                          access:lablgtmp.close()
                          Remove(Lablgtmp)
                      End!IF error()
                      
      
                  !---after routine
                      do endprintrun
                      close(progresswindow)
      
                      If tmp:total = 0
                          Case MessageEx('There are no records that match the selected criteria.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                      Else!If tmp:total = 0
                          Case MessageEx('Export Completed.','ServiceBase 2000',|
                                         'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                      End!If tmp:total = 0
                  end!if not filedialog
      
              End!If error# = 0
          Of 1
              thiswindow.reset(1)
              error# = 0
              If tmp:Exporttype <> 1 And tmp:Exporttype <> 2
                  Case MessageEx('You must select a Consolidation Type.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End!If tmp:Exporttype <> 1 And tmp:Exporttype <> 2
              If tmp:EndDate - tmp:startDate > 49 And error# = 0 And tmp:exporttype = 2
                  Case MessageEx('You cannot have a Date Range greater than 49 days.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End!If tmp:EndDate - tmp:startDate > 49
      
              If tmp:exporttype = 2 And error# = 0
                  Case MessageEx('Warning! The Summary will take a LONG time to completed.<13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                      Of 2 ! &No Button
                          error# = 1
                  End!Case MessageEx
              End!If tmp:exporttype = 2 And error# = 0
      
              If error# = 0
                  savepath = path()
      
                  set(defaults)
                  access:defaults.next()
                  If def:exportpath <> ''
                      glo:file_name = Clip(def:exportpath) & '\LBINCONS.CSV'
                  Else!If def:exportpath <> ''
                      glo:file_name = 'C:\LBINCONS.CSV'
                  End!If def:exportpath <> ''
      
                  if not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
                              file:keepdir + file:noerror + file:longname)
                      !failed
                      setpath(savepath)
                  else!if not filedialog
                      !found
                      setpath(savepath)
                      thiswindow.reset(1)
      
                              
                      glo:file_name2   = 'L' & Format(Clock(),@n07) & '.TMP'
                      access:lablgtmp.open()
                      access:lablgtmp.usefile()
      
                  ! COUNT RECORDS
                      recordspercycle     = 25
                      recordsprocessed    = 0
                      percentprogress     = 0
                      open(progresswindow)
                      progress:thermometer    = 0
                      ?progress:pcttext{prop:hide} = 1
                      ?progress:userstring{prop:Text} = 'Preparing To Run..'
                      recordstoprocess    = 5000
                      count_records# = 0
                              
                      save_job_id = access:jobs.savefile()
                      access:jobs.clearkey(job:date_booked_key)
                      job:date_booked = tmp:startdate
                      set(job:date_booked_key,job:date_booked_key)
                      loop
                          if access:jobs.next()
                             break
                          end !if
                          Case tmp:exporttype
                              Of 1 !Detail
                                  If job:date_booked <> tmp:StartDate
                                      Break
                                  End!If job:date_despatched <> tmp:StartDate
                              Of 2 !Summary
                                  If job:date_booked > tmp:EndDate
                                      Break
                                  End!If job:date_despatched > tmp:EndDate
                          End!Case tmp:exporttype
                              
                          If job:incoming_courier <> ''     
                              access:courier.clearkey(cou:courier_key)
                              cou:courier    = job:incoming_courier            
                              If access:courier.tryfetch(cou:courier_key) = Level:Benign
                                  IF cou:courier_type = 'LABEL G'
                                      If job:account_number <> tmp:selectaccount and tmp:useaccount = 1
                                          Cycle
                                      End!If job:account_number <> tmp:selectaccount
                                      Do GetNextRecord2
                                      count_records# += 1
                                      If count_records# >5000
                                          count_records# = Records(jobs)
                                          Break
                                      End!If count_records# >4000
                                  End!IF cou:couirer_type = 'LABEL G'
                              End!If access:courier.tryfetch(cou:courier_type_key)                            
                          End!If job:incoming_courier <> ''                 
                      end !loop
                      access:jobs.restorefile(save_job_id)
      
                  ! COUNT RECORDS - END
      
                      recordspercycle     = 25
                      recordsprocessed    = 0
                      percentprogress     = 0
                      
                      progress:thermometer    = 0
                      ?progress:pcttext{prop:hide} = 0
                      ?progress:pcttext{prop:text} = '0% Completed'
                      ?progress:userstring{prop:Text} = 'Running..'
                      recordstoprocess    = count_records#
                          
                      save_job_id = access:jobs.savefile()
                      access:jobs.clearkey(job:date_booked_key)
                      job:date_booked = tmp:startdate
                      set(job:date_booked_key,job:date_booked_key)
                      loop
                          if access:jobs.next()
                             break
                          end !if
                          Case tmp:exporttype
                              Of 1 !Detail
                                  If job:date_booked <> tmp:StartDate
                                      Break
                                  End!If job:date_despatched <> tmp:StartDate
                              Of 2 !Summary
                                  If job:date_booked > tmp:EndDate
                                      Break
                                  End!If job:date_despatched > tmp:EndDate
                          End!Case tmp:exporttype
      
                          If job:incoming_courier <> ''     
                              access:courier.clearkey(cou:courier_key)
                              cou:courier    = job:incoming_courier            
                              If access:courier.tryfetch(cou:courier_key) = Level:Benign
                                  IF cou:courier_type = 'LABEL G'
      Compile('***',Debug=1)
          Message('Job Number: ' & job:Ref_number &|
                  '|Incoming Courier: ' & job:incoming_courier &|
                  '|Job Account Number: ' & job:account_number & |
                  '|Select ACcount: ' & tmp:selectaccount,'Debug Message',icon:exclamation)
      ***
      
                                      If job:account_number <> tmp:selectaccount and tmp:useaccount = 1
                                          Cycle
                                      End!If job:account_number <> tmp:selectaccount
      Compile('***',Debug=1)
          Message('Should be importing. ','Debug Message',icon:exclamation)
      ***
      
                                      Do GetNextRecord2
                                      cancelcheck# += 1
                                      If cancelcheck# > (RecordsToProcess/100)
                                          Do cancelcheck
                                          If tmp:cancel = 1
                                              Break
                                          End!If tmp:cancel = 1
                                          cancelcheck# = 0
                                      End!If cancelcheck# > 50
                      
                                      do_insert# = 0
                                      Case tmp:exporttype
                                          Of 1 !Detailed
                                              do_insert# = 1
                                          Of 2 !Summary
                                              access:lablgtmp.clearkey(lab:datekey)
                                              lab:date      = job:date_booked
                                              lab:accountno = job:account_number
                                              lab:postcode  = job:postcode_delivery
                                              if access:lablgtmp.tryfetch(lab:datekey) = Level:Benign
                                                  lab:count   += 1
                                                  access:lablgtmp.update()
                                              Else!if access:lablgtmp.tryfetch(lab:datekey) = Level:Benign
                                                  do_insert# = 1
                                              End!if access:lablgtmp.tryfetch(lab:datekey) = Level:Benign
                                      End!Case tmp:exporttype
                                      If do_insert# = 1
                                          get(lablgtmp,0)
                                          if access:lablgtmp.primerecord() = Level:Benign
                                              lab:accountno   = job:account_number
                                              access:subtracc.clearkey(sub:account_number_key)
                                              sub:account_number = job:account_number
                                              if access:subtracc.fetch(sub:account_number_key) = level:benign
                                                  lab:accountname = sub:company_name
                                              end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
                                              lab:postcode    = job:postcode_Delivery
                                              lab:date        = job:date_booked
                                              lab:jobnumber   = job:ref_number
                                              lab:consignmentno   = job:incoming_consignment_number
                                              lab:count           = 1
                                              if access:lablgtmp.insert()
                                                 access:lablgtmp.cancelautoinc()
                                              end
                                          End!if access:lablgtmp.primerecord() = Level:Benign
                                      End!If do_insert# = 1
                                              
                                  End!IF cou:couirer_type = 'LABEL G'
                              End!If access:courier.tryfetch(cou:courier_type_key)
                          End!If job:incoming_courier <> ''
                      end !loop
                      access:jobs.restorefile(save_job_id)
                      
                      Remove(expgendm)
                      If error()
                          access:expgendm.close()
                          Remove(expgendm)
                      End!If error()
                      access:expgendm.open()
                      access:expgendm.usefile()
      
                      RecordsToProcess    = Records(lablgtmp)
      
                      Case tmp:ExportType
                          Of 1 !Detailed
                              Clear(exp:record)
                              exp:line1[1]    = 'Account Number'
                              exp:line1[2]    = 'Account Name'
                              exp:line1[3]    = 'Date'
                              exp:line1[4]    = 'Job Number'
                              exp:line1[5]    = 'Consignment Number'
                              exp:line1[6]    = 'Postcode'
                              access:expgendm.insert()
                              tmp:total   = 0
                              save_lab_id = access:lablgtmp.savefile()
                              access:lablgtmp.clearkey(lab:datekey)
                              set(lab:datekey)
                              loop
                                  if access:lablgtmp.next()
                                     break
                                  end !if
                                  do getnextrecord2
                                  cancelcheck# += 1
                                  if cancelcheck# > (recordstoprocess/100)
                                      do cancelcheck
                                      if tmp:cancel = 1
                                          break
                                      end!if tmp:cancel = 1
                                      cancelcheck# = 0
                                  end!if cancelcheck# > 50
      
                                  exp:line1[1]    = lab:accountno
                                  exp:line1[2]    = lab:accountname
                                  exp:line1[3]    = Format(lab:date,@d6)
                                  exp:line1[4]    = lab:jobnumber
                                  exp:line1[5]    = lab:consignmentno
                                  exp:line1[6]    = lab:postcode
                                  access:expgendm.insert()
                                  tmp:total       += 1
                              end !loop
                              access:lablgtmp.restorefile(save_lab_id)
      
                              Clear(exp:record)
                              exp:line1[1]    = 'Total:'
                              exp:line1[2]    = tmp:total
                              access:expgendm.insert()
      
                          Of 2 !Summary
                              RecordsToProcess    = tmp:EndDate - tmp:StartDate
      
                              Clear(exp:record)
                              y# = 3
                              exp:line1[1]    = 'Account Number'
                              exp:line1[2]    = 'Account Name'
                              exp:line1[3]    = 'Postcode'
                              Loop x# = tmp:StartDate  To tmp:EndDate
                                  y# += 1
                                  exp:line1[y#]   = Format(x#,@d6)
                                  tmp:Dates[y#]   = x#
                              End!Loop x# = 2 To (tmp:EndDate - tmp:StartDate) + 1
                              access:expgendm.insert()
                              tmp:total   = 0
                              Clear(exp:record)
                              Loop x# = 4 To (tmp:EndDate - tmp:StartDate) + 4
                                  do getnextrecord2
                                  cancelcheck# += 1
                                  if cancelcheck# > (recordstoprocess/100)
                                      do cancelcheck
                                      if tmp:cancel = 1
                                          break
                                      end!if tmp:cancel = 1
                                      cancelcheck# = 0
                                  end!if cancelcheck# > 50
      
                                  sav:accountno   = ''
                                  sav:postcode    = ''
                                  save_lab_id = access:lablgtmp.savefile()
                                  access:lablgtmp.clearkey(lab:datekey)
                                  lab:date      = tmp:StartDate + x# - 4
                                  set(lab:datekey,lab:datekey)
                                  loop
                                      if access:lablgtmp.next()
                                         break
                                      end !if
                                      If lab:date      <> tmp:StartDate + x# - 4      |
                                          then break.  ! end if
                                      Clear(exp:record)
                                      exp:line1[1]    = lab:accountno
                                      exp:line1[2]    = lab:accountname
                                      exp:line1[3]    = lab:postcode
                                      exp:line1[x#]   = lab:count
                                      access:expgendm.insert()
                                      tmp:total       += 1
                                      tmp:sumtotal[x#]    += lab:count
                                  end !loop
                                  access:lablgtmp.restorefile(save_lab_id)
      
                              End!Loop x# = 3 To (tmp:EndDate - tmp:StartDate) + 3
                              clear(exp:record)
                              exp:line1[1] = 'Totals:'
                              Loop x# = 4 To (tmp:EndDate - tmp:StartDate) + 4
                                  exp:line1[x#]    = tmp:sumtotal[x#]
                              End!Loop x# = 4 To (tmp:EndDate - tmp:StartDate) + 4
                              access:expgendm.insert()
      
                      End!Case tmp:ExportType
      
                      access:lablgtmp.close()
                      access:expgendm.close()
                      Remove(lablgtmp)
                      IF error()
                          access:lablgtmp.close()
                          Remove(Lablgtmp)
                      End!IF error()
                      
      
                  !---after routine
                      do endprintrun
                      close(progresswindow)
      
                      If tmp:total = 0
                          Case MessageEx('There are no records that match the selected criteria.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                      Else!If tmp:total = 0
                          Case MessageEx('Export Completed.','ServiceBase 2000',|
                                         'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                      End!If tmp:total = 0
                  end!if not filedialog
      
              End!If error# = 0
      End!Case tmp:exportinout
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'LabelG_Consolidation')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?tmp:SelectAccount
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SelectAccount, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Account Number')
             Post(Event:Accepted,?LookupAccountNumber)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupAccountNumber)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SelectAccount, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

