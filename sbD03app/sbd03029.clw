

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03029.INC'),ONCE        !Local module procedure declarations
                     END


Income_Report_Criteria PROCEDURE                      !Generated from procedure template - Window

FilesOpened          BYTE
tag_temp             STRING(1)
trade_account_temp   BYTE(0)
manfuacturer_temp    STRING(30)
single_page_temp     STRING(3)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:AccountType      BYTE(0)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select3
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?GLO:Select6
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
FDCB8::View:FileDropCombo VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
window               WINDOW('Income Report Criteria'),AT(,,278,207),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,272,172),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Exchange Units In Channel Criteria'),USE(?Tab1)
                           PROMPT('Select Trade Account'),AT(8,8),USE(?Prompt4),FONT(,,COLOR:Navy,FONT:bold)
                           OPTION('Trade Account'),AT(13,20,111,28),USE(trade_account_temp),BOXED
                             RADIO('All'),AT(24,32),USE(?trade_account_temp:Radio1),VALUE('0')
                             RADIO('Individual'),AT(64,32),USE(?trade_account_temp:Radio2),VALUE('1')
                           END
                           OPTION,AT(12,44,20,32),USE(tmp:AccountType)
                             RADIO,AT(16,52),USE(?Option3:Radio1),VALUE('0')
                             RADIO,AT(16,64),USE(?Option3:Radio2),VALUE('1')
                           END
                           COMBO(@s40),AT(124,52,124,10),USE(GLO:Select6),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('67L(2)|M@s15@120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                           PROMPT('Header Account Number'),AT(36,52),USE(?HeadAccountNumber:Prompt)
                           PROMPT('Sub Account Number'),AT(36,64),USE(?SubAccountNumber:Prompt)
                           COMBO(@s40),AT(124,64,124,10),USE(GLO:Select3),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('60L(2)|M@s15@120L(2)|M@s30@'),DROP(10,160),FROM(Queue:FileDropCombo)
                           OPTION('Invoice Types To Include'),AT(12,88,236,48),USE(?Option2),BOXED
                           END
                           CHECK('Service Invoices'),AT(112,100),USE(GLO:Select4),VALUE('YES','NO')
                           CHECK('Retail Invoices'),AT(112,116),USE(GLO:Select5),VALUE('YES','NO')
                           PROMPT('Start Date'),AT(12,144),USE(?glo:select1:Prompt)
                           ENTRY(@d6b),AT(112,144,64,10),USE(GLO:Select1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           BUTTON,AT(184,144,10,10),USE(?PopCalendar),ICON('calenda2.ico')
                           PROMPT('End Date'),AT(12,160),USE(?glo:select2:Prompt)
                           ENTRY(@d6b),AT(112,160,64,10),USE(GLO:Select2),FONT(,,,FONT:bold,CHARSET:ANSI)
                           BUTTON,AT(184,160,10,10),USE(?PopCalendar:2),ICON('calenda2.ico')
                         END
                       END
                       BUTTON('&OK'),AT(160,184,56,16),USE(?OkButton),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(216,184,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,180,272,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?trade_account_temp{prop:Font,3} = -1
    ?trade_account_temp{prop:Color} = 15066597
    ?trade_account_temp{prop:Trn} = 0
    ?trade_account_temp:Radio1{prop:Font,3} = -1
    ?trade_account_temp:Radio1{prop:Color} = 15066597
    ?trade_account_temp:Radio1{prop:Trn} = 0
    ?trade_account_temp:Radio2{prop:Font,3} = -1
    ?trade_account_temp:Radio2{prop:Color} = 15066597
    ?trade_account_temp:Radio2{prop:Trn} = 0
    ?tmp:AccountType{prop:Font,3} = -1
    ?tmp:AccountType{prop:Color} = 15066597
    ?tmp:AccountType{prop:Trn} = 0
    ?Option3:Radio1{prop:Font,3} = -1
    ?Option3:Radio1{prop:Color} = 15066597
    ?Option3:Radio1{prop:Trn} = 0
    ?Option3:Radio2{prop:Font,3} = -1
    ?Option3:Radio2{prop:Color} = 15066597
    ?Option3:Radio2{prop:Trn} = 0
    If ?GLO:Select6{prop:ReadOnly} = True
        ?GLO:Select6{prop:FontColor} = 65793
        ?GLO:Select6{prop:Color} = 15066597
    Elsif ?GLO:Select6{prop:Req} = True
        ?GLO:Select6{prop:FontColor} = 65793
        ?GLO:Select6{prop:Color} = 8454143
    Else ! If ?GLO:Select6{prop:Req} = True
        ?GLO:Select6{prop:FontColor} = 65793
        ?GLO:Select6{prop:Color} = 16777215
    End ! If ?GLO:Select6{prop:Req} = True
    ?GLO:Select6{prop:Trn} = 0
    ?GLO:Select6{prop:FontStyle} = font:Bold
    ?HeadAccountNumber:Prompt{prop:FontColor} = -1
    ?HeadAccountNumber:Prompt{prop:Color} = 15066597
    ?SubAccountNumber:Prompt{prop:FontColor} = -1
    ?SubAccountNumber:Prompt{prop:Color} = 15066597
    If ?GLO:Select3{prop:ReadOnly} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 15066597
    Elsif ?GLO:Select3{prop:Req} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 8454143
    Else ! If ?GLO:Select3{prop:Req} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 16777215
    End ! If ?GLO:Select3{prop:Req} = True
    ?GLO:Select3{prop:Trn} = 0
    ?GLO:Select3{prop:FontStyle} = font:Bold
    ?Option2{prop:Font,3} = -1
    ?Option2{prop:Color} = 15066597
    ?Option2{prop:Trn} = 0
    ?GLO:Select4{prop:Font,3} = -1
    ?GLO:Select4{prop:Color} = 15066597
    ?GLO:Select4{prop:Trn} = 0
    ?GLO:Select5{prop:Font,3} = -1
    ?GLO:Select5{prop:Color} = 15066597
    ?GLO:Select5{prop:Trn} = 0
    ?glo:select1:Prompt{prop:FontColor} = -1
    ?glo:select1:Prompt{prop:Color} = 15066597
    If ?GLO:Select1{prop:ReadOnly} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 15066597
    Elsif ?GLO:Select1{prop:Req} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 8454143
    Else ! If ?GLO:Select1{prop:Req} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 16777215
    End ! If ?GLO:Select1{prop:Req} = True
    ?GLO:Select1{prop:Trn} = 0
    ?GLO:Select1{prop:FontStyle} = font:Bold
    ?glo:select2:Prompt{prop:FontColor} = -1
    ?glo:select2:Prompt{prop:Color} = 15066597
    If ?GLO:Select2{prop:ReadOnly} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 15066597
    Elsif ?GLO:Select2{prop:Req} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 8454143
    Else ! If ?GLO:Select2{prop:Req} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 16777215
    End ! If ?GLO:Select2{prop:Req} = True
    ?GLO:Select2{prop:Trn} = 0
    ?GLO:Select2{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
show_hide       Routine
    Case trade_account_temp
        Of 0
            ?tmp:AccountType{prop:Disable} = 1
            ?HeadAccountNumber:Prompt{prop:Disable} = 1
            ?SubAccountNumber:Prompt{prop:Disable} = 1
            ?glo:Select3{prop:Disable} = 1
            ?glo:Select6{prop:Disable} = 1
        Of 1
            ?tmp:AccountType{prop:Disable} = 0
            Post(Event:Accepted,?tmp:AccountType)
    End!Case trade_account_temp
    Display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Income_Report_Criteria',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Income_Report_Criteria',1)
    SolaceViewVars('tag_temp',tag_temp,'Income_Report_Criteria',1)
    SolaceViewVars('trade_account_temp',trade_account_temp,'Income_Report_Criteria',1)
    SolaceViewVars('manfuacturer_temp',manfuacturer_temp,'Income_Report_Criteria',1)
    SolaceViewVars('single_page_temp',single_page_temp,'Income_Report_Criteria',1)
    SolaceViewVars('tmp:AccountType',tmp:AccountType,'Income_Report_Criteria',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trade_account_temp;  SolaceCtrlName = '?trade_account_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trade_account_temp:Radio1;  SolaceCtrlName = '?trade_account_temp:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trade_account_temp:Radio2;  SolaceCtrlName = '?trade_account_temp:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:AccountType;  SolaceCtrlName = '?tmp:AccountType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option3:Radio1;  SolaceCtrlName = '?Option3:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option3:Radio2;  SolaceCtrlName = '?Option3:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select6;  SolaceCtrlName = '?GLO:Select6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?HeadAccountNumber:Prompt;  SolaceCtrlName = '?HeadAccountNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SubAccountNumber:Prompt;  SolaceCtrlName = '?SubAccountNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select3;  SolaceCtrlName = '?GLO:Select3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option2;  SolaceCtrlName = '?Option2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select4;  SolaceCtrlName = '?GLO:Select4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select5;  SolaceCtrlName = '?GLO:Select5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select1:Prompt;  SolaceCtrlName = '?glo:select1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select1;  SolaceCtrlName = '?GLO:Select1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select2:Prompt;  SolaceCtrlName = '?glo:select2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select2;  SolaceCtrlName = '?GLO:Select2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Income_Report_Criteria')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Income_Report_Criteria')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt4
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:SUBTRACC.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  glo:select1 = Deformat('1/1/1998',@d6)
  glo:select2  = Today()
  glo:select3 = ''
  glo:select4 = 'YES'
  glo:select5 = 'YES'
  Do RecolourWindow
  ?glo:select1{Prop:Alrt,255} = MouseLeft2
  ?glo:select1{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  FDCB2.Init(GLO:Select3,?GLO:Select3,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:SUBTRACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(sub:Account_Number_Key)
  FDCB2.AddField(sub:Account_Number,FDCB2.Q.sub:Account_Number)
  FDCB2.AddField(sub:Company_Name,FDCB2.Q.sub:Company_Name)
  FDCB2.AddField(sub:RecordNumber,FDCB2.Q.sub:RecordNumber)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  FDCB8.Init(GLO:Select6,?GLO:Select6,Queue:FileDropCombo:1.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo:1,Relate:TRADEACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo:1
  FDCB8.AddSortOrder(tra:Account_Number_Key)
  FDCB8.AddField(tra:Account_Number,FDCB8.Q.tra:Account_Number)
  FDCB8.AddField(tra:Company_Name,FDCB8.Q.tra:Company_Name)
  FDCB8.AddField(tra:RecordNumber,FDCB8.Q.tra:RecordNumber)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SUBTRACC.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Income_Report_Criteria',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?trade_account_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trade_account_temp, Accepted)
      Do show_hide
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trade_account_temp, Accepted)
    OF ?tmp:AccountType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AccountType, Accepted)
      Case tmp:AccountType
          Of 0
              ?HeadAccountNumber:Prompt{prop:Disable} = 0
              ?SubAccountNumber:Prompt{prop:Disable} = 1
              ?glo:Select6{prop:Disable} = 0
              ?glo:Select3{prop:Disable} = 1
      
          Of 1
              ?HeadAccountNumber:Prompt{prop:Disable} = 1
              ?SubAccountNumber:Prompt{prop:Disable} = 0
              ?glo:Select6{prop:Disable} = 1
              ?glo:Select3{prop:Disable} = 0
      
      End !tmp:AccountType
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AccountType, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select1 = TINCALENDARStyle1(GLO:Select1)
          Display(?glo:select1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select1 = TINCALENDARStyle1(GLO:Select1)
          Display(?glo:select1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      error# = 0
      If glo:select4 = 'NO' And glo:select5 = 'NO'
          MsgExButton# = MessageEx('You must select at least one Invoice Type.','ServiceBase 2000',icon:hand,'&OK',1,0,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'','',0+0+0+0,80,30,0)
          Error# = 1
      End!If glo:select4 = 'NO' And glo:select5 = 'NO'
      IF error# = 0
          Income_Report(Trade_Account_Temp,tmp:AccountType)
      End!IF error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      glo:select1 = ''
      glo:select2 = ''
      glo:select3 = ''
      glo:select4 = ''
      glo:select5 = ''
      Post(event:Closewindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Income_Report_Criteria')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?GLO:Select1
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do show_hide
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

