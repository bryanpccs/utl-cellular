

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('SBD03015.INC'),ONCE        !Local module procedure declarations
                     END


Stock_Usage_Report PROCEDURE                          !Generated from procedure template - Report

Progress:Thermometer BYTE
save_stm_id          USHORT,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:RecordsCount     REAL
tmp:printer          STRING(255)
tmp:PrintedBy        STRING(60)
endofreport          BYTE,AUTO
count_temp           REAL
FilesOpened          BYTE
used_temp            REAL
purchase_cost_total_temp REAL
line_total_temp      REAL
line_total_total_temp REAL
used_total_temp      REAL
on_order_total_temp  REAL
in_stock_total_temp  REAL
location_temp        STRING(30)
supplier_temp        STRING(30)
shelf_location_temp  STRING(30)
purchase_cost_temp   REAL
tmp:FirstModel       STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(LOCATION)
                       PROJECT(loc:Location)
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(43,42,56,16),USE(?Progress:Cancel),LEFT,ICON('cancel.gif')
                     END

report               REPORT('Stock Usage Report'),AT(396,2771,7521,8583),PRE(RPT),FONT('Arial',10,,),THOUS
                       HEADER,AT(396,479,7521,1750),USE(?unnamed:2)
                         STRING(@s30),AT(94,52),USE(def:User_Name),TRN,FONT(,14,,FONT:bold)
                         STRING('Site Location:'),AT(4896,469),USE(?String34),TRN,FONT(,8,,)
                         STRING(@s30),AT(5938,469),USE(location_temp),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(94,313,3531,198),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING('Supplier:'),AT(4896,625),USE(?String35),TRN,FONT(,8,,)
                         STRING(@s30),AT(5938,625),USE(supplier_temp),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Printed:'),AT(4896,1354),USE(?RunPrompt),TRN,FONT(,8,,)
                         STRING(@s30),AT(94,469,3531,198),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(94,625,3531,198),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING('Suppress Zeros:'),AT(4896,781),USE(?String36),TRN,FONT(,8,,)
                         STRING(@s30),AT(5938,781),USE(GLO:Select5),TRN,FONT(,8,,FONT:bold)
                         STRING('XX/XX/XXXX'),AT(5938,1354),USE(?ReportDateStamp),TRN,FONT(,8,,FONT:bold)
                         STRING(@s15),AT(94,781),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Date Range:'),AT(4896,938),USE(?String37),TRN,FONT(,8,,)
                         STRING(@d6),AT(5938,938),USE(GLO:Select1),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6),AT(6667,938),USE(GLO:Select2),TRN,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(4896,1198,625,208),USE(?String67),TRN,FONT(,8,,)
                         STRING(@s60),AT(5938,1198),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Tel: '),AT(104,1094),USE(?String15),TRN,FONT(,9,,)
                         STRING(@s15),AT(573,1094),USE(def:Telephone_Number),TRN,FONT(,9,,)
                         STRING('Fax:'),AT(104,1250),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s15),AT(573,1250),USE(def:Fax_Number),TRN,FONT(,9,,)
                         STRING(@s255),AT(573,1406,3531,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(104,1406),USE(?String16:2),TRN,FONT(,9,,)
                         STRING('Page:'),AT(4896,1510),USE(?PagePrompt),TRN,FONT(,8,,)
                         STRING(@s3),AT(5938,1510),USE(ReportPageNumber),TRN,FONT(,8,,FONT:bold)
                       END
break1                 BREAK(EndOfReport),USE(?unnamed:4)
DETAIL                   DETAIL,AT(,,,156),USE(?DetailBand)
                           STRING(@s20),AT(2719,0),USE(sto:Part_Number),TRN,FONT(,7,,)
                           STRING(@s25),AT(1313,0),USE(sto:Description),TRN,FONT(,7,,)
                           STRING(@s22),AT(104,0),USE(shelf_location_temp),TRN,FONT(,7,,)
                           STRING(@N8),AT(5000,0),USE(sto:Quantity_Stock),TRN,RIGHT,FONT(,7,,)
                           STRING(@n7),AT(5469,0),USE(sto:Quantity_On_Order),TRN,RIGHT,FONT(,7,,)
                           STRING(@n-8),AT(5833,0,469,208),USE(used_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(6354,0),USE(sto:Purchase_Cost),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(6875,0),USE(line_total_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@s30),AT(3802,0,1198,156),USE(tmp:FirstModel),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:3)
                           LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(104,104),USE(?String68),TRN,FONT(,8,,FONT:bold)
                           STRING(@s9),AT(1094,104),USE(count_temp),TRN,FONT(,8,,FONT:bold)
                           STRING(@n9),AT(4969,104),USE(in_stock_total_temp),TRN,RIGHT,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                           STRING(@n7),AT(5469,104),USE(on_order_total_temp),TRN,RIGHT,FONT(,7,,FONT:bold)
                           STRING(@n-8),AT(5885,104),USE(used_total_temp),TRN,RIGHT,FONT(,7,,FONT:bold)
                           STRING(@n10.2),AT(6354,104),USE(purchase_cost_total_temp),TRN,RIGHT,FONT(,7,,FONT:bold)
                           STRING(@n11.2),AT(6823,104),USE(line_total_total_temp),TRN,RIGHT,FONT(,7,,FONT:bold)
                         END
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:5)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11198),USE(?Image1)
                         STRING('STOCK USAGE REPORT'),AT(5104,52),USE(?String19),TRN,FONT(,14,,FONT:bold)
                         STRING('Description'),AT(1313,2083),USE(?String20),TRN,FONT(,7,,FONT:bold)
                         STRING('Location'),AT(104,2083),USE(?String21),TRN,FONT(,7,,FONT:bold)
                         STRING('In Stock'),AT(5052,2083),USE(?String25),TRN,FONT(,7,,FONT:bold)
                         STRING('On Order'),AT(5417,2083),USE(?String27),TRN,FONT(,7,,FONT:bold)
                         STRING('Used'),AT(6063,2083),USE(?String29),TRN,FONT(,7,,FONT:bold)
                         STRING('Purch Cost'),AT(6344,2083),USE(?String31),TRN,FONT(,7,,FONT:bold)
                         STRING('Total Cost'),AT(6917,2083),USE(?String47),TRN,FONT(,7,,FONT:bold)
                         STRING('First Model No'),AT(3802,2083),USE(?String21:2),TRN,FONT(,7,,FONT:bold)
                         STRING('Part Number'),AT(2719,2083),USE(?String23),TRN,FONT(,7,,FONT:bold)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepStringClass                  !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 22
save_sto_id   ushort,auto
save_shi_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Progress:UserString{prop:FontColor} = -1
    ?Progress:UserString{prop:Color} = 15066597
    ?Progress:PctText{prop:FontColor} = -1
    ?Progress:PctText{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Stock_Usage_Report',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Stock_Usage_Report',1)
    SolaceViewVars('save_stm_id',save_stm_id,'Stock_Usage_Report',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Stock_Usage_Report',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Stock_Usage_Report',1)
    SolaceViewVars('tmp:RecordsCount',tmp:RecordsCount,'Stock_Usage_Report',1)
    SolaceViewVars('tmp:printer',tmp:printer,'Stock_Usage_Report',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Stock_Usage_Report',1)
    SolaceViewVars('endofreport',endofreport,'Stock_Usage_Report',1)
    SolaceViewVars('count_temp',count_temp,'Stock_Usage_Report',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Stock_Usage_Report',1)
    SolaceViewVars('used_temp',used_temp,'Stock_Usage_Report',1)
    SolaceViewVars('purchase_cost_total_temp',purchase_cost_total_temp,'Stock_Usage_Report',1)
    SolaceViewVars('line_total_temp',line_total_temp,'Stock_Usage_Report',1)
    SolaceViewVars('line_total_total_temp',line_total_total_temp,'Stock_Usage_Report',1)
    SolaceViewVars('used_total_temp',used_total_temp,'Stock_Usage_Report',1)
    SolaceViewVars('on_order_total_temp',on_order_total_temp,'Stock_Usage_Report',1)
    SolaceViewVars('in_stock_total_temp',in_stock_total_temp,'Stock_Usage_Report',1)
    SolaceViewVars('location_temp',location_temp,'Stock_Usage_Report',1)
    SolaceViewVars('supplier_temp',supplier_temp,'Stock_Usage_Report',1)
    SolaceViewVars('shelf_location_temp',shelf_location_temp,'Stock_Usage_Report',1)
    SolaceViewVars('purchase_cost_temp',purchase_cost_temp,'Stock_Usage_Report',1)
    SolaceViewVars('tmp:FirstModel',tmp:FirstModel,'Stock_Usage_Report',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Progress:Thermometer;  SolaceCtrlName = '?Progress:Thermometer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:UserString;  SolaceCtrlName = '?Progress:UserString';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:PctText;  SolaceCtrlName = '?Progress:PctText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:Cancel;  SolaceCtrlName = '?Progress:Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 34
  PARENT.Ask


ThisWindow.AskPreview PROCEDURE

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(AskPreview, ())
  if tmp:RecordsCount = 0
  	Case MessageEx('There are no records that match the selected criteria.','ServiceBase 2000',|
  	               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
  		Of 1 ! &OK Button
  	End!Case MessageEx
      tmp:RecordsCount = 1
  end!if tmp:RecordsCount = 0
  IF ClarioNETServer:Active()                         !---ClarioNET 46
    IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
      ENDPAGE(SELF.Report)
      LOOP TempNameFunc_Flag = 1 TO RECORDS(SELF.PreviewQueue)
        GET(SELF.PreviewQueue, TempNameFunc_Flag)
        ClarioNET:AddMetafileName(SELF.PreviewQueue.Filename)
      END
      ClarioNET:SetReportProperties(report)
      FREE(SELF.PreviewQueue)
      TempNameFunc_Flag = 0
    END
  ELSE
    PARENT.AskPreview
  END                                                 !---ClarioNET 48
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(AskPreview, ())


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Stock_Usage_Report')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Stock_Usage_Report')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:LOCATION.Open
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:USERS.UseFile
  Access:STOMODEL.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisReport.Init(Process:View, Relate:LOCATION, ?Progress:PctText, Progress:Thermometer, ProgressMgr, loc:Location)
  ThisReport.CaseSensitiveValue = FALSE
  ThisReport.AddSortOrder(loc:Location_Key)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:LOCATION.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  Do RecolourWindow


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:LOCATION.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Stock_Usage_Report',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  tmp:printer = printer{propprint:device}
  choose_printer(new_printer",preview",copies")
  printer{propprint:device} = new_printer"
  printer{propprint:copies} = copies"
  if preview" <> 'YES'
      self.skippreview = true
  end!if preview" <> 'YES'
  ReturnValue = PARENT.OpenReport()
  set(defaults)
  access:Defaults.next()
  If glo:select3 = ''
      location_temp = 'ALL'
  Else
      location_Temp = glo:select3
  End
  If glo:select4 = ''
      supplier_temp = 'ALL'
  Else
      supplier_Temp = glo:select4
  End
  
  access:users.clearkey(use:password_key)
  use:password =glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = Clip(use:Forename) & ' '  & Clip(use:surname)
  IF ~ReturnValue
    report$?ReportPageNumber{PROP:PageNo}=True
  END
  IF ~ReturnValue
    SELF.Report $ ?ReportDateStamp{PROP:Text}=FORMAT(TODAY(),@d6)
  END
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF ClarioNETServer:Active()
    ClarioNET:EndReport                               !---ClarioNET 101
  END                                                 !---ClarioNET 102
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Stock_Usage_Report')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeNoRecords, ())
  return
  PARENT.TakeNoRecords
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeNoRecords, ())


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 91
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
    IF ClarioNETServer:Active()                       !---ClarioNET 44
      IF TempNameFunc_Flag = 0
        TempNameFunc_Flag = 1
        report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
      END
    END
  !tmp:MainStore   = MainStoreLocation()
  
  print# = 1
  If glo:select3 <> ''
      If glo:select3 <> loc:location
          print# = 0
      End!If glo:select3 = loc:location
  End!If glo:select3 <> ''
  If print# = 1
      IF GLO:Select6 = 'P' !Added by Neil, allows to sort by location or part number!
        save_sto_id = access:stock.savefile()
        access:stock.clearkey(sto:location_key)
        sto:location    = loc:location
        set(sto:location_key,sto:location_key)
      ELSE
        save_sto_id = access:stock.savefile()
        access:stock.clearkey(sto:Shelf_location_key)
        sto:location    = loc:location
        set(sto:Shelf_location_key,sto:Shelf_location_key)
      END
      loop
          if access:stock.next()
             break
          end !if
          if sto:location    <> loc:location      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
          If glo:select4 <> ''
              If sto:supplier <> glo:select4
                  Cycle
              End!If sto:supplier <> glo:select4
          End!If glo:select4 <> ''
  
  !Get First Model
  
          tmp:FirstModel = ''
          save_stm_id = access:stomodel.savefile()
          access:stomodel.clearkey(stm:model_number_key)
          stm:ref_number   = sto:ref_number
          stm:manufacturer = sto:manufacturer
          set(stm:model_number_key,stm:model_number_key)
          loop
              if access:stomodel.next()
                 break
              end !if
              if stm:ref_number   <> sto:ref_number      |
              or stm:manufacturer <> sto:manufacturer      |
                  then break.  ! end if
              tmp:FirstModel = stm:model_number
              Break
          end !loop
          access:stomodel.restorefile(save_stm_id)
  
          used_temp = 0
          line_total_temp = 0
          save_shi_id = access:stohist.savefile()
          access:stohist.clearkey(shi:ref_number_key)
          shi:ref_number = sto:ref_number
          shi:date       = glo:select1
          set(shi:ref_number_key,shi:ref_number_key)
          loop
              if access:stohist.next()
                 break
              end !if
              if shi:ref_number <> sto:ref_number      |
              or shi:date       > glo:select2      |
                  then break.  ! end if
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              If shi:job_number = ''
                  Cycle
              End
  
              If shi:transaction_type = 'DEC'
                  used_temp += shi:quantity
              End!If shi:transaction_type = 'DEC'
              If shi:transaction_type = 'REC'
                  used_temp -= shi:quantity
              End!If shi:transaction_type = 'REC'
          end !loop
          access:stohist.restorefile(save_shi_id)
          If glo:select5 = 'YES'
              If used_temp = 0
                  Cycle
              End!If used_temp = 0
          End!If glo:select5 = 'YES'
          line_total_temp += used_temp * sto:purchase_cost
          on_order_total_temp += sto:quantity_on_order
          in_stock_total_temp += sto:quantity_stock
          line_total_total_temp += line_Total_temp
          used_total_temp += used_temp
          purchase_cost_total_temp += sto:purchase_cost
          count_temp += 1
          shelf_location_temp = Clip(sto:shelf_location) & ' / ' & Clip(sto:second_location)
          tmp:RecordsCount += 1
          Print(rpt:detail)
      end !loop
      access:stock.restorefile(save_sto_id)
  
  End!If print# = 1
  ReturnValue = PARENT.TakeRecord()
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

