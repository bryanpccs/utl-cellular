   PROGRAM

_ABCDllMode_  EQUATE(1)
_ABCLinkMode_ EQUATE(0)

! Start of "Before Global INCLUDEs"
! [Priority 5000]

! End of "Before Global INCLUDEs"
   INCLUDE('ABFILE.INC'),ONCE
   INCLUDE('EQUATES.CLW'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE
! Start of "After Global INCLUDEs"
! [Priority 1000]

INCLUDE('MSGEX.INC')
MEMORYSTATUS    GROUP,TYPE
dwLength            ULONG
dwMemoryLoad        ULONG
dwTotalPhys         ULONG
dwAvailPhys         ULONG
dwTotalPageFile     ULONG
dwAvailPageFile     ULONG
dwTotalVirtual      ULONG
dwAvailVirtual      ULONG
                end
CurrentMem      Group(MemoryStatus)
                end
OSVERSIONINFO          GROUP,TYPE
dwOSVersionInfoSize      ULONG
dwMajorVersion           ULONG
dwMinorVersion           ULONG
dwBuildNumber            ULONG
dwPlatformId             ULONG
szCSDVersion             Byte,DIM(128)
                        END
WinVersion       Group(OSVERSIONINFO)
                 end
! [Priority 2900]

   INCLUDE('CLARIONET.INC')                           !--- ClarioNET 8
! [Priority 6900]

   INCLUDE('PrnProp.clw')
! [Priority 9950]

! End of "After Global INCLUDEs"

   MAP
    MODULE('SBE01APP.DLL')
Status_Routine         PROCEDURE(String,*String,*String,*String,String),DLL !
Browse_Users_Job_Assignment PROCEDURE,DLL             !
Turnaround_Routine     PROCEDURE(String,String,*String,*String),DLL !
    END
    MODULE('SBA01APP.DLL')
Weekend_Routine        PROCEDURE(String,String,*String),DLL !
Total_Price            PROCEDURE(String,*String,*String,*String),DLL !
Pricing_Routine        PROCEDURE(String,*string,*string,*string,*string),DLL !
CountBouncer           FUNCTION(Long,Date,String),Long,DLL !
Strippoint             FUNCTION(string),string,DLL    !
ToBeExchanged          FUNCTION(),Byte,DLL            !
GetStatus              PROCEDURE(Long,Byte,String),DLL !
Stripcomma             FUNCTION(String),String,DLL    !
Stripreturn            FUNCTION(String),String,DLL    !
StripReturnToComma     FUNCTION(String),String,DLL    !
StripQuotes            FUNCTION(String),String,DLL    !
    END
    MODULE('SBE02APP.DLL')
Browse_Sub_Accounts    PROCEDURE,DLL                  !
    END
    MODULE('SBA03APP.DLL')
Browse_Charge_Type     PROCEDURE,DLL                  !
Browse_Locations       PROCEDURE,DLL                  !
Browse_Manufacturers   PROCEDURE,DLL                  !
Browse_Model_Numbers   PROCEDURE,DLL                  !
Browse_Status          PROCEDURE,DLL                  !
Browse_Unit_Types      PROCEDURE,DLL                  !
BrowseMainAccount      PROCEDURE,DLL                  !
PickDefaultColour      PROCEDURE,DLL                  !
    END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('SBD03001.CLW')
Courier_Collection_Report PROCEDURE   !Courier Collection Repert
     END
     MODULE('SBD03002.CLW')
Special_Delivery_Criteria PROCEDURE(string)   !
     END
     MODULE('SBD03003.CLW')
Exchange_Exceptions_Report PROCEDURE(String)   !Exchange Exceptions Report
     END
     MODULE('SBD03004.CLW')
Exchange_Exceptions_Criteria PROCEDURE(String)   !
     END
     MODULE('SBD03006.CLW')
Status_Report_Summary  PROCEDURE(String)   !Status Report Summary (Booking/Completed)
     END
     MODULE('SBD03007.CLW')
Completed_Job_Stats_Export PROCEDURE   !
     END
     MODULE('SBD03008.CLW')
Date_Range             PROCEDURE(String)   !
     END
     MODULE('SBD03009.CLW')
QA_Failure_Report      PROCEDURE   !QA Failure Report
     END
     MODULE('SBD03010.CLW')
SagemDailyExport       PROCEDURE   !
     END
     MODULE('SBD03011.CLW')
Status_Summary_Report  PROCEDURE   !Status Summary Report
     END
     MODULE('SBD03013.CLW')
Stock_Value_Summary_Report PROCEDURE   !Stock Value Summary Report - By Site Location
     END
     MODULE('SBD03014.CLW')
Stock_Value_Criteria   PROCEDURE   !
     END
     MODULE('SBD03015.CLW')
Stock_Usage_Report     PROCEDURE   !
     END
     MODULE('SBD03016.CLW')
Stock_Usage_Criteria   PROCEDURE   !
     END
     MODULE('SBD03017.CLW')
Stock_Check_Criteria_Old PROCEDURE   !
     END
     MODULE('SBD03018.CLW')
Parts_Not_Received_Criteria PROCEDURE   !
     END
     MODULE('SBD03019.CLW')
Days_Between_Routine   PROCEDURE(String,String,*String)   !
     END
     MODULE('SBD03020.CLW')
Choose_Printer         PROCEDURE(*string,*string,*string)   !
     END
     MODULE('SBD03021.CLW')
Exchange_Units_In_Channel PROCEDURE   !Fix Later
     END
     MODULE('SBD03022.CLW')
Units_In_Channel_Criteria PROCEDURE   !
     END
     MODULE('SBD03023.CLW')
Stock_Forecast_Report_Old PROCEDURE   !
     END
     MODULE('SBD03024.CLW')
Stock_Forecast_Criteria PROCEDURE   !
     END
     MODULE('SBD03025.CLW')
Exchange_Unit_Audit_Criteria PROCEDURE   !
     END
     MODULE('SBD03026.CLW')
Exchange_Unit_Audit_Report PROCEDURE   !Exchange Unit Audit Report
     END
     MODULE('SBD03027.CLW')
SetDefault             PROCEDURE   !
     END
     MODULE('SBD03028.CLW')
Job_Pending_Parts_Criteria PROCEDURE   !
     END
     MODULE('SBD03029.CLW')
Income_Report_Criteria PROCEDURE   !
     END
     MODULE('SBD03030.CLW')
Exchange_Units_In_Channel_Fix_Later PROCEDURE   !Exchange Units In Channel
     END
     MODULE('SBD03031.CLW')
QA_Failed              PROCEDURE   !QA_Failed
     END
     MODULE('SBD03032.CLW')
Retail_Sales_Valuation_Report PROCEDURE   !Retail Sales Valuation Report
     END
     MODULE('SBD03033.CLW')
Retail_Sales_Valuation_Criteria PROCEDURE   !
     END
     MODULE('SBD03034.CLW')
Third_Party_Consignment_Note PROCEDURE   !Third Party Consignment Note
     END
     MODULE('SBD03035.CLW')
Parts_Order            PROCEDURE   !Parts Order
     END
     MODULE('SBD03036.CLW')
Third_Party_Returns_Report PROCEDURE   !Third Party Consignment Note
     END
     MODULE('SBD03037.CLW')
Stock_Value_Detailed_Criteria PROCEDURE   !
     END
     MODULE('SBD03038.CLW')
Stock_Value_Report     PROCEDURE   !Stock Value Report - By Shelf Location
     END
     MODULE('SBD03039.CLW')
Stock_Check_Report     PROCEDURE   !Stock Check Report - By Site Location
     END
     MODULE('SBD03040.CLW')
Stock_Check_Criteria   PROCEDURE   !
     END
     MODULE('SBD03041.CLW')
Third_Party_Failed_Returns PROCEDURE   !Third Party Consignment Note
     END
     MODULE('SBD03042.CLW')
Third_Party_Report_Criteria PROCEDURE   !
     END
     MODULE('SBD03043.CLW')
Income_Report          PROCEDURE(Byte,Byte)   !Income Report
     END
     MODULE('SBD03044.CLW')
Job_Pending_Parts_Report PROCEDURE   !Job Pending Parts Report
     END
     MODULE('SBD03045.CLW')
Nokia_accessories_claim_report PROCEDURE   !Nokia Accessories Claim Report
     END
     MODULE('SBD03046.CLW')
Parts_Not_Received_Report PROCEDURE   !Parts Not Received Report
     END
     MODULE('SBD03047.CLW')
Special_Delivery_Report PROCEDURE(Byte,Byte,Time,Time)   !Special Delivery Report
     END
     MODULE('SBD03048.CLW')
Overdue_Loan_Report    PROCEDURE   !Overdue Loan Report
     END
     MODULE('SBD03049.CLW')
Overdue_Loan_Criteria  PROCEDURE   !
     END
     MODULE('SBD03050.CLW')
Stock_Forecast_Report  PROCEDURE   !Stock Forecast Report
     END
     MODULE('SBD03051.CLW')
Bouncer_History        PROCEDURE   !Bouncer History Report
     END
     MODULE('SBD03052.CLW')
Standard_Letter        PROCEDURE   !Standard Letter
     END
     MODULE('SBD03054.CLW')
QA_Failed_Exchange     PROCEDURE   !QA_Failed
     END
     MODULE('SBD03055.CLW')
QA_Failed_Loan         PROCEDURE   !QA_Failed
     END
     MODULE('SBD03056.CLW')
Restocking_Note        PROCEDURE   !QA_Failed
     END
     MODULE('SBD03057.CLW')
Third_Party_Consignment_Criteria PROCEDURE   !For Consignment Note & Failed Returns
     END
     MODULE('SBD03058.CLW')
Third_Party_Returns_Criteria PROCEDURE   !For Consignment Note & Failed Returns
     END
     MODULE('SBD03059.CLW')
Rapid_Status_Update_Report PROCEDURE   !Rapid Status Update Report
     END
     MODULE('SBD03060.CLW')
Chargeable_Query_Report PROCEDURE   !Chargeable Query Report
     END
     MODULE('SBD03061.CLW')
Nokia_Exchange_Report  PROCEDURE   !Nokia Accessories Claim Report
     END
     MODULE('SBD03062.CLW')
NEC_Export_Criteria    PROCEDURE   !
     END
     MODULE('SBD03063.CLW')
Overdue_Status_Criteria PROCEDURE   !
     END
     MODULE('SBD03064.CLW')
Overdue_Status_Report  PROCEDURE   !Overdue Status Report
     END
     MODULE('SBD03065.CLW')
SelectSubAccount       PROCEDURE   !Sub Accounts
     END
     MODULE('SBD03066.CLW')
LabelG_Consolidation   PROCEDURE   !
     END
     MODULE('SBD03067.CLW')
Unallocated_Jobs_Report PROCEDURE   !Unallocated Jobs
     END
     MODULE('SBD03068.CLW')
Unallocated_Criteria   PROCEDURE   !
     END
     MODULE('SBD03069.CLW')
Part_Usage_Export      PROCEDURE   !
     END
     MODULE('SBD03070.CLW')
RepairPerformanceCriteria PROCEDURE   !
     END
     MODULE('SBD03071.CLW')
RepairPerformanceReport PROCEDURE   !Repair Performance Report
     END
     MODULE('SBD03072.CLW')
CompletedJobStatisticsExportCriteria PROCEDURE   !
     END
     MODULE('SBD03073.CLW')
SelectMainSubAccount   PROCEDURE   !Sub Accounts
     END
     MODULE('SBD03074.CLW')
PriceListExport        PROCEDURE   !
     END
     MODULE('SBD03075.CLW')
RepairExchangeReportCriteria PROCEDURE   !
     END
     MODULE('SBD03076.CLW')
RepairExchangeReport   PROCEDURE(Date,Date)   !RepairExchangeReport
     END
     MODULE('SBD03077.CLW')
Multiple_Job_Booking_Report PROCEDURE   !Rapid Status Update Report
     END
     MODULE('SBD03078.CLW')
CustomerEnquiryReportCriteria PROCEDURE   !
     END
     MODULE('SBD03079.CLW')
CustomerEnquiryReport  PROCEDURE(Date,Date,Long)   !Customer Enquiry Report
     END
     MODULE('SBD03080.CLW')
TLCExportCriteria      PROCEDURE   !
     END
     MODULE('SBD03081.CLW')
TLCExport              PROCEDURE(Date,Date,String)   !
     END
     MODULE('SBD03082.CLW')
StatusExport           PROCEDURE   !
     END
     MODULE('SBD03083.CLW')
Stock_Audit_Report     PROCEDURE(String)   !
     END
     MODULE('SBD03084.CLW')
NetworkReportCriteria  PROCEDURE   !
     END
     MODULE('SBD03085.CLW')
NetworkReport          PROCEDURE(String,Date,Date)   !Network / Colour Report
     END
     MODULE('SBD03086.CLW')
Status_Report_Criteria PROCEDURE   !
     END
     MODULE('SBD03087.CLW')
BrowseStatusCriteria   PROCEDURE   !Browse the STATCRIT File
     END
     MODULE('SBD03088.CLW')
UpdateStatusCriteria   PROCEDURE   !Update the STATREP File
     END
     MODULE('SBD03089.CLW')
InsertCriteriaDescription FUNCTION(),String   !
     END
     MODULE('SBD03090.CLW')
StatusReportValidation FUNCTION(Byte,Date,Date,Byte,Byte,Byte,Byte,Byte,Long,Long),Byte   !
     END
     MODULE('SBD03091.CLW')
StatusReport           PROCEDURE(Byte,Date,Date,Byte,Byte,Byte,Byte,Byte,String,Byte,Long,Long,Byte)   !Status Report (Booking/Completed)
     END
     MODULE('SBD03092.CLW')
ToteReport             PROCEDURE(Date,Date,Byte,String)   !Tote Report
     END
     MODULE('SBD03093.CLW')
ToteReportCriteria     PROCEDURE   !
     END
     MODULE('SBD03094.CLW')
ExchangeReasonCriteria PROCEDURE   !
     END
     MODULE('SBD03095.CLW')
ExchangeReasonReport   PROCEDURE(Date,Date)   !Exchange Reason Report
     END
     MODULE('SBD03096.CLW')
VKEdiExport            PROCEDURE   !VK EDI Export
     END
     MODULE('SBD03097.CLW')
SIDExtract             FUNCTION(Byte f:Mode),String   !
     END
     ! Start of "Inside the Global Map"
     ! [Priority 1000]

          MODULE('MsgEx Runtime Library')
            MessageEx(<STRING TheText>,      |
                      <STRING TheTitel>,     |
                      <STRING TheImage>,     |
                      <STRING TheButtons>,   |
                       SIGNED DefButton=0,   |
                       SIGNED EscButton=0,   |
                      <STRING CheckText>,    |
                      <*?     CheckVar>,     |
                      <STRING Fontname>,     |
                      SIGNED  Fontsize=8,    |
                      LONG    Fontcolor=0,   |
                      SIGNED  Fontstyle=0,   |
                      LONG    Charset=0,     |
                      LONG    BackColor=COLOR:NONE, |
                      <STRING Wallpaper>,    |
                      LONG    WallMode=0,    |
                      <STRING Sound>,        |
                      LONG    Options=0,     |
                      SIGNED  MinWidth=0,    |
                      SIGNED  MinHeight=0,   |
                      LONG    TimeOut=0      ),SIGNED,PROC,NAME('MESSAGEEX')
          END
   MODULE('cellmain.DLL')
DebugToolbox           PROCEDURE(String),DLL          !
SolaceViewVars         PROCEDURE(<String>,<?>,<String>,Byte,<String>),DLL !
   END
      ! [Priority 4000]
      MODULE('SEQLIB32.LIB')
        SEQUENCE2(BYTE,BYTE,*CSTRING,*CSTRING),PASCAL,RAW
        !CHECK(BYTE,BYTE,*?,*?),PASCAL,NAME('CHECK'),RAW
      END
      Module('WINAPI')
          GetDesktopWindow(), Unsigned, Pascal 
          ShellExecute(Unsigned, *Cstring, *Cstring, *Cstring, *Cstring, Signed), Unsigned, Pascal, Raw, Proc,name('ShellExecuteA')
      End
      
          MODULE('WINAPI')   
              GetTempPathA(ULONG,*Cstring),Ulong,PASCAL,RAW
          END
      
      INCLUDE('CLARIONET.CLW')                        !--- ClarioNET 3
      ClarioNET:GetPrintFileName(SIGNED PageNo),STRING !--- ClarioNET 4
      ClarioNET:HandleClientQuery,STRING              !--- ClarioNET 4a
      ! [Priority 7000]

      MODULE('CPC55P32.LIB')
        Wmf2Ascii(QUEUE, STRING, LONG),NAME('Wmf2Ascii'),DLL(dll_mode)
        PaperSupport(SHORT,<QUEUE>),STRING,NAME('PaperSupport'),DLL(dll_mode)
        PrintPreview(QUEUE,SHORT,<STRING>,REPORT,BYTE,<STRING>,<LONG>,<STRING>,<STRING>),LONG,NAME('PrintPreview'),DLL(dll_mode)
        Supported(LONG),SHORT,NAME('Supported'),DLL(dll_mode)
        HandleCopies(QUEUE,LONG),NAME('HandleCopies'),DLL(dll_mode)
        ShowPrinterStatus(STRING,SHORT,<STRING>),NAME('ShowPrinterStatus'),DLL(dll_mode)
        AssgnPgOfPg(Queue,<STRING>,<BYTE>,<STRING>,<LONG>),NAME('AssgnPgOfPg'),DLL(dll_mode)
        SaveClipToFile(<STRING>),STRING,NAME('SaveClipToFile'),DLL(dll_mode)
        SetPrinterDraftMode(),LONG,NAME('SetPrinterDraftMode'),DLL(dll_mode)
      END
           FillCpcsIniStrings(STRING,STRING,STRING,STRING)
           MODULE('sbd03_SF.CLW')
             CheckOpen(FILE File,<BYTE OverrideCreate>,<BYTE OverrideOpenMode>)
             StandardWarning(LONG WarningID),LONG,PROC
             StandardWarning(LONG WarningID,STRING WarningText1),LONG,PROC
             StandardWarning(LONG WarningID,STRING WarningText1,STRING WarningText2),LONG,PROC
             RISaveError
           END
      !--------------------------------------------------------------------------
      ! -----------Tintools-------------
      !--------------------------------------------------------------------------
        MODULE('TAPI.LIB')
         TapiRequestMakeCall(*CSTRING,*CSTRING,*CSTRING,*CSTRING),SHORT,PASCAL,RAW
        END
      !--------------------------------------------------------------------------
      ! -----------Tintools-------------
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! -----------Tintools-------------
      !--------------------------------------------------------------------------
       MODULE('sbd03_VW.CLW')    !REPORT VIEWER
          TINCALENDARSTYLE1(<LONG>),LONG
          TINCALENDARSTYLE2(<STRING>,<LONG>),LONG
          TINCALENDARSTYLE3(<STRING>,<LONG>),LONG
          TINCALENDARSTYLE4(<STRING>,<LONG>),LONG
          TINCALENDARmonyear(<LONG>),LONG
          TINCALENDARmonth(<LONG>),LONG
          TINCALCULATORSTYLE1(<STRING>,<REAL>,<BYTE>),REAL
          TINCALCULATORSTYLE2(<STRING>,<REAL>,<BYTE>),REAL
          TINCALCULATORSTYLE3(<STRING>,<REAL>,<BYTE>),REAL
       END
      !--------------------------------------------------------------------------
      ! -----------Tintools-------------
      !--------------------------------------------------------------------------
      MODULE('SWiz255.lib')
        SSWizInit(<string>),DLL(dll_mode)
        SpreadWizard(QUEUE,<BYTE CalledFrom>,<LONG SysID>,<STRING ViewClass>,<tqField>,<*CSTRING Sort>,<QUEUE KeyQ>,tqField),LONG,PROC,DLL(dll_mode)
        SpreadStyle(*GROUP StyleSettings),DLL(dll_mode)
        UsBrowse(tqField,STRING ViewClass,<tqField>,<*CSTRING Sort>,<QUEUE KeyQ>,tqField,<*CSTRING SheetDesc>,<*LONG StartRow>,<*LONG StartCol>,<*CSTRING Template>),LONG,PROC,DLL(dll_mode)
        UsGetFileName(*CSTRING f1FileName, BYTE CallFileDialog, BYTE f1Action),BYTE,DLL(dll_mode)
        UsSaveType(*CSTRING f1FileName,LONG F1SS),BYTE,DLL(dll_mode)
        UsInitDoc(tqField,LONG,LONG,LONG,STRING),DLL(dll_mode)
        UsFillRow(tqField,LONG,*USHORT,LONG),DLL(dll_mode)
        BuildSQ(string,tqField,tqField),DLL(dll_mode)
        UsDesigner(LONG F1SS),BYTE,DLL(dll_mode)
        UsDeInit(*CSTRING f1FileName,LONG F1SS, BYTE f1Action),DLL(dll_mode)
      END
      ! [Priority 9950]

      ! End of "Inside the Global Map"
   END

glo:file_name      STRING(255),EXTERNAL,DLL(dll_mode)
glo:CriteriaFile   STRING(255)
glo:tradetmp       STRING(255),EXTERNAL,DLL(dll_mode)
glo:filename       STRING(255),EXTERNAL,DLL(dll_mode)
glo:file_name2     STRING(255),EXTERNAL,DLL(dll_mode)
glo:file_name3     STRING(255),EXTERNAL,DLL(dll_mode)
glo:file_name4     STRING(255),EXTERNAL,DLL(dll_mode)

! Start of "Before File Declarations"
! [Priority 1000]

    Map
BHStripReplace           Procedure(String func:String,String func:Strip,String func:Replace),String
BHStripNonAlphaNum           Procedure(String func:String,String func:Replace),String
BHReturnDaysHoursMins       Procedure(Long func:TotalMins,*Long func:Days,*Long func:Hours,*Long func:Mins)
BHTimeDifference24Hr        Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime),Long
BHTimeDifference24Hr5Days   Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime),Long
BHTimeDifference24Hr5DaysMonday   Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime,String func:MondayStart),Long
    End
! [Priority 6000]

CPCSStartUpPrintDevice      CSTRING(64)

CPCS:ProgWinTitlePrvw   CSTRING(64)  
CPCS:ProgWinTitlePrnt   CSTRING(64)  
CPCS:ProgWinPctText     CSTRING(64)  
CPCS:ProgWinRecText     CSTRING(64)  
CPCS:ProgWinUsrText     CSTRING(64)  
CPCS:PrtrDlgTitle       CSTRING(64)  
CPCS:AskPrvwDlgTitle    CSTRING(64)  
CPCS:AskPrvwDlgText     CSTRING(64)  
CPCS:AreYouSureTitle    CSTRING(64)  
CPCS:AreYouSureText     CSTRING(64)  
CPCS:PrvwPartialTitle   CSTRING(64)  
CPCS:PrvwPartialText    CSTRING(128) 
CPCS:NoDfltPrtrTitle    CSTRING(64)  
CPCS:NoDfltPrtrText     CSTRING(255) 
CPCS:NthgToPrvwTitle    CSTRING(64)  
CPCS:NthgToPrvwText     CSTRING(64)  
CPCS:NthgToPrntTitle    CSTRING(64)  
CPCS:NthgToPrntText     CSTRING(64)  
CPCS:AsciiOutTitle      CSTRING(64)  
CPCS:AsciiOutmasks      CSTRING(64)  
CPCS:DynLblErrTitle     CSTRING(64)
CPCS:DynLblErrText1     CSTRING(64)  
CPCS:DynLblErrText2     CSTRING(64)  

SaveErrorCode               LONG
SaveError                   CSTRING(255)
SaveFileErrorCode           LONG
SaveFileError               CSTRING(255)

Warn:InvalidFile         EQUATE (1)
Warn:InvalidKey          EQUATE (2)
Warn:RebuildError        EQUATE (3)
Warn:CreateError         EQUATE (4)
Warn:CreateOpenError     EQUATE (5)
Warn:ProcedureToDo       EQUATE (6)
Warn:BadKeyedRec         EQUATE (7)
Warn:OutOfRangeHigh      EQUATE (8)
Warn:OutOfRangeLow       EQUATE (9)
Warn:OutOfRange          EQUATE (10)
Warn:NotInFile           EQUATE (11)
Warn:RestrictUpdate      EQUATE (12)
Warn:RestrictDelete      EQUATE (13)
Warn:InsertError         EQUATE (14)
Warn:RIUpdateError       EQUATE (15)
Warn:UpdateError         EQUATE (16)
Warn:RIDeleteError       EQUATE (17)
Warn:DeleteError         EQUATE (18)
Warn:InsertDisabled      EQUATE (19)
Warn:UpdateDisabled      EQUATE (20)
Warn:DeleteDisabled      EQUATE (21)
Warn:NoCreate            EQUATE (22)
Warn:ConfirmCancel       EQUATE (23)
Warn:DuplicateKey        EQUATE (24)
Warn:AutoIncError        EQUATE (25)
Warn:FileLoadError       EQUATE (26)
Warn:ConfirmCancelLoad   EQUATE (27)
Warn:FileZeroLength      EQUATE (28)
Warn:EndOfAsciiQueue     EQUATE (29)
Warn:DiskError           EQUATE (30)
Warn:ProcessActionError  EQUATE (31)
Warn:StandardDelete      EQUATE (32)
Warn:SaveOnCancel        EQUATE (33)
Warn:LogoutError         EQUATE (34)
Warn:RecordFetchError    EQUATE (35)
Warn:ViewOpenError       EQUATE (36)
Warn:NewRecordAdded      EQUATE (37)
Warn:RIFormUpdateError   EQUATE (38)


! [Priority 9950]

! End of "Before File Declarations"
STDCHRGE             FILE,DRIVER('Btrieve'),PRE(sta),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Standard Charges
Model_Number_Charge_Key  KEY(sta:Model_Number,sta:Charge_Type,sta:Unit_Type,sta:Repair_Type),NOCASE,PRIMARY ! By Charge Type
Charge_Type_Key          KEY(sta:Model_Number,sta:Charge_Type),DUP,NOCASE ! By Charge Type
Unit_Type_Key            KEY(sta:Model_Number,sta:Unit_Type),DUP,NOCASE ! By Unit Type
Repair_Type_Key          KEY(sta:Model_Number,sta:Repair_Type),DUP,NOCASE ! By Repair Type
Cost_Key                 KEY(sta:Model_Number,sta:Cost),DUP,NOCASE ! By Cost
Charge_Type_Only_Key     KEY(sta:Charge_Type),DUP,NOCASE ! 
Repair_Type_Only_Key     KEY(sta:Repair_Type),DUP,NOCASE ! 
Unit_Type_Only_Key       KEY(sta:Unit_Type),DUP,NOCASE ! 
Record                   RECORD,PRE()
Charge_Type                 STRING(30)                ! 
Model_Number                STRING(30)                ! 
Unit_Type                   STRING(30)                ! 
Repair_Type                 STRING(30)                ! 
Cost                        REAL                      ! 
dummy                       BYTE                      ! 
                         END
                     END                       

CARISMA              FILE,DRIVER('Btrieve'),PRE(cma),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! SID Oracle Codes
RecordNumberKey          KEY(cma:RecordNumber),NOCASE,PRIMARY ! 
ManufactModColourKey     KEY(cma:Manufacturer,cma:ModelNo,cma:Colour),DUP,NOCASE ! 
ContractCodeKey          KEY(cma:ContractOracleCode),DUP,NOCASE ! 
PAYTCodeKey              KEY(cma:PAYTOracleCode),DUP,NOCASE ! 
Record                   RECORD,PRE()
RecordNumber                LONG                      ! 
Manufacturer                STRING(30)                ! 
ModelNo                     STRING(30)                ! 
Colour                      STRING(30)                ! 
ContractOracleCode          STRING(30)                ! 
PAYTOracleCode              STRING(30)                ! 
                         END
                     END                       

DEFEMAIL             FILE,DRIVER('Btrieve'),OEM,PRE(dem),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Defaults - Email (CRC/Vodafone)
RecordNumberKey          KEY(dem:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
SMTPServer                  STRING(50)                ! SMTP Server
SMTPPort                    STRING(3)                 ! SMTP Server Port
SMTPUserName                STRING(30)                ! SMTP User Name
SMTPPassword                STRING(30)                ! SMTP Password
EmailFromAddress            STRING(255)               ! From Email Address
SMSToAddress                STRING(255)               ! To Email Address (SMS)
EmailToAddress              STRING(255)               ! To Email Address
AdminToAddress              STRING(255)               ! To Email Address (Administrator Alerts)
DespFromSCText              STRING(480)               ! SMS Text (Device Despatched From ServiceBase)
ArrivedAtRetailText         STRING(480)               ! SMS Text (Device Arrived At Retail Store)
SMSFTPAddress               STRING(30)                ! SMS FTP Address
SMSFTPUsername              STRING(30)                ! FTP Username
SMSFTPPassword              STRING(30)                ! FTP Password
SMSFTPLocation              STRING(255)               ! FTP Location
R1Active                    BYTE                      ! Alert Active
R2Active                    BYTE                      ! Alert Active
R3Active                    BYTE                      ! Alert Active
R1SMSActive                 BYTE                      ! Alert Active
R2SMSActive                 BYTE                      ! Alert Active
R3SMSActive                 BYTE                      ! Alert Active
BookedRetailText            STRING(480)               ! SMS Text (Device Booked At Retail Store)
R4Active                    BYTE                      ! Alert Active
R4SMSActive                 BYTE                      ! Alert Active
DespFromStoreText           STRING(480)               ! SMS Text (Device Despatched From Store)
                         END
                     END                       

CRCDEFS              FILE,DRIVER('Btrieve'),OEM,PRE(crcdef),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! CRC/Wilfred Defaults
RecordNumberKey          KEY(crcdef:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
FTPServer                   STRING(255)               ! FTPServer
FTPUserName                 STRING(60)                ! FTPUserName
FTPPassword                 STRING(60)                ! FTPPassword
FTPImportPath               STRING(255)               ! FTPImportPath
FTPExportPath               STRING(255)               ! FTPExportPath
EmailServer                 STRING(255)               ! EmailServer
EmailServerPort             STRING(3)                 ! EmailServerPort
EmailFromAddress            STRING(255)               ! EmailFromAddress
CRCEmailName                STRING(60)                ! CRCEmailName
CRCEmailAddress             STRING(255)               ! CRCEmailAddress
PCCSEmailName               STRING(60)                ! PCCSEmailName
PCCSEmailAddress            STRING(255)               ! PCCSEmailAddress
VodafoneEmailName           STRING(60)                ! VodafoneEmailName
VodafoneEmailAddress        STRING(255)               ! VodafoneEmailAddress
                         END
                     END                       

REGIONS              FILE,DRIVER('Btrieve'),PRE(reg),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! 
RegionIDKey              KEY(reg:RegionID),NOCASE,PRIMARY ! 
RegionNameKey            KEY(reg:RegionName),NOCASE   ! 
Record                   RECORD,PRE()
RegionID                    LONG                      ! 
RegionName                  STRING(30)                ! 
BankHolidaysFileName        STRING(100)               ! 
                         END
                     END                       

ACCREG               FILE,DRIVER('Btrieve'),PRE(acg),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! Account Regions
AccRegIDKey              KEY(acg:AccRegID),NOCASE,PRIMARY ! 
AccountNoKey             KEY(acg:AccountNo),NOCASE    ! 
RegionNameKey            KEY(acg:RegionName,acg:AccountNo),DUP,NOCASE ! 
Record                   RECORD,PRE()
AccRegID                    LONG                      ! 
AccountNo                   STRING(15)                ! 
RegionName                  STRING(30)                ! 
                         END
                     END                       

SIDACREG             FILE,DRIVER('Btrieve'),PRE(sar),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! SID Service Centre Account Regions
RecordNumberKey          KEY(sar:RecordNumber),NOCASE,PRIMARY ! 
SCAccRegIDKey            KEY(sar:SCAccountID,sar:AccRegID),DUP,NOCASE ! 
AccRegIDKey              KEY(sar:AccRegID),DUP,NOCASE ! 
Record                   RECORD,PRE()
RecordNumber                LONG                      ! 
SCAccountID                 LONG                      ! 
AccRegID                    LONG                      ! 
TransitDaysException        BYTE                      ! 
TransitDaysToSC             LONG                      ! Number of days to arrive at service centre from store
TransitDaysToStore          LONG                      ! Number of days to arrive at store from service centre
                         END
                     END                       

XMLJOBS              FILE,DRIVER('Btrieve'),OEM,PRE(XJB),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Samsung XML Jobs
RecordNumberKey          KEY(XJB:RECORD_NUMBER),NOCASE,PRIMARY ! 
TR_NO_KEY                KEY(XJB:TR_NO),DUP,NOCASE    ! 
JobNumberKey             KEY(XJB:JOB_NO),DUP,NOCASE   ! 
State_Job_Number_Key     KEY(XJB:RECORD_STATE,XJB:JOB_NO),DUP,NOCASE ! 
State_Surname_Key        KEY(XJB:RECORD_STATE,XJB:CONSUMER_LAST_NAME),DUP,NOCASE ! 
State_Postcode_Key       KEY(XJB:RECORD_STATE,XJB:CONSUMER_POSTCODE),DUP,NOCASE ! 
State_Model_Key          KEY(XJB:RECORD_STATE,XJB:MODEL_CODE),DUP,NOCASE ! 
State_Serial_No_Key      KEY(XJB:RECORD_STATE,XJB:SERIAL_NO),DUP,NOCASE ! 
Record                   RECORD,PRE()
RECORD_NUMBER               LONG                      ! 
RECORD_STATE                BYTE                      ! 
EXCEPTION_CODE              LONG                      ! 
EXCEPTION_DESC              STRING(255)               ! 
JOB_NO                      LONG                      ! 
COMPANY                     STRING(4)                 ! 
TR_NO                       STRING(30)                ! 
ASC_CODE                    STRING(10)                ! 
MODEL_CODE                  STRING(40)                ! 
SERIAL_NO                   STRING(16)                ! 
PURCHASE_DATE               DATE                      ! 
SERVICE_TYPE                STRING(2)                 ! 
IN_OUT_WARRANTY             STRING(1)                 ! 
REQUEST_DATE                DATE                      ! 
REQUEST_TIME                TIME                      ! 
REPAIR_ETD_DATE             DATE                      ! 
REPAIR_ETD_TIME             TIME                      ! 
TR_STATUS                   STRING(2)                 ! 
TR_REASON                   STRING(2)                 ! 
CONSUMER_TITLE              STRING(20)                ! 
CONSUMER_FIRST_NAME         STRING(40)                ! 
CONSUMER_LAST_NAME          STRING(40)                ! 
CONSUMER_TEL_NUMBER1        STRING(30)                ! 
CONSUMER_FAX_NUMBER         STRING(30)                ! 
CONSUMER_EMAIL              STRING(241)               ! 
CONSUMER_COUNTRY            STRING(20)                ! 
CONSUMER_REGION             STRING(20)                ! 
CONSUMER_POSTCODE           STRING(10)                ! 
CONSUMER_CITY               STRING(40)                ! 
CONSUMER_STREET             STRING(60)                ! 
CONSUMER_HOUSE_NUMBER       STRING(20)                ! 
REPAIR_COMPLETE_DATE        DATE                      ! 
REPAIR_COMPLETE_TIME        TIME                      ! 
GOODS_DELIVERY_DATE         DATE                      ! 
GOODS_DELIVERY_TIME         TIME                      ! 
SYMPTOM1_DESC               STRING(40)                ! 
SYMPTOM2_DESC               STRING(40)                ! 
SYMPTOM3_DESC               STRING(40)                ! 
BP_NO                       STRING(10)                ! 
INQUIRY_TEXT                STRING(254)               ! 
                         END
                     END                       

EXCHAMF              FILE,DRIVER('Btrieve'),OEM,PRE(emf),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! 
Audit_Number_Key         KEY(emf:Audit_Number),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
Audit_Number                LONG                      ! 
Date                        DATE                      ! 
Time                        LONG                      ! 
User                        STRING(3)                 ! 
Status                      STRING(30)                ! 
Complete_Flag               BYTE                      ! 
                         END
                     END                       

EXCHAUI              FILE,DRIVER('Btrieve'),OEM,PRE(eau),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! 
Internal_No_Key          KEY(eau:Internal_No),NOCASE,PRIMARY ! 
Audit_Number_Key         KEY(eau:Audit_Number),DUP,NOCASE ! 
Ref_Number_Key           KEY(eau:Ref_Number),DUP,NOCASE ! 
Exch_Browse_Key          KEY(eau:Audit_Number,eau:Exists),DUP,NOCASE ! 
Record                   RECORD,PRE()
Internal_No                 LONG                      ! 
Audit_Number                LONG                      ! 
Ref_Number                  LONG                      ! Unit Number
Exists                      BYTE                      ! 
New_IMEI                    BYTE                      ! 
                         END
                     END                       

JOBSENG              FILE,DRIVER('Btrieve'),OEM,PRE(joe),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Engineers Attached To Jobs
RecordNumberKey          KEY(joe:RecordNumber),NOCASE,PRIMARY ! By Record Number
UserCodeKey              KEY(joe:JobNumber,joe:UserCode,joe:DateAllocated),DUP,NOCASE ! By Date
UserCodeOnlyKey          KEY(joe:UserCode,joe:DateAllocated),DUP,NOCASE ! By Date
AllocatedKey             KEY(joe:JobNumber,joe:AllocatedBy,joe:DateAllocated),DUP,NOCASE ! By Date
JobNumberKey             KEY(joe:JobNumber,-joe:DateAllocated,-joe:TimeAllocated),DUP,NOCASE ! By Date
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
JobNumber                   LONG                      ! Job Number
UserCode                    STRING(3)                 ! User Code
DateAllocated               DATE                      ! Date Allocated
TimeAllocated               STRING(20)                ! Time Allocated
AllocatedBy                 STRING(3)                 ! Allocated By
EngSkillLevel               LONG                      ! Engineer Skill Level
JobSkillLevel               STRING(30)                ! Job Skill Level
                         END
                     END                       

MULDESPJ             FILE,DRIVER('Btrieve'),OEM,PRE(mulj),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Multiple Despatch Jobs
RecordNumberKey          KEY(mulj:RecordNumber),NOCASE,PRIMARY ! By Record Number
JobNumberKey             KEY(mulj:RefNumber,mulj:JobNumber),DUP,NOCASE ! By Job Number
CurrentJobKey            KEY(mulj:RefNumber,mulj:Current,mulj:JobNumber),DUP,NOCASE ! By Job Number
JobNumberOnlyKey         KEY(mulj:JobNumber),DUP,NOCASE ! By Job Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
RefNumber                   LONG                      ! RefNumber
JobNumber                   LONG                      ! Job Number
IMEINumber                  STRING(30)                ! I.M.E.I. Number
MSN                         STRING(30)                ! M.S.N.
AccountNumber               STRING(30)                ! AccountNumber
Courier                     STRING(30)                ! Courier
Current                     BYTE                      ! Current
                         END
                     END                       

AUDSTAEX             FILE,DRIVER('Btrieve'),OEM,PRE(aux),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Audit For Status Changes Extension
RecordNumberKey          KEY(aux:RecordNumber),NOCASE,PRIMARY ! By Record Number
RefNumberKey             KEY(aux:RefNumber),DUP,NOCASE ! By Ref Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
RefNumber                   LONG                      ! AUDSTATS unique reference
AgentName                   STRING(30)                ! Agent Name
                         END
                     END                       

MULDESP              FILE,DRIVER('Btrieve'),OEM,PRE(muld),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Multiple Despatch File
RecordNumberKey          KEY(muld:RecordNumber),NOCASE,PRIMARY ! By Record Number
BatchNumberKey           KEY(muld:BatchNumber),DUP,NOCASE ! By Batch Number
AccountNumberKey         KEY(muld:AccountNumber),DUP,NOCASE ! By Account Number
CourierKey               KEY(muld:Courier),DUP,NOCASE ! By Courier
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
BatchNumber                 STRING(30)                ! Batch Number
AccountNumber               STRING(30)                ! Account Number
Courier                     STRING(30)                ! Courier
BatchTotal                  LONG                      ! Total In Batch
                         END
                     END                       

INSMODELS            FILE,DRIVER('Btrieve'),OEM,PRE(INS3),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)               ! 
KeyModelNo               KEY(INS3:ModelNo),NOCASE,PRIMARY ! 
KeyMakeNo                KEY(INS3:MakeNo),DUP,NOCASE  ! 
KeyBrwModelNumber        KEY(INS3:MakeNo,INS3:ModelNumber),NOCASE ! 
KeyModelNumber           KEY(INS3:ModelNumber),DUP,NOCASE ! 
Record                   RECORD,PRE()
ModelNo                     LONG                      ! 
MakeNo                      LONG                      ! 
ModelNumber                 STRING(30)                ! 
                         END
                     END                       

STOCKLOG             FILE,DRIVER('Btrieve'),OEM,PRE(sal),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! 
Audit_Number_Key         KEY(sal:Audit_Number),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
Audit_Number                LONG                      ! 
Date_created                DATE                      ! 
Time_Created                TIME                      ! 
User                        STRING(20)                ! 
Checked_Date                DATE                      ! 
Checked_Time                TIME                      ! 
Checked_By                  STRING(3)                 ! 
Status                      STRING(10)                ! 
Scanned_Count               LONG                      ! 
Stock_Status                STRING(10)                ! 
                         END
                     END                       

IMEILOG              FILE,DRIVER('Btrieve'),OEM,PRE(ime),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! 
RecordNumberKey          KEY(ime:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
Audit_Number                LONG                      ! 
IMEI_Number                 STRING(30)                ! I.M.E.I. Number
Stock_Type                  STRING(30)                ! 
Status_Code                 STRING(6)                 ! 
Model_No                    STRING(30)                ! 
Manufacturer                STRING(30)                ! 
                         END
                     END                       

STMASAUD             FILE,DRIVER('Btrieve'),PRE(stom),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Stock Master Audit
AutoIncrement_Key        KEY(-stom:Audit_No),NOCASE,PRIMARY ! 
Compeleted_Key           KEY(stom:Complete),DUP,NOCASE ! 
Sent_Key                 KEY(stom:Send),DUP,NOCASE    ! 
Record                   RECORD,PRE()
Audit_No                    LONG                      ! 
branch                      STRING(30)                ! 
branch_id                   LONG                      ! 
Audit_Date                  LONG                      ! 
Audit_Time                  LONG                      ! 
End_Date                    LONG                      ! 
End_Time                    LONG                      ! 
Audit_User                  STRING(3)                 ! 
Complete                    STRING(1)                 ! 
Send                        STRING(1)                 ! 
                         END
                     END                       

AUDSTATS             FILE,DRIVER('Btrieve'),OEM,PRE(aus),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Audit For Status Changes
RecordNumberKey          KEY(aus:RecordNumber),NOCASE,PRIMARY ! By Record Number
DateChangedKey           KEY(aus:RefNumber,aus:Type,aus:DateChanged),DUP,NOCASE ! By Date Changed
NewStatusKey             KEY(aus:RefNumber,aus:Type,aus:NewStatus),DUP,NOCASE ! By New Status
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
RefNumber                   LONG                      ! Job Number
Type                        STRING(3)                 ! Type
DateChanged                 DATE                      ! Date Changed
TimeChanged                 STRING(20)                ! Time Changed
OldStatus                   STRING(30)                ! Old Status
NewStatus                   STRING(30)                ! New Status
UserCode                    STRING(3)                 ! User Code
                         END
                     END                       

IEXPDEFS             FILE,DRIVER('Btrieve'),OEM,PRE(iex),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! 
KeyRecNo                 KEY(iex:RecNo),NOCASE,PRIMARY ! 
KeyName                  KEY(iex:Name),NOCASE         ! 
KeyTypeName              KEY(iex:Type,iex:Name),DUP,NOCASE ! 
Record                   RECORD,PRE()
RecNo                       LONG                      ! 
Name                        STRING(50)                ! 
Type                        STRING(1)                 ! 
                         END
                     END                       

RAPIDLST             FILE,DRIVER('Btrieve'),OEM,PRE(RAP),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Rapids List
RecordNumberKey          KEY(RAP:RecordNumber),NOCASE,PRIMARY ! By Record Number
OrderKey                 KEY(RAP:RapidOrder,RAP:ProgramName),DUP,NOCASE ! By Order
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
RapidOrder                  LONG                      ! Rapid Order
ProgramName                 STRING(60)                ! Program Name
Description                 STRING(255)               ! Description
EXEPath                     STRING(255)               ! EXE Path
                         END
                     END                       

ICONTHIST            FILE,DRIVER('Btrieve'),OEM,PRE(ico),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)               ! 
KeyRecordNumber          KEY(ico:RecordNumber),NOCASE,PRIMARY ! 
KeyAction                KEY(ico:RefNumber,ico:Action,-ico:Date,-ico:Time),DUP,NOCASE ! 
KeyUser                  KEY(ico:RefNumber,ico:User,-ico:Date,-ico:Time),DUP,NOCASE ! 
KeyRefNumber             KEY(ico:RefNumber,-ico:Date,-ico:Time,ico:Action),DUP,NOCASE ! 
Record                   RECORD,PRE()
RecordNumber                REAL                      ! 
RefNumber                   LONG                      ! 
Date                        DATE                      ! 
Time                        TIME                      ! 
User                        STRING(3)                 ! 
Action                      STRING(80)                ! 
Notes                       STRING(1000)              ! 
Dummy                       STRING(1)                 ! 
                         END
                     END                       

ORDTEMP              FILE,DRIVER('Btrieve'),PRE(ort),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! 
recordnumberkey          KEY(ort:recordnumber),NOCASE,PRIMARY ! 
Keyordhno                KEY(ort:ordhno),DUP,NOCASE   ! 
Record                   RECORD,PRE()
recordnumber                LONG                      ! Record Number
ordhno                      LONG                      ! 
manufact                    STRING(40)                ! 
partno                      STRING(20)                ! 
partdiscription             STRING(60)                ! 
qty                         LONG                      ! 
itemcost                    REAL                      ! 
totalcost                   REAL                      ! 
thedate                     DATE                      ! 
                         END
                     END                       

ORDITEMS             FILE,DRIVER('Btrieve'),PRE(ori),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! 
Keyordhno                KEY(ori:ordhno),DUP,NOCASE   ! 
recordnumberkey          KEY(ori:recordnumber),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
recordnumber                LONG                      ! Record Number
ordhno                      LONG                      ! 
manufact                    STRING(40)                ! 
partno                      STRING(20)                ! 
partdiscription             STRING(60)                ! 
qty                         LONG                      ! 
itemcost                    REAL                      ! 
totalcost                   REAL                      ! 
                         END
                     END                       

ORDHEAD              FILE,DRIVER('Btrieve'),PRE(orh),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! 
KeyOrder_no              KEY(orh:Order_no),NOCASE,PRIMARY ! 
book_date_key            KEY(orh:thedate),DUP,NOCASE  ! 
pro_date_key             KEY(-orh:pro_date),DUP,NOCASE ! 
pro_ord_no_key           KEY(orh:procesed,orh:thedate,orh:Order_no),DUP,NOCASE ! 
pro_cust_name_key        KEY(orh:procesed,orh:pro_date,orh:CustName),DUP,NOCASE ! 
KeyCustName              KEY(orh:CustName),DUP,NOCASE ! 
AccountDateKey           KEY(orh:account_no,orh:thedate),DUP,NOCASE ! By Date
SalesNumberKey           KEY(orh:SalesNumber),DUP,NOCASE ! By Sales Number
ProcessSaleNoKey         KEY(orh:procesed,orh:SalesNumber),DUP,NOCASE ! By Sales Number
DateDespatchedKey        KEY(orh:DateDespatched),DUP,NOCASE ! By Date Despatched
Record                   RECORD,PRE()
Order_no                    LONG                      ! 
account_no                  STRING(40)                ! 
CustName                    STRING(30)                ! 
CustAdd1                    STRING(30)                ! 
CustAdd2                    STRING(30)                ! 
CustAdd3                    STRING(30)                ! 
CustPostCode                STRING(10)                ! 
CustTel                     STRING(20)                ! 
CustFax                     STRING(20)                ! 
dName                       STRING(30)                ! 
dAdd1                       STRING(30)                ! 
dAdd2                       STRING(30)                ! 
dAdd3                       STRING(30)                ! 
dPostCode                   STRING(10)                ! 
dTel                        STRING(20)                ! 
dFax                        STRING(20)                ! 
thedate                     DATE                      ! 
thetime                     TIME                      ! 
procesed                    BYTE                      ! 
pro_date                    DATE                      ! 
WhoBooked                   STRING(30)                ! Who Booked
Department                  STRING(30)                ! Department
CustOrderNumber             STRING(30)                ! Customer Order Number
DateDespatched              DATE                      ! Date Despatched
SalesNumber                 LONG                      ! Sales Transaction Number
                         END
                     END                       

IMEISHIP             FILE,DRIVER('Btrieve'),OEM,PRE(IMEI),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! I.M.E.I. Number Shipments
RecordNumberKey          KEY(IMEI:RecordNumber),NOCASE,PRIMARY ! By Record Number
IMEIKey                  KEY(IMEI:IMEINumber),DUP,NOCASE ! By I.M.E.I. Number
ShipDateKey              KEY(IMEI:ShipDate),DUP,NOCASE ! By Shipment Date
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
IMEINumber                  STRING(30)                ! I.M.E.I. Number
ShipDate                    DATE                      ! Shipment Date
BER                         BYTE                      ! Is this IMEI BER?
                         END
                     END                       

IACTION              FILE,DRIVER('Btrieve'),OEM,PRE(iac),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! 
KeyActionNo              KEY(iac:ActionNo),NOCASE,PRIMARY ! 
KeyAction                KEY(iac:Action),NOCASE       ! 
Record                   RECORD,PRE()
ActionNo                    LONG                      ! 
Action                      STRING(80)                ! 
                         END
                     END                       

JOBTHIRD             FILE,DRIVER('Btrieve'),OEM,PRE(jot),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Job Third Party Despatched
RecordNumberKey          KEY(jot:RecordNumber),NOCASE,PRIMARY ! By Record Number
RefNumberKey             KEY(jot:RefNumber),DUP,NOCASE ! By Ref Number
OutIMEIKey               KEY(jot:OutIMEI),DUP,NOCASE  ! By Outgoing IMEI Number
InIMEIKEy                KEY(jot:InIMEI),DUP,NOCASE   ! By Incoming IMEI Number
OutDateKey               KEY(jot:RefNumber,jot:DateOut,jot:RecordNumber),DUP,NOCASE ! By Outgoing Date
ThirdPartyKey            KEY(jot:ThirdPartyNumber),DUP,NOCASE ! 
OriginalIMEIKey          KEY(jot:OriginalIMEI),DUP,NOCASE ! By Original I.M.E.I. No
Record                   RECORD,PRE()
RecordNumber                LONG                      ! 
RefNumber                   LONG                      ! Link To Job's File
OriginalIMEI                STRING(30)                ! Original IMEI Number
OutIMEI                     STRING(30)                ! Outgoing I.M.E.I. Number
InIMEI                      STRING(30)                ! Incoming I.M.E.I. Number
DateOut                     DATE                      ! 
DateDespatched              DATE                      ! Date Despatched
DateIn                      DATE                      ! 
ThirdPartyNumber            LONG                      ! Link To Third Party Entry
OriginalMSN                 STRING(30)                ! Original MSN
OutMSN                      STRING(30)                ! Outgoing M.S.N.
InMSN                       STRING(30)                ! Incoming M.S.N.
                         END
                     END                       

DEFAULTV             FILE,DRIVER('Btrieve'),OEM,PRE(defv),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! 
RecordNumberKey          KEY(defv:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
VersionNumber               STRING(30)                ! Version Number
NagDate                     DATE                      ! Nag Date
ReUpdate                    BYTE                      ! ReUpdate
                         END
                     END                       

LOGRETRN             FILE,DRIVER('Btrieve'),OEM,PRE(lrtn),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Logistic Stock Returns
RefNumberKey             KEY(lrtn:RefNumber),NOCASE,PRIMARY ! By Ref Number
ClubNokiaKey             KEY(lrtn:ClubNokiaSite,lrtn:Date),DUP,NOCASE ! By Club Nokia Site
DateKey                  KEY(lrtn:Date),DUP,NOCASE    ! By Date
ReturnsNoKey             KEY(lrtn:ReturnsNumber,lrtn:Date),DUP,NOCASE ! By Returns Number
Record                   RECORD,PRE()
RefNumber                   LONG                      ! 
ClubNokiaSite               STRING(30)                ! 
ModelNumber                 STRING(30)                ! 
Quantity                    LONG                      ! 
ReturnsNumber               STRING(30)                ! 
Date                        DATE                      ! 
ReturnType                  STRING(3)                 ! QTY/SCN
DummyField                  STRING(1)                 ! For File Manager
To_Site                     STRING(30)                ! 
SalesCode                   STRING(30)                ! Sales Code
                         END
                     END                       

PRODCODE             FILE,DRIVER('Btrieve'),OEM,PRE(prd),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Product Codes For Model Numbers
RecordNumberKey          KEY(prd:RecordNumber),NOCASE,PRIMARY ! By Record Number
ProductCodeKey           KEY(prd:ProductCode),NOCASE  ! By Product Code
ModelProductKey          KEY(prd:ModelNumber,prd:ProductCode),DUP,NOCASE ! By Product Code
Record                   RECORD,PRE()
RecordNumber                LONG                      ! 
ModelNumber                 STRING(30)                ! 
ProductCode                 STRING(30)                ! 
DummyField                  STRING(2)                 ! For File Manager
                         END
                     END                       

LOG2TEMP             FILE,DRIVER('Btrieve'),OEM,PRE(lo2tmp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Logistic Temporary File
RefNumberKey             KEY(lo2tmp:RefNumber),NOCASE,PRIMARY ! By Ref Number
IMEIKey                  KEY(lo2tmp:IMEI),DUP,NOCASE  ! By I.M.E.I.
Record                   RECORD,PRE()
RefNumber                   LONG                      ! 
IMEI                        STRING(30)                ! 
Marker                      BYTE                      ! 
                         END
                     END                       

LOGSTOCK             FILE,DRIVER('Btrieve'),OEM,PRE(logsto),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Logistic Stock
RefNumberKey             KEY(logsto:RefNumber),NOCASE,PRIMARY ! By Ref Number
SalesKey                 KEY(logsto:SalesCode),DUP,NOCASE ! By Sales Code
DescriptionKey           KEY(logsto:Description),DUP,NOCASE ! By Description
SalesModelNoKey          KEY(logsto:SalesCode,logsto:ModelNumber),NOCASE ! By Sales Code
RefModelNoKey            KEY(logsto:RefNumber,logsto:ModelNumber),DUP,NOCASE ! By ModelNumber
ModelNumberKey           KEY(logsto:ModelNumber),DUP,NOCASE ! 
Record                   RECORD,PRE()
RefNumber                   LONG                      ! 
SalesCode                   STRING(30)                ! 
Description                 STRING(30)                ! 
ModelNumber                 STRING(30)                ! 
DummyField                  STRING(4)                 ! 
                         END
                     END                       

LABLGTMP             FILE,DRIVER('Btrieve'),OEM,PRE(lab),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! LabelG Temporary File
RefNumberKey             KEY(lab:RefNumber),NOCASE,PRIMARY ! By Ref Number
DateKey                  KEY(lab:Date,lab:AccountNo,lab:Postcode),DUP,NOCASE ! 
Record                   RECORD,PRE()
RefNumber                   LONG                      ! 
AccountNo                   STRING(15)                ! 
AccountName                 STRING(30)                ! 
Postcode                    STRING(15)                ! 
Date                        DATE                      ! 
JobNumber                   LONG                      ! 
ConsignmentNo               STRING(30)                ! 
Count                       LONG                      ! 
                         END
                     END                       

DEFEMAR1             FILE,DRIVER('Btrieve'),OEM,PRE(der1),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! UTL Email Default R1
RecordNumberKey          KEY(der1:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
R1Email                     STRING(4000)              ! R1 Email
Subject                     STRING(60)                ! Subject
                         END
                     END                       

LETTERS              FILE,DRIVER('Btrieve'),OEM,PRE(let),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Letter Editor
RecordNumberKey          KEY(let:RecordNumber),NOCASE,PRIMARY ! By Record Number
DescriptionKey           KEY(let:Description),NOCASE  ! By Description
Record                   RECORD,PRE()
RecordNumber                LONG                      ! 
Description                 STRING(255)               ! Letter's Description
Subject                     STRING(255)               ! Letter's Subject (i.e. RE:)
TelephoneNumber             STRING(20)                ! Telephone Number on top of letter
FaxNumber                   STRING(20)                ! Fax Number on top of letter
LetterText                  STRING(10000)             ! 
UseStatus                   STRING(3)                 ! Is a Status Change Required?
Status                      STRING(30)                ! 
DummyField                  STRING(1)                 ! For File Manager
                         END
                     END                       

DEFEMAB2             FILE,DRIVER('Btrieve'),OEM,PRE(deb2),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! UTL Email Default B2
RecordNumberKey          KEY(deb2:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
B2Email                     STRING(4000)              ! B2 Email
Subject                     STRING(60)                ! Subject
                         END
                     END                       

DEFEMAB3             FILE,DRIVER('Btrieve'),OEM,PRE(deb3),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! UTL Email Default B3
RecordNumberKey          KEY(deb3:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
B3Email                     STRING(4000)              ! B3 Email
Subject                     STRING(60)                ! Subject
                         END
                     END                       

DEFEMAB4             FILE,DRIVER('Btrieve'),OEM,PRE(deb4),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! UTL Email Default B4
RecordNumberKey          KEY(deb4:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
B4Email                     STRING(4000)              ! B4 Email
Subject                     STRING(60)                ! Subject
                         END
                     END                       

DEFEMAB5             FILE,DRIVER('Btrieve'),OEM,PRE(deb5),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! UTL Email Default B5
RecordNumberKey          KEY(deb5:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
B5Email                     STRING(4000)              ! B5 Email
Subject                     STRING(60)                ! Subject
                         END
                     END                       

DEFEMAB6             FILE,DRIVER('Btrieve'),OEM,PRE(deb6),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! UTL Email Default B6
RecordNumberKey          KEY(deb6:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
B6Email                     STRING(4000)              ! B6 Email
Subject                     STRING(60)                ! Subject
                         END
                     END                       

DEFEMAB7             FILE,DRIVER('Btrieve'),OEM,PRE(deb7),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! UTL Email Default B7
RecordNumberKey          KEY(deb7:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
B7Email                     STRING(4000)              ! B7 Email
Subject                     STRING(60)                ! Subject
                         END
                     END                       

DEFEMAE2             FILE,DRIVER('Btrieve'),OEM,PRE(dee2),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! UTL Email Default E2
RecordNumberKey          KEY(dee2:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
E2Email                     STRING(4000)              ! E2 Email
Subject                     STRING(60)                ! Subject
                         END
                     END                       

DEFEMAE3             FILE,DRIVER('Btrieve'),OEM,PRE(dee3),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! UTL Email Default E3
RecordNumberKey          KEY(dee3:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
E3Email                     STRING(4000)              ! E3 Email
Subject                     STRING(60)                ! Subject
                         END
                     END                       

DEFEME2B             FILE,DRIVER('Btrieve'),OEM,PRE(dee2B),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! UTL Email Default E2B
RecordNumberKey          KEY(dee2B:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
E2BEmail                    STRING(4000)              ! E2B Email
Subject                     STRING(60)                ! Subject
                         END
                     END                       

DEFEME3B             FILE,DRIVER('Btrieve'),OEM,PRE(dee3B),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! UTL Email Default E3B
RecordNumberKey          KEY(dee3B:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
E3BEmail                    STRING(4000)              ! E3B Email
Subject                     STRING(60)                ! Subject
                         END
                     END                       

DEFEMAR2             FILE,DRIVER('Btrieve'),OEM,PRE(der2),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! UTL Email Default R2
RecordNumberKey          KEY(der2:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
R2Email                     STRING(4000)              ! R2 Email
Subject                     STRING(60)                ! Subject
                         END
                     END                       

DEFEMAR3             FILE,DRIVER('Btrieve'),OEM,PRE(der3),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! UTL Email Default R3
RecordNumberKey          KEY(der3:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
R3Email                     STRING(4000)              ! R3 Email
Subject                     STRING(60)                ! Subject
                         END
                     END                       

DEFEMAR4             FILE,DRIVER('Btrieve'),OEM,PRE(der4),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! UTL Email Default R4
RecordNumberKey          KEY(der4:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
R4Email                     STRING(4000)              ! R4 Email
Subject                     STRING(60)                ! Subject
                         END
                     END                       

DEFEMAB1             FILE,DRIVER('Btrieve'),OEM,PRE(deb1),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! UTL Email Default B1
RecordNumberKey          KEY(deb1:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
B1Email                     STRING(4000)              ! B1 Email
Subject                     STRING(60)                ! Subject
                         END
                     END                       

DEFEMAE1             FILE,DRIVER('Btrieve'),OEM,PRE(dee1),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! UTL Email Default E1
RecordNumberKey          KEY(dee1:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
E1Email                     STRING(4000)              ! E1 Email
Subject                     STRING(60)                ! Subject
                         END
                     END                       

DEFEMAI2             FILE,DRIVER('Btrieve'),OEM,PRE(dem2),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! UTL SMS Defaults
RecordNumberKey          KEY(dem2:RecordNumber),NOCASE,PRIMARY ! Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
E1SMS                       STRING(480)               ! E1 SMS
E2SMS                       STRING(480)               ! E2 SMS
E3SMS                       STRING(480)               ! E3 SMS
B1SMS                       STRING(480)               ! B1 SMS
B2SMS                       STRING(480)               ! B2 SMS
B3SMS                       STRING(480)               ! B3 SMS
B4SMS                       STRING(480)               ! B4 SMS
B5SMS                       STRING(480)               ! B5 SMS
B6SMS                       STRING(480)               ! B6 SMS
B7SMS                       STRING(480)               ! B7 SMS
E1Active                    BYTE                      ! Alert Active
E2Active                    BYTE                      ! Alert Active
E3Active                    BYTE                      ! Alert Active
B1Active                    BYTE                      ! Alert Active
B2Active                    BYTE                      ! Alert Active
B3Active                    BYTE                      ! Alert Active
B4Active                    BYTE                      ! Alert Active
B5Active                    BYTE                      ! Alert Active
B6Active                    BYTE                      ! Alert Active
B7Active                    BYTE                      ! Alert Active
E1SMSActive                 BYTE                      ! Alert Active
E2SMSActive                 BYTE                      ! Alert Active
E3SMSActive                 BYTE                      ! Alert Active
B1SMSActive                 BYTE                      ! Alert Active
B2SMSActive                 BYTE                      ! Alert Active
B3SMSActive                 BYTE                      ! Alert Active
B4SMSActive                 BYTE                      ! Alert Active
B5SMSActive                 BYTE                      ! Alert Active
B6SMSActive                 BYTE                      ! Alert Active
B7SMSActive                 BYTE                      ! Alert Active
B6NotifyDays                LONG                      ! B6 Notify Days
B7NotifyDays                LONG                      ! B7 Notify Days
E2BSMS                      STRING(480)               ! E2B SMS
E3BSMS                      STRING(480)               ! E3B SMS
E2BActive                   BYTE                      ! Alert Active
E3BActive                   BYTE                      ! Alert Active
E2BSMSActive                BYTE                      ! Alert Active
E3BSMSActive                BYTE                      ! Alert Active
                         END
                     END                       

DEFSIDEX             FILE,DRIVER('Btrieve'),OEM,PRE(defe),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Defaults For Sid Extract
RecordNumberKey          KEY(defe:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
SMTPServer                  STRING(50)                ! SMTP Server
SMTPPortNumber              STRING(6)                 ! SMTP Port Number
SMTPUserName                STRING(60)                ! SMTP Username
SMTPPassword                STRING(60)                ! SMTP Password
FromEmailAddress            STRING(255)               ! From Email Address
EmailSubject                STRING(100)               ! Email Subject
EmailRecipientList          STRING(255)               ! Email Recipient List
FTPServer                   STRING(60)                ! FTP Server
FTPUsername                 STRING(60)                ! FTP Username
FTPPassword                 STRING(60)                ! Password
FTPExportPath               STRING(255)               ! FTP Export Path
ExportDays                  LONG                      ! Export Days
                         END
                     END                       

EXPGEN               FILE,DRIVER('ASCII'),OEM,PRE(gen),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! Export - Generic File
Record                   RECORD,PRE()
Line1                       STRING(2000)              ! 
                         END
                     END                       

STOAUDIT             FILE,DRIVER('Btrieve'),PRE(stoa),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! 
Internal_AutoNumber_Key  KEY(stoa:Internal_AutoNumber),NOCASE,PRIMARY ! 
Audit_Ref_No_Key         KEY(stoa:Audit_Ref_No),DUP,NOCASE ! 
Cellular_Key             KEY(stoa:Site_Location,stoa:Stock_Ref_No),DUP,NOCASE ! 
Stock_Ref_No_Key         KEY(stoa:Stock_Ref_No),DUP,NOCASE ! 
Stock_Site_Number_Key    KEY(stoa:Audit_Ref_No,stoa:Site_Location),DUP,NOCASE ! 
Lock_Down_Key            KEY(stoa:Audit_Ref_No,stoa:Stock_Ref_No),NOCASE ! 
Record                   RECORD,PRE()
Internal_AutoNumber         LONG                      ! 
Site_Location               STRING(30)                ! 
Stock_Ref_No                LONG                      ! 
Original_Level              LONG                      ! 
New_Level                   LONG                      ! 
Audit_Reason                STRING(255)               ! 
Audit_Ref_No                LONG                      ! 
Preliminary                 STRING(1)                 ! 
Confirmed                   STRING(1)                 ! 
                         END
                     END                       

DEFAULT2             FILE,DRIVER('Btrieve'),OEM,PRE(de2),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! 
RecordNumberKey          KEY(de2:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
UserSkillLevel              BYTE                      ! Allocate Skill Level To Users
Inv2CompanyName             STRING(30)                ! 
Inv2AddressLine1            STRING(30)                ! 
Inv2AddressLine2            STRING(30)                ! Address
Inv2AddressLine3            STRING(30)                ! 
Inv2Postcode                STRING(15)                ! 
Inv2TelephoneNumber         STRING(15)                ! 
Inv2FaxNumber               STRING(15)                ! 
Inv2EmailAddress            STRING(255)               ! Email Address
Inv2VATNumber               STRING(30)                ! 
CurrencySymbol              STRING(3)                 ! Currency Symbol
UseRequisitionNumber        BYTE                      ! Use Requisition Number
PickEngStockOnly            BYTE                      ! Pick Stock From Engineer's Location Only
AllocateEngPassword         BYTE                      ! Password Required To Allocate Engineer
PrintRetainedAccLabel       BYTE                      ! Print Retained Accessory Label
                         END
                     END                       

ACTION               FILE,DRIVER('Btrieve'),OEM,PRE(act),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! Action for Contact History
Record_Number_Key        KEY(act:Record_Number),NOCASE,PRIMARY ! By Record Number
Action_Key               KEY(act:Action),NOCASE       ! By Action
Record                   RECORD,PRE()
Record_Number               LONG                      ! 
Action                      STRING(30)                ! 
                         END
                     END                       

DEFRAPID             FILE,DRIVER('Btrieve'),OEM,PRE(der),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Defaults - Rapid Job Booking
Record_Number_Key        KEY(der:Record_Number),NOCASE,PRIMARY ! By Record_Number
Record                   RECORD,PRE()
Record_Number               LONG                      ! 
Print_Job_Card              STRING(20)                ! 
Print_Job_Label             STRING(3)                 ! 
Use_Transit_Type            STRING(20)                ! 
Transit_Type                STRING(30)                ! 
Use_Workshop                STRING(20)                ! 
Workshop                    STRING(20)                ! 
                         END
                     END                       

RETSALES             FILE,DRIVER('Btrieve'),OEM,PRE(ret),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Retail Sales
Ref_Number_Key           KEY(ret:Ref_Number),NOCASE,PRIMARY ! By Sales Number
Invoice_Number_Key       KEY(ret:Invoice_Number),DUP,NOCASE ! By Invoice Number
Despatched_Purchase_Key  KEY(ret:Despatched,ret:Purchase_Order_Number),DUP,NOCASE ! By Purchase Order No
Despatched_Ref_Key       KEY(ret:Despatched,ret:Ref_Number),DUP,NOCASE ! By Sales Number
Despatched_Account_Key   KEY(ret:Despatched,ret:Account_Number,ret:Purchase_Order_Number),DUP,NOCASE ! By Account No
Account_Number_Key       KEY(ret:Account_Number,ret:Ref_Number),DUP,NOCASE ! By Account Number
Purchase_Number_Key      KEY(ret:Purchase_Order_Number),DUP,NOCASE ! By Purchase Order No
Despatch_Number_Key      KEY(ret:Despatch_Number),DUP,NOCASE ! By Despatch_Number
Date_Booked_Key          KEY(ret:date_booked),DUP,NOCASE ! By Date Booked
AccountInvoiceKey        KEY(ret:Account_Number,ret:Invoice_Number),DUP,NOCASE ! By Invoice Number
DespatchedPurchDateKey   KEY(ret:Despatched,ret:Purchase_Order_Number,ret:date_booked),DUP,NOCASE ! By Date
DespatchNoRefNoKey       KEY(ret:Despatch_Number,ret:Ref_Number),DUP,NOCASE ! By Ref Number
Record                   RECORD,PRE()
Ref_Number                  LONG                      ! 
who_booked                  STRING(3)                 ! 
date_booked                 DATE                      ! 
time_booked                 TIME                      ! 
Account_Number              STRING(15)                ! 
Contact_Name                STRING(30)                ! 
Purchase_Order_Number       STRING(30)                ! 
Delivery_Collection         STRING(3)                 ! 
Payment_Method              STRING(3)                 ! 
Courier_Cost                REAL                      ! 
Parts_Cost                  REAL                      ! 
Sub_Total                   REAL                      ! 
Courier                     STRING(30)                ! 
Consignment_Number          STRING(30)                ! 
Date_Despatched             DATE                      ! 
Despatched                  STRING(3)                 ! 
Despatch_Number             LONG                      ! 
Invoice_Number              LONG                      ! 
Invoice_Date                DATE                      ! 
Invoice_Courier_Cost        REAL                      ! 
Invoice_Parts_Cost          REAL                      ! 
Invoice_Sub_Total           REAL                      ! 
WebOrderNumber              LONG                      ! Web Order Number
WebDateCreated              DATE                      ! Date Created
WebTimeCreated              STRING(20)                ! Time Created
WebCreatedUser              STRING(30)                ! Web User
Postcode                    STRING(10)                ! 
Company_Name                STRING(30)                ! 
Building_Name               STRING(30)                ! 
Address_Line1               STRING(30)                ! 
Address_Line2               STRING(30)                ! 
Address_Line3               STRING(30)                ! 
Telephone_Number            STRING(15)                ! 
Fax_Number                  STRING(15)                ! 
Postcode_Delivery           STRING(10)                ! 
Company_Name_Delivery       STRING(30)                ! 
Building_Name_Delivery      STRING(30)                ! 
Address_Line1_Delivery      STRING(30)                ! 
Address_Line2_Delivery      STRING(30)                ! 
Address_Line3_Delivery      STRING(30)                ! 
Telephone_Delivery          STRING(15)                ! 
Fax_Number_Delivery         STRING(15)                ! 
Delivery_Text               STRING(255)               ! 
Invoice_Text                STRING(255)               ! 
                         END
                     END                       

BOUNCER              FILE,DRIVER('Btrieve'),OEM,PRE(bou),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Bouncer Files
Record_Number_Key        KEY(bou:Record_Number),NOCASE,PRIMARY ! By Record Number
Bouncer_Job_Number_Key   KEY(bou:Original_Ref_Number,bou:Bouncer_Job_Number),NOCASE ! By Job Number
Bouncer_Job_Only_Key     KEY(bou:Bouncer_Job_Number),DUP,NOCASE ! By Job Number
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
Original_Ref_Number         REAL                      ! 
Bouncer_Job_Number          REAL                      ! 
                         END
                     END                       

TEAMS                FILE,DRIVER('Btrieve'),OEM,PRE(tea),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                   ! Teams
Record_Number_Key        KEY(tea:Record_Number),NOCASE,PRIMARY ! By Record Number
Team_Key                 KEY(tea:Team),NOCASE         ! By Team
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
Team                        STRING(30)                ! 
Completed_Jobs              LONG                      ! 
                         END
                     END                       

MERGE                FILE,DRIVER('Btrieve'),OEM,PRE(mer),BINDABLE,CREATE,THREAD,EXTERNAL(''),DLL(dll_mode)                   ! MergeFields
RefNumberKey             KEY(mer:RefNumber),NOCASE,PRIMARY ! By Ref Number
FieldNameKey             KEY(mer:FieldName),NOCASE    ! By Field Name
Record                   RECORD,PRE()
RefNumber                   LONG                      ! 
FieldName                   STRING(30)                ! 
FileName                    STRING(60)                ! 
Type                        STRING(3)                 ! String, Decimal, Date
Description                 STRING(255)               ! 
Capitals                    BYTE                      ! Upper / Caps / Lower
                         END
                     END                       

UPDDATA              FILE,DRIVER('Btrieve'),OEM,PRE(upd),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Update Data
RefNumberKey             KEY(upd:RefNumber),NOCASE,PRIMARY ! By Ref Number
TypeKey                  KEY(upd:Type,upd:DataField),DUP,NOCASE ! By Data
Record                   RECORD,PRE()
RefNumber                   LONG                      ! 
Type                        STRING(3)                 ! 
DataField                   STRING(30)                ! 
DataField2                  STRING(30)                ! 
DataField3                  STRING(30)                ! 
DataField4                  STRING(30)                ! 
DataField5                  STRING(30)                ! 
DataField6                  STRING(30)                ! 
DataField7                  STRING(30)                ! 
Description                 STRING(500)               ! 
                         END
                     END                       

EXPSPARES            FILE,DRIVER('BASIC'),OEM,PRE(expspa),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)               ! Export - Chargeable Parts
Record                   RECORD,PRE()
Supplier                    STRING(30)                ! 
Part_Number                 STRING(30)                ! 
Description                 STRING(30)                ! 
Quantity                    STRING(30)                ! 
Sale_Cost                   STRING(20)                ! 
Total_Cost                  STRING(20)                ! 
                         END
                     END                       

WEBDEFLT             FILE,DRIVER('Btrieve'),OEM,PRE(web),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! 
ordnokey                 KEY(web:ordno),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
ordno                       LONG                      ! 
                         END
                     END                       

EXCAUDIT             FILE,DRIVER('Btrieve'),OEM,PRE(exa),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Exchange Unit Audit Number File
RecordNumberKey          KEY(exa:Record_Number),NOCASE,PRIMARY ! By Record Number
Audit_Number_Key         KEY(exa:Stock_Type,exa:Audit_Number),NOCASE ! By Audit Number
StockUnitNoKey           KEY(exa:Stock_Unit_Number),DUP,NOCASE ! 
ReplaceUnitNoKey         KEY(exa:Replacement_Unit_Number),DUP,NOCASE ! 
TypeStockNumber          KEY(exa:Stock_Type,exa:Stock_Unit_Number,exa:Audit_Number),DUP,NOCASE ! By Audit Number
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
Stock_Type                  STRING(30)                ! 
Audit_Number                REAL                      ! 
Stock_Unit_Number           REAL                      ! 
Replacement_Unit_Number     REAL                      ! 
                         END
                     END                       

COMMONFA             FILE,DRIVER('Btrieve'),OEM,PRE(com),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Common Faults
Ref_Number_Key           KEY(com:Ref_Number),NOCASE,PRIMARY ! By Ref Number
Description_Key          KEY(com:Model_Number,com:Category,com:Description),DUP,NOCASE ! By Description
DescripOnlyKey           KEY(com:Model_Number,com:Description),DUP,NOCASE ! By Description
Ref_Model_Key            KEY(com:Model_Number,com:Category,com:Ref_Number),DUP,NOCASE ! By Ref Number
RefOnlyKey               KEY(com:Model_Number,com:Ref_Number),DUP,NOCASE ! By Ref Number
Record                   RECORD,PRE()
Ref_Number                  REAL                      ! 
Category                    STRING(30)                ! 
date_booked                 DATE                      ! 
time_booked                 TIME                      ! 
who_booked                  STRING(3)                 ! 
Model_Number                STRING(30)                ! 
Description                 STRING(30)                ! 
Chargeable_Job              STRING(3)                 ! 
Chargeable_Charge_Type      STRING(30)                ! 
Chargeable_Repair_Type      STRING(30)                ! 
Warranty_Job                STRING(3)                 ! 
Warranty_Charge_Type        STRING(30)                ! 
Warranty_Repair_Type        STRING(30)                ! 
Auto_Complete               STRING(3)                 ! 
Attach_Diagram              STRING(3)                 ! 
Diagram_Setting             STRING(1)                 ! 
Diagram_Path                STRING(255)               ! 
Fault_Codes1                GROUP                     ! 
Fault_Code1                   STRING(30)              ! 
Fault_Code2                   STRING(30)              ! 
Fault_Code3                   STRING(30)              ! 
Fault_Code4                   STRING(30)              ! 
Fault_Code5                   STRING(30)              ! 
Fault_Code6                   STRING(30)              ! 
                            END                       ! 
Fault_Codes2                GROUP                     ! 
Fault_Code7                   STRING(30)              ! 
Fault_Code8                   STRING(30)              ! 
Fault_Code9                   STRING(30)              ! 
Fault_Code10                  STRING(255)             ! 
Fault_Code11                  STRING(255)             ! 
Fault_Code12                  STRING(255)             ! 
                            END                       ! 
Invoice_Text                STRING(255)               ! 
Engineers_Notes             STRING(255)               ! 
DummyField                  STRING(2)                 ! For File Manager
                         END
                     END                       

PARAMSS              FILE,DRIVER('BASIC'),PRE(prm),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Parameters For Sage
Record                   RECORD,PRE()
Account_Ref                 STRING(8)                 ! 
Name                        STRING(60)                ! 
Address_1                   STRING(60)                ! 
Address_2                   STRING(60)                ! 
Address_3                   STRING(60)                ! 
Address_4                   STRING(60)                ! 
Address_5                   STRING(60)                ! 
Del_Address_1               STRING(60)                ! 
Del_Address_2               STRING(60)                ! 
Del_Address_3               STRING(60)                ! 
Del_Address_4               STRING(60)                ! 
Del_Address_5               STRING(60)                ! 
Cust_Tel_Number             STRING(30)                ! 
Contact_Name                STRING(30)                ! 
Notes_1                     STRING(60)                ! 
Notes_2                     STRING(60)                ! 
Notes_3                     STRING(60)                ! 
Taken_By                    STRING(60)                ! 
Order_Number                STRING(7)                 ! 
Cust_Order_Number           STRING(30)                ! 
Payment_Ref                 STRING(8)                 ! 
Global_Nom_Code             STRING(8)                 ! 
Global_Details              STRING(60)                ! 
Items_Net                   STRING(8)                 ! 
Items_Tax                   STRING(8)                 ! 
Stock_Code                  STRING(30)                ! 
Description                 STRING(60)                ! 
Nominal_Code                STRING(8)                 ! 
Qty_Order                   STRING(8)                 ! 
Unit_Price                  STRING(8)                 ! 
Net_Amount                  STRING(8)                 ! 
Tax_Amount                  STRING(8)                 ! 
Comment_1                   STRING(60)                ! 
Comment_2                   STRING(60)                ! 
Unit_Of_Sale                STRING(8)                 ! 
Full_Net_Amount             STRING(8)                 ! 
Invoice_Date                STRING(10)                ! 
Data_filepath               STRING(255)               ! 
User_Name                   STRING(30)                ! 
Password                    STRING(30)                ! 
Set_Invoice_Number          STRING(8)                 ! 
Invoice_No                  STRING(8)                 ! 
                         END
                     END                       

INSJOBS              FILE,DRIVER('Btrieve'),OEM,PRE(ijob),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Install Jobs
RefNumberKey             KEY(-ijob:RefNumber),NOCASE,PRIMARY ! By Job Number
CompletedKey             KEY(ijob:EndDate,ijob:RefNumber),DUP,NOCASE ! By Job Number
InvoiceNumberKey         KEY(ijob:InvoiceNumber,ijob:RefNumber),DUP,NOCASE ! By Job Number
JobTypeKey               KEY(ijob:JobType),DUP,NOCASE ! 
JobStatusKey             KEY(ijob:JobStatus),DUP,NOCASE ! 
CompanyKey               KEY(ijob:AccountName),DUP,NOCASE ! 
ContactKey               KEY(ijob:ContactName),DUP,NOCASE ! 
PostCodeKey              KEY(ijob:Postcode),DUP,NOCASE ! 
AllocJobKey              KEY(ijob:SubContractor,-ijob:RefNumber),DUP,NOCASE ! 
AllocCompKey             KEY(ijob:SubContractor,ijob:AccountName),DUP,NOCASE ! 
AllocPostCodeKey         KEY(ijob:SubContractor,ijob:Postcode),DUP,NOCASE ! 
AllocStatusKey           KEY(ijob:SubContractor,ijob:JobStatus),DUP,NOCASE ! 
AllocContactKey          KEY(ijob:SubContractor,ijob:ContactName),DUP,NOCASE ! 
KeyClient                KEY(ijob:Client),DUP,NOCASE  ! 
KeyClientCoord           KEY(ijob:Client,ijob:WhoBooked),DUP,NOCASE ! 
KeyAllocClient           KEY(ijob:SubContractor,ijob:WhoBooked,ijob:Client),DUP,NOCASE ! 
KeyCoord                 KEY(ijob:WhoBooked),DUP,NOCASE ! 
KeyAllocCoord            KEY(ijob:SubContractor,ijob:WhoBooked),DUP,NOCASE ! 
Record                   RECORD,PRE()
RefNumber                   LONG                      ! Job Number
DateBooked                  DATE                      ! Date Booked
TimeBooked                  TIME                      ! Time Booked
WhoBooked                   STRING(3)                 ! Who Booked
AccountNumber               STRING(30)                ! Trade Account Number
AccountName                 STRING(30)                ! Trade Account Name
Postcode1                   STRING(4)                 ! Postcode
Postcode2                   STRING(4)                 ! Postcode
BuildingName                STRING(30)                ! Building Name
AddressLine1                STRING(30)                ! Address Line 1
AddressLine2                STRING(30)                ! Address Line 2
AddressLine3                STRING(30)                ! Address Line 3
TelephoneNumber             STRING(30)                ! Telephone Number
FaxNumber                   STRING(30)                ! Fax Number
MobileNumber                STRING(30)                ! Mobile Number
OtherNumber                 STRING(30)                ! Other Telephone Number
ContactName                 STRING(30)                ! Contact Name
EmailAddress                STRING(255)               ! Email Address
OrderNumber                 STRING(30)                ! Trade Order Number
NoOfJobs                    LONG                      ! Number Of Jobs
EngineersOnSite             LONG                      ! Number Of Engineers On Site
SubContractor               STRING(30)                ! Sub Contractor
StartDate                   DATE                      ! Start Date
StartTime                   TIME                      ! Start Time
EndDate                     DATE                      ! Completion Date
EndTime                     TIME                      ! Completion Time
Completed                   BYTE                      ! Job Completed
SpecialInstruct             STRING(255)               ! 
InvoiceNumber               LONG                      ! Invoice Number
DummyField                  STRING(13)                ! For File Manager
AssStock1                   BYTE                      ! 1
AssStock2                   BYTE                      ! 2
AssStock3                   BYTE                      ! 3
AssStock4                   BYTE                      ! 4
AssStock5                   BYTE                      ! 5
AssStock6                   BYTE                      ! 6
AssStock7                   BYTE                      ! 7
AssStock8                   BYTE                      ! 8
AssStock9                   BYTE                      ! 9
AssStock10                  BYTE                      ! 10
AssStock11                  BYTE                      ! 
AssStock12                  BYTE                      ! 
AssStock13                  BYTE                      ! 
AssStock14                  BYTE                      ! 
AssStock15                  BYTE                      ! 
AssStock16                  BYTE                      ! 
JobType                     STRING(30)                ! 
JobStatus                   STRING(30)                ! 
Invoiced                    BYTE                      ! 
Surveyed                    BYTE                      ! 
EngineerInfo                STRING(255)               ! 
Client                      STRING(30)                ! 
DolphinRef                  STRING(20)                ! 
MainJobType                 BYTE                      ! 
CityLinkRef                 STRING(30)                ! 
NoStoreys                   LONG                      ! 
ApproxHeight                LONG                      ! 
Brick                       BYTE                      ! 
Glass                       BYTE                      ! 
House                       CSTRING(20)               ! 
Portacabin                  BYTE                      ! 
CladdingDesign              BYTE                      ! 
POD                         STRING(100)               ! 
Postcode                    STRING(10)                ! 
Projected                   LONG                      ! 
                         END
                     END                       

CONSIGN              FILE,DRIVER('Btrieve'),OEM,PRE(cns),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Consignment Note Number File
Record_Number_Key        KEY(cns:Record_Number),NOCASE,PRIMARY ! By Record Number
Consignment_Number_Key   KEY(cns:Consignment_Note_Number,cns:Ref_Number),DUP,NOCASE ! By Consignment Number
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
Consignment_Note_Number     STRING(30)                ! 
Ref_Number                  REAL                      ! 
Type                        STRING(3)                 ! 
                         END
                     END                       

JOBEXACC             FILE,DRIVER('Btrieve'),OEM,PRE(jea),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Job Exchange Accessories
Record_Number_Key        KEY(jea:Record_Number),NOCASE,PRIMARY ! 
Part_Number_Key          KEY(jea:Job_Ref_Number,jea:Part_Number),DUP,NOCASE ! 
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
Job_Ref_Number              REAL                      ! 
Stock_Ref_Number            REAL                      ! 
Part_Number                 STRING(30)                ! 
Description                 STRING(30)                ! 
                         END
                     END                       

MODELCOL             FILE,DRIVER('Btrieve'),OEM,PRE(moc),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Model Colours
Record_Number_Key        KEY(moc:Record_Number),NOCASE,PRIMARY ! 
Colour_Key               KEY(moc:Model_Number,moc:Colour),NOCASE ! By Colour
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
Model_Number                STRING(30)                ! 
Colour                      STRING(30)                ! 
                         END
                     END                       

COMMCAT              FILE,DRIVER('Btrieve'),OEM,PRE(cmc),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Common Fault Category
Record_Number_Key        KEY(cmc:Record_Number),NOCASE,PRIMARY ! By Record Number
Category_Key             KEY(cmc:Model_Number,cmc:Category),NOCASE ! By Category
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
Model_Number                STRING(30)                ! 
Category                    STRING(30)                ! 
                         END
                     END                       

MESSAGES             FILE,DRIVER('Btrieve'),PRE(mes),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Messages - Internal Email
Record_Number_Key        KEY(mes:Record_Number),NOCASE,PRIMARY ! By Record Number
Read_Key                 KEY(mes:Read,mes:Message_For,-mes:Date),DUP,NOCASE ! 
Who_To_Key               KEY(mes:Message_For,-mes:Date),DUP,NOCASE ! 
Who_From_Key             KEY(mes:Message_For,mes:Message_From),DUP,NOCASE ! 
Read_Only_Key            KEY(mes:Read,-mes:Date),DUP,NOCASE ! 
Date_Only_Key            KEY(-mes:Date),DUP,NOCASE    ! 
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
Date                        DATE                      ! 
Time                        TIME                      ! 
Message_For                 STRING(3)                 ! 
Message_From                STRING(3)                 ! 
Comment                     STRING(80)                ! 
Read                        STRING(3)                 ! 
Message_Memo                STRING(1000)              ! 
                         END
                     END                       

LOGEXHE              FILE,DRIVER('Btrieve'),OEM,PRE(log1),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Logistic Exhange Stock Header
Batch_Number_Key         KEY(log1:Batch_No),NOCASE,PRIMARY ! 
Processed_Key            KEY(log1:Processed,log1:Batch_No),DUP,NOCASE ! 
Record                   RECORD,PRE()
Batch_No                    LONG                      ! 
Processed                   STRING(1)                 ! 
Date                        DATE                      ! 
SMPF_No                     STRING(30)                ! 
ModelNumber                 STRING(30)                ! 
Qty                         LONG                      ! 
                         END
                     END                       

DEFCRC               FILE,DRIVER('Btrieve'),OEM,PRE(dfc),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! Defaults For CRC Import
RecordNumberKey          KEY(dfc:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
AccountNumber               STRING(30)                ! Account Number
TransitType                 STRING(30)                ! Transit Type
Location                    STRING(30)                ! Location
PAccountNumber              STRING(30)                ! Account Number
PTransitType                STRING(30)                ! Transit Type
PLocation                   STRING(30)                ! Location
EAccountNumber              STRING(30)                ! Account Number (End User)
ETransitType                STRING(30)                ! Transit Type (End User)
ELocation                   STRING(30)                ! Location (End User)
SAccountNumber              STRING(30)                ! Account Number
STransitType                STRING(30)                ! Transit Type
STransitType2               STRING(30)                ! Transit Type
SLocation                   STRING(30)                ! Location
                         END
                     END                       

PAYTYPES             FILE,DRIVER('Btrieve'),PRE(pay),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Payment Types
Payment_Type_Key         KEY(pay:Payment_Type),NOCASE,PRIMARY ! By Payment Type
Record                   RECORD,PRE()
Payment_Type                STRING(30)                ! 
Credit_Card                 STRING(3)                 ! 
                         END
                     END                       

COMMONWP             FILE,DRIVER('Btrieve'),PRE(cwp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Common Faults - Warranty Parts
RecordNumberKey          KEY(cwp:Record_Number),NOCASE,PRIMARY ! 
Ref_Number_Key           KEY(cwp:Ref_Number),DUP,NOCASE ! By Ref Number
Description_Key          KEY(cwp:Ref_Number,cwp:Description),DUP,NOCASE ! By Description
RefPartNumberKey         KEY(cwp:Ref_Number,cwp:Part_Number),DUP,NOCASE ! 
PartNumberKey            KEY(cwp:Part_Number),DUP,NOCASE ! By Part Number
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
Ref_Number                  REAL                      ! 
Quantity                    REAL                      ! 
Part_Ref_Number             REAL                      ! Reference To The Stock File
Adjustment                  STRING(3)                 ! 
Exclude_From_Order          STRING(3)                 ! 
Part_Number                 STRING(30)                ! 
Description                 STRING(30)                ! 
Supplier                    STRING(30)                ! 
Purchase_Cost               REAL                      ! 
Sale_Cost                   REAL                      ! 
Fault_Code1                 STRING(30)                ! 
Fault_Code2                 STRING(30)                ! 
Fault_Code3                 STRING(30)                ! 
Fault_Code4                 STRING(30)                ! 
Fault_Code5                 STRING(30)                ! 
Fault_Code6                 STRING(30)                ! 
Fault_Code7                 STRING(30)                ! 
Fault_Code8                 STRING(30)                ! 
Fault_Code9                 STRING(30)                ! 
Fault_Code10                STRING(30)                ! 
Fault_Code11                STRING(30)                ! 
Fault_Code12                STRING(30)                ! 
DummyField                  STRING(1)                 ! For File Manager
                         END
                     END                       

LOGEXCH              FILE,DRIVER('Btrieve'),PRE(xch1),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Logistic Exchange units
Batch_Number_Key         KEY(xch1:Batch_Number),DUP,NOCASE ! 
Ref_Number_Key           KEY(xch1:Ref_Number),NOCASE,PRIMARY ! By Loan Unit Number
ESN_Only_Key             KEY(xch1:ESN),DUP,NOCASE     ! 
MSN_Only_Key             KEY(xch1:MSN),DUP,NOCASE     ! 
Ref_Number_Stock_Key     KEY(xch1:Stock_Type,xch1:Ref_Number),DUP,NOCASE ! By Ref Number
Model_Number_Key         KEY(xch1:Stock_Type,xch1:Model_Number,xch1:Ref_Number),DUP,NOCASE ! By Model Number
ESN_Key                  KEY(xch1:Stock_Type,xch1:ESN),DUP,NOCASE ! By E.S.N. / I.M.E.I. Number
MSN_Key                  KEY(xch1:Stock_Type,xch1:MSN),DUP,NOCASE ! By M.S.N. Number
ESN_Available_Key        KEY(xch1:Available,xch1:Stock_Type,xch1:ESN),DUP,NOCASE ! By E.S.N. / I.M.E.I.
MSN_Available_Key        KEY(xch1:Available,xch1:Stock_Type,xch1:MSN),DUP,NOCASE ! By M.S.N.
Ref_Available_Key        KEY(xch1:Available,xch1:Stock_Type,xch1:Ref_Number),DUP,NOCASE ! By Exchange Unit Number
Model_Available_Key      KEY(xch1:Available,xch1:Stock_Type,xch1:Model_Number,xch1:Ref_Number),DUP,NOCASE ! By Model Number
Stock_Type_Key           KEY(xch1:Stock_Type),DUP,NOCASE ! By Stock Type
ModelRefNoKey            KEY(xch1:Model_Number,xch1:Ref_Number),DUP,NOCASE ! By Unit Number
AvailIMEIOnlyKey         KEY(xch1:Available,xch1:ESN),DUP,NOCASE ! By I.M.E.I. Number
AvailMSNOnlyKey          KEY(xch1:Available,xch1:MSN),DUP,NOCASE ! By M.S.N.
AvailRefOnlyKey          KEY(xch1:Available,xch1:Ref_Number),DUP,NOCASE ! By Unit Number
AvailModOnlyKey          KEY(xch1:Available,xch1:Model_Number,xch1:Ref_Number),DUP,NOCASE ! By Unit Number
Record                   RECORD,PRE()
Ref_Number                  LONG                      ! Unit Number
Batch_Number                LONG                      ! 
Model_Number                STRING(30)                ! 
Manufacturer                STRING(30)                ! 
ESN                         STRING(30)                ! 
MSN                         STRING(30)                ! 
Colour                      STRING(30)                ! 
Location                    STRING(30)                ! 
Shelf_Location              STRING(30)                ! 
Date_Booked                 DATE                      ! 
Times_Issued                REAL                      ! 
Available                   STRING(3)                 ! 
Job_Number                  LONG                      ! Job Number
Stock_Type                  STRING(30)                ! 
                         END
                     END                       

QAREASON             FILE,DRIVER('Btrieve'),OEM,PRE(qar),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! QA Rejection Reasons
RecordNumberKey          KEY(qar:RecordNumber),NOCASE,PRIMARY ! By Record Number
ReasonKey                KEY(qar:Reason),NOCASE       ! By Reason
Record                   RECORD,PRE()
RecordNumber                LONG                      ! 
Reason                      STRING(60)                ! 
                         END
                     END                       

COMMONCP             FILE,DRIVER('Btrieve'),PRE(ccp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Common Faults - Chargeable Parts
RecordNumberKey          KEY(ccp:Record_Number),NOCASE,PRIMARY ! 
Ref_Number_Key           KEY(ccp:Ref_Number),DUP,NOCASE ! By Ref Number
Description_Key          KEY(ccp:Ref_Number,ccp:Description),DUP,NOCASE ! By Description
RefPartNumberKey         KEY(ccp:Ref_Number,ccp:Part_Number),DUP,NOCASE ! 
PartNumberKey            KEY(ccp:Part_Number),DUP,NOCASE ! By Part Number
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
Ref_Number                  REAL                      ! 
Quantity                    REAL                      ! 
Part_Ref_Number             REAL                      ! Reference To The Stock File
Adjustment                  STRING(3)                 ! 
Exclude_From_Order          STRING(3)                 ! 
Part_Number                 STRING(30)                ! 
Description                 STRING(30)                ! 
Supplier                    STRING(30)                ! 
Purchase_Cost               REAL                      ! 
Sale_Cost                   REAL                      ! 
Fault_Code1                 STRING(30)                ! 
Fault_Code2                 STRING(30)                ! 
Fault_Code3                 STRING(30)                ! 
Fault_Code4                 STRING(30)                ! 
Fault_Code5                 STRING(30)                ! 
Fault_Code6                 STRING(30)                ! 
Fault_Code7                 STRING(30)                ! 
Fault_Code8                 STRING(30)                ! 
Fault_Code9                 STRING(30)                ! 
Fault_Code10                STRING(30)                ! 
Fault_Code11                STRING(30)                ! 
Fault_Code12                STRING(30)                ! 
DummyField                  STRING(1)                 ! For File Manager
                         END
                     END                       

ADDSEARCH            FILE,DRIVER('TOPSPEED'),OEM,PRE(addtmp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)               ! Surname/Address Search File
RecordNumberKey          KEY(addtmp:RecordNumber),NOCASE,PRIMARY ! By Record Number
AddressLine1Key          KEY(addtmp:AddressLine1),DUP,NOCASE ! By Address
PostcodeKey              KEY(addtmp:Postcode),DUP,NOCASE ! By Postcode
IncomingIMEIKey          KEY(addtmp:IncomingIMEI),DUP,NOCASE ! By Incoming I.M.E.I. No
ExchangedIMEIKey         KEY(addtmp:ExchangedIMEI),DUP,NOCASE ! By Exchanged I.M.E.I. No
FinalIMEIKey             KEY(addtmp:FinalIMEI),DUP,NOCASE ! By Final I.M.E.I. No
JobNumberKey             KEY(addtmp:JobNumber),DUP,NOCASE ! By Job Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
AddressLine1                STRING(30)                ! Address Line 1
AddressLine2                STRING(30)                ! Address Line 2
AddressLine3                STRING(30)                ! Address Line 3
Postcode                    STRING(30)                ! Postcode
JobNumber                   LONG                      ! Job Number
IncomingIMEI                STRING(30)                ! Incoming I.M.E.I. Number
ExchangedIMEI               STRING(30)                ! Exchanged I.M.E.I. Number
FinalIMEI                   STRING(30)                ! Final I.M.E.I. Number
Surname                     STRING(30)                ! Surname
                         END
                     END                       

SIDDEF               FILE,DRIVER('Btrieve'),PRE(SID),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! SID Defaults
RecordNumberKey          KEY(SID:RecordNumber),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
FTPServer                   STRING(30)                ! FTP Server
FTPUsername                 STRING(30)                ! FTP Username
FTPPassword                 STRING(30)                ! FTP Password
FTPPath                     STRING(255)               ! FTP Path
ContactName                 STRING(30)                ! Contact Name
ContactNumber               STRING(30)                ! Contact Number
ContactEmail                STRING(255)               ! Contact Email
SMTPServer                  STRING(50)                ! SMTP Server
SMTPPort                    STRING(3)                 ! SMTP Server Port
SMTPUserName                STRING(30)                ! SMTP User Name
SMTPPassword                STRING(30)                ! SMTP Password
EmailFromAddress            STRING(255)               ! From Email Address
EmailSubject                STRING(60)                ! Email Subject
EmailToAddress              STRING(255)               ! FTP Email Alert Recipients
ProcessFailureCount         LONG                      ! Send Alert Email After (xx) Successive Process Failures
FTPFailedCount              LONG                      ! 
UnallocTransitToSC          LONG                      ! Number of days to arrive at service centre from store
UnallocTurnaround           LONG                      ! 
UnallocTransitStore         LONG                      ! Number of days to arrive at store from service centre
WebJobConn                  STRING(50)                ! WebJob SQL Connection String
CPMConn                     STRING(50)                ! CPM SQL Connection String
                         END
                     END                       

CONTHIST             FILE,DRIVER('Btrieve'),PRE(cht),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Contact History
Ref_Number_Key           KEY(cht:Ref_Number,-cht:Date,-cht:Time,cht:Action),DUP,NOCASE ! By Date
Action_Key               KEY(cht:Ref_Number,cht:Action,-cht:Date),DUP,NOCASE ! By Action
User_Key                 KEY(cht:Ref_Number,cht:User,-cht:Date),DUP,NOCASE ! By User
Record_Number_Key        KEY(cht:record_number),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
record_number               REAL                      ! 
Ref_Number                  REAL                      ! 
Date                        DATE                      ! 
Time                        TIME                      ! 
User                        STRING(3)                 ! 
Action                      STRING(80)                ! 
Notes                       STRING(1000)              ! 
                         END
                     END                       

CONTACTS             FILE,DRIVER('Btrieve'),PRE(con),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Contact Database
Name_Key                 KEY(con:Name),NOCASE,PRIMARY ! By Name
Record                   RECORD,PRE()
Name                        STRING(30)                ! 
Postcode                    STRING(10)                ! 
Address_Line1               STRING(30)                ! 
Address_Line2               STRING(30)                ! 
Address_Line3               STRING(30)                ! 
Telephone_Number            STRING(15)                ! 
Fax_Number                  STRING(15)                ! 
Contact_Name1               STRING(30)                ! 
Contact_Name2               STRING(30)                ! 
                         END
                     END                       

EXPJOBS              FILE,DRIVER('BASIC'),PRE(expjob),BINDABLE,CREATE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Export - Jobs
Record                   RECORD,PRE()
Ref_Number                  STRING(20)                ! 
Batch_Number                STRING(20)                ! 
who_booked                  STRING(20)                ! 
date_booked                 STRING(20)                ! 
time_booked                 STRING(20)                ! 
Current_Status              STRING(30)                ! 
Title                       STRING(20)                ! 
Initial                     STRING(20)                ! 
Surname                     STRING(30)                ! 
Account_Number              STRING(15)                ! 
Order_Number                STRING(30)                ! 
Chargeable_Job              STRING(20)                ! 
Charge_Type                 STRING(30)                ! 
Repair_Type                 STRING(30)                ! 
Warranty_Job                STRING(20)                ! 
Warranty_Charge_Type        STRING(30)                ! 
Repair_Type_Warranty        STRING(30)                ! 
Unit_Details_Group          GROUP                     ! 
Model_Number                  STRING(30)              ! 
Manufacturer                  STRING(30)              ! 
ESN                           STRING(16)              ! 
MSN                           STRING(16)              ! 
Unit_Type                     STRING(30)              ! 
                            END                       ! 
Mobile_Number               STRING(15)                ! 
Workshop                    STRING(20)                ! 
Location                    STRING(30)                ! 
Authority_Number            STRING(30)                ! 
DOP                         STRING(20)                ! 
Physical_Damage             STRING(20)                ! 
Intermittent_Fault          STRING(20)                ! 
Transit_Type                STRING(30)                ! 
Job_Priority                STRING(30)                ! 
Engineer                    STRING(20)                ! 
Address_Group               GROUP                     ! 
Postcode                      STRING(20)              ! 
Company_Name                  STRING(30)              ! 
Address_Line1                 STRING(30)              ! 
Address_Line2                 STRING(30)              ! 
Address_Line3                 STRING(30)              ! 
Telephone_Number              STRING(15)              ! 
Fax_Number                    STRING(15)              ! 
                            END                       ! 
Address_Collection_Group    GROUP                     ! 
Postcode_Collection           STRING(20)              ! 
Company_Name_Collection       STRING(30)              ! 
Address_Line1_Collection      STRING(30)              ! 
Address_Line2_Collection      STRING(30)              ! 
Address_Line3_Collection      STRING(30)              ! 
Telephone_Collection          STRING(15)              ! 
                            END                       ! 
Address_Delivery_Group      GROUP                     ! 
Postcode_Delivery             STRING(20)              ! 
Company_Name_Delivery         STRING(30)              ! 
Address_Line1_Delivery        STRING(30)              ! 
Address_Line2_Delivery        STRING(30)              ! 
Address_Line3_Delivery        STRING(30)              ! 
Telephone_Delivery            STRING(15)              ! 
                            END                       ! 
In_Repair_Group             GROUP                     ! 
In_Repair                     STRING(20)              ! 
Date_In_Repair                STRING(20)              ! 
Time_In_Repair                STRING(20)              ! 
                            END                       ! 
On_Test_Group               GROUP                     ! 
On_Test                       STRING(20)              ! 
Date_On_Test                  STRING(20)              ! 
Time_On_Test                  STRING(20)              ! 
                            END                       ! 
Completed_Group             GROUP                     ! 
Date_Completed                STRING(20)              ! 
Time_Completed                STRING(20)              ! 
                            END                       ! 
QA_Group                    GROUP                     ! 
QA_Passed                     STRING(20)              ! 
Date_QA_Passed                STRING(20)              ! 
Time_QA_Passed                STRING(20)              ! 
QA_Rejected                   STRING(20)              ! 
Date_QA_Rejected              STRING(20)              ! 
Time_QA_Rejected              STRING(20)              ! 
QA_Second_Passed              STRING(20)              ! 
Date_QA_Second_Passed         STRING(20)              ! 
Time_QA_Second_Passed         STRING(20)              ! 
                            END                       ! 
Estimate_Ready              STRING(20)                ! 
Estimate_Group              GROUP                     ! 
Estimate                      STRING(20)              ! 
Estimate_If_Over              STRING(20)              ! 
Estimate_Accepted             STRING(20)              ! 
Estimate_Rejected             STRING(20)              ! 
                            END                       ! 
Chargeable_Costs            GROUP                     ! 
Courier_Cost                  STRING(20)              ! 
Labour_Cost                   STRING(20)              ! 
Parts_Cost                    STRING(20)              ! 
Sub_Total                     STRING(20)              ! 
                            END                       ! 
Estimate_Costs              GROUP                     ! 
Courier_Cost_Estimate         STRING(20)              ! 
Labour_Cost_Estimate          STRING(20)              ! 
Parts_Cost_Estimate           STRING(20)              ! 
Sub_Total_Estimate            STRING(20)              ! 
                            END                       ! 
Warranty_Costs              GROUP                     ! 
Courier_Cost_Warranty         STRING(20)              ! 
Labour_Cost_Warranty          STRING(20)              ! 
Parts_Cost_Warranty           STRING(20)              ! 
Sub_Total_Warranty            STRING(20)              ! 
                            END                       ! 
Payment_Group               GROUP                     ! 
Paid                          STRING(20)              ! 
Paid_Warranty                 STRING(20)              ! 
Date_Paid                     STRING(20)              ! 
Paid_User                     STRING(20)              ! 
                            END                       ! 
Loan_Status                 STRING(30)                ! 
Loan_Issued_Date            STRING(20)                ! 
Loan_Unit_Number            STRING(20)                ! 
Loan_User                   STRING(20)                ! 
Loan_Despatched_Group       GROUP                     ! 
Loan_Courier                  STRING(30)              ! 
Loan_Consignment_Number       STRING(30)              ! 
Loan_Despatched_User          STRING(20)              ! 
Loan_Despatch_Number          STRING(20)              ! 
                            END                       ! 
Exchange_Status             STRING(30)                ! 
Exchange_Unit_Number        STRING(20)                ! 
Exchange_Issued_Date        STRING(20)                ! 
Exchange_User               STRING(20)                ! 
Exchange_Despatched_Group   GROUP                     ! 
Exchange_Courier              STRING(30)              ! 
Exchange_Consignment_Number   STRING(30)              ! 
Exchange_Despatched_User      STRING(20)              ! 
Exchange_Despatch_Number      STRING(20)              ! 
                            END                       ! 
Despatch_Group              GROUP                     ! 
Date_Despatched               STRING(20)              ! 
Despatch_Number               STRING(20)              ! 
Despatch_User                 STRING(20)              ! 
Courier                       STRING(30)              ! 
Consignment_Number            STRING(30)              ! 
                            END                       ! 
Incoming_Group              GROUP                     ! 
Incoming_Courier              STRING(30)              ! 
Incoming_Consignment_Number   STRING(30)              ! 
Incoming_Date                 STRING(20)              ! 
                            END                       ! 
Despatched                  STRING(20)                ! 
Third_Party_Site            STRING(30)                ! 
Fault_Codes1                GROUP                     ! 
Fault_Code1                   STRING(30)              ! 
Fault_Code2                   STRING(30)              ! 
Fault_Code3                   STRING(30)              ! 
Fault_Code4                   STRING(30)              ! 
Fault_Code5                   STRING(30)              ! 
Fault_Code6                   STRING(30)              ! 
                            END                       ! 
Fault_Codes2                GROUP                     ! 
Fault_Code7                   STRING(30)              ! 
Fault_Code8                   STRING(30)              ! 
Fault_Code9                   STRING(30)              ! 
Fault_Code10                  STRING(255)             ! 
Fault_Code11                  STRING(255)             ! 
Fault_Code12                  STRING(255)             ! 
                            END                       ! 
Special_Instructions        STRING(30)                ! 
EDI_Batch_Number            STRING(20)                ! 
Fault_Description           STRING(255)               ! 
Notes                       STRING(255)               ! 
Engineers_Notes             STRING(255)               ! 
Invoice_Text                STRING(255)               ! 
Fault_Report                STRING(255)               ! 
Collection_Text             STRING(255)               ! 
Delivery_Text               STRING(255)               ! 
Exchange_Reason             STRING(255)               ! Exchange Reason
                         END
                     END                       

EXPAUDIT             FILE,DRIVER('BASIC'),PRE(expaud),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Export - Audit
Record                   RECORD,PRE()
Ref_Number                  STRING(20)                ! 
Date                        STRING(20)                ! 
Time                        STRING(20)                ! 
User                        STRING(20)                ! 
Action                      STRING(80)                ! 
Notes                       STRING(10000)             ! 
                         END
                     END                       

JOBBATCH             FILE,DRIVER('Btrieve'),PRE(jbt),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Job Batch Number
Batch_Number_Key         KEY(jbt:Batch_Number),NOCASE,PRIMARY ! By Batch Number
Record                   RECORD,PRE()
Batch_Number                REAL                      ! 
Date                        DATE                      ! 
Time                        TIME                      ! 
                         END
                     END                       

DEFEDI2              FILE,DRIVER('Btrieve'),PRE(ed2),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Defaults - Ericsson
record_number_key        KEY(ed2:record_number),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
record_number               REAL                      ! 
Country_Code                STRING(3)                 ! 
                         END
                     END                       

DEFPRINT             FILE,DRIVER('Btrieve'),PRE(dep),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Default Printer List
Printer_Name_Key         KEY(dep:Printer_Name),NOCASE,PRIMARY ! By Printer Name
Record                   RECORD,PRE()
Printer_Name                STRING(60)                ! 
Printer_Path                STRING(255)               ! 
Background                  STRING(3)                 ! 
Copies                      LONG                      ! Copies
                         END
                     END                       

DEFWEB               FILE,DRIVER('Btrieve'),PRE(dew),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! Defaults - Web
Record_Number_Key        KEY(dew:Record_Number),NOCASE,PRIMARY ! 
Account_Number_Key       KEY(dew:Account_Number),NOCASE ! By Account Number
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
Account_Number              STRING(15)                ! 
Warranty_Charge_Type        STRING(30)                ! 
Location                    STRING(30)                ! 
Transit_Type                STRING(30)                ! 
Job_Priority                STRING(30)                ! 
Unit_Type                   STRING(30)                ! 
Status_Type                 STRING(30)                ! 
HandSet                     STRING(30)                ! Handset Type
Chargeable_Charge_Type      STRING(30)                ! 
Insurance_Charge_Type       STRING(30)                ! 
AllowJobBooking             BYTE                      ! New Job Booking
AllowJobProgress            BYTE                      ! Job Progress
AllowOrderParts             BYTE                      ! Order Parts
AllowOrderProgress          BYTE                      ! Order Progress
Option5                     BYTE                      ! Option 5
Option6                     BYTE                      ! Option 6
Option7                     BYTE                      ! Option 7
Option8                     BYTE                      ! Option 8
Option9                     BYTE                      ! Option 9
Option10                    BYTE                      ! Option 10
                         END
                     END                       

EXPCITY              FILE,DRIVER('ASCII'),PRE(epc),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Export - City Link
Record                   RECORD,PRE()
Account_Number              STRING(8)                 ! 
Ref_Number                  STRING(31)                ! 
Customer_Name               STRING(31)                ! 
Contact_Name                STRING(31)                ! 
Address_Line1               STRING(31)                ! 
Address_Line2               STRING(31)                ! 
Town                        STRING(31)                ! 
County                      STRING(31)                ! 
Postcode                    STRING(9)                 ! 
City_Service                STRING(2)                 ! 
City_Instructions           STRING(31)                ! 
Pudamt                      STRING(5)                 ! 
Return_It                   STRING(2)                 ! 
Saturday                    STRING(2)                 ! 
Dog                         STRING(31)                ! 
Nol                         STRING(3)                 ! 
                         END
                     END                       

PROCCODE             FILE,DRIVER('Btrieve'),PRE(pro),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Process Code
Code_Number_Key          KEY(pro:Code_Number),NOCASE,PRIMARY ! By Code Number
Record                   RECORD,PRE()
Code_Number                 STRING(3)                 ! 
Description                 STRING(30)                ! 
Allow_Loan                  STRING(3)                 ! 
Accessory_Only              STRING(3)                 ! 
                         END
                     END                       

COLOUR               FILE,DRIVER('Btrieve'),OEM,PRE(col),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! Colour - Default List
Colour_Key               KEY(col:Colour),NOCASE,PRIMARY ! By Colour
Record                   RECORD,PRE()
Colour                      STRING(30)                ! 
                         END
                     END                       

VODAIMP              FILE,DRIVER('ASCII'),PRE(vdi),BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! 
Record                   RECORD,PRE()
Line                        STRING(255)               ! 
                         END
                     END                       

JOBSVODA             FILE,DRIVER('Btrieve'),PRE(jvf),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Vodafone Exchange Jobs
Ref_Number_Key           KEY(jvf:Ref_Number),NOCASE,PRIMARY ! By Ref Number
Ref_Pending_Key          KEY(jvf:Pending,jvf:Ref_Number),DUP,NOCASE ! By Ref Number
Order_Number_Key         KEY(jvf:Pending,jvf:Order_Number),DUP,NOCASE ! By Order Number
Process_Code_Key         KEY(jvf:Pending,jvf:Process_Code,jvf:Ref_Number),DUP,NOCASE ! By Process Code
Model_Number_Key         KEY(jvf:Pending,jvf:Model_Number,jvf:Unit_Type),DUP,NOCASE ! By Unit Type
ESN_Key                  KEY(jvf:Pending,jvf:ESN),DUP,NOCASE ! By ESN
Job_Number_Key           KEY(jvf:Pending,jvf:Job_Number),DUP,NOCASE ! By Job Number
Model_Ref_key            KEY(jvf:Pending,jvf:Model_Number,jvf:Ref_Number),DUP,NOCASE ! By Model Number
Record                   RECORD,PRE()
Import_Query_Reason         STRING(1000)              ! 
Ref_Number                  REAL                      ! 
Job_Number                  REAL                      ! 
Status                      STRING(30)                ! 
Account_Number              STRING(15)                ! 
Account_Type                STRING(30)                ! 
Order_Number                STRING(30)                ! 
Process_Code                STRING(5)                 ! 
Process_Description         STRING(30)                ! 
Job_Type                    STRING(30)                ! 
Customer_Name               STRING(60)                ! 
Contact_Number              STRING(15)                ! 
Mobile_Number               STRING(15)                ! 
Reported_Fault              STRING(2)                 ! 
Delegation                  STRING(30)                ! 
Consignment_Number          STRING(30)                ! 
Date_Dispatch               DATE                      ! 
Time_Dispatch               TIME                      ! 
Accessory_String            STRING(255)               ! 
Pending_Reason              STRING(255)               ! 
Customer_Unit_Group         GROUP                     ! 
ESN                           STRING(30)              ! 
Model_Number                  STRING(30)              ! 
Manufacturer                  STRING(30)              ! 
Unit_Type                     STRING(30)              ! 
Colour                        STRING(30)              ! 
                            END                       ! 
Exchange_Unit_Group         GROUP                     ! 
Exchange_Unit_Number          REAL                    ! 
ESN_Exchange                  STRING(30)              ! 
Model_Number_Exchange         STRING(30)              ! 
Manufacturer_Exchange         STRING(30)              ! 
Unit_Type_Exchange            STRING(30)              ! 
                            END                       ! 
Pending                     STRING(3)                 ! 
Delivery_Address_Group      GROUP                     ! 
Postcode_Delivery             STRING(10)              ! 
Address_Line1_Delivery        STRING(30)              ! 
Address_Line2_Delivery        STRING(30)              ! 
Address_Line3_Delivery        STRING(30)              ! 
                            END                       ! 
Collection_Address_Group    GROUP                     ! 
Postcode_Collection           STRING(10)              ! 
Address_Line1_Collection      STRING(30)              ! 
Address_Line2_Collection      STRING(30)              ! 
Address_Line3_Collection      STRING(30)              ! 
                            END                       ! 
DummyField                  STRING(1)                 ! For File Manager
                         END
                     END                       

XREPACT              FILE,DRIVER('BASIC'),PRE(xre),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Repair Activity
Record                   RECORD,PRE()
Field1                      STRING(40)                ! 
Field2                      STRING(40)                ! 
Field3                      STRING(40)                ! 
Field4                      STRING(40)                ! 
                         END
                     END                       

ACCESDEF             FILE,DRIVER('Btrieve'),PRE(acd),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Accessories - Default List
Accessory_Key            KEY(acd:Accessory),NOCASE,PRIMARY ! By Accessory
Record                   RECORD,PRE()
Accessory                   STRING(30)                ! 
                         END
                     END                       

STANTEXT             FILE,DRIVER('Btrieve'),PRE(stt),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Standard Document Texts
Description_Key          KEY(stt:Description),NOCASE,PRIMARY ! By Description
Record                   RECORD,PRE()
Description                 STRING(30)                ! 
TelephoneNumber             STRING(30)                ! 
FaxNumber                   STRING(30)                ! 
DummyField                  STRING(1)                 ! For File Manager
Text                        STRING(500)               ! 
                         END
                     END                       

NOTESENG             FILE,DRIVER('Btrieve'),PRE(noe),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Notes - Engineers
Notes_Key                KEY(noe:Reference,noe:Notes),NOCASE,PRIMARY ! By Notes
Record                   RECORD,PRE()
Reference                   STRING(30)                ! 
Notes                       STRING(80)                ! 
                         END
                     END                       

IAUDIT               FILE,DRIVER('Btrieve'),OEM,PRE(iau),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! 
KeyRecordNumber          KEY(iau:RecordNumber),NOCASE,PRIMARY ! 
KeyRefNumber             KEY(iau:RefNumber,-iau:Date,-iau:Time,iau:Action),DUP,NOCASE ! 
KeyAction                KEY(iau:RefNumber,iau:Action,-iau:Date,-iau:Time),DUP,NOCASE ! 
KeyUser                  KEY(iau:RefNumber,iau:User,-iau:Date,-iau:Time),DUP,NOCASE ! 
Record                   RECORD,PRE()
RecordNumber                REAL                      ! 
RefNumber                   LONG                      ! 
Date                        DATE                      ! 
Time                        TIME                      ! 
User                        STRING(3)                 ! 
Action                      STRING(80)                ! 
Notes                       STRING(255)               ! 
Dummy                       STRING(1)                 ! 
                         END
                     END                       

IINVJOB              FILE,DRIVER('Btrieve'),OEM,PRE(IIN),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! 
KeyInvNo                 KEY(IIN:InvoiceNo),NOCASE,PRIMARY ! 
KeyPostedStatus          KEY(IIN:PostedStatus),DUP,NOCASE ! 
KeySageNo                KEY(IIN:SageInvNo),DUP,NOCASE ! 
Record                   RECORD,PRE()
InvoiceNo                   LONG                      ! 
Customer                    STRING(30)                ! 
DateCreated                 DATE                      ! 
PartCosts                   DECIMAL(12,2)             ! 
LabourCosts                 DECIMAL(12,2)             ! 
TotalNet                    DECIMAL(12,2)             ! 
PartVAT                     DECIMAL(12,2)             ! 
LabourVAT                   DECIMAL(12,2)             ! 
TotalVAT                    DECIMAL(12,2)             ! 
Gross                       DECIMAL(12,2)             ! 
SageInvNo                   LONG                      ! 
PostedStatus                STRING(1)                 ! 
RaisedInSage                STRING(1)                 ! 
                         END
                     END                       

EXPLABG              FILE,DRIVER('ASCII'),PRE(epg),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Export - Label G
Record                   RECORD,PRE()
Account_Number              STRING(8)                 ! 
Ref_Number                  STRING(31)                ! 
Customer_Name               STRING(31)                ! 
Contact_Name                STRING(31)                ! 
Address_Line1               STRING(31)                ! 
Address_Line2               STRING(31)                ! 
Address_Line3               STRING(31)                ! 
Address_Line4               STRING(31)                ! 
Postcode                    STRING(5)                 ! 
City_Service                STRING(2)                 ! 
City_Instructions           STRING(31)                ! 
Pudamt                      STRING(5)                 ! 
Return_It                   STRING(2)                 ! 
Saturday                    STRING(2)                 ! 
Dog                         STRING(31)                ! 
Nol                         STRING(3)                 ! 
JobNo                       STRING(9)                 ! 
Weight                      STRING(6)                 ! 
                         END
                     END                       

INSDEF               FILE,DRIVER('Btrieve'),OEM,PRE(ind),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! Install Defaults
RecordNumberKey          KEY(ind:RecordNumber),NOCASE ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
AssStock1                   STRING(30)                ! Field 1
AssStock2                   STRING(30)                ! Field 2
AssStock3                   STRING(30)                ! Field 3
AssStock4                   STRING(30)                ! Field 4
AssStock5                   STRING(30)                ! Field 5
AssStock6                   STRING(30)                ! Field 6
AssStock7                   STRING(30)                ! Field 7
AssStock8                   STRING(30)                ! Field 8
AssStock9                   STRING(30)                ! Field 9
AssStock10                  STRING(30)                ! Field 10
AssStock11                  STRING(30)                ! 
AssStock12                  STRING(30)                ! 
AssStock13                  STRING(30)                ! 
AssStock14                  STRING(30)                ! 
AssStock15                  STRING(30)                ! 
AssStock16                  STRING(30)                ! 
SMTPServer                  STRING(80)                ! 
SMTPPort                    USHORT                    ! 
POP3Port                    USHORT                    ! 
AskIfTooBig                 BYTE                      ! 
SMTPPassword                STRING(80)                ! 
User                        STRING(80)                ! 
DeleteAfterDownload         BYTE                      ! 
MaxKBytes                   LONG                      ! 
DontSaveAttachments         BYTE                      ! 
DontSaveEmbeds              BYTE                      ! 
LocalAttachFolder           STRING(255)               ! 
POP3Server                  STRING(80)                ! 
UseDefaultLocation          BYTE                      ! Use Default Stock Location
Location                    STRING(30)                ! Location
EmailAddress                STRING(50)                ! 
DefExportPath               STRING(255)               ! 
Dummy                       STRING(1)                 ! 
                         END
                     END                       

IEXPUSE              FILE,DRIVER('Btrieve'),OEM,PRE(iex1),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! 
KeyRecNo                 KEY(iex1:RecNo),NOCASE,PRIMARY ! 
KeyName                  KEY(iex1:Name),DUP,NOCASE    ! 
KeyExpNo                 KEY(iex1:ExpNo),DUP,NOCASE   ! 
KeyBrwExpNo              KEY(iex1:RecipientNo,iex1:ExpNo),NOCASE ! 
KeyRecipientNo           KEY(iex1:RecipientNo),DUP,NOCASE ! 
KeyBrowseName            KEY(iex1:RecipientNo,iex1:Name),DUP,NOCASE ! 
Record                   RECORD,PRE()
RecNo                       LONG                      ! 
RecipientNo                 LONG                      ! 
ExpNo                       LONG                      ! 
Name                        STRING(50)                ! 
Type                        STRING(1)                 ! 
                         END
                     END                       

IRECEIVE             FILE,DRIVER('Btrieve'),OEM,PRE(ire),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! 
KeyRecipientNo           KEY(ire:RecipientNo),NOCASE,PRIMARY ! 
KeyCompany               KEY(ire:Company),DUP,NOCASE  ! 
KeyName                  KEY(ire:Name),DUP,NOCASE     ! 
Record                   RECORD,PRE()
RecipientNo                 LONG                      ! 
Name                        STRING(30)                ! 
Company                     STRING(30)                ! 
EmailAddress                STRING(50)                ! 
                         END
                     END                       

IJOBSTATUS           FILE,DRIVER('Btrieve'),OEM,PRE(ij01),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)              ! 
JobStatusNoKey           KEY(ij01:JobStatusNo),NOCASE,PRIMARY ! 
JobStatusKey             KEY(ij01:JobStatus),NOCASE   ! 
Record                   RECORD,PRE()
JobStatusNo                 LONG                      ! 
JobStatus                   STRING(30)                ! 
                         END
                     END                       

ICLCOST              FILE,DRIVER('Btrieve'),OEM,PRE(ICL1),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! 
KeyICLNo                 KEY(ICL1:ICLNo),NOCASE,PRIMARY ! 
KeyStatusNo              KEY(ICL1:StatusNo),DUP,NOCASE ! 
KeyBrwStatus             KEY(ICL1:ClientNo,ICL1:Status),NOCASE ! 
KeyBrwStatusNo           KEY(ICL1:ClientNo,ICL1:StatusNo),DUP,NOCASE ! 
KeyStatus                KEY(ICL1:Status),DUP,NOCASE  ! 
Record                   RECORD,PRE()
ICLNo                       LONG                      ! 
ClientNo                    LONG                      ! 
Status                      STRING(30)                ! 
StatusNo                    LONG                      ! 
LabCost                     DECIMAL(7,2)              ! 
Dummy                       STRING(1)                 ! 
                         END
                     END                       

IVEHDETS             FILE,DRIVER('Btrieve'),OEM,PRE(ive),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Vehicle details for the Install program
KeyVehDetNo              KEY(ive:VehDetNo),NOCASE,PRIMARY ! 
KeyJobNo                 KEY(ive:JobNumber),DUP,NOCASE ! 
BrwMakeKey               KEY(ive:JobNumber,ive:Make),DUP,NOCASE ! 
KeyRegNo                 KEY(ive:Registration),DUP,NOCASE ! 
KeyMobileNo              KEY(ive:MobileNo),DUP,NOCASE ! 
KeyBrwMobileNo           KEY(ive:JobNumber,ive:MobileNo),DUP,NOCASE ! 
MakeKey                  KEY(ive:Make),DUP,NOCASE     ! 
ModelKey                 KEY(ive:Model),DUP,NOCASE    ! 
BrwModelKey              KEY(ive:JobNumber,ive:Model),DUP,NOCASE ! 
KeyFitted                KEY(ive:Fitted),DUP,NOCASE   ! 
KeyBrwFitted             KEY(ive:JobNumber,ive:Fitted),DUP,NOCASE ! 
KeyBrwRegNo              KEY(ive:JobNumber,ive:Registration),DUP,NOCASE ! 
KeyModelNo               KEY(ive:ModelNo),DUP,NOCASE  ! 
KeyMakeNo                KEY(ive:MakeNo),DUP,NOCASE   ! 
Record                   RECORD,PRE()
VehDetNo                    LONG                      ! 
JobNumber                   LONG                      ! 
Make                        STRING(30)                ! 
Model                       STRING(30)                ! 
Registration                STRING(10)                ! 
Dummy                       STRING(7)                 ! 
Fitted                      STRING(1)                 ! 
MobileNo                    STRING(30)                ! 
FittedEngineer              STRING(30)                ! 
FittedContractor            STRING(30)                ! 
FittedContractorNo          LONG                      ! 
MakeNo                      LONG                      ! 
ModelNo                     LONG                      ! 
                         END
                     END                       

IAPPENG              FILE,DRIVER('Btrieve'),OEM,PRE(IAP1),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! 
KeyAppEngNo              KEY(IAP1:AppEngNo),NOCASE,PRIMARY ! 
KeyAppEngineer           KEY(IAP1:ApptNo,IAP1:AppEngineer),NOCASE ! 
KeyApptNo                KEY(IAP1:ApptNo),DUP,NOCASE  ! 
KeyContractEng           KEY(IAP1:ContractorNo,IAP1:AppEngineer),DUP,NOCASE ! 
Record                   RECORD,PRE()
AppEngNo                    LONG                      ! 
ApptNo                      LONG                      ! 
JobNo                       LONG                      ! 
ContractorNo                LONG                      ! 
AppEngineer                 STRING(30)                ! 
                         END
                     END                       

IAPPTYPE             FILE,DRIVER('Btrieve'),OEM,PRE(iap),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Install Appointment Types
RecordNumberKey          KEY(iap:RecordNumber),NOCASE,PRIMARY ! By Record Number
AppTypeKey               KEY(iap:AppType),NOCASE      ! By Appointment Type
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
AppType                     STRING(30)                ! Appointment Type
                         END
                     END                       

INSMAKE              FILE,DRIVER('Btrieve'),OEM,PRE(INS2),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! 
KeyMakeNo                KEY(INS2:MakeNo),NOCASE,PRIMARY ! 
KeyMake                  KEY(INS2:Make),NOCASE        ! 
Record                   RECORD,PRE()
MakeNo                      LONG                      ! 
Make                        STRING(30)                ! 
                         END
                     END                       

INSAPPS              FILE,DRIVER('Btrieve'),OEM,PRE(ins),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Install Appointment
RecordNumberKey          KEY(ins:RecordNumber),NOCASE,PRIMARY ! By Record Number
AccountDateKey           KEY(ins:AccountNumber,ins:Date),DUP,NOCASE ! By Date
DateKey                  KEY(ins:Date,ins:AccountNumber),DUP,NOCASE ! By Date
JobDateKey               KEY(ins:RefNumber,ins:Date),DUP,NOCASE ! By Date
KeyDayTime               KEY(ins:DayTime),DUP,NOCASE  ! 
KeyAppDate               KEY(ins:Date),DUP,NOCASE     ! 
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
AccountNumber               STRING(30)                ! Sub Contractor Account Number
RefNumber                   LONG                      ! Job Number
Date                        DATE                      ! Appointment Date
Time                        STRING(30)                ! Appointment Time
DayTime                     STRING(30)                ! Time
Type                        STRING(30)                ! Appointment Type
Comment                     STRING(255)               ! Comment
Printed                     BYTE                      ! Printed
DummyField                  STRING(4)                 ! For File Manager
                         END
                     END                       

ISUBCONT             FILE,DRIVER('Btrieve'),OEM,PRE(isub),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Install Sub Contractors
RecordNumberKey          KEY(isub:RecordNumber),NOCASE,PRIMARY ! By Record Number
AccountNumberKey         KEY(isub:AccountNumber),NOCASE ! By Account Number
BusinessNameKey          KEY(isub:BusinessName,isub:AccountNumber),DUP,NOCASE ! By Business Key
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
ContactName                 STRING(30)                ! Contact Name
AccountNumber               STRING(30)                ! Account Number
BusinessName                STRING(30)                ! Business Name
Postcode                    STRING(20)                ! Postcode
BuildingName                STRING(30)                ! Building Name
AddressLine1                STRING(30)                ! Address
AddressLine2                STRING(30)                ! Address Line 2
AddressLine3                STRING(30)                ! Address Line 3
BusinessNumber              STRING(20)                ! Business Number
FaxNumber                   STRING(20)                ! Fax Number
MobileNumber                STRING(20)                ! Mobile Number
OtherNumber                 STRING(20)                ! Other Number
EmailAddress                STRING(255)               ! E-Mail Address
DailyWorkLoad               LONG                      ! Daily Work Load
Rating                      LONG                      ! Rating
NumInstallers               LONG                      ! 
ExpDatePubLiabIns           DATE                      ! 
                         END
                     END                       

TRDSPEC              FILE,DRIVER('Btrieve'),PRE(tsp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Third Party Special Instructions
Short_Description_Key    KEY(tsp:Short_Description),NOCASE,PRIMARY ! By Short Description
Record                   RECORD,PRE()
Short_Description           STRING(30)                ! 
Long_Description            STRING(255)               ! 
                         END
                     END                       

ITIMES               FILE,DRIVER('Btrieve'),OEM,PRE(tim),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! 
KeyTimeNo                KEY(tim:TimeNo),NOCASE,PRIMARY ! 
KeyComputed              KEY(tim:Computed),NOCASE     ! 
Record                   RECORD,PRE()
TimeNo                      LONG                      ! 
TimeFrom                    LONG                      ! 
TimeTo                      LONG                      ! 
Computed                    STRING(30)                ! 
                         END
                     END                       

IEQUIP               FILE,DRIVER('Btrieve'),OEM,PRE(iequ),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! Install Job Equipment
RecordNumberKey          KEY(iequ:RecordNumber),NOCASE,PRIMARY ! By Record Number
JobPartNumberKey         KEY(iequ:RefNumber,iequ:PartNumber),DUP,NOCASE ! By Part Number
RefPendRefKey            KEY(iequ:RefNumber,iequ:PendingRefNumber),DUP,NOCASE ! By Pending Ref Number
ManufacturerKey          KEY(iequ:Manufacturer),DUP,NOCASE ! 
BrwManufacturer          KEY(iequ:RefNumber,iequ:Manufacturer),DUP,NOCASE ! 
KeyPartNumber            KEY(iequ:PartNumber),DUP,NOCASE ! 
KeyDescription           KEY(iequ:Description),DUP,NOCASE ! 
KeyBrwDescription        KEY(iequ:RefNumber,iequ:Description),DUP,NOCASE ! 
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
RefNumber                   LONG                      ! Job Reference Number
PartNumber                  STRING(30)                ! Part Number
Description                 STRING(30)                ! Description
PurchaseCost                REAL                      ! Purchase Cost
SaleCost                    REAL                      ! Sale Cost
Supplier                    STRING(30)                ! Supplier
Quantity                    LONG                      ! Quantity
ExcludeFromOrder            BYTE                      ! Exclude From Order
DespatchNoteNumber          STRING(30)                ! Despatch Note Number
PartRefNumber               LONG                      ! Stock Part Number
DateOrdered                 DATE                      ! Date Ordered
PendingRefNumber            LONG                      ! Pending Part Number
OrderNumber                 LONG                      ! Order Number
OrderPartNumber             LONG                      ! Order Part Number
DateReceived                DATE                      ! Date Received
Manufacturer                STRING(30)                ! 
Dummy                       STRING(3)                 ! 
                         END
                     END                       

EPSCSV               FILE,DRIVER('BASIC'),PRE(eps),BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! 
Record                   RECORD,PRE()
LABEL1                      STRING(32)                ! 
LABEL2                      STRING(32)                ! 
LABEL3                      STRING(32)                ! 
LABEL4                      STRING(32)                ! 
LABEL5                      STRING(32)                ! 
LABEL6                      STRING(32)                ! 
LABEL7                      STRING(32)                ! 
LABEL8                      STRING(32)                ! 
LABEL9                      STRING(32)                ! 
LABEL10                     STRING(32)                ! 
LABEL11                     STRING(32)                ! 
LABEL12                     STRING(32)                ! 
LABEL13                     STRING(32)                ! 
LABEL14                     STRING(32)                ! 
LABEL15                     STRING(1000)              ! 
LABEL16                     STRING(32)                ! 
                         END
                     END                       

IENGINEERS           FILE,DRIVER('Btrieve'),OEM,PRE(ien),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)              ! 
KeyEngineerNo            KEY(ien:EngineerNo),NOCASE,PRIMARY ! 
KeyEngineer              KEY(ien:Engineer),DUP,NOCASE ! 
KeyBrwEngineer           KEY(ien:ContractorNo,ien:Engineer),NOCASE ! 
KeyContractorNo          KEY(ien:ContractorNo),DUP,NOCASE ! 
Record                   RECORD,PRE()
EngineerNo                  LONG                      ! 
ContractorNo                LONG                      ! 
Engineer                    STRING(30)                ! 
DatePassedCityGuilds        DATE                      ! 
InstallPassExpiryDate       DATE                      ! 
Passed                      STRING(1)                 ! 
GotPass                     STRING(1)                 ! 
                         END
                     END                       

ICLIENTS             FILE,DRIVER('Btrieve'),OEM,PRE(icl),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! 
KeyClientNo              KEY(icl:ClientNo),NOCASE,PRIMARY ! 
KeyClientName            KEY(icl:ClientName),NOCASE   ! 
Record                   RECORD,PRE()
ClientNo                    LONG                      ! 
ClientName                  STRING(30)                ! 
LogoPath                    STRING(255)               ! 
                         END
                     END                       

ISUBPOST             FILE,DRIVER('Btrieve'),OEM,PRE(ispt),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Install Subcontractor Postcode Areas
RecordNumberKey          KEY(ispt:RecordNumber),NOCASE,PRIMARY ! By Record Number
RefPostCodeKey           KEY(ispt:RefNumber,ispt:Postcode),DUP,NOCASE ! By Postcode
PostcodeType             KEY(ispt:Postcode,ispt:Type,ispt:RefNumber),DUP,NOCASE ! By Ref Number
PostCodeRefKey           KEY(ispt:Postcode,ispt:RefNumber),DUP,NOCASE ! 
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
RefNumber                   LONG                      ! Ref Number
Postcode                    STRING(4)                 ! Postcode
Type                        BYTE                      ! Postcode Type
DummyField                  STRING(2)                 ! For File Manager
                         END
                     END                       

IJOBTYPE             FILE,DRIVER('Btrieve'),OEM,PRE(ijo),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Job types for the Install program
JobTypeNoKey             KEY(ijo:JobTypeNo),NOCASE,PRIMARY ! 
JobTypeKey               KEY(ijo:JobType),NOCASE      ! 
Record                   RECORD,PRE()
JobTypeNo                   LONG                      ! 
JobType                     STRING(30)                ! 
LabourCost                  DECIMAL(7,2)              ! 
DummyField                  STRING(1)                 ! 
                         END
                     END                       

EXPGENDM             FILE,DRIVER('BASIC'),OEM,PRE(exp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Export Generic With Dimension
Record                   RECORD,PRE()
line1                       STRING(60),DIM(50)        ! 
                         END
                     END                       

EDIBATCH             FILE,DRIVER('Btrieve'),PRE(ebt),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! EDI Batch Number
Batch_Number_Key         KEY(ebt:Manufacturer,ebt:Batch_Number),NOCASE ! By Batch Number
Record                   RECORD,PRE()
Batch_Number                REAL                      ! 
Manufacturer                STRING(30)                ! 
Date                        DATE                      ! 
Time                        TIME                      ! 
                         END
                     END                       

ORDPEND              FILE,DRIVER('Btrieve'),PRE(ope),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Orders - Pending Parts
Ref_Number_Key           KEY(ope:Ref_Number),NOCASE,PRIMARY ! By Ref Number
Supplier_Key             KEY(ope:Supplier,ope:Part_Number),DUP,NOCASE ! By Supplier
DescriptionKey           KEY(ope:Supplier,ope:Description),DUP,NOCASE ! By Description
Supplier_Name_Key        KEY(ope:Supplier),DUP,NOCASE ! By Supplier
Part_Ref_Number_Key      KEY(ope:Part_Ref_Number),DUP,NOCASE ! By Stock Ref Number
Awaiting_Supplier_Key    KEY(ope:Awaiting_Stock,ope:Supplier,ope:Part_Number),DUP,NOCASE ! By Part Number
PartRecordNumberKey      KEY(ope:PartRecordNumber),DUP,NOCASE ! By Record Number
Job_Number_Key           KEY(ope:Job_Number),DUP,NOCASE ! By Job Number
Supplier_Job_Key         KEY(ope:Supplier,ope:Job_Number),DUP,NOCASE ! 
Record                   RECORD,PRE()
Ref_Number                  LONG                      ! 
Part_Ref_Number             LONG                      ! 
Job_Number                  LONG                      ! 
Part_Type                   STRING(3)                 ! 
Supplier                    STRING(30)                ! 
Part_Number                 STRING(30)                ! 
Description                 STRING(30)                ! 
Quantity                    LONG                      ! 
Account_Number              STRING(15)                ! 
Awaiting_Stock              STRING(3)                 ! 
Reason                      STRING(255)               ! Reason
PartRecordNumber            LONG                      ! Part Record Number
                         END
                     END                       

STOHIST              FILE,DRIVER('Btrieve'),PRE(shi),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Stock History
Ref_Number_Key           KEY(shi:Ref_Number,shi:Date),DUP,NOCASE ! By Ref_Number
record_number_key        KEY(shi:Record_Number),NOCASE,PRIMARY ! 
Transaction_Type_Key     KEY(shi:Ref_Number,shi:Transaction_Type,shi:Date),DUP,NOCASE ! By Transaction
DateKey                  KEY(shi:Date),DUP,NOCASE     ! By Date
Record                   RECORD,PRE()
Record_Number               LONG                      ! Record Number
Ref_Number                  LONG                      ! 
User                        STRING(3)                 ! 
Transaction_Type            STRING(3)                 ! 
Despatch_Note_Number        STRING(30)                ! 
Job_Number                  LONG                      ! 
Sales_Number                LONG                      ! 
Quantity                    REAL                      ! 
Date                        DATE                      ! 
Purchase_Cost               REAL                      ! 
Sale_Cost                   REAL                      ! 
Retail_Cost                 REAL                      ! 
Notes                       STRING(255)               ! 
Information                 STRING(255)               ! 
                         END
                     END                       

NEWFEAT              FILE,DRIVER('Btrieve'),PRE(fea),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! New Features
Record_Number_Key        KEY(fea:Record_Number),NOCASE,PRIMARY ! By Record Number
date_key                 KEY(-fea:date,fea:description),DUP,NOCASE ! By Date
description_Key          KEY(fea:description,fea:date),DUP,NOCASE ! By Description
DateTypeKey              KEY(fea:ReportType,-fea:date,fea:description),DUP,NOCASE ! By Date
DescriptionTypeKey       KEY(fea:ReportType,fea:description),DUP,NOCASE ! By Description
DescriptionOnlyKey       KEY(fea:description),DUP,NOCASE ! By Description
RefNoKey                 KEY(fea:RefNumber),DUP,NOCASE ! By Ref No
RefNoTypeKey             KEY(fea:ReportType,fea:RefNumber),DUP,NOCASE ! By Ref No
Date_Key_Ascending       KEY(fea:date),DUP,NOCASE     ! 
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
date                        DATE                      ! 
description                 STRING(60)                ! 
Text                        STRING(1000)              ! 
ReportType                  BYTE                      ! Report Type
RefNumber                   STRING(30)                ! Reference Number
DocumentPath                STRING(255)               ! Document Path
                         END
                     END                       

EXREASON             FILE,DRIVER('Btrieve'),OEM,PRE(exr),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Exchange / Loan Reason
RecordNumberKey          KEY(exr:RecordNumber),NOCASE,PRIMARY ! By Record Number
ReasonKey                KEY(exr:ReasonType,exr:Reason),DUP,NOCASE ! By Reason
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
ReasonType                  BYTE                      ! ReasonType
Reason                      STRING(80)                ! Reason
                         END
                     END                       

REPTYDEF             FILE,DRIVER('Btrieve'),PRE(rtd),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Repair Types Default List
Repair_Type_Key          KEY(rtd:Repair_Type),NOCASE,PRIMARY ! By Repair Type
Chargeable_Key           KEY(rtd:Chargeable,rtd:Repair_Type),DUP,NOCASE ! 
Warranty_Key             KEY(rtd:Warranty,rtd:Repair_Type),DUP,NOCASE ! 
Record                   RECORD,PRE()
Repair_Type                 STRING(30)                ! 
Chargeable                  STRING(3)                 ! 
Warranty                    STRING(3)                 ! 
WarrantyCode                STRING(5)                 ! Code For Warranty (Sagem)
CompFaultCoding             BYTE                      ! Compulsory Fault Coding
ExcludeFromEDI              BYTE                      ! Exclude From EDI
ExcludeFromInvoicing        BYTE                      ! Exclude From Invoicing
BER                         BYTE                      ! B.E.R.
                         END
                     END                       

EPSIMP               FILE,DRIVER('Btrieve'),PRE(epi),BINDABLE,CREATE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! Siemens Import Pre File
Record_Number_Key        KEY(epi:Record_Number),NOCASE,PRIMARY ! By Record_Number
Fault_Description           MEMO(1000)                ! 
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
Model_Number                STRING(30)                ! 
Title                       STRING(4)                 ! 
Initial                     STRING(1)                 ! 
Surname                     STRING(30)                ! 
Telephone_Collection        STRING(15)                ! 
ESN                         STRING(30)                ! 
Fault_Code1                 STRING(30)                ! 
Company_Name_Collection     STRING(30)                ! 
Address_Line1_Collection    STRING(30)                ! 
Address_Line2_Collection    STRING(30)                ! 
Address_Line3_Collection    STRING(30)                ! 
Postcode_Collection         STRING(10)                ! 
Order_Number                STRING(30)                ! 
Passed                      STRING(3)                 ! 
                         END
                     END                       

RETACCOUNTSLIST      FILE,DRIVER('Btrieve'),OEM,PRE(retacc),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)         ! Retail Orders Receiving Accounts List
RecordNumberKey          KEY(retacc:RecordNumber),NOCASE,PRIMARY ! 
AccountNumberKey         KEY(retacc:RefNumber,retacc:AccountNumber),DUP,NOCASE ! By Account Number
DateOrderedKey           KEY(retacc:RefNumber,retacc:DateOrdered,retacc:AccountNumber),DUP,NOCASE ! By Back Order Date
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
RefNumber                   LONG                      ! Reference To Part Entry
AccountNumber               STRING(30)                ! Account Number
CompanyName                 STRING(30)                ! Company Name
DateOrdered                 DATE                      ! Date Ordered
QuantityOrdered             LONG                      ! Quantity Ordered
QuantityToShip              LONG                      ! Quantity To Ship
SaleNumber                  LONG                      ! Sale Number
OrigRecordNumber            LONG                      ! Original Record Number
SaleType                    STRING(4)                 ! 
                         END
                     END                       

PRIORITY             FILE,DRIVER('Btrieve'),PRE(pri),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Priority
Priority_Type_Key        KEY(pri:Priority_Type),NOCASE,PRIMARY ! By Priority Type
Record                   RECORD,PRE()
Priority_Type               STRING(30)                ! 
Time                        REAL                      ! 
Book_Before                 TIME                      ! 
                         END
                     END                       

DEFEPS               FILE,DRIVER('Btrieve'),PRE(dee),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! Defaults - Siemens
Record_Number_Key        KEY(dee:Record_Number),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
Account_Number              STRING(15)                ! 
Warranty_Charge_Type        STRING(30)                ! 
Location                    STRING(30)                ! 
Transit_Type                STRING(30)                ! 
Job_Priority                STRING(30)                ! 
Unit_Type                   STRING(30)                ! 
Status_Type                 STRING(30)                ! 
Delivery_Text               STRING(1000)              ! 
                         END
                     END                       

JOBSE                FILE,DRIVER('Btrieve'),OEM,PRE(jobe),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                   ! Jobs Extension
RecordNumberKey          KEY(jobe:RecordNumber),NOCASE,PRIMARY ! By Record Number
RefNumberKey             KEY(jobe:RefNumber),DUP,NOCASE ! By Ref Number
InWorkshopDateKey        KEY(jobe:InWorkshopDate),DUP,NOCASE ! 
CompleteRepairKey        KEY(jobe:CompleteRepairDate,jobe:CompleteRepairTime),DUP,NOCASE ! 
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
RefNumber                   LONG                      ! Refence To Job Number
JobMark                     BYTE                      ! Job Mark
TraFaultCode1               STRING(30)                ! Fault Code 1
TraFaultCode2               STRING(30)                ! Fault Code 2
TraFaultCode3               STRING(30)                ! Fault Code 3
TraFaultCode4               STRING(30)                ! Fault Code 4
TraFaultCode5               STRING(30)                ! Fault Code 5
TraFaultCode6               STRING(30)                ! Fault Code 6
TraFaultCode7               STRING(30)                ! Fault Code 7
TraFaultCode8               STRING(30)                ! Fault Code 8
TraFaultCode9               STRING(30)                ! Fault Code 9
TraFaultCode10              STRING(30)                ! Fault Code 10
TraFaultCode11              STRING(30)                ! Fault Code 11
TraFaultCode12              STRING(30)                ! Fault Code 12
SIMNumber                   STRING(30)                ! SIM Number
JobReceived                 BYTE                      ! Job Received
SkillLevel                  LONG                      ! Skill Level
UPSFlagCode                 STRING(1)                 ! UPS Flag Code
FailedDelivery              BYTE                      ! Failed Delivery
CConfirmSecondEntry         STRING(3)                 ! Chargeable Confirm Second Entry
WConfirmSecondEntry         STRING(3)                 ! Warranty Confirm Second Entry
EndUserEmailAddress         STRING(255)               ! Email Address
Network                     STRING(30)                ! Network
ExchangeReason              STRING(255)               ! Exchange Reason
LoanReason                  STRING(255)               ! Loan Reason
InWorkshopDate              DATE                      ! Date
InWorkshopTime              TIME                      ! In Workshop Time
Pre_RF_Board_IMEI           STRING(20)                ! 
CompleteRepairType          BYTE                      ! 
CompleteRepairDate          DATE                      ! 
CompleteRepairTime          TIME                      ! 
CustomerCollectionDate      DATE                      ! Customer Collection Date
CustomerCollectionTime      TIME                      ! Customer Collection Time
EstimatedDespatchDate       DATE                      ! Estimated Despatch Date
EstimatedDespatchTime       TIME                      ! Estimated Despatch Time
FaultCode13                 STRING(30)                ! Fault Code 13
FaultCode14                 STRING(30)                ! Fault Code 14
FaultCode15                 STRING(30)                ! Fault Code 15
FaultCode16                 STRING(30)                ! Fault Code 16
FaultCode17                 STRING(30)                ! Fault Code 17
FaultCode18                 STRING(30)                ! Fault Code 18
FaultCode19                 STRING(30)                ! Fault Code 19
FaultCode20                 STRING(30)                ! Fault Code 20
                         END
                     END                       

REPEXTTP             FILE,DRIVER('Btrieve'),OEM,PRE(rpt),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! External Report Types
RecordNumberKey          KEY(rpt:RecordNumber),NOCASE,PRIMARY ! By Record Number
ReportTypeKey            KEY(rpt:ReportType),NOCASE   ! By Report Type
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
ReportType                  STRING(80)                ! Report Type
                         END
                     END                       

MANFAULT             FILE,DRIVER('Btrieve'),PRE(maf),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Manufacturer Fault Coding On Jobs
Field_Number_Key         KEY(maf:Manufacturer,maf:Field_Number),NOCASE,PRIMARY ! By Field Number
Record                   RECORD,PRE()
Manufacturer                STRING(30)                ! 
Field_Number                REAL                      ! Fault Code Field Number
Field_Name                  STRING(30)                ! 
Field_Type                  STRING(30)                ! 
Lookup                      STRING(3)                 ! 
Force_Lookup                STRING(3)                 ! 
Compulsory                  STRING(3)                 ! 
Compulsory_At_Booking       STRING(3)                 ! 
ReplicateFault              STRING(3)                 ! Replicate To Fault Description Field
ReplicateInvoice            STRING(3)                 ! Replicate To Fault Description Field
RestrictLength              BYTE                      ! Restrict Length
LengthFrom                  LONG                      ! Length From
LengthTo                    LONG                      ! Length To
ForceFormat                 BYTE                      ! Force Format
FieldFormat                 STRING(30)                ! Field Format
DateType                    STRING(30)                ! Date Type
                         END
                     END                       

INVPARTS             FILE,DRIVER('Btrieve'),OEM,PRE(ivp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Invoice (Credit Note Parts)
RecordNoKey              KEY(ivp:RecordNumber),NOCASE,PRIMARY ! By Record Number
InvoiceNoKey             KEY(ivp:InvoiceNumber,ivp:PartNumber),DUP,NOCASE ! By Invoice Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! AutoNumber Field
InvoiceNumber               LONG                      ! Link To Invoice
RetstockNumber              LONG                      ! Link to the Retail Item's Record Number
CreditQuantity              LONG                      ! Quantity Credited
PartNumber                  STRING(30)                ! Retail Stock Part Number
Description                 STRING(30)                ! Retail Stock Description
Supplier                    STRING(30)                ! Retail Stock Supplier
PurchaseCost                REAL                      ! Retail Stock Purchase Cost
SaleCost                    REAL                      ! Retail Stock Sale Cost
RetailCost                  REAL                      ! Retail Stock Retail Cost
JobNumber                   LONG                      ! Original Job Number (For Multiple Invoices)
                         END
                     END                       

JOBSEARC             FILE,DRIVER('Btrieve'),OEM,PRE(jobser),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Temporary Job Search
RecordNumberKey          KEY(jobser:RecordNumber),NOCASE,PRIMARY ! By Record Number
RefNumberKey             KEY(jobser:RefNumber),DUP,NOCASE ! By Job Number
ConsignmentNoKey         KEY(jobser:ConsignmentNumber,jobser:RefNumber),DUP,NOCASE ! By Consignment Number
AccountNoKey             KEY(jobser:AccountNumber),DUP,NOCASE ! By Account Number
CourierKey               KEY(jobser:Courier),DUP,NOCASE ! By Courier
IMEIKey                  KEY(jobser:IMEI),DUP,NOCASE  ! By I.M.E.I. Number
JobTypeKey               KEY(jobser:JobType,jobser:RefNumber),DUP,NOCASE ! By Job Type
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
RefNumber                   LONG                      ! Job Number
ConsignmentNumber           STRING(30)                ! Consignment Number
JobType                     STRING(30)                ! Job Type
IMEI                        STRING(30)                ! I.M.E.I. Number
Courier                     STRING(30)                ! Courier
AccountNumber               STRING(30)                ! Account Number
                         END
                     END                       

INVPATMP             FILE,DRIVER('Btrieve'),OEM,PRE(ivptmp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Invoice (Credit Note Parts) - Temporary
RecordNoKey              KEY(ivptmp:RecordNumber),NOCASE,PRIMARY ! By Record Number
InvoiceNoKey             KEY(ivptmp:InvoiceNumber,ivptmp:PartNumber),DUP,NOCASE ! By Invoice Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! AutoNumber Field
InvoiceNumber               LONG                      ! Link To Invoice
RetstockNumber              LONG                      ! Link to the Retail Item's Record Number
CreditQuantity              LONG                      ! Quantity Credited
PartNumber                  STRING(30)                ! Retail Stock Part Number
Description                 STRING(30)                ! Retail Stock Description
Supplier                    STRING(30)                ! Retail Stock Supplier
PurchaseCost                REAL                      ! Retail Stock Purchase Cost
SaleCost                    REAL                      ! Retail Stock Sale Cost
RetailCost                  REAL                      ! Retail Stock Retail Cost
JobNumber                   LONG                      ! Original Job Number (For Multiple Invoices)
                         END
                     END                       

TRACHAR              FILE,DRIVER('Btrieve'),PRE(tch),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Trade Charge Types For Priced Despatch
Account_Number_Key       KEY(tch:Account_Number,tch:Charge_Type),NOCASE,PRIMARY ! By Charge Type
Record                   RECORD,PRE()
Account_Number              STRING(15)                ! 
Charge_Type                 STRING(30)                ! 
                         END
                     END                       

CITYSERV             FILE,DRIVER('Btrieve'),OEM,PRE(cit),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! City Link Services
RefNumberKey             KEY(cit:RefNumber),NOCASE,PRIMARY ! By Ref Number
ServiceKey               KEY(cit:Service),NOCASE      ! By Service
Record                   RECORD,PRE()
RefNumber                   LONG                      ! 
Service                     STRING(1)                 ! 
Description                 STRING(30)                ! 
                         END
                     END                       

PICKNOTE             FILE,DRIVER('Btrieve'),OEM,PRE(pnt),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! 
keypicknote              KEY(pnt:PickNoteRef),NOCASE,PRIMARY ! 
keyonlocation            KEY(pnt:Location),DUP,NOCASE ! 
keyonjobno               KEY(pnt:JobReference),DUP,NOCASE ! 
Record                   RECORD,PRE()
PickNoteRef                 LONG                      ! 
JobReference                LONG                      ! 
PickNoteNumber              LONG                      ! 
EngineerCode                STRING(3)                 ! 
Location                    STRING(30)                ! 
IsPrinted                   BYTE                      ! 
PrintedBy                   STRING(3)                 ! 
PrintedDate                 DATE                      ! 
PrintedTime                 TIME                      ! 
IsConfirmed                 BYTE                      ! 
ConfirmedBy                 STRING(3)                 ! 
ConfirmedDate               DATE                      ! 
ConfirmedTime               TIME                      ! 
ConfirmedNotes              STRING(255)               ! 
Usercode                    STRING(3)                 ! 
                         END
                     END                       

DISCOUNT             FILE,DRIVER('Btrieve'),PRE(dis),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Discount Code
Discount_Code_Key        KEY(dis:Discount_Code),NOCASE,PRIMARY ! By Discount Code
Record                   RECORD,PRE()
Discount_Code               STRING(2)                 ! 
Discount_Rate               REAL                      ! 
                         END
                     END                       

LOCVALUE             FILE,DRIVER('Btrieve'),OEM,PRE(lov),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Location, End Of Period Values
RecordNumberKey          KEY(lov:RecordNumber),NOCASE,PRIMARY ! By Record Number
DateKey                  KEY(lov:Location,lov:TheDate),DUP,NOCASE ! By Date
DateOnly                 KEY(lov:TheDate),DUP,NOCASE,OPT ! 
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
Location                    STRING(30)                ! Location
TheDate                     DATE                      ! The Date
PurchaseTotal               REAL                      ! Purchase Cost Total
SaleCostTotal               REAL                      ! SaleCostTotal
RetailCostTotal             REAL                      ! RetailCostTotal
QuantityTotal               LONG                      ! QuantityTotal
                         END
                     END                       

STOCKTYP             FILE,DRIVER('Btrieve'),PRE(stp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Stock Type For Loan/Exhange Units
Stock_Type_Key           KEY(stp:Stock_Type),NOCASE,PRIMARY ! By Stock_Type
Use_Loan_Key             KEY(stp:Use_Loan,stp:Stock_Type),DUP,NOCASE ! By Stock Type
Use_Exchange_Key         KEY(stp:Use_Exchange,stp:Stock_Type),DUP,NOCASE ! By Stock Type
Record                   RECORD,PRE()
Use_Loan                    STRING(3)                 ! 
Use_Exchange                STRING(3)                 ! 
Stock_Type                  STRING(30)                ! 
Available                   BYTE                      ! Available
                         END
                     END                       

IEMAILS              FILE,DRIVER('Btrieve'),OEM,PRE(iem),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Form emails for the Install program
KeyEmailNo               KEY(iem:EmailNo),NOCASE,PRIMARY ! 
KeyDescription           KEY(iem:Description),DUP,NOCASE ! 
KeySubject               KEY(iem:Subject),DUP,NOCASE  ! 
KeyStatus                KEY(iem:Status),DUP,NOCASE   ! 
Record                   RECORD,PRE()
EmailNo                     LONG                      ! 
Description                 STRING(255)               ! 
Subject                     STRING(255)               ! 
Status                      STRING(30)                ! 
BodyText                    STRING(10000)             ! 
ReturnAddress               STRING(50)                ! 
Dummy                       STRING(2)                 ! 
                         END
                     END                       

COURIER              FILE,DRIVER('Btrieve'),PRE(cou),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Couriers
Courier_Key              KEY(cou:Courier),NOCASE,PRIMARY ! By Courier
Courier_Type_Key         KEY(cou:Courier_Type,cou:Courier),DUP,NOCASE ! By Courier_Type
Record                   RECORD,PRE()
Courier                     STRING(30)                ! 
Account_Number              STRING(15)                ! 
Postcode                    STRING(10)                ! 
Address_Line1               STRING(30)                ! 
Address_Line2               STRING(30)                ! 
Address_Line3               STRING(30)                ! 
Telephone_Number            STRING(15)                ! 
Fax_Number                  STRING(15)                ! 
Contact_Name                STRING(60)                ! 
Export_Path                 STRING(255)               ! 
Include_Estimate            STRING(3)                 ! 
Include_Chargeable          STRING(3)                 ! 
Include_Warranty            STRING(3)                 ! 
Service                     STRING(15)                ! 
Import_Path                 STRING(255)               ! 
Courier_Type                STRING(30)                ! 
Last_Despatch_Time          TIME                      ! 
ICOLSuffix                  STRING(3)                 ! ANC ICOL File Suffix
ContractNumber              STRING(20)                ! ANC Contract Number
ANCPath                     STRING(255)               ! Path to ANCPAPER.EXE  (I.e. s:\apps\service)
ANCCount                    LONG                      ! Count Of Each ANC Export
DespatchClose               STRING(3)                 ! Despatch At Closing
LabGOptions                 STRING(60)                ! Label G Command Line Options
CustomerCollection          BYTE                      ! Customer Collection
NewStatus                   STRING(30)                ! On Despatch Change Status To
NoOfDespatchNotes           LONG                      ! No Of Despatch Notes
AutoConsignmentNo           BYTE                      ! Auto Allocate Consignment Number
LastConsignmentNo           LONG                      ! Last Allocated Consignment Number
PrintLabel                  BYTE                      ! Print Label
                         END
                     END                       

TRDPARTY             FILE,DRIVER('Btrieve'),PRE(trd),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Third Party Repairer
Company_Name_Key         KEY(trd:Company_Name),NOCASE,PRIMARY ! By Company Name
Account_Number_Key       KEY(trd:Account_Number),NOCASE ! By Account_Number
Special_Instructions_Key KEY(trd:Special_Instructions),DUP,NOCASE ! By Special Instructions
Courier_Only_Key         KEY(trd:Courier),DUP,NOCASE  ! 
Record                   RECORD,PRE()
Company_Name                STRING(30)                ! 
Account_Number              STRING(30)                ! 
Contact_Name                STRING(60)                ! 
Address_Group               GROUP                     ! 
Postcode                      STRING(15)              ! 
Address_Line1                 STRING(30)              ! 
Address_Line2                 STRING(30)              ! 
Address_Line3                 STRING(30)              ! 
Telephone_Number              STRING(15)              ! 
Fax_Number                    STRING(15)              ! 
                            END                       ! 
Turnaround_Time             REAL                      ! 
Courier_Direct              STRING(3)                 ! 
Courier                     STRING(30)                ! 
Print_Order                 STRING(15)                ! 
Special_Instructions        STRING(30)                ! 
Batch_Number                LONG                      ! 
BatchLimit                  LONG                      ! 
                         END
                     END                       

JOBSTMP              FILE,DRIVER('Btrieve'),OEM,PRE(jobpre),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Pre-Insert File
RefNumberKey             KEY(jobpre:RefNumber),NOCASE,PRIMARY ! By Job Number
Record                   RECORD,PRE()
RefNumber                   LONG                      ! 
TransitType                 STRING(30)                ! 
ESN                         STRING(30)                ! 
MSN                         STRING(30)                ! 
ModelNumber                 STRING(30)                ! 
Manufacturer                STRING(30)                ! 
UnitType                    STRING(30)                ! 
Colour                      STRING(30)                ! 
DOP                         DATE                      ! 
MobileNumber                STRING(30)                ! 
Location                    STRING(30)                ! 
AccountNumber               STRING(30)                ! 
OrderNumber                 STRING(30)                ! 
ChargeableJob               STRING(3)                 ! 
WarrantyJob                 STRING(3)                 ! 
CChargeType                 STRING(30)                ! 
WChargeType                 STRING(30)                ! Warranty Charge Type
Intermittent                STRING(3)                 ! 
InCourier                   STRING(30)                ! 
InConsignNo                 STRING(30)                ! Incoming Consignment Number
InDate                      DATE                      ! 
Title                       STRING(5)                 ! 
Initial                     STRING(1)                 ! 
Surname                     STRING(30)                ! 
CompanyName                 STRING(30)                ! 
Postcode                    STRING(15)                ! 
Address1                    STRING(30)                ! 
Address2                    STRING(30)                ! 
Address3                    STRING(30)                ! 
Telephone                   STRING(30)                ! 
Fax                         STRING(30)                ! 
CCompanyName                STRING(30)                ! Collection
CPostcode                   STRING(15)                ! Collection
CAddress1                   STRING(30)                ! Collection
CAddress2                   STRING(30)                ! Collection
CAddress3                   STRING(30)                ! Collection
CTelephone                  STRING(30)                ! Collection
DCompanyName                STRING(30)                ! Delivery
DPostcode                   STRING(15)                ! Delivery
DAddress1                   STRING(30)                ! Delivery
DAddress2                   STRING(30)                ! Delivery
DAddress3                   STRING(30)                ! Delivery
DTelephone                  STRING(30)                ! Delivery
FaultCode1                  STRING(30)                ! 
FaultCode2                  STRING(30)                ! 
FaultCode3                  STRING(30)                ! 
FaultCode4                  STRING(30)                ! 
FaultCode5                  STRING(30)                ! 
FaultCode6                  STRING(30)                ! 
FaultCode7                  STRING(30)                ! 
FaultCode8                  STRING(30)                ! 
FaultCode9                  STRING(30)                ! 
FaultCode10                 STRING(255)               ! 
FaultCode11                 STRING(255)               ! 
FaultCode12                 STRING(255)               ! 
FaultText                   STRING(255)               ! 
EngineerText                STRING(255)               ! 
                         END
                     END                       

LOGALLOC             FILE,DRIVER('Btrieve'),OEM,PRE(LOG2),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Logistics Allocated Stock Header
AutoNumber_Key           KEY(LOG2:RefNumber),NOCASE,PRIMARY ! 
Browse_Key               KEY(LOG2:Done_Flag,LOG2:RefNumber),DUP,NOCASE ! 
Record                   RECORD,PRE()
RefNumber                   LONG                      ! 
Date                        DATE                      ! 
UserCode                    STRING(3)                 ! 
ModelNumber                 STRING(30)                ! 
Location                    STRING(30)                ! 
SMPFNumber                  STRING(30)                ! 
DespatchNo                  LONG                      ! 
SalesCode                   STRING(30)                ! Sales Code
Description                 STRING(30)                ! 
From                        STRING(30)                ! 
To_Site                     STRING(30)                ! 
Qty                         REAL                      ! 
Done_Flag                   BYTE                      ! 
                         END
                     END                       

JOBNOTES             FILE,DRIVER('Btrieve'),OEM,PRE(jbn),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Notes Attached To Jobs
RefNumberKey             KEY(jbn:RefNumber),NOCASE,PRIMARY ! By Job Number
Record                   RECORD,PRE()
RefNumber                   LONG                      ! Related Job Number
Fault_Description           STRING(255)               ! 
Engineers_Notes             STRING(255)               ! 
Invoice_Text                STRING(255)               ! 
Collection_Text             STRING(255)               ! 
Delivery_Text               STRING(255)               ! 
ColContatName               STRING(30)                ! Contact Name
ColDepartment               STRING(30)                ! Department
DelContactName              STRING(30)                ! Contact Name
DelDepartment               STRING(30)                ! Department
                         END
                     END                       

PROINV               FILE,DRIVER('Btrieve'),OEM,PRE(prv),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! Proforma Invoice (Batch) File
RefNumberKey             KEY(prv:RefNumber),NOCASE,PRIMARY ! By Ref Number
AccountKey               KEY(prv:Invoiced,prv:AccountNumber,prv:BatchNumber),DUP,NOCASE ! By Account Number
AccStatusKey             KEY(prv:Status,prv:AccountNumber,prv:BatchNumber),DUP,NOCASE ! By Account Number
Record                   RECORD,PRE()
RefNumber                   LONG                      ! 
AccountNumber               STRING(15)                ! Trade Account Number
AccountName                 STRING(30)                ! 
AccountType                 STRING(3)                 ! Main or Sub Account (MAI/SUB)
BatchNumber                 LONG                      ! 
DateCreated                 DATE                      ! Date Batch Created
User                        STRING(3)                 ! 
BatchValue                  REAL                      ! 
NoOfJobs                    LONG                      ! Total Number Of Jobs In The Batch
Status                      STRING(3)                 ! UnRead or Read Batch (UNR / REA)
Invoiced                    STRING(3)                 ! 
                         END
                     END                       

EXPBUS               FILE,DRIVER('ASCII'),PRE(exb),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! Export - Business Post
Record                   RECORD,PRE()
Job_Date                    STRING(9)                 ! 
Customer_Name               STRING(31)                ! 
Address_Line1               STRING(31)                ! 
Address_Line2               STRING(31)                ! 
Town                        STRING(31)                ! 
Postcode                    STRING(31)                ! 
Contact_Name                STRING(31)                ! 
Telephone_Number            STRING(21)                ! 
Business_Service            STRING(5)                 ! 
Ref_Number                  STRING(21)                ! 
Alt_Ref                     STRING(21)                ! 
Items                       STRING(11)                ! 
Weight                      STRING(11)                ! 
Spec_1                      STRING(51)                ! 
Spec_2                      STRING(51)                ! 
Comp_No                     STRING(2)                 ! 
                         END
                     END                       

IMPCITY              FILE,DRIVER('BASIC'),PRE(imc),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Import - City Link
Record                   RECORD,PRE()
ref_number                  STRING(30)                ! 
consignment_number          STRING(30)                ! 
date                        STRING(20)                ! 
                         END
                     END                       

JOBACTMP             FILE,DRIVER('Btrieve'),PRE(jactmp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Temporary Job Accesories
Record_Number_Key        KEY(jactmp:Record_Number),NOCASE,PRIMARY ! By Accessory
Accessory_Key            KEY(jactmp:Accessory),NOCASE ! By Accessory
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
Accessory                   STRING(30)                ! 
                         END
                     END                       

RETPARTSLIST         FILE,DRIVER('Btrieve'),OEM,PRE(retpar),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)            ! Retail Orders Receiving Parts List
RecordNumberKey          KEY(retpar:RecordNumber),NOCASE,PRIMARY ! By Record Number
PartNumberKey            KEY(retpar:Processed,retpar:PartNumber),DUP,NOCASE ! By Part Number
LocPartNumberKey         KEY(retpar:Location,retpar:Processed,retpar:PartNumber),DUP,NOCASE ! By Part Number
LocDescriptionKey        KEY(retpar:Location,retpar:Processed,retpar:Description),DUP,NOCASE ! By Description
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
PartRefNumber               LONG                      ! Reference To Stock
PartNumber                  STRING(30)                ! Part Number
Description                 STRING(30)                ! Description
Location                    STRING(30)                ! Location
QuantityStock               LONG                      ! Quantity In Stock
QuantityBackOrder           LONG                      ! Quantity On Back Order
QuantityOnOrder             LONG                      ! Quantity On Order
Processed                   BYTE                      ! Processed
                         END
                     END                       

TRANTYPE             FILE,DRIVER('Btrieve'),PRE(trt),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Transit Types
Transit_Type_Key         KEY(trt:Transit_Type),NOCASE,PRIMARY ! By Transit_Type
Record                   RECORD,PRE()
Transit_Type                STRING(30)                ! 
Courier_Collection_Report   STRING(3)                 ! 
Collection_Address          STRING(3)                 ! 
Delivery_Address            STRING(3)                 ! 
Loan_Unit                   STRING(3)                 ! 
Exchange_Unit               STRING(3)                 ! 
Force_DOP                   STRING(3)                 ! 
Force_Location              STRING(3)                 ! 
Skip_Workshop               STRING(3)                 ! 
HideLocation                BYTE                      ! Hide Location
InternalLocation            STRING(30)                ! Location
Workshop_Label              STRING(3)                 ! 
Job_Card                    STRING(3)                 ! 
JobReceipt                  STRING(3)                 ! Print Job Receipt
Initial_Status              STRING(30)                ! 
Initial_Priority            STRING(30)                ! 
ExchangeStatus              STRING(30)                ! 
LoanStatus                  STRING(30)                ! 
Location                    STRING(3)                 ! 
ANCCollNote                 STRING(3)                 ! Print ANC Collection Note
InWorkshop                  STRING(3)                 ! 
DummyField                  STRING(1)                 ! For File Manager
                         END
                     END                       

DESBATCH             FILE,DRIVER('Btrieve'),PRE(dbt),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Despatch Job Number
Batch_Number_Key         KEY(dbt:Batch_Number),NOCASE,PRIMARY ! By Batch Number
Record                   RECORD,PRE()
Batch_Number                REAL                      ! 
Date                        DATE                      ! 
Time                        TIME                      ! 
                         END
                     END                       

JOBVODAC             FILE,DRIVER('Btrieve'),PRE(jva),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Job Vodafone Accessories
Ref_Number_Key           KEY(jva:Ref_Number,jva:Accessory),NOCASE,PRIMARY ! By Accessory
Record                   RECORD,PRE()
Ref_Number                  REAL                      ! 
Accessory                   STRING(30)                ! 
                         END
                     END                       

ESNMODEL             FILE,DRIVER('Btrieve'),PRE(esn),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! ESN And Model Number File
Record_Number_Key        KEY(esn:Record_Number),NOCASE,PRIMARY ! By Record Number
ESN_Key                  KEY(esn:ESN,esn:Model_Number),DUP,NOCASE ! By ESN
ESN_Only_Key             KEY(esn:ESN),DUP,NOCASE      ! By ESN
Model_Number_Key         KEY(esn:Model_Number,esn:ESN),DUP,NOCASE ! 
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
ESN                         STRING(6)                 ! 
Model_Number                STRING(30)                ! 
                         END
                     END                       

JOBPAYMT             FILE,DRIVER('Btrieve'),PRE(jpt),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Job Payments
Record_Number_Key        KEY(jpt:Record_Number),NOCASE,PRIMARY ! By Record Number
All_Date_Key             KEY(jpt:Ref_Number,jpt:Date),DUP,NOCASE ! By Date
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
Ref_Number                  REAL                      ! 
Date                        DATE                      ! 
Payment_Type                STRING(30)                ! 
Credit_Card_Number          STRING(20)                ! 
Expiry_Date                 STRING(5)                 ! 
Issue_Number                STRING(5)                 ! 
Amount                      REAL                      ! 
User_Code                   STRING(3)                 ! 
                         END
                     END                       

LOGASSSTTEMP         FILE,DRIVER('Btrieve'),OEM,PRE(logasttmp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)            ! Logistic Associated Stock
RecordNumberKey          KEY(logasttmp:RecordNumber),NOCASE,PRIMARY ! By Record Number
PartNumberKey            KEY(logasttmp:PartNumber),DUP,NOCASE ! By Ref Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! 
RefNumber                   LONG                      ! Link Back To Associated Stock
PartNumber                  STRING(30)                ! Part Number
Description                 STRING(30)                ! 
Quantity                    LONG                      ! 
PartRefNumber               LONG                      ! Relation To Stock Control
Location                    STRING(30)                ! 
DummyField                  STRING(1)                 ! 
                         END
                     END                       

STOCK                FILE,DRIVER('Btrieve'),PRE(sto),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                   ! Stock Control
Ref_Number_Key           KEY(sto:Ref_Number),NOCASE,PRIMARY ! By Ref Number
Sundry_Item_Key          KEY(sto:Sundry_Item),DUP,NOCASE ! By Sundry Item
Description_Key          KEY(sto:Location,sto:Description),DUP,NOCASE ! By Description
Description_Accessory_Key KEY(sto:Location,sto:Accessory,sto:Description),DUP,NOCASE ! By Description
Shelf_Location_Key       KEY(sto:Location,sto:Shelf_Location),DUP,NOCASE ! By Shelf Location
Shelf_Location_Accessory_Key KEY(sto:Location,sto:Accessory,sto:Shelf_Location),DUP,NOCASE ! By Shelf Location
Supplier_Accessory_Key   KEY(sto:Location,sto:Accessory,sto:Shelf_Location),DUP,NOCASE ! By Supplier
Manufacturer_Key         KEY(sto:Manufacturer,sto:Part_Number),DUP,NOCASE ! By Manufacturer
Supplier_Key             KEY(sto:Location,sto:Supplier),DUP,NOCASE ! By Supplier
Location_Key             KEY(sto:Location,sto:Part_Number),DUP,NOCASE ! By Location
Part_Number_Accessory_Key KEY(sto:Location,sto:Accessory,sto:Part_Number),DUP,NOCASE ! By Part Number
Ref_Part_Description_Key KEY(sto:Location,sto:Ref_Number,sto:Part_Number,sto:Description),DUP,NOCASE ! Why?
Location_Manufacturer_Key KEY(sto:Location,sto:Manufacturer,sto:Part_Number),DUP,NOCASE ! By Manufacturer
Manufacturer_Accessory_Key KEY(sto:Location,sto:Accessory,sto:Manufacturer,sto:Part_Number),DUP,NOCASE ! By Part Number
Location_Part_Description_Key KEY(sto:Location,sto:Part_Number,sto:Description),DUP,NOCASE ! By Part Number
Ref_Part_Description2_Key KEY(sto:Ref_Number,sto:Part_Number,sto:Description),DUP,NOCASE ! CASCADE Key
Minimum_Part_Number_Key  KEY(sto:Minimum_Stock,sto:Location,sto:Part_Number),DUP,NOCASE ! By Part Number
Minimum_Description_Key  KEY(sto:Minimum_Stock,sto:Location,sto:Description),DUP,NOCASE ! 
SecondLocKey             KEY(sto:Location,sto:Shelf_Location,sto:Second_Location,sto:Part_Number),DUP,NOCASE ! By Part Number
RequestedKey             KEY(sto:QuantityRequested,sto:Part_Number),DUP,NOCASE ! By Part Number
ExchangeAccPartKey       KEY(sto:ExchangeUnit,sto:Location,sto:Part_Number),DUP,NOCASE ! By Part Number
ExchangeAccDescKey       KEY(sto:ExchangeUnit,sto:Location,sto:Description),DUP,NOCASE ! By Description
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)                 ! 
Ref_Number                  LONG                      ! Reference Number
Part_Number                 STRING(30)                ! 
Description                 STRING(30)                ! 
Supplier                    STRING(30)                ! 
Purchase_Cost               REAL                      ! 
Sale_Cost                   REAL                      ! 
Retail_Cost                 REAL                      ! 
AccessoryCost               REAL                      ! Accessory Cost
Percentage_Mark_Up          REAL                      ! 
Shelf_Location              STRING(30)                ! 
Manufacturer                STRING(30)                ! 
Location                    STRING(30)                ! 
Second_Location             STRING(30)                ! 
Quantity_Stock              REAL                      ! 
Quantity_To_Order           REAL                      ! 
Quantity_On_Order           REAL                      ! 
Minimum_Level               REAL                      ! 
Reorder_Level               REAL                      ! 
Use_VAT_Code                STRING(3)                 ! 
Service_VAT_Code            STRING(2)                 ! 
Retail_VAT_Code             STRING(2)                 ! 
Superceeded                 STRING(3)                 ! 
Superceeded_Ref_Number      REAL                      ! 
Pending_Ref_Number          REAL                      ! 
Accessory                   STRING(3)                 ! 
Minimum_Stock               STRING(3)                 ! 
Assign_Fault_Codes          STRING(3)                 ! 
Individual_Serial_Numbers   STRING(3)                 ! 
ExchangeUnit                STRING(3)                 ! 
QuantityRequested           LONG                      ! Quantity Requested
Suspend                     BYTE                      ! Suspend Part
Fault_Code1                 STRING(30)                ! 
Fault_Code2                 STRING(30)                ! 
Fault_Code3                 STRING(30)                ! 
Fault_Code4                 STRING(30)                ! 
Fault_Code5                 STRING(30)                ! 
Fault_Code6                 STRING(30)                ! 
Fault_Code7                 STRING(30)                ! 
Fault_Code8                 STRING(30)                ! 
Fault_Code9                 STRING(30)                ! 
Fault_Code10                STRING(30)                ! 
Fault_Code11                STRING(30)                ! 
Fault_Code12                STRING(30)                ! 
E1                          BYTE                      ! E1
E2                          BYTE                      ! E2
E3                          BYTE                      ! E3
AllowDuplicate              BYTE                      ! Allow Duplicate Part On Jobs
ExcludeFromEDI              BYTE                      ! Exclude From EDI
RF_Board                    BYTE                      ! 
                         END
                     END                       

LOGGED               FILE,DRIVER('Btrieve'),PRE(log),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! Logged In
Surname_Key              KEY(log:Surname,log:Date),DUP,NOCASE ! By Surname
record_number_key        KEY(log:record_number),NOCASE,PRIMARY ! 
User_Code_Key            KEY(log:User_Code,log:Time),DUP,NOCASE ! By User Code
Record                   RECORD,PRE()
User_Code                   STRING(3)                 ! 
record_number               REAL                      ! 
Surname                     STRING(30)                ! 
Forename                    STRING(30)                ! 
Date                        DATE                      ! 
Time                        TIME                      ! 
                         END
                     END                       

LOGRTHIS             FILE,DRIVER('Btrieve'),OEM,PRE(lsrh),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Logisitics Stock Returns History
RefNumberKey             KEY(lsrh:RefNumber),DUP,NOCASE ! By Ref Number
DateIMEIKey              KEY(lsrh:Date,lsrh:IMEI),DUP,NOCASE ! By I.M.E.I. Number
Record                   RECORD,PRE()
RefNumber                   LONG                      ! 
IMEI                        STRING(30)                ! 
Quantity                    LONG                      ! 
Date                        DATE                      ! 
ModelNumber                 STRING(30)                ! 
DummyField                  STRING(1)                 ! For File Manager
                         END
                     END                       

LOGSTHIS             FILE,DRIVER('Btrieve'),OEM,PRE(logsth),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Logistic Stock History
RecordNumberKey          KEY(logsth:RecordNumber),NOCASE,PRIMARY ! By Record Number
RefNumberKey             KEY(logsth:RefNumber),DUP,NOCASE ! By Ref Number
DateKey                  KEY(logsth:RefNumber,logsth:Date),DUP,NOCASE ! By Date
StatusDateKey            KEY(logsth:RefNumber,logsth:Status,logsth:Date),DUP,NOCASE ! By Date
DateStaClubKey           KEY(logsth:Date,logsth:Status,logsth:ModelNumber,logsth:Location,logsth:SMPFNumber,logsth:DespatchNoteNo,logsth:ClubNokia),DUP,NOCASE ! By Status
RefModelNoKey            KEY(logsth:RefNumber,logsth:ModelNumber),DUP,NOCASE ! By ModelNumber
DespatchNoKey            KEY(logsth:DespatchNo),DUP,NOCASE ! By Despatch Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! 
RefNumber                   LONG                      ! 
Date                        DATE                      ! 
UserCode                    STRING(3)                 ! 
StockIn                     LONG                      ! 
StockOut                    LONG                      ! 
Status                      STRING(10)                ! 
ClubNokia                   STRING(30)                ! 
SMPFNumber                  STRING(30)                ! 
DespatchNoteNo              STRING(30)                ! 
ModelNumber                 STRING(30)                ! 
Location                    STRING(30)                ! 
OrderRefNo                  STRING(30)                ! 
DespatchNo                  LONG                      ! 
Deletion_Reason             STRING(40)                ! 
DummyField                  STRING(6)                 ! 
                         END
                     END                       

LOGASSST             FILE,DRIVER('Btrieve'),OEM,PRE(logast),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Logistic Associated Stock
RecordNumberKey          KEY(logast:RecordNumber),NOCASE,PRIMARY ! By Record Number
RefNumberKey             KEY(logast:RefNumber,logast:PartNumber),DUP,NOCASE ! By Ref Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! 
RefNumber                   LONG                      ! 
PartNumber                  STRING(30)                ! Part Number
Description                 STRING(30)                ! 
PartRefNumber               LONG                      ! Relation To Stock Control
Location                    STRING(30)                ! 
DummyField                  STRING(1)                 ! 
                         END
                     END                       

LOGSTOLC             FILE,DRIVER('Btrieve'),OEM,PRE(logstl),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Logistics Stock Location
RefNumberKey             KEY(logstl:RefNumber),NOCASE,PRIMARY ! By Ref Number
LocationKey              KEY(logstl:Location),NOCASE  ! By Location
Record                   RECORD,PRE()
RefNumber                   LONG                      ! 
Location                    STRING(30)                ! 
Mark_Returns_Available      BYTE                      ! 
                         END
                     END                       

LOGTEMP              FILE,DRIVER('Btrieve'),OEM,PRE(logtmp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Logistic Temporary File
RefNumberKey             KEY(logtmp:RefNumber),NOCASE,PRIMARY ! By Ref Number
IMEIKey                  KEY(logtmp:IMEI),DUP,NOCASE  ! By I.M.E.I.
Record                   RECORD,PRE()
RefNumber                   LONG                      ! 
IMEI                        STRING(30)                ! 
Marker                      BYTE                      ! 
                         END
                     END                       

LOGSERST             FILE,DRIVER('Btrieve'),OEM,PRE(logser),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Logistic Serialized Stock
RecordNumberKey          KEY(logser:RecordNumber),NOCASE,PRIMARY ! By Record Number
ESNKey                   KEY(logser:ESN),DUP,NOCASE   ! By I.M.E.I.
RefNumberKey             KEY(logser:RefNumber,logser:ESN),DUP,NOCASE ! By Ref Number
ESNStatusKey             KEY(logser:RefNumber,logser:Status,logser:ESN),DUP,NOCASE ! By E.S.N. / I.M.E.I.
NokiaStatusKey           KEY(logser:RefNumber,logser:Status,logser:ClubNokia,logser:ESN),DUP,NOCASE ! By Club Nokia Site
AllocNo_Key              KEY(logser:AllocNo,logser:Status,logser:ESN),DUP,NOCASE ! 
Status_Alloc_Key         KEY(logser:RefNumber,logser:ClubNokia,logser:Status),DUP,NOCASE ! 
Record                   RECORD,PRE()
RecordNumber                LONG                      ! 
RefNumber                   LONG                      ! 
ESN                         STRING(30)                ! 
Status                      STRING(10)                ! 
ClubNokia                   STRING(30)                ! Club Nokia Site
AllocNo                     LONG                      ! 
DummyField                  STRING(4)                 ! For File Manager
                         END
                     END                       

LOGDEFLT             FILE,DRIVER('Btrieve'),OEM,PRE(ldef),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Logisitics Defaults
RecordNumberKey          KEY(ldef:RecordNumber),NOCASE,PRIMARY ! By Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! 
Location                    STRING(30)                ! 
                         END
                     END                       

LOGSTLOC             FILE,DRIVER('Btrieve'),OEM,PRE(lstl),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Logisitics Stock Locations - Related
RecordNumberKey          KEY(lstl:RecordNumber),NOCASE,PRIMARY ! By Record Number
RefLocationKey           KEY(lstl:RefNumber,lstl:Location),NOCASE ! By Location
Record                   RECORD,PRE()
RecordNumber                LONG                      ! 
RefNumber                   LONG                      ! 
Location                    STRING(30)                ! 
AvlQuantity                 LONG                      ! 
DesQuantity                 LONG                      ! 
allocquantity               LONG                      ! 
DummyField                  STRING(1)                 ! For File Manager
                         END
                     END                       

MULTIDEF             FILE,DRIVER('Btrieve'),OEM,PRE(mul),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Multiple Booking Defaults
RecordNumberKey          KEY(mul:RecordNumber),NOCASE,PRIMARY ! By Record Number
DescriptionKey           KEY(mul:Description),NOCASE  ! By Description
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
Description                 STRING(255)               ! Description
ModelNumber                 STRING(30)                ! Model Number
UnitType                    STRING(30)                ! Unit Type
Accessories                 STRING(30)                ! Accessories
OrderNumber                 STRING(30)                ! Order Number
InternalLocation            STRING(30)                ! Internal Location
FaultDescription            STRING(255)               ! Fault Description
DOP                         DATE                      ! Date Of Purchase
ChaChargeType               STRING(30)                ! Chargeable Charge Type
ChaRepairType               STRING(30)                ! Chargeable Repair Type
WarChargeType               STRING(30)                ! Warranty Charge Type
WarRepairType               STRING(30)                ! Warranty Repair Type
                         END
                     END                       

TRAFAULO             FILE,DRIVER('Btrieve'),PRE(tfo),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Trade Account Fault Coding Lookup Table
Field_Key                KEY(tfo:AccountNumber,tfo:Field_Number,tfo:Field,tfo:Description),NOCASE,PRIMARY ! By Field
DescriptionKey           KEY(tfo:AccountNumber,tfo:Field_Number,tfo:Description),DUP,NOCASE ! By Description
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
AccountNumber               STRING(30)                ! Account Number
Field_Number                REAL                      ! 
Field                       STRING(30)                ! 
Description                 STRING(60)                ! 
                         END
                     END                       

MPXJOBS              FILE,DRIVER('Btrieve'),OEM,PRE(MXJ),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! MPX XML Jobs File
RecordNoKey              KEY(MXJ:RecordNo),NOCASE,PRIMARY ! 
IDKey                    KEY(MXJ:ID),DUP,NOCASE       ! 
JobNoKey                 KEY(MXJ:InspACRJobNo),DUP,NOCASE ! 
StateJobNoKey            KEY(MXJ:Record_State,MXJ:InspACRJobNo),DUP,NOCASE ! 
StateSurnameKey          KEY(MXJ:Record_State,MXJ:CustLastName),DUP,NOCASE ! 
StatePostcodeKey         KEY(MXJ:Record_State,MXJ:CustPostCode),DUP,NOCASE ! 
StateModelKey            KEY(MXJ:Record_State,MXJ:ModelID),DUP,NOCASE ! 
StateReportCodeKey       KEY(MXJ:Record_State,MXJ:ReportCode),DUP,NOCASE ! 
Record                   RECORD,PRE()
RecordNo                    LONG                      ! 
Record_State                BYTE                      ! 
ReportCode                  LONG                      ! 
Exception_Code              LONG                      ! 
Exception_Desc              STRING(255)               ! 
ImportDate                  DATE                      ! 
ImportTime                  TIME                      ! 
ID                          LONG                      ! 
InspACRJobNo                LONG                      ! 
EnvDateSent                 DATE                      ! 
InspACRStatus               STRING(50)                ! 
InspDateRecd                DATE                      ! 
InspModelID                 STRING(50)                ! 
InspFlag                    BYTE                      ! 
InspAttention               STRING(255)               ! 
InspQty                     LONG                      ! 
InspPackageCond             STRING(50)                ! 
InspQ1                      BYTE                      ! 
InspQ2                      BYTE                      ! 
InspQ3                      BYTE                      ! 
InspACC1                    BYTE                      ! 
InspACC2                    BYTE                      ! 
InspACC3                    BYTE                      ! 
InspACC4                    BYTE                      ! 
InspACC5                    BYTE                      ! 
InspGrade                   STRING(50)                ! 
InspIMEI                    STRING(15)                ! 
InspNotes                   STRING(255)               ! 
InspDate                    DATE                      ! 
InspBrand                   STRING(50)                ! 
InspInWarranty              BYTE                      ! 
DespatchConsignment         STRING(50)                ! 
DespatchDate                DATE                      ! 
CustCTN                     STRING(50)                ! 
QuoteValue                  LONG                      ! 
ModelID                     STRING(50)                ! 
QuoteQ1                     BYTE                      ! 
QuoteQ2                     BYTE                      ! 
QuoteQ3                     BYTE                      ! 
QuoteACC1                   BYTE                      ! 
QuoteACC2                   BYTE                      ! 
QuoteACC3                   BYTE                      ! 
QuoteACC4                   BYTE                      ! 
QuoteACC5                   BYTE                      ! 
CustTitle                   STRING(50)                ! 
CustFirstName               STRING(50)                ! 
CustLastName                STRING(50)                ! 
CustAdd1                    STRING(50)                ! 
CustAdd2                    STRING(50)                ! 
CustAdd3                    STRING(50)                ! 
CustAdd4                    STRING(50)                ! 
CustPostCode                STRING(50)                ! 
CustRef                     STRING(50)                ! 
CustNetwork                 STRING(50)                ! 
EnvLetter                   STRING(50)                ! 
InspType                    STRING(50)                ! 
EnvType                     STRING(50)                ! 
EnvCount                    LONG                      ! 
                         END
                     END                       

MPXSTAT              FILE,DRIVER('Btrieve'),OEM,PRE(MXS),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! MPX XML Status Mapping File
RecordNoKey              KEY(MXS:RecordNo),NOCASE,PRIMARY ! 
MpxStatusKey             KEY(MXS:MPX_STATUS),DUP,NOCASE ! 
SBStatusKey              KEY(MXS:SB_STATUS),DUP,NOCASE ! 
Record                   RECORD,PRE()
RecordNo                    LONG                      ! 
MPX_STATUS                  LONG                      ! 
SB_STATUS                   STRING(30)                ! 
                         END
                     END                       

TRAFAULT             FILE,DRIVER('Btrieve'),PRE(taf),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Trade Account Fault Coding On Jobs
RecordNumberKey          KEY(taf:RecordNumber),NOCASE,PRIMARY ! 
Field_Number_Key         KEY(taf:AccountNumber,taf:Field_Number),NOCASE ! By Field Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
AccountNumber               STRING(30)                ! Account Number
Field_Number                REAL                      ! Fault Code Field Number
Field_Name                  STRING(30)                ! 
Field_Type                  STRING(30)                ! 
Lookup                      STRING(3)                 ! 
Force_Lookup                STRING(3)                 ! 
Compulsory                  STRING(3)                 ! 
Compulsory_At_Booking       STRING(3)                 ! 
ReplicateFault              STRING(3)                 ! Replicate To Fault Description Field
ReplicateInvoice            STRING(3)                 ! Replicate To Fault Description Field
RestrictLength              BYTE                      ! Restrict Length
LengthFrom                  LONG                      ! Length From
LengthTo                    LONG                      ! Length To
                         END
                     END                       

EXMINLEV             FILE,DRIVER('Btrieve'),OEM,PRE(exm),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Exchange Unit Minimum Level Browse
RecordNumberKey          KEY(exm:RecordNumber),NOCASE,PRIMARY ! By Record Number
ManufacturerKey          KEY(exm:Manufacturer,exm:ModelNumber),DUP,NOCASE ! By Model Number
ModelNumberKey           KEY(exm:ModelNumber),DUP,NOCASE ! By Model Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
Manufacturer                STRING(30)                ! Manufacturer
ModelNumber                 STRING(30)                ! Model Number
MinimumLevel                LONG                      ! Minimum Level
Available                   LONG                      ! Available
                         END
                     END                       

LOGSTHII             FILE,DRIVER('Btrieve'),OEM,PRE(logsti),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Logistic Stock History IMEIs
RecordNumberKey          KEY(logsti:RecordNumber),NOCASE,PRIMARY ! By Record Number
RefNumberKey             KEY(logsti:RefNumber,logsti:IMEI),DUP,NOCASE ! By I.M.E.I.
IMEIRefNoKey             KEY(logsti:IMEI,logsti:RefNumber),DUP,NOCASE ! By I.M.E.I. Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! 
RefNumber                   LONG                      ! 
IMEI                        STRING(30)                ! 
DummyField                  STRING(2)                 ! 
                         END
                     END                       

LOGCLSTE             FILE,DRIVER('Btrieve'),OEM,PRE(logclu),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Logistic Club Nokia Site
RecordNumberKey          KEY(logclu:RecordNumber),NOCASE,PRIMARY ! By Record Number
ClubNokiaKey             KEY(logclu:ClubNokia),NOCASE ! By Club Nokia Site
Record                   RECORD,PRE()
RecordNumber                LONG                      ! 
ClubNokia                   STRING(30)                ! 
Postcode                    STRING(15)                ! 
AddressLine1                STRING(30)                ! 
AddressLine2                STRING(30)                ! 
AddressLine3                STRING(30)                ! 
TelephoneNumber             STRING(30)                ! 
FaxNumber                   STRING(30)                ! 
DummyField                  STRING(1)                 ! For File Manager
                         END
                     END                       

STATCRIT             FILE,DRIVER('Btrieve'),OEM,PRE(stac),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Status Report Criteria
RecordNumberKey          KEY(stac:RecordNumber),NOCASE,PRIMARY ! By Record Number
DescriptionKey           KEY(stac:Description),DUP,NOCASE ! By Description
DescriptionOptionKey     KEY(stac:Description,stac:OptionType),DUP,NOCASE ! By FieldValue
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
Description                 STRING(60)                ! Description
OptionType                  STRING(30)                ! Option Type
FieldValue                  STRING(40)                ! Field Value
                         END
                     END                       

NETWORKS             FILE,DRIVER('Btrieve'),OEM,PRE(net),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Networks
RecordNumberKey          KEY(net:RecordNumber),NOCASE,PRIMARY ! By Record Number
NetworkKey               KEY(net:Network),NOCASE      ! By Network
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
Network                     STRING(30)                ! Network
                         END
                     END                       

LOGDESNO             FILE,DRIVER('Btrieve'),OEM,PRE(ldes),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Logistics Despatch Numbers
DespatchNoKey            KEY(ldes:DespatchNo),NOCASE,PRIMARY ! By Despatch Number
Record                   RECORD,PRE()
DespatchNo                  LONG                      ! 
Date                        DATE                      ! 
Time                        TIME                      ! 
                         END
                     END                       

CPNDPRTS             FILE,DRIVER('Btrieve'),OEM,PRE(tmppen),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Crystal Reports
RecordNumberKey          KEY(tmppen:RecordNumber),NOCASE,PRIMARY ! By Record Number
JobNumberKey             KEY(tmppen:UserID,tmppen:JobNumber),DUP,NOCASE ! By Job Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
UserID                      STRING(30)                ! User ID
JobNumber                   LONG                      ! Job Number
DateBooked                  DATE                      ! Date Booked
AccountNumber               STRING(30)                ! Account Number
ModelNumber                 STRING(30)                ! Model Number
PartNumber                  STRING(30)                ! Part Number
Description                 STRING(30)                ! Description
Quantity                    LONG                      ! Quantity
DateOrdered                 DATE                      ! Date Ordered
Location                    STRING(30)                ! Location
Status                      STRING(30)                ! Status
                         END
                     END                       

LOGSALCD             FILE,DRIVER('Btrieve'),OEM,PRE(logsal),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Logistics Sales Codes
RefNumberKey             KEY(logsal:RefNumber),NOCASE,PRIMARY ! By Ref Number
SalesCodeKey             KEY(logsal:SalesCode),NOCASE ! By Sales Code
Record                   RECORD,PRE()
RefNumber                   LONG                      ! 
SalesCode                   STRING(30)                ! Sales Code
Description                 STRING(30)                ! 
DummyField                  STRING(1)                 ! 
                         END
                     END                       

DEFSTOCK             FILE,DRIVER('Btrieve'),PRE(dst),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Defaults - Stock Control
record_number_key        KEY(dst:record_number),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
Use_Site_Location           STRING(3)                 ! 
record_number               REAL                      ! 
Site_Location               STRING(30)                ! 
                         END
                     END                       

JOBLOHIS             FILE,DRIVER('Btrieve'),PRE(jlh),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Job Loan History
Ref_Number_Key           KEY(jlh:Ref_Number,jlh:Date,jlh:Time),DUP,NOCASE ! By Date
record_number_key        KEY(jlh:record_number),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
Ref_Number                  REAL                      ! 
record_number               REAL                      ! 
Loan_Unit_Number            REAL                      ! 
Date                        DATE                      ! 
Time                        TIME                      ! 
User                        STRING(3)                 ! 
Status                      STRING(60)                ! 
                         END
                     END                       

LOAN                 FILE,DRIVER('Btrieve'),PRE(loa),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                    ! Loan Units
Ref_Number_Key           KEY(loa:Ref_Number),NOCASE,PRIMARY ! By Loan Unit Number
ESN_Only_Key             KEY(loa:ESN),DUP,NOCASE      ! 
MSN_Only_Key             KEY(loa:MSN),DUP,NOCASE      ! 
Ref_Number_Stock_Key     KEY(loa:Stock_Type,loa:Ref_Number),DUP,NOCASE ! By Ref Number
Model_Number_Key         KEY(loa:Stock_Type,loa:Model_Number,loa:Ref_Number),DUP,NOCASE ! By Model Number
ESN_Key                  KEY(loa:Stock_Type,loa:ESN),DUP,NOCASE ! By E.S.N. / I.M.E.I. Number
MSN_Key                  KEY(loa:Stock_Type,loa:MSN),DUP,NOCASE ! By M.S.N. Number
ESN_Available_Key        KEY(loa:Available,loa:Stock_Type,loa:ESN),DUP,NOCASE ! By E.S.N. / I.M.E.I.
MSN_Available_Key        KEY(loa:Available,loa:Stock_Type,loa:MSN),DUP,NOCASE ! By M.S.N.
Ref_Available_Key        KEY(loa:Available,loa:Stock_Type,loa:Ref_Number),DUP,NOCASE ! By Loan Unit Number
Model_Available_Key      KEY(loa:Available,loa:Stock_Type,loa:Model_Number,loa:Ref_Number),DUP,NOCASE ! By Model Number
Stock_Type_Key           KEY(loa:Stock_Type),DUP,NOCASE ! By Stock Type
ModelRefNoKey            KEY(loa:Model_Number,loa:Ref_Number),DUP,NOCASE ! By Unit Number
AvailIMEIOnlyKey         KEY(loa:Available,loa:ESN),DUP,NOCASE ! By I.M.E.I. Number
AvailMSNOnlyKey          KEY(loa:Available,loa:MSN),DUP,NOCASE ! By M.S.N
AvailRefOnlyKey          KEY(loa:Available,loa:Ref_Number),DUP,NOCASE ! By Unit Number
AvailModOnlyKey          KEY(loa:Available,loa:Model_Number,loa:Ref_Number),DUP,NOCASE ! By Unit Number
Record                   RECORD,PRE()
Ref_Number                  LONG                      ! 
Model_Number                STRING(30)                ! 
Manufacturer                STRING(30)                ! 
ESN                         STRING(30)                ! 
MSN                         STRING(30)                ! 
Colour                      STRING(30)                ! 
Location                    STRING(30)                ! 
Shelf_Location              STRING(30)                ! 
Date_Booked                 DATE                      ! 
Times_Issued                REAL                      ! 
Available                   STRING(3)                 ! 
Job_Number                  LONG                      ! 
Stock_Type                  STRING(30)                ! 
DummyField                  STRING(1)                 ! For File Manager
                         END
                     END                       

NOTESCON             FILE,DRIVER('Btrieve'),PRE(noc),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Notes - Contact History
RecordNumberKey          KEY(noc:RecordNumber),NOCASE,PRIMARY ! By Record Number
Notes_Key                KEY(noc:Reference,noc:Notes),NOCASE ! By Notes
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
Reference                   STRING(30)                ! 
Notes                       STRING(80)                ! 
                         END
                     END                       

VATCODE              FILE,DRIVER('Btrieve'),PRE(vat),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Vat Code
Vat_code_Key             KEY(vat:VAT_Code),NOCASE,PRIMARY ! By V.A.T. Code
Record                   RECORD,PRE()
VAT_Code                    STRING(2)                 ! 
VAT_Rate                    REAL                      ! 
                         END
                     END                       

LOANHIST             FILE,DRIVER('Btrieve'),PRE(loh),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Loan History
Record_Number_Key        KEY(loh:record_number),NOCASE,PRIMARY ! By Record Number
Ref_Number_Key           KEY(loh:Ref_Number,-loh:Date,-loh:Time),DUP,NOCASE ! By Ref Number
Record                   RECORD,PRE()
Ref_Number                  REAL                      ! 
record_number               REAL                      ! 
Date                        DATE                      ! 
Time                        TIME                      ! 
User                        STRING(3)                 ! 
Status                      STRING(60)                ! 
                         END
                     END                       

TURNARND             FILE,DRIVER('Btrieve'),PRE(tur),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Turnaround Time
Turnaround_Time_Key      KEY(tur:Turnaround_Time),NOCASE,PRIMARY ! By Turnaround Time
Record                   RECORD,PRE()
Turnaround_Time             STRING(30)                ! 
Days                        REAL                      ! 
Hours                       REAL                      ! 
                         END
                     END                       

ORDJOBS              FILE,DRIVER('Btrieve'),OEM,PRE(orjtmp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Temp List Of Jobs Requiring Parts
RecordNumberKey          KEY(orjtmp:RecordNumber),NOCASE,PRIMARY ! Record Number
JobNumberKey             KEY(orjtmp:PartNumber,orjtmp:JobNumber),DUP,NOCASE ! By Job Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
OrderNumber                 LONG                      ! Order Number
PartNumber                  STRING(30)                ! Part Number
JobNumber                   LONG                      ! Job Number
RefNumber                   LONG                      ! Record Number Of Part
CharWarr                    STRING(1)                 ! Chargeable/Warranty
Quantity                    LONG                      ! Quantity
                         END
                     END                       

MERGETXT             FILE,DRIVER('Btrieve'),OEM,PRE(mrt),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Mail Merge Text
RefNumberKey             KEY(mrt:RefNumber),DUP,NOCASE ! By Ref Number
Record                   RECORD,PRE()
RefNumber                   LONG                      ! 
Letter                      STRING(10000)             ! 
                         END
                     END                       

MERGELET             FILE,DRIVER('Btrieve'),PRE(mrg),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Mail Merge Letters
RefNumberKey             KEY(mrg:RefNumber),NOCASE,PRIMARY ! By Ref Number
LetterNameKey            KEY(mrg:LetterName),NOCASE   ! By Letter Name
Record                   RECORD,PRE()
RefNumber                   LONG                      ! 
LetterName                  STRING(30)                ! 
UseStatus                   STRING(3)                 ! Is a Status Change Required?
Status                      STRING(30)                ! 
                         END
                     END                       

ORDSTOCK             FILE,DRIVER('Btrieve'),OEM,PRE(orstmp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Temporary List Of Stock Requests
RecordNumberKey          KEY(orstmp:RecordNumber),NOCASE,PRIMARY ! By Record Number
LocationKey              KEY(orstmp:PartNumber,orstmp:Location),DUP,NOCASE ! By Location
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
OrderNumber                 LONG                      ! Order Number
PartNumber                  STRING(30)                ! Part Number
Location                    STRING(30)                ! Location
Quantity                    LONG                      ! Quantity
                         END
                     END                       

EXCHHIST             FILE,DRIVER('Btrieve'),PRE(exh),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Exchange History
Record_Number_Key        KEY(exh:record_number),NOCASE,PRIMARY ! By Record Number
Ref_Number_Key           KEY(exh:Ref_Number,-exh:Date,-exh:Time),DUP,NOCASE ! By Ref Number
Record                   RECORD,PRE()
Ref_Number                  REAL                      ! 
record_number               REAL                      ! 
Date                        DATE                      ! 
Time                        TIME                      ! 
User                        STRING(3)                 ! 
Status                      STRING(60)                ! 
                         END
                     END                       

ACCESSOR             FILE,DRIVER('Btrieve'),PRE(acr),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Accessories
Accesory_Key             KEY(acr:Model_Number,acr:Accessory),NOCASE,PRIMARY ! By Accessory
Model_Number_Key         KEY(acr:Accessory,acr:Model_Number),NOCASE ! By Model Number
AccessOnlyKey            KEY(acr:Accessory),DUP,NOCASE ! By Accessory
Record                   RECORD,PRE()
Accessory                   STRING(30)                ! 
Model_Number                STRING(30)                ! 
                         END
                     END                       

SUBACCAD             FILE,DRIVER('Btrieve'),OEM,PRE(sua),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Sub Account Addresses
RecordNumberKey          KEY(sua:RecordNumber),NOCASE,PRIMARY ! By Record Number
AccountNumberKey         KEY(sua:RefNumber,sua:AccountNumber),DUP,NOCASE ! By Account Number
CompanyNameKey           KEY(sua:RefNumber,sua:CompanyName),DUP,NOCASE ! By Company Name
AccountNumberOnlyKey     KEY(sua:AccountNumber),NOCASE ! By Account Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
RefNumber                   LONG                      ! RefNumber
AccountNumber               STRING(15)                ! 
CompanyName                 STRING(30)                ! 
Postcode                    STRING(15)                ! 
AddressLine1                STRING(30)                ! 
AddressLine2                STRING(30)                ! 
AddressLine3                STRING(30)                ! 
TelephoneNumber             STRING(15)                ! 
FaxNumber                   STRING(15)                ! 
EmailAddress                STRING(255)               ! Email Address
ContactName                 STRING(30)                ! 
                         END
                     END                       

NOTESDEL             FILE,DRIVER('Btrieve'),OEM,PRE(nod),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! 
keyonnotesdel            KEY(nod:ReasonCode),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
ReasonCode                  STRING(30)                ! 
ReasonText                  STRING(80)                ! 
                         END
                     END                       

PICKDET              FILE,DRIVER('Btrieve'),OEM,PRE(pdt),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! 
keypickdet               KEY(pdt:PickDetailRef),NOCASE,PRIMARY ! 
keyonpicknote            KEY(pdt:PickNoteRef),DUP,NOCASE ! 
Record                   RECORD,PRE()
PickDetailRef               LONG                      ! 
PickNoteRef                 LONG                      ! 
PartRefNumber               LONG                      ! 
Quantity                    LONG                      ! 
IsChargeable                BYTE                      ! 
RequestDate                 DATE                      ! 
RequestTime                 TIME                      ! 
EngineerCode                STRING(3)                 ! 
IsInStock                   BYTE                      ! 
IsPrinted                   BYTE                      ! 
IsDelete                    BYTE                      ! 
DeleteReason                STRING(255)               ! 
Usercode                    STRING(3)                 ! 
StockPartRefNumber          LONG                      ! 
PartNumber                  STRING(30)                ! 
                         END
                     END                       

WARPARTS             FILE,DRIVER('Btrieve'),PRE(wpr),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Warrant Parts On A Job
Part_Number_Key          KEY(wpr:Ref_Number,wpr:Part_Number),DUP,NOCASE ! By Part Number
RecordNumberKey          KEY(wpr:Record_Number),NOCASE,PRIMARY ! 
PartRefNoKey             KEY(wpr:Part_Ref_Number),DUP,NOCASE ! By Part Ref Number
Date_Ordered_Key         KEY(wpr:Date_Ordered),DUP,NOCASE ! By Date Ordered
RefPartRefNoKey          KEY(wpr:Ref_Number,wpr:Part_Ref_Number),DUP,NOCASE ! By Part Ref Number
PendingRefNoKey          KEY(wpr:Ref_Number,wpr:Pending_Ref_Number),DUP,NOCASE ! 
Order_Number_Key         KEY(wpr:Ref_Number,wpr:Order_Number),DUP,NOCASE ! 
Supplier_Key             KEY(wpr:Supplier),DUP,NOCASE ! By Supplier
Order_Part_Key           KEY(wpr:Ref_Number,wpr:Order_Part_Number),DUP,NOCASE ! By Order Part Number
SuppDateOrdKey           KEY(wpr:Supplier,wpr:Date_Ordered),DUP,NOCASE ! By Date Ordered
SuppDateRecKey           KEY(wpr:Supplier,wpr:Date_Received),DUP,NOCASE ! By Date Received
RequestedKey             KEY(wpr:Requested,wpr:Part_Number),DUP,NOCASE ! By Part Number
Record                   RECORD,PRE()
Ref_Number                  LONG                      ! Job Number
Record_Number               LONG                      ! Record Number
Adjustment                  STRING(3)                 ! 
Part_Ref_Number             LONG                      ! Stock Reference Number
Part_Number                 STRING(30)                ! 
Description                 STRING(30)                ! 
Supplier                    STRING(30)                ! 
Purchase_Cost               REAL                      ! 
Sale_Cost                   REAL                      ! 
Retail_Cost                 REAL                      ! 
Quantity                    REAL                      ! 
Warranty_Part               STRING(3)                 ! 
Exclude_From_Order          STRING(3)                 ! 
Despatch_Note_Number        STRING(30)                ! 
Date_Ordered                DATE                      ! 
Pending_Ref_Number          LONG                      ! Pending Reference Number
Order_Number                LONG                      ! Order Number
Order_Part_Number           REAL                      ! 
Date_Received               DATE                      ! 
Status_Date                 DATE                      ! 
Main_Part                   STRING(3)                 ! 
Fault_Codes_Checked         STRING(3)                 ! 
InvoicePart                 LONG                      ! Link to Record Number of Invoice(Credit) Part
Credit                      STRING(3)                 ! Has this part been credited?
Requested                   BYTE                      ! Requested For Order
Fault_Code1                 STRING(30)                ! 
Fault_Code2                 STRING(30)                ! 
Fault_Code3                 STRING(30)                ! 
Fault_Code4                 STRING(30)                ! 
Fault_Code5                 STRING(30)                ! 
Fault_Code6                 STRING(30)                ! 
Fault_Code7                 STRING(30)                ! 
Fault_Code8                 STRING(30)                ! 
Fault_Code9                 STRING(30)                ! 
Fault_Code10                STRING(30)                ! 
Fault_Code11                STRING(30)                ! 
Fault_Code12                STRING(30)                ! 
                         END
                     END                       

PARTS                FILE,DRIVER('Btrieve'),PRE(par),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                   ! Parts On A Job
Part_Number_Key          KEY(par:Ref_Number,par:Part_Number),DUP,NOCASE ! By Part Number
recordnumberkey          KEY(par:Record_Number),NOCASE,PRIMARY ! 
PartRefNoKey             KEY(par:Part_Ref_Number),DUP,NOCASE ! By Part_Ref_Number
Date_Ordered_Key         KEY(par:Date_Ordered),DUP,NOCASE ! By Date Ordered
RefPartRefNoKey          KEY(par:Ref_Number,par:Part_Ref_Number),DUP,NOCASE ! By Part Ref Number
PendingRefNoKey          KEY(par:Ref_Number,par:Pending_Ref_Number),DUP,NOCASE ! 
Order_Number_Key         KEY(par:Ref_Number,par:Order_Number),DUP,NOCASE ! 
Supplier_Key             KEY(par:Supplier),DUP,NOCASE ! By Supplier
Order_Part_Key           KEY(par:Ref_Number,par:Order_Part_Number),DUP,NOCASE ! By Order Part Number
SuppDateOrdKey           KEY(par:Supplier,par:Date_Ordered),DUP,NOCASE ! By Date Ordered
SuppDateRecKey           KEY(par:Supplier,par:Date_Received),DUP,NOCASE ! By Date Received
RequestedKey             KEY(par:Requested,par:Part_Number),DUP,NOCASE ! By Part Number
Record                   RECORD,PRE()
Ref_Number                  LONG                      ! Job Number
Record_Number               LONG                      ! Record Number
Adjustment                  STRING(3)                 ! 
Part_Ref_Number             LONG                      ! Stock Reference Number
Part_Number                 STRING(30)                ! 
Description                 STRING(30)                ! 
Supplier                    STRING(30)                ! 
Purchase_Cost               REAL                      ! 
Sale_Cost                   REAL                      ! 
Retail_Cost                 REAL                      ! 
Quantity                    REAL                      ! 
Warranty_Part               STRING(3)                 ! 
Exclude_From_Order          STRING(3)                 ! 
Despatch_Note_Number        STRING(30)                ! 
Date_Ordered                DATE                      ! 
Pending_Ref_Number          LONG                      ! Pending Reference Number
Order_Number                LONG                      ! Order Number
Order_Part_Number           LONG                      ! Order Reference Number
Date_Received               DATE                      ! 
Status_Date                 DATE                      ! 
Fault_Codes_Checked         STRING(3)                 ! 
InvoicePart                 LONG                      ! Link to Record Number of Invoice(Credit) Part
Credit                      STRING(3)                 ! Has this part been credited?
Requested                   BYTE                      ! Parts Requested For Order
Fault_Code1                 STRING(30)                ! 
Fault_Code2                 STRING(30)                ! 
Fault_Code3                 STRING(30)                ! 
Fault_Code4                 STRING(30)                ! 
Fault_Code5                 STRING(30)                ! 
Fault_Code6                 STRING(30)                ! 
Fault_Code7                 STRING(30)                ! 
Fault_Code8                 STRING(30)                ! 
Fault_Code9                 STRING(30)                ! 
Fault_Code10                STRING(30)                ! 
Fault_Code11                STRING(30)                ! 
Fault_Code12                STRING(30)                ! 
                         END
                     END                       

JOBACC               FILE,DRIVER('Btrieve'),PRE(jac),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! Job Accesories
Ref_Number_Key           KEY(jac:Ref_Number,jac:Accessory),NOCASE,PRIMARY ! By Accessory
Record                   RECORD,PRE()
Ref_Number                  REAL                      ! 
Accessory                   STRING(30)                ! 
                         END
                     END                       

USELEVEL             FILE,DRIVER('Btrieve'),PRE(lev),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! User Levels
User_Level_Key           KEY(lev:User_Level),NOCASE,PRIMARY ! By User Level
Record                   RECORD,PRE()
User_Level                  STRING(30)                ! 
                         END
                     END                       

ALLLEVEL             FILE,DRIVER('Btrieve'),PRE(all),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! All Access Levels
Access_Level_Key         KEY(all:Access_Level),NOCASE,PRIMARY ! By Access Level
Record                   RECORD,PRE()
Access_Level                STRING(30)                ! 
Description                 STRING(500)               ! 
                         END
                     END                       

QAPARTSTEMP          FILE,DRIVER('Btrieve'),OEM,PRE(qap),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)             ! List of parts for QA Check
RecordNumberKey          KEY(qap:RecordNumber),NOCASE,PRIMARY ! By Record Number
PartNumberKey            KEY(qap:PartNumber),DUP,NOCASE ! By Part Number
DescriptionKey           KEY(qap:Description),DUP,NOCASE ! By Description
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
PartNumber                  STRING(30)                ! Part Number
Description                 STRING(30)                ! Description
PartRecordNumber            LONG                      ! Part Record Number
PartType                    STRING(3)                 ! Part Type
                         END
                     END                       

ACCAREAS             FILE,DRIVER('Btrieve'),PRE(acc),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Access Areas
Access_level_key         KEY(acc:User_Level,acc:Access_Area),NOCASE,PRIMARY ! By Access Level
AccessOnlyKey            KEY(acc:Access_Area),DUP,NOCASE ! 
Record                   RECORD,PRE()
Access_Area                 STRING(30)                ! 
User_Level                  STRING(30)                ! 
DummyField                  STRING(1)                 ! For File Manager
                         END
                     END                       

AUDIT                FILE,DRIVER('Btrieve'),PRE(aud),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                   ! Audit Trail
Ref_Number_Key           KEY(aud:Ref_Number,-aud:Date,-aud:Time,aud:Action),DUP,NOCASE ! By Date
Action_Key               KEY(aud:Ref_Number,aud:Action,-aud:Date),DUP,NOCASE ! By Action
User_Key                 KEY(aud:Ref_Number,aud:User,-aud:Date),DUP,NOCASE ! By User
Record_Number_Key        KEY(aud:record_number),NOCASE,PRIMARY ! 
ActionOnlyKey            KEY(aud:Action,aud:Date),DUP,NOCASE ! By Action
TypeRefKey               KEY(aud:Ref_Number,aud:Type,-aud:Date,-aud:Time,aud:Action),DUP,NOCASE ! By Date
TypeActionKey            KEY(aud:Ref_Number,aud:Type,aud:Action,-aud:Date),DUP,NOCASE ! By Action
TypeUserKey              KEY(aud:Ref_Number,aud:Type,aud:User,-aud:Date),DUP,NOCASE ! By User
DateActionJobKey         KEY(aud:Date,aud:Action,aud:Ref_Number),DUP,NOCASE ! By Job Number
DateJobKey               KEY(aud:Date,aud:Ref_Number),DUP,NOCASE ! By Job Number
DateTypeJobKey           KEY(aud:Date,aud:Type,aud:Ref_Number),DUP,NOCASE ! By Job Number
DateTypeActionKey        KEY(aud:Date,aud:Type,aud:Action,aud:Ref_Number),DUP,NOCASE ! By Job Number
Record                   RECORD,PRE()
record_number               REAL                      ! 
Ref_Number                  REAL                      ! 
Date                        DATE                      ! 
Time                        TIME                      ! 
User                        STRING(3)                 ! 
Action                      STRING(80)                ! 
DummyField                  STRING(2)                 ! For File Manager
Notes                       STRING(255)               ! 
Type                        STRING(3)                 ! Job/Exchange/Loan
                         END
                     END                       

USERS                FILE,DRIVER('Btrieve'),PRE(use),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                   ! Users
User_Code_Key            KEY(use:User_Code),NOCASE,PRIMARY ! By User Code
User_Type_Active_Key     KEY(use:User_Type,use:Active,use:Surname),DUP,NOCASE ! By Surname
Surname_Active_Key       KEY(use:Active,use:Surname),DUP,NOCASE ! By Surname
User_Code_Active_Key     KEY(use:Active,use:User_Code),DUP,NOCASE ! 
User_Type_Key            KEY(use:User_Type,use:Surname),DUP,NOCASE ! By Surname
surname_key              KEY(use:Surname),DUP,NOCASE  ! 
password_key             KEY(use:Password),NOCASE     ! 
Logged_In_Key            KEY(use:Logged_In,use:Surname),DUP,NOCASE ! By Name
Team_Surname             KEY(use:Team,use:Surname),DUP,NOCASE ! Surname
Active_Team_Surname      KEY(use:Team,use:Active,use:Surname),DUP,NOCASE ! By Surname
Record                   RECORD,PRE()
User_Code                   STRING(3)                 ! 
Forename                    STRING(30)                ! 
Surname                     STRING(30)                ! 
Password                    STRING(20)                ! 
User_Type                   STRING(15)                ! 
Job_Assignment              STRING(15)                ! 
Supervisor                  STRING(3)                 ! 
User_Level                  STRING(30)                ! 
Logged_In                   STRING(1)                 ! 
Logged_in_date              DATE                      ! 
Logged_In_Time              TIME                      ! 
Restrict_Logins             STRING(3)                 ! 
Concurrent_Logins           REAL                      ! 
Active                      STRING(3)                 ! 
Team                        STRING(30)                ! 
Location                    STRING(30)                ! 
Repair_Target               REAL                      ! 
SkillLevel                  LONG                      ! Skill Level
StockFromLocationOnly       BYTE                      ! Pick Stock From Users Location Only
EmailAddress                STRING(255)               ! Email Address
                         END
                     END                       

SIDREG               FILE,DRIVER('Btrieve'),PRE(srg),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! SID Service Centre Region Default Transit Days
SIDRegIDKey              KEY(srg:SIDRegID),NOCASE,PRIMARY ! 
SCRegionNameKey          KEY(srg:SCAccountID,srg:RegionName),DUP,NOCASE ! 
RegionNameKey            KEY(srg:RegionName),DUP,NOCASE ! 
Record                   RECORD,PRE()
SIDRegID                    LONG                      ! 
SCAccountID                 LONG                      ! 
RegionName                  STRING(30)                ! 
TransitDaysToSC             LONG                      ! Number of days to arrive at service centre from store
TransitDaysToStore          LONG                      ! Number of days to arrive at store from service centre
                         END
                     END                       

LOCSHELF             FILE,DRIVER('Btrieve'),PRE(los),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Locations - Shelf
Shelf_Location_Key       KEY(los:Site_Location,los:Shelf_Location),NOCASE,PRIMARY ! By Shelf Location
Record                   RECORD,PRE()
Site_Location               STRING(30)                ! 
Shelf_Location              STRING(30)                ! 
                         END
                     END                       

PENDMAIL             FILE,DRIVER('Btrieve'),OEM,PRE(pem),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! UTL Pending Email
RecordNumberKey          KEY(pem:RecordNumber),NOCASE,PRIMARY ! By Record Number
DateCreatedKey           KEY(-pem:DateCreated,-pem:TimeCreated),DUP,NOCASE ! By Date Created
DateMessageKey           KEY(pem:MessageType,-pem:DateCreated,-pem:TimeCreated),DUP,NOCASE ! By Date Created
SMSSentKey               KEY(pem:SMSEmail,pem:DateSent),DUP,NOCASE ! By Date Sent
MessageRefKey            KEY(pem:MessageType,pem:RefNumber),DUP,NOCASE ! By Message Type, Ref Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
DateCreated                 DATE                      ! Date
TimeCreated                 TIME                      ! Time Created
MessageType                 STRING(3)                 ! Message Type
SMSEmail                    STRING(5)                 ! SMS or Email
RefNumber                   LONG                      ! Link To JOBS Ref Number
DateToSend                  DATE                      ! Date To Send
TimeToSend                  TIME                      ! Time To Send
DateSent                    DATE                      ! Date Sent
TimeSent                    TIME                      ! Time Sent
ErrorMessage                STRING(255)               ! Error Message
MobileNumber                STRING(30)                ! Mobile Number
EmailAddress                STRING(255)               ! Email Address
Dummy                       BYTE                      ! 
                         END
                     END                       

DEFAULTS             FILE,DRIVER('Btrieve'),PRE(def),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Defaults
RecordNumberKey          KEY(def:record_number),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
User_Name                   STRING(30)                ! 
record_number               REAL                      ! 
Version_Number              STRING(10)                ! 
Address_Line1               STRING(30)                ! 
Address_Line2               STRING(30)                ! 
Address_Line3               STRING(30)                ! 
Postcode                    STRING(15)                ! 
Telephone_Number            STRING(15)                ! 
Fax_Number                  STRING(15)                ! 
EmailAddress                STRING(255)               ! Email Address
VAT_Number                  STRING(30)                ! 
Use_Invoice_Address         STRING(3)                 ! 
Use_For_Order               STRING(3)                 ! 
Invoice_Company_Name        STRING(30)                ! 
Invoice_Address_Line1       STRING(30)                ! 
Invoice_Address_Line2       STRING(30)                ! 
Invoice_Address_Line3       STRING(30)                ! 
Invoice_Postcode            STRING(15)                ! 
Invoice_Telephone_Number    STRING(15)                ! 
Invoice_Fax_Number          STRING(15)                ! 
InvoiceEmailAddress         STRING(255)               ! Email Address
Invoice_VAT_Number          STRING(30)                ! 
OrderCompanyName            STRING(30)                ! Company Name
OrderAddressLine1           STRING(30)                ! Address
OrderAddressLine2           STRING(30)                ! Address
OrderAddressLine3           STRING(30)                ! Address
OrderPostcode               STRING(30)                ! Postcode
OrderTelephoneNumber        STRING(30)                ! Telephone Number
OrderFaxNumber              STRING(30)                ! Fax Number
OrderEmailAddress           STRING(255)               ! Email Address
Use_Postcode                STRING(3)                 ! 
Postcode_Path               STRING(255)               ! 
PostcodeDll                 STRING(3)                 ! Use DLL Version Of Postcode
Vat_Rate_Labour             STRING(2)                 ! 
Vat_Rate_Parts              STRING(2)                 ! 
License_Number              STRING(10)                ! 
Maximum_Users               REAL                      ! 
Warranty_Period             REAL                      ! 
Automatic_Replicate         STRING(3)                 ! 
Automatic_Replicate_Field   STRING(15)                ! 
Estimate_If_Over            REAL                      ! 
Start_Work_Hours            TIME                      ! 
End_Work_Hours              TIME                      ! 
Include_Saturday            STRING(3)                 ! 
Include_Sunday              STRING(3)                 ! 
Show_Mobile_Number          STRING(3)                 ! 
Show_Loan_Exchange_Details  STRING(3)                 ! 
Use_Credit_Limit            STRING(3)                 ! 
Hide_Physical_Damage        STRING(3)                 ! 
Hide_Insurance              STRING(3)                 ! 
Hide_Authority_Number       STRING(3)                 ! 
Hide_Advance_Payment        STRING(3)                 ! 
HideColour                  STRING(3)                 ! 
HideInCourier               STRING(3)                 ! Hide Incoming Courier
HideIntFault                STRING(3)                 ! Hide Intermittant Fault
HideProduct                 STRING(3)                 ! Hide Product Code
HideLocation                BYTE                      ! Hide Internal Location
Allow_Bouncer               STRING(3)                 ! 
Job_Batch_Number            REAL                      ! 
QA_Required                 STRING(3)                 ! 
QA_Before_Complete          STRING(3)                 ! 
QAPreliminary               STRING(3)                 ! Use Preliminary QA Check
QAExchLoan                  STRING(3)                 ! QA Required For Exchange/Loan Units Before Despatch
CompleteAtQA                BYTE                      ! Complete At QA
RapidQA                     STRING(3)                 ! Use Rapid QA
ValidateESN                 STRING(3)                 ! Validate ESN Before Rapid Job Update
ValidateDesp                STRING(3)                 ! Validate ESN At Despatch
Force_Accessory_Check       STRING(3)                 ! 
Force_Initial_Transit_Type  STRING(1)                 ! 
Force_Mobile_Number         STRING(1)                 ! 
Force_Model_Number          STRING(1)                 ! 
Force_Unit_Type             STRING(1)                 ! 
Force_Fault_Description     STRING(1)                 ! 
Force_ESN                   STRING(1)                 ! 
Force_MSN                   STRING(1)                 ! 
Force_Job_Type              STRING(1)                 ! 
Force_Engineer              STRING(1)                 ! 
Force_Invoice_Text          STRING(1)                 ! 
Force_Repair_Type           STRING(1)                 ! 
Force_Authority_Number      STRING(1)                 ! 
Force_Fault_Coding          STRING(1)                 ! 
Force_Spares                STRING(1)                 ! 
Force_Incoming_Courier      STRING(1)                 ! 
Force_Outoing_Courier       STRING(1)                 ! 
Force_DOP                   STRING(1)                 ! 
Order_Number                STRING(1)                 ! 
Customer_Name               STRING(1)                 ! 
ForcePostcode               STRING(1)                 ! 
ForceDelPostcode            STRING(1)                 ! 
ForceCommonFault            STRING(1)                 ! Force Common Fault
Remove_Backgrounds          STRING(3)                 ! 
Use_Loan_Exchange_Label     STRING(3)                 ! 
Use_Job_Label               STRING(3)                 ! 
UseSmallLabel               BYTE                      ! Use Small Label
Job_Label                   STRING(255)               ! 
add_stock_label             STRING(3)                 ! 
receive_stock_label         STRING(3)                 ! 
QA_Failed_Label             STRING(3)                 ! 
QA_Failed_Report            STRING(3)                 ! 
QAPassLabel                 BYTE                      ! Print QA Pass Label
ThirdPartyNote              STRING(3)                 ! Print Single 3rd Party Despatch Note
Vodafone_Import_Path        STRING(255)               ! 
Label_Printer_Type          STRING(30)                ! 
Browse_Option               STRING(3)                 ! Which Address To Show On Main Browse
ANCCollectionNo             LONG                      ! Next ANC Collection Number
Use_Sage                    STRING(3)                 ! 
Global_Nominal_Code         STRING(30)                ! 
Parts_Stock_Code            STRING(30)                ! 
Labour_Stock_Code           STRING(30)                ! 
Courier_Stock_Code          STRING(30)                ! 
Parts_Description           STRING(60)                ! 
Labour_Description          STRING(60)                ! 
Courier_Description         STRING(60)                ! 
Parts_Code                  STRING(30)                ! 
Labour_Code                 STRING(30)                ! 
Courier_Code                STRING(30)                ! 
User_Name_Sage              STRING(30)                ! 
Password_Sage               STRING(30)                ! 
Company_Number_Sage         STRING(30)                ! 
Path_Sage                   STRING(255)               ! 
BouncerTime                 LONG                      ! Bouncer Time
ExportPath                  STRING(255)               ! Default Export Path
PrintBarcode                BYTE                      ! Print Barcode
ChaChargeType               STRING(30)                ! Chargeable Charge Type
WarChargeType               STRING(30)                ! Warranty Charge Type
RetBackOrders               BYTE                      ! Retail Back Orders
RemoveWorkshopDespatch      BYTE                      ! Remove From Workshop On Despatch
SummaryOrders               BYTE                      ! Summary Parts Orders
EuroRate                    REAL                      ! Euro Exchange Rate
IrishVatRate                REAL                      ! Irish V.A.T. Rate
EmailServerAddress          STRING(255)               ! Email Server Address
EmailServerPort             LONG                      ! Email Service Port Number
Job_Label_Accessories       STRING(3)                 ! 
ShowRepTypeCosts            BYTE                      ! Show Repair Type Costs
PickNoteNormal              BYTE                      ! 
PickNoteMainStore           BYTE                      ! 
PickNoteChangeStatus        BYTE                      ! 
PickNoteJobStatus           STRING(30)                ! 
TeamPerformanceTicker       BYTE                      ! 
TickerRefreshRate           SHORT                     ! 
UseOriginalBookingTemplate  BYTE                      ! 
                         END
                     END                       

REPTYCAT             FILE,DRIVER('Btrieve'),OEM,PRE(repc),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Repair Type Categories
RecordNumberKey          KEY(repc:RecordNumber),NOCASE,PRIMARY ! By Record Number
RepairTypeKey            KEY(repc:RepairType),DUP,NOCASE ! By Repair Type
CategoryKey              KEY(repc:Category,repc:RepairType),DUP,NOCASE ! By Repair Type
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
RepairType                  STRING(30)                ! Repair Type
Category                    STRING(30)                ! Category
                         END
                     END                       

REPTYPETEMP          FILE,DRIVER('Btrieve'),OEM,PRE(rept),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)             ! 
RecordNumberKey          KEY(rept:RecordNumber),NOCASE,PRIMARY ! By Record Number
RepairTypeKey            KEY(rept:RepairType),DUP,NOCASE ! By Repair Type
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
RepairType                  STRING(30)                ! Repair Type
Cost                        REAL                      ! Cost
                         END
                     END                       

MANFPALO             FILE,DRIVER('Btrieve'),PRE(mfp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Manufacturer Fault Coding Lookup Table
Field_Key                KEY(mfp:Manufacturer,mfp:Field_Number,mfp:Field),NOCASE,PRIMARY ! By Field
DescriptionKey           KEY(mfp:Manufacturer,mfp:Field_Number,mfp:Description),DUP,NOCASE ! By Description.
Record                   RECORD,PRE()
Manufacturer                STRING(30)                ! 
Field_Number                REAL                      ! 
Field                       STRING(30)                ! 
Description                 STRING(60)                ! 
                         END
                     END                       

JOBSTAGE             FILE,DRIVER('Btrieve'),PRE(jst),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Job Stage
Ref_Number_Key           KEY(jst:Ref_Number,-jst:Date),DUP,NOCASE ! By Ref Number
Job_Stage_Key            KEY(jst:Ref_Number,jst:Job_Stage),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
Ref_Number                  REAL                      ! 
Job_Stage                   STRING(30)                ! 
Date                        DATE                      ! 
Time                        TIME                      ! 
User                        STRING(3)                 ! 
                         END
                     END                       

MANFAUPA             FILE,DRIVER('Btrieve'),PRE(map),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Manufacturer Fault Coding on Parts
Field_Number_Key         KEY(map:Manufacturer,map:Field_Number),NOCASE,PRIMARY ! By Field Number
Record                   RECORD,PRE()
Manufacturer                STRING(30)                ! 
Field_Number                REAL                      ! Fault Code Field Number
Field_Name                  STRING(30)                ! 
Field_Type                  STRING(30)                ! 
Lookup                      STRING(3)                 ! 
Force_Lookup                STRING(3)                 ! 
Compulsory                  STRING(3)                 ! 
RestrictLength              BYTE                      ! Restrict Length
LengthFrom                  LONG                      ! Length From
LengthTo                    LONG                      ! Length To
ForceFormat                 BYTE                      ! Force Format
FieldFormat                 STRING(30)                ! Field Format
DateType                    STRING(30)                ! Date Type
                         END
                     END                       

EXPPARTS             FILE,DRIVER('BASIC'),PRE(exppar),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Export - Parts
Record                   RECORD,PRE()
Ref_Number                  STRING(20)                ! 
Part_Details_Group          GROUP                     ! 
Part_Number                   STRING(30)              ! 
Description                   STRING(30)              ! 
Supplier                      STRING(30)              ! 
Purchase_Cost                 STRING(20)              ! 
Sale_Cost                     STRING(20)              ! 
                            END                       ! 
Quantity                    STRING(20)                ! 
Exclude_From_Order          STRING(20)                ! 
Despatch_Note_Number        STRING(30)                ! 
Date_Ordered                STRING(20)                ! 
Order_Number                STRING(20)                ! 
Date_Received               STRING(20)                ! 
Fault_Codes1                GROUP                     ! 
Fault_Code1                   STRING(30)              ! 
Fault_Code2                   STRING(30)              ! 
Fault_Code3                   STRING(30)              ! 
Fault_Code4                   STRING(30)              ! 
Fault_Code5                   STRING(30)              ! 
Fault_Code6                   STRING(30)              ! 
                            END                       ! 
Fault_Codes2                GROUP                     ! 
Fault_Code7                   STRING(30)              ! 
Fault_Code8                   STRING(30)              ! 
Fault_Code9                   STRING(30)              ! 
Fault_Code10                  STRING(30)              ! 
Fault_Code11                  STRING(30)              ! 
Fault_Code12                  STRING(30)              ! 
                            END                       ! 
                         END
                     END                       

REPEXTRP             FILE,DRIVER('Btrieve'),OEM,PRE(rex),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! External Reports
RecordNumberKey          KEY(rex:RecordNumber),NOCASE,PRIMARY ! By Record Number
ReportNameKey            KEY(rex:ReportType,rex:ReportName),NOCASE ! By Report Name
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
ReportType                  STRING(80)                ! Report Type
ReportName                  STRING(80)                ! Report Type
EXEName                     STRING(30)                ! EXEName
                         END
                     END                       

GENSHORT             FILE,DRIVER('Btrieve'),PRE(gens),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! 
AutoNumber_Key           KEY(gens:Autonumber_Field),NOCASE,PRIMARY ! 
Lock_Down_Key            KEY(gens:Stock_Ref_No,gens:Audit_No),DUP,NOCASE ! 
Record                   RECORD,PRE()
Autonumber_Field            LONG                      ! 
branch_id                   LONG                      ! 
Audit_No                    LONG                      ! 
Stock_Ref_No                LONG                      ! 
Stock_Qty                   LONG                      ! 
                         END
                     END                       

LOCINTER             FILE,DRIVER('Btrieve'),PRE(loi),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Locations - Internal
Location_Key             KEY(loi:Location),NOCASE,PRIMARY ! By Location
Location_Available_Key   KEY(loi:Location_Available,loi:Location),DUP,NOCASE ! By Location
Record                   RECORD,PRE()
Location                    STRING(30)                ! 
Location_Available          STRING(3)                 ! 
Allocate_Spaces             STRING(3)                 ! 
Total_Spaces                REAL                      ! 
Current_Spaces              STRING(6)                 ! 
                         END
                     END                       

STAHEAD              FILE,DRIVER('Btrieve'),PRE(sth),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Status Type Heading
Heading_Key              KEY(sth:Ref_Number,sth:Heading),NOCASE,PRIMARY ! By Heading
Ref_Number_Key           KEY(sth:Ref_Number),DUP,NOCASE ! 
Record                   RECORD,PRE()
Ref_Number                  REAL                      ! 
Heading                     STRING(30)                ! 
                         END
                     END                       

SIDEXUPD             FILE,DRIVER('Btrieve'),PRE(seu),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! SID Extract Updates
SBJobNoKey               KEY(seu:SBJobNo),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
SBJobNo                     LONG                      ! ServiceBase Job Number
                         END
                     END                       

SIDSRVCN             FILE,DRIVER('Btrieve'),PRE(srv),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! SID Service Centres
SCAccountIDKey           KEY(srv:SCAccountID),NOCASE,PRIMARY ! 
ServiceCentreNameKey     KEY(srv:ServiceCentreName),DUP,NOCASE ! 
Record                   RECORD,PRE()
SCAccountID                 LONG                      ! 
ServiceCentreName           STRING(30)                ! 
AddressLine1                STRING(30)                ! 
AddressLine2                STRING(30)                ! 
AddressLine3                STRING(30)                ! 
Postcode                    STRING(15)                ! 
AccountActive               BYTE                      ! 
TurnaroundDaysRetail        LONG                      ! 
TurnaroundDaysExch          LONG                      ! 
BankHolidaysFileName        STRING(100)               ! 
ContactName                 STRING(30)                ! Contact Name
ContactNumber               STRING(30)                ! Contact Number
ContactEmail                STRING(255)               ! Contact Email
UseServiceCentreFTP         BYTE                      ! 
FTPServer                   STRING(30)                ! FTP Server
FTPUsername                 STRING(30)                ! FTP Username
FTPPassword                 STRING(30)                ! FTP Password
FTPPath                     STRING(255)               ! FTP Path
EmailToAddress              STRING(255)               ! FTP Email Alert Recipients
ProcessFailureCount         LONG                      ! Send Alert Email After (xx) Successive Process Failures
FTPFailedCount              LONG                      ! 
MonRepairCapacity           LONG                      ! 
TueRepairCapacity           LONG                      ! 
WedRepairCapacity           LONG                      ! 
ThuRepairCapacity           LONG                      ! 
FriRepairCapacity           LONG                      ! 
ExchangeImportType          BYTE                      ! 0 - Not Set, 1 - Model Code, 2 - Oracle Code
                         END
                     END                       

SIDMODSC             FILE,DRIVER('Btrieve'),PRE(msc),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! SID Models To Service Centre
RecordNumberKey          KEY(msc:RecordNumber),NOCASE,PRIMARY ! 
ManModelTypeKey          KEY(msc:Manufacturer,msc:ModelNo,msc:BookingType),NOCASE ! 
SCAccountIDKey           KEY(msc:SCAccountID),DUP,NOCASE ! 
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
Manufacturer                STRING(30)                ! 
ModelNo                     STRING(30)                ! 
BookingType                 STRING(1)                 ! 
SCAccountID                 LONG                      ! 
                         END
                     END                       

SIDNWDAY             FILE,DRIVER('Btrieve'),PRE(nwd),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! SID Non Working Days
RecordNumberKey          KEY(nwd:RecordNumber),NOCASE,PRIMARY ! 
SCBookTypeDayKey         KEY(nwd:SCAccountID,nwd:BookingType,nwd:NonWorkingDay),DUP,NOCASE ! 
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
SCAccountID                 LONG                      ! 
BookingType                 STRING(1)                 ! 
NonWorkingDay               DATE                      ! 
                         END
                     END                       

NOTESINV             FILE,DRIVER('Btrieve'),PRE(noi),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Notes - Invoice
Notes_Key                KEY(noi:Reference,noi:Notes),NOCASE,PRIMARY ! By Notes
Record                   RECORD,PRE()
Reference                   STRING(30)                ! 
Notes                       STRING(80)                ! 
                         END
                     END                       

NOTESFAU             FILE,DRIVER('Btrieve'),PRE(nof),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Notes - Fault Description
Notes_Key                KEY(nof:Manufacturer,nof:Reference,nof:Notes),NOCASE,PRIMARY ! By Notes
Record                   RECORD,PRE()
Manufacturer                STRING(30)                ! 
Reference                   STRING(30)                ! 
Notes                       STRING(80)                ! 
                         END
                     END                       

MANFAULO             FILE,DRIVER('Btrieve'),PRE(mfo),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Manufacturer Fault Coding Lookup Table O
Field_Key                KEY(mfo:Manufacturer,mfo:Field_Number,mfo:Field),NOCASE,PRIMARY ! By Field
DescriptionKey           KEY(mfo:Manufacturer,mfo:Field_Number,mfo:Description),DUP,NOCASE ! By Description
Record                   RECORD,PRE()
Manufacturer                STRING(30)                ! 
Field_Number                REAL                      ! 
Field                       STRING(30)                ! 
Description                 STRING(60)                ! 
                         END
                     END                       

STATREP              FILE,DRIVER('Btrieve'),OEM,PRE(star),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Status Report Title
RecordNumberKey          KEY(star:RecordNumber),NOCASE,PRIMARY ! By Record Number
DescriptionKey           KEY(star:Description),NOCASE ! By Description
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
Description                 STRING(60)                ! Description
                         END
                     END                       

JOBEXHIS             FILE,DRIVER('Btrieve'),PRE(jxh),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Job Exchange History
Ref_Number_Key           KEY(jxh:Ref_Number,jxh:Date,jxh:Time),DUP,NOCASE ! By Date
record_number_key        KEY(jxh:record_number),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
Ref_Number                  REAL                      ! 
record_number               REAL                      ! 
Loan_Unit_Number            REAL                      ! 
Date                        DATE                      ! 
Time                        TIME                      ! 
User                        STRING(3)                 ! 
Status                      STRING(60)                ! 
                         END
                     END                       

STARECIP             FILE,DRIVER('Btrieve'),OEM,PRE(str),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Status Recipients
RecordNumberKey          KEY(str:RecordNumber),NOCASE,PRIMARY ! By Record Number
RecipientTypeKey         KEY(str:RefNumber,str:RecipientType),NOCASE ! By Recipient Type
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
RefNumber                   LONG                      ! Ref Number
RecipientType               STRING(30)                ! Recipient Type
                         END
                     END                       

RETSTOCK             FILE,DRIVER('Btrieve'),OEM,PRE(res),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Retail Stock
Record_Number_Key        KEY(res:Record_Number),NOCASE,PRIMARY ! By Record Number
Part_Number_Key          KEY(res:Ref_Number,res:Part_Number),DUP,NOCASE ! By Part Number
Despatched_Key           KEY(res:Ref_Number,res:Despatched,res:Despatch_Note_Number,res:Part_Number),DUP,NOCASE ! By Part Number
Despatched_Only_Key      KEY(res:Ref_Number,res:Despatched),DUP,NOCASE ! By Despatched
Pending_Ref_Number_Key   KEY(res:Ref_Number,res:Pending_Ref_Number),DUP,NOCASE ! 
Order_Part_Key           KEY(res:Ref_Number,res:Order_Part_Number),DUP,NOCASE ! 
Order_Number_Key         KEY(res:Ref_Number,res:Order_Number),DUP,NOCASE ! 
DespatchedKey            KEY(res:Despatched),DUP,NOCASE ! By Despatched
DespatchedPartKey        KEY(res:Ref_Number,res:Despatched,res:Part_Number),DUP,NOCASE ! By Part Number
Record                   RECORD,PRE()
Record_Number               LONG                      ! Record Number
Ref_Number                  REAL                      ! 
Part_Number                 STRING(30)                ! 
Description                 STRING(30)                ! 
Supplier                    STRING(30)                ! 
Purchase_Cost               REAL                      ! 
Sale_Cost                   REAL                      ! 
Retail_Cost                 REAL                      ! 
AccessoryCost               REAL                      ! Accesory Cost
Item_Cost                   REAL                      ! 
Quantity                    REAL                      ! 
Despatched                  STRING(20)                ! 
Order_Number                REAL                      ! 
Date_Ordered                DATE                      ! 
Date_Received               DATE                      ! 
Despatch_Note_Number        REAL                      ! 
Despatch_Date               DATE                      ! 
Part_Ref_Number             REAL                      ! 
Pending_Ref_Number          REAL                      ! 
Order_Part_Number           REAL                      ! 
Purchase_Order_Number       STRING(30)                ! 
Previous_Sale_Number        LONG                      ! 
InvoicePart                 LONG                      ! Link to Record Number of Invoice(Credit) Part
Credit                      STRING(3)                 ! Has this part been credited?
                         END
                     END                       

RETPAY               FILE,DRIVER('Btrieve'),OEM,PRE(rtp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! Retail Sales Payment
Record_Number_Key        KEY(rtp:Record_Number),NOCASE,PRIMARY ! By Record Number
Date_Key                 KEY(rtp:Ref_Number,rtp:Date,rtp:Payment_Type),DUP,NOCASE ! By Date
Record                   RECORD,PRE()
Record_Number               LONG                      ! 
Ref_Number                  REAL                      ! 
Date                        DATE                      ! 
Payment_Type                STRING(30)                ! 
Credit_Card_Number          STRING(20)                ! 
Expiry_Date                 STRING(5)                 ! 
Issue_Number                STRING(5)                 ! 
Amount                      REAL                      ! 
User_Code                   STRING(3)                 ! 
Authorisation_Code          STRING(30)                ! 
Card_Holder                 STRING(30)                ! 
                         END
                     END                       

RETDESNO             FILE,DRIVER('Btrieve'),OEM,PRE(rdn),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Retail Despatch Number File
Despatch_Number_Key      KEY(rdn:Despatch_Number),NOCASE,PRIMARY ! By Despatch Number
Record                   RECORD,PRE()
Despatch_Number             LONG                      ! 
Date                        DATE                      ! 
Time                        TIME                      ! 
Consignment_Number          STRING(30)                ! 
Courier                     STRING(30)                ! 
Sale_Number                 LONG                      ! 
                         END
                     END                       

ORDPARTS             FILE,DRIVER('Btrieve'),PRE(orp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Orders - Parts Attached
Order_Number_Key         KEY(orp:Order_Number,orp:Part_Ref_Number),DUP,NOCASE ! By Order Number
record_number_key        KEY(orp:Record_Number),NOCASE,PRIMARY ! 
Ref_Number_Key           KEY(orp:Part_Ref_Number,orp:Part_Number,orp:Description),DUP,NOCASE ! By Ref Number
Part_Number_Key          KEY(orp:Order_Number,orp:Part_Number,orp:Description),DUP,NOCASE ! By Part Number
Description_Key          KEY(orp:Order_Number,orp:Description,orp:Part_Number),DUP,NOCASE ! By Description
Received_Part_Number_Key KEY(orp:All_Received,orp:Part_Number),DUP,NOCASE ! By Part Number
Account_Number_Key       KEY(orp:Account_Number,orp:Part_Type,orp:All_Received,orp:Part_Number),DUP,NOCASE ! 
Allocated_Key            KEY(orp:Part_Type,orp:All_Received,orp:Allocated_To_Sale,orp:Part_Number),DUP,NOCASE ! 
Allocated_Account_Key    KEY(orp:Part_Type,orp:All_Received,orp:Allocated_To_Sale,orp:Account_Number,orp:Part_Number),DUP,NOCASE ! 
Order_Type_Received      KEY(orp:Order_Number,orp:Part_Type,orp:All_Received,orp:Part_Number,orp:Description),DUP,NOCASE ! By Part Number
PartRecordNumberKey      KEY(orp:PartRecordNumber),DUP,NOCASE ! By Record Number
Record                   RECORD,PRE()
Order_Number                LONG                      ! Order Number
Record_Number               LONG                      ! Record Number
Part_Ref_Number             LONG                      ! Part Ref Number
Quantity                    LONG                      ! Quantity
Part_Number                 STRING(30)                ! 
Description                 STRING(30)                ! 
Purchase_Cost               REAL                      ! 
Sale_Cost                   REAL                      ! 
Retail_Cost                 REAL                      ! 
Job_Number                  LONG                      ! Job Number
Part_Type                   STRING(3)                 ! 
Number_Received             LONG                      ! Number Received
Date_Received               DATE                      ! 
All_Received                STRING(3)                 ! 
Allocated_To_Sale           STRING(3)                 ! 
Account_Number              STRING(15)                ! 
DespatchNoteNumber          STRING(30)                ! Despatch Note Number
Reason                      STRING(255)               ! Reason
PartRecordNumber            LONG                      ! Part Record Number
                         END
                     END                       

STOCKMIN             FILE,DRIVER('Btrieve'),OEM,PRE(smin),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Minimum Stock File
RecordNumberKey          KEY(smin:RecordNumber),NOCASE,PRIMARY ! By Record Number
LocationPartNoKey        KEY(smin:Location,smin:PartNumber),DUP,NOCASE ! By Part Number
LocationDescriptionKey   KEY(smin:Location,smin:Description),DUP,NOCASE ! By Description
LocationSuppPartKey      KEY(smin:Location,smin:Supplier,smin:PartNumber),DUP,NOCASE ! By Part Number
LocationSuppDescKey      KEY(smin:Location,smin:Supplier,smin:Description),DUP,NOCASE ! By Description
PartRefNumberKey         KEY(smin:PartRefNumber),DUP,NOCASE ! By Part Ref Number
PartNumberKey            KEY(smin:PartNumber),DUP,NOCASE ! By Part Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
Location                    STRING(30)                ! Location
PartNumber                  STRING(30)                ! Part Number
Description                 STRING(20)                ! Description
Supplier                    STRING(30)                ! Supplier
Usage                       LONG                      ! Usage
MinLevel                    LONG                      ! Minimum Level
QuantityOnOrder             LONG                      ! Quantity On Order
QuantityInStock             LONG                      ! Quantity In Stock
QuantityRequired            LONG                      ! Quantity Required
PartRefNumber               LONG                      ! Part Ref number
QuantityJobsPending         LONG                      ! Jobs Pending Parts
                         END
                     END                       

LOCATION             FILE,DRIVER('Btrieve'),PRE(loc),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Locations - Site
RecordNumberKey          KEY(loc:RecordNumber),NOCASE,PRIMARY ! By Record Number
Location_Key             KEY(loc:Location),NOCASE     ! By Location
Main_Store_Key           KEY(loc:Main_Store,loc:Location),DUP,NOCASE ! By Location
ActiveLocationKey        KEY(loc:Active,loc:Location),DUP,NOCASE ! By Location
ActiveMainStoreKey       KEY(loc:Active,loc:Main_Store,loc:Location),DUP,NOCASE ! By Location
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
Location                    STRING(30)                ! 
Main_Store                  STRING(3)                 ! 
Active                      BYTE                      ! Active
PickNoteEnable              BYTE                      ! 
                         END
                     END                       

WPARTTMP             FILE,DRIVER('Btrieve'),PRE(wartmp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Temporary Parts
Part_Number_Key          KEY(wartmp:Ref_Number,wartmp:Part_Number),DUP,NOCASE ! By Part Number
record_number_key        KEY(wartmp:record_number),NOCASE,PRIMARY ! 
Part_Ref_Number2_Key     KEY(wartmp:Part_Ref_Number),DUP,NOCASE ! By Part_Ref_Number
Date_Ordered_Key         KEY(wartmp:Date_Ordered),DUP,NOCASE ! By Date Ordered
Part_Ref_Number_Key      KEY(wartmp:Ref_Number,wartmp:Part_Ref_Number),DUP,NOCASE ! By Part Ref Number
Pending_Ref_Number_Key   KEY(wartmp:Ref_Number,wartmp:Pending_Ref_Number),DUP,NOCASE ! 
Order_Number_Key         KEY(wartmp:Ref_Number,wartmp:Order_Number),DUP,NOCASE ! 
Supplier_Key             KEY(wartmp:Supplier),DUP,NOCASE ! By Supplier
Record                   RECORD,PRE()
Ref_Number                  REAL                      ! 
record_number               REAL                      ! 
Adjustment                  STRING(3)                 ! 
Part_Ref_Number             REAL                      ! 
Part_Details_Group          GROUP                     ! 
Part_Number                   STRING(30)              ! 
Description                   STRING(30)              ! 
Supplier                      STRING(30)              ! 
Purchase_Cost                 REAL                    ! 
Sale_Cost                     REAL                    ! 
Retail_Cost                   REAL                    ! 
                            END                       ! 
Quantity                    REAL                      ! 
Warranty_Part               STRING(3)                 ! 
Exclude_From_Order          STRING(3)                 ! 
Despatch_Note_Number        STRING(30)                ! 
Date_Ordered                DATE                      ! 
Pending_Ref_Number          REAL                      ! 
Order_Number                REAL                      ! 
Main_Part                   STRING(3)                 ! 
Fault_Codes1                GROUP                     ! 
Fault_Code1                   STRING(30)              ! 
Fault_Code2                   STRING(30)              ! 
Fault_Code3                   STRING(30)              ! 
Fault_Code4                   STRING(30)              ! 
Fault_Code5                   STRING(30)              ! 
Fault_Code6                   STRING(30)              ! 
                            END                       ! 
Fault_Codes2                GROUP                     ! 
Fault_Code7                   STRING(30)              ! 
Fault_Code8                   STRING(30)              ! 
Fault_Code9                   STRING(30)              ! 
Fault_Code10                  STRING(30)              ! 
Fault_Code11                  STRING(30)              ! 
Fault_Code12                  STRING(30)              ! 
                            END                       ! 
                         END
                     END                       

PARTSTMP             FILE,DRIVER('Btrieve'),PRE(partmp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Temporary Parts
Part_Number_Key          KEY(partmp:Ref_Number,partmp:Part_Number),DUP,NOCASE ! By Part Number
record_number_key        KEY(partmp:record_number),NOCASE,PRIMARY ! 
Part_Ref_Number2_Key     KEY(partmp:Part_Ref_Number),DUP,NOCASE ! By Part_Ref_Number
Date_Ordered_Key         KEY(partmp:Date_Ordered),DUP,NOCASE ! By Date Ordered
Part_Ref_Number_Key      KEY(partmp:Ref_Number,partmp:Part_Ref_Number),DUP,NOCASE ! By Part Ref Number
Pending_Ref_Number_Key   KEY(partmp:Ref_Number,partmp:Pending_Ref_Number),DUP,NOCASE ! 
Order_Number_Key         KEY(partmp:Ref_Number,partmp:Order_Number),DUP,NOCASE ! 
Supplier_Key             KEY(partmp:Supplier),DUP,NOCASE ! By Supplier
Record                   RECORD,PRE()
Ref_Number                  REAL                      ! 
record_number               REAL                      ! 
Adjustment                  STRING(3)                 ! 
Part_Ref_Number             REAL                      ! 
Part_Details_Group          GROUP                     ! 
Part_Number                   STRING(30)              ! 
Description                   STRING(30)              ! 
Supplier                      STRING(30)              ! 
Purchase_Cost                 REAL                    ! 
Sale_Cost                     REAL                    ! 
Retail_Cost                   REAL                    ! 
                            END                       ! 
Quantity                    REAL                      ! 
Warranty_Part               STRING(3)                 ! 
Exclude_From_Order          STRING(3)                 ! 
Despatch_Note_Number        STRING(30)                ! 
Date_Ordered                DATE                      ! 
Pending_Ref_Number          REAL                      ! 
Order_Number                REAL                      ! 
Fault_Codes1                GROUP                     ! 
Fault_Code1                   STRING(30)              ! 
Fault_Code2                   STRING(30)              ! 
Fault_Code3                   STRING(30)              ! 
Fault_Code4                   STRING(30)              ! 
Fault_Code5                   STRING(30)              ! 
Fault_Code6                   STRING(30)              ! 
                            END                       ! 
Fault_Codes2                GROUP                     ! 
Fault_Code7                   STRING(30)              ! 
Fault_Code8                   STRING(30)              ! 
Fault_Code9                   STRING(30)              ! 
Fault_Code10                  STRING(30)              ! 
Fault_Code11                  STRING(30)              ! 
Fault_Code12                  STRING(30)              ! 
                            END                       ! 
                         END
                     END                       

EXCHANGE             FILE,DRIVER('Btrieve'),PRE(xch),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Exchange units
Ref_Number_Key           KEY(xch:Ref_Number),NOCASE,PRIMARY ! By Loan Unit Number
ESN_Only_Key             KEY(xch:ESN),DUP,NOCASE      ! 
MSN_Only_Key             KEY(xch:MSN),DUP,NOCASE      ! 
Ref_Number_Stock_Key     KEY(xch:Stock_Type,xch:Ref_Number),DUP,NOCASE ! By Ref Number
Model_Number_Key         KEY(xch:Stock_Type,xch:Model_Number,xch:Ref_Number),DUP,NOCASE ! By Model Number
ESN_Key                  KEY(xch:Stock_Type,xch:ESN),DUP,NOCASE ! By E.S.N. / I.M.E.I. Number
MSN_Key                  KEY(xch:Stock_Type,xch:MSN),DUP,NOCASE ! By M.S.N. Number
ESN_Available_Key        KEY(xch:Available,xch:Stock_Type,xch:ESN),DUP,NOCASE ! By E.S.N. / I.M.E.I.
MSN_Available_Key        KEY(xch:Available,xch:Stock_Type,xch:MSN),DUP,NOCASE ! By M.S.N.
Ref_Available_Key        KEY(xch:Available,xch:Stock_Type,xch:Ref_Number),DUP,NOCASE ! By Exchange Unit Number
Model_Available_Key      KEY(xch:Available,xch:Stock_Type,xch:Model_Number,xch:Ref_Number),DUP,NOCASE ! By Model Number
Stock_Type_Key           KEY(xch:Stock_Type),DUP,NOCASE ! By Stock Type
ModelRefNoKey            KEY(xch:Model_Number,xch:Ref_Number),DUP,NOCASE ! By Unit Number
AvailIMEIOnlyKey         KEY(xch:Available,xch:ESN),DUP,NOCASE ! By I.M.E.I. Number
AvailMSNOnlyKey          KEY(xch:Available,xch:MSN),DUP,NOCASE ! By M.S.N.
AvailRefOnlyKey          KEY(xch:Available,xch:Ref_Number),DUP,NOCASE ! By Unit Number
AvailModOnlyKey          KEY(xch:Available,xch:Model_Number,xch:Ref_Number),DUP,NOCASE ! By Unit Number
StockBookedKey           KEY(xch:Stock_Type,xch:Model_Number,xch:Date_Booked,xch:Ref_Number),DUP,NOCASE ! By Unit Number
AvailBookedKey           KEY(xch:Available,xch:Stock_Type,xch:Model_Number,xch:Date_Booked,xch:Ref_Number),DUP,NOCASE ! By Unit Number
DateBookedKey            KEY(xch:Date_Booked),DUP,NOCASE ! By Date Booked
Record                   RECORD,PRE()
Ref_Number                  LONG                      ! Unit Number
Model_Number                STRING(30)                ! 
Manufacturer                STRING(30)                ! 
ESN                         STRING(30)                ! 
MSN                         STRING(30)                ! 
Colour                      STRING(30)                ! 
Location                    STRING(30)                ! 
Shelf_Location              STRING(30)                ! 
Date_Booked                 DATE                      ! 
Times_Issued                REAL                      ! 
Available                   STRING(3)                 ! 
Job_Number                  LONG                      ! Job Number
Stock_Type                  STRING(30)                ! 
Audit_Number                REAL                      ! 
DummyField                  STRING(1)                 ! For File Manager
                         END
                     END                       

STOESN               FILE,DRIVER('Btrieve'),OEM,PRE(ste),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! Stock Serial Number File
Record_Number_Key        KEY(ste:Record_Number),NOCASE,PRIMARY ! By Record Number
Sold_Key                 KEY(ste:Ref_Number,ste:Sold,ste:Serial_Number),DUP,NOCASE ! By Serial Number
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
Ref_Number                  REAL                      ! 
Serial_Number               STRING(16)                ! 
Sold                        STRING(3)                 ! 
                         END
                     END                       

LOANACC              FILE,DRIVER('Btrieve'),PRE(lac),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Loan Accesories
Ref_Number_Key           KEY(lac:Ref_Number,lac:Accessory),NOCASE,PRIMARY ! By Accessory
Record                   RECORD,PRE()
Ref_Number                  REAL                      ! 
Accessory                   STRING(30)                ! 
                         END
                     END                       

SUPPLIER             FILE,DRIVER('Btrieve'),PRE(sup),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Suppliers
RecordNumberKey          KEY(sup:RecordNumber),NOCASE,PRIMARY ! 
AccountNumberKey         KEY(sup:Account_Number),DUP,NOCASE ! By Account Number
Company_Name_Key         KEY(sup:Company_Name),NOCASE ! By Company Name
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
Company_Name                STRING(30)                ! 
Postcode                    STRING(10)                ! 
Address_Line1               STRING(30)                ! 
Address_Line2               STRING(30)                ! 
Address_Line3               STRING(30)                ! 
Telephone_Number            STRING(15)                ! 
Fax_Number                  STRING(15)                ! 
EmailAddress                STRING(255)               ! Email Address
Contact_Name                STRING(60)                ! 
Account_Number              STRING(15)                ! 
Order_Period                REAL                      ! 
Normal_Supply_Period        REAL                      ! 
Minimum_Order_Value         REAL                      ! 
HistoryUsage                LONG                      ! History Usage
Factor                      REAL                      ! Multiply By Factor Of
Notes                       STRING(255)               ! 
                         END
                     END                       

EXCHACC              FILE,DRIVER('Btrieve'),PRE(xca),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Exchange Accesories
Ref_Number_Key           KEY(xca:Ref_Number,xca:Accessory),NOCASE,PRIMARY ! By Accessory
Record                   RECORD,PRE()
Ref_Number                  REAL                      ! 
Accessory                   STRING(30)                ! 
                         END
                     END                       

RECIPTYP             FILE,DRIVER('Btrieve'),OEM,PRE(rec),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Recipient Types
RecordNumberKey          KEY(rec:RecordNumber),NOCASE,PRIMARY ! By Record Number
RecipientTypeKey         KEY(rec:RecipientType),NOCASE ! By Recipient Type
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
RecipientType               STRING(30)                ! Recipient Type
                         END
                     END                       

SUPVALA              FILE,DRIVER('Btrieve'),OEM,PRE(suva),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Suppliers End Of Day Calcs A
RecordNumberKey          KEY(suva:RecordNumber),NOCASE,PRIMARY ! 
RunDateKey               KEY(suva:Supplier,suva:RunDate),DUP,NOCASE ! By Run Date
DateOnly                 KEY(suva:RunDate),DUP,NOCASE,OPT ! 
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
Supplier                    STRING(30)                ! Supplier
RunDate                     DATE                      ! Date
OldBackOrder                DATE                      ! Date Of Oldest Back Order
OldOutOrder                 DATE                      ! Date Of Oldest Outstanding Order
                         END
                     END                       

ESTPARTS             FILE,DRIVER('Btrieve'),PRE(epr),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Estimated Parts On A Job
Part_Number_Key          KEY(epr:Ref_Number,epr:Part_Number),DUP,NOCASE ! By Part Number
record_number_key        KEY(epr:record_number),NOCASE,PRIMARY ! 
Part_Ref_Number2_Key     KEY(epr:Part_Ref_Number),DUP,NOCASE ! By Part Ref Number
Date_Ordered_Key         KEY(epr:Date_Ordered),DUP,NOCASE ! By Date Ordered
Part_Ref_Number_Key      KEY(epr:Ref_Number,epr:Part_Ref_Number),DUP,NOCASE ! By Part Ref Number
Order_Number_Key         KEY(epr:Ref_Number,epr:Order_Number),DUP,NOCASE ! 
Supplier_Key             KEY(epr:Supplier),DUP,NOCASE ! 
Order_Part_Key           KEY(epr:Ref_Number,epr:Order_Part_Number),DUP,NOCASE ! By Order Part Number
Record                   RECORD,PRE()
Ref_Number                  REAL                      ! 
record_number               REAL                      ! 
Adjustment                  STRING(3)                 ! 
Part_Ref_Number             REAL                      ! 
Part_Details_Group          GROUP                     ! 
Part_Number                   STRING(30)              ! 
Description                   STRING(30)              ! 
Supplier                      STRING(30)              ! 
Purchase_Cost                 REAL                    ! 
Sale_Cost                     REAL                    ! 
Retail_Cost                   REAL                    ! 
                            END                       ! 
Quantity                    REAL                      ! 
Warranty_Part               STRING(3)                 ! 
Exclude_From_Order          STRING(3)                 ! 
Despatch_Note_Number        STRING(30)                ! 
Date_Ordered                DATE                      ! 
Pending_Ref_Number          REAL                      ! 
Order_Number                REAL                      ! 
Date_Received               DATE                      ! 
Order_Part_Number           REAL                      ! 
Status_Date                 DATE                      ! 
Fault_Codes1                GROUP                     ! 
Fault_Code1                   STRING(30)              ! 
Fault_Code2                   STRING(30)              ! 
Fault_Code3                   STRING(30)              ! 
Fault_Code4                   STRING(30)              ! 
Fault_Code5                   STRING(30)              ! 
Fault_Code6                   STRING(30)              ! 
                            END                       ! 
Fault_Codes2                GROUP                     ! 
Fault_Code7                   STRING(30)              ! 
Fault_Code8                   STRING(30)              ! 
Fault_Code9                   STRING(30)              ! 
Fault_Code10                  STRING(30)              ! 
Fault_Code11                  STRING(30)              ! 
Fault_Code12                  STRING(30)              ! 
                            END                       ! 
                         END
                     END                       

TRAEMAIL             FILE,DRIVER('Btrieve'),OEM,PRE(tre),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Trade Account Email Recipients
RecordNumberKey          KEY(tre:RecordNumber),NOCASE,PRIMARY ! By Record Number
RecipientKey             KEY(tre:RefNumber,tre:RecipientType),DUP,NOCASE ! By Recipient Type
ContactNameKey           KEY(tre:RefNumber,tre:ContactName),DUP,NOCASE ! By Contact Name
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
RefNumber                   LONG                      ! Ref Number
RecipientType               STRING(30)                ! Recipient Type
ContactName                 STRING(60)                ! Contact Name
EmailAddress                STRING(255)               ! Email Address
                         END
                     END                       

SUPVALB              FILE,DRIVER('Btrieve'),OEM,PRE(suvb),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Suppliers End Of Date Calcs B
RecordNumberKey          KEY(suvb:RecordNumber),NOCASE,PRIMARY ! By Record Number
RunDateKey               KEY(suvb:Supplier,suvb:RunDate),DUP,NOCASE ! By Run Date
LocationKey              KEY(suvb:Supplier,suvb:RunDate,suvb:Location),DUP,NOCASE ! By Location
DateOnly                 KEY(suvb:RunDate),DUP,NOCASE,OPT ! 
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
Supplier                    STRING(30)                ! Supplier
RunDate                     DATE                      ! Date
Location                    STRING(30)                ! Location
BackOrderValue              REAL                      ! Total Cost Of All Oustanding Orders
                         END
                     END                       

JOBS                 FILE,DRIVER('Btrieve'),PRE(job),BINDABLE,CREATE,THREAD,EXTERNAL(''),DLL(dll_mode)                    ! Jobs
Ref_Number_Key           KEY(job:Ref_Number),NOCASE,PRIMARY ! By Job Number
Model_Unit_Key           KEY(job:Model_Number,job:Unit_Type),DUP,NOCASE ! By Job Number
EngCompKey               KEY(job:Engineer,job:Completed,job:Ref_Number),DUP,NOCASE ! By Job Number
EngWorkKey               KEY(job:Engineer,job:Workshop,job:Ref_Number),DUP,NOCASE ! By Job Number
Surname_Key              KEY(job:Surname),DUP,NOCASE  ! By Surname
MobileNumberKey          KEY(job:Mobile_Number),DUP,NOCASE ! By Mobile Number
ESN_Key                  KEY(job:ESN),DUP,NOCASE      ! By E.S.N. / I.M.E.I.
MSN_Key                  KEY(job:MSN),DUP,NOCASE      ! By M.S.N.
AccountNumberKey         KEY(job:Account_Number,job:Ref_Number),DUP,NOCASE ! By Account Number
AccOrdNoKey              KEY(job:Account_Number,job:Order_Number),DUP,NOCASE ! By Order Number
Model_Number_Key         KEY(job:Model_Number),DUP,NOCASE ! By Model Number
Engineer_Key             KEY(job:Engineer,job:Model_Number),DUP,NOCASE ! By Engineer
Date_Booked_Key          KEY(job:date_booked),DUP,NOCASE ! By Date Booked
DateCompletedKey         KEY(job:Date_Completed),DUP,NOCASE ! By Date Completed
ModelCompKey             KEY(job:Model_Number,job:Date_Completed),DUP,NOCASE ! By Completed Date
By_Status                KEY(job:Current_Status,job:Ref_Number),DUP,NOCASE ! By Job Number
StatusLocKey             KEY(job:Current_Status,job:Location,job:Ref_Number),DUP,NOCASE ! By Job Number
Location_Key             KEY(job:Location,job:Ref_Number),DUP,NOCASE ! By Location
Third_Party_Key          KEY(job:Third_Party_Site,job:Third_Party_Printed,job:Ref_Number),DUP,NOCASE ! By Third Party
ThirdEsnKey              KEY(job:Third_Party_Site,job:Third_Party_Printed,job:ESN),DUP,NOCASE ! By ESN
ThirdMsnKey              KEY(job:Third_Party_Site,job:Third_Party_Printed,job:MSN),DUP,NOCASE ! By MSN
PriorityTypeKey          KEY(job:Job_Priority,job:Ref_Number),DUP,NOCASE ! By Priority
Unit_Type_Key            KEY(job:Unit_Type),DUP,NOCASE ! By Unit Type
EDI_Key                  KEY(job:Manufacturer,job:EDI,job:EDI_Batch_Number,job:Ref_Number),DUP,NOCASE ! By Job Number
InvoiceNumberKey         KEY(job:Invoice_Number),DUP,NOCASE ! By Invoice_Number
WarInvoiceNoKey          KEY(job:Invoice_Number_Warranty),DUP,NOCASE ! By Invoice Number
Batch_Number_Key         KEY(job:Batch_Number,job:Ref_Number),DUP,NOCASE ! By Job Number
Batch_Status_Key         KEY(job:Batch_Number,job:Current_Status,job:Ref_Number),DUP,NOCASE ! By Ref Number
BatchModelNoKey          KEY(job:Batch_Number,job:Model_Number,job:Ref_Number),DUP,NOCASE ! By Job Number
BatchInvoicedKey         KEY(job:Batch_Number,job:Invoice_Number,job:Ref_Number),DUP,NOCASE ! By Ref Number
BatchCompKey             KEY(job:Batch_Number,job:Completed,job:Ref_Number),DUP,NOCASE ! By Job Number
ChaInvoiceKey            KEY(job:Chargeable_Job,job:Account_Number,job:Invoice_Number,job:Ref_Number),DUP,NOCASE ! 
InvoiceExceptKey         KEY(job:Invoice_Exception,job:Ref_Number),DUP,NOCASE ! By Job Number
ConsignmentNoKey         KEY(job:Consignment_Number),DUP,NOCASE ! By Cosignment Number
InConsignKey             KEY(job:Incoming_Consignment_Number),DUP,NOCASE ! By Consignment Number
ReadyToDespKey           KEY(job:Despatched,job:Ref_Number),DUP,NOCASE ! By Job Number
ReadyToTradeKey          KEY(job:Despatched,job:Account_Number,job:Ref_Number),DUP,NOCASE ! By Job Number
ReadyToCouKey            KEY(job:Despatched,job:Current_Courier,job:Ref_Number),DUP,NOCASE ! By Job Number
ReadyToAllKey            KEY(job:Despatched,job:Account_Number,job:Current_Courier,job:Ref_Number),DUP,NOCASE ! By Job Number
DespJobNumberKey         KEY(job:Despatch_Number,job:Ref_Number),DUP,NOCASE ! By Job Number
DateDespatchKey          KEY(job:Courier,job:Date_Despatched,job:Ref_Number),DUP,NOCASE ! By Job Number
DateDespLoaKey           KEY(job:Loan_Courier,job:Loan_Despatched,job:Ref_Number),DUP,NOCASE ! By Job Number
DateDespExcKey           KEY(job:Exchange_Courier,job:Exchange_Despatched,job:Ref_Number),DUP,NOCASE ! By Job Number
ChaRepTypeKey            KEY(job:Repair_Type),DUP,NOCASE ! By Chargeable Repair Type
WarRepTypeKey            KEY(job:Repair_Type_Warranty),DUP,NOCASE ! By Warranty Repair Type
ChaTypeKey               KEY(job:Charge_Type),DUP,NOCASE ! 
WarChaTypeKey            KEY(job:Warranty_Charge_Type),DUP,NOCASE ! 
Bouncer_Key              KEY(job:Bouncer,job:Ref_Number),DUP,NOCASE ! By Job Number
EngDateCompKey           KEY(job:Engineer,job:Date_Completed),DUP,NOCASE ! By Date Completed
ExcStatusKey             KEY(job:Exchange_Status,job:Ref_Number),DUP,NOCASE ! By Job Number
LoanStatusKey            KEY(job:Loan_Status,job:Ref_Number),DUP,NOCASE ! By Job Number
ExchangeLocKey           KEY(job:Exchange_Status,job:Location,job:Ref_Number),DUP,NOCASE ! By Job Number
LoanLocKey               KEY(job:Loan_Status,job:Location,job:Ref_Number),DUP,NOCASE ! By Job Number
BatchJobKey              KEY(job:InvoiceAccount,job:InvoiceBatch,job:Ref_Number),DUP,NOCASE ! By Job Number
BatchStatusKey           KEY(job:InvoiceAccount,job:InvoiceBatch,job:InvoiceStatus,job:Ref_Number),DUP,NOCASE ! By Job Number
Record                   RECORD,PRE()
Ref_Number                  LONG                      ! 
Batch_Number                LONG                      ! 
Internal_Status             STRING(10)                ! 
Auto_Search                 STRING(30)                ! 
who_booked                  STRING(3)                 ! 
date_booked                 DATE                      ! 
time_booked                 TIME                      ! 
Cancelled                   STRING(3)                 ! 
Bouncer                     STRING(8)                 ! 
Bouncer_Type                STRING(3)                 ! Type Of Bouncer  CHArgeable / WARranty / BOTh
Web_Type                    STRING(3)                 ! Warranty/Insurance/Chargeable
Warranty_Job                STRING(3)                 ! 
Chargeable_Job              STRING(3)                 ! 
Model_Number                STRING(30)                ! 
Manufacturer                STRING(30)                ! 
ESN                         STRING(20)                ! 
MSN                         STRING(20)                ! 
ProductCode                 STRING(30)                ! 
Unit_Type                   STRING(30)                ! 
Colour                      STRING(30)                ! 
Location_Type               STRING(10)                ! 
Phone_Lock                  STRING(30)                ! 
Workshop                    STRING(3)                 ! 
Location                    STRING(30)                ! 
Authority_Number            STRING(30)                ! 
Insurance_Reference_Number  STRING(30)                ! 
DOP                         DATE                      ! 
Insurance                   STRING(3)                 ! 
Insurance_Type              STRING(30)                ! 
Transit_Type                STRING(30)                ! 
Physical_Damage             STRING(3)                 ! 
Intermittent_Fault          STRING(3)                 ! 
Loan_Status                 STRING(30)                ! 
Exchange_Status             STRING(30)                ! 
Job_Priority                STRING(30)                ! 
Charge_Type                 STRING(30)                ! 
Warranty_Charge_Type        STRING(30)                ! 
Current_Status              STRING(30)                ! 
Account_Number              STRING(15)                ! 
Trade_Account_Name          STRING(30)                ! 
Department_Name             STRING(30)                ! 
Order_Number                STRING(30)                ! 
POP                         STRING(3)                 ! 
In_Repair                   STRING(3)                 ! 
Date_In_Repair              DATE                      ! 
Time_In_Repair              TIME                      ! 
On_Test                     STRING(3)                 ! 
Date_On_Test                DATE                      ! 
Time_On_Test                TIME                      ! 
Estimate_Ready              STRING(3)                 ! 
QA_Passed                   STRING(3)                 ! 
Date_QA_Passed              DATE                      ! 
Time_QA_Passed              TIME                      ! 
QA_Rejected                 STRING(3)                 ! 
Date_QA_Rejected            DATE                      ! 
Time_QA_Rejected            TIME                      ! 
QA_Second_Passed            STRING(3)                 ! 
Date_QA_Second_Passed       DATE                      ! 
Time_QA_Second_Passed       TIME                      ! 
Title                       STRING(4)                 ! 
Initial                     STRING(1)                 ! 
Surname                     STRING(30)                ! 
Postcode                    STRING(10)                ! 
Company_Name                STRING(30)                ! 
Address_Line1               STRING(30)                ! 
Address_Line2               STRING(30)                ! 
Address_Line3               STRING(30)                ! 
Telephone_Number            STRING(15)                ! 
Fax_Number                  STRING(15)                ! 
Mobile_Number               STRING(15)                ! 
Postcode_Collection         STRING(10)                ! 
Company_Name_Collection     STRING(30)                ! 
Address_Line1_Collection    STRING(30)                ! 
Address_Line2_Collection    STRING(30)                ! 
Address_Line3_Collection    STRING(30)                ! 
Telephone_Collection        STRING(15)                ! 
Postcode_Delivery           STRING(10)                ! 
Address_Line1_Delivery      STRING(30)                ! 
Company_Name_Delivery       STRING(30)                ! 
Address_Line2_Delivery      STRING(30)                ! 
Address_Line3_Delivery      STRING(30)                ! 
Telephone_Delivery          STRING(15)                ! 
Date_Completed              DATE                      ! 
Time_Completed              TIME                      ! 
Completed                   STRING(3)                 ! 
Paid                        STRING(3)                 ! 
Paid_Warranty               STRING(3)                 ! 
Date_Paid                   DATE                      ! 
Repair_Type                 STRING(30)                ! 
Repair_Type_Warranty        STRING(30)                ! 
Engineer                    STRING(3)                 ! 
Ignore_Chargeable_Charges   STRING(3)                 ! 
Ignore_Warranty_Charges     STRING(3)                 ! 
Ignore_Estimate_Charges     STRING(3)                 ! 
Courier_Cost                REAL                      ! 
Advance_Payment             REAL                      ! 
Labour_Cost                 REAL                      ! 
Parts_Cost                  REAL                      ! 
Sub_Total                   REAL                      ! 
Courier_Cost_Estimate       REAL                      ! 
Labour_Cost_Estimate        REAL                      ! 
Parts_Cost_Estimate         REAL                      ! 
Sub_Total_Estimate          REAL                      ! 
Courier_Cost_Warranty       REAL                      ! 
Labour_Cost_Warranty        REAL                      ! 
Parts_Cost_Warranty         REAL                      ! 
Sub_Total_Warranty          REAL                      ! 
Loan_Issued_Date            DATE                      ! 
Loan_Unit_Number            LONG                      ! 
Loan_accessory              STRING(3)                 ! 
Loan_User                   STRING(3)                 ! 
Loan_Courier                STRING(30)                ! 
Loan_Consignment_Number     STRING(30)                ! 
Loan_Despatched             DATE                      ! 
Loan_Despatched_User        STRING(3)                 ! 
Loan_Despatch_Number        LONG                      ! 
LoaService                  STRING(1)                 ! Loan Service For City Link / LabelG
Exchange_Unit_Number        LONG                      ! 
Exchange_Authorised         STRING(3)                 ! 
Loan_Authorised             STRING(3)                 ! 
Exchange_Accessory          STRING(3)                 ! 
Exchange_Issued_Date        DATE                      ! 
Exchange_User               STRING(3)                 ! 
Exchange_Courier            STRING(30)                ! 
Exchange_Consignment_Number STRING(30)                ! 
Exchange_Despatched         DATE                      ! 
Exchange_Despatched_User    STRING(3)                 ! 
Exchange_Despatch_Number    LONG                      ! 
ExcService                  STRING(1)                 ! Exchange Service For City Link / Label G
Date_Despatched             DATE                      ! 
Despatch_Number             LONG                      ! 
Despatch_User               STRING(3)                 ! 
Courier                     STRING(30)                ! 
Consignment_Number          STRING(30)                ! 
Incoming_Courier            STRING(30)                ! 
Incoming_Consignment_Number STRING(30)                ! 
Incoming_Date               DATE                      ! 
Despatched                  STRING(3)                 ! 
Despatch_Type               STRING(3)                 ! 
Current_Courier             STRING(30)                ! 
JobService                  STRING(1)                 ! Job Service For City Link / Label G
Last_Repair_Days            REAL                      ! 
Third_Party_Site            STRING(30)                ! 
Estimate                    STRING(3)                 ! 
Estimate_If_Over            REAL                      ! 
Estimate_Accepted           STRING(3)                 ! 
Estimate_Rejected           STRING(3)                 ! 
Third_Party_Printed         STRING(3)                 ! 
ThirdPartyDateDesp          DATE                      ! 
Fault_Code1                 STRING(30)                ! 
Fault_Code2                 STRING(30)                ! 
Fault_Code3                 STRING(30)                ! 
Fault_Code4                 STRING(30)                ! 
Fault_Code5                 STRING(30)                ! 
Fault_Code6                 STRING(30)                ! 
Fault_Code7                 STRING(30)                ! 
Fault_Code8                 STRING(30)                ! 
Fault_Code9                 STRING(30)                ! 
Fault_Code10                STRING(255)               ! 
Fault_Code11                STRING(255)               ! 
Fault_Code12                STRING(255)               ! 
PreviousStatus              STRING(30)                ! 
StatusUser                  STRING(3)                 ! User who last changed the status
Status_End_Date             DATE                      ! 
Status_End_Time             TIME                      ! 
Turnaround_End_Date         DATE                      ! 
Turnaround_End_Time         TIME                      ! 
Turnaround_Time             STRING(30)                ! 
Special_Instructions        STRING(30)                ! 
InvoiceAccount              STRING(15)                ! Invoice Batch Account Number
InvoiceStatus               STRING(3)                 ! Proforma Status (UNA - Unathorised / AUT - Authorised / QUE - Query)
InvoiceBatch                LONG                      ! Proforma Batch Number
InvoiceQuery                STRING(255)               ! Query Reason
EDI                         STRING(3)                 ! 
EDI_Batch_Number            REAL                      ! 
Invoice_Exception           STRING(3)                 ! 
Invoice_Failure_Reason      STRING(80)                ! 
Invoice_Number              LONG                      ! 
Invoice_Date                DATE                      ! 
Invoice_Date_Warranty       DATE                      ! 
Invoice_Courier_Cost        REAL                      ! 
Invoice_Labour_Cost         REAL                      ! 
Invoice_Parts_Cost          REAL                      ! 
Invoice_Sub_Total           REAL                      ! 
Invoice_Number_Warranty     LONG                      ! 
WInvoice_Courier_Cost       REAL                      ! 
WInvoice_Labour_Cost        REAL                      ! 
WInvoice_Parts_Cost         REAL                      ! 
WInvoice_Sub_Total          REAL                      ! 
                         END
                     END                       

MODELNUM             FILE,DRIVER('Btrieve'),PRE(mod),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Model Numbers
Model_Number_Key         KEY(mod:Model_Number),NOCASE,PRIMARY ! By Model Number
Manufacturer_Key         KEY(mod:Manufacturer,mod:Model_Number),DUP,NOCASE ! By Manufacturer
Manufacturer_Unit_Type_Key KEY(mod:Manufacturer,mod:Unit_Type),DUP,NOCASE ! By Unit Type
Record                   RECORD,PRE()
Model_Number                STRING(30)                ! 
Manufacturer                STRING(30)                ! 
Specify_Unit_Type           STRING(3)                 ! 
Product_Type                STRING(30)                ! 
Unit_Type                   STRING(30)                ! 
Nokia_Unit_Type             STRING(30)                ! 
ESN_Length_From             REAL                      ! 
ESN_Length_To               REAL                      ! 
MSN_Length_From             REAL                      ! 
MSN_Length_To               REAL                      ! 
ExchangeUnitMinLevel        LONG                      ! Exchange Unit Min Level
IsVodafoneSpecific          BYTE                      ! 
IsCCentreSpecific           BYTE                      ! Vodafone Contact Centre Specific
UnsupportedExchange         BYTE                      ! Vodafone Unsupported Exchange Device
                         END
                     END                       

SUBEMAIL             FILE,DRIVER('Btrieve'),OEM,PRE(sue),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Sub Account Email Recipients
RecordNumberKey          KEY(sue:RecordNumber),NOCASE,PRIMARY ! By Record Number
RecipientTypeKey         KEY(sue:RefNumber,sue:RecipientType),DUP,NOCASE ! By Recipient Type
ContactNameKey           KEY(sue:RefNumber,sue:ContactName),DUP,NOCASE ! By Contact Name
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
RefNumber                   LONG                      ! Ref Number
RecipientType               STRING(30)                ! Recipient Type
ContactName                 STRING(60)                ! Contact Name
EmailAddress                STRING(255)               ! Email Address
                         END
                     END                       

SUPPTEMP             FILE,DRIVER('Btrieve'),PRE(suptmp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Temporary Suppliers File
Company_Name_Key         KEY(suptmp:Company_Name),NOCASE,PRIMARY ! By Company Name
Account_Number_Key       KEY(suptmp:Account_Number),DUP,NOCASE ! By Account Number
Record                   RECORD,PRE()
Company_Name                STRING(30)                ! 
Address_Group               GROUP                     ! 
Postcode                      STRING(10)              ! 
Address_Line1                 STRING(30)              ! 
Address_Line2                 STRING(30)              ! 
Address_Line3                 STRING(30)              ! 
                            END                       ! 
Telephone_Number            STRING(15)                ! 
Fax_Number                  STRING(15)                ! 
Contact_Name                STRING(60)                ! 
Account_Number              STRING(15)                ! 
Order_Period                REAL                      ! 
Normal_Supply_Period        REAL                      ! 
Minimum_Order_Value         REAL                      ! 
Notes                       STRING(1000)              ! 
                         END
                     END                       

ORDERS               FILE,DRIVER('Btrieve'),PRE(ord),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! Orders
Order_Number_Key         KEY(ord:Order_Number),NOCASE,PRIMARY ! By Order_Number
Printed_Key              KEY(ord:Printed,ord:Order_Number),DUP,NOCASE ! By Order Number
Supplier_Printed_Key     KEY(ord:Printed,ord:Supplier,ord:Order_Number),DUP,NOCASE ! By Order Number
Received_Key             KEY(ord:All_Received,ord:Order_Number),DUP,NOCASE ! 
Supplier_Key             KEY(ord:Supplier,ord:Order_Number),DUP,NOCASE ! By Order Number
Supplier_Received_Key    KEY(ord:Supplier,ord:All_Received,ord:Order_Number),DUP,NOCASE ! By Order Number
DateKey                  KEY(ord:Date),DUP,NOCASE     ! By Date
Record                   RECORD,PRE()
Order_Number                REAL                      ! 
Supplier                    STRING(30)                ! 
Date                        DATE                      ! 
Printed                     STRING(3)                 ! 
All_Received                STRING(3)                 ! 
User                        STRING(3)                 ! 
Dummy_Field                 STRING(1)                 ! For File Manager
                         END
                     END                       

UNITTYPE             FILE,DRIVER('Btrieve'),PRE(uni),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Unit Types
Unit_Type_Key            KEY(uni:Unit_Type),NOCASE,PRIMARY ! By Unit Type
ActiveKey                KEY(uni:Active,uni:Unit_Type),DUP,NOCASE ! By Unit Type
Record                   RECORD,PRE()
Unit_Type                   STRING(30)                ! 
Active                      BYTE                      ! Active
                         END
                     END                       

STATUS               FILE,DRIVER('Btrieve'),PRE(sts),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! Status Types
Ref_Number_Only_Key      KEY(sts:Ref_Number),NOCASE,PRIMARY ! 
Status_Key               KEY(sts:Status),DUP,NOCASE   ! By Status
Heading_Key              KEY(sts:Heading_Ref_Number,sts:Status),NOCASE ! By Status Type
Ref_Number_Key           KEY(sts:Heading_Ref_Number,sts:Ref_Number),NOCASE ! By Ref Number
LoanKey                  KEY(sts:Loan,sts:Status),DUP,NOCASE ! By Status
ExchangeKey              KEY(sts:Exchange,sts:Status),DUP,NOCASE ! By Status
JobKey                   KEY(sts:Job,sts:Status),DUP,NOCASE ! By Status
TurnJobKey               KEY(sts:Use_Turnaround_Time,sts:Job,sts:Status),DUP,NOCASE ! By Status
Record                   RECORD,PRE()
Heading_Ref_Number          REAL                      ! 
Status                      STRING(30)                ! 
Ref_Number                  REAL                      ! 
Use_Turnaround_Time         STRING(3)                 ! 
Use_Turnaround_Days         STRING(3)                 ! 
Turnaround_Days             LONG                      ! 
Use_Turnaround_Hours        STRING(3)                 ! 
Turnaround_Hours            LONG                      ! 
Notes                       STRING(1000)              ! 
Loan                        STRING(3)                 ! 
Exchange                    STRING(3)                 ! 
Job                         STRING(3)                 ! 
SystemStatus                STRING(3)                 ! This Status Cannot Be Changed Or Deleted
EngineerStatus              BYTE                      ! Display In Engineer's Browse
EnableEmail                 BYTE                      ! Enable Email
SenderEmailAddress          STRING(255)               ! Sender Email Address
EmailSubject                STRING(255)               ! Email Subject
EmailBody                   STRING(500)               ! Email Body
PickNoteEnable              BYTE                      ! 
                         END
                     END                       

REPAIRTY             FILE,DRIVER('Btrieve'),PRE(rep),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Repair Types
Repair_Type_Key          KEY(rep:Manufacturer,rep:Model_Number,rep:Repair_Type),NOCASE,PRIMARY ! By Repair Type
Manufacturer_Key         KEY(rep:Manufacturer,rep:Repair_Type),DUP,NOCASE ! By Repair Type
Model_Number_Key         KEY(rep:Model_Number,rep:Repair_Type),DUP,NOCASE ! By Repair Type
Repair_Type_Only_Key     KEY(rep:Repair_Type),DUP,NOCASE ! By Repair Type
Model_Chargeable_Key     KEY(rep:Model_Number,rep:Chargeable,rep:Repair_Type),DUP,NOCASE ! By Repair Type
Model_Warranty_Key       KEY(rep:Model_Number,rep:Warranty,rep:Repair_Type),DUP,NOCASE ! By Repair Type
Record                   RECORD,PRE()
Manufacturer                STRING(30)                ! 
Model_Number                STRING(30)                ! 
Repair_Type                 STRING(30)                ! 
Chargeable                  STRING(3)                 ! 
Warranty                    STRING(3)                 ! 
CompFaultCoding             BYTE                      ! Compulsory Fault Coding
ExcludeFromEDI              BYTE                      ! Exclude From EDI
ExcludeFromInvoicing        BYTE                      ! Exclude From Invoicing
                         END
                     END                       

TRADETMP             FILE,DRIVER('Btrieve'),OEM,PRE(tratmp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! File Used For Multiple Invoicing
RefNumberKey             KEY(tratmp:RefNumber),NOCASE,PRIMARY ! By Ref Number
AccountNoKey             KEY(tratmp:Account_Number),DUP,NOCASE ! By Account Number
CompanyNameKey           KEY(tratmp:Company_Name),DUP,NOCASE ! By Company Name
TypeKey                  KEY(tratmp:Type,tratmp:Account_Number),DUP,NOCASE ! By Account Number
Record                   RECORD,PRE()
RefNumber                   LONG                      ! 
Account_Number              STRING(15)                ! 
Company_Name                STRING(30)                ! 
Type                        STRING(3)                 ! Main / Sub
                         END
                     END                       

EXCCHRGE             FILE,DRIVER('Btrieve'),PRE(exc),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Standard Charges Exceptions
Manufacturer_Key         KEY(exc:Manufacturer,exc:Charge_Type,exc:Model_Number,exc:Unit_Type,exc:Repair_Type),DUP,NOCASE ! By Manufacturer
Record                   RECORD,PRE()
Manufacturer                STRING(30)                ! 
Charge_Type                 STRING(30)                ! 
Model_Number                STRING(30)                ! 
Unit_Type                   STRING(30)                ! 
Repair_Type                 STRING(30)                ! 
Cost                        REAL                      ! 
                         END
                     END                       

INVOICE              FILE,DRIVER('Btrieve'),PRE(inv),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Invoices
Invoice_Number_Key       KEY(inv:Invoice_Number),NOCASE,PRIMARY ! By Invoice Number
Invoice_Type_Key         KEY(inv:Invoice_Type,inv:Invoice_Number),DUP,NOCASE ! By Invoice_Number
Account_Number_Type_Key  KEY(inv:Invoice_Type,inv:Account_Number,inv:Invoice_Number),DUP,NOCASE ! By Invoice Number
Account_Number_Key       KEY(inv:Account_Number,inv:Invoice_Number),DUP,NOCASE ! By Invoice Number
Date_Created_Key         KEY(inv:Date_Created),DUP,NOCASE ! By Invoice Date
Batch_Number_Key         KEY(inv:Manufacturer,inv:Batch_Number),DUP,NOCASE ! By Batch Number
Record                   RECORD,PRE()
Invoice_Number              REAL                      ! 
Invoice_Type                STRING(3)                 ! 
Job_Number                  REAL                      ! 
Date_Created                DATE                      ! 
Account_Number              STRING(15)                ! 
AccountType                 STRING(3)                 ! Main Or Sub (MAI/SUB)
Total                       REAL                      ! 
Vat_Rate_Labour             REAL                      ! 
Vat_Rate_Parts              REAL                      ! 
Vat_Rate_Retail             REAL                      ! 
VAT_Number                  STRING(30)                ! 
Invoice_VAT_Number          STRING(30)                ! 
Currency                    STRING(30)                ! 
Batch_Number                REAL                      ! 
Manufacturer                STRING(30)                ! 
Claim_Reference             STRING(30)                ! 
Total_Claimed               REAL                      ! 
Courier_Paid                REAL                      ! 
Labour_Paid                 REAL                      ! 
Parts_Paid                  REAL                      ! 
Reconciled_Date             DATE                      ! 
jobs_count                  REAL                      ! 
PrevInvoiceNo               LONG                      ! Link To Previous Invoice Number for Credit Notes
InvoiceCredit               STRING(3)                 ! Invoice Type: Invoice or Credit
UseAlternativeAddress       BYTE                      ! Use Alternative Address
EuroExhangeRate             REAL                      ! Euro Exchange Rate
                         END
                     END                       

TRDBATCH             FILE,DRIVER('Btrieve'),OEM,PRE(trb),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Third Party Batch Number
RecordNumberKey          KEY(trb:RecordNumber),NOCASE,PRIMARY ! By Record Number
Batch_Number_Key         KEY(trb:Company_Name,trb:Batch_Number),DUP,NOCASE ! By Job Number
Batch_Number_Status_Key  KEY(trb:Status,trb:Batch_Number),DUP,NOCASE ! By Batch Number
BatchOnlyKey             KEY(trb:Batch_Number),DUP,NOCASE ! By Batch Number
Batch_Company_Status_Key KEY(trb:Status,trb:Company_Name,trb:Batch_Number),DUP,NOCASE ! By Batch Number
ESN_Only_Key             KEY(trb:ESN),DUP,NOCASE      ! By ESN
ESN_Status_Key           KEY(trb:Status,trb:ESN),DUP,NOCASE ! By ESN
Company_Batch_ESN_Key    KEY(trb:Company_Name,trb:Batch_Number,trb:ESN),DUP,NOCASE ! By ESN
AuthorisationKey         KEY(trb:AuthorisationNo),DUP,NOCASE ! By Authorisation Number
StatusAuthKey            KEY(trb:Status,trb:AuthorisationNo),DUP,NOCASE ! By Status
CompanyDateKey           KEY(trb:Company_Name,trb:Status,trb:DateReturn),DUP,NOCASE ! By Date
JobStatusKey             KEY(trb:Status,trb:Ref_Number),DUP,NOCASE ! By Job Number
JobNumberKey             KEY(trb:Ref_Number),DUP,NOCASE ! By Job Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! 
Batch_Number                LONG                      ! 
Company_Name                STRING(30)                ! 
Ref_Number                  LONG                      ! Job Number
ESN                         STRING(20)                ! 
Status                      STRING(3)                 ! Out or In
Date                        DATE                      ! 
Time                        TIME                      ! 
AuthorisationNo             STRING(30)                ! Authorisation Number for the Despatched Batch
Exchanged                   STRING(3)                 ! Has the Job got an Exchange Unit attached
DateReturn                  DATE                      ! Date The Unit Was Returned
TimeReturn                  TIME                      ! Time Unit Returned
TurnTime                    LONG                      ! Turnaround Time
MSN                         STRING(30)                ! M.S.N.
                         END
                     END                       

TRACHRGE             FILE,DRIVER('Btrieve'),PRE(trc),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Trade Account Charges
Account_Charge_Key       KEY(trc:Account_Number,trc:Model_Number,trc:Charge_Type,trc:Unit_Type,trc:Repair_Type),NOCASE,PRIMARY ! By Charge Type
Model_Repair_Key         KEY(trc:Model_Number,trc:Repair_Type),DUP,NOCASE ! 
Charge_Type_Key          KEY(trc:Account_Number,trc:Model_Number,trc:Charge_Type),DUP,NOCASE ! By Charge Type
Unit_Type_Key            KEY(trc:Account_Number,trc:Model_Number,trc:Unit_Type),DUP,NOCASE ! By Unit Type
Repair_Type_Key          KEY(trc:Account_Number,trc:Model_Number,trc:Repair_Type),DUP,NOCASE ! By Repair Type
Cost_Key                 KEY(trc:Account_Number,trc:Model_Number,trc:Cost),DUP,NOCASE ! By Cost
Charge_Type_Only_Key     KEY(trc:Charge_Type),DUP,NOCASE ! 
Repair_Type_Only_Key     KEY(trc:Repair_Type),DUP,NOCASE ! 
Unit_Type_Only_Key       KEY(trc:Unit_Type),DUP,NOCASE ! 
Record                   RECORD,PRE()
Account_Number              STRING(15)                ! 
Charge_Type                 STRING(30)                ! 
Unit_Type                   STRING(30)                ! 
Model_Number                STRING(30)                ! 
Repair_Type                 STRING(30)                ! 
Cost                        REAL                      ! 
dummy                       BYTE                      ! 
                         END
                     END                       

TRDMODEL             FILE,DRIVER('Btrieve'),PRE(trm),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Third Party Models
Model_Number_Key         KEY(trm:Company_Name,trm:Model_Number),NOCASE,PRIMARY ! By Model Number
Model_Number_Only_Key    KEY(trm:Model_Number),DUP,NOCASE ! By Model Number
Record                   RECORD,PRE()
Company_Name                STRING(30)                ! 
Model_Number                STRING(30)                ! 
                         END
                     END                       

STOMODEL             FILE,DRIVER('Btrieve'),PRE(stm),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Stock Model Numbers
Model_Number_Key         KEY(stm:Ref_Number,stm:Manufacturer,stm:Model_Number),NOCASE,PRIMARY ! By Model Number
Model_Part_Number_Key    KEY(stm:Accessory,stm:Model_Number,stm:Location,stm:Part_Number),DUP,NOCASE ! By Model Number
Description_Key          KEY(stm:Accessory,stm:Model_Number,stm:Location,stm:Description),DUP,NOCASE ! By Description
Manufacturer_Key         KEY(stm:Manufacturer,stm:Model_Number),DUP,NOCASE ! By Manufacturer
Ref_Part_Description     KEY(stm:Ref_Number,stm:Part_Number,stm:Location,stm:Description),DUP,NOCASE ! Why?
Location_Part_Number_Key KEY(stm:Model_Number,stm:Location,stm:Part_Number),DUP,NOCASE ! 
Location_Description_Key KEY(stm:Model_Number,stm:Location,stm:Description),DUP,NOCASE ! 
Mode_Number_Only_Key     KEY(stm:Ref_Number,stm:Model_Number),DUP,NOCASE ! By Model Number
Record                   RECORD,PRE()
Ref_Number                  LONG                      ! 
Manufacturer                STRING(30)                ! 
Model_Number                STRING(30)                ! 
Part_Number                 STRING(30)                ! 
Description                 STRING(30)                ! 
Location                    STRING(30)                ! 
Accessory                   STRING(3)                 ! 
FaultCode1                  STRING(30)                ! Fault Code 1
FaultCode2                  STRING(30)                ! Fault Code 2
FaultCode3                  STRING(30)                ! Fault Code 3
FaultCode4                  STRING(30)                ! Fault Code 4
FaultCode5                  STRING(30)                ! Fault Code 5
FaultCode6                  STRING(30)                ! Fault Code 6
FaultCode7                  STRING(30)                ! Fault Code 7
FaultCode8                  STRING(30)                ! Fault Code 8
FaultCode9                  STRING(30)                ! Fault Code 9
FaultCode10                 STRING(30)                ! Fault Code 10
FaultCode11                 STRING(30)                ! Fault Code 11
FaultCode12                 STRING(30)                ! Fault Code 12
                         END
                     END                       

TRADEACC             FILE,DRIVER('Btrieve'),PRE(tra),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Trade Accounts
RecordNumberKey          KEY(tra:RecordNumber),NOCASE,PRIMARY ! By Record Number
Account_Number_Key       KEY(tra:Account_Number),NOCASE ! By Account Number
Company_Name_Key         KEY(tra:Company_Name),DUP,NOCASE ! By Company Name
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
Account_Number              STRING(15)                ! 
Postcode                    STRING(15)                ! 
Company_Name                STRING(30)                ! 
Address_Line1               STRING(30)                ! 
Address_Line2               STRING(30)                ! 
Address_Line3               STRING(30)                ! 
Telephone_Number            STRING(15)                ! 
Fax_Number                  STRING(15)                ! 
EmailAddress                STRING(255)               ! Email Address
Contact_Name                STRING(30)                ! 
Labour_Discount_Code        STRING(2)                 ! 
Retail_Discount_Code        STRING(2)                 ! 
Parts_Discount_Code         STRING(2)                 ! 
Labour_VAT_Code             STRING(2)                 ! 
Parts_VAT_Code              STRING(2)                 ! 
Retail_VAT_Code             STRING(2)                 ! 
Account_Type                STRING(6)                 ! 
Credit_Limit                REAL                      ! 
Account_Balance             REAL                      ! 
Use_Sub_Accounts            STRING(3)                 ! 
Invoice_Sub_Accounts        STRING(3)                 ! 
Stop_Account                STRING(3)                 ! 
Allow_Cash_Sales            STRING(3)                 ! 
Price_Despatch              STRING(3)                 ! 
Price_Retail_Despatch       STRING(3)                 ! 
Print_Despatch_Complete     STRING(3)                 ! Print Despatch Note At Completion
Print_Despatch_Despatch     STRING(3)                 ! Print Despatch Note At Despatch
Standard_Repair_Type        STRING(15)                ! 
Courier_Incoming            STRING(30)                ! 
Courier_Outgoing            STRING(30)                ! 
Use_Contact_Name            STRING(3)                 ! 
VAT_Number                  STRING(30)                ! 
Use_Delivery_Address        STRING(3)                 ! 
Use_Collection_Address      STRING(3)                 ! 
UseCustDespAdd              STRING(3)                 ! Use Customer Address As Despatch Address
Allow_Loan                  STRING(3)                 ! 
Allow_Exchange              STRING(3)                 ! 
Force_Fault_Codes           STRING(3)                 ! 
Despatch_Invoiced_Jobs      STRING(3)                 ! 
Despatch_Paid_Jobs          STRING(3)                 ! 
Print_Despatch_Notes        STRING(3)                 ! 
Print_Retail_Despatch_Note  STRING(3)                 ! 
Print_Retail_Picking_Note   STRING(3)                 ! 
Despatch_Note_Per_Item      STRING(3)                 ! 
Skip_Despatch               STRING(3)                 ! Use Batch Despatch
IgnoreDespatch              STRING(3)                 ! Do not use Despatch Table
Summary_Despatch_Notes      STRING(3)                 ! 
Exchange_Stock_Type         STRING(30)                ! 
Loan_Stock_Type             STRING(30)                ! 
Courier_Cost                REAL                      ! 
Force_Estimate              STRING(3)                 ! 
Estimate_If_Over            REAL                      ! 
Turnaround_Time             STRING(30)                ! 
Password                    STRING(20)                ! 
Retail_Payment_Type         STRING(3)                 ! 
Retail_Price_Structure      STRING(3)                 ! 
Use_Customer_Address        STRING(3)                 ! 
Invoice_Customer_Address    STRING(3)                 ! 
ZeroChargeable              STRING(3)                 ! Don't show Chargeable Costs
RefurbCharge                STRING(3)                 ! 
ChargeType                  STRING(30)                ! 
WarChargeType               STRING(30)                ! 
ExchangeAcc                 STRING(3)                 ! Account used for repairing exchange units
MultiInvoice                STRING(3)                 ! Invoice Type (SIN - Single / SIM - Single (Summary) / MUL - Multiple / BAT - Batch)
EDIInvoice                  STRING(3)                 ! Will the Proforma Invoice be Exported?
ExportPath                  STRING(255)               ! For ProForma Export
ImportPath                  STRING(255)               ! For ProForma Export
BatchNumber                 LONG                      ! ProForma Batch Number
ExcludeBouncer              BYTE                      ! Exclude From Bouncer
RetailZeroParts             BYTE                      ! Zero Value Parts
HideDespAdd                 BYTE                      ! Hide Address On Despatch Note
EuroApplies                 BYTE                      ! Apply Euro
ForceCommonFault            BYTE                      ! Force Common Fault
ForceOrderNumber            BYTE                      ! Force Order Number
ShowRepairTypes             BYTE                      ! Repair Type Display
WebMemo                     STRING(255)               ! Web Memo
WebPassword1                STRING(30)                ! Web Password1
WebPassword2                STRING(30)                ! WebPassword2
InvoiceAtDespatch           BYTE                      ! Print Invoice At Despatch
InvoiceType                 BYTE                      ! Invoice Type
IndividualSummary           BYTE                      ! Print Individual Summary Report At Despatch
UseTradeContactNo           BYTE                      ! Use Trade Contact No
StopThirdParty              BYTE                      ! Stop Unit For Going To Third Party
E1                          BYTE                      ! E1
E2                          BYTE                      ! E2
E3                          BYTE                      ! E3
UseDespatchDetails          BYTE                      ! Use Alternative Contact Nos On Despatch Note
AltTelephoneNumber          STRING(30)                ! Telephone Number
AltFaxNumber                STRING(30)                ! Fax Number
AltEmailAddress             STRING(255)               ! Email Address
AutoSendStatusEmails        BYTE                      ! Auto Send Status Emails
EmailRecipientList          BYTE                      ! Email Recipient List
EmailEndUser                BYTE                      ! Email End User
MakeSplitJob                BYTE                      ! Make Split Job On Exchange
SplitJobStatus              STRING(30)                ! Job Status
SplitExchangeStatus         STRING(30)                ! Exchange Status
SplitCChargeType            STRING(30)                ! Chargeable Charge Type
SplitWChargeType            STRING(30)                ! Warranty Charge Type
CheckChargeablePartsCost    BYTE                      ! 
MaxChargeablePartsCost      REAL                      ! 
UseAlternateDespatchNote    BYTE                      ! 
                         END
                     END                       

QUERYREA             FILE,DRIVER('Btrieve'),OEM,PRE(que),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Query Reason
RefNumberKey             KEY(que:RefNumber),NOCASE,PRIMARY ! By Ref Number
QueryKey                 KEY(que:Query),NOCASE        ! By Query
Record                   RECORD,PRE()
RefNumber                   LONG                      ! 
Query                       STRING(60)                ! 
                         END
                     END                       

SUBCHRGE             FILE,DRIVER('Btrieve'),PRE(suc),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Sub Trade Account Charges
Model_Repair_Type_Key    KEY(suc:Account_Number,suc:Model_Number,suc:Charge_Type,suc:Unit_Type,suc:Repair_Type),NOCASE,PRIMARY ! By Repair Type
Account_Charge_Key       KEY(suc:Account_Number,suc:Model_Number,suc:Charge_Type),DUP,NOCASE ! By Charge Type
Unit_Type_Key            KEY(suc:Account_Number,suc:Model_Number,suc:Unit_Type),DUP,NOCASE ! By Unit Type
Repair_Type_Key          KEY(suc:Account_Number,suc:Model_Number,suc:Repair_Type),DUP,NOCASE ! By Repair Type
Cost_Key                 KEY(suc:Account_Number,suc:Model_Number,suc:Cost),DUP,NOCASE ! By Cost
Charge_Type_Only_Key     KEY(suc:Charge_Type),DUP,NOCASE ! 
Repair_Type_Only_Key     KEY(suc:Repair_Type),DUP,NOCASE ! 
Unit_Type_Only_Key       KEY(suc:Unit_Type),DUP,NOCASE ! 
Model_Repair_Key         KEY(suc:Model_Number,suc:Repair_Type),DUP,NOCASE ! 
Record                   RECORD,PRE()
Account_Number              STRING(15)                ! 
Charge_Type                 STRING(30)                ! 
Unit_Type                   STRING(30)                ! 
Model_Number                STRING(30)                ! 
Repair_Type                 STRING(30)                ! 
Cost                        REAL                      ! 
dummy                       BYTE                      ! 
                         END
                     END                       

DEFCHRGE             FILE,DRIVER('Btrieve'),PRE(dec),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Default Charges
Charge_Type_Key          KEY(dec:Charge_Type),NOCASE,PRIMARY ! By Charge Type
Repair_Type_Only_Key     KEY(dec:Repair_Type),DUP,NOCASE ! 
Unit_Type_Only_Key       KEY(dec:Unit_Type),DUP,NOCASE ! 
Record                   RECORD,PRE()
Charge_Type                 STRING(30)                ! 
Unit_Type                   STRING(30)                ! 
Model_Number                STRING(30)                ! 
Repair_Type                 STRING(30)                ! 
Cost                        REAL                      ! 
                         END
                     END                       

SUBTRACC             FILE,DRIVER('Btrieve'),PRE(sub),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Sub Trade Accounts
RecordNumberKey          KEY(sub:RecordNumber),NOCASE,PRIMARY ! By Record Number
Main_Account_Key         KEY(sub:Main_Account_Number,sub:Account_Number),NOCASE ! By Account Number
Main_Name_Key            KEY(sub:Main_Account_Number,sub:Company_Name),DUP,NOCASE ! By Company Name
Account_Number_Key       KEY(sub:Account_Number),NOCASE ! By Account Number
Branch_Key               KEY(sub:Branch),DUP,NOCASE   ! By Branch
Main_Branch_Key          KEY(sub:Main_Account_Number,sub:Branch),DUP,NOCASE ! By Branch
Company_Name_Key         KEY(sub:Company_Name),DUP,NOCASE ! By Company Name
Contact_Centre_Key       KEY(sub:IsContactCentre,sub:Account_Number),DUP,NOCASE ! By Contact Centre
RetailStoreKey           KEY(sub:VodafoneRetailStore,sub:Account_Number),DUP,NOCASE ! By Account Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
Main_Account_Number         STRING(15)                ! 
Account_Number              STRING(15)                ! 
Postcode                    STRING(15)                ! 
Company_Name                STRING(30)                ! 
Address_Line1               STRING(30)                ! 
Address_Line2               STRING(30)                ! 
Address_Line3               STRING(30)                ! 
Telephone_Number            STRING(15)                ! 
Fax_Number                  STRING(15)                ! 
EmailAddress                STRING(255)               ! Email Address
Branch                      STRING(30)                ! 
Contact_Name                STRING(30)                ! 
Enquiry_Source              STRING(30)                ! 
Labour_Discount_Code        STRING(2)                 ! 
Retail_Discount_Code        STRING(2)                 ! 
Parts_Discount_Code         STRING(2)                 ! 
Labour_VAT_Code             STRING(2)                 ! 
Retail_VAT_Code             STRING(2)                 ! 
Parts_VAT_Code              STRING(2)                 ! 
Account_Type                STRING(6)                 ! 
Credit_Limit                REAL                      ! 
Account_Balance             REAL                      ! 
Stop_Account                STRING(3)                 ! 
Allow_Cash_Sales            STRING(3)                 ! 
Use_Delivery_Address        STRING(3)                 ! 
Use_Collection_Address      STRING(3)                 ! 
UseCustDespAdd              STRING(3)                 ! Use Customer Address As Despatch Address
Courier_Incoming            STRING(30)                ! 
Courier_Outgoing            STRING(30)                ! 
VAT_Number                  STRING(30)                ! 
Despatch_Invoiced_Jobs      STRING(3)                 ! 
Despatch_Paid_Jobs          STRING(3)                 ! 
Print_Despatch_Notes        STRING(3)                 ! 
PriceDespatchNotes          BYTE                      ! Price Despatch Notes
Print_Despatch_Complete     STRING(3)                 ! Print Despatch Note At Completion
Print_Despatch_Despatch     STRING(3)                 ! Print Despatch Note At Despatch
Print_Retail_Despatch_Note  STRING(3)                 ! 
Print_Retail_Picking_Note   STRING(3)                 ! 
Despatch_Note_Per_Item      STRING(3)                 ! 
Summary_Despatch_Notes      STRING(3)                 ! 
Courier_Cost                REAL                      ! 
Password                    STRING(20)                ! 
Retail_Payment_Type         STRING(3)                 ! 
Retail_Price_Structure      STRING(3)                 ! 
Use_Customer_Address        STRING(3)                 ! 
Invoice_Customer_Address    STRING(3)                 ! 
ZeroChargeable              STRING(3)                 ! Don't show Chargeable Costs
MultiInvoice                STRING(3)                 ! Invoice Type (SIN - Single / SIM - Single (Summary) / MUL - Multiple / BAT - Batch)
EDIInvoice                  STRING(3)                 ! Will the Proforma Invoice be Exported?
ExportPath                  STRING(255)               ! For ProForma Export
ImportPath                  STRING(255)               ! For ProForma Export
BatchNumber                 LONG                      ! ProForma Batch Number
HideDespAdd                 BYTE                      ! Hide Address On Despatch Note
EuroApplies                 BYTE                      ! Apply Euro
ForceCommonFault            BYTE                      ! Force Common Fault
ForceOrderNumber            BYTE                      ! Force Order Number
WebPassword1                STRING(30)                ! Web Password1
WebPassword2                STRING(30)                ! WebPassword2
InvoiceAtDespatch           BYTE                      ! Print Invoice At Despatch
InvoiceType                 BYTE                      ! Invoice Type
IndividualSummary           BYTE                      ! Print Individual Summary Report At Despatch
UseTradeContactNo           BYTE                      ! Use Trade Contact No
E1                          BYTE                      ! E1
E2                          BYTE                      ! E2
E3                          BYTE                      ! E3
UseDespatchDetails          BYTE                      ! Use Alternative Contact Nos On Despatch Note
AltTelephoneNumber          STRING(30)                ! Telephone Number
AltFaxNumber                STRING(30)                ! Fax Number
AltEmailAddress             STRING(255)               ! Email Address
UseAlternativeAdd           BYTE                      ! Use Alternative Delivery Addresses
AutoSendStatusEmails        BYTE                      ! Auto Send Status Emails
EmailRecipientList          BYTE                      ! Email Recipient List
EmailEndUser                BYTE                      ! Email End User
IsContactCentre             BYTE                      ! Is This Account A Vodafone Contact Centre
VodafoneRetailStore         BYTE                      ! Vodafone Retail Store
                         END
                     END                       

CHARTYPE             FILE,DRIVER('Btrieve'),PRE(cha),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Charge Types
Charge_Type_Key          KEY(cha:Charge_Type),NOCASE,PRIMARY ! By Charge Type
Ref_Number_Key           KEY(cha:Ref_Number,cha:Charge_Type),DUP,NOCASE ! By Charge Type
Physical_Damage_Key      KEY(cha:Allow_Physical_Damage,cha:Charge_Type),DUP,NOCASE ! By Charge Type
Warranty_Key             KEY(cha:Warranty,cha:Charge_Type),DUP,NOCASE ! By Charge Type
Warranty_Ref_Number_Key  KEY(cha:Warranty,cha:Ref_Number,cha:Charge_Type),DUP,NOCASE ! By Charge Type
Record                   RECORD,PRE()
Charge_Type                 STRING(30)                ! 
Force_Warranty              STRING(3)                 ! 
Allow_Physical_Damage       STRING(3)                 ! 
Allow_Estimate              STRING(3)                 ! 
Force_Estimate              STRING(3)                 ! 
Invoice_Customer            STRING(3)                 ! 
Invoice_Trade_Customer      STRING(3)                 ! 
Invoice_Manufacturer        STRING(3)                 ! 
No_Charge                   STRING(3)                 ! 
Zero_Parts                  STRING(3)                 ! 
Warranty                    STRING(3)                 ! 
Ref_Number                  REAL                      ! 
Exclude_EDI                 STRING(3)                 ! 
ExcludeInvoice              BYTE                      ! Exclude From Invoicing
                         END
                     END                       

MANUFACT             FILE,DRIVER('Btrieve'),PRE(man),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Manufacturers
RecordNumberKey          KEY(man:RecordNumber),NOCASE,PRIMARY ! By Record Number
Manufacturer_Key         KEY(man:Manufacturer),NOCASE ! By Manufacturer
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
Manufacturer                STRING(30)                ! 
Account_Number              STRING(15)                ! 
Postcode                    STRING(15)                ! 
Address_Line1               STRING(30)                ! 
Address_Line2               STRING(30)                ! 
Address_Line3               STRING(30)                ! 
Telephone_Number            STRING(15)                ! 
Fax_Number                  STRING(15)                ! 
EmailAddress                STRING(255)               ! Email Address
Use_MSN                     STRING(3)                 ! 
Contact_Name1               STRING(60)                ! 
Contact_Name2               STRING(60)                ! 
Head_Office_Telephone       STRING(15)                ! 
Head_Office_Fax             STRING(15)                ! 
Technical_Support_Telephone STRING(15)                ! 
Technical_Support_Fax       STRING(15)                ! 
Technical_Support_Hours     STRING(30)                ! 
Batch_Number                REAL                      ! 
EDI_Account_Number          STRING(30)                ! 
EDI_Path                    STRING(255)               ! 
Trade_Account               STRING(30)                ! 
Supplier                    STRING(30)                ! 
Warranty_Period             REAL                      ! 
SamsungCount                LONG                      ! Count Of Samsung Claims
IncludeAdjustment           STRING(3)                 ! EDI Warranty Adjustments
AdjustPart                  BYTE                      ! Assign Part Number To Warranty Adjustment
ForceParts                  BYTE                      ! Force Warranty Adjustment If No Parts Used
NokiaType                   STRING(20)                ! 
RemAccCosts                 BYTE                      ! Remove Accessory Claims Costs
SiemensNewEDI               BYTE                      ! Use New EDI Format
DOPCompulsory               BYTE                      ! D.O.P. Compulsory for Warranty
Notes                       STRING(255)               ! 
UseQA                       BYTE                      ! Use QA
UseElectronicQA             BYTE                      ! Use Electronic QA
QALoanExchange              BYTE                      ! QA Loan/Exchange Units
QAAtCompletion              BYTE                      ! QA At Completion
SiemensNumber               LONG                      ! Siemens Number
SiemensDate                 DATE                      ! Date
UseProductCode              BYTE                      ! Use Product Code
QAParts                     BYTE                      ! Validate Parts At QA
QANetwork                   BYTE                      ! Validate Network At QA
DisallowNA                  BYTE                      ! Disallow N/A for MSN
IsVodafoneSpecific          BYTE                      ! 
MSNFieldMask                STRING(30)                ! 
IsCCentreSpecific           BYTE                      ! Vodafone Contact Centre Specific
EDIFileType                 STRING(30)                ! EDI File Type
                         END
                     END                       

USMASSIG             FILE,DRIVER('Btrieve'),PRE(usm),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Users Model Number Assignments
Model_Number_Key         KEY(usm:User_Code,usm:Model_Number),NOCASE,PRIMARY ! By Model Number
Record                   RECORD,PRE()
User_Code                   STRING(3)                 ! 
Model_Number                STRING(30)                ! 
                         END
                     END                       

EXPWARPARTS          FILE,DRIVER('BASIC'),PRE(expwar),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)             ! Export - Warranty Parts
Record                   RECORD,PRE()
Ref_Number                  STRING(20)                ! 
Part_Details_Group          GROUP                     ! 
Part_Number                   STRING(30)              ! 
Description                   STRING(30)              ! 
Supplier                      STRING(30)              ! 
Purchase_Cost                 STRING(20)              ! 
Sale_Cost                     STRING(20)              ! 
                            END                       ! 
Quantity                    STRING(20)                ! 
Exclude_From_Order          STRING(20)                ! 
Despatch_Note_Number        STRING(30)                ! 
Date_Ordered                STRING(20)                ! 
Order_Number                STRING(20)                ! 
Date_Received               STRING(20)                ! 
UnitType                    STRING(30)                ! Unit Type
Fault_Codes1                GROUP                     ! 
Fault_Code1                   STRING(30)              ! 
Fault_Code2                   STRING(30)              ! 
Fault_Code3                   STRING(30)              ! 
Fault_Code4                   STRING(30)              ! 
Fault_Code5                   STRING(30)              ! 
Fault_Code6                   STRING(30)              ! 
                            END                       ! 
Fault_Codes2                GROUP                     ! 
Fault_Code7                   STRING(30)              ! 
Fault_Code8                   STRING(30)              ! 
Fault_Code9                   STRING(30)              ! 
Fault_Code10                  STRING(30)              ! 
Fault_Code11                  STRING(30)              ! 
Fault_Code12                  STRING(30)              ! 
                            END                       ! 
FirstJob                    STRING(1)                 ! First Job
                         END
                     END                       

USUASSIG             FILE,DRIVER('Btrieve'),PRE(usu),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Users Unit Type Assignments
Unit_Type_Key            KEY(usu:User_Code,usu:Unit_Type),NOCASE,PRIMARY ! By Unit Type
Unit_Type_Only_Key       KEY(usu:Unit_Type),DUP,NOCASE ! By Unit Type
Record                   RECORD,PRE()
User_Code                   STRING(3)                 ! 
Unit_Type                   STRING(30)                ! 
                         END
                     END                       

XMLTRADE             FILE,DRIVER('Btrieve'),OEM,PRE(XTR),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                ! Samsung XML Allowable Trade Accounts
AccountNumberKey         KEY(XTR:AccountNumber),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
AccountNumber               STRING(15)                ! 
                         END
                     END                       

XMLJOBQ              FILE,DRIVER('Btrieve'),OEM,PRE(XJQ),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Samsung XML Job Export Queue
JobNumberKey             KEY(XJQ:JobNumber),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
JobNumber                   LONG                      ! 
                         END
                     END                       

XMLDEFS              FILE,DRIVER('Btrieve'),OEM,PRE(XDF),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Samsung XML Defaults
RecordNumberKey          KEY(XDF:RecordNumber),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
RecordNumber                LONG                      ! 
UserCode                    STRING(3)                 ! 
AccountNumber               STRING(15)                ! 
ChargeType                  STRING(30)                ! 
WarrantyChargeType          STRING(30)                ! 
TransitType                 STRING(30)                ! 
JobTurnaroundTime           STRING(30)                ! 
UnitType                    STRING(30)                ! 
CollectionStatus            STRING(30)                ! 
DropOffStatus               STRING(30)                ! 
DeliveryText                STRING(255)               ! 
DropPointStatus             STRING(30)                ! 
DropPointLocation           STRING(30)                ! 
                         END
                     END                       

XMLSB                FILE,DRIVER('Btrieve'),OEM,PRE(XSB),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                   ! Samsung XML ServiceBase Status Codes
SBStatusKey              KEY(XSB:SBStatus),NOCASE,PRIMARY ! 
FKStatusCodeKey          KEY(XSB:FKStatusCode),DUP,NOCASE ! 
Record                   RECORD,PRE()
SBStatus                    STRING(30)                ! 
FKStatusCode                STRING(4)                 ! 
                         END
                     END                       

MPXDEFS              FILE,DRIVER('Btrieve'),OEM,PRE(MXD),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! MPX XML Trade Account Import Defaults
AccountNoKey             KEY(MXD:AccountNo),NOCASE,PRIMARY ! Primary Key
Record                   RECORD,PRE()
AccountNo                   STRING(15)                ! 
UserCode                    STRING(3)                 ! 
IsChargeable                BYTE                      ! 
ChargeType                  STRING(30)                ! 
IsWarranty                  BYTE                      ! 
WarrantyType                STRING(30)                ! 
TransitType                 STRING(30)                ! 
UnitType                    STRING(30)                ! 
DeliveryText                STRING(255)               ! 
Status                      STRING(30)                ! 
Location                    STRING(30)                ! 
EnvCountStatus              STRING(30)                ! 
                         END
                     END                       

XMLSAM               FILE,DRIVER('Btrieve'),OEM,PRE(XSA),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! Samsung XML Samsung Status Codes
StatusCodeKey            KEY(XSA:StatusCode),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
StatusCode                  STRING(4)                 ! 
                         END
                     END                       

XMLDOCS              FILE,DRIVER('Btrieve'),OEM,PRE(XMD),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                 ! Samsung XML Documents
RecordNoKey              KEY(XMD:RECORD_NO),NOCASE,PRIMARY ! 
TypeKey                  KEY(XMD:TYPE),DUP,NOCASE     ! 
Record                   RECORD,PRE()
RECORD_NO                   LONG                      ! 
ENTRY_DATE                  DATE                      ! 
ENTRY_TIME                  TIME                      ! 
TYPE                        BYTE                      ! 
PATHNAME                    STRING(255)               ! 
                         END
                     END                       

DEFEDI               FILE,DRIVER('Btrieve'),PRE(edi),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)                  ! Defaults - Motorola
record_number_key        KEY(edi:record_number),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
Supplier_Code               STRING(8)                 ! 
record_number               REAL                      ! 
Country_Code                STRING(3)                 ! 
Dealer_ID                   STRING(3)                 ! 
                         END
                     END                       

SIDSRVCN_ALIAS       FILE,DRIVER('Btrieve'),PRE(srv_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
SCAccountIDKey           KEY(srv_ali:SCAccountID),NOCASE,PRIMARY ! 
ServiceCentreNameKey     KEY(srv_ali:ServiceCentreName),DUP,NOCASE ! 
Record                   RECORD,PRE()
SCAccountID                 LONG                      ! 
ServiceCentreName           STRING(30)                ! 
AddressLine1                STRING(30)                ! 
AddressLine2                STRING(30)                ! 
AddressLine3                STRING(30)                ! 
Postcode                    STRING(15)                ! 
AccountActive               BYTE                      ! 
TurnaroundDaysRetail        LONG                      ! 
TurnaroundDaysExch          LONG                      ! 
BankHolidaysFileName        STRING(100)               ! 
ContactName                 STRING(30)                ! Contact Name
ContactNumber               STRING(30)                ! Contact Number
ContactEmail                STRING(255)               ! Contact Email
UseServiceCentreFTP         BYTE                      ! 
FTPServer                   STRING(30)                ! FTP Server
FTPUsername                 STRING(30)                ! FTP Username
FTPPassword                 STRING(30)                ! FTP Password
FTPPath                     STRING(255)               ! FTP Path
EmailToAddress              STRING(255)               ! FTP Email Alert Recipients
ProcessFailureCount         LONG                      ! Send Alert Email After (xx) Successive Process Failures
FTPFailedCount              LONG                      ! 
MonRepairCapacity           LONG                      ! 
TueRepairCapacity           LONG                      ! 
WedRepairCapacity           LONG                      ! 
ThuRepairCapacity           LONG                      ! 
FriRepairCapacity           LONG                      ! 
ExchangeImportType          BYTE                      ! 0 - Not Set, 1 - Model Code, 2 - Oracle Code
                         END
                     END                       

MPXJOBS_ALIAS        FILE,DRIVER('Btrieve'),OEM,PRE(mxj_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)           ! 
RecordNoKey              KEY(mxj_ali:RecordNo),NOCASE,PRIMARY ! 
IDKey                    KEY(mxj_ali:ID),DUP,NOCASE   ! 
JobNoKey                 KEY(mxj_ali:InspACRJobNo),DUP,NOCASE ! 
StateJobNoKey            KEY(mxj_ali:Record_State,mxj_ali:InspACRJobNo),DUP,NOCASE ! 
StateSurnameKey          KEY(mxj_ali:Record_State,mxj_ali:CustLastName),DUP,NOCASE ! 
StatePostcodeKey         KEY(mxj_ali:Record_State,mxj_ali:CustPostCode),DUP,NOCASE ! 
StateModelKey            KEY(mxj_ali:Record_State,mxj_ali:ModelID),DUP,NOCASE ! 
StateReportCodeKey       KEY(mxj_ali:Record_State,mxj_ali:ReportCode),DUP,NOCASE ! 
Record                   RECORD,PRE()
RecordNo                    LONG                      ! 
Record_State                BYTE                      ! 
ReportCode                  LONG                      ! 
Exception_Code              LONG                      ! 
Exception_Desc              STRING(255)               ! 
ImportDate                  DATE                      ! 
ImportTime                  TIME                      ! 
ID                          LONG                      ! 
InspACRJobNo                LONG                      ! 
EnvDateSent                 DATE                      ! 
InspACRStatus               STRING(50)                ! 
InspDateRecd                DATE                      ! 
InspModelID                 STRING(50)                ! 
InspFlag                    BYTE                      ! 
InspAttention               STRING(255)               ! 
InspQty                     LONG                      ! 
InspPackageCond             STRING(50)                ! 
InspQ1                      BYTE                      ! 
InspQ2                      BYTE                      ! 
InspQ3                      BYTE                      ! 
InspACC1                    BYTE                      ! 
InspACC2                    BYTE                      ! 
InspACC3                    BYTE                      ! 
InspACC4                    BYTE                      ! 
InspACC5                    BYTE                      ! 
InspGrade                   STRING(50)                ! 
InspIMEI                    STRING(15)                ! 
InspNotes                   STRING(255)               ! 
InspDate                    DATE                      ! 
InspBrand                   STRING(50)                ! 
InspInWarranty              BYTE                      ! 
DespatchConsignment         STRING(50)                ! 
DespatchDate                DATE                      ! 
CustCTN                     STRING(50)                ! 
QuoteValue                  LONG                      ! 
ModelID                     STRING(50)                ! 
QuoteQ1                     BYTE                      ! 
QuoteQ2                     BYTE                      ! 
QuoteQ3                     BYTE                      ! 
QuoteACC1                   BYTE                      ! 
QuoteACC2                   BYTE                      ! 
QuoteACC3                   BYTE                      ! 
QuoteACC4                   BYTE                      ! 
QuoteACC5                   BYTE                      ! 
CustTitle                   STRING(50)                ! 
CustFirstName               STRING(50)                ! 
CustLastName                STRING(50)                ! 
CustAdd1                    STRING(50)                ! 
CustAdd2                    STRING(50)                ! 
CustAdd3                    STRING(50)                ! 
CustAdd4                    STRING(50)                ! 
CustPostCode                STRING(50)                ! 
CustRef                     STRING(50)                ! 
CustNetwork                 STRING(50)                ! 
EnvLetter                   STRING(50)                ! 
InspType                    STRING(50)                ! 
EnvType                     STRING(50)                ! 
EnvCount                    LONG                      ! 
                         END
                     END                       

MPXSTAT_ALIAS        FILE,DRIVER('Btrieve'),OEM,PRE(mxs_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)           ! 
RecordNoKey              KEY(mxs_ali:RecordNo),NOCASE,PRIMARY ! 
MpxStatusKey             KEY(mxs_ali:MPX_STATUS),DUP,NOCASE ! 
SBStatusKey              KEY(mxs_ali:SB_STATUS),DUP,NOCASE ! 
Record                   RECORD,PRE()
RecordNo                    LONG                      ! 
MPX_STATUS                  LONG                      ! 
SB_STATUS                   STRING(30)                ! 
                         END
                     END                       

XMLJOBS_ALIAS        FILE,DRIVER('Btrieve'),OEM,PRE(xjb_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)           ! 
RecordNumberKey          KEY(xjb_ali:RECORD_NUMBER),NOCASE,PRIMARY ! 
TR_NO_KEY                KEY(xjb_ali:TR_NO),DUP,NOCASE ! 
JobNumberKey             KEY(xjb_ali:JOB_NO),DUP,NOCASE ! 
State_Job_Number_Key     KEY(xjb_ali:RECORD_STATE,xjb_ali:JOB_NO),DUP,NOCASE ! 
State_Surname_Key        KEY(xjb_ali:RECORD_STATE,xjb_ali:CONSUMER_LAST_NAME),DUP,NOCASE ! 
State_Postcode_Key       KEY(xjb_ali:RECORD_STATE,xjb_ali:CONSUMER_POSTCODE),DUP,NOCASE ! 
State_Model_Key          KEY(xjb_ali:RECORD_STATE,xjb_ali:MODEL_CODE),DUP,NOCASE ! 
State_Serial_No_Key      KEY(xjb_ali:RECORD_STATE,xjb_ali:SERIAL_NO),DUP,NOCASE ! 
Record                   RECORD,PRE()
RECORD_NUMBER               LONG                      ! 
RECORD_STATE                BYTE                      ! 
EXCEPTION_CODE              LONG                      ! 
EXCEPTION_DESC              STRING(255)               ! 
JOB_NO                      LONG                      ! 
COMPANY                     STRING(4)                 ! 
TR_NO                       STRING(30)                ! 
ASC_CODE                    STRING(10)                ! 
MODEL_CODE                  STRING(40)                ! 
SERIAL_NO                   STRING(16)                ! 
PURCHASE_DATE               DATE                      ! 
SERVICE_TYPE                STRING(2)                 ! 
IN_OUT_WARRANTY             STRING(1)                 ! 
REQUEST_DATE                DATE                      ! 
REQUEST_TIME                TIME                      ! 
REPAIR_ETD_DATE             DATE                      ! 
REPAIR_ETD_TIME             TIME                      ! 
TR_STATUS                   STRING(2)                 ! 
TR_REASON                   STRING(2)                 ! 
CONSUMER_TITLE              STRING(20)                ! 
CONSUMER_FIRST_NAME         STRING(40)                ! 
CONSUMER_LAST_NAME          STRING(40)                ! 
CONSUMER_TEL_NUMBER1        STRING(30)                ! 
CONSUMER_FAX_NUMBER         STRING(30)                ! 
CONSUMER_EMAIL              STRING(241)               ! 
CONSUMER_COUNTRY            STRING(20)                ! 
CONSUMER_REGION             STRING(20)                ! 
CONSUMER_POSTCODE           STRING(10)                ! 
CONSUMER_CITY               STRING(40)                ! 
CONSUMER_STREET             STRING(60)                ! 
CONSUMER_HOUSE_NUMBER       STRING(20)                ! 
REPAIR_COMPLETE_DATE        DATE                      ! 
REPAIR_COMPLETE_TIME        TIME                      ! 
GOODS_DELIVERY_DATE         DATE                      ! 
GOODS_DELIVERY_TIME         TIME                      ! 
SYMPTOM1_DESC               STRING(40)                ! 
SYMPTOM2_DESC               STRING(40)                ! 
SYMPTOM3_DESC               STRING(40)                ! 
BP_NO                       STRING(10)                ! 
INQUIRY_TEXT                STRING(254)               ! 
                         END
                     END                       

AUDIT_ALIAS          FILE,DRIVER('Btrieve'),PRE(audali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)             ! 
Ref_Number_Key           KEY(audali:Ref_Number,-audali:Date,-audali:Time,audali:Action),DUP,NOCASE ! By Date
Action_Key               KEY(audali:Ref_Number,audali:Action,-audali:Date),DUP,NOCASE ! By Action
User_Key                 KEY(audali:Ref_Number,audali:User,-audali:Date),DUP,NOCASE ! By User
Record_Number_Key        KEY(audali:record_number),NOCASE,PRIMARY ! 
ActionOnlyKey            KEY(audali:Action,audali:Date),DUP,NOCASE ! By Action
TypeRefKey               KEY(audali:Ref_Number,audali:Type,-audali:Date,-audali:Time,audali:Action),DUP,NOCASE ! By Date
TypeActionKey            KEY(audali:Ref_Number,audali:Type,audali:Action,-audali:Date),DUP,NOCASE ! By Action
TypeUserKey              KEY(audali:Ref_Number,audali:Type,audali:User,-audali:Date),DUP,NOCASE ! By User
DateActionJobKey         KEY(audali:Date,audali:Action,audali:Ref_Number),DUP,NOCASE ! By Job Number
DateJobKey               KEY(audali:Date,audali:Ref_Number),DUP,NOCASE ! By Job Number
DateTypeJobKey           KEY(audali:Date,audali:Type,audali:Ref_Number),DUP,NOCASE ! By Job Number
DateTypeActionKey        KEY(audali:Date,audali:Type,audali:Action,audali:Ref_Number),DUP,NOCASE ! By Job Number
Record                   RECORD,PRE()
record_number               REAL                      ! 
Ref_Number                  REAL                      ! 
Date                        DATE                      ! 
Time                        TIME                      ! 
User                        STRING(3)                 ! 
Action                      STRING(80)                ! 
DummyField                  STRING(2)                 ! For File Manager
Notes                       STRING(255)               ! 
Type                        STRING(3)                 ! Job/Exchange/Loan
                         END
                     END                       

XMLSAM_ALIAS         FILE,DRIVER('Btrieve'),OEM,PRE(xsa_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)            ! 
StatusCodeKey            KEY(xsa_ali:StatusCode),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
StatusCode                  STRING(4)                 ! 
                         END
                     END                       

MULDESP_ALIAS        FILE,DRIVER('Btrieve'),OEM,PRE(muld_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)           ! 
RecordNumberKey          KEY(muld_ali:RecordNumber),NOCASE,PRIMARY ! By Record Number
BatchNumberKey           KEY(muld_ali:BatchNumber),DUP,NOCASE ! By Batch Number
AccountNumberKey         KEY(muld_ali:AccountNumber),DUP,NOCASE ! By Account Number
CourierKey               KEY(muld_ali:Courier),DUP,NOCASE ! By Courier
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
BatchNumber                 STRING(30)                ! Batch Number
AccountNumber               STRING(30)                ! Account Number
Courier                     STRING(30)                ! Courier
BatchTotal                  LONG                      ! Total In Batch
                         END
                     END                       

MULDESPJ_ALIAS       FILE,DRIVER('Btrieve'),OEM,PRE(mulj_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
RecordNumberKey          KEY(mulj_ali:RecordNumber),NOCASE,PRIMARY ! By Record Number
JobNumberKey             KEY(mulj_ali:RefNumber,mulj_ali:JobNumber),DUP,NOCASE ! By Job Number
CurrentJobKey            KEY(mulj_ali:RefNumber,mulj_ali:Current,mulj_ali:JobNumber),DUP,NOCASE ! By Job Number
JobNumberOnlyKey         KEY(mulj_ali:JobNumber),DUP,NOCASE ! By Job Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
RefNumber                   LONG                      ! RefNumber
JobNumber                   LONG                      ! Job Number
IMEINumber                  STRING(30)                ! I.M.E.I. Number
MSN                         STRING(30)                ! M.S.N.
AccountNumber               STRING(30)                ! AccountNumber
Courier                     STRING(30)                ! Courier
Current                     BYTE                      ! Current
                         END
                     END                       

SUBCONTALIAS         FILE,DRIVER('Btrieve'),OEM,PRE(SUB1),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)            ! 
RecordNumberKey          KEY(SUB1:RecordNumber),NOCASE,PRIMARY ! By Record Number
AccountNumberKey         KEY(SUB1:AccountNumber),NOCASE ! By Account Number
BusinessNameKey          KEY(SUB1:BusinessName,SUB1:AccountNumber),DUP,NOCASE ! By Business Key
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
ContactName                 STRING(30)                ! Contact Name
AccountNumber               STRING(30)                ! Account Number
BusinessName                STRING(30)                ! Business Name
Postcode                    STRING(20)                ! Postcode
BuildingName                STRING(30)                ! Building Name
AddressLine1                STRING(30)                ! Address
AddressLine2                STRING(30)                ! Address Line 2
AddressLine3                STRING(30)                ! Address Line 3
BusinessNumber              STRING(20)                ! Business Number
FaxNumber                   STRING(20)                ! Fax Number
MobileNumber                STRING(20)                ! Mobile Number
OtherNumber                 STRING(20)                ! Other Number
EmailAddress                STRING(255)               ! E-Mail Address
DailyWorkLoad               LONG                      ! Daily Work Load
Rating                      LONG                      ! Rating
NumInstallers               LONG                      ! 
ExpDatePubLiabIns           DATE                      ! 
                         END
                     END                       

INSJOBSALIAS         FILE,DRIVER('Btrieve'),OEM,PRE(ins1),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)            ! Alias for install jobs
RefNumberKey             KEY(-ins1:RefNumber),NOCASE,PRIMARY ! By Job Number
CompletedKey             KEY(ins1:EndDate,ins1:RefNumber),DUP,NOCASE ! By Job Number
InvoiceNumberKey         KEY(ins1:InvoiceNumber,ins1:RefNumber),DUP,NOCASE ! By Job Number
JobTypeKey               KEY(ins1:JobType),DUP,NOCASE ! 
JobStatusKey             KEY(ins1:JobStatus),DUP,NOCASE ! 
CompanyKey               KEY(ins1:AccountName),DUP,NOCASE ! 
ContactKey               KEY(ins1:ContactName),DUP,NOCASE ! 
PostCodeKey              KEY(ins1:Postcode),DUP,NOCASE ! 
AllocJobKey              KEY(ins1:SubContractor,-ins1:RefNumber),DUP,NOCASE ! 
AllocCompKey             KEY(ins1:SubContractor,ins1:AccountName),DUP,NOCASE ! 
AllocPostCodeKey         KEY(ins1:SubContractor,ins1:Postcode),DUP,NOCASE ! 
AllocStatusKey           KEY(ins1:SubContractor,ins1:JobStatus),DUP,NOCASE ! 
AllocContactKey          KEY(ins1:SubContractor,ins1:ContactName),DUP,NOCASE ! 
KeyClient                KEY(ins1:Client),DUP,NOCASE  ! 
KeyClientCoord           KEY(ins1:Client,ins1:WhoBooked),DUP,NOCASE ! 
KeyAllocClient           KEY(ins1:SubContractor,ins1:WhoBooked,ins1:Client),DUP,NOCASE ! 
KeyCoord                 KEY(ins1:WhoBooked),DUP,NOCASE ! 
KeyAllocCoord            KEY(ins1:SubContractor,ins1:WhoBooked),DUP,NOCASE ! 
Record                   RECORD,PRE()
RefNumber                   LONG                      ! Job Number
DateBooked                  DATE                      ! Date Booked
TimeBooked                  TIME                      ! Time Booked
WhoBooked                   STRING(3)                 ! Who Booked
AccountNumber               STRING(30)                ! Trade Account Number
AccountName                 STRING(30)                ! Trade Account Name
Postcode1                   STRING(4)                 ! Postcode
Postcode2                   STRING(4)                 ! Postcode
BuildingName                STRING(30)                ! Building Name
AddressLine1                STRING(30)                ! Address Line 1
AddressLine2                STRING(30)                ! Address Line 2
AddressLine3                STRING(30)                ! Address Line 3
TelephoneNumber             STRING(30)                ! Telephone Number
FaxNumber                   STRING(30)                ! Fax Number
MobileNumber                STRING(30)                ! Mobile Number
OtherNumber                 STRING(30)                ! Other Telephone Number
ContactName                 STRING(30)                ! Contact Name
EmailAddress                STRING(255)               ! Email Address
OrderNumber                 STRING(30)                ! Trade Order Number
NoOfJobs                    LONG                      ! Number Of Jobs
EngineersOnSite             LONG                      ! Number Of Engineers On Site
SubContractor               STRING(30)                ! Sub Contractor
StartDate                   DATE                      ! Start Date
StartTime                   TIME                      ! Start Time
EndDate                     DATE                      ! Completion Date
EndTime                     TIME                      ! Completion Time
Completed                   BYTE                      ! Job Completed
SpecialInstruct             STRING(255)               ! 
InvoiceNumber               LONG                      ! Invoice Number
DummyField                  STRING(13)                ! For File Manager
AssStock1                   BYTE                      ! 1
AssStock2                   BYTE                      ! 2
AssStock3                   BYTE                      ! 3
AssStock4                   BYTE                      ! 4
AssStock5                   BYTE                      ! 5
AssStock6                   BYTE                      ! 6
AssStock7                   BYTE                      ! 7
AssStock8                   BYTE                      ! 8
AssStock9                   BYTE                      ! 9
AssStock10                  BYTE                      ! 10
AssStock11                  BYTE                      ! 
AssStock12                  BYTE                      ! 
AssStock13                  BYTE                      ! 
AssStock14                  BYTE                      ! 
AssStock15                  BYTE                      ! 
AssStock16                  BYTE                      ! 
JobType                     STRING(30)                ! 
JobStatus                   STRING(30)                ! 
Invoiced                    BYTE                      ! 
Surveyed                    BYTE                      ! 
EngineerInfo                STRING(255)               ! 
Client                      STRING(30)                ! 
DolphinRef                  STRING(20)                ! 
MainJobType                 BYTE                      ! 
CityLinkRef                 STRING(30)                ! 
NoStoreys                   LONG                      ! 
ApproxHeight                LONG                      ! 
Brick                       BYTE                      ! 
Glass                       BYTE                      ! 
House                       CSTRING(20)               ! 
Portacabin                  BYTE                      ! 
CladdingDesign              BYTE                      ! 
POD                         STRING(100)               ! 
Postcode                    STRING(10)                ! 
Projected                   LONG                      ! 
                         END
                     END                       

ESTPARTS_ALIAS       FILE,DRIVER('Btrieve'),PRE(epr_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
Part_Number_Key          KEY(epr_ali:Ref_Number,epr_ali:Part_Number),DUP,NOCASE ! By Part Number
record_number_key        KEY(epr_ali:record_number),NOCASE,PRIMARY ! 
Part_Ref_Number2_Key     KEY(epr_ali:Part_Ref_Number),DUP,NOCASE ! By Part Ref Number
Date_Ordered_Key         KEY(epr_ali:Date_Ordered),DUP,NOCASE ! By Date Ordered
Part_Ref_Number_Key      KEY(epr_ali:Ref_Number,epr_ali:Part_Ref_Number),DUP,NOCASE ! By Part Ref Number
Order_Number_Key         KEY(epr_ali:Ref_Number,epr_ali:Order_Number),DUP,NOCASE ! 
Supplier_Key             KEY(epr_ali:Supplier),DUP,NOCASE ! 
Order_Part_Key           KEY(epr_ali:Ref_Number,epr_ali:Order_Part_Number),DUP,NOCASE ! By Order Part Number
Record                   RECORD,PRE()
Ref_Number                  REAL                      ! 
record_number               REAL                      ! 
Adjustment                  STRING(3)                 ! 
Part_Ref_Number             REAL                      ! 
Part_Details_Group          GROUP                     ! 
Part_Number                   STRING(30)              ! 
Description                   STRING(30)              ! 
Supplier                      STRING(30)              ! 
Purchase_Cost                 REAL                    ! 
Sale_Cost                     REAL                    ! 
Retail_Cost                   REAL                    ! 
                            END                       ! 
Quantity                    REAL                      ! 
Warranty_Part               STRING(3)                 ! 
Exclude_From_Order          STRING(3)                 ! 
Despatch_Note_Number        STRING(30)                ! 
Date_Ordered                DATE                      ! 
Pending_Ref_Number          REAL                      ! 
Order_Number                REAL                      ! 
Date_Received               DATE                      ! 
Order_Part_Number           REAL                      ! 
Status_Date                 DATE                      ! 
Fault_Codes1                GROUP                     ! 
Fault_Code1                   STRING(30)              ! 
Fault_Code2                   STRING(30)              ! 
Fault_Code3                   STRING(30)              ! 
Fault_Code4                   STRING(30)              ! 
Fault_Code5                   STRING(30)              ! 
Fault_Code6                   STRING(30)              ! 
                            END                       ! 
Fault_Codes2                GROUP                     ! 
Fault_Code7                   STRING(30)              ! 
Fault_Code8                   STRING(30)              ! 
Fault_Code9                   STRING(30)              ! 
Fault_Code10                  STRING(30)              ! 
Fault_Code11                  STRING(30)              ! 
Fault_Code12                  STRING(30)              ! 
                            END                       ! 
                         END
                     END                       

IEQUIP_ALIAS         FILE,DRIVER('Btrieve'),OEM,PRE(iequ_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)            ! 
RecordNumberKey          KEY(iequ_ali:RecordNumber),NOCASE,PRIMARY ! By Record Number
JobPartNumberKey         KEY(iequ_ali:RefNumber,iequ_ali:PartNumber),DUP,NOCASE ! By Part Number
RefPendRefKey            KEY(iequ_ali:RefNumber,iequ_ali:PendingRefNumber),DUP,NOCASE ! By Pending Ref Number
ManufacturerKey          KEY(iequ_ali:Manufacturer),DUP,NOCASE ! 
BrwManufacturer          KEY(iequ_ali:RefNumber,iequ_ali:Manufacturer),DUP,NOCASE ! 
KeyPartNumber            KEY(iequ_ali:PartNumber),DUP,NOCASE ! 
KeyDescription           KEY(iequ_ali:Description),DUP,NOCASE ! 
KeyBrwDescription        KEY(iequ_ali:RefNumber,iequ_ali:Description),DUP,NOCASE ! 
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
RefNumber                   LONG                      ! Job Reference Number
PartNumber                  STRING(30)                ! Part Number
Description                 STRING(30)                ! Description
PurchaseCost                REAL                      ! Purchase Cost
SaleCost                    REAL                      ! Sale Cost
Supplier                    STRING(30)                ! Supplier
Quantity                    LONG                      ! Quantity
ExcludeFromOrder            BYTE                      ! Exclude From Order
DespatchNoteNumber          STRING(30)                ! Despatch Note Number
PartRefNumber               LONG                      ! Stock Part Number
DateOrdered                 DATE                      ! Date Ordered
PendingRefNumber            LONG                      ! Pending Part Number
OrderNumber                 LONG                      ! Order Number
OrderPartNumber             LONG                      ! Order Part Number
DateReceived                DATE                      ! Date Received
Manufacturer                STRING(30)                ! 
Dummy                       STRING(3)                 ! 
                         END
                     END                       

JOBS2_ALIAS          FILE,DRIVER('Btrieve'),PRE(job2),BINDABLE,CREATE,THREAD,EXTERNAL(''),DLL(dll_mode)             ! 
Ref_Number_Key           KEY(job2:Ref_Number),NOCASE,PRIMARY ! By Job Number
Model_Unit_Key           KEY(job2:Model_Number,job2:Unit_Type),DUP,NOCASE ! By Job Number
EngCompKey               KEY(job2:Engineer,job2:Completed,job2:Ref_Number),DUP,NOCASE ! By Job Number
EngWorkKey               KEY(job2:Engineer,job2:Workshop,job2:Ref_Number),DUP,NOCASE ! By Job Number
Surname_Key              KEY(job2:Surname),DUP,NOCASE ! By Surname
MobileNumberKey          KEY(job2:Mobile_Number),DUP,NOCASE ! By Mobile Number
ESN_Key                  KEY(job2:ESN),DUP,NOCASE     ! By E.S.N. / I.M.E.I.
MSN_Key                  KEY(job2:MSN),DUP,NOCASE     ! By M.S.N.
AccountNumberKey         KEY(job2:Account_Number,job2:Ref_Number),DUP,NOCASE ! By Account Number
AccOrdNoKey              KEY(job2:Account_Number,job2:Order_Number),DUP,NOCASE ! By Order Number
Model_Number_Key         KEY(job2:Model_Number),DUP,NOCASE ! By Model Number
Engineer_Key             KEY(job2:Engineer,job2:Model_Number),DUP,NOCASE ! By Engineer
Date_Booked_Key          KEY(job2:date_booked),DUP,NOCASE ! By Date Booked
DateCompletedKey         KEY(job2:Date_Completed),DUP,NOCASE ! By Date Completed
ModelCompKey             KEY(job2:Model_Number,job2:Date_Completed),DUP,NOCASE ! By Completed Date
By_Status                KEY(job2:Current_Status,job2:Ref_Number),DUP,NOCASE ! By Job Number
StatusLocKey             KEY(job2:Current_Status,job2:Location,job2:Ref_Number),DUP,NOCASE ! By Job Number
Location_Key             KEY(job2:Location,job2:Ref_Number),DUP,NOCASE ! By Location
Third_Party_Key          KEY(job2:Third_Party_Site,job2:Third_Party_Printed,job2:Ref_Number),DUP,NOCASE ! By Third Party
ThirdEsnKey              KEY(job2:Third_Party_Site,job2:Third_Party_Printed,job2:ESN),DUP,NOCASE ! By ESN
ThirdMsnKey              KEY(job2:Third_Party_Site,job2:Third_Party_Printed,job2:MSN),DUP,NOCASE ! By MSN
PriorityTypeKey          KEY(job2:Job_Priority,job2:Ref_Number),DUP,NOCASE ! By Priority
Unit_Type_Key            KEY(job2:Unit_Type),DUP,NOCASE ! By Unit Type
EDI_Key                  KEY(job2:Manufacturer,job2:EDI,job2:EDI_Batch_Number,job2:Ref_Number),DUP,NOCASE ! By Job Number
InvoiceNumberKey         KEY(job2:Invoice_Number),DUP,NOCASE ! By Invoice_Number
WarInvoiceNoKey          KEY(job2:Invoice_Number_Warranty),DUP,NOCASE ! By Invoice Number
Batch_Number_Key         KEY(job2:Batch_Number,job2:Ref_Number),DUP,NOCASE ! By Job Number
Batch_Status_Key         KEY(job2:Batch_Number,job2:Current_Status,job2:Ref_Number),DUP,NOCASE ! By Ref Number
BatchModelNoKey          KEY(job2:Batch_Number,job2:Model_Number,job2:Ref_Number),DUP,NOCASE ! By Job Number
BatchInvoicedKey         KEY(job2:Batch_Number,job2:Invoice_Number,job2:Ref_Number),DUP,NOCASE ! By Ref Number
BatchCompKey             KEY(job2:Batch_Number,job2:Completed,job2:Ref_Number),DUP,NOCASE ! By Job Number
ChaInvoiceKey            KEY(job2:Chargeable_Job,job2:Account_Number,job2:Invoice_Number,job2:Ref_Number),DUP,NOCASE ! 
InvoiceExceptKey         KEY(job2:Invoice_Exception,job2:Ref_Number),DUP,NOCASE ! By Job Number
ConsignmentNoKey         KEY(job2:Consignment_Number),DUP,NOCASE ! By Cosignment Number
InConsignKey             KEY(job2:Incoming_Consignment_Number),DUP,NOCASE ! By Consignment Number
ReadyToDespKey           KEY(job2:Despatched,job2:Ref_Number),DUP,NOCASE ! By Job Number
ReadyToTradeKey          KEY(job2:Despatched,job2:Account_Number,job2:Ref_Number),DUP,NOCASE ! By Job Number
ReadyToCouKey            KEY(job2:Despatched,job2:Current_Courier,job2:Ref_Number),DUP,NOCASE ! By Job Number
ReadyToAllKey            KEY(job2:Despatched,job2:Account_Number,job2:Current_Courier,job2:Ref_Number),DUP,NOCASE ! By Job Number
DespJobNumberKey         KEY(job2:Despatch_Number,job2:Ref_Number),DUP,NOCASE ! By Job Number
DateDespatchKey          KEY(job2:Courier,job2:Date_Despatched,job2:Ref_Number),DUP,NOCASE ! By Job Number
DateDespLoaKey           KEY(job2:Loan_Courier,job2:Loan_Despatched,job2:Ref_Number),DUP,NOCASE ! By Job Number
DateDespExcKey           KEY(job2:Exchange_Courier,job2:Exchange_Despatched,job2:Ref_Number),DUP,NOCASE ! By Job Number
ChaRepTypeKey            KEY(job2:Repair_Type),DUP,NOCASE ! By Chargeable Repair Type
WarRepTypeKey            KEY(job2:Repair_Type_Warranty),DUP,NOCASE ! By Warranty Repair Type
ChaTypeKey               KEY(job2:Charge_Type),DUP,NOCASE ! 
WarChaTypeKey            KEY(job2:Warranty_Charge_Type),DUP,NOCASE ! 
Bouncer_Key              KEY(job2:Bouncer,job2:Ref_Number),DUP,NOCASE ! By Job Number
EngDateCompKey           KEY(job2:Engineer,job2:Date_Completed),DUP,NOCASE ! By Date Completed
ExcStatusKey             KEY(job2:Exchange_Status,job2:Ref_Number),DUP,NOCASE ! By Job Number
LoanStatusKey            KEY(job2:Loan_Status,job2:Ref_Number),DUP,NOCASE ! By Job Number
ExchangeLocKey           KEY(job2:Exchange_Status,job2:Location,job2:Ref_Number),DUP,NOCASE ! By Job Number
LoanLocKey               KEY(job2:Loan_Status,job2:Location,job2:Ref_Number),DUP,NOCASE ! By Job Number
BatchJobKey              KEY(job2:InvoiceAccount,job2:InvoiceBatch,job2:Ref_Number),DUP,NOCASE ! By Job Number
BatchStatusKey           KEY(job2:InvoiceAccount,job2:InvoiceBatch,job2:InvoiceStatus,job2:Ref_Number),DUP,NOCASE ! By Job Number
Record                   RECORD,PRE()
Ref_Number                  LONG                      ! 
Batch_Number                LONG                      ! 
Internal_Status             STRING(10)                ! 
Auto_Search                 STRING(30)                ! 
who_booked                  STRING(3)                 ! 
date_booked                 DATE                      ! 
time_booked                 TIME                      ! 
Cancelled                   STRING(3)                 ! 
Bouncer                     STRING(8)                 ! 
Bouncer_Type                STRING(3)                 ! Type Of Bouncer  CHArgeable / WARranty / BOTh
Web_Type                    STRING(3)                 ! Warranty/Insurance/Chargeable
Warranty_Job                STRING(3)                 ! 
Chargeable_Job              STRING(3)                 ! 
Model_Number                STRING(30)                ! 
Manufacturer                STRING(30)                ! 
ESN                         STRING(20)                ! 
MSN                         STRING(20)                ! 
ProductCode                 STRING(30)                ! 
Unit_Type                   STRING(30)                ! 
Colour                      STRING(30)                ! 
Location_Type               STRING(10)                ! 
Phone_Lock                  STRING(30)                ! 
Workshop                    STRING(3)                 ! 
Location                    STRING(30)                ! 
Authority_Number            STRING(30)                ! 
Insurance_Reference_Number  STRING(30)                ! 
DOP                         DATE                      ! 
Insurance                   STRING(3)                 ! 
Insurance_Type              STRING(30)                ! 
Transit_Type                STRING(30)                ! 
Physical_Damage             STRING(3)                 ! 
Intermittent_Fault          STRING(3)                 ! 
Loan_Status                 STRING(30)                ! 
Exchange_Status             STRING(30)                ! 
Job_Priority                STRING(30)                ! 
Charge_Type                 STRING(30)                ! 
Warranty_Charge_Type        STRING(30)                ! 
Current_Status              STRING(30)                ! 
Account_Number              STRING(15)                ! 
Trade_Account_Name          STRING(30)                ! 
Department_Name             STRING(30)                ! 
Order_Number                STRING(30)                ! 
POP                         STRING(3)                 ! 
In_Repair                   STRING(3)                 ! 
Date_In_Repair              DATE                      ! 
Time_In_Repair              TIME                      ! 
On_Test                     STRING(3)                 ! 
Date_On_Test                DATE                      ! 
Time_On_Test                TIME                      ! 
Estimate_Ready              STRING(3)                 ! 
QA_Passed                   STRING(3)                 ! 
Date_QA_Passed              DATE                      ! 
Time_QA_Passed              TIME                      ! 
QA_Rejected                 STRING(3)                 ! 
Date_QA_Rejected            DATE                      ! 
Time_QA_Rejected            TIME                      ! 
QA_Second_Passed            STRING(3)                 ! 
Date_QA_Second_Passed       DATE                      ! 
Time_QA_Second_Passed       TIME                      ! 
Title                       STRING(4)                 ! 
Initial                     STRING(1)                 ! 
Surname                     STRING(30)                ! 
Postcode                    STRING(10)                ! 
Company_Name                STRING(30)                ! 
Address_Line1               STRING(30)                ! 
Address_Line2               STRING(30)                ! 
Address_Line3               STRING(30)                ! 
Telephone_Number            STRING(15)                ! 
Fax_Number                  STRING(15)                ! 
Mobile_Number               STRING(15)                ! 
Postcode_Collection         STRING(10)                ! 
Company_Name_Collection     STRING(30)                ! 
Address_Line1_Collection    STRING(30)                ! 
Address_Line2_Collection    STRING(30)                ! 
Address_Line3_Collection    STRING(30)                ! 
Telephone_Collection        STRING(15)                ! 
Postcode_Delivery           STRING(10)                ! 
Address_Line1_Delivery      STRING(30)                ! 
Company_Name_Delivery       STRING(30)                ! 
Address_Line2_Delivery      STRING(30)                ! 
Address_Line3_Delivery      STRING(30)                ! 
Telephone_Delivery          STRING(15)                ! 
Date_Completed              DATE                      ! 
Time_Completed              TIME                      ! 
Completed                   STRING(3)                 ! 
Paid                        STRING(3)                 ! 
Paid_Warranty               STRING(3)                 ! 
Date_Paid                   DATE                      ! 
Repair_Type                 STRING(30)                ! 
Repair_Type_Warranty        STRING(30)                ! 
Engineer                    STRING(3)                 ! 
Ignore_Chargeable_Charges   STRING(3)                 ! 
Ignore_Warranty_Charges     STRING(3)                 ! 
Ignore_Estimate_Charges     STRING(3)                 ! 
Courier_Cost                REAL                      ! 
Advance_Payment             REAL                      ! 
Labour_Cost                 REAL                      ! 
Parts_Cost                  REAL                      ! 
Sub_Total                   REAL                      ! 
Courier_Cost_Estimate       REAL                      ! 
Labour_Cost_Estimate        REAL                      ! 
Parts_Cost_Estimate         REAL                      ! 
Sub_Total_Estimate          REAL                      ! 
Courier_Cost_Warranty       REAL                      ! 
Labour_Cost_Warranty        REAL                      ! 
Parts_Cost_Warranty         REAL                      ! 
Sub_Total_Warranty          REAL                      ! 
Loan_Issued_Date            DATE                      ! 
Loan_Unit_Number            LONG                      ! 
Loan_accessory              STRING(3)                 ! 
Loan_User                   STRING(3)                 ! 
Loan_Courier                STRING(30)                ! 
Loan_Consignment_Number     STRING(30)                ! 
Loan_Despatched             DATE                      ! 
Loan_Despatched_User        STRING(3)                 ! 
Loan_Despatch_Number        LONG                      ! 
LoaService                  STRING(1)                 ! Loan Service For City Link / LabelG
Exchange_Unit_Number        LONG                      ! 
Exchange_Authorised         STRING(3)                 ! 
Loan_Authorised             STRING(3)                 ! 
Exchange_Accessory          STRING(3)                 ! 
Exchange_Issued_Date        DATE                      ! 
Exchange_User               STRING(3)                 ! 
Exchange_Courier            STRING(30)                ! 
Exchange_Consignment_Number STRING(30)                ! 
Exchange_Despatched         DATE                      ! 
Exchange_Despatched_User    STRING(3)                 ! 
Exchange_Despatch_Number    LONG                      ! 
ExcService                  STRING(1)                 ! Exchange Service For City Link / Label G
Date_Despatched             DATE                      ! 
Despatch_Number             LONG                      ! 
Despatch_User               STRING(3)                 ! 
Courier                     STRING(30)                ! 
Consignment_Number          STRING(30)                ! 
Incoming_Courier            STRING(30)                ! 
Incoming_Consignment_Number STRING(30)                ! 
Incoming_Date               DATE                      ! 
Despatched                  STRING(3)                 ! 
Despatch_Type               STRING(3)                 ! 
Current_Courier             STRING(30)                ! 
JobService                  STRING(1)                 ! Job Service For City Link / Label G
Last_Repair_Days            REAL                      ! 
Third_Party_Site            STRING(30)                ! 
Estimate                    STRING(3)                 ! 
Estimate_If_Over            REAL                      ! 
Estimate_Accepted           STRING(3)                 ! 
Estimate_Rejected           STRING(3)                 ! 
Third_Party_Printed         STRING(3)                 ! 
ThirdPartyDateDesp          DATE                      ! 
Fault_Code1                 STRING(30)                ! 
Fault_Code2                 STRING(30)                ! 
Fault_Code3                 STRING(30)                ! 
Fault_Code4                 STRING(30)                ! 
Fault_Code5                 STRING(30)                ! 
Fault_Code6                 STRING(30)                ! 
Fault_Code7                 STRING(30)                ! 
Fault_Code8                 STRING(30)                ! 
Fault_Code9                 STRING(30)                ! 
Fault_Code10                STRING(255)               ! 
Fault_Code11                STRING(255)               ! 
Fault_Code12                STRING(255)               ! 
PreviousStatus              STRING(30)                ! 
StatusUser                  STRING(3)                 ! User who last changed the status
Status_End_Date             DATE                      ! 
Status_End_Time             TIME                      ! 
Turnaround_End_Date         DATE                      ! 
Turnaround_End_Time         TIME                      ! 
Turnaround_Time             STRING(30)                ! 
Special_Instructions        STRING(30)                ! 
InvoiceAccount              STRING(15)                ! Invoice Batch Account Number
InvoiceStatus               STRING(3)                 ! Proforma Status (UNA - Unathorised / AUT - Authorised / QUE - Query)
InvoiceBatch                LONG                      ! Proforma Batch Number
InvoiceQuery                STRING(255)               ! Query Reason
EDI                         STRING(3)                 ! 
EDI_Batch_Number            REAL                      ! 
Invoice_Exception           STRING(3)                 ! 
Invoice_Failure_Reason      STRING(80)                ! 
Invoice_Number              LONG                      ! 
Invoice_Date                DATE                      ! 
Invoice_Date_Warranty       DATE                      ! 
Invoice_Courier_Cost        REAL                      ! 
Invoice_Labour_Cost         REAL                      ! 
Invoice_Parts_Cost          REAL                      ! 
Invoice_Sub_Total           REAL                      ! 
Invoice_Number_Warranty     LONG                      ! 
WInvoice_Courier_Cost       REAL                      ! 
WInvoice_Labour_Cost        REAL                      ! 
WInvoice_Parts_Cost         REAL                      ! 
WInvoice_Sub_Total          REAL                      ! 
                         END
                     END                       

LOGASSST_ALIAS       FILE,DRIVER('Btrieve'),OEM,PRE(logast_alias),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
RecordNumberKey          KEY(logast_alias:RecordNumber),NOCASE,PRIMARY ! By Record Number
RefNumberKey             KEY(logast_alias:RefNumber,logast_alias:PartNumber),DUP,NOCASE ! By Ref Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! 
RefNumber                   LONG                      ! 
PartNumber                  STRING(30)                ! Part Number
Description                 STRING(30)                ! 
PartRefNumber               LONG                      ! Relation To Stock Control
Location                    STRING(30)                ! 
DummyField                  STRING(1)                 ! 
                         END
                     END                       

LOGSTOCK_ALIAS       FILE,DRIVER('Btrieve'),OEM,PRE(logsto_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
RefNumberKey             KEY(logsto_ali:RefNumber),NOCASE,PRIMARY ! By Ref Number
SalesKey                 KEY(logsto_ali:SalesCode),DUP,NOCASE ! By Sales Code
DescriptionKey           KEY(logsto_ali:Description),DUP,NOCASE ! By Description
SalesModelNoKey          KEY(logsto_ali:SalesCode,logsto_ali:ModelNumber),NOCASE ! By Sales Code
RefModelNoKey            KEY(logsto_ali:RefNumber,logsto_ali:ModelNumber),DUP,NOCASE ! By ModelNumber
ModelNumberKey           KEY(logsto_ali:ModelNumber),DUP,NOCASE ! 
Record                   RECORD,PRE()
RefNumber                   LONG                      ! 
SalesCode                   STRING(30)                ! 
Description                 STRING(30)                ! 
ModelNumber                 STRING(30)                ! 
DummyField                  STRING(4)                 ! 
                         END
                     END                       

LOGSERST_ALIAS       FILE,DRIVER('Btrieve'),OEM,PRE(logser_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
RecordNumberKey          KEY(logser_ali:RecordNumber),NOCASE,PRIMARY ! By Record Number
ESNKey                   KEY(logser_ali:ESN),DUP,NOCASE ! By I.M.E.I.
RefNumberKey             KEY(logser_ali:RefNumber,logser_ali:ESN),DUP,NOCASE ! By Ref Number
ESNStatusKey             KEY(logser_ali:RefNumber,logser_ali:Status,logser_ali:ESN),DUP,NOCASE ! By E.S.N. / I.M.E.I.
NokiaStatusKey           KEY(logser_ali:RefNumber,logser_ali:Status,logser_ali:ClubNokia,logser_ali:ESN),DUP,NOCASE ! By Club Nokia Site
AllocNo_Key              KEY(logser_ali:AllocNo,logser_ali:Status,logser_ali:ESN),DUP,NOCASE ! 
Status_Alloc_Key         KEY(logser_ali:RefNumber,logser_ali:ClubNokia,logser_ali:Status),DUP,NOCASE ! 
Record                   RECORD,PRE()
RecordNumber                LONG                      ! 
RefNumber                   LONG                      ! 
ESN                         STRING(30)                ! 
Status                      STRING(10)                ! 
ClubNokia                   STRING(30)                ! Club Nokia Site
AllocNo                     LONG                      ! 
DummyField                  STRING(4)                 ! For File Manager
                         END
                     END                       

COURIER_ALIAS        FILE,DRIVER('Btrieve'),PRE(cou_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)           ! 
Courier_Key              KEY(cou_ali:Courier),NOCASE,PRIMARY ! By Courier
Courier_Type_Key         KEY(cou_ali:Courier_Type,cou_ali:Courier),DUP,NOCASE ! By Courier_Type
Record                   RECORD,PRE()
Courier                     STRING(30)                ! 
Account_Number              STRING(15)                ! 
Postcode                    STRING(10)                ! 
Address_Line1               STRING(30)                ! 
Address_Line2               STRING(30)                ! 
Address_Line3               STRING(30)                ! 
Telephone_Number            STRING(15)                ! 
Fax_Number                  STRING(15)                ! 
Contact_Name                STRING(60)                ! 
Export_Path                 STRING(255)               ! 
Include_Estimate            STRING(3)                 ! 
Include_Chargeable          STRING(3)                 ! 
Include_Warranty            STRING(3)                 ! 
Service                     STRING(15)                ! 
Import_Path                 STRING(255)               ! 
Courier_Type                STRING(30)                ! 
Last_Despatch_Time          TIME                      ! 
ICOLSuffix                  STRING(3)                 ! ANC ICOL File Suffix
ContractNumber              STRING(20)                ! ANC Contract Number
ANCPath                     STRING(255)               ! Path to ANCPAPER.EXE  (I.e. s:\apps\service)
ANCCount                    LONG                      ! Count Of Each ANC Export
DespatchClose               STRING(3)                 ! Despatch At Closing
LabGOptions                 STRING(60)                ! Label G Command Line Options
CustomerCollection          BYTE                      ! Customer Collection
NewStatus                   STRING(30)                ! On Despatch Change Status To
NoOfDespatchNotes           LONG                      ! No Of Despatch Notes
AutoConsignmentNo           BYTE                      ! Auto Allocate Consignment Number
LastConsignmentNo           LONG                      ! Last Allocated Consignment Number
PrintLabel                  BYTE                      ! Print Label
                         END
                     END                       

JOBNOTES_ALIAS       FILE,DRIVER('Btrieve'),OEM,PRE(jbn_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
RefNumberKey             KEY(jbn_ali:RefNumber),NOCASE,PRIMARY ! By Job Number
Record                   RECORD,PRE()
RefNumber                   LONG                      ! Related Job Number
Fault_Description           STRING(255)               ! 
Engineers_Notes             STRING(255)               ! 
Invoice_Text                STRING(255)               ! 
Collection_Text             STRING(255)               ! 
Delivery_Text               STRING(255)               ! 
ColContatName               STRING(30)                ! Contact Name
ColDepartment               STRING(30)                ! Department
DelContactName              STRING(30)                ! Contact Name
DelDepartment               STRING(30)                ! Department
                         END
                     END                       

TRDBATCH_ALIAS       FILE,DRIVER('Btrieve'),OEM,PRE(trb_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
RecordNumberKey          KEY(trb_ali:RecordNumber),NOCASE,PRIMARY ! By Record Number
Batch_Number_Key         KEY(trb_ali:Company_Name,trb_ali:Batch_Number),DUP,NOCASE ! By Job Number
Batch_Number_Status_Key  KEY(trb_ali:Status,trb_ali:Batch_Number),DUP,NOCASE ! By Batch Number
BatchOnlyKey             KEY(trb_ali:Batch_Number),DUP,NOCASE ! By Batch Number
Batch_Company_Status_Key KEY(trb_ali:Status,trb_ali:Company_Name,trb_ali:Batch_Number),DUP,NOCASE ! By Batch Number
ESN_Only_Key             KEY(trb_ali:ESN),DUP,NOCASE  ! By ESN
ESN_Status_Key           KEY(trb_ali:Status,trb_ali:ESN),DUP,NOCASE ! By ESN
Company_Batch_ESN_Key    KEY(trb_ali:Company_Name,trb_ali:Batch_Number,trb_ali:ESN),DUP,NOCASE ! By ESN
AuthorisationKey         KEY(trb_ali:AuthorisationNo),DUP,NOCASE ! By Authorisation Number
StatusAuthKey            KEY(trb_ali:Status,trb_ali:AuthorisationNo),DUP,NOCASE ! By Status
CompanyDateKey           KEY(trb_ali:Company_Name,trb_ali:Status,trb_ali:DateReturn),DUP,NOCASE ! By Date
JobStatusKey             KEY(trb_ali:Status,trb_ali:Ref_Number),DUP,NOCASE ! By Job Number
JobNumberKey             KEY(trb_ali:Ref_Number),DUP,NOCASE ! By Job Number
Record                   RECORD,PRE()
RecordNumber                LONG                      ! 
Batch_Number                LONG                      ! 
Company_Name                STRING(30)                ! 
Ref_Number                  LONG                      ! Job Number
ESN                         STRING(20)                ! 
Status                      STRING(3)                 ! Out or In
Date                        DATE                      ! 
Time                        TIME                      ! 
AuthorisationNo             STRING(30)                ! Authorisation Number for the Despatched Batch
Exchanged                   STRING(3)                 ! Has the Job got an Exchange Unit attached
DateReturn                  DATE                      ! Date The Unit Was Returned
TimeReturn                  TIME                      ! Time Unit Returned
TurnTime                    LONG                      ! Turnaround Time
MSN                         STRING(30)                ! M.S.N.
                         END
                     END                       

INVOICE_ALIAS        FILE,DRIVER('Btrieve'),PRE(inv_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)           ! 
Invoice_Number_Key       KEY(inv_ali:Invoice_Number),NOCASE,PRIMARY ! By Invoice Number
Invoice_Type_Key         KEY(inv_ali:Invoice_Type,inv_ali:Invoice_Number),DUP,NOCASE ! By Invoice_Number
Account_Number_Type_Key  KEY(inv_ali:Invoice_Type,inv_ali:Account_Number,inv_ali:Invoice_Number),DUP,NOCASE ! By Invoice Number
Account_Number_Key       KEY(inv_ali:Account_Number,inv_ali:Invoice_Number),DUP,NOCASE ! By Invoice Number
Date_Created_Key         KEY(inv_ali:Date_Created),DUP,NOCASE ! By Invoice Date
Batch_Number_Key         KEY(inv_ali:Manufacturer,inv_ali:Batch_Number),DUP,NOCASE ! By Batch Number
Record                   RECORD,PRE()
Invoice_Number              REAL                      ! 
Invoice_Type                STRING(3)                 ! 
Job_Number                  REAL                      ! 
Date_Created                DATE                      ! 
Account_Number              STRING(15)                ! 
AccountType                 STRING(3)                 ! Main Or Sub (MAI/SUB)
Total                       REAL                      ! 
Vat_Rate_Labour             REAL                      ! 
Vat_Rate_Parts              REAL                      ! 
Vat_Rate_Retail             REAL                      ! 
VAT_Number                  STRING(30)                ! 
Invoice_VAT_Number          STRING(30)                ! 
Currency                    STRING(30)                ! 
Batch_Number                REAL                      ! 
Manufacturer                STRING(30)                ! 
Claim_Reference             STRING(30)                ! 
Total_Claimed               REAL                      ! 
Courier_Paid                REAL                      ! 
Labour_Paid                 REAL                      ! 
Parts_Paid                  REAL                      ! 
Reconciled_Date             DATE                      ! 
jobs_count                  REAL                      ! 
PrevInvoiceNo               LONG                      ! Link To Previous Invoice Number for Credit Notes
InvoiceCredit               STRING(3)                 ! Invoice Type: Invoice or Credit
UseAlternativeAddress       BYTE                      ! Use Alternative Address
EuroExhangeRate             REAL                      ! Euro Exchange Rate
                         END
                     END                       

ORDPEND_ALIAS        FILE,DRIVER('Btrieve'),PRE(ope_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)           ! 
Ref_Number_Key           KEY(ope_ali:Ref_Number),NOCASE,PRIMARY ! By Ref Number
Supplier_Key             KEY(ope_ali:Supplier,ope_ali:Part_Number),DUP,NOCASE ! By Supplier
DescriptionKey           KEY(ope_ali:Supplier,ope_ali:Description),DUP,NOCASE ! By Description
Supplier_Name_Key        KEY(ope_ali:Supplier),DUP,NOCASE ! By Supplier
Part_Ref_Number_Key      KEY(ope_ali:Part_Ref_Number),DUP,NOCASE ! By Stock Ref Number
Awaiting_Supplier_Key    KEY(ope_ali:Awaiting_Stock,ope_ali:Supplier,ope_ali:Part_Number),DUP,NOCASE ! By Part Number
PartRecordNumberKey      KEY(ope_ali:PartRecordNumber),DUP,NOCASE ! By Record Number
Job_Number_Key           KEY(ope_ali:Job_Number),DUP,NOCASE ! By Job Number
Supplier_Job_Key         KEY(ope_ali:Supplier,ope_ali:Job_Number),DUP,NOCASE ! 
Record                   RECORD,PRE()
Ref_Number                  LONG                      ! 
Part_Ref_Number             LONG                      ! 
Job_Number                  LONG                      ! 
Part_Type                   STRING(3)                 ! 
Supplier                    STRING(30)                ! 
Part_Number                 STRING(30)                ! 
Description                 STRING(30)                ! 
Quantity                    LONG                      ! 
Account_Number              STRING(15)                ! 
Awaiting_Stock              STRING(3)                 ! 
Reason                      STRING(255)               ! Reason
PartRecordNumber            LONG                      ! Part Record Number
                         END
                     END                       

RETSALES_ALIAS       FILE,DRIVER('Btrieve'),OEM,PRE(res_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
Ref_Number_Key           KEY(res_ali:Ref_Number),NOCASE,PRIMARY ! By Sales Number
Invoice_Number_Key       KEY(res_ali:Invoice_Number),DUP,NOCASE ! By Invoice Number
Despatched_Purchase_Key  KEY(res_ali:Despatched,res_ali:Purchase_Order_Number),DUP,NOCASE ! By Purchase Order No
Despatched_Ref_Key       KEY(res_ali:Despatched,res_ali:Ref_Number),DUP,NOCASE ! By Sales Number
Despatched_Account_Key   KEY(res_ali:Despatched,res_ali:Account_Number,res_ali:Purchase_Order_Number),DUP,NOCASE ! By Account No
Account_Number_Key       KEY(res_ali:Account_Number,res_ali:Ref_Number),DUP,NOCASE ! By Account Number
Purchase_Number_Key      KEY(res_ali:Purchase_Order_Number),DUP,NOCASE ! By Purchase Order No
Despatch_Number_Key      KEY(res_ali:Despatch_Number),DUP,NOCASE ! By Despatch_Number
Date_Booked_Key          KEY(res_ali:date_booked),DUP,NOCASE ! By Date Booked
AccountInvoiceKey        KEY(res_ali:Account_Number,res_ali:Invoice_Number),DUP,NOCASE ! By Invoice Number
DespatchedPurchDateKey   KEY(res_ali:Despatched,res_ali:Purchase_Order_Number,res_ali:date_booked),DUP,NOCASE ! By Date
DespatchNoRefNoKey       KEY(res_ali:Despatch_Number,res_ali:Ref_Number),DUP,NOCASE ! By Ref Number
Record                   RECORD,PRE()
Ref_Number                  LONG                      ! 
who_booked                  STRING(3)                 ! 
date_booked                 DATE                      ! 
time_booked                 TIME                      ! 
Account_Number              STRING(15)                ! 
Contact_Name                STRING(30)                ! 
Purchase_Order_Number       STRING(30)                ! 
Delivery_Collection         STRING(3)                 ! 
Payment_Method              STRING(3)                 ! 
Courier_Cost                REAL                      ! 
Parts_Cost                  REAL                      ! 
Sub_Total                   REAL                      ! 
Courier                     STRING(30)                ! 
Consignment_Number          STRING(30)                ! 
Date_Despatched             DATE                      ! 
Despatched                  STRING(3)                 ! 
Despatch_Number             LONG                      ! 
Invoice_Number              LONG                      ! 
Invoice_Date                DATE                      ! 
Invoice_Courier_Cost        REAL                      ! 
Invoice_Parts_Cost          REAL                      ! 
Invoice_Sub_Total           REAL                      ! 
WebOrderNumber              LONG                      ! Web Order Number
WebDateCreated              DATE                      ! Date Created
WebTimeCreated              STRING(20)                ! Time Created
WebCreatedUser              STRING(30)                ! Web User
Postcode                    STRING(10)                ! 
Company_Name                STRING(30)                ! 
Building_Name               STRING(30)                ! 
Address_Line1               STRING(30)                ! 
Address_Line2               STRING(30)                ! 
Address_Line3               STRING(30)                ! 
Telephone_Number            STRING(15)                ! 
Fax_Number                  STRING(15)                ! 
Postcode_Delivery           STRING(10)                ! 
Company_Name_Delivery       STRING(30)                ! 
Building_Name_Delivery      STRING(30)                ! 
Address_Line1_Delivery      STRING(30)                ! 
Address_Line2_Delivery      STRING(30)                ! 
Address_Line3_Delivery      STRING(30)                ! 
Telephone_Delivery          STRING(15)                ! 
Fax_Number_Delivery         STRING(15)                ! 
Delivery_Text               STRING(255)               ! 
Invoice_Text                STRING(255)               ! 
                         END
                     END                       

RETSTOCK_ALIAS       FILE,DRIVER('Btrieve'),OEM,PRE(ret_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
Record_Number_Key        KEY(ret_ali:Record_Number),NOCASE,PRIMARY ! By Record Number
Part_Number_Key          KEY(ret_ali:Ref_Number,ret_ali:Part_Number),DUP,NOCASE ! By Part Number
Despatched_Key           KEY(ret_ali:Ref_Number,ret_ali:Despatched,ret_ali:Despatch_Note_Number,ret_ali:Part_Number),DUP,NOCASE ! By Part Number
Despatched_Only_Key      KEY(ret_ali:Ref_Number,ret_ali:Despatched),DUP,NOCASE ! By Despatched
Pending_Ref_Number_Key   KEY(ret_ali:Ref_Number,ret_ali:Pending_Ref_Number),DUP,NOCASE ! 
Order_Part_Key           KEY(ret_ali:Ref_Number,ret_ali:Order_Part_Number),DUP,NOCASE ! 
Order_Number_Key         KEY(ret_ali:Ref_Number,ret_ali:Order_Number),DUP,NOCASE ! 
DespatchedKey            KEY(ret_ali:Despatched),DUP,NOCASE ! By Despatched
DespatchedPartKey        KEY(ret_ali:Ref_Number,ret_ali:Despatched,ret_ali:Part_Number),DUP,NOCASE ! By Part Number
Record                   RECORD,PRE()
Record_Number               LONG                      ! Record Number
Ref_Number                  REAL                      ! 
Part_Number                 STRING(30)                ! 
Description                 STRING(30)                ! 
Supplier                    STRING(30)                ! 
Purchase_Cost               REAL                      ! 
Sale_Cost                   REAL                      ! 
Retail_Cost                 REAL                      ! 
AccessoryCost               REAL                      ! Accesory Cost
Item_Cost                   REAL                      ! 
Quantity                    REAL                      ! 
Despatched                  STRING(20)                ! 
Order_Number                REAL                      ! 
Date_Ordered                DATE                      ! 
Date_Received               DATE                      ! 
Despatch_Note_Number        REAL                      ! 
Despatch_Date               DATE                      ! 
Part_Ref_Number             REAL                      ! 
Pending_Ref_Number          REAL                      ! 
Order_Part_Number           REAL                      ! 
Purchase_Order_Number       STRING(30)                ! 
Previous_Sale_Number        LONG                      ! 
InvoicePart                 LONG                      ! Link to Record Number of Invoice(Credit) Part
Credit                      STRING(3)                 ! Has this part been credited?
                         END
                     END                       

LOCATION_ALIAS       FILE,DRIVER('Btrieve'),PRE(loc_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
RecordNumberKey          KEY(loc_ali:RecordNumber),NOCASE,PRIMARY ! By Record Number
Location_Key             KEY(loc_ali:Location),NOCASE ! By Location
Main_Store_Key           KEY(loc_ali:Main_Store,loc_ali:Location),DUP,NOCASE ! By Location
ActiveLocationKey        KEY(loc_ali:Active,loc_ali:Location),DUP,NOCASE ! By Location
ActiveMainStoreKey       KEY(loc_ali:Active,loc_ali:Main_Store,loc_ali:Location),DUP,NOCASE ! By Location
Record                   RECORD,PRE()
RecordNumber                LONG                      ! Record Number
Location                    STRING(30)                ! 
Main_Store                  STRING(3)                 ! 
Active                      BYTE                      ! Active
PickNoteEnable              BYTE                      ! 
                         END
                     END                       

COMMONFA_ALIAS       FILE,DRIVER('Btrieve'),OEM,PRE(com_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
Ref_Number_Key           KEY(com_ali:Ref_Number),NOCASE,PRIMARY ! By Ref Number
Description_Key          KEY(com_ali:Model_Number,com_ali:Category,com_ali:Description),DUP,NOCASE ! By Description
DescripOnlyKey           KEY(com_ali:Model_Number,com_ali:Description),DUP,NOCASE ! By Description
Ref_Model_Key            KEY(com_ali:Model_Number,com_ali:Category,com_ali:Ref_Number),DUP,NOCASE ! By Ref Number
RefOnlyKey               KEY(com_ali:Model_Number,com_ali:Ref_Number),DUP,NOCASE ! By Ref Number
Record                   RECORD,PRE()
Ref_Number                  REAL                      ! 
Category                    STRING(30)                ! 
date_booked                 DATE                      ! 
time_booked                 TIME                      ! 
who_booked                  STRING(3)                 ! 
Model_Number                STRING(30)                ! 
Description                 STRING(30)                ! 
Chargeable_Job              STRING(3)                 ! 
Chargeable_Charge_Type      STRING(30)                ! 
Chargeable_Repair_Type      STRING(30)                ! 
Warranty_Job                STRING(3)                 ! 
Warranty_Charge_Type        STRING(30)                ! 
Warranty_Repair_Type        STRING(30)                ! 
Auto_Complete               STRING(3)                 ! 
Attach_Diagram              STRING(3)                 ! 
Diagram_Setting             STRING(1)                 ! 
Diagram_Path                STRING(255)               ! 
Fault_Codes1                GROUP                     ! 
Fault_Code1                   STRING(30)              ! 
Fault_Code2                   STRING(30)              ! 
Fault_Code3                   STRING(30)              ! 
Fault_Code4                   STRING(30)              ! 
Fault_Code5                   STRING(30)              ! 
Fault_Code6                   STRING(30)              ! 
                            END                       ! 
Fault_Codes2                GROUP                     ! 
Fault_Code7                   STRING(30)              ! 
Fault_Code8                   STRING(30)              ! 
Fault_Code9                   STRING(30)              ! 
Fault_Code10                  STRING(255)             ! 
Fault_Code11                  STRING(255)             ! 
Fault_Code12                  STRING(255)             ! 
                            END                       ! 
Invoice_Text                STRING(255)               ! 
Engineers_Notes             STRING(255)               ! 
DummyField                  STRING(2)                 ! For File Manager
                         END
                     END                       

COMMONCP_ALIAS       FILE,DRIVER('Btrieve'),PRE(ccp_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
RecordNumberKey          KEY(ccp_ali:Record_Number),NOCASE,PRIMARY ! 
Ref_Number_Key           KEY(ccp_ali:Ref_Number),DUP,NOCASE ! By Ref Number
Description_Key          KEY(ccp_ali:Ref_Number,ccp_ali:Description),DUP,NOCASE ! By Description
RefPartNumberKey         KEY(ccp_ali:Ref_Number,ccp_ali:Part_Number),DUP,NOCASE ! 
PartNumberKey            KEY(ccp_ali:Part_Number),DUP,NOCASE ! By Part Number
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
Ref_Number                  REAL                      ! 
Quantity                    REAL                      ! 
Part_Ref_Number             REAL                      ! Reference To The Stock File
Adjustment                  STRING(3)                 ! 
Exclude_From_Order          STRING(3)                 ! 
Part_Number                 STRING(30)                ! 
Description                 STRING(30)                ! 
Supplier                    STRING(30)                ! 
Purchase_Cost               REAL                      ! 
Sale_Cost                   REAL                      ! 
Fault_Code1                 STRING(30)                ! 
Fault_Code2                 STRING(30)                ! 
Fault_Code3                 STRING(30)                ! 
Fault_Code4                 STRING(30)                ! 
Fault_Code5                 STRING(30)                ! 
Fault_Code6                 STRING(30)                ! 
Fault_Code7                 STRING(30)                ! 
Fault_Code8                 STRING(30)                ! 
Fault_Code9                 STRING(30)                ! 
Fault_Code10                STRING(30)                ! 
Fault_Code11                STRING(30)                ! 
Fault_Code12                STRING(30)                ! 
DummyField                  STRING(1)                 ! For File Manager
                         END
                     END                       

COMMONWP_ALIAS       FILE,DRIVER('Btrieve'),PRE(cwp_alias),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
RecordNumberKey          KEY(cwp_alias:Record_Number),NOCASE,PRIMARY ! 
Ref_Number_Key           KEY(cwp_alias:Ref_Number),DUP,NOCASE ! By Ref Number
Description_Key          KEY(cwp_alias:Ref_Number,cwp_alias:Description),DUP,NOCASE ! By Description
RefPartNumberKey         KEY(cwp_alias:Ref_Number,cwp_alias:Part_Number),DUP,NOCASE ! 
PartNumberKey            KEY(cwp_alias:Part_Number),DUP,NOCASE ! By Part Number
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
Ref_Number                  REAL                      ! 
Quantity                    REAL                      ! 
Part_Ref_Number             REAL                      ! Reference To The Stock File
Adjustment                  STRING(3)                 ! 
Exclude_From_Order          STRING(3)                 ! 
Part_Number                 STRING(30)                ! 
Description                 STRING(30)                ! 
Supplier                    STRING(30)                ! 
Purchase_Cost               REAL                      ! 
Sale_Cost                   REAL                      ! 
Fault_Code1                 STRING(30)                ! 
Fault_Code2                 STRING(30)                ! 
Fault_Code3                 STRING(30)                ! 
Fault_Code4                 STRING(30)                ! 
Fault_Code5                 STRING(30)                ! 
Fault_Code6                 STRING(30)                ! 
Fault_Code7                 STRING(30)                ! 
Fault_Code8                 STRING(30)                ! 
Fault_Code9                 STRING(30)                ! 
Fault_Code10                STRING(30)                ! 
Fault_Code11                STRING(30)                ! 
Fault_Code12                STRING(30)                ! 
DummyField                  STRING(1)                 ! For File Manager
                         END
                     END                       

STOMODEL_ALIAS       FILE,DRIVER('Btrieve'),PRE(stm_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
Model_Number_Key         KEY(stm_ali:Ref_Number,stm_ali:Manufacturer,stm_ali:Model_Number),NOCASE,PRIMARY ! By Model Number
Model_Part_Number_Key    KEY(stm_ali:Accessory,stm_ali:Model_Number,stm_ali:Location,stm_ali:Part_Number),DUP,NOCASE ! By Model Number
Description_Key          KEY(stm_ali:Accessory,stm_ali:Model_Number,stm_ali:Location,stm_ali:Description),DUP,NOCASE ! By Description
Manufacturer_Key         KEY(stm_ali:Manufacturer,stm_ali:Model_Number),DUP,NOCASE ! By Manufacturer
Ref_Part_Description     KEY(stm_ali:Ref_Number,stm_ali:Part_Number,stm_ali:Location,stm_ali:Description),DUP,NOCASE ! Why?
Location_Part_Number_Key KEY(stm_ali:Model_Number,stm_ali:Location,stm_ali:Part_Number),DUP,NOCASE ! 
Location_Description_Key KEY(stm_ali:Model_Number,stm_ali:Location,stm_ali:Description),DUP,NOCASE ! 
Mode_Number_Only_Key     KEY(stm_ali:Ref_Number,stm_ali:Model_Number),DUP,NOCASE ! By Model Number
Record                   RECORD,PRE()
Ref_Number                  LONG                      ! 
Manufacturer                STRING(30)                ! 
Model_Number                STRING(30)                ! 
Part_Number                 STRING(30)                ! 
Description                 STRING(30)                ! 
Location                    STRING(30)                ! 
Accessory                   STRING(3)                 ! 
FaultCode1                  STRING(30)                ! Fault Code 1
FaultCode2                  STRING(30)                ! Fault Code 2
FaultCode3                  STRING(30)                ! Fault Code 3
FaultCode4                  STRING(30)                ! Fault Code 4
FaultCode5                  STRING(30)                ! Fault Code 5
FaultCode6                  STRING(30)                ! Fault Code 6
FaultCode7                  STRING(30)                ! Fault Code 7
FaultCode8                  STRING(30)                ! Fault Code 8
FaultCode9                  STRING(30)                ! Fault Code 9
FaultCode10                 STRING(30)                ! Fault Code 10
FaultCode11                 STRING(30)                ! Fault Code 11
FaultCode12                 STRING(30)                ! Fault Code 12
                         END
                     END                       

JOBPAYMT_ALIAS       FILE,DRIVER('Btrieve'),PRE(jpt_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
Record_Number_Key        KEY(jpt_ali:Record_Number),NOCASE,PRIMARY ! By Record Number
All_Date_Key             KEY(jpt_ali:Ref_Number,jpt_ali:Date),DUP,NOCASE ! By Date
Record                   RECORD,PRE()
Record_Number               REAL                      ! 
Ref_Number                  REAL                      ! 
Date                        DATE                      ! 
Payment_Type                STRING(30)                ! 
Credit_Card_Number          STRING(20)                ! 
Expiry_Date                 STRING(5)                 ! 
Issue_Number                STRING(5)                 ! 
Amount                      REAL                      ! 
User_Code                   STRING(3)                 ! 
                         END
                     END                       

PARTSTMP_ALIAS       FILE,DRIVER('Btrieve'),PRE(partmp_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
Part_Number_Key          KEY(partmp_ali:Ref_Number,partmp_ali:Part_Number),DUP,NOCASE ! By Part Number
record_number_key        KEY(partmp_ali:record_number),NOCASE,PRIMARY ! 
Part_Ref_Number2_Key     KEY(partmp_ali:Part_Ref_Number),DUP,NOCASE ! By Part_Ref_Number
Date_Ordered_Key         KEY(partmp_ali:Date_Ordered),DUP,NOCASE ! By Date Ordered
Part_Ref_Number_Key      KEY(partmp_ali:Ref_Number,partmp_ali:Part_Ref_Number),DUP,NOCASE ! By Part Ref Number
Pending_Ref_Number_Key   KEY(partmp_ali:Ref_Number,partmp_ali:Pending_Ref_Number),DUP,NOCASE ! 
Order_Number_Key         KEY(partmp_ali:Ref_Number,partmp_ali:Order_Number),DUP,NOCASE ! 
Supplier_Key             KEY(partmp_ali:Supplier),DUP,NOCASE ! By Supplier
Record                   RECORD,PRE()
Ref_Number                  REAL                      ! 
record_number               REAL                      ! 
Adjustment                  STRING(3)                 ! 
Part_Ref_Number             REAL                      ! 
Part_Details_Group          GROUP                     ! 
Part_Number                   STRING(30)              ! 
Description                   STRING(30)              ! 
Supplier                      STRING(30)              ! 
Purchase_Cost                 REAL                    ! 
Sale_Cost                     REAL                    ! 
Retail_Cost                   REAL                    ! 
                            END                       ! 
Quantity                    REAL                      ! 
Warranty_Part               STRING(3)                 ! 
Exclude_From_Order          STRING(3)                 ! 
Despatch_Note_Number        STRING(30)                ! 
Date_Ordered                DATE                      ! 
Pending_Ref_Number          REAL                      ! 
Order_Number                REAL                      ! 
Fault_Codes1                GROUP                     ! 
Fault_Code1                   STRING(30)              ! 
Fault_Code2                   STRING(30)              ! 
Fault_Code3                   STRING(30)              ! 
Fault_Code4                   STRING(30)              ! 
Fault_Code5                   STRING(30)              ! 
Fault_Code6                   STRING(30)              ! 
                            END                       ! 
Fault_Codes2                GROUP                     ! 
Fault_Code7                   STRING(30)              ! 
Fault_Code8                   STRING(30)              ! 
Fault_Code9                   STRING(30)              ! 
Fault_Code10                  STRING(30)              ! 
Fault_Code11                  STRING(30)              ! 
Fault_Code12                  STRING(30)              ! 
                            END                       ! 
                         END
                     END                       

MPXDEFS_ALIAS        FILE,DRIVER('Btrieve'),OEM,PRE(mxd_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)           ! 
AccountNoKey             KEY(mxd_ali:AccountNo),NOCASE,PRIMARY ! Primary Key
Record                   RECORD,PRE()
AccountNo                   STRING(15)                ! 
UserCode                    STRING(3)                 ! 
IsChargeable                BYTE                      ! 
ChargeType                  STRING(30)                ! 
IsWarranty                  BYTE                      ! 
WarrantyType                STRING(30)                ! 
TransitType                 STRING(30)                ! 
UnitType                    STRING(30)                ! 
DeliveryText                STRING(255)               ! 
Status                      STRING(30)                ! 
Location                    STRING(30)                ! 
EnvCountStatus              STRING(30)                ! 
                         END
                     END                       

WPARTTMP_ALIAS       FILE,DRIVER('Btrieve'),PRE(wartmp_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
Part_Number_Key          KEY(wartmp_ali:Ref_Number,wartmp_ali:Part_Number),DUP,NOCASE ! By Part Number
record_number_key        KEY(wartmp_ali:record_number),NOCASE,PRIMARY ! 
Part_Ref_Number2_Key     KEY(wartmp_ali:Part_Ref_Number),DUP,NOCASE ! By Part_Ref_Number
Date_Ordered_Key         KEY(wartmp_ali:Date_Ordered),DUP,NOCASE ! By Date Ordered
Part_Ref_Number_Key      KEY(wartmp_ali:Ref_Number,wartmp_ali:Part_Ref_Number),DUP,NOCASE ! By Part Ref Number
Pending_Ref_Number_Key   KEY(wartmp_ali:Ref_Number,wartmp_ali:Pending_Ref_Number),DUP,NOCASE ! 
Order_Number_Key         KEY(wartmp_ali:Ref_Number,wartmp_ali:Order_Number),DUP,NOCASE ! 
Supplier_Key             KEY(wartmp_ali:Supplier),DUP,NOCASE ! By Supplier
Record                   RECORD,PRE()
Ref_Number                  REAL                      ! 
record_number               REAL                      ! 
Adjustment                  STRING(3)                 ! 
Part_Ref_Number             REAL                      ! 
Part_Details_Group          GROUP                     ! 
Part_Number                   STRING(30)              ! 
Description                   STRING(30)              ! 
Supplier                      STRING(30)              ! 
Purchase_Cost                 REAL                    ! 
Sale_Cost                     REAL                    ! 
Retail_Cost                   REAL                    ! 
                            END                       ! 
Quantity                    REAL                      ! 
Warranty_Part               STRING(3)                 ! 
Exclude_From_Order          STRING(3)                 ! 
Despatch_Note_Number        STRING(30)                ! 
Date_Ordered                DATE                      ! 
Pending_Ref_Number          REAL                      ! 
Order_Number                REAL                      ! 
Main_Part                   STRING(3)                 ! 
Fault_Codes1                GROUP                     ! 
Fault_Code1                   STRING(30)              ! 
Fault_Code2                   STRING(30)              ! 
Fault_Code3                   STRING(30)              ! 
Fault_Code4                   STRING(30)              ! 
Fault_Code5                   STRING(30)              ! 
Fault_Code6                   STRING(30)              ! 
                            END                       ! 
Fault_Codes2                GROUP                     ! 
Fault_Code7                   STRING(30)              ! 
Fault_Code8                   STRING(30)              ! 
Fault_Code9                   STRING(30)              ! 
Fault_Code10                  STRING(30)              ! 
Fault_Code11                  STRING(30)              ! 
Fault_Code12                  STRING(30)              ! 
                            END                       ! 
                         END
                     END                       

LOAN_ALIAS           FILE,DRIVER('Btrieve'),PRE(loa_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)              ! 
Ref_Number_Key           KEY(loa_ali:Ref_Number),NOCASE,PRIMARY ! By Loan Unit Number
ESN_Only_Key             KEY(loa_ali:ESN),DUP,NOCASE  ! 
MSN_Only_Key             KEY(loa_ali:MSN),DUP,NOCASE  ! 
Ref_Number_Stock_Key     KEY(loa_ali:Stock_Type,loa_ali:Ref_Number),DUP,NOCASE ! By Ref Number
Model_Number_Key         KEY(loa_ali:Stock_Type,loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE ! By Model Number
ESN_Key                  KEY(loa_ali:Stock_Type,loa_ali:ESN),DUP,NOCASE ! By E.S.N. / I.M.E.I. Number
MSN_Key                  KEY(loa_ali:Stock_Type,loa_ali:MSN),DUP,NOCASE ! By M.S.N. Number
ESN_Available_Key        KEY(loa_ali:Available,loa_ali:Stock_Type,loa_ali:ESN),DUP,NOCASE ! By E.S.N. / I.M.E.I.
MSN_Available_Key        KEY(loa_ali:Available,loa_ali:Stock_Type,loa_ali:MSN),DUP,NOCASE ! By M.S.N.
Ref_Available_Key        KEY(loa_ali:Available,loa_ali:Stock_Type,loa_ali:Ref_Number),DUP,NOCASE ! By Loan Unit Number
Model_Available_Key      KEY(loa_ali:Available,loa_ali:Stock_Type,loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE ! By Model Number
Stock_Type_Key           KEY(loa_ali:Stock_Type),DUP,NOCASE ! By Stock Type
ModelRefNoKey            KEY(loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE ! By Unit Number
AvailIMEIOnlyKey         KEY(loa_ali:Available,loa_ali:ESN),DUP,NOCASE ! By I.M.E.I. Number
AvailMSNOnlyKey          KEY(loa_ali:Available,loa_ali:MSN),DUP,NOCASE ! By M.S.N
AvailRefOnlyKey          KEY(loa_ali:Available,loa_ali:Ref_Number),DUP,NOCASE ! By Unit Number
AvailModOnlyKey          KEY(loa_ali:Available,loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE ! By Unit Number
Record                   RECORD,PRE()
Ref_Number                  LONG                      ! 
Model_Number                STRING(30)                ! 
Manufacturer                STRING(30)                ! 
ESN                         STRING(30)                ! 
MSN                         STRING(30)                ! 
Colour                      STRING(30)                ! 
Location                    STRING(30)                ! 
Shelf_Location              STRING(30)                ! 
Date_Booked                 DATE                      ! 
Times_Issued                REAL                      ! 
Available                   STRING(3)                 ! 
Job_Number                  LONG                      ! 
Stock_Type                  STRING(30)                ! 
DummyField                  STRING(1)                 ! For File Manager
                         END
                     END                       

EXCHANGE_ALIAS       FILE,DRIVER('Btrieve'),PRE(xch_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
Ref_Number_Key           KEY(xch_ali:Ref_Number),NOCASE,PRIMARY ! By Loan Unit Number
ESN_Only_Key             KEY(xch_ali:ESN),DUP,NOCASE  ! 
MSN_Only_Key             KEY(xch_ali:MSN),DUP,NOCASE  ! 
Ref_Number_Stock_Key     KEY(xch_ali:Stock_Type,xch_ali:Ref_Number),DUP,NOCASE ! By Ref Number
Model_Number_Key         KEY(xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE ! By Model Number
ESN_Key                  KEY(xch_ali:Stock_Type,xch_ali:ESN),DUP,NOCASE ! By E.S.N. / I.M.E.I. Number
MSN_Key                  KEY(xch_ali:Stock_Type,xch_ali:MSN),DUP,NOCASE ! By M.S.N. Number
ESN_Available_Key        KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:ESN),DUP,NOCASE ! By E.S.N. / I.M.E.I.
MSN_Available_Key        KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:MSN),DUP,NOCASE ! By M.S.N.
Ref_Available_Key        KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:Ref_Number),DUP,NOCASE ! By Exchange Unit Number
Model_Available_Key      KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE ! By Model Number
Stock_Type_Key           KEY(xch_ali:Stock_Type),DUP,NOCASE ! By Stock Type
ModelRefNoKey            KEY(xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE ! By Unit Number
AvailIMEIOnlyKey         KEY(xch_ali:Available,xch_ali:ESN),DUP,NOCASE ! By I.M.E.I. Number
AvailMSNOnlyKey          KEY(xch_ali:Available,xch_ali:MSN),DUP,NOCASE ! By M.S.N.
AvailRefOnlyKey          KEY(xch_ali:Available,xch_ali:Ref_Number),DUP,NOCASE ! By Unit Number
AvailModOnlyKey          KEY(xch_ali:Available,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE ! By Unit Number
StockBookedKey           KEY(xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Date_Booked,xch_ali:Ref_Number),DUP,NOCASE ! By Unit Number
AvailBookedKey           KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Date_Booked,xch_ali:Ref_Number),DUP,NOCASE ! By Unit Number
DateBookedKey            KEY(xch_ali:Date_Booked),DUP,NOCASE ! By Date Booked
Record                   RECORD,PRE()
Ref_Number                  LONG                      ! Unit Number
Model_Number                STRING(30)                ! 
Manufacturer                STRING(30)                ! 
ESN                         STRING(30)                ! 
MSN                         STRING(30)                ! 
Colour                      STRING(30)                ! 
Location                    STRING(30)                ! 
Shelf_Location              STRING(30)                ! 
Date_Booked                 DATE                      ! 
Times_Issued                REAL                      ! 
Available                   STRING(3)                 ! 
Job_Number                  LONG                      ! Job Number
Stock_Type                  STRING(30)                ! 
Audit_Number                REAL                      ! 
DummyField                  STRING(1)                 ! For File Manager
                         END
                     END                       

ACCAREAS_ALIAS       FILE,DRIVER('Btrieve'),PRE(acc_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
Access_level_key         KEY(acc_ali:User_Level,acc_ali:Access_Area),NOCASE,PRIMARY ! By Access Level
AccessOnlyKey            KEY(acc_ali:Access_Area),DUP,NOCASE ! 
Record                   RECORD,PRE()
Access_Area                 STRING(30)                ! 
User_Level                  STRING(30)                ! 
DummyField                  STRING(1)                 ! For File Manager
                         END
                     END                       

USERS_ALIAS          FILE,DRIVER('Btrieve'),PRE(use_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)             ! 
User_Code_Key            KEY(use_ali:User_Code),NOCASE,PRIMARY ! By User Code
User_Type_Active_Key     KEY(use_ali:User_Type,use_ali:Active,use_ali:Surname),DUP,NOCASE ! By Surname
Surname_Active_Key       KEY(use_ali:Active,use_ali:Surname),DUP,NOCASE ! By Surname
User_Code_Active_Key     KEY(use_ali:Active,use_ali:User_Code),DUP,NOCASE ! 
User_Type_Key            KEY(use_ali:User_Type,use_ali:Surname),DUP,NOCASE ! By Surname
surname_key              KEY(use_ali:Surname),DUP,NOCASE ! 
password_key             KEY(use_ali:Password),NOCASE ! 
Logged_In_Key            KEY(use_ali:Logged_In,use_ali:Surname),DUP,NOCASE ! By Name
Team_Surname             KEY(use_ali:Team,use_ali:Surname),DUP,NOCASE ! Surname
Active_Team_Surname      KEY(use_ali:Team,use_ali:Active,use_ali:Surname),DUP,NOCASE ! By Surname
Record                   RECORD,PRE()
User_Code                   STRING(3)                 ! 
Forename                    STRING(30)                ! 
Surname                     STRING(30)                ! 
Password                    STRING(20)                ! 
User_Type                   STRING(15)                ! 
Job_Assignment              STRING(15)                ! 
Supervisor                  STRING(3)                 ! 
User_Level                  STRING(30)                ! 
Logged_In                   STRING(1)                 ! 
Logged_in_date              DATE                      ! 
Logged_In_Time              TIME                      ! 
Restrict_Logins             STRING(3)                 ! 
Concurrent_Logins           REAL                      ! 
Active                      STRING(3)                 ! 
Team                        STRING(30)                ! 
Location                    STRING(30)                ! 
Repair_Target               REAL                      ! 
SkillLevel                  LONG                      ! Skill Level
StockFromLocationOnly       BYTE                      ! Pick Stock From Users Location Only
EmailAddress                STRING(255)               ! Email Address
                         END
                     END                       

WARPARTS_ALIAS       FILE,DRIVER('Btrieve'),PRE(war_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
Part_Number_Key          KEY(war_ali:Ref_Number,war_ali:Part_Number),DUP,NOCASE ! By Part Number
RecordNumberKey          KEY(war_ali:Record_Number),NOCASE,PRIMARY ! 
PartRefNoKey             KEY(war_ali:Part_Ref_Number),DUP,NOCASE ! By Part Ref Number
Date_Ordered_Key         KEY(war_ali:Date_Ordered),DUP,NOCASE ! By Date Ordered
RefPartRefNoKey          KEY(war_ali:Ref_Number,war_ali:Part_Ref_Number),DUP,NOCASE ! By Part Ref Number
PendingRefNoKey          KEY(war_ali:Ref_Number,war_ali:Pending_Ref_Number),DUP,NOCASE ! 
Order_Number_Key         KEY(war_ali:Ref_Number,war_ali:Order_Number),DUP,NOCASE ! 
Supplier_Key             KEY(war_ali:Supplier),DUP,NOCASE ! By Supplier
Order_Part_Key           KEY(war_ali:Ref_Number,war_ali:Order_Part_Number),DUP,NOCASE ! By Order Part Number
SuppDateOrdKey           KEY(war_ali:Supplier,war_ali:Date_Ordered),DUP,NOCASE ! By Date Ordered
SuppDateRecKey           KEY(war_ali:Supplier,war_ali:Date_Received),DUP,NOCASE ! By Date Received
RequestedKey             KEY(war_ali:Requested,war_ali:Part_Number),DUP,NOCASE ! By Part Number
Record                   RECORD,PRE()
Ref_Number                  LONG                      ! Job Number
Record_Number               LONG                      ! Record Number
Adjustment                  STRING(3)                 ! 
Part_Ref_Number             LONG                      ! Stock Reference Number
Part_Number                 STRING(30)                ! 
Description                 STRING(30)                ! 
Supplier                    STRING(30)                ! 
Purchase_Cost               REAL                      ! 
Sale_Cost                   REAL                      ! 
Retail_Cost                 REAL                      ! 
Quantity                    REAL                      ! 
Warranty_Part               STRING(3)                 ! 
Exclude_From_Order          STRING(3)                 ! 
Despatch_Note_Number        STRING(30)                ! 
Date_Ordered                DATE                      ! 
Pending_Ref_Number          LONG                      ! Pending Reference Number
Order_Number                LONG                      ! Order Number
Order_Part_Number           REAL                      ! 
Date_Received               DATE                      ! 
Status_Date                 DATE                      ! 
Main_Part                   STRING(3)                 ! 
Fault_Codes_Checked         STRING(3)                 ! 
InvoicePart                 LONG                      ! Link to Record Number of Invoice(Credit) Part
Credit                      STRING(3)                 ! Has this part been credited?
Requested                   BYTE                      ! Requested For Order
Fault_Code1                 STRING(30)                ! 
Fault_Code2                 STRING(30)                ! 
Fault_Code3                 STRING(30)                ! 
Fault_Code4                 STRING(30)                ! 
Fault_Code5                 STRING(30)                ! 
Fault_Code6                 STRING(30)                ! 
Fault_Code7                 STRING(30)                ! 
Fault_Code8                 STRING(30)                ! 
Fault_Code9                 STRING(30)                ! 
Fault_Code10                STRING(30)                ! 
Fault_Code11                STRING(30)                ! 
Fault_Code12                STRING(30)                ! 
                         END
                     END                       

ORDPARTS_ALIAS       FILE,DRIVER('Btrieve'),PRE(orp_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
Order_Number_Key         KEY(orp_ali:Order_Number,orp_ali:Part_Ref_Number),DUP,NOCASE ! By Order Number
record_number_key        KEY(orp_ali:Record_Number),NOCASE,PRIMARY ! 
Ref_Number_Key           KEY(orp_ali:Part_Ref_Number,orp_ali:Part_Number,orp_ali:Description),DUP,NOCASE ! By Ref Number
Part_Number_Key          KEY(orp_ali:Order_Number,orp_ali:Part_Number,orp_ali:Description),DUP,NOCASE ! By Part Number
Description_Key          KEY(orp_ali:Order_Number,orp_ali:Description,orp_ali:Part_Number),DUP,NOCASE ! By Description
Received_Part_Number_Key KEY(orp_ali:All_Received,orp_ali:Part_Number),DUP,NOCASE ! By Part Number
Account_Number_Key       KEY(orp_ali:Account_Number,orp_ali:Part_Type,orp_ali:All_Received,orp_ali:Part_Number),DUP,NOCASE ! 
Allocated_Key            KEY(orp_ali:Part_Type,orp_ali:All_Received,orp_ali:Allocated_To_Sale,orp_ali:Part_Number),DUP,NOCASE ! 
Allocated_Account_Key    KEY(orp_ali:Part_Type,orp_ali:All_Received,orp_ali:Allocated_To_Sale,orp_ali:Account_Number,orp_ali:Part_Number),DUP,NOCASE ! 
Order_Type_Received      KEY(orp_ali:Order_Number,orp_ali:Part_Type,orp_ali:All_Received,orp_ali:Part_Number,orp_ali:Description),DUP,NOCASE ! By Part Number
PartRecordNumberKey      KEY(orp_ali:PartRecordNumber),DUP,NOCASE ! By Record Number
Record                   RECORD,PRE()
Order_Number                LONG                      ! Order Number
Record_Number               LONG                      ! Record Number
Part_Ref_Number             LONG                      ! Part Ref Number
Quantity                    LONG                      ! Quantity
Part_Number                 STRING(30)                ! 
Description                 STRING(30)                ! 
Purchase_Cost               REAL                      ! 
Sale_Cost                   REAL                      ! 
Retail_Cost                 REAL                      ! 
Job_Number                  LONG                      ! Job Number
Part_Type                   STRING(3)                 ! 
Number_Received             LONG                      ! Number Received
Date_Received               DATE                      ! 
All_Received                STRING(3)                 ! 
Allocated_To_Sale           STRING(3)                 ! 
Account_Number              STRING(15)                ! 
DespatchNoteNumber          STRING(30)                ! Despatch Note Number
Reason                      STRING(255)               ! Reason
PartRecordNumber            LONG                      ! Part Record Number
                         END
                     END                       

PARTS_ALIAS          FILE,DRIVER('Btrieve'),PRE(par_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)             ! 
Part_Number_Key          KEY(par_ali:Ref_Number,par_ali:Part_Number),DUP,NOCASE ! By Part Number
recordnumberkey          KEY(par_ali:Record_Number),NOCASE,PRIMARY ! 
PartRefNoKey             KEY(par_ali:Part_Ref_Number),DUP,NOCASE ! By Part_Ref_Number
Date_Ordered_Key         KEY(par_ali:Date_Ordered),DUP,NOCASE ! By Date Ordered
RefPartRefNoKey          KEY(par_ali:Ref_Number,par_ali:Part_Ref_Number),DUP,NOCASE ! By Part Ref Number
PendingRefNoKey          KEY(par_ali:Ref_Number,par_ali:Pending_Ref_Number),DUP,NOCASE ! 
Order_Number_Key         KEY(par_ali:Ref_Number,par_ali:Order_Number),DUP,NOCASE ! 
Supplier_Key             KEY(par_ali:Supplier),DUP,NOCASE ! By Supplier
Order_Part_Key           KEY(par_ali:Ref_Number,par_ali:Order_Part_Number),DUP,NOCASE ! By Order Part Number
SuppDateOrdKey           KEY(par_ali:Supplier,par_ali:Date_Ordered),DUP,NOCASE ! By Date Ordered
SuppDateRecKey           KEY(par_ali:Supplier,par_ali:Date_Received),DUP,NOCASE ! By Date Received
RequestedKey             KEY(par_ali:Requested,par_ali:Part_Number),DUP,NOCASE ! By Part Number
Record                   RECORD,PRE()
Ref_Number                  LONG                      ! Job Number
Record_Number               LONG                      ! Record Number
Adjustment                  STRING(3)                 ! 
Part_Ref_Number             LONG                      ! Stock Reference Number
Part_Number                 STRING(30)                ! 
Description                 STRING(30)                ! 
Supplier                    STRING(30)                ! 
Purchase_Cost               REAL                      ! 
Sale_Cost                   REAL                      ! 
Retail_Cost                 REAL                      ! 
Quantity                    REAL                      ! 
Warranty_Part               STRING(3)                 ! 
Exclude_From_Order          STRING(3)                 ! 
Despatch_Note_Number        STRING(30)                ! 
Date_Ordered                DATE                      ! 
Pending_Ref_Number          LONG                      ! Pending Reference Number
Order_Number                LONG                      ! Order Number
Order_Part_Number           LONG                      ! Order Reference Number
Date_Received               DATE                      ! 
Status_Date                 DATE                      ! 
Fault_Codes_Checked         STRING(3)                 ! 
InvoicePart                 LONG                      ! Link to Record Number of Invoice(Credit) Part
Credit                      STRING(3)                 ! Has this part been credited?
Requested                   BYTE                      ! Parts Requested For Order
Fault_Code1                 STRING(30)                ! 
Fault_Code2                 STRING(30)                ! 
Fault_Code3                 STRING(30)                ! 
Fault_Code4                 STRING(30)                ! 
Fault_Code5                 STRING(30)                ! 
Fault_Code6                 STRING(30)                ! 
Fault_Code7                 STRING(30)                ! 
Fault_Code8                 STRING(30)                ! 
Fault_Code9                 STRING(30)                ! 
Fault_Code10                STRING(30)                ! 
Fault_Code11                STRING(30)                ! 
Fault_Code12                STRING(30)                ! 
                         END
                     END                       

STOCK_ALIAS          FILE,DRIVER('Btrieve'),PRE(sto_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)             ! 
Ref_Number_Key           KEY(sto_ali:Ref_Number),NOCASE,PRIMARY ! By Ref Number
Sundry_Item_Key          KEY(sto_ali:Sundry_Item),DUP,NOCASE ! By Sundry Item
Description_Key          KEY(sto_ali:Location,sto_ali:Description),DUP,NOCASE ! By Description
Description_Accessory_Key KEY(sto_ali:Location,sto_ali:Accessory,sto_ali:Description),DUP,NOCASE ! By Description
Shelf_Location_Key       KEY(sto_ali:Location,sto_ali:Shelf_Location),DUP,NOCASE ! By Shelf Location
Shelf_Location_Accessory_Key KEY(sto_ali:Location,sto_ali:Accessory,sto_ali:Shelf_Location),DUP,NOCASE ! By Shelf Location
Supplier_Accessory_Key   KEY(sto_ali:Location,sto_ali:Accessory,sto_ali:Shelf_Location),DUP,NOCASE ! By Supplier
Manufacturer_Key         KEY(sto_ali:Manufacturer,sto_ali:Part_Number),DUP,NOCASE ! By Manufacturer
Supplier_Key             KEY(sto_ali:Location,sto_ali:Supplier),DUP,NOCASE ! By Supplier
Location_Key             KEY(sto_ali:Location,sto_ali:Part_Number),DUP,NOCASE ! By Location
Part_Number_Accessory_Key KEY(sto_ali:Location,sto_ali:Accessory,sto_ali:Part_Number),DUP,NOCASE ! By Part Number
Ref_Part_Description_Key KEY(sto_ali:Location,sto_ali:Ref_Number,sto_ali:Part_Number,sto_ali:Description),DUP,NOCASE ! Why?
Location_Manufacturer_Key KEY(sto_ali:Location,sto_ali:Manufacturer,sto_ali:Part_Number),DUP,NOCASE ! By Manufacturer
Manufacturer_Accessory_Key KEY(sto_ali:Location,sto_ali:Accessory,sto_ali:Manufacturer,sto_ali:Part_Number),DUP,NOCASE ! By Part Number
Location_Part_Description_Key KEY(sto_ali:Location,sto_ali:Part_Number,sto_ali:Description),DUP,NOCASE ! By Part Number
Ref_Part_Description2_Key KEY(sto_ali:Ref_Number,sto_ali:Part_Number,sto_ali:Description),DUP,NOCASE ! CASCADE Key
Minimum_Part_Number_Key  KEY(sto_ali:Minimum_Stock,sto_ali:Location,sto_ali:Part_Number),DUP,NOCASE ! By Part Number
Minimum_Description_Key  KEY(sto_ali:Minimum_Stock,sto_ali:Location,sto_ali:Description),DUP,NOCASE ! 
SecondLocKey             KEY(sto_ali:Location,sto_ali:Shelf_Location,sto_ali:Second_Location,sto_ali:Part_Number),DUP,NOCASE ! By Part Number
RequestedKey             KEY(sto_ali:QuantityRequested,sto_ali:Part_Number),DUP,NOCASE ! By Part Number
ExchangeAccPartKey       KEY(sto_ali:ExchangeUnit,sto_ali:Location,sto_ali:Part_Number),DUP,NOCASE ! By Part Number
ExchangeAccDescKey       KEY(sto_ali:ExchangeUnit,sto_ali:Location,sto_ali:Description),DUP,NOCASE ! By Description
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)                 ! 
Ref_Number                  LONG                      ! Reference Number
Part_Number                 STRING(30)                ! 
Description                 STRING(30)                ! 
Supplier                    STRING(30)                ! 
Purchase_Cost               REAL                      ! 
Sale_Cost                   REAL                      ! 
Retail_Cost                 REAL                      ! 
AccessoryCost               REAL                      ! Accessory Cost
Percentage_Mark_Up          REAL                      ! 
Shelf_Location              STRING(30)                ! 
Manufacturer                STRING(30)                ! 
Location                    STRING(30)                ! 
Second_Location             STRING(30)                ! 
Quantity_Stock              REAL                      ! 
Quantity_To_Order           REAL                      ! 
Quantity_On_Order           REAL                      ! 
Minimum_Level               REAL                      ! 
Reorder_Level               REAL                      ! 
Use_VAT_Code                STRING(3)                 ! 
Service_VAT_Code            STRING(2)                 ! 
Retail_VAT_Code             STRING(2)                 ! 
Superceeded                 STRING(3)                 ! 
Superceeded_Ref_Number      REAL                      ! 
Pending_Ref_Number          REAL                      ! 
Accessory                   STRING(3)                 ! 
Minimum_Stock               STRING(3)                 ! 
Assign_Fault_Codes          STRING(3)                 ! 
Individual_Serial_Numbers   STRING(3)                 ! 
ExchangeUnit                STRING(3)                 ! 
QuantityRequested           LONG                      ! Quantity Requested
Suspend                     BYTE                      ! Suspend Part
Fault_Code1                 STRING(30)                ! 
Fault_Code2                 STRING(30)                ! 
Fault_Code3                 STRING(30)                ! 
Fault_Code4                 STRING(30)                ! 
Fault_Code5                 STRING(30)                ! 
Fault_Code6                 STRING(30)                ! 
Fault_Code7                 STRING(30)                ! 
Fault_Code8                 STRING(30)                ! 
Fault_Code9                 STRING(30)                ! 
Fault_Code10                STRING(30)                ! 
Fault_Code11                STRING(30)                ! 
Fault_Code12                STRING(30)                ! 
E1                          BYTE                      ! E1
E2                          BYTE                      ! E2
E3                          BYTE                      ! E3
AllowDuplicate              BYTE                      ! Allow Duplicate Part On Jobs
ExcludeFromEDI              BYTE                      ! Exclude From EDI
RF_Board                    BYTE                      ! 
                         END
                     END                       

JOBS_ALIAS           FILE,DRIVER('Btrieve'),PRE(job_ali),BINDABLE,CREATE,THREAD,EXTERNAL(''),DLL(dll_mode)              ! 
Ref_Number_Key           KEY(job_ali:Ref_Number),NOCASE,PRIMARY ! By Job Number
Model_Unit_Key           KEY(job_ali:Model_Number,job_ali:Unit_Type),DUP,NOCASE ! By Job Number
EngCompKey               KEY(job_ali:Engineer,job_ali:Completed,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
EngWorkKey               KEY(job_ali:Engineer,job_ali:Workshop,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
Surname_Key              KEY(job_ali:Surname),DUP,NOCASE ! By Surname
MobileNumberKey          KEY(job_ali:Mobile_Number),DUP,NOCASE ! By Mobile Number
ESN_Key                  KEY(job_ali:ESN),DUP,NOCASE  ! By E.S.N. / I.M.E.I.
MSN_Key                  KEY(job_ali:MSN),DUP,NOCASE  ! By M.S.N.
AccountNumberKey         KEY(job_ali:Account_Number,job_ali:Ref_Number),DUP,NOCASE ! By Account Number
AccOrdNoKey              KEY(job_ali:Account_Number,job_ali:Order_Number),DUP,NOCASE ! By Order Number
Model_Number_Key         KEY(job_ali:Model_Number),DUP,NOCASE ! By Model Number
Engineer_Key             KEY(job_ali:Engineer,job_ali:Model_Number),DUP,NOCASE ! By Engineer
Date_Booked_Key          KEY(job_ali:date_booked),DUP,NOCASE ! By Date Booked
DateCompletedKey         KEY(job_ali:Date_Completed),DUP,NOCASE ! By Date Completed
ModelCompKey             KEY(job_ali:Model_Number,job_ali:Date_Completed),DUP,NOCASE ! By Completed Date
By_Status                KEY(job_ali:Current_Status,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
StatusLocKey             KEY(job_ali:Current_Status,job_ali:Location,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
Location_Key             KEY(job_ali:Location,job_ali:Ref_Number),DUP,NOCASE ! By Location
Third_Party_Key          KEY(job_ali:Third_Party_Site,job_ali:Third_Party_Printed,job_ali:Ref_Number),DUP,NOCASE ! By Third Party
ThirdEsnKey              KEY(job_ali:Third_Party_Site,job_ali:Third_Party_Printed,job_ali:ESN),DUP,NOCASE ! By ESN
ThirdMsnKey              KEY(job_ali:Third_Party_Site,job_ali:Third_Party_Printed,job_ali:MSN),DUP,NOCASE ! By MSN
PriorityTypeKey          KEY(job_ali:Job_Priority,job_ali:Ref_Number),DUP,NOCASE ! By Priority
Unit_Type_Key            KEY(job_ali:Unit_Type),DUP,NOCASE ! By Unit Type
EDI_Key                  KEY(job_ali:Manufacturer,job_ali:EDI,job_ali:EDI_Batch_Number,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
InvoiceNumberKey         KEY(job_ali:Invoice_Number),DUP,NOCASE ! By Invoice_Number
WarInvoiceNoKey          KEY(job_ali:Invoice_Number_Warranty),DUP,NOCASE ! By Invoice Number
Batch_Number_Key         KEY(job_ali:Batch_Number,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
Batch_Status_Key         KEY(job_ali:Batch_Number,job_ali:Current_Status,job_ali:Ref_Number),DUP,NOCASE ! By Ref Number
BatchModelNoKey          KEY(job_ali:Batch_Number,job_ali:Model_Number,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
BatchInvoicedKey         KEY(job_ali:Batch_Number,job_ali:Invoice_Number,job_ali:Ref_Number),DUP,NOCASE ! By Ref Number
BatchCompKey             KEY(job_ali:Batch_Number,job_ali:Completed,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
ChaInvoiceKey            KEY(job_ali:Chargeable_Job,job_ali:Account_Number,job_ali:Invoice_Number,job_ali:Ref_Number),DUP,NOCASE ! 
InvoiceExceptKey         KEY(job_ali:Invoice_Exception,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
ConsignmentNoKey         KEY(job_ali:Consignment_Number),DUP,NOCASE ! By Cosignment Number
InConsignKey             KEY(job_ali:Incoming_Consignment_Number),DUP,NOCASE ! By Consignment Number
ReadyToDespKey           KEY(job_ali:Despatched,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
ReadyToTradeKey          KEY(job_ali:Despatched,job_ali:Account_Number,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
ReadyToCouKey            KEY(job_ali:Despatched,job_ali:Current_Courier,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
ReadyToAllKey            KEY(job_ali:Despatched,job_ali:Account_Number,job_ali:Current_Courier,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
DespJobNumberKey         KEY(job_ali:Despatch_Number,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
DateDespatchKey          KEY(job_ali:Courier,job_ali:Date_Despatched,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
DateDespLoaKey           KEY(job_ali:Loan_Courier,job_ali:Loan_Despatched,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
DateDespExcKey           KEY(job_ali:Exchange_Courier,job_ali:Exchange_Despatched,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
ChaRepTypeKey            KEY(job_ali:Repair_Type),DUP,NOCASE ! By Chargeable Repair Type
WarRepTypeKey            KEY(job_ali:Repair_Type_Warranty),DUP,NOCASE ! By Warranty Repair Type
ChaTypeKey               KEY(job_ali:Charge_Type),DUP,NOCASE ! 
WarChaTypeKey            KEY(job_ali:Warranty_Charge_Type),DUP,NOCASE ! 
Bouncer_Key              KEY(job_ali:Bouncer,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
EngDateCompKey           KEY(job_ali:Engineer,job_ali:Date_Completed),DUP,NOCASE ! By Date Completed
ExcStatusKey             KEY(job_ali:Exchange_Status,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
LoanStatusKey            KEY(job_ali:Loan_Status,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
ExchangeLocKey           KEY(job_ali:Exchange_Status,job_ali:Location,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
LoanLocKey               KEY(job_ali:Loan_Status,job_ali:Location,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
BatchJobKey              KEY(job_ali:InvoiceAccount,job_ali:InvoiceBatch,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
BatchStatusKey           KEY(job_ali:InvoiceAccount,job_ali:InvoiceBatch,job_ali:InvoiceStatus,job_ali:Ref_Number),DUP,NOCASE ! By Job Number
Record                   RECORD,PRE()
Ref_Number                  LONG                      ! 
Batch_Number                LONG                      ! 
Internal_Status             STRING(10)                ! 
Auto_Search                 STRING(30)                ! 
who_booked                  STRING(3)                 ! 
date_booked                 DATE                      ! 
time_booked                 TIME                      ! 
Cancelled                   STRING(3)                 ! 
Bouncer                     STRING(8)                 ! 
Bouncer_Type                STRING(3)                 ! Type Of Bouncer  CHArgeable / WARranty / BOTh
Web_Type                    STRING(3)                 ! Warranty/Insurance/Chargeable
Warranty_Job                STRING(3)                 ! 
Chargeable_Job              STRING(3)                 ! 
Model_Number                STRING(30)                ! 
Manufacturer                STRING(30)                ! 
ESN                         STRING(20)                ! 
MSN                         STRING(20)                ! 
ProductCode                 STRING(30)                ! 
Unit_Type                   STRING(30)                ! 
Colour                      STRING(30)                ! 
Location_Type               STRING(10)                ! 
Phone_Lock                  STRING(30)                ! 
Workshop                    STRING(3)                 ! 
Location                    STRING(30)                ! 
Authority_Number            STRING(30)                ! 
Insurance_Reference_Number  STRING(30)                ! 
DOP                         DATE                      ! 
Insurance                   STRING(3)                 ! 
Insurance_Type              STRING(30)                ! 
Transit_Type                STRING(30)                ! 
Physical_Damage             STRING(3)                 ! 
Intermittent_Fault          STRING(3)                 ! 
Loan_Status                 STRING(30)                ! 
Exchange_Status             STRING(30)                ! 
Job_Priority                STRING(30)                ! 
Charge_Type                 STRING(30)                ! 
Warranty_Charge_Type        STRING(30)                ! 
Current_Status              STRING(30)                ! 
Account_Number              STRING(15)                ! 
Trade_Account_Name          STRING(30)                ! 
Department_Name             STRING(30)                ! 
Order_Number                STRING(30)                ! 
POP                         STRING(3)                 ! 
In_Repair                   STRING(3)                 ! 
Date_In_Repair              DATE                      ! 
Time_In_Repair              TIME                      ! 
On_Test                     STRING(3)                 ! 
Date_On_Test                DATE                      ! 
Time_On_Test                TIME                      ! 
Estimate_Ready              STRING(3)                 ! 
QA_Passed                   STRING(3)                 ! 
Date_QA_Passed              DATE                      ! 
Time_QA_Passed              TIME                      ! 
QA_Rejected                 STRING(3)                 ! 
Date_QA_Rejected            DATE                      ! 
Time_QA_Rejected            TIME                      ! 
QA_Second_Passed            STRING(3)                 ! 
Date_QA_Second_Passed       DATE                      ! 
Time_QA_Second_Passed       TIME                      ! 
Title                       STRING(4)                 ! 
Initial                     STRING(1)                 ! 
Surname                     STRING(30)                ! 
Postcode                    STRING(10)                ! 
Company_Name                STRING(30)                ! 
Address_Line1               STRING(30)                ! 
Address_Line2               STRING(30)                ! 
Address_Line3               STRING(30)                ! 
Telephone_Number            STRING(15)                ! 
Fax_Number                  STRING(15)                ! 
Mobile_Number               STRING(15)                ! 
Postcode_Collection         STRING(10)                ! 
Company_Name_Collection     STRING(30)                ! 
Address_Line1_Collection    STRING(30)                ! 
Address_Line2_Collection    STRING(30)                ! 
Address_Line3_Collection    STRING(30)                ! 
Telephone_Collection        STRING(15)                ! 
Postcode_Delivery           STRING(10)                ! 
Address_Line1_Delivery      STRING(30)                ! 
Company_Name_Delivery       STRING(30)                ! 
Address_Line2_Delivery      STRING(30)                ! 
Address_Line3_Delivery      STRING(30)                ! 
Telephone_Delivery          STRING(15)                ! 
Date_Completed              DATE                      ! 
Time_Completed              TIME                      ! 
Completed                   STRING(3)                 ! 
Paid                        STRING(3)                 ! 
Paid_Warranty               STRING(3)                 ! 
Date_Paid                   DATE                      ! 
Repair_Type                 STRING(30)                ! 
Repair_Type_Warranty        STRING(30)                ! 
Engineer                    STRING(3)                 ! 
Ignore_Chargeable_Charges   STRING(3)                 ! 
Ignore_Warranty_Charges     STRING(3)                 ! 
Ignore_Estimate_Charges     STRING(3)                 ! 
Courier_Cost                REAL                      ! 
Advance_Payment             REAL                      ! 
Labour_Cost                 REAL                      ! 
Parts_Cost                  REAL                      ! 
Sub_Total                   REAL                      ! 
Courier_Cost_Estimate       REAL                      ! 
Labour_Cost_Estimate        REAL                      ! 
Parts_Cost_Estimate         REAL                      ! 
Sub_Total_Estimate          REAL                      ! 
Courier_Cost_Warranty       REAL                      ! 
Labour_Cost_Warranty        REAL                      ! 
Parts_Cost_Warranty         REAL                      ! 
Sub_Total_Warranty          REAL                      ! 
Loan_Issued_Date            DATE                      ! 
Loan_Unit_Number            LONG                      ! 
Loan_accessory              STRING(3)                 ! 
Loan_User                   STRING(3)                 ! 
Loan_Courier                STRING(30)                ! 
Loan_Consignment_Number     STRING(30)                ! 
Loan_Despatched             DATE                      ! 
Loan_Despatched_User        STRING(3)                 ! 
Loan_Despatch_Number        LONG                      ! 
LoaService                  STRING(1)                 ! Loan Service For City Link / LabelG
Exchange_Unit_Number        LONG                      ! 
Exchange_Authorised         STRING(3)                 ! 
Loan_Authorised             STRING(3)                 ! 
Exchange_Accessory          STRING(3)                 ! 
Exchange_Issued_Date        DATE                      ! 
Exchange_User               STRING(3)                 ! 
Exchange_Courier            STRING(30)                ! 
Exchange_Consignment_Number STRING(30)                ! 
Exchange_Despatched         DATE                      ! 
Exchange_Despatched_User    STRING(3)                 ! 
Exchange_Despatch_Number    LONG                      ! 
ExcService                  STRING(1)                 ! Exchange Service For City Link / Label G
Date_Despatched             DATE                      ! 
Despatch_Number             LONG                      ! 
Despatch_User               STRING(3)                 ! 
Courier                     STRING(30)                ! 
Consignment_Number          STRING(30)                ! 
Incoming_Courier            STRING(30)                ! 
Incoming_Consignment_Number STRING(30)                ! 
Incoming_Date               DATE                      ! 
Despatched                  STRING(3)                 ! 
Despatch_Type               STRING(3)                 ! 
Current_Courier             STRING(30)                ! 
JobService                  STRING(1)                 ! Job Service For City Link / Label G
Last_Repair_Days            REAL                      ! 
Third_Party_Site            STRING(30)                ! 
Estimate                    STRING(3)                 ! 
Estimate_If_Over            REAL                      ! 
Estimate_Accepted           STRING(3)                 ! 
Estimate_Rejected           STRING(3)                 ! 
Third_Party_Printed         STRING(3)                 ! 
ThirdPartyDateDesp          DATE                      ! 
Fault_Code1                 STRING(30)                ! 
Fault_Code2                 STRING(30)                ! 
Fault_Code3                 STRING(30)                ! 
Fault_Code4                 STRING(30)                ! 
Fault_Code5                 STRING(30)                ! 
Fault_Code6                 STRING(30)                ! 
Fault_Code7                 STRING(30)                ! 
Fault_Code8                 STRING(30)                ! 
Fault_Code9                 STRING(30)                ! 
Fault_Code10                STRING(255)               ! 
Fault_Code11                STRING(255)               ! 
Fault_Code12                STRING(255)               ! 
PreviousStatus              STRING(30)                ! 
StatusUser                  STRING(3)                 ! User who last changed the status
Status_End_Date             DATE                      ! 
Status_End_Time             TIME                      ! 
Turnaround_End_Date         DATE                      ! 
Turnaround_End_Time         TIME                      ! 
Turnaround_Time             STRING(30)                ! 
Special_Instructions        STRING(30)                ! 
InvoiceAccount              STRING(15)                ! Invoice Batch Account Number
InvoiceStatus               STRING(3)                 ! Proforma Status (UNA - Unathorised / AUT - Authorised / QUE - Query)
InvoiceBatch                LONG                      ! Proforma Batch Number
InvoiceQuery                STRING(255)               ! Query Reason
EDI                         STRING(3)                 ! 
EDI_Batch_Number            REAL                      ! 
Invoice_Exception           STRING(3)                 ! 
Invoice_Failure_Reason      STRING(80)                ! 
Invoice_Number              LONG                      ! 
Invoice_Date                DATE                      ! 
Invoice_Date_Warranty       DATE                      ! 
Invoice_Courier_Cost        REAL                      ! 
Invoice_Labour_Cost         REAL                      ! 
Invoice_Parts_Cost          REAL                      ! 
Invoice_Sub_Total           REAL                      ! 
Invoice_Number_Warranty     LONG                      ! 
WInvoice_Courier_Cost       REAL                      ! 
WInvoice_Labour_Cost        REAL                      ! 
WInvoice_Parts_Cost         REAL                      ! 
WInvoice_Sub_Total          REAL                      ! 
                         END
                     END                       

XMLTRADE_ALIAS       FILE,DRIVER('Btrieve'),OEM,PRE(xtr_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
AccountNumberKey         KEY(xtr_ali:AccountNumber),NOCASE,PRIMARY ! 
Record                   RECORD,PRE()
AccountNumber               STRING(15)                ! 
                         END
                     END                       

XMLSB_ALIAS          FILE,DRIVER('Btrieve'),OEM,PRE(xsb_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)             ! 
SBStatusKey              KEY(xsb_ali:SBStatus),NOCASE,PRIMARY ! 
FKStatusCodeKey          KEY(xsb_ali:FKStatusCode),DUP,NOCASE ! 
Record                   RECORD,PRE()
SBStatus                    STRING(30)                ! 
FKStatusCode                STRING(4)                 ! 
                         END
                     END                       

STDCHRGE_ALIAS       FILE,DRIVER('Btrieve'),PRE(sta_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)          ! 
Model_Number_Charge_Key  KEY(sta_ali:Model_Number,sta_ali:Charge_Type,sta_ali:Unit_Type,sta_ali:Repair_Type),NOCASE,PRIMARY ! By Charge Type
Charge_Type_Key          KEY(sta_ali:Model_Number,sta_ali:Charge_Type),DUP,NOCASE ! By Charge Type
Unit_Type_Key            KEY(sta_ali:Model_Number,sta_ali:Unit_Type),DUP,NOCASE ! By Unit Type
Repair_Type_Key          KEY(sta_ali:Model_Number,sta_ali:Repair_Type),DUP,NOCASE ! By Repair Type
Cost_Key                 KEY(sta_ali:Model_Number,sta_ali:Cost),DUP,NOCASE ! By Cost
Charge_Type_Only_Key     KEY(sta_ali:Charge_Type),DUP,NOCASE ! 
Repair_Type_Only_Key     KEY(sta_ali:Repair_Type),DUP,NOCASE ! 
Unit_Type_Only_Key       KEY(sta_ali:Unit_Type),DUP,NOCASE ! 
Record                   RECORD,PRE()
Charge_Type                 STRING(30)                ! 
Model_Number                STRING(30)                ! 
Unit_Type                   STRING(30)                ! 
Repair_Type                 STRING(30)                ! 
Cost                        REAL                      ! 
dummy                       BYTE                      ! 
                         END
                     END                       


glo:ErrorText        STRING(1000),EXTERNAL,DLL
glo:owner            STRING(20),EXTERNAL,DLL
glo:DateModify       DATE,EXTERNAL,DLL
glo:TimeModify       TIME,EXTERNAL,DLL
glo:Notes_Global     STRING(1000),EXTERNAL,DLL
glo:Q_Invoice        QUEUE,PRE(glo),EXTERNAL,DLL
Invoice_Number         REAL
Account_Number         STRING(15)
                     END
glo:afe_path         STRING(255),EXTERNAL,DLL
glo:Password         STRING(20),EXTERNAL,DLL
glo:PassAccount      STRING(30),EXTERNAL,DLL
glo:Preview          STRING('True'),EXTERNAL,DLL
glo:q_JobNumber      QUEUE,PRE(GLO),EXTERNAL,DLL
Job_Number_Pointer     REAL
                     END
glo:Q_PartsOrder     QUEUE,PRE(GLO),EXTERNAL,DLL
Q_Order_Number         REAL
Q_Part_Number          STRING(30)
Q_Description          STRING(30)
Q_Supplier             STRING(30)
Q_Quantity             LONG
Q_Purchase_Cost        REAL
Q_Sale_Cost            REAL
                     END
glo:G_Select1        GROUP,PRE(GLO),EXTERNAL,DLL
Select1                STRING(30)
Select2                STRING(40)
Select3                STRING(40)
Select4                STRING(40)
Select5                STRING(40)
Select6                STRING(40)
Select7                STRING(40)
Select8                STRING(40)
Select9                STRING(40)
Select10               STRING(40)
Select11               STRING(40)
Select12               STRING(40)
Select13               STRING(40)
Select14               STRING(40)
Select15               STRING(40)
Select16               STRING(40)
Select17               STRING(40)
Select18               STRING(40)
Select19               STRING(40)
Select20               STRING(40)
Select21               STRING(40)
Select22               STRING(40)
Select23               STRING(40)
Select24               STRING(40)
Select25               STRING(40)
Select26               STRING(40)
Select27               STRING(40)
Select28               STRING(40)
Select29               STRING(40)
Select30               STRING(40)
Select31               STRING(40)
Select32               STRING(40)
Select33               STRING(40)
Select34               STRING(40)
Select35               STRING(40)
Select36               STRING(40)
Select37               STRING(40)
Select38               STRING(40)
Select39               STRING(40)
Select40               STRING(40)
Select41               STRING(40)
Select42               STRING(40)
Select43               STRING(40)
Select44               STRING(40)
Select45               STRING(40)
Select46               STRING(40)
Select47               STRING(40)
Select48               STRING(40)
Select49               STRING(40)
Select50               STRING(40)
                     END
glo:q_RepairType     QUEUE,PRE(GLO),EXTERNAL,DLL
Repair_Type_Pointer    STRING(30)
                     END
glo:Q_UnitType       QUEUE,PRE(GLO),EXTERNAL,DLL
Unit_Type_Pointer      STRING(30)
                     END
glo:Q_ChargeType     QUEUE,PRE(GLO),EXTERNAL,DLL
Charge_Type_Pointer    STRING(30)
                     END
glo:Q_ModelNumber    QUEUE,PRE(glo),EXTERNAL,DLL
Model_Number_Pointer   STRING(30)
                     END
glo:Q_Accessory      QUEUE,PRE(GLO),EXTERNAL,DLL
Accessory_Pointer      STRING(30)
                     END
glo:Q_AccessLevel    QUEUE,PRE(GLO),EXTERNAL,DLL
Access_Level_Pointer   STRING(30)
                     END
glo:Q_AccessAreas    QUEUE,PRE(GLO),EXTERNAL,DLL
Access_Areas_Pointer   STRING(30)
                     END
glo:Q_Notes          QUEUE,PRE(GLO),EXTERNAL,DLL
notes_pointer          STRING(80)
                     END
glo:EnableFlag       STRING(3),EXTERNAL,DLL
glo:TimeLogged       TIME,EXTERNAL,DLL
glo:GLO:ApplicationName STRING(8),EXTERNAL,DLL
glo:GLO:ApplicationCreationDate LONG,EXTERNAL,DLL
glo:GLO:ApplicationCreationTime LONG,EXTERNAL,DLL
glo:GLO:ApplicationChangeDate LONG,EXTERNAL,DLL
glo:GLO:ApplicationChangeTime LONG,EXTERNAL,DLL
glo:GLO:Compiled32   BYTE,EXTERNAL,DLL
glo:GLO:AppINIFile   STRING(255),EXTERNAL,DLL
glo:Queue            QUEUE,PRE(GLO),EXTERNAL,DLL
Pointer                STRING(40)
                     END
glo:Queue2           QUEUE,PRE(GLO),EXTERNAL,DLL
Pointer2               STRING(40)
                     END
glo:Queue3           QUEUE,PRE(GLO),EXTERNAL,DLL
Pointer3               STRING(40)
                     END
glo:Queue4           QUEUE,PRE(GLO),EXTERNAL,DLL
Pointer4               STRING(40)
                     END
glo:Queue5           QUEUE,PRE(GLO),EXTERNAL,DLL
Pointer5               STRING(40)
                     END
glo:Queue6           QUEUE,PRE(GLO),EXTERNAL,DLL
Pointer6               STRING(40)
                     END
glo:Queue7           QUEUE,PRE(GLO),EXTERNAL,DLL
Pointer7               STRING(40)
                     END
glo:Queue8           QUEUE,PRE(GLO),EXTERNAL,DLL
Pointer8               STRING(40)
                     END
glo:Queue9           QUEUE,PRE(GLO),EXTERNAL,DLL
Pointer9               STRING(40)
                     END
glo:Queue10          QUEUE,PRE(GLO),EXTERNAL,DLL
Pointer10              STRING(40)
                     END
glo:Queue11          QUEUE,PRE(GLO),EXTERNAL,DLL
Pointer11              STRING(40)
                     END
glo:Queue12          QUEUE,PRE(GLO),EXTERNAL,DLL
Pointer12              STRING(40)
                     END
glo:Queue13          QUEUE,PRE(GLO),EXTERNAL,DLL
Pointer13              STRING(40)
                     END
glo:Queue14          QUEUE,PRE(GLO),EXTERNAL,DLL
Pointer14              STRING(40)
                     END
glo:Queue15          QUEUE,PRE(GLO),EXTERNAL,DLL
Pointer15              STRING(40)
                     END
glo:Queue16          QUEUE,PRE(GLO),EXTERNAL,DLL
Pointer16              STRING(40)
                     END
glo:Queue17          QUEUE,PRE(GLO),EXTERNAL,DLL
Pointer17              STRING(40)
                     END
glo:Queue18          QUEUE,PRE(GLO),EXTERNAL,DLL
Pointer18              STRING(40)
                     END
glo:Queue19          QUEUE,PRE(GLO),EXTERNAL,DLL
Pointer19              STRING(40)
                     END
glo:Queue20          QUEUE,PRE(GLO),EXTERNAL,DLL
Pointer20              STRING(40)
                     END
glo:FaultCode1       STRING(30),EXTERNAL,DLL
glo:FaultCode2       STRING(30),EXTERNAL,DLL
glo:FaultCode3       STRING(30),EXTERNAL,DLL
glo:FaultCode4       STRING(30),EXTERNAL,DLL
glo:FaultCode5       STRING(30),EXTERNAL,DLL
glo:FaultCode6       STRING(30),EXTERNAL,DLL
glo:FaultCode7       STRING(30),EXTERNAL,DLL
glo:FaultCode8       STRING(30),EXTERNAL,DLL
glo:FaultCode9       STRING(30),EXTERNAL,DLL
glo:FaultCode10      STRING(255),EXTERNAL,DLL
glo:FaultCode11      STRING(255),EXTERNAL,DLL
glo:FaultCode12      STRING(255),EXTERNAL,DLL
glo:FaultCode13      STRING(30),EXTERNAL,DLL
glo:FaultCode14      STRING(30),EXTERNAL,DLL
glo:FaultCode15      STRING(30),EXTERNAL,DLL
glo:FaultCode16      STRING(30),EXTERNAL,DLL
glo:FaultCode17      STRING(30),EXTERNAL,DLL
glo:FaultCode18      STRING(30),EXTERNAL,DLL
glo:FaultCode19      STRING(30),EXTERNAL,DLL
glo:FaultCode20      STRING(30),EXTERNAL,DLL
glo:TradeFaultCode1  STRING(30),EXTERNAL,DLL
glo:TradeFaultCode2  STRING(30),EXTERNAL,DLL
glo:TradeFaultCode3  STRING(30),EXTERNAL,DLL
glo:TradeFaultCode4  STRING(30),EXTERNAL,DLL
glo:TradeFaultCode5  STRING(30),EXTERNAL,DLL
glo:TradeFaultCode6  STRING(30),EXTERNAL,DLL
glo:TradeFaultCode7  STRING(30),EXTERNAL,DLL
glo:TradeFaultCode8  STRING(30),EXTERNAL,DLL
glo:TradeFaultCode9  STRING(30),EXTERNAL,DLL
glo:TradeFaultCode10 STRING(30),EXTERNAL,DLL
glo:TradeFaultCode11 STRING(30),EXTERNAL,DLL
glo:TradeFaultCode12 STRING(30),EXTERNAL,DLL
! Start of "After File Declarations"
! [Priority 5000]

! End of "After File Declarations"
! Start of "Global Data"
! [Priority 50]

  include('mo.inc')
ThisMakeOver   MakeOver,External,DLL(dll_mode)
Win:Browse     Equate(1)
Win:Form       Equate(2)
Win:Frame      Equate(3)
Win:Window     Equate(4)
Win:Lookup     Equate(5)
Win:Wizard     Equate(6)
Win:Process    Equate(7)
Win:Report     Equate(8)
Win:Background Equate(9)
Win:About      Equate(10)
Win:Splash     Equate(11)
Win:Viewer     Equate(12)
Win:Other1     Equate(13)
Win:Other2     Equate(14)
Win:Other3     Equate(15)
Win:ToolBox    Equate(16)
Win:Modal      Equate(128)
SolaceCurrentFile           CString(126),external,dll(dll_mode)
SolaceNoRefreshAllFile      Byte,external,dll(dll_mode)
SolaceNoRefreshOneFile      Byte,external,dll(dll_mode)
SolaceNoRefreshFileStatus   Byte,external,dll(dll_mode)
SolaceNoRefreshEnvironment  Byte,external,dll(dll_mode)
SolaceNoRefreshWatch        Byte,external,dll(dll_mode)
SolaceNoRefreshEvents       Byte,external,dll(dll_mode)
SolaceCurrentTab            Byte,external,dll(dll_mode)
GLO:SolaceViewVariables     Byte,external,dll(dll_mode)
SolaceThreadNo              Byte,external,dll(dll_mode)
SolaceCurThreadNo           CString(45),external,dll(dll_mode)
SolaceSaveToFile            Byte,external,dll(dll_mode)
SolaceInit                  Byte,external,dll(dll_mode)
SolaceRootPath              CString(4),external,dll(dll_mode)
SolaceSectPerClust          ULong,external,dll(dll_mode)
SolaceBytesPerSect          ULong,external,dll(dll_mode)
SolaceFreeClust             ULong,external,dll(dll_mode)
SolaceClusters              ULong,external,dll(dll_mode)
SolaceSaveFile              CString('VariView'),external,dll(dll_mode)
SolSaveSettings             Byte(0),external,dll(dll_mode)
SolaceIniFile               CString('VariView.ini'),external,dll(dll_mode)
SolaceApplyFileName         CString(255),external,dll(dll_mode)

SolaceVarView      QUEUE,PRE(SOLVV),external,dll(dll_mode)
ProcName             CString(60),external,dll(dll_mode)
VariableName         CString(60),external,dll(dll_mode)
VariableValue        CString(2048),external,dll(dll_mode)
                   END
SolaceGlobView     QUEUE,PRE(SOLGLO),external,dll(dll_mode)
VariableName         CString(60),external,dll(dll_mode)
VariableValue        CString(2048),external,dll(dll_mode)
                   END
SolaceFilesNames   QUEUE,PRE(SOLFN),external,dll(dll_mode)               !Dropdown list of files
FileName             CString(60),external,dll(dll_mode)
                   END
SolaceFiles        QUEUE,PRE(SOLFS),external,dll(dll_mode)
FileName             CString(60),external,dll(dll_mode)
OpenStatus           CString(10),external,dll(dll_mode)
NoRecords            ULong,external,dll(dll_mode)
                   END
SolaceAllFiles     QUEUE,PRE(SOLALL),external,dll(dll_mode)
FieldName           CString(60),external,dll(dll_mode)
FieldValue          CString(2048),external,dll(dll_mode)
FileName            CString(60),external,dll(dll_mode)
                   END
SolaceFileView     QUEUE,PRE(SOLFIL),external,dll(dll_mode)
FieldName           CString(60),external,dll(dll_mode)
FieldValue          CString(2048),external,dll(dll_mode)
                   END
FileFieldsQueue    QUEUE,PRE(SOLFFV),external,dll(dll_mode)
FieldName           CString(60),external,dll(dll_mode)
FieldValue          CString(2048),external,dll(dll_mode)
                   END
SolaceEnvQueue       QUEUE,PRE(SOLENV),external,dll(dll_mode)
EnvName             CString(60),external,dll(dll_mode)
EnvValue            CString(255),external,dll(dll_mode)
                   end
SolaceEventQueue    Queue,PRE(SOLEVT),external,dll(dll_mode)
CtrlName             CString(30),external,dll(dll_mode)
EventName            CString(20),external,dll(dll_mode)
ProcName             CString(60),external,dll(dll_mode)
                    end
SolaceWatchQueue    Queue,PRE(SOLWAT),external,dll(dll_mode)
FieldName            CString(60),external,dll(dll_mode)
OldValue             CString(2048),external,dll(dll_mode)
FieldValue           CString(2048),external,dll(dll_mode)
CtrlName             CString(30),external,dll(dll_mode)
EventName            CString(20),external,dll(dll_mode)
ProcName             CString(60),external,dll(dll_mode)
                    end
SolaceMessQueue     Queue,PRE(SOLMES),external,dll(dll_mode)
Value1               CString(225),external,dll(dll_mode)
Value2               CString(225),external,dll(dll_mode)
Value3               CString(225),external,dll(dll_mode)
                    end
SolaceProcsQueue    Queue,PRE(SOLPRC),external,dll(dll_mode)
ProcName             CString(225),external,dll(dll_mode)
                    end
SolaceDBTraceQueue  Queue,PRE(SOLDBT),external,dll(dll_mode)          !New Stuff
TraceLine            CString(225),external,dll(dll_mode)
                    end
! [Priority 6000]

!--------------------------------------------------------------------------
! -----------Tintools-------------
!--------------------------------------------------------------------------
TinToolsViewRecord  EQUATE(15)
TinToolsCopyRecord  EQUATE(16)
!--------------------------------------------------------------------------
! -----------Tintools-------------
!--------------------------------------------------------------------------
tqField         QUEUE,TYPE
Desc              CSTRING(30)
Name              CSTRING(40)
IsString          BYTE
                END
! [Priority 9950]

! End of "Global Data"
Access:STDCHRGE      &FileManager,EXTERNAL,DLL
Relate:STDCHRGE      &RelationManager,EXTERNAL,DLL
Access:CARISMA       &FileManager,EXTERNAL,DLL
Relate:CARISMA       &RelationManager,EXTERNAL,DLL
Access:DEFEMAIL      &FileManager,EXTERNAL,DLL
Relate:DEFEMAIL      &RelationManager,EXTERNAL,DLL
Access:CRCDEFS       &FileManager,EXTERNAL,DLL
Relate:CRCDEFS       &RelationManager,EXTERNAL,DLL
Access:REGIONS       &FileManager,EXTERNAL,DLL
Relate:REGIONS       &RelationManager,EXTERNAL,DLL
Access:ACCREG        &FileManager,EXTERNAL,DLL
Relate:ACCREG        &RelationManager,EXTERNAL,DLL
Access:SIDACREG      &FileManager,EXTERNAL,DLL
Relate:SIDACREG      &RelationManager,EXTERNAL,DLL
Access:XMLJOBS       &FileManager,EXTERNAL,DLL
Relate:XMLJOBS       &RelationManager,EXTERNAL,DLL
Access:EXCHAMF       &FileManager,EXTERNAL,DLL
Relate:EXCHAMF       &RelationManager,EXTERNAL,DLL
Access:EXCHAUI       &FileManager,EXTERNAL,DLL
Relate:EXCHAUI       &RelationManager,EXTERNAL,DLL
Access:JOBSENG       &FileManager,EXTERNAL,DLL
Relate:JOBSENG       &RelationManager,EXTERNAL,DLL
Access:MULDESPJ      &FileManager,EXTERNAL,DLL
Relate:MULDESPJ      &RelationManager,EXTERNAL,DLL
Access:AUDSTAEX      &FileManager,EXTERNAL,DLL
Relate:AUDSTAEX      &RelationManager,EXTERNAL,DLL
Access:MULDESP       &FileManager,EXTERNAL,DLL
Relate:MULDESP       &RelationManager,EXTERNAL,DLL
Access:INSMODELS     &FileManager,EXTERNAL,DLL
Relate:INSMODELS     &RelationManager,EXTERNAL,DLL
Access:STOCKLOG      &FileManager,EXTERNAL,DLL
Relate:STOCKLOG      &RelationManager,EXTERNAL,DLL
Access:IMEILOG       &FileManager,EXTERNAL,DLL
Relate:IMEILOG       &RelationManager,EXTERNAL,DLL
Access:STMASAUD      &FileManager,EXTERNAL,DLL
Relate:STMASAUD      &RelationManager,EXTERNAL,DLL
Access:AUDSTATS      &FileManager,EXTERNAL,DLL
Relate:AUDSTATS      &RelationManager,EXTERNAL,DLL
Access:IEXPDEFS      &FileManager,EXTERNAL,DLL
Relate:IEXPDEFS      &RelationManager,EXTERNAL,DLL
Access:RAPIDLST      &FileManager,EXTERNAL,DLL
Relate:RAPIDLST      &RelationManager,EXTERNAL,DLL
Access:ICONTHIST     &FileManager,EXTERNAL,DLL
Relate:ICONTHIST     &RelationManager,EXTERNAL,DLL
Access:ORDTEMP       &FileManager,EXTERNAL,DLL
Relate:ORDTEMP       &RelationManager,EXTERNAL,DLL
Access:ORDITEMS      &FileManager,EXTERNAL,DLL
Relate:ORDITEMS      &RelationManager,EXTERNAL,DLL
Access:ORDHEAD       &FileManager,EXTERNAL,DLL
Relate:ORDHEAD       &RelationManager,EXTERNAL,DLL
Access:IMEISHIP      &FileManager,EXTERNAL,DLL
Relate:IMEISHIP      &RelationManager,EXTERNAL,DLL
Access:IACTION       &FileManager,EXTERNAL,DLL
Relate:IACTION       &RelationManager,EXTERNAL,DLL
Access:JOBTHIRD      &FileManager,EXTERNAL,DLL
Relate:JOBTHIRD      &RelationManager,EXTERNAL,DLL
Access:DEFAULTV      &FileManager,EXTERNAL,DLL
Relate:DEFAULTV      &RelationManager,EXTERNAL,DLL
Access:LOGRETRN      &FileManager,EXTERNAL,DLL
Relate:LOGRETRN      &RelationManager,EXTERNAL,DLL
Access:PRODCODE      &FileManager,EXTERNAL,DLL
Relate:PRODCODE      &RelationManager,EXTERNAL,DLL
Access:LOG2TEMP      &FileManager,EXTERNAL,DLL
Relate:LOG2TEMP      &RelationManager,EXTERNAL,DLL
Access:LOGSTOCK      &FileManager,EXTERNAL,DLL
Relate:LOGSTOCK      &RelationManager,EXTERNAL,DLL
Access:LABLGTMP      &FileManager,EXTERNAL,DLL
Relate:LABLGTMP      &RelationManager,EXTERNAL,DLL
Access:DEFEMAR1      &FileManager,EXTERNAL,DLL
Relate:DEFEMAR1      &RelationManager,EXTERNAL,DLL
Access:LETTERS       &FileManager,EXTERNAL,DLL
Relate:LETTERS       &RelationManager,EXTERNAL,DLL
Access:DEFEMAB2      &FileManager,EXTERNAL,DLL
Relate:DEFEMAB2      &RelationManager,EXTERNAL,DLL
Access:DEFEMAB3      &FileManager,EXTERNAL,DLL
Relate:DEFEMAB3      &RelationManager,EXTERNAL,DLL
Access:DEFEMAB4      &FileManager,EXTERNAL,DLL
Relate:DEFEMAB4      &RelationManager,EXTERNAL,DLL
Access:DEFEMAB5      &FileManager,EXTERNAL,DLL
Relate:DEFEMAB5      &RelationManager,EXTERNAL,DLL
Access:DEFEMAB6      &FileManager,EXTERNAL,DLL
Relate:DEFEMAB6      &RelationManager,EXTERNAL,DLL
Access:DEFEMAB7      &FileManager,EXTERNAL,DLL
Relate:DEFEMAB7      &RelationManager,EXTERNAL,DLL
Access:DEFEMAE2      &FileManager,EXTERNAL,DLL
Relate:DEFEMAE2      &RelationManager,EXTERNAL,DLL
Access:DEFEMAE3      &FileManager,EXTERNAL,DLL
Relate:DEFEMAE3      &RelationManager,EXTERNAL,DLL
Access:DEFEME2B      &FileManager,EXTERNAL,DLL
Relate:DEFEME2B      &RelationManager,EXTERNAL,DLL
Access:DEFEME3B      &FileManager,EXTERNAL,DLL
Relate:DEFEME3B      &RelationManager,EXTERNAL,DLL
Access:DEFEMAR2      &FileManager,EXTERNAL,DLL
Relate:DEFEMAR2      &RelationManager,EXTERNAL,DLL
Access:DEFEMAR3      &FileManager,EXTERNAL,DLL
Relate:DEFEMAR3      &RelationManager,EXTERNAL,DLL
Access:DEFEMAR4      &FileManager,EXTERNAL,DLL
Relate:DEFEMAR4      &RelationManager,EXTERNAL,DLL
Access:DEFEMAB1      &FileManager,EXTERNAL,DLL
Relate:DEFEMAB1      &RelationManager,EXTERNAL,DLL
Access:DEFEMAE1      &FileManager,EXTERNAL,DLL
Relate:DEFEMAE1      &RelationManager,EXTERNAL,DLL
Access:DEFEMAI2      &FileManager,EXTERNAL,DLL
Relate:DEFEMAI2      &RelationManager,EXTERNAL,DLL
Access:DEFSIDEX      &FileManager,EXTERNAL,DLL
Relate:DEFSIDEX      &RelationManager,EXTERNAL,DLL
Access:EXPGEN        &FileManager,EXTERNAL,DLL
Relate:EXPGEN        &RelationManager,EXTERNAL,DLL
Access:STOAUDIT      &FileManager,EXTERNAL,DLL
Relate:STOAUDIT      &RelationManager,EXTERNAL,DLL
Access:DEFAULT2      &FileManager,EXTERNAL,DLL
Relate:DEFAULT2      &RelationManager,EXTERNAL,DLL
Access:ACTION        &FileManager,EXTERNAL,DLL
Relate:ACTION        &RelationManager,EXTERNAL,DLL
Access:DEFRAPID      &FileManager,EXTERNAL,DLL
Relate:DEFRAPID      &RelationManager,EXTERNAL,DLL
Access:RETSALES      &FileManager,EXTERNAL,DLL
Relate:RETSALES      &RelationManager,EXTERNAL,DLL
Access:BOUNCER       &FileManager,EXTERNAL,DLL
Relate:BOUNCER       &RelationManager,EXTERNAL,DLL
Access:TEAMS         &FileManager,EXTERNAL,DLL
Relate:TEAMS         &RelationManager,EXTERNAL,DLL
Access:MERGE         &FileManager,EXTERNAL,DLL
Relate:MERGE         &RelationManager,EXTERNAL,DLL
Access:UPDDATA       &FileManager,EXTERNAL,DLL
Relate:UPDDATA       &RelationManager,EXTERNAL,DLL
Access:EXPSPARES     &FileManager,EXTERNAL,DLL
Relate:EXPSPARES     &RelationManager,EXTERNAL,DLL
Access:WEBDEFLT      &FileManager,EXTERNAL,DLL
Relate:WEBDEFLT      &RelationManager,EXTERNAL,DLL
Access:EXCAUDIT      &FileManager,EXTERNAL,DLL
Relate:EXCAUDIT      &RelationManager,EXTERNAL,DLL
Access:COMMONFA      &FileManager,EXTERNAL,DLL
Relate:COMMONFA      &RelationManager,EXTERNAL,DLL
Access:PARAMSS       &FileManager,EXTERNAL,DLL
Relate:PARAMSS       &RelationManager,EXTERNAL,DLL
Access:INSJOBS       &FileManager,EXTERNAL,DLL
Relate:INSJOBS       &RelationManager,EXTERNAL,DLL
Access:CONSIGN       &FileManager,EXTERNAL,DLL
Relate:CONSIGN       &RelationManager,EXTERNAL,DLL
Access:JOBEXACC      &FileManager,EXTERNAL,DLL
Relate:JOBEXACC      &RelationManager,EXTERNAL,DLL
Access:MODELCOL      &FileManager,EXTERNAL,DLL
Relate:MODELCOL      &RelationManager,EXTERNAL,DLL
Access:COMMCAT       &FileManager,EXTERNAL,DLL
Relate:COMMCAT       &RelationManager,EXTERNAL,DLL
Access:MESSAGES      &FileManager,EXTERNAL,DLL
Relate:MESSAGES      &RelationManager,EXTERNAL,DLL
Access:LOGEXHE       &FileManager,EXTERNAL,DLL
Relate:LOGEXHE       &RelationManager,EXTERNAL,DLL
Access:DEFCRC        &FileManager,EXTERNAL,DLL
Relate:DEFCRC        &RelationManager,EXTERNAL,DLL
Access:PAYTYPES      &FileManager,EXTERNAL,DLL
Relate:PAYTYPES      &RelationManager,EXTERNAL,DLL
Access:COMMONWP      &FileManager,EXTERNAL,DLL
Relate:COMMONWP      &RelationManager,EXTERNAL,DLL
Access:LOGEXCH       &FileManager,EXTERNAL,DLL
Relate:LOGEXCH       &RelationManager,EXTERNAL,DLL
Access:QAREASON      &FileManager,EXTERNAL,DLL
Relate:QAREASON      &RelationManager,EXTERNAL,DLL
Access:COMMONCP      &FileManager,EXTERNAL,DLL
Relate:COMMONCP      &RelationManager,EXTERNAL,DLL
Access:ADDSEARCH     &FileManager,EXTERNAL,DLL
Relate:ADDSEARCH     &RelationManager,EXTERNAL,DLL
Access:SIDDEF        &FileManager,EXTERNAL,DLL
Relate:SIDDEF        &RelationManager,EXTERNAL,DLL
Access:CONTHIST      &FileManager,EXTERNAL,DLL
Relate:CONTHIST      &RelationManager,EXTERNAL,DLL
Access:CONTACTS      &FileManager,EXTERNAL,DLL
Relate:CONTACTS      &RelationManager,EXTERNAL,DLL
Access:EXPJOBS       &FileManager,EXTERNAL,DLL
Relate:EXPJOBS       &RelationManager,EXTERNAL,DLL
Access:EXPAUDIT      &FileManager,EXTERNAL,DLL
Relate:EXPAUDIT      &RelationManager,EXTERNAL,DLL
Access:JOBBATCH      &FileManager,EXTERNAL,DLL
Relate:JOBBATCH      &RelationManager,EXTERNAL,DLL
Access:DEFEDI2       &FileManager,EXTERNAL,DLL
Relate:DEFEDI2       &RelationManager,EXTERNAL,DLL
Access:DEFPRINT      &FileManager,EXTERNAL,DLL
Relate:DEFPRINT      &RelationManager,EXTERNAL,DLL
Access:DEFWEB        &FileManager,EXTERNAL,DLL
Relate:DEFWEB        &RelationManager,EXTERNAL,DLL
Access:EXPCITY       &FileManager,EXTERNAL,DLL
Relate:EXPCITY       &RelationManager,EXTERNAL,DLL
Access:PROCCODE      &FileManager,EXTERNAL,DLL
Relate:PROCCODE      &RelationManager,EXTERNAL,DLL
Access:COLOUR        &FileManager,EXTERNAL,DLL
Relate:COLOUR        &RelationManager,EXTERNAL,DLL
Access:VODAIMP       &FileManager,EXTERNAL,DLL
Relate:VODAIMP       &RelationManager,EXTERNAL,DLL
Access:JOBSVODA      &FileManager,EXTERNAL,DLL
Relate:JOBSVODA      &RelationManager,EXTERNAL,DLL
Access:XREPACT       &FileManager,EXTERNAL,DLL
Relate:XREPACT       &RelationManager,EXTERNAL,DLL
Access:ACCESDEF      &FileManager,EXTERNAL,DLL
Relate:ACCESDEF      &RelationManager,EXTERNAL,DLL
Access:STANTEXT      &FileManager,EXTERNAL,DLL
Relate:STANTEXT      &RelationManager,EXTERNAL,DLL
Access:NOTESENG      &FileManager,EXTERNAL,DLL
Relate:NOTESENG      &RelationManager,EXTERNAL,DLL
Access:IAUDIT        &FileManager,EXTERNAL,DLL
Relate:IAUDIT        &RelationManager,EXTERNAL,DLL
Access:IINVJOB       &FileManager,EXTERNAL,DLL
Relate:IINVJOB       &RelationManager,EXTERNAL,DLL
Access:EXPLABG       &FileManager,EXTERNAL,DLL
Relate:EXPLABG       &RelationManager,EXTERNAL,DLL
Access:INSDEF        &FileManager,EXTERNAL,DLL
Relate:INSDEF        &RelationManager,EXTERNAL,DLL
Access:IEXPUSE       &FileManager,EXTERNAL,DLL
Relate:IEXPUSE       &RelationManager,EXTERNAL,DLL
Access:IRECEIVE      &FileManager,EXTERNAL,DLL
Relate:IRECEIVE      &RelationManager,EXTERNAL,DLL
Access:IJOBSTATUS    &FileManager,EXTERNAL,DLL
Relate:IJOBSTATUS    &RelationManager,EXTERNAL,DLL
Access:ICLCOST       &FileManager,EXTERNAL,DLL
Relate:ICLCOST       &RelationManager,EXTERNAL,DLL
Access:IVEHDETS      &FileManager,EXTERNAL,DLL
Relate:IVEHDETS      &RelationManager,EXTERNAL,DLL
Access:IAPPENG       &FileManager,EXTERNAL,DLL
Relate:IAPPENG       &RelationManager,EXTERNAL,DLL
Access:IAPPTYPE      &FileManager,EXTERNAL,DLL
Relate:IAPPTYPE      &RelationManager,EXTERNAL,DLL
Access:INSMAKE       &FileManager,EXTERNAL,DLL
Relate:INSMAKE       &RelationManager,EXTERNAL,DLL
Access:INSAPPS       &FileManager,EXTERNAL,DLL
Relate:INSAPPS       &RelationManager,EXTERNAL,DLL
Access:ISUBCONT      &FileManager,EXTERNAL,DLL
Relate:ISUBCONT      &RelationManager,EXTERNAL,DLL
Access:TRDSPEC       &FileManager,EXTERNAL,DLL
Relate:TRDSPEC       &RelationManager,EXTERNAL,DLL
Access:ITIMES        &FileManager,EXTERNAL,DLL
Relate:ITIMES        &RelationManager,EXTERNAL,DLL
Access:IEQUIP        &FileManager,EXTERNAL,DLL
Relate:IEQUIP        &RelationManager,EXTERNAL,DLL
Access:EPSCSV        &FileManager,EXTERNAL,DLL
Relate:EPSCSV        &RelationManager,EXTERNAL,DLL
Access:IENGINEERS    &FileManager,EXTERNAL,DLL
Relate:IENGINEERS    &RelationManager,EXTERNAL,DLL
Access:ICLIENTS      &FileManager,EXTERNAL,DLL
Relate:ICLIENTS      &RelationManager,EXTERNAL,DLL
Access:ISUBPOST      &FileManager,EXTERNAL,DLL
Relate:ISUBPOST      &RelationManager,EXTERNAL,DLL
Access:IJOBTYPE      &FileManager,EXTERNAL,DLL
Relate:IJOBTYPE      &RelationManager,EXTERNAL,DLL
Access:EXPGENDM      &FileManager,EXTERNAL,DLL
Relate:EXPGENDM      &RelationManager,EXTERNAL,DLL
Access:EDIBATCH      &FileManager,EXTERNAL,DLL
Relate:EDIBATCH      &RelationManager,EXTERNAL,DLL
Access:ORDPEND       &FileManager,EXTERNAL,DLL
Relate:ORDPEND       &RelationManager,EXTERNAL,DLL
Access:STOHIST       &FileManager,EXTERNAL,DLL
Relate:STOHIST       &RelationManager,EXTERNAL,DLL
Access:NEWFEAT       &FileManager,EXTERNAL,DLL
Relate:NEWFEAT       &RelationManager,EXTERNAL,DLL
Access:EXREASON      &FileManager,EXTERNAL,DLL
Relate:EXREASON      &RelationManager,EXTERNAL,DLL
Access:REPTYDEF      &FileManager,EXTERNAL,DLL
Relate:REPTYDEF      &RelationManager,EXTERNAL,DLL
Access:EPSIMP        &FileManager,EXTERNAL,DLL
Relate:EPSIMP        &RelationManager,EXTERNAL,DLL
Access:RETACCOUNTSLIST &FileManager,EXTERNAL,DLL
Relate:RETACCOUNTSLIST &RelationManager,EXTERNAL,DLL
Access:PRIORITY      &FileManager,EXTERNAL,DLL
Relate:PRIORITY      &RelationManager,EXTERNAL,DLL
Access:DEFEPS        &FileManager,EXTERNAL,DLL
Relate:DEFEPS        &RelationManager,EXTERNAL,DLL
Access:JOBSE         &FileManager,EXTERNAL,DLL
Relate:JOBSE         &RelationManager,EXTERNAL,DLL
Access:REPEXTTP      &FileManager,EXTERNAL,DLL
Relate:REPEXTTP      &RelationManager,EXTERNAL,DLL
Access:MANFAULT      &FileManager,EXTERNAL,DLL
Relate:MANFAULT      &RelationManager,EXTERNAL,DLL
Access:INVPARTS      &FileManager,EXTERNAL,DLL
Relate:INVPARTS      &RelationManager,EXTERNAL,DLL
Access:JOBSEARC      &FileManager,EXTERNAL,DLL
Relate:JOBSEARC      &RelationManager,EXTERNAL,DLL
Access:INVPATMP      &FileManager,EXTERNAL,DLL
Relate:INVPATMP      &RelationManager,EXTERNAL,DLL
Access:TRACHAR       &FileManager,EXTERNAL,DLL
Relate:TRACHAR       &RelationManager,EXTERNAL,DLL
Access:CITYSERV      &FileManager,EXTERNAL,DLL
Relate:CITYSERV      &RelationManager,EXTERNAL,DLL
Access:PICKNOTE      &FileManager,EXTERNAL,DLL
Relate:PICKNOTE      &RelationManager,EXTERNAL,DLL
Access:DISCOUNT      &FileManager,EXTERNAL,DLL
Relate:DISCOUNT      &RelationManager,EXTERNAL,DLL
Access:LOCVALUE      &FileManager,EXTERNAL,DLL
Relate:LOCVALUE      &RelationManager,EXTERNAL,DLL
Access:STOCKTYP      &FileManager,EXTERNAL,DLL
Relate:STOCKTYP      &RelationManager,EXTERNAL,DLL
Access:IEMAILS       &FileManager,EXTERNAL,DLL
Relate:IEMAILS       &RelationManager,EXTERNAL,DLL
Access:COURIER       &FileManager,EXTERNAL,DLL
Relate:COURIER       &RelationManager,EXTERNAL,DLL
Access:TRDPARTY      &FileManager,EXTERNAL,DLL
Relate:TRDPARTY      &RelationManager,EXTERNAL,DLL
Access:JOBSTMP       &FileManager,EXTERNAL,DLL
Relate:JOBSTMP       &RelationManager,EXTERNAL,DLL
Access:LOGALLOC      &FileManager,EXTERNAL,DLL
Relate:LOGALLOC      &RelationManager,EXTERNAL,DLL
Access:JOBNOTES      &FileManager,EXTERNAL,DLL
Relate:JOBNOTES      &RelationManager,EXTERNAL,DLL
Access:PROINV        &FileManager,EXTERNAL,DLL
Relate:PROINV        &RelationManager,EXTERNAL,DLL
Access:EXPBUS        &FileManager,EXTERNAL,DLL
Relate:EXPBUS        &RelationManager,EXTERNAL,DLL
Access:IMPCITY       &FileManager,EXTERNAL,DLL
Relate:IMPCITY       &RelationManager,EXTERNAL,DLL
Access:JOBACTMP      &FileManager,EXTERNAL,DLL
Relate:JOBACTMP      &RelationManager,EXTERNAL,DLL
Access:RETPARTSLIST  &FileManager,EXTERNAL,DLL
Relate:RETPARTSLIST  &RelationManager,EXTERNAL,DLL
Access:TRANTYPE      &FileManager,EXTERNAL,DLL
Relate:TRANTYPE      &RelationManager,EXTERNAL,DLL
Access:DESBATCH      &FileManager,EXTERNAL,DLL
Relate:DESBATCH      &RelationManager,EXTERNAL,DLL
Access:JOBVODAC      &FileManager,EXTERNAL,DLL
Relate:JOBVODAC      &RelationManager,EXTERNAL,DLL
Access:ESNMODEL      &FileManager,EXTERNAL,DLL
Relate:ESNMODEL      &RelationManager,EXTERNAL,DLL
Access:JOBPAYMT      &FileManager,EXTERNAL,DLL
Relate:JOBPAYMT      &RelationManager,EXTERNAL,DLL
Access:LOGASSSTTEMP  &FileManager,EXTERNAL,DLL
Relate:LOGASSSTTEMP  &RelationManager,EXTERNAL,DLL
Access:STOCK         &FileManager,EXTERNAL,DLL
Relate:STOCK         &RelationManager,EXTERNAL,DLL
Access:LOGGED        &FileManager,EXTERNAL,DLL
Relate:LOGGED        &RelationManager,EXTERNAL,DLL
Access:LOGRTHIS      &FileManager,EXTERNAL,DLL
Relate:LOGRTHIS      &RelationManager,EXTERNAL,DLL
Access:LOGSTHIS      &FileManager,EXTERNAL,DLL
Relate:LOGSTHIS      &RelationManager,EXTERNAL,DLL
Access:LOGASSST      &FileManager,EXTERNAL,DLL
Relate:LOGASSST      &RelationManager,EXTERNAL,DLL
Access:LOGSTOLC      &FileManager,EXTERNAL,DLL
Relate:LOGSTOLC      &RelationManager,EXTERNAL,DLL
Access:LOGTEMP       &FileManager,EXTERNAL,DLL
Relate:LOGTEMP       &RelationManager,EXTERNAL,DLL
Access:LOGSERST      &FileManager,EXTERNAL,DLL
Relate:LOGSERST      &RelationManager,EXTERNAL,DLL
Access:LOGDEFLT      &FileManager,EXTERNAL,DLL
Relate:LOGDEFLT      &RelationManager,EXTERNAL,DLL
Access:LOGSTLOC      &FileManager,EXTERNAL,DLL
Relate:LOGSTLOC      &RelationManager,EXTERNAL,DLL
Access:MULTIDEF      &FileManager,EXTERNAL,DLL
Relate:MULTIDEF      &RelationManager,EXTERNAL,DLL
Access:TRAFAULO      &FileManager,EXTERNAL,DLL
Relate:TRAFAULO      &RelationManager,EXTERNAL,DLL
Access:MPXJOBS       &FileManager,EXTERNAL,DLL
Relate:MPXJOBS       &RelationManager,EXTERNAL,DLL
Access:MPXSTAT       &FileManager,EXTERNAL,DLL
Relate:MPXSTAT       &RelationManager,EXTERNAL,DLL
Access:TRAFAULT      &FileManager,EXTERNAL,DLL
Relate:TRAFAULT      &RelationManager,EXTERNAL,DLL
Access:EXMINLEV      &FileManager,EXTERNAL,DLL
Relate:EXMINLEV      &RelationManager,EXTERNAL,DLL
Access:LOGSTHII      &FileManager,EXTERNAL,DLL
Relate:LOGSTHII      &RelationManager,EXTERNAL,DLL
Access:LOGCLSTE      &FileManager,EXTERNAL,DLL
Relate:LOGCLSTE      &RelationManager,EXTERNAL,DLL
Access:STATCRIT      &FileManager,EXTERNAL,DLL
Relate:STATCRIT      &RelationManager,EXTERNAL,DLL
Access:NETWORKS      &FileManager,EXTERNAL,DLL
Relate:NETWORKS      &RelationManager,EXTERNAL,DLL
Access:LOGDESNO      &FileManager,EXTERNAL,DLL
Relate:LOGDESNO      &RelationManager,EXTERNAL,DLL
Access:CPNDPRTS      &FileManager,EXTERNAL,DLL
Relate:CPNDPRTS      &RelationManager,EXTERNAL,DLL
Access:LOGSALCD      &FileManager,EXTERNAL,DLL
Relate:LOGSALCD      &RelationManager,EXTERNAL,DLL
Access:DEFSTOCK      &FileManager,EXTERNAL,DLL
Relate:DEFSTOCK      &RelationManager,EXTERNAL,DLL
Access:JOBLOHIS      &FileManager,EXTERNAL,DLL
Relate:JOBLOHIS      &RelationManager,EXTERNAL,DLL
Access:LOAN          &FileManager,EXTERNAL,DLL
Relate:LOAN          &RelationManager,EXTERNAL,DLL
Access:NOTESCON      &FileManager,EXTERNAL,DLL
Relate:NOTESCON      &RelationManager,EXTERNAL,DLL
Access:VATCODE       &FileManager,EXTERNAL,DLL
Relate:VATCODE       &RelationManager,EXTERNAL,DLL
Access:LOANHIST      &FileManager,EXTERNAL,DLL
Relate:LOANHIST      &RelationManager,EXTERNAL,DLL
Access:TURNARND      &FileManager,EXTERNAL,DLL
Relate:TURNARND      &RelationManager,EXTERNAL,DLL
Access:ORDJOBS       &FileManager,EXTERNAL,DLL
Relate:ORDJOBS       &RelationManager,EXTERNAL,DLL
Access:MERGETXT      &FileManager,EXTERNAL,DLL
Relate:MERGETXT      &RelationManager,EXTERNAL,DLL
Access:MERGELET      &FileManager,EXTERNAL,DLL
Relate:MERGELET      &RelationManager,EXTERNAL,DLL
Access:ORDSTOCK      &FileManager,EXTERNAL,DLL
Relate:ORDSTOCK      &RelationManager,EXTERNAL,DLL
Access:EXCHHIST      &FileManager,EXTERNAL,DLL
Relate:EXCHHIST      &RelationManager,EXTERNAL,DLL
Access:ACCESSOR      &FileManager,EXTERNAL,DLL
Relate:ACCESSOR      &RelationManager,EXTERNAL,DLL
Access:SUBACCAD      &FileManager,EXTERNAL,DLL
Relate:SUBACCAD      &RelationManager,EXTERNAL,DLL
Access:NOTESDEL      &FileManager,EXTERNAL,DLL
Relate:NOTESDEL      &RelationManager,EXTERNAL,DLL
Access:PICKDET       &FileManager,EXTERNAL,DLL
Relate:PICKDET       &RelationManager,EXTERNAL,DLL
Access:WARPARTS      &FileManager,EXTERNAL,DLL
Relate:WARPARTS      &RelationManager,EXTERNAL,DLL
Access:PARTS         &FileManager,EXTERNAL,DLL
Relate:PARTS         &RelationManager,EXTERNAL,DLL
Access:JOBACC        &FileManager,EXTERNAL,DLL
Relate:JOBACC        &RelationManager,EXTERNAL,DLL
Access:USELEVEL      &FileManager,EXTERNAL,DLL
Relate:USELEVEL      &RelationManager,EXTERNAL,DLL
Access:ALLLEVEL      &FileManager,EXTERNAL,DLL
Relate:ALLLEVEL      &RelationManager,EXTERNAL,DLL
Access:QAPARTSTEMP   &FileManager,EXTERNAL,DLL
Relate:QAPARTSTEMP   &RelationManager,EXTERNAL,DLL
Access:ACCAREAS      &FileManager,EXTERNAL,DLL
Relate:ACCAREAS      &RelationManager,EXTERNAL,DLL
Access:AUDIT         &FileManager,EXTERNAL,DLL
Relate:AUDIT         &RelationManager,EXTERNAL,DLL
Access:USERS         &FileManager,EXTERNAL,DLL
Relate:USERS         &RelationManager,EXTERNAL,DLL
Access:SIDREG        &FileManager,EXTERNAL,DLL
Relate:SIDREG        &RelationManager,EXTERNAL,DLL
Access:LOCSHELF      &FileManager,EXTERNAL,DLL
Relate:LOCSHELF      &RelationManager,EXTERNAL,DLL
Access:PENDMAIL      &FileManager,EXTERNAL,DLL
Relate:PENDMAIL      &RelationManager,EXTERNAL,DLL
Access:DEFAULTS      &FileManager,EXTERNAL,DLL
Relate:DEFAULTS      &RelationManager,EXTERNAL,DLL
Access:REPTYCAT      &FileManager,EXTERNAL,DLL
Relate:REPTYCAT      &RelationManager,EXTERNAL,DLL
Access:REPTYPETEMP   &FileManager,EXTERNAL,DLL
Relate:REPTYPETEMP   &RelationManager,EXTERNAL,DLL
Access:MANFPALO      &FileManager,EXTERNAL,DLL
Relate:MANFPALO      &RelationManager,EXTERNAL,DLL
Access:JOBSTAGE      &FileManager,EXTERNAL,DLL
Relate:JOBSTAGE      &RelationManager,EXTERNAL,DLL
Access:MANFAUPA      &FileManager,EXTERNAL,DLL
Relate:MANFAUPA      &RelationManager,EXTERNAL,DLL
Access:EXPPARTS      &FileManager,EXTERNAL,DLL
Relate:EXPPARTS      &RelationManager,EXTERNAL,DLL
Access:REPEXTRP      &FileManager,EXTERNAL,DLL
Relate:REPEXTRP      &RelationManager,EXTERNAL,DLL
Access:GENSHORT      &FileManager,EXTERNAL,DLL
Relate:GENSHORT      &RelationManager,EXTERNAL,DLL
Access:LOCINTER      &FileManager,EXTERNAL,DLL
Relate:LOCINTER      &RelationManager,EXTERNAL,DLL
Access:STAHEAD       &FileManager,EXTERNAL,DLL
Relate:STAHEAD       &RelationManager,EXTERNAL,DLL
Access:SIDEXUPD      &FileManager,EXTERNAL,DLL
Relate:SIDEXUPD      &RelationManager,EXTERNAL,DLL
Access:SIDSRVCN      &FileManager,EXTERNAL,DLL
Relate:SIDSRVCN      &RelationManager,EXTERNAL,DLL
Access:SIDMODSC      &FileManager,EXTERNAL,DLL
Relate:SIDMODSC      &RelationManager,EXTERNAL,DLL
Access:SIDNWDAY      &FileManager,EXTERNAL,DLL
Relate:SIDNWDAY      &RelationManager,EXTERNAL,DLL
Access:NOTESINV      &FileManager,EXTERNAL,DLL
Relate:NOTESINV      &RelationManager,EXTERNAL,DLL
Access:NOTESFAU      &FileManager,EXTERNAL,DLL
Relate:NOTESFAU      &RelationManager,EXTERNAL,DLL
Access:MANFAULO      &FileManager,EXTERNAL,DLL
Relate:MANFAULO      &RelationManager,EXTERNAL,DLL
Access:STATREP       &FileManager,EXTERNAL,DLL
Relate:STATREP       &RelationManager,EXTERNAL,DLL
Access:JOBEXHIS      &FileManager,EXTERNAL,DLL
Relate:JOBEXHIS      &RelationManager,EXTERNAL,DLL
Access:STARECIP      &FileManager,EXTERNAL,DLL
Relate:STARECIP      &RelationManager,EXTERNAL,DLL
Access:RETSTOCK      &FileManager,EXTERNAL,DLL
Relate:RETSTOCK      &RelationManager,EXTERNAL,DLL
Access:RETPAY        &FileManager,EXTERNAL,DLL
Relate:RETPAY        &RelationManager,EXTERNAL,DLL
Access:RETDESNO      &FileManager,EXTERNAL,DLL
Relate:RETDESNO      &RelationManager,EXTERNAL,DLL
Access:ORDPARTS      &FileManager,EXTERNAL,DLL
Relate:ORDPARTS      &RelationManager,EXTERNAL,DLL
Access:STOCKMIN      &FileManager,EXTERNAL,DLL
Relate:STOCKMIN      &RelationManager,EXTERNAL,DLL
Access:LOCATION      &FileManager,EXTERNAL,DLL
Relate:LOCATION      &RelationManager,EXTERNAL,DLL
Access:WPARTTMP      &FileManager,EXTERNAL,DLL
Relate:WPARTTMP      &RelationManager,EXTERNAL,DLL
Access:PARTSTMP      &FileManager,EXTERNAL,DLL
Relate:PARTSTMP      &RelationManager,EXTERNAL,DLL
Access:EXCHANGE      &FileManager,EXTERNAL,DLL
Relate:EXCHANGE      &RelationManager,EXTERNAL,DLL
Access:STOESN        &FileManager,EXTERNAL,DLL
Relate:STOESN        &RelationManager,EXTERNAL,DLL
Access:LOANACC       &FileManager,EXTERNAL,DLL
Relate:LOANACC       &RelationManager,EXTERNAL,DLL
Access:SUPPLIER      &FileManager,EXTERNAL,DLL
Relate:SUPPLIER      &RelationManager,EXTERNAL,DLL
Access:EXCHACC       &FileManager,EXTERNAL,DLL
Relate:EXCHACC       &RelationManager,EXTERNAL,DLL
Access:RECIPTYP      &FileManager,EXTERNAL,DLL
Relate:RECIPTYP      &RelationManager,EXTERNAL,DLL
Access:SUPVALA       &FileManager,EXTERNAL,DLL
Relate:SUPVALA       &RelationManager,EXTERNAL,DLL
Access:ESTPARTS      &FileManager,EXTERNAL,DLL
Relate:ESTPARTS      &RelationManager,EXTERNAL,DLL
Access:TRAEMAIL      &FileManager,EXTERNAL,DLL
Relate:TRAEMAIL      &RelationManager,EXTERNAL,DLL
Access:SUPVALB       &FileManager,EXTERNAL,DLL
Relate:SUPVALB       &RelationManager,EXTERNAL,DLL
Access:JOBS          &FileManager,EXTERNAL,DLL
Relate:JOBS          &RelationManager,EXTERNAL,DLL
Access:MODELNUM      &FileManager,EXTERNAL,DLL
Relate:MODELNUM      &RelationManager,EXTERNAL,DLL
Access:SUBEMAIL      &FileManager,EXTERNAL,DLL
Relate:SUBEMAIL      &RelationManager,EXTERNAL,DLL
Access:SUPPTEMP      &FileManager,EXTERNAL,DLL
Relate:SUPPTEMP      &RelationManager,EXTERNAL,DLL
Access:ORDERS        &FileManager,EXTERNAL,DLL
Relate:ORDERS        &RelationManager,EXTERNAL,DLL
Access:UNITTYPE      &FileManager,EXTERNAL,DLL
Relate:UNITTYPE      &RelationManager,EXTERNAL,DLL
Access:STATUS        &FileManager,EXTERNAL,DLL
Relate:STATUS        &RelationManager,EXTERNAL,DLL
Access:REPAIRTY      &FileManager,EXTERNAL,DLL
Relate:REPAIRTY      &RelationManager,EXTERNAL,DLL
Access:TRADETMP      &FileManager,EXTERNAL,DLL
Relate:TRADETMP      &RelationManager,EXTERNAL,DLL
Access:EXCCHRGE      &FileManager,EXTERNAL,DLL
Relate:EXCCHRGE      &RelationManager,EXTERNAL,DLL
Access:INVOICE       &FileManager,EXTERNAL,DLL
Relate:INVOICE       &RelationManager,EXTERNAL,DLL
Access:TRDBATCH      &FileManager,EXTERNAL,DLL
Relate:TRDBATCH      &RelationManager,EXTERNAL,DLL
Access:TRACHRGE      &FileManager,EXTERNAL,DLL
Relate:TRACHRGE      &RelationManager,EXTERNAL,DLL
Access:TRDMODEL      &FileManager,EXTERNAL,DLL
Relate:TRDMODEL      &RelationManager,EXTERNAL,DLL
Access:STOMODEL      &FileManager,EXTERNAL,DLL
Relate:STOMODEL      &RelationManager,EXTERNAL,DLL
Access:TRADEACC      &FileManager,EXTERNAL,DLL
Relate:TRADEACC      &RelationManager,EXTERNAL,DLL
Access:QUERYREA      &FileManager,EXTERNAL,DLL
Relate:QUERYREA      &RelationManager,EXTERNAL,DLL
Access:SUBCHRGE      &FileManager,EXTERNAL,DLL
Relate:SUBCHRGE      &RelationManager,EXTERNAL,DLL
Access:DEFCHRGE      &FileManager,EXTERNAL,DLL
Relate:DEFCHRGE      &RelationManager,EXTERNAL,DLL
Access:SUBTRACC      &FileManager,EXTERNAL,DLL
Relate:SUBTRACC      &RelationManager,EXTERNAL,DLL
Access:CHARTYPE      &FileManager,EXTERNAL,DLL
Relate:CHARTYPE      &RelationManager,EXTERNAL,DLL
Access:MANUFACT      &FileManager,EXTERNAL,DLL
Relate:MANUFACT      &RelationManager,EXTERNAL,DLL
Access:USMASSIG      &FileManager,EXTERNAL,DLL
Relate:USMASSIG      &RelationManager,EXTERNAL,DLL
Access:EXPWARPARTS   &FileManager,EXTERNAL,DLL
Relate:EXPWARPARTS   &RelationManager,EXTERNAL,DLL
Access:USUASSIG      &FileManager,EXTERNAL,DLL
Relate:USUASSIG      &RelationManager,EXTERNAL,DLL
Access:XMLTRADE      &FileManager,EXTERNAL,DLL
Relate:XMLTRADE      &RelationManager,EXTERNAL,DLL
Access:XMLJOBQ       &FileManager,EXTERNAL,DLL
Relate:XMLJOBQ       &RelationManager,EXTERNAL,DLL
Access:XMLDEFS       &FileManager,EXTERNAL,DLL
Relate:XMLDEFS       &RelationManager,EXTERNAL,DLL
Access:XMLSB         &FileManager,EXTERNAL,DLL
Relate:XMLSB         &RelationManager,EXTERNAL,DLL
Access:MPXDEFS       &FileManager,EXTERNAL,DLL
Relate:MPXDEFS       &RelationManager,EXTERNAL,DLL
Access:XMLSAM        &FileManager,EXTERNAL,DLL
Relate:XMLSAM        &RelationManager,EXTERNAL,DLL
Access:XMLDOCS       &FileManager,EXTERNAL,DLL
Relate:XMLDOCS       &RelationManager,EXTERNAL,DLL
Access:DEFEDI        &FileManager,EXTERNAL,DLL
Relate:DEFEDI        &RelationManager,EXTERNAL,DLL
Access:SIDSRVCN_ALIAS &FileManager,EXTERNAL,DLL
Relate:SIDSRVCN_ALIAS &RelationManager,EXTERNAL,DLL
Access:MPXJOBS_ALIAS &FileManager,EXTERNAL,DLL
Relate:MPXJOBS_ALIAS &RelationManager,EXTERNAL,DLL
Access:MPXSTAT_ALIAS &FileManager,EXTERNAL,DLL
Relate:MPXSTAT_ALIAS &RelationManager,EXTERNAL,DLL
Access:XMLJOBS_ALIAS &FileManager,EXTERNAL,DLL
Relate:XMLJOBS_ALIAS &RelationManager,EXTERNAL,DLL
Access:AUDIT_ALIAS   &FileManager,EXTERNAL,DLL
Relate:AUDIT_ALIAS   &RelationManager,EXTERNAL,DLL
Access:XMLSAM_ALIAS  &FileManager,EXTERNAL,DLL
Relate:XMLSAM_ALIAS  &RelationManager,EXTERNAL,DLL
Access:MULDESP_ALIAS &FileManager,EXTERNAL,DLL
Relate:MULDESP_ALIAS &RelationManager,EXTERNAL,DLL
Access:MULDESPJ_ALIAS &FileManager,EXTERNAL,DLL
Relate:MULDESPJ_ALIAS &RelationManager,EXTERNAL,DLL
Access:SUBCONTALIAS  &FileManager,EXTERNAL,DLL
Relate:SUBCONTALIAS  &RelationManager,EXTERNAL,DLL
Access:INSJOBSALIAS  &FileManager,EXTERNAL,DLL
Relate:INSJOBSALIAS  &RelationManager,EXTERNAL,DLL
Access:ESTPARTS_ALIAS &FileManager,EXTERNAL,DLL
Relate:ESTPARTS_ALIAS &RelationManager,EXTERNAL,DLL
Access:IEQUIP_ALIAS  &FileManager,EXTERNAL,DLL
Relate:IEQUIP_ALIAS  &RelationManager,EXTERNAL,DLL
Access:JOBS2_ALIAS   &FileManager,EXTERNAL,DLL
Relate:JOBS2_ALIAS   &RelationManager,EXTERNAL,DLL
Access:LOGASSST_ALIAS &FileManager,EXTERNAL,DLL
Relate:LOGASSST_ALIAS &RelationManager,EXTERNAL,DLL
Access:LOGSTOCK_ALIAS &FileManager,EXTERNAL,DLL
Relate:LOGSTOCK_ALIAS &RelationManager,EXTERNAL,DLL
Access:LOGSERST_ALIAS &FileManager,EXTERNAL,DLL
Relate:LOGSERST_ALIAS &RelationManager,EXTERNAL,DLL
Access:COURIER_ALIAS &FileManager,EXTERNAL,DLL
Relate:COURIER_ALIAS &RelationManager,EXTERNAL,DLL
Access:JOBNOTES_ALIAS &FileManager,EXTERNAL,DLL
Relate:JOBNOTES_ALIAS &RelationManager,EXTERNAL,DLL
Access:TRDBATCH_ALIAS &FileManager,EXTERNAL,DLL
Relate:TRDBATCH_ALIAS &RelationManager,EXTERNAL,DLL
Access:INVOICE_ALIAS &FileManager,EXTERNAL,DLL
Relate:INVOICE_ALIAS &RelationManager,EXTERNAL,DLL
Access:ORDPEND_ALIAS &FileManager,EXTERNAL,DLL
Relate:ORDPEND_ALIAS &RelationManager,EXTERNAL,DLL
Access:RETSALES_ALIAS &FileManager,EXTERNAL,DLL
Relate:RETSALES_ALIAS &RelationManager,EXTERNAL,DLL
Access:RETSTOCK_ALIAS &FileManager,EXTERNAL,DLL
Relate:RETSTOCK_ALIAS &RelationManager,EXTERNAL,DLL
Access:LOCATION_ALIAS &FileManager,EXTERNAL,DLL
Relate:LOCATION_ALIAS &RelationManager,EXTERNAL,DLL
Access:COMMONFA_ALIAS &FileManager,EXTERNAL,DLL
Relate:COMMONFA_ALIAS &RelationManager,EXTERNAL,DLL
Access:COMMONCP_ALIAS &FileManager,EXTERNAL,DLL
Relate:COMMONCP_ALIAS &RelationManager,EXTERNAL,DLL
Access:COMMONWP_ALIAS &FileManager,EXTERNAL,DLL
Relate:COMMONWP_ALIAS &RelationManager,EXTERNAL,DLL
Access:STOMODEL_ALIAS &FileManager,EXTERNAL,DLL
Relate:STOMODEL_ALIAS &RelationManager,EXTERNAL,DLL
Access:JOBPAYMT_ALIAS &FileManager,EXTERNAL,DLL
Relate:JOBPAYMT_ALIAS &RelationManager,EXTERNAL,DLL
Access:PARTSTMP_ALIAS &FileManager,EXTERNAL,DLL
Relate:PARTSTMP_ALIAS &RelationManager,EXTERNAL,DLL
Access:MPXDEFS_ALIAS &FileManager,EXTERNAL,DLL
Relate:MPXDEFS_ALIAS &RelationManager,EXTERNAL,DLL
Access:WPARTTMP_ALIAS &FileManager,EXTERNAL,DLL
Relate:WPARTTMP_ALIAS &RelationManager,EXTERNAL,DLL
Access:LOAN_ALIAS    &FileManager,EXTERNAL,DLL
Relate:LOAN_ALIAS    &RelationManager,EXTERNAL,DLL
Access:EXCHANGE_ALIAS &FileManager,EXTERNAL,DLL
Relate:EXCHANGE_ALIAS &RelationManager,EXTERNAL,DLL
Access:ACCAREAS_ALIAS &FileManager,EXTERNAL,DLL
Relate:ACCAREAS_ALIAS &RelationManager,EXTERNAL,DLL
Access:USERS_ALIAS   &FileManager,EXTERNAL,DLL
Relate:USERS_ALIAS   &RelationManager,EXTERNAL,DLL
Access:WARPARTS_ALIAS &FileManager,EXTERNAL,DLL
Relate:WARPARTS_ALIAS &RelationManager,EXTERNAL,DLL
Access:ORDPARTS_ALIAS &FileManager,EXTERNAL,DLL
Relate:ORDPARTS_ALIAS &RelationManager,EXTERNAL,DLL
Access:PARTS_ALIAS   &FileManager,EXTERNAL,DLL
Relate:PARTS_ALIAS   &RelationManager,EXTERNAL,DLL
Access:STOCK_ALIAS   &FileManager,EXTERNAL,DLL
Relate:STOCK_ALIAS   &RelationManager,EXTERNAL,DLL
Access:JOBS_ALIAS    &FileManager,EXTERNAL,DLL
Relate:JOBS_ALIAS    &RelationManager,EXTERNAL,DLL
Access:XMLTRADE_ALIAS &FileManager,EXTERNAL,DLL
Relate:XMLTRADE_ALIAS &RelationManager,EXTERNAL,DLL
Access:XMLSB_ALIAS   &FileManager,EXTERNAL,DLL
Relate:XMLSB_ALIAS   &RelationManager,EXTERNAL,DLL
Access:STDCHRGE_ALIAS &FileManager,EXTERNAL,DLL
Relate:STDCHRGE_ALIAS &RelationManager,EXTERNAL,DLL
GlobalRequest        BYTE,EXTERNAL,DLL
GlobalResponse       BYTE,EXTERNAL,DLL
VCRRequest           LONG,EXTERNAL,DLL
FuzzyMatcher         FuzzyClass
LocalErrors          ErrorClass
LocalINIMgr          INIClass
GlobalErrors         &ErrorClass
INIMgr               &INIClass
DLLInitializer       CLASS                              !This object is used to trigger initialization of the dll
Construct              PROCEDURE
Destruct               PROCEDURE
                     END
   CODE
