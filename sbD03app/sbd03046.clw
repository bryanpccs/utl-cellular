

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03046.INC'),ONCE        !Local module procedure declarations
                     END








Parts_Not_Received_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_ord_id          USHORT,AUTO
save_orp_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
job_stock_temp       STRING(20)
tmp:PrintedBy        STRING(60)
no_temp              STRING('NO')
count_temp           STRING(9)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Line_Purchase        REAL
Line_Total           REAL
!-----------------------------------------------------------------------------
Process:View         VIEW(SUPPLIER)
                       PROJECT(sup:Account_Number)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:Contact_Name)
                       PROJECT(sup:Fax_Number)
                     END
Report               REPORT('Parts Not Received Report'),AT(385,2781,7521,8510),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,948,7521,1250),USE(?unnamed)
                         STRING('Supplier:'),AT(5000,52),USE(?String30),TRN,FONT(,8,,)
                         STRING(@s30),AT(5833,52),USE(sup:Company_Name),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Account No.:'),AT(5000,208),USE(?String31),TRN,FONT(,8,,)
                         STRING(@s15),AT(5833,208),USE(sup:Account_Number),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(5833,365),USE(sup:Contact_Name),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(5833,521),USE(sup:Fax_Number),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax Number:'),AT(5000,521),USE(?String31:3),TRN,FONT(,8,,)
                         STRING('Contact Name:'),AT(5000,365),USE(?String31:2),TRN,FONT(,8,,)
                         STRING('Date Printed:'),AT(5000,833),USE(?String22),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5833,833),USE(ReportRunDate),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(5000,677),USE(?String29),TRN,FONT(,8,,)
                         STRING(@s60),AT(5833,677),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(5000,990),USE(?String59),TRN,FONT(,8,,)
                         STRING(@s3),AT(5833,990),PAGENO,USE(?ReportPageNo),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6146,990),USE(?String35),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6354,990,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,177),USE(?DetailBand)
                           STRING(@n8.2),AT(4531,0),USE(orp:Purchase_Cost),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@s9),AT(104,0),USE(ord:Order_Number),TRN,RIGHT,FONT(,7,,)
                           STRING(@s30),AT(833,0),USE(orp:Part_Number),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s30),AT(2427,0),USE(orp:Description),TRN,LEFT,FONT(,7,,)
                           STRING(@s6),AT(4031,0),USE(orp:Quantity),TRN,RIGHT,FONT(,7,,)
                           STRING(@s9),AT(5833,0),USE(job_stock_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@d6b),AT(6823,0),USE(ord:Date),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(5135,0),USE(Line_Purchase),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         END
                       END
new_page               DETAIL,PAGEBEFORE(-1),AT(,,,52),USE(?new_page)
                       END
Totals                 DETAIL,AT(,,,458),USE(?Totals)
                         LINE,AT(104,52,7240,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Purchase Cost Total'),AT(4125,104),USE(?String48),TRN,FONT(,7,,FONT:bold)
                         STRING(@n11.2),AT(5083,104),USE(Line_Total),TRN,RIGHT,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Total Lines Not Received:'),AT(104,104),USE(?String33),TRN,FONT(,7,,FONT:bold)
                         STRING(@s9),AT(1344,104),USE(count_temp),TRN,FONT(,7,,FONT:bold)
                       END
                       FOOTER,AT(375,11344,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:OrderCompanyName),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('PARTS NOT RECEIVED REPORT'),AT(4271,0,3177,260),USE(?String20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:OrderAddressLine1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:OrderAddressLine2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:OrderAddressLine3),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,729,1156,156),USE(def:OrderPostcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,990),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,990),USE(def:OrderTelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1146),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s30),AT(521,1146),USE(def:OrderFaxNumber),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1302),USE(?String19:2),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1302,3844,198),USE(def:OrderEmailAddress),TRN,FONT(,9,,)
                         STRING('Purchase Cost'),AT(4521,2083),USE(?String45),TRN,FONT('Arial',7,,FONT:bold)
                         STRING('Total'),AT(5417,2083),USE(?String46),TRN,FONT('Arial',7,,FONT:bold)
                         STRING('Order No'),AT(94,2083),USE(?String44),TRN,FONT(,7,,FONT:bold)
                         STRING('Part Number'),AT(823,2083),USE(?String24),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Qty'),AT(4219,2083),USE(?String25),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Job/Retail/Stock'),AT(5823,2083),USE(?String28),TRN,FONT(,7,,FONT:bold)
                         STRING('Date Ordered'),AT(6708,2083),USE(?String32),TRN,FONT(,7,,FONT:bold)
                         STRING('Description'),AT(2417,2083),USE(?String27),TRN,FONT(,7,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Parts_Not_Received_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:SUPPLIER.Open
  Relate:DEFAULTS.Open
  Access:ORDPEND.UseFile
  Access:ORDPARTS.UseFile
  Access:ORDERS.UseFile
  Access:USERS.UseFile
  
  
  RecordsToProcess = RECORDS(SUPPLIER)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(SUPPLIER,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(sup:Company_Name_Key)
      Process:View{Prop:Filter} = ''
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        print# = 1
        Sort(glo:Queue,glo:pointer)
        glo:pointer = sup:company_name
        Get(glo:Queue,glo:pointer)
        If Error()
            print# = 0
        End!If Error()
        
        If print# = 1
            new_page# = 1
            count_temp = 0
            Line_Total = 0
            save_ord_id = access:orders.savefile()
            access:orders.clearkey(ord:supplier_key)
            ord:supplier     = sup:company_name
            set(ord:supplier_key,ord:supplier_key)
            loop
                if access:orders.next()
                   break
                end !if
                if ord:supplier     <> sup:company_name      |
                    then break.  ! end if
                If ord:all_received = 'YES'
                    Cycle
                End!If ord:all_received = 'YES'
                If glo:select1 = 'OVR'
                    Weekend_Routine(ord:date,sup:normal_supply_period,end_date")
                    If Today() <= end_date"
                        Cycle
                    End!If Today() > day_number$
                End!If glo:select1 = 'OVR'
                save_orp_id = access:ordparts.savefile()
                access:ordparts.clearkey(orp:order_number_key)
                orp:order_number    = ord:order_number
                set(orp:order_number_key,orp:order_number_key)
                loop
                    if access:ordparts.next()
                       break
                    end !if
                    if orp:order_number    <> ord:order_number      |
                        then break.  ! end if
                    yldcnt# += 1
                    if yldcnt# > 25
                       yield() ; yldcnt# = 0
                    end !if
                    !Neil!
                    Line_Purchase = orp:Quantity*orp:Purchase_Cost
                    If orp:job_number <> ''
                        Case orp:part_type
                            Of 'RET'
                                job_stock_temp = 'R-' & orp:job_number
                            Else
                                job_stock_temp  = 'J-' & orp:job_number
                        End!Case orp:part_type
                    Else!If orp:job_number <> ''
                        job_stock_temp = 'Stock'
                    End!If orp:job_number <> ''
                    If orp:all_received <> 'YES'
                        If new_page# = 1
                            Print(rpt:new_page)
                            new_page# = 0
                        End
                        Line_Total += Line_Purchase
                        count_temp += 1
                        Print(rpt:detail)
                    End!If orp:all_received <> 'YES'
                end !loop
                access:ordparts.restorefile(save_orp_id)
            end !loop
            access:orders.restorefile(save_ord_id)
            If count_temp <> 0
                Print(rpt:totals)
            End!If count_temp <> 0
        
        End!If print# = 1
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(SUPPLIER,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:ORDERS.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'SUPPLIER')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Parts_Not_Received_Report'
  END
  Report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Parts_Not_Received_Report',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Parts_Not_Received_Report',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Parts_Not_Received_Report',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Parts_Not_Received_Report',1)
    SolaceViewVars('save_ord_id',save_ord_id,'Parts_Not_Received_Report',1)
    SolaceViewVars('save_orp_id',save_orp_id,'Parts_Not_Received_Report',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Parts_Not_Received_Report',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Parts_Not_Received_Report',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Parts_Not_Received_Report',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Parts_Not_Received_Report',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Parts_Not_Received_Report',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Parts_Not_Received_Report',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Parts_Not_Received_Report',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Parts_Not_Received_Report',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Parts_Not_Received_Report',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Parts_Not_Received_Report',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Parts_Not_Received_Report',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Parts_Not_Received_Report',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Parts_Not_Received_Report',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Parts_Not_Received_Report',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Parts_Not_Received_Report',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Parts_Not_Received_Report',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Parts_Not_Received_Report',1)
    SolaceViewVars('InitialPath',InitialPath,'Parts_Not_Received_Report',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Parts_Not_Received_Report',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Parts_Not_Received_Report',1)
    SolaceViewVars('job_stock_temp',job_stock_temp,'Parts_Not_Received_Report',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Parts_Not_Received_Report',1)
    SolaceViewVars('no_temp',no_temp,'Parts_Not_Received_Report',1)
    SolaceViewVars('count_temp',count_temp,'Parts_Not_Received_Report',1)
    SolaceViewVars('Line_Purchase',Line_Purchase,'Parts_Not_Received_Report',1)
    SolaceViewVars('Line_Total',Line_Total,'Parts_Not_Received_Report',1)


BuildCtrlQueue      Routine







