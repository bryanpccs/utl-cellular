

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('SBD03082.INC'),ONCE        !Local module procedure declarations
                     END


StatusExport PROCEDURE                                !Generated from procedure template - Process

Progress:Thermometer BYTE
save_aus_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
SheetDesc            CSTRING(41)
tmp:CustomerName     STRING(60)
tmp:StatusUser       STRING(60)
tmp:StatusDate       DATE
tmp:OldStatus        STRING(30)
Process:View         VIEW(JOBS)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,148,60),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(4,4,140,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(4,30,140,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(44,40,56,16),USE(?Progress:Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                    !Progress Manager
LastPctValue        LONG(0)                           !---ClarioNET 20
ClarioNET:PW:UserString   STRING(40)
ClarioNET:PW:PctText      STRING(40)
ClarioNET:PW WINDOW('Progress...'),AT(,,142,59),CENTER,TIMER(1),GRAY,DOUBLE
               PROGRESS,USE(Progress:Thermometer,,?ClarioNET:Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
               STRING(''),AT(0,3,141,10),USE(ClarioNET:PW:UserString),CENTER
               STRING(''),AT(0,30,141,10),USE(ClarioNET:PW:PctText),CENTER
             END
?F1SS                EQUATE(1000)
!# SW2 Extension            STRING(3)
f1Action             BYTE
f1FileName           CSTRING(256)
RowNumber            USHORT
StartRow             LONG
StartCol             LONG
Template             CSTRING(129)
f1Disabled           BYTE(0)
ssFieldQ              QUEUE(tqField).
CQ                    QUEUE(tqField).
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Progress:UserString{prop:FontColor} = -1
    ?Progress:UserString{prop:Color} = 15066597
    ?Progress:PctText{prop:FontColor} = -1
    ?Progress:PctText{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!---------------------
ssInit         ROUTINE
!---------------------
  DO ssBuildQueue                                     ! Initialize f1 field queue
  IF NOT UsBrowse(SsFieldQ,'StatusExport',,,,CQ,SheetDesc,StartRow,StartCol,Template)
    ThisWindow.Kill
    RETURN
  END

!---------------------
ssBuildQueue   ROUTINE
!---------------------
    ssFieldQ.Name = 'Format(job:Date_Booked,@d17)'
    ssFieldQ.Desc = 'Date Booked'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Ref_Number'
    ssFieldQ.Desc = 'Job Number'
    ssFieldQ.IsString = FALSE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Account_Number'
    ssFieldQ.Desc = 'Account Number'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Model_Number'
    ssFieldQ.Desc = 'Model Number'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'chr(39) & job:esn'
    ssFieldQ.Desc = 'I.M.E.I. Number'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:CustomerName'
    ssFieldQ.Desc = 'Customer Details'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Current_Status'
    ssFieldQ.Desc = 'Status'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:OldStatus'
    ssFieldQ.Desc = 'Previous Status'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Format(aus:StatusDate,@d17)'
    ssFieldQ.Desc = 'Status Changed Date'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:StatusUser'
    ssFieldQ.Desc = 'Status Changed By'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Location'
    ssFieldQ.Desc = 'Location'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Unit_Type'
    ssFieldQ.Desc = 'Unit Type'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Manufacturer'
    ssFieldQ.Desc = 'Manufacturer'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
  SORT(ssFieldQ,+ssFieldQ.Desc)



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'StatusExport',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'StatusExport',1)
    SolaceViewVars('save_aus_id',save_aus_id,'StatusExport',1)
    SolaceViewVars('SheetDesc',SheetDesc,'StatusExport',1)
    SolaceViewVars('tmp:CustomerName',tmp:CustomerName,'StatusExport',1)
    SolaceViewVars('tmp:StatusUser',tmp:StatusUser,'StatusExport',1)
    SolaceViewVars('tmp:StatusDate',tmp:StatusDate,'StatusExport',1)
    SolaceViewVars('tmp:OldStatus',tmp:OldStatus,'StatusExport',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Progress:Thermometer;  SolaceCtrlName = '?Progress:Thermometer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:UserString;  SolaceCtrlName = '?Progress:UserString';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:PctText;  SolaceCtrlName = '?Progress:PctText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:Cancel;  SolaceCtrlName = '?Progress:Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  LastPctValue = 0                                    !---ClarioNET 37
  ClarioNET:PW:PctText = ?Progress:PctText{Prop:Text}
  ClarioNET:PW:UserString{Prop:Text} = ?Progress:UserString{Prop:Text}
  ClarioNET:OpenPushWindow(ClarioNET:PW)
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('StatusExport')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'StatusExport')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
    BIND('tmp:CustomerName',tmp:CustomerName)
    BIND('tmp:OldStatus',tmp:OldStatus)
    BIND('tmp:StatusDate',tmp:StatusDate)
    BIND('tmp:StatusUser',tmp:StatusUser)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  DO ssInit
  Relate:AUDSTATS.Open
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  Do RecolourWindow
  IF ThisWindow.Response <> RequestCancelled
    UsInitDoc(CQ,?F1ss,StartRow,StartCol,Template)
    ?F1SS{'Sheets("Sheet1").Name'} = SheetDesc
    IF Template = ''
      RowNumber = StartRow + 1
    ELSE
      RowNumber = StartRow
    END
  END
  ! support for CPCS
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:date_booked)
  ThisProcess.AddSortOrder(job:Date_Booked_Key)
  ThisProcess.AddRange(job:date_booked,GLO:Select20,GLO:Select21)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}='Status Report'
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(JOBS,'QUICKSCAN=on')
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  Do RecolourWindow


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDSTATS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'StatusExport',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?Progress:Cancel
                                                      ! SW2 This section changed
      IF Message('Would you like to view the partially created spreadsheet?','Process Cancelled',ICON:Question,Button:Yes+Button:No,Button:No)|
      = Button:Yes
        UsDeInit(f1FileName, ?F1SS, 1)
      END
      ReturnValue = Level:Fatal
      RETURN ReturnValue
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ThisWindow.Response = RequestCompleted
    f1FileName = ''
    UsDeInit(f1FileName, ?F1SS, 1)
  
  ELSE
    IF RowNumber THEN ?F1SS{'quit()'}.
  END
  ReturnValue = PARENT.TakeCloseEvent()
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'StatusExport')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:ClosePushWindow(ClarioNET:PW)         !---ClarioNET 88
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  IF LastPctValue <> Progress:Thermometer             !---ClarioNET 99
    IF INLIST(Progress:Thermometer,'5','10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95')
      LastPctValue = Progress:Thermometer
      ClarioNET:UpdatePushWindow(ClarioNET:PW)
    END
  END
      print# = 1
      tmp:CustomerName=''
      tmp:StatusUser=''
      tmp:StatusDate=''
      tmp:OldStatus=''
  
      Case glo:select22
          Of 'WAR'
              If job:chargeable_job = 'YES' Or job:warranty_job <> 'YES'
                  print# = 0
              End!If job:chargeable_job = 'YES'
          Of 'CHA'
              IF job:warranty_job = 'YES' Or job:chargeable_job <> 'YES'
                  print# = 0
              End!IF job:warranty_job = 'YES'
          Of 'SPL'
              If job:warranty_job <> 'YES' Or job:chargeable_job <> 'YES'
                  print# = 0
              End!If job:warranty_job <> 'YES' Or job:chargeable_job <> 'YES'
          Of 'WAS'
              If job:warranty_job <> 'YES'
                  print# = 0
              End!If job:warranty_job <> 'YES'
          Of 'CHS'
              If job:chargeable_job <> 'YES'
                  print# = 0
              End!If job:chargeable_job <> 'YES'
      End!Case glo:select22
  
      If glo:select1 <> ''
          If job:account_number <> glo:select1
              print# = 0
          End
      End
      If glo:select30 <> ''
          access:subtracc.clearkey(sub:account_number_key)
          sub:account_number = job:account_number
          if access:subtracc.fetch(sub:account_number_key) = level:benign
              If sub:main_account_number <> glo:select30
                  print# = 0
              End!If sub:main_account_number <> glo:select30
          Else!if access:subtracc.fetch(sub:account_number_key) = level:benign
              print# = 0
          end!if access:subtracc.fetch(sub:account_number_key) = level:benign
      End!If glo:select30 <> ''
  
      If glo:select2 <> ''
          If job:current_status <> glo:select2
              print# = 0
          End
      End
  
      If glo:select3 <> ''
          If JOB:Turnaround_Time <> glo:select3
              Print# = 0
          End
      End
  
      If glo:select4 <> ''
          If job:workshop <> glo:select4
              print# = 0
          End
      End
  
      If glo:select5 <> ''
          If job:location <> glo:select5
              print# = 0
          End
      End
  
      If glo:select6 <> ''
          If job:engineer <> glo:select6
              print# = 0
          End
      End
  
      IF glo:select7 <> ''
          If job:model_number <> glo:select7
              print# = 0
          End
      End
  
      If glo:select8 <> ''
          If job:manufacturer <> glo:select8
              print# = 0
          End
      End
  
      If glo:select9 <> ''
          If job:unit_type <> glo:select9
              print# = 0
          End
      End
  
      If glo:select10 <> ''
          If job:transit_type <> glo:select10
              print# = 0
          End
      End
  
      If glo:select11 <> ''
          If job:chargeable_job = 'YES'
              If job:charge_type <> glo:select11
                  print# = 0
              End
          Else
              if glo:select22 = 'CHA'
                  print# = 0
              End!if glo:select22 = 'CHA'
          End!If job:chargeable_job = 'YES'
      End
  
      If glo:select12 <> ''
          If job:warranty_job = 'YES'
              If job:warranty_charge_type <> glo:select12
                  print# = 0
              Else
                  If glo:select22 = 'WAR'
                      print# = 0
                  End!If glo:select22 = 'WAR'
              End
          End!If job:warranty_job = 'YES'
      End
  
      If glo:select15 <> ''
          If job:chargeable_job = 'YES'
              If job:repair_type <> glo:select15
                  print# = 0
              End
          Else!If job:chargeable_job = 'YES'
              If glo:select22 = 'CHA'
                  print# = 0
              End!If job:select22 = 'CHA'
          End!If job:chargeable_job = 'YES'
      End
  
      If glo:select16 <> ''
          If job:warranty_job = 'YES'
              If job:repair_type_warranty <> glo:select16
                  print# = 0
              End
          Else!If job:warranty_job = 'YES'
              If glo:select22 = 'WAR'
                  print# = 0
              End!If glo:select22 = 'WAR'
          End!If job:warranty_job = 'YES'
      End
  
      Case glo:select17
          Of 'INV'
              Case glo:select22
                  Of 'WAR'
                      If job:invoice_number_warranty = ''
                          print# = 0
                      End!If job:invoice_number_warranty = ''
                  Of 'CHA'
                      If job:invoice_number = ''
                          print# = 0
                      End
                  Else
                      If (job:chargeable_job = 'YES' and job:invoice_number = '') Or |
                          (job:warranty_job = 'YES' and job:invoice_number_warranty = '')
                          print# = 0
                      End!If job:invoice_number = '' Of job:invoice_Number_warranty = ''
              End!Case glo:select22
          Of 'NOT'
              Case glo:select22
                  Of 'WAR'
                      If job:invoice_number_warranty <> ''
                          print# = 0
                      End!If job:invoice_number_warranty = ''
                  Of 'CHA'
                      If job:invoice_number <> ''
                          print# = 0
                      End
                  Else
                      If (job:chargeable_job = 'YES' and job:invoice_number <> '') Or |
                          (job:warranty_job = 'YES' and job:invoice_number_warranty <> '')
                          print# = 0
                      End!If job:invoice_number = '' Of job:invoice_Number_warranty = ''
              End!Case glo:select22
      End
  
      Case glo:select19
          Of 'DES'
              If job:consignment_number = ''
                  Print# = 0
              End!If job:consignment_number = ''
          Of 'NOT'
              If job:consignment_number <> ''
                  Print# = 0
              End!If job:consignment_number <> ''
      End!Case glo:select19
  
      Case glo:select18
          Of 'COM'
              If job:date_completed = ''
                  print# = 0
              End
          Of 'NOT'
              If job:date_completed <> ''
                  print# = 0
              End
      End
      If glo:select23 <> ''
          If job:exchange_status <> glo:select23
              print# = 0
          End!If job:exchange_status <> glo:select23
      End!If glo:select23
  
      If glo:select24 <> ''
          If job:loan_status <> glo:select24
              print# = 0
          End!If job:loan_status <> glo:select24
      End!If glo:select24 <> ''
  
      If print# = 1
  !        count_temp += 1
  !        tmp:RecordsCount += 1
          If job:surname  = ''
              access:subtracc.clearkey(sub:account_number_key)
              sub:account_number = job:account_number
              if access:subtracc.fetch(sub:account_number_key) = level:benign
                  access:tradeacc.clearkey(tra:account_number_key) 
                  tra:account_number = sub:main_account_number
                  if access:tradeacc.fetch(tra:account_number_key) = level:benign
                      if tra:use_sub_accounts = 'YES'
                          tmp:CustomerName    = sub:company_name
                      else!if tra:use_sub_accounts = 'YES'
                          tmp:CustomerName    = tra:company_name
                      end!if tra:use_sub_accounts = 'YES'
                  end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
              end!if access:subtracc.fetch(sub:account_number_key) = level:benign  
          Else!If job:surname  = ''
              tmp:CustomerName    = job:surname
          End!If job:surname  = ''
  
          Access:AUDSTATS.ClearKey(aus:NewStatusKey)
          aus:RefNumber = job:Ref_Number
          aus:Type      = 'JOB'
          aus:NewStatus = job:Current_Status
          Set(aus:NewStatusKey,aus:NewStatusKey)
          Loop
              If Access:AUDSTATS.PREVIOUS()
                 Break
              End !If
              If aus:RefNumber <> job:Ref_Number      |
              Or aus:Type      <> 'JOB'      |
              Or aus:NewStatus <> job:Current_Status      |
                  Then Break.  ! End If
              tmp:OldStatus   = aus:OldStatus
              tmp:StatusDate  = aus:DateChanged
              
              Access:USERS.Clearkey(use:User_Code_Key)
              use:User_Code   = aus:UserCode
              If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                  !Found
                  tmp:StatusUser  = Clip(use:Forename) & ' ' & Clip(use:Surname)
              Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
  
              Break
  
          End !Loop
  
      Else
          Return Level:User
      End
  ReturnValue = PARENT.TakeRecord()
  UsFillRow(CQ,?F1SS,RowNumber,StartCol)
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

