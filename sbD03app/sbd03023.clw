

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('SBD03023.INC'),ONCE        !Local module procedure declarations
                     END


Stock_Forecast_Report_Old PROCEDURE                   !Generated from procedure template - Report

Progress:Thermometer BYTE
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:RecordsCount     REAL
save_shi_id          USHORT,AUTO
count_temp           REAL
endofreport          BYTE,AUTO
tmp:printer          STRING(255)
FilesOpened          BYTE
tmp:PrintedBy        STRING(60)
thirty_days_temp     REAL
sixty_days_temp      REAL
ninty_days_temp      REAL
days_to_reorder_temp REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(STOCK)
                       PROJECT(sto:Description)
                       PROJECT(sto:Location)
                       PROJECT(sto:Part_Number)
                       PROJECT(sto:Quantity_On_Order)
                       PROJECT(sto:Quantity_Stock)
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

report               REPORT('Stock Forecast Report'),AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,792,7521,1365),USE(?unnamed)
                         STRING('Site Location:'),AT(5000,156),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,156),USE(GLO:Select1),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('XX/XX/XXXX'),AT(5885,781),USE(?ReportDateStamp),TRN,FONT(,8,,FONT:bold)
                         STRING('Suppress Zeros'),AT(5000,365),USE(?String27),TRN,FONT(,8,,)
                         STRING(@s40),AT(5885,365),USE(GLO:Select2),TRN,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(5000,573),USE(?String29),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,573),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Printed:'),AT(5000,781),USE(?String31),TRN,FONT(,8,,)
                         STRING('Page Number:'),AT(5000,990),USE(?string59),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,990),USE(ReportPageNumber),TRN,LEFT,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,229),USE(?detailband)
                           STRING(@n8b),AT(5479,0),USE(sixty_days_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@n8b),AT(6156,0),USE(ninty_days_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@n8b),AT(6896,0),USE(days_to_reorder_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@s25),AT(208,0),USE(sto:Part_Number),TRN,FONT(,8,,)
                           STRING(@s25),AT(1823,0),USE(sto:Description),TRN,LEFT,FONT(,8,,)
                           STRING(@N8b),AT(3490,0),USE(sto:Quantity_Stock),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@N8b),AT(4115,0),USE(sto:Quantity_On_Order),TRN,RIGHT,FONT(,8,,)
                           STRING(@n8b),AT(4854,0),USE(thirty_days_temp),TRN,RIGHT,FONT(,8,,)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Lines:'),AT(208,104),USE(?String38),TRN
                           STRING(@s8),AT(1719,104),USE(count_temp),TRN,LEFT,FONT(,,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('STOCK FORECAST REPORT'),AT(4271,0,3177,260),USE(?string20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,885),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s15),AT(521,885),USE(def:Telephone_Number),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1042),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s15),AT(521,1042),USE(def:Fax_Number),TRN,FONT(,9,,)
                         STRING('Part Number'),AT(208,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(1823,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('In Stock'),AT(3646,2083),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('On Order'),AT(4219,2083),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('0-30 Days'),AT(4896,2083),USE(?String33),TRN,FONT(,8,,FONT:bold)
                         STRING('31-60 Days'),AT(5521,2083),USE(?String33:2),TRN,FONT(,8,,FONT:bold)
                         STRING('61-90 Days'),AT(6198,2083),USE(?String33:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Reorder'),AT(7031,2135),USE(?String36),TRN,FONT(,7,,FONT:bold)
                         STRING('Days To'),AT(7031,2031),USE(?String36:2),TRN,FONT(,7,,FONT:bold)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepStringClass                  !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 22
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Progress:UserString{prop:FontColor} = -1
    ?Progress:UserString{prop:Color} = 15066597
    ?Progress:PctText{prop:FontColor} = -1
    ?Progress:PctText{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Stock_Forecast_Report_Old',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Stock_Forecast_Report_Old',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Stock_Forecast_Report_Old',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Stock_Forecast_Report_Old',1)
    SolaceViewVars('tmp:RecordsCount',tmp:RecordsCount,'Stock_Forecast_Report_Old',1)
    SolaceViewVars('save_shi_id',save_shi_id,'Stock_Forecast_Report_Old',1)
    SolaceViewVars('count_temp',count_temp,'Stock_Forecast_Report_Old',1)
    SolaceViewVars('endofreport',endofreport,'Stock_Forecast_Report_Old',1)
    SolaceViewVars('tmp:printer',tmp:printer,'Stock_Forecast_Report_Old',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Stock_Forecast_Report_Old',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Stock_Forecast_Report_Old',1)
    SolaceViewVars('thirty_days_temp',thirty_days_temp,'Stock_Forecast_Report_Old',1)
    SolaceViewVars('sixty_days_temp',sixty_days_temp,'Stock_Forecast_Report_Old',1)
    SolaceViewVars('ninty_days_temp',ninty_days_temp,'Stock_Forecast_Report_Old',1)
    SolaceViewVars('days_to_reorder_temp',days_to_reorder_temp,'Stock_Forecast_Report_Old',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Progress:Thermometer;  SolaceCtrlName = '?Progress:Thermometer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:UserString;  SolaceCtrlName = '?Progress:UserString';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:PctText;  SolaceCtrlName = '?Progress:PctText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:Cancel;  SolaceCtrlName = '?Progress:Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 34
  PARENT.Ask


ThisWindow.AskPreview PROCEDURE

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(AskPreview, ())
  if tmp:RecordsCount = 0
  	Case MessageEx('There are no records that match the selected criteria.','ServiceBase 2000',|
  	               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
  		Of 1 ! &OK Button
  	End!Case MessageEx
      tmp:RecordsCount = 1
  end!if tmp:RecordsCount = 0
  IF ClarioNETServer:Active()                         !---ClarioNET 46
    IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
      ENDPAGE(SELF.Report)
      LOOP TempNameFunc_Flag = 1 TO RECORDS(SELF.PreviewQueue)
        GET(SELF.PreviewQueue, TempNameFunc_Flag)
        ClarioNET:AddMetafileName(SELF.PreviewQueue.Filename)
      END
      ClarioNET:SetReportProperties(report)
      FREE(SELF.PreviewQueue)
      TempNameFunc_Flag = 0
    END
  ELSE
    PARENT.AskPreview
  END                                                 !---ClarioNET 48
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(AskPreview, ())


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Stock_Forecast_Report_Old')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Stock_Forecast_Report_Old')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:STOCK.Open
  Access:USERS.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisReport.Init(Process:View, Relate:STOCK, ?Progress:PctText, Progress:Thermometer, ProgressMgr, sto:Part_Number)
  ThisReport.CaseSensitiveValue = FALSE
  ThisReport.AddSortOrder(sto:Location_Part_Description_Key)
  ThisReport.AddRange(sto:Location,GLO:Select1)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:STOCK.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  Do RecolourWindow


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:STOCK.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Stock_Forecast_Report_Old',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  tmp:printer = printer{propprint:device}
  choose_printer(new_printer",preview",copies")
  printer{propprint:device} = new_printer"
  printer{propprint:copies} = copies"
  if preview" <> 'YES'
      self.skippreview = true
  end!if preview" <> 'YES'
  ReturnValue = PARENT.OpenReport()
  Set(Defaults)
  Access:defaults.next()
  
  access:users.clearkey(use:password_key)
  use:password =glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = Clip(use:forename) & ' ' & Clip(use:surname)
  IF ~ReturnValue
    report$?ReportPageNumber{PROP:PageNo}=True
  END
  IF ~ReturnValue
    SELF.Report $ ?ReportDateStamp{PROP:Text}=FORMAT(TODAY(),@d6)
  END
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF ClarioNETServer:Active()
    ClarioNET:EndReport                               !---ClarioNET 101
  END                                                 !---ClarioNET 102
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Stock_Forecast_Report_Old')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeNoRecords, ())
  return
  PARENT.TakeNoRecords
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeNoRecords, ())


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 91
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
    IF ClarioNETServer:Active()                       !---ClarioNET 44
      IF TempNameFunc_Flag = 0
        TempNameFunc_Flag = 1
        report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
      END
    END
  print# = 1
  thirty_days_temp = 0
  sixty_days_temp = 0
  ninty_days_temp = 0
  save_shi_id = access:stohist.savefile()
  access:stohist.clearkey(shi:ref_number_key)
  shi:ref_number = sto:ref_number
  set(shi:ref_number_key,shi:ref_number_key)
  loop
      if access:stohist.next()
         break
      end !if
      if shi:ref_number <> sto:ref_number      |
          then break.  ! end if
      yldcnt# += 1
      if yldcnt# > 25
         yield() ; yldcnt# = 0
      end !if
      If shi:job_number = ''
          Cycle
      End
      If shi:date => Today() - 30
          If shi:transaction_type = 'DEC'
              thirty_days_temp += shi:quantity
          End!If shi:transaction_type = 'DEC'
          If shi:transaction_type = 'REC'
              thirty_days_temp -= shi:quantity
          End!If shi:transaction_type = 'REC'
      End!If shi:date => Today() - 30
      If shi:date => Today() - 60 And shi:date < Today() - 30
          If shi:transaction_type = 'DEC'
              sixty_days_temp += shi:quantity
          End!If shi:transaction_type = 'DEC'
          If shi:transaction_type = 'REC'
              sixty_days_temp -= shi:quantity
          End!If shi:transaction_type = 'REC'
      End!If shi:date => Today() - 30
      If shi:date => Today() - 90 And shi:date < Today() - 60
          If shi:transaction_type = 'DEC'
              ninty_days_temp += shi:quantity
          End!If shi:transaction_type = 'DEC'
          If shi:transaction_type = 'REC'
              ninty_days_temp -= shi:quantity
          End!If shi:transaction_type = 'REC'
      End!If shi:date => Today() - 30
  end !loop
  access:stohist.restorefile(save_shi_id)
  If glo:select2 = 'YES'
      If thirty_days_temp = 0 And sixty_days_temp = 0 And ninty_days_temp = 0
          print# = 0
      End!If used_temp = 0
  End!If glo:select5 = 'YES'
  If print# = 1
      days_to_reorder_temp = Int(sto:quantity_stock / ((thirty_days_temp + sixty_days_temp + ninty_days_temp) / 90))
      count_temp += 1
      tmp:RecordsCount += 1
      Print(rpt:detail)
  End!If print# = 1
  ReturnValue = PARENT.TakeRecord()
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

