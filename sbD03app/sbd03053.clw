

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_fwiz.inc'),ONCE

                     MAP
                       INCLUDE('SBD03053.INC'),ONCE        !Local module procedure declarations
                     END



Status_Report_Criteria_Old PROCEDURE                  !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
CurrentTab           STRING(80)
save_stm_id          USHORT,AUTO
save_stm_ali_id      USHORT,AUTO
sav:Purchase_Cost    REAL
sav:Sale_Cost        REAL
sav:Retail_Cost      REAL
sav:Minimum_Level    REAL
sav:Reorder_Level    REAL
save_loc_id          USHORT,AUTO
save_sto_ali_id      USHORT,AUTO
save_sto_id          USHORT,AUTO
no_temp              STRING('NO')
tag_temp             STRING(1)
levels_temp          STRING('0')
costs_temp           BYTE(0)
all_temp             BYTE(0)
tmp:ReportOrder      STRING(3)
tmp:CurrentStatus    BYTE(0)
tmp:AccountNumber    BYTE(0)
tmp:TurnaroundTime   BYTE(0)
tmp:Workshop         BYTE(0)
tmp:Location         BYTE(0)
tmp:Engineer         BYTE(0)
tmp:Manufacturer     BYTE(0)
tmp:ModelNumber      BYTE(0)
tmp:UnitType         BYTE(0)
tmp:TransitType      BYTE(0)
tmp:ChargeableChargeType BYTE(0)
tmp:WarrantyChargeType BYTE(0)
tmp:ChargeableRepairType BYTE(0)
tmp:WarrantyRepairType BYTE(0)
tmp:ReportType       STRING(3)
tmp:ExchangeStatus   BYTE(0)
tmp:LoanStatus       BYTE(0)
yes_temp             STRING('YES')
tmp:HeadAccountTick  BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:SortReport       BYTE(0)
tmp:StatusOnReport   BYTE(1)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select2
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?GLO:Select1
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?GLO:Select3
tur:Turnaround_Time    LIKE(tur:Turnaround_Time)      !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:3 QUEUE                           !Queue declaration for browse/combo box using ?GLO:Select5
loi:Location           LIKE(loi:Location)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:4 QUEUE                           !Queue declaration for browse/combo box using ?GLO:Select8
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:5 QUEUE                           !Queue declaration for browse/combo box using ?GLO:Select7
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:6 QUEUE                           !Queue declaration for browse/combo box using ?GLO:Select9
uni:Unit_Type          LIKE(uni:Unit_Type)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:7 QUEUE                           !Queue declaration for browse/combo box using ?GLO:Select10
trt:Transit_Type       LIKE(trt:Transit_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:8 QUEUE                           !Queue declaration for browse/combo box using ?GLO:Select11
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:9 QUEUE                           !Queue declaration for browse/combo box using ?GLO:Select15
rtd:Repair_Type        LIKE(rtd:Repair_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:10 QUEUE                          !Queue declaration for browse/combo box using ?GLO:Select12
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:11 QUEUE                          !Queue declaration for browse/combo box using ?GLO:Select16
rtd:Repair_Type        LIKE(rtd:Repair_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:12 QUEUE                          !Queue declaration for browse/combo box using ?GLO:Select23
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:13 QUEUE                          !Queue declaration for browse/combo box using ?GLO:Select24
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:14 QUEUE                          !Queue declaration for browse/combo box using ?GLO:Select30
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB1::View:FileDropCombo VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                     END
FDCB10::View:FileDropCombo VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
FDCB11::View:FileDropCombo VIEW(TURNARND)
                       PROJECT(tur:Turnaround_Time)
                     END
FDCB12::View:FileDropCombo VIEW(LOCINTER)
                       PROJECT(loi:Location)
                     END
FDCB14::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
FDCB15::View:FileDropCombo VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                     END
FDCB16::View:FileDropCombo VIEW(UNITTYPE)
                       PROJECT(uni:Unit_Type)
                     END
FDCB17::View:FileDropCombo VIEW(TRANTYPE)
                       PROJECT(trt:Transit_Type)
                     END
FDCB18::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
FDCB19::View:FileDropCombo VIEW(REPTYDEF)
                       PROJECT(rtd:Repair_Type)
                     END
FDCB20::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
FDCB21::View:FileDropCombo VIEW(REPTYDEF)
                       PROJECT(rtd:Repair_Type)
                     END
FDCB22::View:FileDropCombo VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                     END
FDCB23::View:FileDropCombo VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                     END
FDCB26::View:FileDropCombo VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
mo:SelectedTab  Long  ! makeover template ! LocalTreat = Default ; PT = Window  WT = Window
QuickWindow          WINDOW('Browse The Location File'),AT(,,343,299),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('Pick_Other_Location'),SYSTEM,GRAY,DOUBLE
                       PROMPT('Status Report Wizard'),AT(8,8),USE(?Title),TRN,FONT(,12,,FONT:bold)
                       SHEET,AT(4,4,336,264),USE(?CurrentTab),SPREAD
                         TAB('Report Type'),USE(?Tab3)
                           PROMPT('Summary'),AT(80,88),USE(?Prompt18),TRN,FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Produces a report listing all the jobs that match the selected criteria.'),AT(80,64,220,24),USE(?Prompt12:3),TRN
                           PROMPT('Status Report Type'),AT(80,28),USE(?Prompt7),TRN,FONT(,,,FONT:bold)
                           PROMPT('Detailed'),AT(79,52),USE(?Prompt17),TRN,FONT(,,COLOR:Navy,FONT:underline,CHARSET:ANSI)
                           PROMPT('Produces a 1-page report just showing the total number of jobs that match the se' &|
   'lected criteria.'),AT(80,100,220,24),USE(?Prompt12:2),TRN
                           PROMPT('Status Change'),AT(80,132),USE(?Prompt18:2),TRN,FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Produces a report listing all jobs that match the selected criteria, including P' &|
   'revious Status information'),AT(80,144,220,24),USE(?Prompt12:4),TRN
                           OPTION('Report Type'),AT(80,172,216,36),USE(tmp:ReportType),BOXED,TRN,COLOR(COLOR:Silver)
                             RADIO('Detailed'),AT(88,188),USE(?Type1),TRN,COLOR(COLOR:Silver),VALUE('DET')
                             RADIO('Summary'),AT(156,188),USE(?Type2),TRN,COLOR(COLOR:Silver),VALUE('SUM')
                             RADIO('Status Change'),AT(224,188),USE(?tmp:ReportType:Radio3),TRN,VALUE('STA')
                           END
                           OPTION('Status Type On Report'),AT(80,212,216,36),USE(tmp:StatusOnReport),BOXED
                             RADIO('Job Status'),AT(88,228),USE(?tmp:StatusOnReport:Radio1),VALUE('1')
                             RADIO('Exchange Status'),AT(156,228),USE(?tmp:StatusOnReport:Radio1:2),VALUE('2')
                             RADIO('Loan Status'),AT(228,228),USE(?tmp:StatusOnReport:Radio1:3),VALUE('3')
                           END
                         END
                         TAB('Report Order'),USE(?Tab:2)
                           PROMPT('The Reportwill be sorted in Job Number Order. (This will take much longer than t' &|
   'he Date Order)'),AT(80,80,256,20),USE(?Prompt5:4),TRN
                           PROMPT('The Report will be sorted in Booking/Completed Date Order.'),AT(80,56,256,20),USE(?Prompt5:2),TRN
                           OPTION('Report Order'),AT(128,100,148,32),USE(tmp:SortReport),BOXED
                             RADIO('Job Number'),AT(208,116),USE(?tmp:SortReport:Radio3),VALUE('1')
                             RADIO('Date'),AT(140,116),USE(?tmp:SortReport:Radio4),VALUE('0')
                           END
                           PROMPT('Select the date range to limit the report to. (A small date range with speed up ' &|
   'the report generation when in "Date Order")'),AT(80,156,256,20),USE(?Prompt5:3),TRN
                           OPTION('Date Type'),AT(128,180,148,32),USE(tmp:ReportOrder),BOXED,TRN,COLOR(COLOR:Silver)
                             RADIO('Booking Date'),AT(136,196),USE(?tmp:ReportOrder:Radio1),TRN,COLOR(COLOR:Silver),VALUE('BOO')
                             RADIO('Completed Date'),AT(208,196,72,12),USE(?tmp:ReportOrder:Radio2),TRN,COLOR(COLOR:Silver),VALUE('COM')
                           END
                           GROUP('Date Range'),AT(128,216,144,44),USE(?Group1),BOXED,TRN,COLOR(COLOR:Silver)
                             PROMPT('Start Date'),AT(136,228),USE(?glo:select20:Prompt),TRN
                             ENTRY(@d6b),AT(188,228,64,10),USE(GLO:Select20),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                             BUTTON,AT(256,228,10,10),USE(?PopCalendar),SKIP,ICON('calenda2.ico')
                             PROMPT('End Date'),AT(136,244),USE(?glo:select21:Prompt),TRN
                             ENTRY(@d6b),AT(188,244,64,10),USE(GLO:Select21),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                             BUTTON,AT(256,244,10,10),USE(?PopCalendar:2),SKIP,ICON('calenda2.ico')
                           END
                           PROMPT('Status Report Order'),AT(80,28),USE(?Prompt8),TRN,FONT(,,,FONT:bold)
                           PROMPT('Job Number'),AT(80,68),USE(?Prompt19:4),TRN,FONT(,,COLOR:Navy,FONT:underline,CHARSET:ANSI)
                           PROMPT('Date Order'),AT(80,44),USE(?DateOrder),TRN,FONT(,,COLOR:Navy,FONT:underline,CHARSET:ANSI)
                           PROMPT('Date Range Limit'),AT(80,144),USE(?Prompt19:3),TRN,FONT(,,COLOR:Navy,FONT:underline,CHARSET:ANSI)
                         END
                         TAB('Tab 4'),USE(?Tab4)
                           PROMPT('Job Types Options'),AT(80,28),USE(?Prompt9),TRN,FONT(,,,FONT:bold)
                           PROMPT('Do you wish to include Warranty, Chargeable or both kinds of jobs?'),AT(80,48,240,12),USE(?Prompt6),TRN
                           PROMPT('Do you wish to include Invoiced, Uninvoiced or both types of jobs?'),AT(80,104,240,12),USE(?Prompt6:2),TRN
                           PROMPT('Do you wish to include Completed, Incomplete or both types of jobs?'),AT(80,160,240,12),USE(?Prompt6:3),TRN
                           PROMPT('Do you wish to include Despatched, Not Despatched or both types of jobs?'),AT(80,212,256,12),USE(?Prompt6:4),TRN
                           OPTION('Despatch Types'),AT(80,224,256,36),USE(GLO:Select19),BOXED,TRN,COLOR(COLOR:Silver)
                             RADIO('Despatched'),AT(88,239),USE(?glo:select19:Radio10),TRN,COLOR(COLOR:Silver),VALUE('DES')
                             RADIO('Not Despatched'),AT(184,239),USE(?glo:select19:Radio11),TRN,COLOR(COLOR:Silver),VALUE('NOT')
                             RADIO('Both'),AT(288,239),USE(?glo:select19:Radio12),TRN,COLOR(COLOR:Silver),VALUE('ALL')
                           END
                           OPTION('Job Types'),AT(80,60,256,36),USE(GLO:Select22),BOXED,TRN,COLOR(COLOR:Silver)
                             RADIO('Warranty Only'),AT(88,68),USE(?glo:select22:Radio1),TRN,COLOR(COLOR:Silver),VALUE('WAR')
                             RADIO('Warranty (Inc Split)'),AT(184,68),USE(?GLO:Select22:Radio11),VALUE('WAS')
                             RADIO('Chargeable Only'),AT(88,80),USE(?glo:select22:Radio2),TRN,COLOR(COLOR:Silver),VALUE('CHA')
                             RADIO('Split Only'),AT(287,68),USE(?GLO:Select22:Radio10),TRN,VALUE('SPL')
                             RADIO('Both'),AT(288,80),USE(?glo:select22:Radio3),TRN,COLOR(COLOR:Silver),VALUE('ALL')
                             RADIO('Chargeable (Inc Split)'),AT(184,80),USE(?GLO:Select22:Radio12),VALUE('CHS')
                           END
                           OPTION('Invoice Types'),AT(80,116,256,36),USE(GLO:Select17),BOXED,TRN,COLOR(COLOR:Silver)
                             RADIO('Invoiced'),AT(88,132),USE(?glo:select17:Radio1),TRN,COLOR(COLOR:Silver),VALUE('INV')
                             RADIO('UnInvoiced'),AT(184,132),USE(?glo:select17:Radio2),TRN,COLOR(COLOR:Silver),VALUE('NOT')
                             RADIO('Both'),AT(288,132),USE(?glo:select17:Radio3),TRN,COLOR(COLOR:Silver),VALUE('ALL')
                           END
                           OPTION('Completed Types'),AT(80,172,256,36),USE(GLO:Select18),BOXED,TRN,COLOR(COLOR:Silver)
                             RADIO('Completed'),AT(88,188),USE(?glo:select18:Radio1),TRN,COLOR(COLOR:Silver),VALUE('COM')
                             RADIO('Incomplete'),AT(184,188),USE(?glo:select18:Radio2),TRN,COLOR(COLOR:Silver),VALUE('NOT')
                             RADIO('Both'),AT(288,188),USE(?glo:select18:Radio3),TRN,COLOR(COLOR:Silver),VALUE('ALL')
                           END
                         END
                         TAB('Tab 5'),USE(?Tab5)
                           PROMPT('Report Critera'),AT(80,28),USE(?Prompt15),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Select the fields you wish to filter the report by'),AT(80,44),USE(?Prompt15:2),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                           CHECK('Job Status'),AT(88,56),USE(tmp:CurrentStatus),VALUE('1','0')
                           COMBO(@s30),AT(184,56,124,10),USE(GLO:Select2),IMM,HIDE,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           CHECK('Exchange Status'),AT(88,72),USE(tmp:ExchangeStatus),VALUE('1','0')
                           COMBO(@s40),AT(184,72,124,10),USE(GLO:Select23),IMM,HIDE,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:12)
                           CHECK('Loan Status'),AT(88,88),USE(tmp:LoanStatus),VALUE('1','0')
                           COMBO(@s40),AT(184,88,124,10),USE(GLO:Select24),IMM,HIDE,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:13)
                           CHECK('All Accounts '),AT(88,120),USE(tmp:AccountNumber),VALUE('1','0')
                           COMBO(@s30),AT(184,120,124,10),USE(GLO:Select1),IMM,HIDE,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('65L(2)|M@s15@120L(2)|M@s30@'),DROP(10,200),FROM(Queue:FileDropCombo:1)
                           CHECK('Main Account Only'),AT(88,104),USE(tmp:HeadAccountTick),RIGHT,VALUE('1','0')
                           COMBO(@s40),AT(184,104,124,10),USE(GLO:Select30),IMM,HIDE,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('64L(2)|M@s15@120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:14)
                           CHECK('Turnaround Time'),AT(88,136),USE(tmp:TurnaroundTime),VALUE('1','0')
                           COMBO(@s40),AT(184,136,124,10),USE(GLO:Select3),IMM,HIDE,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:2)
                           OPTION,AT(176,144,133,18),USE(GLO:Select4),TRN,HIDE,COLOR(COLOR:Silver)
                             RADIO('Yes'),AT(184,148),USE(?glo:select4:Radio1),TRN,COLOR(COLOR:Silver),VALUE('YES')
                             RADIO('No'),AT(220,148),USE(?glo:select4:Radio2),TRN,COLOR(COLOR:Silver),VALUE('NO')
                           END
                           CHECK('Workshop'),AT(88,152),USE(tmp:Workshop),VALUE('1','0')
                           CHECK('Location'),AT(88,168),USE(tmp:Location),VALUE('1','0')
                           COMBO(@s40),AT(184,168,124,10),USE(GLO:Select5),IMM,HIDE,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:3)
                           CHECK('Engineer'),AT(88,184),USE(tmp:Engineer),VALUE('1','0')
                           ENTRY(@s40),AT(184,184,32,10),USE(GLO:Select6),HIDE,FONT(,,,FONT:bold)
                           BUTTON,AT(220,184,10,10),USE(?LookupEngineer),HIDE,ICON('list3.ico')
                           CHECK('Manufacturer'),AT(88,200),USE(tmp:Manufacturer),VALUE('1','0')
                           COMBO(@s40),AT(184,200,124,10),USE(GLO:Select8),IMM,HIDE,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:4)
                           CHECK('Model Number'),AT(88,216),USE(tmp:ModelNumber),VALUE('1','0')
                           COMBO(@s40),AT(184,216,124,10),USE(GLO:Select7),IMM,HIDE,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:5)
                           CHECK('Unit Type'),AT(88,232),USE(tmp:UnitType),VALUE('1','0')
                           COMBO(@s40),AT(184,232,124,10),USE(GLO:Select9),IMM,HIDE,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:6)
                           CHECK('Transit Type'),AT(88,248),USE(tmp:TransitType),VALUE('1','0')
                           COMBO(@s40),AT(184,248,124,10),USE(GLO:Select10),IMM,HIDE,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:7)
                         END
                         TAB('Tab 6'),USE(?Tab6)
                           PROMPT('Chargeable Options'),AT(80,28),USE(?Prompt22),TRN,FONT(,,,FONT:bold)
                           PROMPT('The following criteria is specific to Chargeable Jobs Only.'),AT(80,48),USE(?Prompt23),TRN
                           CHECK('Charge Type'),AT(88,72),USE(tmp:ChargeableChargeType),VALUE('1','0')
                           COMBO(@s40),AT(184,72,124,10),USE(GLO:Select11),IMM,HIDE,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:8)
                           COMBO(@s40),AT(184,92,124,10),USE(GLO:Select15),IMM,HIDE,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:9)
                           CHECK('Repair Type'),AT(88,92),USE(tmp:ChargeableRepairType),VALUE('1','0')
                         END
                         TAB('Tab 7'),USE(?Tab7)
                           PROMPT('Warranty Options'),AT(80,28),USE(?Prompt22:2),TRN,FONT(,,,FONT:bold)
                           PROMPT('The following criteria is specific to Warranty Jobs Only.'),AT(80,48),USE(?Prompt23:2),TRN
                           CHECK('Charge Type'),AT(88,72),USE(tmp:WarrantyChargeType),VALUE('1','0')
                           COMBO(@s40),AT(184,72,124,10),USE(GLO:Select12),IMM,HIDE,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:10)
                           CHECK('Repair Type'),AT(88,92),USE(tmp:WarrantyRepairType),VALUE('1','0')
                           COMBO(@s40),AT(184,92,124,10),USE(GLO:Select16),IMM,HIDE,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:11)
                         END
                       END
                       BUTTON('Close'),AT(280,276,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                       BUTTON('&Finish'),AT(224,276,56,16),USE(?Ok),LEFT,ICON('thumbs.gif')
                       BUTTON('&Next'),AT(64,276,56,16),USE(?VSNextButton),LEFT,ICON(ICON:VCRfastforward)
                       PANEL,AT(4,272,336,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Back'),AT(8,276,56,16),USE(?VSBackButton),LEFT,ICON(ICON:VCRrewind)
                       IMAGE('wiz.gif'),AT(8,96,64,68),USE(?Image1)
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Arial',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Arial',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Arial',8,,)
     end
Wizard2         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
Resize                 PROCEDURE(),BYTE,PROC,DERIVED
Resize                 PROCEDURE(SIGNED Control),BYTE,PROC,DERIVED
                     END

FDCB1                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB10               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB11               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

FDCB12               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:3         !Reference to browse queue type
                     END

FDCB14               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:4         !Reference to browse queue type
                     END

FDCB15               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:5         !Reference to browse queue type
                     END

FDCB16               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:6         !Reference to browse queue type
                     END

FDCB17               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:7         !Reference to browse queue type
                     END

FDCB18               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:8         !Reference to browse queue type
                     END

FDCB19               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:9         !Reference to browse queue type
                     END

FDCB20               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:10        !Reference to browse queue type
                     END

FDCB21               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:11        !Reference to browse queue type
                     END

FDCB22               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:12        !Reference to browse queue type
                     END

FDCB23               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:13        !Reference to browse queue type
                     END

FDCB26               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:14        !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:GLO:Select6                Like(GLO:Select6)
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Status_Report_Criteria_Old',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Status_Report_Criteria_Old',1)
    SolaceViewVars('save_stm_id',save_stm_id,'Status_Report_Criteria_Old',1)
    SolaceViewVars('save_stm_ali_id',save_stm_ali_id,'Status_Report_Criteria_Old',1)
    SolaceViewVars('sav:Purchase_Cost',sav:Purchase_Cost,'Status_Report_Criteria_Old',1)
    SolaceViewVars('sav:Sale_Cost',sav:Sale_Cost,'Status_Report_Criteria_Old',1)
    SolaceViewVars('sav:Retail_Cost',sav:Retail_Cost,'Status_Report_Criteria_Old',1)
    SolaceViewVars('sav:Minimum_Level',sav:Minimum_Level,'Status_Report_Criteria_Old',1)
    SolaceViewVars('sav:Reorder_Level',sav:Reorder_Level,'Status_Report_Criteria_Old',1)
    SolaceViewVars('save_loc_id',save_loc_id,'Status_Report_Criteria_Old',1)
    SolaceViewVars('save_sto_ali_id',save_sto_ali_id,'Status_Report_Criteria_Old',1)
    SolaceViewVars('save_sto_id',save_sto_id,'Status_Report_Criteria_Old',1)
    SolaceViewVars('no_temp',no_temp,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tag_temp',tag_temp,'Status_Report_Criteria_Old',1)
    SolaceViewVars('levels_temp',levels_temp,'Status_Report_Criteria_Old',1)
    SolaceViewVars('costs_temp',costs_temp,'Status_Report_Criteria_Old',1)
    SolaceViewVars('all_temp',all_temp,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:ReportOrder',tmp:ReportOrder,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:CurrentStatus',tmp:CurrentStatus,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:AccountNumber',tmp:AccountNumber,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:TurnaroundTime',tmp:TurnaroundTime,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:Workshop',tmp:Workshop,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:Location',tmp:Location,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:Engineer',tmp:Engineer,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:Manufacturer',tmp:Manufacturer,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:ModelNumber',tmp:ModelNumber,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:UnitType',tmp:UnitType,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:TransitType',tmp:TransitType,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:ChargeableChargeType',tmp:ChargeableChargeType,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:WarrantyChargeType',tmp:WarrantyChargeType,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:ChargeableRepairType',tmp:ChargeableRepairType,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:WarrantyRepairType',tmp:WarrantyRepairType,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:ReportType',tmp:ReportType,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:ExchangeStatus',tmp:ExchangeStatus,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:LoanStatus',tmp:LoanStatus,'Status_Report_Criteria_Old',1)
    SolaceViewVars('yes_temp',yes_temp,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:HeadAccountTick',tmp:HeadAccountTick,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:SortReport',tmp:SortReport,'Status_Report_Criteria_Old',1)
    SolaceViewVars('tmp:StatusOnReport',tmp:StatusOnReport,'Status_Report_Criteria_Old',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Title;  SolaceCtrlName = '?Title';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt18;  SolaceCtrlName = '?Prompt18';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt12:3;  SolaceCtrlName = '?Prompt12:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt17;  SolaceCtrlName = '?Prompt17';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt12:2;  SolaceCtrlName = '?Prompt12:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt18:2;  SolaceCtrlName = '?Prompt18:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt12:4;  SolaceCtrlName = '?Prompt12:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ReportType;  SolaceCtrlName = '?tmp:ReportType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Type1;  SolaceCtrlName = '?Type1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Type2;  SolaceCtrlName = '?Type2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ReportType:Radio3;  SolaceCtrlName = '?tmp:ReportType:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StatusOnReport;  SolaceCtrlName = '?tmp:StatusOnReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StatusOnReport:Radio1;  SolaceCtrlName = '?tmp:StatusOnReport:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StatusOnReport:Radio1:2;  SolaceCtrlName = '?tmp:StatusOnReport:Radio1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StatusOnReport:Radio1:3;  SolaceCtrlName = '?tmp:StatusOnReport:Radio1:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:4;  SolaceCtrlName = '?Prompt5:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:2;  SolaceCtrlName = '?Prompt5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SortReport;  SolaceCtrlName = '?tmp:SortReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SortReport:Radio3;  SolaceCtrlName = '?tmp:SortReport:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SortReport:Radio4;  SolaceCtrlName = '?tmp:SortReport:Radio4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:3;  SolaceCtrlName = '?Prompt5:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ReportOrder;  SolaceCtrlName = '?tmp:ReportOrder';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ReportOrder:Radio1;  SolaceCtrlName = '?tmp:ReportOrder:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ReportOrder:Radio2;  SolaceCtrlName = '?tmp:ReportOrder:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group1;  SolaceCtrlName = '?Group1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select20:Prompt;  SolaceCtrlName = '?glo:select20:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select20;  SolaceCtrlName = '?GLO:Select20';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select21:Prompt;  SolaceCtrlName = '?glo:select21:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select21;  SolaceCtrlName = '?GLO:Select21';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt19:4;  SolaceCtrlName = '?Prompt19:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DateOrder;  SolaceCtrlName = '?DateOrder';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt19:3;  SolaceCtrlName = '?Prompt19:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt9;  SolaceCtrlName = '?Prompt9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6:2;  SolaceCtrlName = '?Prompt6:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6:3;  SolaceCtrlName = '?Prompt6:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6:4;  SolaceCtrlName = '?Prompt6:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select19;  SolaceCtrlName = '?GLO:Select19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select19:Radio10;  SolaceCtrlName = '?glo:select19:Radio10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select19:Radio11;  SolaceCtrlName = '?glo:select19:Radio11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select19:Radio12;  SolaceCtrlName = '?glo:select19:Radio12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select22;  SolaceCtrlName = '?GLO:Select22';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select22:Radio1;  SolaceCtrlName = '?glo:select22:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select22:Radio11;  SolaceCtrlName = '?GLO:Select22:Radio11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select22:Radio2;  SolaceCtrlName = '?glo:select22:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select22:Radio10;  SolaceCtrlName = '?GLO:Select22:Radio10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select22:Radio3;  SolaceCtrlName = '?glo:select22:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select22:Radio12;  SolaceCtrlName = '?GLO:Select22:Radio12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select17;  SolaceCtrlName = '?GLO:Select17';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select17:Radio1;  SolaceCtrlName = '?glo:select17:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select17:Radio2;  SolaceCtrlName = '?glo:select17:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select17:Radio3;  SolaceCtrlName = '?glo:select17:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select18;  SolaceCtrlName = '?GLO:Select18';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select18:Radio1;  SolaceCtrlName = '?glo:select18:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select18:Radio2;  SolaceCtrlName = '?glo:select18:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select18:Radio3;  SolaceCtrlName = '?glo:select18:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt15;  SolaceCtrlName = '?Prompt15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt15:2;  SolaceCtrlName = '?Prompt15:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CurrentStatus;  SolaceCtrlName = '?tmp:CurrentStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select2;  SolaceCtrlName = '?GLO:Select2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExchangeStatus;  SolaceCtrlName = '?tmp:ExchangeStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select23;  SolaceCtrlName = '?GLO:Select23';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LoanStatus;  SolaceCtrlName = '?tmp:LoanStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select24;  SolaceCtrlName = '?GLO:Select24';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:AccountNumber;  SolaceCtrlName = '?tmp:AccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select1;  SolaceCtrlName = '?GLO:Select1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:HeadAccountTick;  SolaceCtrlName = '?tmp:HeadAccountTick';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select30;  SolaceCtrlName = '?GLO:Select30';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:TurnaroundTime;  SolaceCtrlName = '?tmp:TurnaroundTime';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select3;  SolaceCtrlName = '?GLO:Select3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select4;  SolaceCtrlName = '?GLO:Select4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select4:Radio1;  SolaceCtrlName = '?glo:select4:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select4:Radio2;  SolaceCtrlName = '?glo:select4:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Workshop;  SolaceCtrlName = '?tmp:Workshop';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Location;  SolaceCtrlName = '?tmp:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select5;  SolaceCtrlName = '?GLO:Select5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Engineer;  SolaceCtrlName = '?tmp:Engineer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select6;  SolaceCtrlName = '?GLO:Select6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupEngineer;  SolaceCtrlName = '?LookupEngineer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Manufacturer;  SolaceCtrlName = '?tmp:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select8;  SolaceCtrlName = '?GLO:Select8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ModelNumber;  SolaceCtrlName = '?tmp:ModelNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select7;  SolaceCtrlName = '?GLO:Select7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:UnitType;  SolaceCtrlName = '?tmp:UnitType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select9;  SolaceCtrlName = '?GLO:Select9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:TransitType;  SolaceCtrlName = '?tmp:TransitType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select10;  SolaceCtrlName = '?GLO:Select10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab6;  SolaceCtrlName = '?Tab6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt22;  SolaceCtrlName = '?Prompt22';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt23;  SolaceCtrlName = '?Prompt23';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ChargeableChargeType;  SolaceCtrlName = '?tmp:ChargeableChargeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select11;  SolaceCtrlName = '?GLO:Select11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select15;  SolaceCtrlName = '?GLO:Select15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ChargeableRepairType;  SolaceCtrlName = '?tmp:ChargeableRepairType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab7;  SolaceCtrlName = '?Tab7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt22:2;  SolaceCtrlName = '?Prompt22:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt23:2;  SolaceCtrlName = '?Prompt23:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:WarrantyChargeType;  SolaceCtrlName = '?tmp:WarrantyChargeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select12;  SolaceCtrlName = '?GLO:Select12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:WarrantyRepairType;  SolaceCtrlName = '?tmp:WarrantyRepairType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select16;  SolaceCtrlName = '?GLO:Select16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ok;  SolaceCtrlName = '?Ok';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VSNextButton;  SolaceCtrlName = '?VSNextButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VSBackButton;  SolaceCtrlName = '?VSBackButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1;  SolaceCtrlName = '?Image1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Status_Report_Criteria_Old')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Status_Report_Criteria_Old')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Title
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:REPTYDEF.Open
  Relate:STATUS.Open
  Relate:STOCK_ALIAS.Open
  Relate:STOMODEL_ALIAS.Open
  Relate:TRANTYPE.Open
  Relate:TURNARND.Open
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:STOMODEL.UseFile
  Access:LOCSHELF.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  glo:select22 = 'ALL'
  glo:select17 = 'ALL'
  glo:select18 = 'ALL'
  glo:select20 = Deformat('1/1/1990',@d6)
  glo:select21 = Today()
  glo:select19 = 'ALL'
  
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If def:HideLocation
      ?tmp:Location{prop:Hide} = 1
      ?glo:Select5{prop:Hide} = 1
  End !def:HideLocation
  OPEN(QuickWindow)
  SELF.Opened=True
    Loop DBHControl# = Firstfield() To LastField()
        If DBHControl#{prop:type} = Create:OPTION Or |
            DBHControl#{prop:type} = Create:RADIO Or |
            DBhControl#{prop:type} = Create:GROUP
            DBHControl#{prop:trn} = 1      
        End!DBhControl#{prop:type} = 'GROUP'
    End!Loop DBHControl# = Firstfield() To LastField()
    Wizard2.Init(?CurrentTab, |                       ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Ok, |                           ! OK button
                     ?Close, |                        ! Cancel button
                     1, |                             ! Skip hidden tabs
                     1, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  ?glo:select20{Prop:Alrt,255} = MouseLeft2
  ?glo:select21{Prop:Alrt,255} = MouseLeft2
  IF ?GLO:Select6{Prop:Tip} AND ~?LookupEngineer{Prop:Tip}
     ?LookupEngineer{Prop:Tip} = 'Select ' & ?GLO:Select6{Prop:Tip}
  END
  IF ?GLO:Select6{Prop:Msg} AND ~?LookupEngineer{Prop:Msg}
     ?LookupEngineer{Prop:Msg} = 'Select ' & ?GLO:Select6{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ThisMakeover.SetWindow(Win:Window)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab,?CurrentTab)
      
  
  IF ?tmp:CurrentStatus{Prop:Checked} = True
    UNHIDE(?glo:select2)
  END
  IF ?tmp:CurrentStatus{Prop:Checked} = False
    HIDE(?glo:select2)
  END
  IF ?tmp:ExchangeStatus{Prop:Checked} = True
    UNHIDE(?glo:select23)
  END
  IF ?tmp:ExchangeStatus{Prop:Checked} = False
    HIDE(?glo:select23)
  END
  IF ?tmp:LoanStatus{Prop:Checked} = True
    UNHIDE(?glo:select24)
  END
  IF ?tmp:LoanStatus{Prop:Checked} = False
    HIDE(?glo:select24)
  END
  IF ?tmp:AccountNumber{Prop:Checked} = True
    UNHIDE(?glo:select1)
  END
  IF ?tmp:AccountNumber{Prop:Checked} = False
    HIDE(?glo:select1)
  END
  IF ?tmp:HeadAccountTick{Prop:Checked} = True
    UNHIDE(?glo:select30)
  END
  IF ?tmp:HeadAccountTick{Prop:Checked} = False
    HIDE(?glo:select30)
  END
  IF ?tmp:TurnaroundTime{Prop:Checked} = True
    UNHIDE(?glo:select3)
  END
  IF ?tmp:TurnaroundTime{Prop:Checked} = False
    HIDE(?glo:select3)
  END
  IF ?tmp:Workshop{Prop:Checked} = True
    UNHIDE(?glo:select4)
  END
  IF ?tmp:Workshop{Prop:Checked} = False
    HIDE(?glo:select4)
  END
  IF ?tmp:Location{Prop:Checked} = True
    UNHIDE(?glo:select5)
  END
  IF ?tmp:Location{Prop:Checked} = False
    HIDE(?glo:select5)
  END
  IF ?tmp:Engineer{Prop:Checked} = True
    UNHIDE(?glo:select6)
    UNHIDE(?LookupEngineer)
  END
  IF ?tmp:Engineer{Prop:Checked} = False
    HIDE(?glo:select6)
    HIDE(?LookupEngineer)
  END
  IF ?tmp:Manufacturer{Prop:Checked} = True
    UNHIDE(?glo:select8)
  END
  IF ?tmp:Manufacturer{Prop:Checked} = False
    HIDE(?glo:select8)
  END
  IF ?tmp:ModelNumber{Prop:Checked} = True
    UNHIDE(?glo:select7)
  END
  IF ?tmp:ModelNumber{Prop:Checked} = False
    HIDE(?glo:select7)
  END
  IF ?tmp:UnitType{Prop:Checked} = True
    UNHIDE(?glo:select9)
  END
  IF ?tmp:UnitType{Prop:Checked} = False
    HIDE(?glo:select9)
  END
  IF ?tmp:TransitType{Prop:Checked} = True
    UNHIDE(?glo:select10)
  END
  IF ?tmp:TransitType{Prop:Checked} = False
    HIDE(?glo:select10)
  END
  IF ?tmp:ChargeableChargeType{Prop:Checked} = True
    UNHIDE(?glo:select11)
  END
  IF ?tmp:ChargeableChargeType{Prop:Checked} = False
    HIDE(?glo:select11)
  END
  IF ?tmp:ChargeableRepairType{Prop:Checked} = True
    UNHIDE(?glo:select15)
  END
  IF ?tmp:ChargeableRepairType{Prop:Checked} = False
    HIDE(?glo:select15)
  END
  IF ?tmp:WarrantyChargeType{Prop:Checked} = True
    UNHIDE(?glo:select12)
  END
  IF ?tmp:WarrantyChargeType{Prop:Checked} = False
    HIDE(?glo:select12)
  END
  IF ?tmp:WarrantyRepairType{Prop:Checked} = True
    UNHIDE(?glo:select16)
  END
  IF ?tmp:WarrantyRepairType{Prop:Checked} = False
    HIDE(?glo:select16)
  END
  FDCB1.Init(GLO:Select2,?GLO:Select2,Queue:FileDropCombo.ViewPosition,FDCB1::View:FileDropCombo,Queue:FileDropCombo,Relate:STATUS,ThisWindow,GlobalErrors,0,1,0)
  FDCB1.Q &= Queue:FileDropCombo
  FDCB1.AddSortOrder(sts:JobKey)
  FDCB1.SetFilter('Upper(sts:job) = ''YES''')
  FDCB1.AddField(sts:Status,FDCB1.Q.sts:Status)
  FDCB1.AddField(sts:Ref_Number,FDCB1.Q.sts:Ref_Number)
  ThisWindow.AddItem(FDCB1.WindowComponent)
  FDCB1.DefaultFill = 0
  FDCB10.Init(GLO:Select1,?GLO:Select1,Queue:FileDropCombo:1.ViewPosition,FDCB10::View:FileDropCombo,Queue:FileDropCombo:1,Relate:SUBTRACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB10.Q &= Queue:FileDropCombo:1
  FDCB10.AddSortOrder(sub:Account_Number_Key)
  FDCB10.AddField(sub:Account_Number,FDCB10.Q.sub:Account_Number)
  FDCB10.AddField(sub:Company_Name,FDCB10.Q.sub:Company_Name)
  FDCB10.AddField(sub:RecordNumber,FDCB10.Q.sub:RecordNumber)
  ThisWindow.AddItem(FDCB10.WindowComponent)
  FDCB10.DefaultFill = 0
  FDCB11.Init(GLO:Select3,?GLO:Select3,Queue:FileDropCombo:2.ViewPosition,FDCB11::View:FileDropCombo,Queue:FileDropCombo:2,Relate:TURNARND,ThisWindow,GlobalErrors,0,1,0)
  FDCB11.Q &= Queue:FileDropCombo:2
  FDCB11.AddSortOrder(tur:Turnaround_Time_Key)
  FDCB11.AddField(tur:Turnaround_Time,FDCB11.Q.tur:Turnaround_Time)
  ThisWindow.AddItem(FDCB11.WindowComponent)
  FDCB11.DefaultFill = 0
  FDCB12.Init(GLO:Select5,?GLO:Select5,Queue:FileDropCombo:3.ViewPosition,FDCB12::View:FileDropCombo,Queue:FileDropCombo:3,Relate:LOCINTER,ThisWindow,GlobalErrors,0,1,0)
  FDCB12.Q &= Queue:FileDropCombo:3
  FDCB12.AddSortOrder(loi:Location_Key)
  FDCB12.AddField(loi:Location,FDCB12.Q.loi:Location)
  ThisWindow.AddItem(FDCB12.WindowComponent)
  FDCB12.DefaultFill = 0
  FDCB14.Init(GLO:Select8,?GLO:Select8,Queue:FileDropCombo:4.ViewPosition,FDCB14::View:FileDropCombo,Queue:FileDropCombo:4,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB14.Q &= Queue:FileDropCombo:4
  FDCB14.AddSortOrder(man:Manufacturer_Key)
  FDCB14.AddField(man:Manufacturer,FDCB14.Q.man:Manufacturer)
  FDCB14.AddField(man:RecordNumber,FDCB14.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB14.WindowComponent)
  FDCB14.DefaultFill = 0
  FDCB15.Init(GLO:Select7,?GLO:Select7,Queue:FileDropCombo:5.ViewPosition,FDCB15::View:FileDropCombo,Queue:FileDropCombo:5,Relate:MODELNUM,ThisWindow,GlobalErrors,0,1,0)
  FDCB15.Q &= Queue:FileDropCombo:5
  FDCB15.AddSortOrder(mod:Manufacturer_Key)
  FDCB15.AddRange(mod:Manufacturer,GLO:Select8)
  FDCB15.AddField(mod:Model_Number,FDCB15.Q.mod:Model_Number)
  ThisWindow.AddItem(FDCB15.WindowComponent)
  FDCB15.DefaultFill = 0
  FDCB16.Init(GLO:Select9,?GLO:Select9,Queue:FileDropCombo:6.ViewPosition,FDCB16::View:FileDropCombo,Queue:FileDropCombo:6,Relate:UNITTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB16.Q &= Queue:FileDropCombo:6
  FDCB16.AddSortOrder(uni:Unit_Type_Key)
  FDCB16.AddField(uni:Unit_Type,FDCB16.Q.uni:Unit_Type)
  ThisWindow.AddItem(FDCB16.WindowComponent)
  FDCB16.DefaultFill = 0
  FDCB17.Init(GLO:Select10,?GLO:Select10,Queue:FileDropCombo:7.ViewPosition,FDCB17::View:FileDropCombo,Queue:FileDropCombo:7,Relate:TRANTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB17.Q &= Queue:FileDropCombo:7
  FDCB17.AddSortOrder(trt:Transit_Type_Key)
  FDCB17.AddField(trt:Transit_Type,FDCB17.Q.trt:Transit_Type)
  ThisWindow.AddItem(FDCB17.WindowComponent)
  FDCB17.DefaultFill = 0
  FDCB18.Init(GLO:Select11,?GLO:Select11,Queue:FileDropCombo:8.ViewPosition,FDCB18::View:FileDropCombo,Queue:FileDropCombo:8,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB18.Q &= Queue:FileDropCombo:8
  FDCB18.AddSortOrder(cha:Warranty_Key)
  FDCB18.AddRange(cha:Warranty,no_temp)
  FDCB18.AddField(cha:Charge_Type,FDCB18.Q.cha:Charge_Type)
  ThisWindow.AddItem(FDCB18.WindowComponent)
  FDCB18.DefaultFill = 0
  FDCB19.Init(GLO:Select15,?GLO:Select15,Queue:FileDropCombo:9.ViewPosition,FDCB19::View:FileDropCombo,Queue:FileDropCombo:9,Relate:REPTYDEF,ThisWindow,GlobalErrors,0,1,0)
  FDCB19.Q &= Queue:FileDropCombo:9
  FDCB19.AddSortOrder(rtd:Chargeable_Key)
  FDCB19.SetFilter('Upper(rtd:chargeable) = ''YES''')
  FDCB19.AddField(rtd:Repair_Type,FDCB19.Q.rtd:Repair_Type)
  ThisWindow.AddItem(FDCB19.WindowComponent)
  FDCB19.DefaultFill = 0
  FDCB20.Init(GLO:Select12,?GLO:Select12,Queue:FileDropCombo:10.ViewPosition,FDCB20::View:FileDropCombo,Queue:FileDropCombo:10,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB20.Q &= Queue:FileDropCombo:10
  FDCB20.AddSortOrder(cha:Warranty_Key)
  FDCB20.AddRange(cha:Warranty,yes_temp)
  FDCB20.AddField(cha:Charge_Type,FDCB20.Q.cha:Charge_Type)
  ThisWindow.AddItem(FDCB20.WindowComponent)
  FDCB20.DefaultFill = 0
  FDCB21.Init(GLO:Select16,?GLO:Select16,Queue:FileDropCombo:11.ViewPosition,FDCB21::View:FileDropCombo,Queue:FileDropCombo:11,Relate:REPTYDEF,ThisWindow,GlobalErrors,0,1,0)
  FDCB21.Q &= Queue:FileDropCombo:11
  FDCB21.AddSortOrder(rtd:Warranty_Key)
  FDCB21.SetFilter('Upper(rtd:Warranty) = ''YES''')
  FDCB21.AddField(rtd:Repair_Type,FDCB21.Q.rtd:Repair_Type)
  ThisWindow.AddItem(FDCB21.WindowComponent)
  FDCB21.DefaultFill = 0
  FDCB22.Init(GLO:Select23,?GLO:Select23,Queue:FileDropCombo:12.ViewPosition,FDCB22::View:FileDropCombo,Queue:FileDropCombo:12,Relate:STATUS,ThisWindow,GlobalErrors,0,1,0)
  FDCB22.Q &= Queue:FileDropCombo:12
  FDCB22.AddSortOrder(sts:ExchangeKey)
  FDCB22.SetFilter('Upper(sts:exchange) = ''YES''')
  FDCB22.AddField(sts:Status,FDCB22.Q.sts:Status)
  FDCB22.AddField(sts:Ref_Number,FDCB22.Q.sts:Ref_Number)
  ThisWindow.AddItem(FDCB22.WindowComponent)
  FDCB22.DefaultFill = 0
  FDCB23.Init(GLO:Select24,?GLO:Select24,Queue:FileDropCombo:13.ViewPosition,FDCB23::View:FileDropCombo,Queue:FileDropCombo:13,Relate:STATUS,ThisWindow,GlobalErrors,0,1,0)
  FDCB23.Q &= Queue:FileDropCombo:13
  FDCB23.AddSortOrder(sts:LoanKey)
  FDCB23.SetFilter('Upper(sts:loan) = ''YES''')
  FDCB23.AddField(sts:Status,FDCB23.Q.sts:Status)
  FDCB23.AddField(sts:Ref_Number,FDCB23.Q.sts:Ref_Number)
  ThisWindow.AddItem(FDCB23.WindowComponent)
  FDCB23.DefaultFill = 0
  FDCB26.Init(GLO:Select30,?GLO:Select30,Queue:FileDropCombo:14.ViewPosition,FDCB26::View:FileDropCombo,Queue:FileDropCombo:14,Relate:TRADEACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB26.Q &= Queue:FileDropCombo:14
  FDCB26.AddSortOrder(tra:Account_Number_Key)
  FDCB26.AddField(tra:Account_Number,FDCB26.Q.tra:Account_Number)
  FDCB26.AddField(tra:Company_Name,FDCB26.Q.tra:Company_Name)
  FDCB26.AddField(tra:RecordNumber,FDCB26.Q.tra:RecordNumber)
  ThisWindow.AddItem(FDCB26.WindowComponent)
  FDCB26.DefaultFill = 0
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  Clear(glo:G_Select1)
  Clear(glo:G_Select1)
  Clear(glo:G_Select1)
  Clear(glo:G_Select1)
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:REPTYDEF.Close
    Relate:STATUS.Close
    Relate:STOCK_ALIAS.Close
    Relate:STOMODEL_ALIAS.Close
    Relate:TRANTYPE.Close
    Relate:TURNARND.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Status_Report_Criteria_Old',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Browse_Users_Job_Assignment
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard2.Validate()
        DISABLE(Wizard2.NextControl())
     ELSE
        ENABLE(Wizard2.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?tmp:ModelNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ModelNumber, Accepted)
      If tmp:modelnumber = 1
          If glo:select8 = ''
              Case MessageEx('You must select a Manufacturer first.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              tmp:modelNumber = 0
          End!If glo:select8 = ''
      End!It tmp:modelnumber = 1
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ModelNumber, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select20 = TINCALENDARStyle1(GLO:Select20)
          Display(?glo:select20)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select21 = TINCALENDARStyle1(GLO:Select21)
          Display(?glo:select21)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tmp:CurrentStatus
      IF ?tmp:CurrentStatus{Prop:Checked} = True
        UNHIDE(?glo:select2)
      END
      IF ?tmp:CurrentStatus{Prop:Checked} = False
        HIDE(?glo:select2)
      END
      ThisWindow.Reset
    OF ?tmp:ExchangeStatus
      IF ?tmp:ExchangeStatus{Prop:Checked} = True
        UNHIDE(?glo:select23)
      END
      IF ?tmp:ExchangeStatus{Prop:Checked} = False
        HIDE(?glo:select23)
      END
      ThisWindow.Reset
    OF ?tmp:LoanStatus
      IF ?tmp:LoanStatus{Prop:Checked} = True
        UNHIDE(?glo:select24)
      END
      IF ?tmp:LoanStatus{Prop:Checked} = False
        HIDE(?glo:select24)
      END
      ThisWindow.Reset
    OF ?tmp:AccountNumber
      IF ?tmp:AccountNumber{Prop:Checked} = True
        UNHIDE(?glo:select1)
      END
      IF ?tmp:AccountNumber{Prop:Checked} = False
        HIDE(?glo:select1)
      END
      ThisWindow.Reset
    OF ?tmp:HeadAccountTick
      IF ?tmp:HeadAccountTick{Prop:Checked} = True
        UNHIDE(?glo:select30)
      END
      IF ?tmp:HeadAccountTick{Prop:Checked} = False
        HIDE(?glo:select30)
      END
      ThisWindow.Reset
    OF ?tmp:TurnaroundTime
      IF ?tmp:TurnaroundTime{Prop:Checked} = True
        UNHIDE(?glo:select3)
      END
      IF ?tmp:TurnaroundTime{Prop:Checked} = False
        HIDE(?glo:select3)
      END
      ThisWindow.Reset
    OF ?tmp:Workshop
      IF ?tmp:Workshop{Prop:Checked} = True
        UNHIDE(?glo:select4)
      END
      IF ?tmp:Workshop{Prop:Checked} = False
        HIDE(?glo:select4)
      END
      ThisWindow.Reset
    OF ?tmp:Location
      IF ?tmp:Location{Prop:Checked} = True
        UNHIDE(?glo:select5)
      END
      IF ?tmp:Location{Prop:Checked} = False
        HIDE(?glo:select5)
      END
      ThisWindow.Reset
    OF ?tmp:Engineer
      IF ?tmp:Engineer{Prop:Checked} = True
        UNHIDE(?glo:select6)
        UNHIDE(?LookupEngineer)
      END
      IF ?tmp:Engineer{Prop:Checked} = False
        HIDE(?glo:select6)
        HIDE(?LookupEngineer)
      END
      ThisWindow.Reset
    OF ?GLO:Select6
      IF GLO:Select6 OR ?GLO:Select6{Prop:Req}
        use:User_Code = GLO:Select6
        !Save Lookup Field Incase Of error
        look:GLO:Select6        = GLO:Select6
        IF Access:USERS.TryFetch(use:User_Code_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            GLO:Select6 = use:User_Code
          ELSE
            !Restore Lookup On Error
            GLO:Select6 = look:GLO:Select6
            SELECT(?GLO:Select6)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupEngineer
      ThisWindow.Update
      use:User_Code = GLO:Select6
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          GLO:Select6 = use:User_Code
          Select(?+1)
      ELSE
          Select(?GLO:Select6)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?GLO:Select6)
    OF ?tmp:Manufacturer
      IF ?tmp:Manufacturer{Prop:Checked} = True
        UNHIDE(?glo:select8)
      END
      IF ?tmp:Manufacturer{Prop:Checked} = False
        HIDE(?glo:select8)
      END
      ThisWindow.Reset
    OF ?tmp:ModelNumber
      IF ?tmp:ModelNumber{Prop:Checked} = True
        UNHIDE(?glo:select7)
      END
      IF ?tmp:ModelNumber{Prop:Checked} = False
        HIDE(?glo:select7)
      END
      ThisWindow.Reset
    OF ?GLO:Select7
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select7, Accepted)
      FDCB19.ResetQueue(1)
      FDCB21.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select7, Accepted)
    OF ?tmp:UnitType
      IF ?tmp:UnitType{Prop:Checked} = True
        UNHIDE(?glo:select9)
      END
      IF ?tmp:UnitType{Prop:Checked} = False
        HIDE(?glo:select9)
      END
      ThisWindow.Reset
    OF ?tmp:TransitType
      IF ?tmp:TransitType{Prop:Checked} = True
        UNHIDE(?glo:select10)
      END
      IF ?tmp:TransitType{Prop:Checked} = False
        HIDE(?glo:select10)
      END
      ThisWindow.Reset
    OF ?tmp:ChargeableChargeType
      IF ?tmp:ChargeableChargeType{Prop:Checked} = True
        UNHIDE(?glo:select11)
      END
      IF ?tmp:ChargeableChargeType{Prop:Checked} = False
        HIDE(?glo:select11)
      END
      ThisWindow.Reset
    OF ?tmp:ChargeableRepairType
      IF ?tmp:ChargeableRepairType{Prop:Checked} = True
        UNHIDE(?glo:select15)
      END
      IF ?tmp:ChargeableRepairType{Prop:Checked} = False
        HIDE(?glo:select15)
      END
      ThisWindow.Reset
    OF ?tmp:WarrantyChargeType
      IF ?tmp:WarrantyChargeType{Prop:Checked} = True
        UNHIDE(?glo:select12)
      END
      IF ?tmp:WarrantyChargeType{Prop:Checked} = False
        HIDE(?glo:select12)
      END
      ThisWindow.Reset
    OF ?tmp:WarrantyRepairType
      IF ?tmp:WarrantyRepairType{Prop:Checked} = True
        UNHIDE(?glo:select16)
      END
      IF ?tmp:WarrantyRepairType{Prop:Checked} = False
        HIDE(?glo:select16)
      END
      ThisWindow.Reset
    OF ?Ok
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Ok, Accepted)
      If tmp:CurrentStatus = 0
          glo:select2  = ''
      End!If tmp:CurrentStatus = 0
      If tmp:ExchangeStatus   = 0
          glo:select23 = ''
      End!If tmp:ExchangeStatus   = 0
      If tmp:LoanStatus   = 0
          glo:select24 = ''
      End!If tmp:LoanStatus   = 0
      If tmp:HeadAccountTick = 0
          glo:select30 = ''
      End!If tmp:HeadAccountTick = 0
      If tmp:AccountNumber = 0
          glo:select1  = ''
      End!If tmp:AccountNumber = 0
      If tmp:TurnaroundTime = 0
          glo:select3  = ''
      End!If tmp:TurnaroundTime = 0
      If tmp:Workshop = 0
          glo:select4  = ''
      End!If tmp:Workshop = 0
      If tmp:Location = 0
          glo:select5  = ''
      End!If tmp:Location = 0
      If tmp:Engineer = ''
          glo:select6  = ''
      End!If tmp:Engineer = ''
      If tmp:Manufacturer = 0
          glo:select8  = ''
      End!If tmp:Manufacturer = 0
      If tmp:ModelNumber = 0
          glo:select7  = ''
      End!If tmp:ModelNumber = 0
      If tmp:UnitType = 0
          glo:select9  = ''
      End!If tmp:UnitType = 0
      If tmp:TransitType = 0
          glo:select10 = ''
      End!If tmp:TransitType = 0
      If tmp:ChargeableChargeType = 0
          glo:select11 = ''
      End!If tmp:ChargeableChargeType = 0
      If tmp:WarrantyChargeType = 0
          glo:select12 = ''
      End!If tmp:WarrantyChargeType = 0
      If tmp:ChargeableRepairType = 0
          glo:select15 = ''
      End!If tmp:ChargeableRepairType = 0
      If tmp:WarrantyRepairType = 0
          glo:select16 = ''
      End!If tmp:WarrantyRepairType = 0
      
      If tmp:ReportOrder = 'BOO'
          Case tmp:Reporttype
              Of 'DET'
                  Status_Report('BOOKED',tmp:SortReport,0,tmp:StatusOnReport)
              Of 'SUM'
                  Status_Report_Summary('BOOKED')
              Of 'STA'
                  Status_Report('BOOKED',tmp:SortReport,1,tmp:StatusOnReport)
          End!Case report_type_temp
      End
      If tmp:ReportOrder = 'COM'
          Case tmp:ReportType
              Of 'DET'
                  Status_Report('COMPLETED',tmp:SortReport,0,tmp:StatusOnReport)
              OF 'SUM'
                  Status_Report_Summary('COMPLETED')
              Of 'STA'
                  Status_Report('COMPLETED',tmp:SortReport,1,tmp:StatusOnReport)
          End!Case report_type_temp
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Ok, Accepted)
    OF ?VSNextButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VSNextButton, Accepted)
      continue# = 1
      Case Choice(?CurrentTab)
          Of 1
              If tmp:ReportType = ''
                  Case MessageEx('You must select a Report Type.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  continue# = 0
              End!If tmp:ReportType = ''
              If tmp:reporttype = 'SUM'
                  Disable(?tmp:SortReport:Radio3)
              Else!If tmp:reporttype = 'SUM'
                  Enable(?tmp:SortReport:Radio3)
              End!If tmp:reporttype = 'SUM'
          Of 2
              If tmp:ReportOrder = ''
                  Case MessageEx('You must select a Report Order.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  continue# = 0
              End!If tmp:ReportOrder = ''
              If continue# = 1 And (glo:select20 = '' Or glo:select21 = '')
                  Case MessageEx('You must insert a Date Range.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
              End!If continue# = 1 And (glo:select20 = '' Or glo:select21 = '')
          Of 3
              If continue# = 1
                  Unhide(?tab6)
                  Unhide(?tab7)
                  If glo:select22 = 'WAR'
                      Hide(?Tab6)
                  End!If glo:select22 = 'WAR'
                  If glo:select22  = 'CHA'
                      Hide(?Tab7)
                  End!If glo:select22  = 'CHA'
              End!If continue# = 1
      End!Case Choice(?CurrentTab)
      If continue# = 1
         Wizard2.TakeAccepted()
      End!If continue# = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VSNextButton, Accepted)
    OF ?VSBackButton
      ThisWindow.Update
         Wizard2.TakeAccepted()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Status_Report_Criteria_Old')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?GLO:Select20
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?GLO:Select21
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
    ThisMakeover.Refresh()
  RETURN ReturnValue


Resizer.Resize PROCEDURE(SIGNED Control)

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize(Control)
    ThisMakeover.Refresh()
  RETURN ReturnValue

Wizard2.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()


Wizard2.TakeBackEmbed PROCEDURE
   CODE

Wizard2.TakeNextEmbed PROCEDURE
   CODE

Wizard2.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
