

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03043.INC'),ONCE        !Local module procedure declarations
                     END








Income_Report PROCEDURE(func:all,func:AccountType)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_job_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
invoice_type_temp    STRING(10)
courier_temp         REAL
labour_temp          REAL
parts_temp           REAL
vat_temp             REAL
line_total_temp      REAL
line_total_total_temp REAL
vat_total_temp       REAL
parts_total_temp     REAL
labour_total_temp    REAL
courier_total_temp   REAL
count_temp           REAL
account_number_temp  STRING(15)
tmp:PrintedBy        STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                       PROJECT(inv:Account_Number)
                       PROJECT(inv:Date_Created)
                       PROJECT(inv:Invoice_Number)
                     END
report               REPORT('Income Report'),AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(417,875,7521,1281),USE(?unnamed)
                         STRING('Date Range:'),AT(5000,417),USE(?string22),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5833,417),USE(GLO:Select1),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(6510,417,615,188),USE(GLO:Select2),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Account No:'),AT(5000,208),USE(?string22:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(5833,208),USE(account_number_temp),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@d6),AT(5833,833),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@s60),AT(5833,625),USE(tmp:PrintedBy),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,833),USE(?String43),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(5000,625),USE(?String42),TRN,FONT(,8,,)
                         STRING(@s3),AT(5833,1042),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6094,1042),USE(?String47),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6302,1042,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,1042),USE(?string59),TRN,FONT(,8,,)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,229),USE(?detailband)
                           STRING(@n10.2),AT(5208,0),USE(parts_temp),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s8),AT(104,0),USE(inv:Invoice_Number),TRN,RIGHT,FONT(,8,,)
                           STRING(@s15),AT(2604,0),USE(inv:Account_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@n10.2),AT(3698,0),USE(courier_temp),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(6510,0),USE(line_total_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@n10.2),AT(5885,0),USE(vat_temp),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(4479,0),USE(labour_temp),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s10),AT(1719,0),USE(invoice_type_temp),TRN,FONT(,8,,)
                           STRING(@d6b),AT(938,0),USE(inv:Date_Created),TRN,RIGHT,FONT(,8,,)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           STRING(@n12.2),AT(4385,104),USE(labour_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           LINE,AT(156,52,7083,0),USE(?line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(208,104),USE(?string26),TRN,FONT(,8,,FONT:bold)
                           STRING(@n14.2),AT(6510,104),USE(line_total_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(990,104),USE(count_temp),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@n12.2),AT(3604,104),USE(courier_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@n12.2),AT(5115,104),USE(parts_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@n12.2),AT(5792,104),USE(vat_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('INCOME REPORT'),AT(4271,0,3177,260),USE(?string20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Total'),AT(7031,2083,313,208),USE(?string25:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Client'),AT(2604,2083),USE(?string24:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Invoice No'),AT(156,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Issued'),AT(938,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Invoice Type'),AT(1719,2083),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('V.A.T.'),AT(6167,2083),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Courier'),AT(3875,2083),USE(?string24:5),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Parts'),AT(5521,2083),USE(?string24:4),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Labour'),AT(4688,2083),USE(?string24:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Income_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  BIND('GLO:Select2',GLO:Select2)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULTS.Open
  Access:JOBS.UseFile
  Access:SUBTRACC.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(inv:Date_Created_Key)
      Process:View{Prop:Filter} = |
      'inv:Date_Created >= GLO:Select1 AND inv:Date_Created <<= GLO:Select2'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        courier_temp    = 0
        labour_temp     = 0
        parts_temp      = 0
        vat_temp        = 0
        line_total_temp = 0
        
        error# = 0
        If func:All
            Case func:AccountType
                Of 0 !Head Account
        
                    !If the invoice is marked as account_type = 'MAI', then
                    !the invoice account number is a "Header" account number
                    !otherwise it's a "Sub" Account Number
                    If glo:Select6 <> ''
                        If inv:AccountType = 'MAI'
                            If inv:Account_Number <> glo:Select3
                                Error# = 1
                            End !If inv:Account_Number <> glo:Select3
                        Else !If inv:Account_Type = 'MAI'
        
                            !You have selected a "Header" Account Number
                            !The invoice has a "Sub" Account Number
                            !Does the Sub match the Header?
                            Access:SUBTRACC.Clearkey(sub:Main_Account_Key)
                            sub:Main_Account_Number = glo:Select6
                            sub:Account_Number  = inv:Account_Number
                            Set(sub:Main_Account_Key,sub:Main_Account_Key)
                            If Access:SUBTRACC.Next() = Level:Benign
                                If sub:Main_Account_Number <> glo:Select6 Or |
                                    sub:Account_Number  <> inv:Account_Number
                                    Error# = 1
                                End !sub:Account_Number  = inv:Account_Number
                            Else!If Access:SUBTRACC.Next() = Level:Benign
                                Error# = 1
                            End !If Access:SUBTRACC.Next() = Level:Benign
        
                        End !If inv:Account_Type = 'MAI'
                    
                    End !If glo:Select6 <> ''
                Of 1 !Sub Account
                    If glo:select3 <> ''
                        If inv:AccountType = 'MAI'
                            !You have selected a "Sub" Account Number
                            !The invoice has a "Header" Account Number
                            !Does the Header account number match the sub?
                            Access:SUBTRACC.Clearkey(sub:Main_Account_Key)
                            sub:Main_Account_Number = inv:Account_Number
                            sub:Account_Number  = glo:Select3
                            Set(sub:Main_Account_Key,sub:Main_Account_Key)
                            If Access:SUBTRACC.Next() = Level:Benign
                                If sub:Main_Account_Number <> inv:Account_Number Or |
                                    sub:Account_Number <> glo:Select3
                                    Error# = 1
                                End
                            Else !If Access:SUBTRACC.Next() = Level:Benign
                                Error# = 1
                            End !If Access:SUBTRACC.Next() = Level:Benign
                        Else !If inv:Account_Type = 'MAI'
                            If inv:account_number <> glo:select3
                                error# = 1
                            End!If inv:account_number <> glo:select3
        
                        End !If inv:Account_Type = 'MAI'
        
                    End!If glo:select3 <> ''
            End !func:AccountType
        
        End !func:All
        IF error# = 0 And glo:select4 <> 'YES'
            If inv:invoice_type = 'WAR' or inv:invoice_Type = 'CHA' or inv:invoice_type = 'SIN'
                error# = 1
            End!If inv:invoice_type = 'WAR' or inv:invoice_Type = 'CHA' or inv:invoice_type = 'SIN'
        End!IF error# = 0 And glo:select4 <> 'YES'
        If error# = 0 And glo:select5 <> 'YES'
            If inv:invoice_type = 'RET'
                error# = 1
            End!If inv:invoice_type = 'RET'
        End!If error# = 0 And glo:select5 <> 'YES'
        
        If error# = 0
        
            Case inv:invoice_type
                Of 'WAR'
                    invoice_type_temp   = 'Warranty'
                    courier_temp    += INV:Courier_Paid
                    labour_temp     += INV:Labour_Paid
                    parts_temp      += INV:Parts_Paid
                    vat_temp        += (Round(INV:Courier_Paid * (INV:Vat_Rate_Labour/100),.01)) + |
                                        (Round(INV:Labour_Paid * (INV:Vat_Rate_Labour/100),.01)) + |
                                        (Round(INV:Parts_Paid * (INV:Vat_Rate_Parts/100),.01))
        
                Of 'RET'
                    invoice_type_temp   = 'Retail'
                    courier_temp    += INV:Courier_Paid
                    labour_temp     += INV:Labour_Paid
                    parts_temp      += INV:Parts_Paid
                    vat_temp        += (Round(INV:Courier_Paid * (INV:Vat_Rate_retail/100),.01)) + |
                                        (Round(INV:Labour_Paid * (INV:Vat_Rate_Retail/100),.01)) + |
                                        (Round(INV:Parts_Paid * (INV:Vat_Rate_retail/100),.01))
                Else
                    invoice_type_temp = 'Chargeable'
                
                    save_job_id = access:jobs.savefile()
                    access:jobs.clearkey(job:InvoiceNumberKey)
                    job:invoice_number = inv:invoice_number
                    set(job:InvoiceNumberKey,job:InvoiceNumberKey)
                    loop
                        if access:jobs.next()
                           break
                        end !if
                        if job:invoice_number <> inv:invoice_number      |
                            then break.  ! end if
                        yldcnt# += 1
                        if yldcnt# > 25
                           yield() ; yldcnt# = 0
                        end !if
                        courier_temp    += JOB:Invoice_Courier_Cost
                        labour_temp     += JOB:Invoice_Labour_Cost
                        parts_temp      += JOB:Invoice_Parts_Cost
                        vat_temp        += (Round(JOB:Invoice_Courier_Cost * (inv:vat_rate_labour/100),.01)) + |
                                            (Round(JOB:Invoice_Labour_Cost * (inv:vat_rate_labour/100),.01)) + |
                                            (Round(JOB:Invoice_Parts_Cost * (inv:vat_rate_parts/100),.01))
                    end !loop
                    access:jobs.restorefile(save_job_id)
            End!If inv:invoice_type = 'WAR'
        
            courier_total_temp      += courier_temp
            labour_total_temp       += labour_temp
            parts_total_temp        += parts_temp
            vat_total_temp          += vat_temp
            line_total_temp         += Round(courier_temp,.01) + Round(labour_temp,.01) + Round(parts_temp,.01) + Round(vat_temp,.01)
            line_total_total_temp   += line_total_temp
            count_temp              += 1
            Print(rpt:detail)
            tmp:RecordsCount += 1
        
        End!If error# = 0
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'INVOICE')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  If func:all
      Case func:AccountType
          Of 0
              Account_Number_Temp = glo:Select6
          Of 1
              Account_Number_Temp = glo:Select3
      End !Case func:AccountType
  Else!If glo:select3 <> ''
      account_number_temp = 'ALL'
  End!If glo:select3 <> ''
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Income_Report'
  END
  report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Income_Report',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Income_Report',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Income_Report',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Income_Report',1)
    SolaceViewVars('save_job_id',save_job_id,'Income_Report',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Income_Report',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Income_Report',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Income_Report',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Income_Report',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Income_Report',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Income_Report',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Income_Report',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Income_Report',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Income_Report',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Income_Report',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Income_Report',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Income_Report',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Income_Report',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Income_Report',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Income_Report',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Income_Report',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Income_Report',1)
    SolaceViewVars('InitialPath',InitialPath,'Income_Report',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Income_Report',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Income_Report',1)
    SolaceViewVars('invoice_type_temp',invoice_type_temp,'Income_Report',1)
    SolaceViewVars('courier_temp',courier_temp,'Income_Report',1)
    SolaceViewVars('labour_temp',labour_temp,'Income_Report',1)
    SolaceViewVars('parts_temp',parts_temp,'Income_Report',1)
    SolaceViewVars('vat_temp',vat_temp,'Income_Report',1)
    SolaceViewVars('line_total_temp',line_total_temp,'Income_Report',1)
    SolaceViewVars('line_total_total_temp',line_total_total_temp,'Income_Report',1)
    SolaceViewVars('vat_total_temp',vat_total_temp,'Income_Report',1)
    SolaceViewVars('parts_total_temp',parts_total_temp,'Income_Report',1)
    SolaceViewVars('labour_total_temp',labour_total_temp,'Income_Report',1)
    SolaceViewVars('courier_total_temp',courier_total_temp,'Income_Report',1)
    SolaceViewVars('count_temp',count_temp,'Income_Report',1)
    SolaceViewVars('account_number_temp',account_number_temp,'Income_Report',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Income_Report',1)


BuildCtrlQueue      Routine







