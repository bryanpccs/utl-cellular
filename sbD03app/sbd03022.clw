

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03022.INC'),ONCE        !Local module procedure declarations
                     END


Units_In_Channel_Criteria PROCEDURE                   !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGFLAG          BYTE(0)
DASBRW::6:TAGMOUSE         BYTE(0)
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer                       LIKE(GLO:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
FilesOpened          BYTE
tag_temp             STRING(1)
trade_account_temp   STRING('ALL')
manfuacturer_temp    STRING(30)
single_page_temp     STRING(3)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select3
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?manfuacturer_temp
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
FDCB4::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
BRW3::View:Browse    VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                       PROJECT(mod:Manufacturer)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
mod:Manufacturer       LIKE(mod:Manufacturer)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Exchange Units In Channel Criteria'),AT(,,252,348),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,244,312),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Exchange Units In Channel Criteria'),USE(?Tab1)
                           PROMPT('Select Trade Account'),AT(8,8),USE(?Prompt4),FONT(,,COLOR:Navy,FONT:bold)
                           OPTION('Trade Account'),AT(8,20,235,28),USE(trade_account_temp),BOXED
                             RADIO('All'),AT(23,32),USE(?trade_account_temp:Radio1),VALUE('ALL')
                             RADIO('Individual'),AT(55,32),USE(?trade_account_temp:Radio2),VALUE('IND')
                           END
                           COMBO(@s40),AT(112,32,124,10),USE(GLO:Select3),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('60L(2)|M@s15@120L(2)|M@s30@'),DROP(10,160),FROM(Queue:FileDropCombo)
                           PROMPT('Select Model Number(s)'),AT(8,56),USE(?Prompt1),FONT('Tahoma',8,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           CHECK('One Model Per Page'),AT(164,56),USE(GLO:Select4),VALUE('YES','NO')
                           SHEET,AT(8,68,236,188),USE(?Sheet2),SPREAD
                             TAB('By Model Number'),USE(?Tab2)
                             END
                             TAB('By Manufacturer'),USE(?Tab3)
                               COMBO(@s30),AT(116,84,124,10),USE(manfuacturer_temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:1)
                             END
                           END
                           PROMPT('Select Date Range'),AT(8,264),USE(?Prompt5),FONT(,,COLOR:Navy,FONT:bold)
                           PROMPT('Start Date'),AT(49,280),USE(?glo:select1:Prompt)
                           ENTRY(@d6b),AT(125,280,64,10),USE(GLO:Select1),LEFT,FONT(,,,FONT:bold),UPR
                           BUTTON,AT(193,280,10,10),USE(?PopCalendar),ICON('Calenda2.ico')
                           BUTTON,AT(193,296,10,10),USE(?PopCalendar:2),ICON('Calenda2.ico')
                           PROMPT('End Date'),AT(49,296),USE(?glo:select2:Prompt)
                           ENTRY(@d6b),AT(125,296,64,10),USE(GLO:Select2),LEFT,FONT(,,,FONT:bold),UPR
                         END
                       END
                       LIST,AT(12,100,228,132),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),VCR,FORMAT('11LI@s1@90L(2)|M~Model Number~@s30@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse)
                       BUTTON('&Untag All'),AT(192,236,48,16),USE(?DASUNTAGALL),LEFT,KEY(AltU),ICON('untag.gif')
                       BUTTON('&Rev tags'),AT(76,164,50,13),USE(?DASREVTAG),HIDE
                       BUTTON('sho&W tags'),AT(124,164,70,13),USE(?DASSHOWTAG),HIDE
                       BUTTON('Tag &All'),AT(102,236,48,16),USE(?DASTAGAll),LEFT,KEY(AltA),ICON('tagall.gif')
                       BUTTON('&Tag'),AT(12,236,48,16),USE(?DASTAG),LEFT,KEY(AltT),ICON('tag.gif')
                       ENTRY(@s30),AT(12,84,100,10),USE(mod:Model_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                       BUTTON('&OK'),AT(132,324,56,16),USE(?OkButton),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(188,324,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif'),STD(STD:Close)
                       PANEL,AT(4,320,244,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

BRW3                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW3::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW3::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet2) = 2
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?trade_account_temp{prop:Font,3} = -1
    ?trade_account_temp{prop:Color} = 15066597
    ?trade_account_temp{prop:Trn} = 0
    ?trade_account_temp:Radio1{prop:Font,3} = -1
    ?trade_account_temp:Radio1{prop:Color} = 15066597
    ?trade_account_temp:Radio1{prop:Trn} = 0
    ?trade_account_temp:Radio2{prop:Font,3} = -1
    ?trade_account_temp:Radio2{prop:Color} = 15066597
    ?trade_account_temp:Radio2{prop:Trn} = 0
    If ?GLO:Select3{prop:ReadOnly} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 15066597
    Elsif ?GLO:Select3{prop:Req} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 8454143
    Else ! If ?GLO:Select3{prop:Req} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 16777215
    End ! If ?GLO:Select3{prop:Req} = True
    ?GLO:Select3{prop:Trn} = 0
    ?GLO:Select3{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?GLO:Select4{prop:Font,3} = -1
    ?GLO:Select4{prop:Color} = 15066597
    ?GLO:Select4{prop:Trn} = 0
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    If ?manfuacturer_temp{prop:ReadOnly} = True
        ?manfuacturer_temp{prop:FontColor} = 65793
        ?manfuacturer_temp{prop:Color} = 15066597
    Elsif ?manfuacturer_temp{prop:Req} = True
        ?manfuacturer_temp{prop:FontColor} = 65793
        ?manfuacturer_temp{prop:Color} = 8454143
    Else ! If ?manfuacturer_temp{prop:Req} = True
        ?manfuacturer_temp{prop:FontColor} = 65793
        ?manfuacturer_temp{prop:Color} = 16777215
    End ! If ?manfuacturer_temp{prop:Req} = True
    ?manfuacturer_temp{prop:Trn} = 0
    ?manfuacturer_temp{prop:FontStyle} = font:Bold
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?glo:select1:Prompt{prop:FontColor} = -1
    ?glo:select1:Prompt{prop:Color} = 15066597
    If ?GLO:Select1{prop:ReadOnly} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 15066597
    Elsif ?GLO:Select1{prop:Req} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 8454143
    Else ! If ?GLO:Select1{prop:Req} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 16777215
    End ! If ?GLO:Select1{prop:Req} = True
    ?GLO:Select1{prop:Trn} = 0
    ?GLO:Select1{prop:FontStyle} = font:Bold
    ?glo:select2:Prompt{prop:FontColor} = -1
    ?glo:select2:Prompt{prop:Color} = 15066597
    If ?GLO:Select2{prop:ReadOnly} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 15066597
    Elsif ?GLO:Select2{prop:Req} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 8454143
    Else ! If ?GLO:Select2{prop:Req} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 16777215
    End ! If ?GLO:Select2{prop:Req} = True
    ?GLO:Select2{prop:Trn} = 0
    ?GLO:Select2{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    If ?mod:Model_Number{prop:ReadOnly} = True
        ?mod:Model_Number{prop:FontColor} = 65793
        ?mod:Model_Number{prop:Color} = 15066597
    Elsif ?mod:Model_Number{prop:Req} = True
        ?mod:Model_Number{prop:FontColor} = 65793
        ?mod:Model_Number{prop:Color} = 8454143
    Else ! If ?mod:Model_Number{prop:Req} = True
        ?mod:Model_Number{prop:FontColor} = 65793
        ?mod:Model_Number{prop:Color} = 16777215
    End ! If ?mod:Model_Number{prop:Req} = True
    ?mod:Model_Number{prop:Trn} = 0
    ?mod:Model_Number{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW3.UpdateBuffer
   GLO:Queue.Pointer = mod:Model_Number
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = mod:Model_Number
     ADD(GLO:Queue,GLO:Queue.Pointer)
    tag_temp = '*'
  ELSE
    DELETE(GLO:Queue)
    tag_temp = ''
  END
    Queue:Browse.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse.tag_temp_Icon = 2
  ELSE
    Queue:Browse.tag_temp_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW3.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = mod:Model_Number
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW3.Reset
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::6:QUEUE = GLO:Queue
    ADD(DASBRW::6:QUEUE)
  END
  FREE(GLO:Queue)
  BRW3.Reset
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer = mod:Model_Number
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = mod:Model_Number
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW3.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
show_hide       Routine
    Case trade_account_temp
        Of 'ALL'
            Disable(?glo:select3)
        Of 'IND'
            Enable(?glo:select3)
    End!Case trade_account_temp
    Display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Units_In_Channel_Criteria',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Units_In_Channel_Criteria',1)
    SolaceViewVars('tag_temp',tag_temp,'Units_In_Channel_Criteria',1)
    SolaceViewVars('trade_account_temp',trade_account_temp,'Units_In_Channel_Criteria',1)
    SolaceViewVars('manfuacturer_temp',manfuacturer_temp,'Units_In_Channel_Criteria',1)
    SolaceViewVars('single_page_temp',single_page_temp,'Units_In_Channel_Criteria',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trade_account_temp;  SolaceCtrlName = '?trade_account_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trade_account_temp:Radio1;  SolaceCtrlName = '?trade_account_temp:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trade_account_temp:Radio2;  SolaceCtrlName = '?trade_account_temp:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select3;  SolaceCtrlName = '?GLO:Select3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select4;  SolaceCtrlName = '?GLO:Select4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?manfuacturer_temp;  SolaceCtrlName = '?manfuacturer_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select1:Prompt;  SolaceCtrlName = '?glo:select1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select1;  SolaceCtrlName = '?GLO:Select1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select2:Prompt;  SolaceCtrlName = '?glo:select2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select2;  SolaceCtrlName = '?GLO:Select2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Model_Number;  SolaceCtrlName = '?mod:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Units_In_Channel_Criteria')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Units_In_Channel_Criteria')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt4
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:MANUFACT.Open
  SELF.FilesOpened = True
  BRW3.Init(?List,Queue:Browse.ViewPosition,BRW3::View:Browse,Queue:Browse,Relate:MODELNUM,SELF)
  OPEN(window)
  SELF.Opened=True
  glo:select1 = Deformat('1/1/1990',@d6)
  glo:select2 = Today()
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?glo:select1{Prop:Alrt,255} = MouseLeft2
  ?glo:select2{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  BRW3.Q &= Queue:Browse
  BRW3.RetainRow = 0
  BRW3.AddSortOrder(,mod:Manufacturer_Key)
  BRW3.AddRange(mod:Manufacturer,manfuacturer_temp)
  BRW3.AddLocator(BRW3::Sort1:Locator)
  BRW3::Sort1:Locator.Init(?MOD:Model_Number,mod:Model_Number,1,BRW3)
  BRW3.AddSortOrder(,mod:Model_Number_Key)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(,mod:Model_Number,1,BRW3)
  BIND('tag_temp',tag_temp)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW3.AddField(tag_temp,BRW3.Q.tag_temp)
  BRW3.AddField(mod:Model_Number,BRW3.Q.mod:Model_Number)
  BRW3.AddField(mod:Manufacturer,BRW3.Q.mod:Manufacturer)
  FDCB2.Init(GLO:Select3,?GLO:Select3,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:SUBTRACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(sub:Account_Number_Key)
  FDCB2.AddField(sub:Account_Number,FDCB2.Q.sub:Account_Number)
  FDCB2.AddField(sub:Company_Name,FDCB2.Q.sub:Company_Name)
  FDCB2.AddField(sub:RecordNumber,FDCB2.Q.sub:RecordNumber)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  FDCB4.Init(manfuacturer_temp,?manfuacturer_temp,Queue:FileDropCombo:1.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo:1,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo:1
  FDCB4.AddSortOrder(man:Manufacturer_Key)
  FDCB4.AddField(man:Manufacturer,FDCB4.Q.man:Manufacturer)
  FDCB4.AddField(man:RecordNumber,FDCB4.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW3.AskProcedure = 0
      CLEAR(BRW3.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:MANUFACT.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Units_In_Channel_Criteria',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?trade_account_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trade_account_temp, Accepted)
      Do show_hide
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trade_account_temp, Accepted)
    OF ?manfuacturer_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?manfuacturer_temp, Accepted)
      BRW3.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?manfuacturer_temp, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select1 = TINCALENDARStyle1(GLO:Select1)
          Display(?glo:select1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select2 = TINCALENDARStyle1(GLO:Select2)
          Display(?glo:select2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      error# = 0
      If ~Records(glo:Queue)
      	Case MessageEx('You must select at least ONE Model Number.','ServiceBase 2000',|
      	               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
      		Of 1 ! &OK Button
      	End!Case MessageEx
          error# = 1
      End!If ~Record(glo:Queue)
      IF error# = 0
          If trade_account_temp = 'ALL'
              glo:select3 = ''
          End
          Exchange_Units_In_Channel
      End!IF error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Units_In_Channel_Criteria')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?GLO:Select1
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?GLO:Select2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::6:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::6:DASUNTAGALL
      Do show_hide
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW3.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet2) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW3.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = mod:Model_Number
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW3.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW3::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW3::RecordStatus=ReturnValue
  IF BRW3::RecordStatus NOT=Record:OK THEN RETURN BRW3::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = mod:Model_Number
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW3::RecordStatus
  RETURN ReturnValue

