

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03091.INC'),ONCE        !Local module procedure declarations
                     END








StatusReport PROCEDURE(func:DateRangeType,func:StartDate,func:EndDate,func:JobType,func:InvoiceType,func:DespatchedType,func:CompletedType,func:StatusType,func:ReportOrder,func:StatusReportType,func:JobBatchNumber,func:EDIBatchNumber,func:CustomerStatus)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
InWorkshopQ          QUEUE,PRE(iwq)
DateValue            DATE
JobRefValue          LONG
                     END
tmp:DefaultTelephone STRING(20)
save_aus_id          USHORT,AUTO
tmp:DefaultFax       STRING(20)
page_one_temp        LONG
count_temp           LONG
save_job_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
value_temp           REAL
invoice_job_type_temp STRING(30)
completed_job_type_temp STRING(30)
Date_range_Type_Temp STRING(30)
tmp:PrintedBy        STRING(60)
job_type_temp        STRING(60)
tmp:DespatchType     STRING(60)
tmp:custname         STRING(20)
tmp:OldStatus        STRING(30)
tmp:StatusDate       DATE
tmp:StatusUser       STRING(3)
tmp:status           STRING(30)
tmp:StatusType       STRING(1)
tmp:DateDespatched   DATE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Colour)
                       PROJECT(job:ESN)
                       PROJECT(job:Engineer)
                       PROJECT(job:Location)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:date_booked)
                     END
report               REPORT('Status Report'),AT(458,1938,10938,5719),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,),LANDSCAPE,THOUS
                       HEADER,AT(427,323,9740,1375),USE(?unnamed:2)
                         STRING(@s30),AT(94,52),USE(def:User_Name),TRN,FONT(,12,,FONT:bold)
                         STRING(@s30),AT(104,260,3531,198),USE(def:Address_Line1),TRN,FONT(,8,,)
                         STRING('Date Printed:'),AT(7448,365),USE(?RunPrompt),TRN,FONT(,8,,)
                         STRING(@d6),AT(8438,365),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(104,365,3531,198),USE(def:Address_Line2),TRN,FONT(,8,,)
                         STRING(@s30),AT(104,469,3531,198),USE(def:Address_Line3),TRN,FONT(,8,,)
                         STRING(@s15),AT(104,573),USE(def:Postcode),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(7448,625,625,208),USE(?String67),TRN,FONT(,8,,)
                         STRING(@s60),AT(8438,625),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Tel: '),AT(104,729),USE(?String15),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,729),USE(tmp:DefaultTelephone),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(104,833),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,833),USE(tmp:DefaultFax),TRN,FONT(,8,,)
                         STRING(@s255),AT(573,938,3531,198),USE(def:EmailAddress),TRN,FONT(,8,,)
                         STRING('Email:'),AT(104,938),USE(?String16:2),TRN,FONT(,9,,)
                         STRING('Page:'),AT(7448,885),USE(?PagePrompt),TRN,FONT(,8,,)
                         STRING(@s4),AT(8438,885),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(8802,885),USE(?String72),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(9063,885,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                       END
break1                 BREAK(EndOfReport),USE(?unnamed:5)
DETAIL                   DETAIL,AT(,,,146),USE(?DetailBand)
                           STRING(@d6b),AT(156,0),USE(job:date_booked),TRN,FONT(,7,,)
                           STRING(@s9),AT(781,0),USE(job:Ref_Number),TRN,LEFT,FONT(,7,,)
                           STRING(@s12),AT(1406,0),USE(job:Account_Number),TRN,LEFT,FONT(,7,,)
                           STRING(@s15),AT(2240,0,990,156),USE(job:Model_Number),TRN,LEFT,FONT(,7,,)
                           STRING(@s20),AT(3333,0,1042,156),USE(tmp:custname),TRN,LEFT,FONT(,7,,)
                           STRING(@s16),AT(4479,0),USE(job:ESN),TRN,LEFT,FONT(,7,,)
                           STRING(@s30),AT(9167,0),USE(tmp:status),TRN,FONT(,7,,)
                           STRING(@s20),AT(6563,0),USE(job:Colour),TRN,FONT(,7,,)
                           STRING(@d6b),AT(8229,0),USE(tmp:DateDespatched),RIGHT,FONT(,7,,)
                           STRING(@s20),AT(5417,0),USE(job:Order_Number),TRN,FONT(,7,,)
                           STRING(@s1),AT(8948,0),USE(tmp:StatusType),TRN,FONT(,7,,)
                         END
detail1                  DETAIL,AT(,,,146),USE(?unnamed:4)
                           STRING(@d6b),AT(156,0),USE(job:date_booked,,?job:date_booked:2),TRN,FONT(,7,,)
                           STRING(@s8),AT(729,0),USE(job:Ref_Number,,?job:Ref_Number:2),TRN,LEFT,FONT(,7,,)
                           STRING(@s15),AT(1250,0,729,156),USE(job:Model_Number,,?job:Model_Number:2),TRN,LEFT,FONT(,7,,)
                           STRING(@s30),AT(9167,0),USE(tmp:status,,?tmp:Status:2),TRN,FONT(,7,,)
                           STRING(@s15),AT(3281,0),USE(job:Account_Number,,?job:Account_Number:2),TRN,LEFT,FONT(,7,,)
                           STRING(@s20),AT(4375,0),USE(job:ESN,,?job:ESN:2),TRN,LEFT,FONT(,7,,)
                           STRING(@s3),AT(8854,0),USE(tmp:StatusUser),TRN,FONT(,7,,)
                           STRING(@d6b),AT(8229,0),USE(tmp:StatusDate,,?tmp:StatusDate:2),TRN,LEFT,FONT(,7,,)
                           STRING(@s30),AT(6510,0),USE(tmp:OldStatus),TRN,FONT(,7,,)
                           STRING(@s3),AT(6042,0),USE(job:Engineer),TRN,FONT(,7,,)
                           STRING(@s20),AT(2031,0),USE(job:Location),TRN,FONT(,7,,)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:3)
                           LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(260,156),USE(?String68),TRN,FONT(,10,,FONT:bold)
                           STRING(@s9),AT(1198,156),USE(count_temp),TRN,FONT(,10,,FONT:bold)
                         END
                       END
                       FORM,AT(406,313,10969,7448),USE(?unnamed)
                         IMAGE('Rlistlan.gif'),AT(0,0,10938,7448),USE(?Image1)
                         STRING('STATUS REPORT'),AT(4625,52),USE(?String19),TRN,FONT(,14,,FONT:bold)
                         GROUP,AT(52,1208,10729,365),USE(?MainTitle),BOXED
                           STRING('Booked'),AT(208,1365),USE(?String23),TRN,FONT(,7,,FONT:bold)
                           STRING('Job No'),AT(833,1365),USE(?String20),TRN,FONT(,7,,FONT:bold)
                           STRING('Account No'),AT(1458,1365),USE(?String21),TRN,FONT(,7,,FONT:bold)
                           STRING('Model No'),AT(2292,1365),USE(?String25),TRN,FONT(,7,,FONT:bold)
                           STRING('Cust Details'),AT(3385,1365),USE(?String27),TRN,FONT(,7,,FONT:bold)
                           STRING('I.M.E.I. No'),AT(4531,1365),USE(?String29),TRN,FONT(,7,,FONT:bold)
                           STRING('Order Number'),AT(5469,1365),USE(?String29:2),TRN,FONT(,7,,FONT:bold)
                           STRING('Colour'),AT(6615,1365),USE(?Colour:Prompt),TRN,FONT(,7,,FONT:bold)
                           STRING('Despatched'),AT(8250,1354),USE(?String29:3),TRN,FONT(,7,,FONT:bold)
                           STRING('Status'),AT(9219,1354),USE(?CurrentStatus:2),TRN,FONT(,7,,FONT:bold)
                         END
                         GROUP,AT(52,1208,10729,365),USE(?StatusTitle),BOXED
                           STRING('Booked'),AT(208,1365),USE(?String23:2),TRN,FONT(,7,,FONT:bold)
                           STRING('Job No'),AT(896,1365),USE(?String20:2),TRN,FONT(,7,,FONT:bold)
                           STRING('Model No'),AT(1302,1365),USE(?String25:2),TRN,FONT(,7,,FONT:bold)
                           STRING('Location'),AT(2083,1365),USE(?Location),TRN,FONT(,7,,FONT:bold)
                           STRING('Previous Status'),AT(6563,1365),USE(?String31:4),TRN,FONT(,7,,FONT:bold)
                           STRING('Changed'),AT(8281,1365),USE(?String31:3),TRN,FONT(,7,,FONT:bold)
                           STRING('Current Status'),AT(9219,1365),USE(?CurrentStatus),TRN,FONT(,7,,FONT:bold)
                           STRING('Account No.'),AT(3333,1365),USE(?Location:3),TRN,FONT(,7,,FONT:bold)
                           STRING('I.M.E.I. No.'),AT(4427,1365),USE(?Location:2),TRN,FONT(,7,,FONT:bold)
                           STRING('User'),AT(8906,1365),USE(?String31:6),TRN,FONT(,7,,FONT:bold)
                           STRING('Eng'),AT(6094,1365),USE(?String31:2),TRN,FONT(,7,,FONT:bold)
                         END
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('StatusReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Access:USERS.UseFile
  Access:SUBTRACC.UseFile
  Access:AUDSTATS.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      RecordsToProcess = Records(Jobs)
      ! Start Change 2223 BE(04/03/03)
      IF (func:ReportOrder = 'IN WORKSHOP') THEN
          IF (func:DateRangeType = 2) THEN
              RecordsToProcess  = RecordsToProcess + RECORDS(JOBSE)
          ELSE
              RecordsToProcess  = ( 2 * RecordsToProcess) + RECORDS(JOBSE)
          END
      END
      ! End Change 2223 BE(04/03/03)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
            save_job_id = access:jobs.savefile()
            Case func:ReportOrder
                Of 'JOB NUMBER'
                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                    set(job:ref_number_key,0)
                Of 'I.M.E.I. NUMBER'
                    Access:JOBS.Clearkey(job:ESN_Key)
                    set(job:esn_key,0)
                Of 'ACCOUNT NUMBER'
                    Access:JOBS.Clearkey(job:AccountNumberKey)
                    set(job:accountnumberkey,0)
                Of 'MODEL NUMBER'
                    Access:JOBS.Clearkey(job:Model_Number_Key)
                    set(job:model_number_key,0)
                Of 'STATUS'
                    Case func:StatusType
                        Of 0 !Job Status
                            Access:JOBS.Clearkey(job:By_Status)
                            set(job:by_status,0)
                        Of 1 !Exchange Status
                            Access:JOBS.Clearkey(job:ExcStatusKey)
                            Set(job:ExcStatusKey)
                        Of 2 !Loan Status
                            Access:JOBS.Clearkey(job:LoanStatusKey)
                            Set(job:LoanStatusKey)
                    End !Case tmp:StatusType
                Of 'MSN'
                    Access:JOBS.Clearkey(job:MSN_Key)
                    set(job:msn_key,0)
                Of 'SURNAME'
                    Access:JOBS.Clearkey(job:Surname_Key)
                    set(job:surname_key,0)
                Of 'ENGINEER'
                    Access:JOBS.Clearkey(job:Engineer_Key)
                    set(job:engineer_key,0)
                Of 'MOBILE NUMBER'
                    Access:JOBS.Clearkey(job:MobileNumberKey)
                    set(job:mobilenumberkey,0)
                Of 'DATE BOOKED'
                    Access:JOBS.Clearkey(job:Date_Booked_Key)
                    Case func:DateRangeType
                        Of 0 !Booking
                            job:date_booked = func:StartDate
                            set(job:date_booked_key,job:date_booked_key)
                        ! Start Change 2223 BE(05/03/03)
                        !Of 1 !Completed
                        ELSE
                        ! End Change 2223 BE(05/03/03)
                            Set(job:Date_Booked_Key,0)
                    End !tmp:DateRangeType
                Of 'DATE COMPLETED'
                    Access:JOBS.ClearKey(job:DateCompletedKey)
                    Case func:DateRangeType
                        ! Start Change 2223 BE(05/03/03)
                        !Of 0 !Booking
                        !    Set(job:DateCompletedKey,0)
                        ! End Change 2223 BE(05/03/03)
                        Of 1 !Completed
                            job:date_completed = func:StartDate
                            set(job:datecompletedkey,job:datecompletedkey)
                        ! Start Change 2223 BE(05/03/03)
                        ELSE
                            Set(job:DateCompletedKey,0)
                        ! End Change 2223 BE(05/03/03)
                    End !Case tmp:DateRangeType
                ! Start Change 2223 BE(04/03/03)
                OF 'IN WORKSHOP'
                    ! Build In Workshop Key Queue
                    FREE(InWorkshopQ)
                    Access:JOBSE.ClearKey(jobe:RecordNumberKey)
                    SET(jobe:RecordNumberKey, 0)
                    LOOP
                        IF (Access:JOBSE.NEXT()) THEN
                            BREAK
                        END
                        RecordsProcessed += 1
                        Do DisplayProgress
                        iwq:DateValue = jobe:InWorkshopDate
                        iwq:JobRefValue = jobe:RefNumber
                        IF (func:DateRangeType = 2) THEN
                            IF (INRANGE(jobe:InWorkshopDate,func:StartDate,func:EndDate)) THEN
                                ADD(InWorkshopQ)
                            END
                        ELSE
                            ADD(InWorkshopQ)
                        END
                    END
                    IF (func:DateRangeType <> 2) THEN
                        ! Now check for any Job Records without Job Extension records
                        SORT(InWorkshopQ, iwq:JobRefValue)
                        ! Access Job file in most efficient sequence depending upon
                        ! DateRange setting.
                        IF (func:DateRangeType = 0) THEN   !
                            job:date_booked = func:StartDate
                            set(job:date_booked_key,job:date_booked_key)
                        ELSE
                            job:date_completed = func:StartDate
                            set(job:datecompletedkey,job:datecompletedkey)
                        END
                        LOOP
                            IF (Access:JOBS.NEXT()) THEN
                                BREAK
                            END
                            RecordsProcessed += 1
                            Do DisplayProgress
                            If (func:DateRangeType = 0) THEN
                                IF (job:Date_Booked > func:EndDate) THEN
                                    BREAK
                                END
                            ELSE
                                IF (job:Date_Completed > func:EndDate) THEN
                                    BREAK
                                END
                            END
                            iwq:DateValue = Date(1, 1, 1900)
                            iwq:JobRefValue = job:Ref_Number
                            GET(InWorkshopQ, iwq:JobRefValue)
                            IF (ERRORCODE()) THEN
                                ADD(InWorkshopQ, iwq:JobRefValue)
                            END
                        END
                    END
                    SORT(InWorkshopQ, iwq:DateValue)
                    IWRecordCount# =  Records(InWorkshopQ)
                    IWIndex# = 1
                ! End Change 2223 BE(04/03/03)
            End !Case tmp:ReportOrder
            Loop
                ! Start Change 2223 BE(04/03/03)
                IF (func:ReportOrder = 'IN WORKSHOP') THEN
                    IF (IWIndex# > IWRecordCount#) THEN
                        BREAK
                    END
                    GET(InWorkshopQ, IWIndex#)
                    IWIndex# = IWIndex#+1
                    job:Ref_Number = iwq:JobRefValue
                    IF (Access:JOBS.TryFetch(job:Ref_Number_Key)) THEN
                        ! Ignore Orphaned Job extension Records
                        CYCLE
                    END
                ELSE
                    IF (Access:JOBS.NEXT()) THEN
                        BREAK
                    END
                END
                !If Access:JOBS.NEXT()
                !   Break
                !End !If
                ! End Change 2223 BE(04/03/03)
        
                RecordsProcessed += 1
                Do DisplayProgress
        
                Case func:ReportOrder
                    Of 'DATE BOOKED'
                        If func:DateRangeType = 0
                            If job:Date_Booked > func:EndDate
                                Break
                            End !If job:Date_Booked > tmp:EndDate
                        End !If tmp:DateRangeType = 0
                    Of 'DATE COMPLETED'
                        If func:DateRangeType = 1
                            If job:Date_Completed > func:EndDate
                                Break
                            End !If job:Date_Completed > tmp:EndDate
                        End !If tmp:DateRangeType = 1
                End !Case tmp:ReportOrder
        
                If StatusReportValidation(func:DateRangeType,func:StartDate,func:EndDate,func:JobType,func:InvoiceType,func:DespatchedType,func:CompletedType,func:StatusType,func:JobBatchNumber,func:EDIBatchNumber)
                    Cycle
                End !If StatusReportValidation(tmp:DateRangeType,tmp:StartDate,tmp:EndDate,tmp:JobType,tmp:InvoiceType,tmp:DespatchedType,tmp:CompletedType)
        
                count_temp += 1
                tmp:RecordsCount += 1
                tmp:custname = ''
                If job:surname  = ''
                    access:subtracc.clearkey(sub:account_number_key)
                    sub:account_number = job:account_number
                    if access:subtracc.fetch(sub:account_number_key) = level:benign
                        access:tradeacc.clearkey(tra:account_number_key) 
                        tra:account_number = sub:main_account_number
                        if access:tradeacc.fetch(tra:account_number_key) = level:benign
                            if tra:use_sub_accounts = 'YES'
                                tmp:custname    = sub:company_name
                            else!if tra:use_sub_accounts = 'YES'
                                tmp:custname    = tra:company_name
                            end!if tra:use_sub_accounts = 'YES'
                        end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                    end!if access:subtracc.fetch(sub:account_number_key) = level:benign  
                Else!If job:surname  = ''
                    tmp:custname    = job:surname
                End!If job:surname  = ''
        
                tmp:OldStatus   = ''
                tmp:StatusDate  = ''
                tmp:StatusUser = ''
                tmp:Status  = ''
        
                Save_aus_ID = Access:AUDSTATS.SaveFile()
                Access:AUDSTATS.ClearKey(aus:DateChangedKey)
                aus:RefNumber   = job:Ref_Number
                If func:CustomerStatus And func:StatusReportType = 0
                    If job:Exchange_Unit_Number <> ''
                        tmp:Status  = job:Exchange_Status
                        tmp:StatusType  = 'E'
                        tmp:DateDespatched  = job:Exchange_Despatched
                    Else !If job:Exchange_Unit_Number <> ''
                        tmp:Status  = job:Current_Status
                        tmp:StatusType  = 'J'
                        tmp:DateDespatched  = job:Date_Despatched
                    End !If job:Exchange_Unit_Number <> ''
                
                Else !If func:CustomerStatus And func:StatusReportType = 0
                    Case func:StatusType
                        Of 0
                            aus:Type        = 'JOB'
                            tmp:Status      = job:Current_Status
                            tmp:StatusType  = 'J'
                        Of 1
                            aus:Type        = 'EXC'
                            tmp:Status      = job:Exchange_Status
                            tmp:StatusType  = 'E'
                        Of 2
                            aus:Type        = 'LOA'
                            tmp:Status      = job:Loan_Status
                            tmp:StatusType  = 'L'
                    End !Case func:StatusType
                End !If func:CustomerStatus And func:StatusReportType = 0
                
                aus:DateChanged = Today()
                Set(aus:DateChangedKey,aus:DateChangedKey)
                Loop
                    If Access:AUDSTATS.PREVIOUS()
                       Break
                    End !If
                    If aus:RefNumber   <> job:Ref_Number      |
                    Or aus:DateChanged > Today()      |
                        Then Break.  ! End If
                    Case func:StatusType
                        Of 0
                            If aus:Type <> 'JOB'
                                Break
                            End !If aus:Type <> 'JOB'
                            If aus:newstatus = job:Current_Status
                                tmp:OldStatus   = aus:OldStatus
                                tmp:StatusDate  = aus:DateChanged
                                tmp:StatusUser = aus:UserCode
                                Break
                            End!If aud:newstatus = job:Current_Status
        
                        Of 1
                            If aus:Type <> 'EXC'
                                Break
                            End !If aus:Type <> 'EXC'
                            If aus:newstatus = job:Exchange_Status
                                tmp:OldStatus   = aus:OldStatus
                                tmp:StatusDate  = aus:DateChanged
                                tmp:StatusUser = aus:UserCode
                                Break
                            End!If aud:newstatus = job:Current_Status
        
                        Of 2
                            If aus:Type <> 'LOA'
                                Break
                            End !If aus:Type <> 'LOA'
                            If aus:newstatus = job:Loan_Status
                                tmp:OldStatus   = aus:OldStatus
                                tmp:StatusDate  = aus:DateChanged
                                tmp:StatusUser = aus:UserCode
                                Break
                            End!If aud:newstatus = job:Current_Status
        
                    End !Case func:StatusType
                End !Loop
                Access:AUDSTATS.RestoreFile(Save_aus_ID)
        
                Case func:StatusReportType
                    Of 0
                        Print(rpt:detail)
                    Of 2
                        Print(rpt:detail1)
                End!Case func:Status
        
        
            End !Loop
            Access:JOBS.RestoreFile(Save_job_ID)
        
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDSTATS.Close
    Relate:DEFAULTS.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  Settarget(Report)
  If GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI') = 1
     ?Colour:Prompt{prop:Text} = Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI'))
  End !If GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If def:HideLocation
      ?job:Location{prop:Hide} = 1
      ?location{prop:Hide} = 1
!      ?LocationTitle{prop:Hide} = 1
!      ?glo:Select5{prop:Hide} = 1
  End !def:HideLocation
  !I'm keeping the boxes visible in design mode, because the report formatter is crap
  ?MainTitle{prop:Boxed} = 0
  ?StatusTitle{prop:Boxed} = 0
  Case func:StatusReportType
      Of 0
          Hide(?StatusTitle)
          Unhide(?MainTitle)
      Of 2
          Unhide(?StatusTitle)
          Hide(?MainTitle)
          
  End!Case func:Status
  Case func:StatusType
      Of 1 !Job
          ?CurrentStatus{prop:Text} = 'Job Status'
          ?CurrentStatus:2{prop:Text} = 'Job Status'
      Of 2 !Exchange
          ?CurrentStatus{prop:Text} = 'Exchange Status'
          ?CurrentStatus:2{prop:Text} = 'Exchange Status'
      Of 3 !Loan
          ?CurrentStatus{prop:Text} = 'Loan Status'
          ?CurrentStatus:2{prop:Text} = 'Loan Status'
  End !func:StatusType
  Settarget()
  
  page_one_temp = 1
  
  count_temp = 0
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Status Report'
  END
  report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'StatusReport',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'StatusReport',1)
    SolaceViewVars('InWorkshopQ:DateValue',InWorkshopQ:DateValue,'StatusReport',1)
    SolaceViewVars('InWorkshopQ:JobRefValue',InWorkshopQ:JobRefValue,'StatusReport',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'StatusReport',1)
    SolaceViewVars('save_aus_id',save_aus_id,'StatusReport',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'StatusReport',1)
    SolaceViewVars('page_one_temp',page_one_temp,'StatusReport',1)
    SolaceViewVars('count_temp',count_temp,'StatusReport',1)
    SolaceViewVars('save_job_id',save_job_id,'StatusReport',1)
    SolaceViewVars('LocalRequest',LocalRequest,'StatusReport',1)
    SolaceViewVars('LocalResponse',LocalResponse,'StatusReport',1)
    SolaceViewVars('FilesOpened',FilesOpened,'StatusReport',1)
    SolaceViewVars('WindowOpened',WindowOpened,'StatusReport',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'StatusReport',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'StatusReport',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'StatusReport',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'StatusReport',1)
    SolaceViewVars('PercentProgress',PercentProgress,'StatusReport',1)
    SolaceViewVars('RecordStatus',RecordStatus,'StatusReport',1)
    SolaceViewVars('EndOfReport',EndOfReport,'StatusReport',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'StatusReport',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'StatusReport',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'StatusReport',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'StatusReport',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'StatusReport',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'StatusReport',1)
    SolaceViewVars('InitialPath',InitialPath,'StatusReport',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'StatusReport',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'StatusReport',1)
    SolaceViewVars('value_temp',value_temp,'StatusReport',1)
    SolaceViewVars('invoice_job_type_temp',invoice_job_type_temp,'StatusReport',1)
    SolaceViewVars('completed_job_type_temp',completed_job_type_temp,'StatusReport',1)
    SolaceViewVars('Date_range_Type_Temp',Date_range_Type_Temp,'StatusReport',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'StatusReport',1)
    SolaceViewVars('job_type_temp',job_type_temp,'StatusReport',1)
    SolaceViewVars('tmp:DespatchType',tmp:DespatchType,'StatusReport',1)
    SolaceViewVars('tmp:custname',tmp:custname,'StatusReport',1)
    SolaceViewVars('tmp:OldStatus',tmp:OldStatus,'StatusReport',1)
    SolaceViewVars('tmp:StatusDate',tmp:StatusDate,'StatusReport',1)
    SolaceViewVars('tmp:StatusUser',tmp:StatusUser,'StatusReport',1)
    SolaceViewVars('tmp:status',tmp:status,'StatusReport',1)
    SolaceViewVars('tmp:StatusType',tmp:StatusType,'StatusReport',1)
    SolaceViewVars('tmp:DateDespatched',tmp:DateDespatched,'StatusReport',1)


BuildCtrlQueue      Routine







