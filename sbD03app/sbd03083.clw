

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03083.INC'),ONCE        !Local module procedure declarations
                     END








Stock_Audit_Report PROCEDURE(Audit_No)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
Local_Stock_Type     STRING(15)
Stock_Type_Filter    STRING(1)
Stock_Type_Filter_Display STRING(20)
Grand_Total_Qty      LONG
Grand_Available_Qty  LONG
Stock_Category_filter STRING(30)
Summary_Filter       STRING(3)
Total_No_Of_Lines    LONG
Suppress_Zero        STRING(1)
BarCode_String       CSTRING(16)
Barcode_IMEI         CSTRING(21)
code_temp            BYTE
Option_temp          BYTE
bar_code_temp_String CSTRING(21)
bar_code_string      CSTRING(21)
Serial_No_Rep        STRING(20)
Audit_Qty            LONG
Line_Cost            REAL
Total_Line           REAL
Total_Audited        LONG
Percentage_Audited   REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
!-----------------------------------------------------------------------------
Process:View         VIEW(STOCK)
                       PROJECT(sto:Description)
                       PROJECT(sto:Location)
                       PROJECT(sto:Part_Number)
                       PROJECT(sto:Purchase_Cost)
                     END
Report               REPORT,AT(396,2802,7521,7990),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,531,7521,1802),USE(?unnamed:2)
                         STRING('STOCK AUDIT REPORT'),AT(4948,573),USE(?String3),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(4948,1302),USE(?String16),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@D6),AT(6302,1302),USE(ReportRunDate),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(4948,1458),USE(?String16:2),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@N3),AT(6250,1458),PAGENO,USE(?PageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6552,1458),USE(?String16:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6719,1458,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                         STRING('String 21'),AT(3656,0,3438,260),USE(?String21),TRN,RIGHT(10),FONT('Arial',14,,FONT:bold)
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:5)
DETAIL                   DETAIL,AT(,,,167),USE(?DetailBand)
                           STRING(@s30),AT(104,0),USE(sto:Part_Number),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s30),AT(2073,0),USE(sto:Description),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@n8),AT(5448,0),USE(Audit_Qty),TRN,RIGHT(1),FONT('Arial',8,,)
                           STRING(@n10.2),AT(5969,10),USE(sto:Purchase_Cost),TRN,RIGHT,FONT('Arial',8,,)
                           STRING(@n10.2),AT(6583,10),USE(Line_Cost),TRN,RIGHT,FONT('Arial',8,,)
                           STRING(@s30),AT(4010,0,1406,208),USE(sto:Location),FONT('Arial',8,,)
                         END
                         FOOTER,AT(0,0,,667),USE(?unnamed:4)
                           LINE,AT(115,42,2146,0),USE(?Line3),COLOR(COLOR:Black)
                           STRING('Totals'),AT(4844,104),USE(?String28),TRN,FONT('Arial',8,,FONT:bold)
                           LINE,AT(5417,73,1833,1),USE(?Line1),COLOR(COLOR:Black)
                           STRING(@n10.2),AT(6615,104),USE(Total_Line),TRN,RIGHT,FONT('Arial',8,,FONT:bold)
                           STRING(@n-10),AT(1615,260),USE(Total_Audited),RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING('Stock Items Audited:'),AT(104,260),USE(?String26),TRN,FONT('Arial',8,,FONT:bold)
                           STRING(@n-10.2),AT(1615,417),USE(Percentage_Audited),RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING('%'),AT(2240,417),USE(?String30),TRN,FONT('Arial',8,,)
                           STRING('Percentage Audited:'),AT(104,417),USE(?String27),TRN,FONT('Arial',8,,FONT:bold)
                           STRING('Total Number Of Lines:'),AT(104,104),USE(?String29),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-10),AT(1625,104),USE(Total_No_Of_Lines),TRN,RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-11),AT(5313,104),USE(Grand_Available_Qty),TRN,RIGHT(1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
detail1                DETAIL,PAGEAFTER(-1),AT(,,,52),USE(?unnamed:6)
                       END
detail2                DETAIL,AT(,,,260),USE(?unnamed:7)
                         STRING('NO ITEMS TO REPORT'),AT(115,42,7094,208),USE(?String25),TRN,CENTER,FONT(,12,,FONT:bold)
                       END
                       FOOTER,AT(396,10833,7521,177),USE(?unnamed:3)
                       END
                       FORM,AT(365,510,7521,10802),USE(?unnamed)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,10802),USE(?Image1)
                         STRING('Stock Code'),AT(135,1979),USE(?String5),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Description'),AT(2104,1979),USE(?String6),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Site Location'),AT(4042,1979),USE(?String31),TRN,FONT('Arial',8,,FONT:bold)
                         STRING('Quantity'),AT(5583,1979),USE(?String8),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Unit Cost'),AT(6167,1979),USE(?String9),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Line Cost'),AT(6771,1979),USE(?String9:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CPCSDummyDetail         SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Stock_Audit_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  CASE MESSAGE(CPCS:AskPrvwDlgText,CPCS:AskPrvwDlgTitle,ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
    OF BUTTON:Yes
      PreviewReq = True
    OF BUTTON:No
      PreviewReq = False
    OF BUTTON:Cancel
       DO ProcedureReturn
  END
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:STOCK.Open
  Relate:DEFAULTS.Open
  Relate:GENSHORT.Open
  Access:STOAUDIT.UseFile
  
  
  RecordsToProcess = BYTES(STOCK)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(STOCK,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      !Set Records For Progress Window
      RecordsToProcess = RECORDS(Stock)*2
      
      !Set Stock File (Stock Type Key)
      
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        !Okay, we need to go through , H, G, and X & Y
        SETTARGET(Report)
        ?String21{PROP:Text} = 'SHORTAGES'
        SETTARGET()
        !Loop not here any more
        Access:Stock.ClearKey(sto:Location_Key)
        SET(sto:Location_Key,sto:Location_Key)
        LOOP
          IF Access:Stock.Next()
            BREAK
          END
          !Got a stock code, let's see if it is +/-!
          !Update Progress Display
          SETTARGET(ProgressWindow)
          DO DisplayProgress
        
          !Counter here to see how many items were actually audited!
          !hang 5!
          Access:StoAudit.ClearKey(STOA:Lock_Down_Key)
          STOA:Stock_Ref_No = sto:Ref_Number
          STOA:Audit_Ref_No = Audit_No
          IF Access:StoAudit.Fetch(STOA:Lock_Down_Key)
            !Error!
          ELSE
            Total_Audited +=1
          END
          !OR if it's just general!
          Access:GenShort.ClearKey(GENS:Lock_Down_Key)
          GENS:Audit_No = Audit_No
          GENS:Stock_Ref_No = sto:Ref_Number
          SET(GENS:Lock_Down_Key,GENS:Lock_Down_Key)
          LOOP
            IF Access:GenShort.Next()
              BREAK
            END
            IF GENS:Audit_No <> Audit_No
              BREAK
            END
            IF GENS:Stock_Ref_No <> sto:Ref_Number
              BREAK
            END
            IF GENS:Stock_Qty > 0
              CYCLE
            END
            !got one?!
            Serial_No_Rep = 'N/A'
            Grand_Available_Qty+=-1*(GENS:Stock_Qty)
            Audit_Qty = -1*(GENS:Stock_Qty)
            Line_Cost = sto:Purchase_Cost * Audit_Qty
            Total_No_Of_Lines += 1
            Total_Line += Line_Cost
            Percentage_Audited = (Total_Audited/RECORDS(Stock))*100
            PRINT(RPT:Detail)
          END
        END
        
        IF Total_No_Of_Lines = 0
          PRINT(RPT:Detail2)
        END
        PRINT(RPT:Detail1)
        Grand_Available_Qty = 0
        Total_No_Of_Lines = 0
        Total_Line = 0
        Total_Audited = 0
        SETTARGET(Report)
        ?String21{PROP:Text} = 'EXCESSES'
        SETTARGET()
        !Other side
        Access:Stock.ClearKey(sto:Location_Key)
        SET(sto:Location_Key,sto:Location_Key)
        LOOP
          IF Access:Stock.Next()
            BREAK
          END
          !Got a stock code, let's see if it is +/-!
          !Update Progress Display
          SETTARGET(ProgressWindow)
          DO DisplayProgress
        
          !Counter here to see how many items were actually audited!
          !hang 5!
          !Look up STO2 file...!
          Access:StoAudit.ClearKey(STOA:Lock_Down_Key)
          STOA:Stock_Ref_No = sto:Ref_Number
          STOA:Audit_Ref_No = Audit_No
          IF Access:StoAudit.Fetch(STOA:Lock_Down_Key)
            !Error!
          ELSE
            Total_Audited +=1
          END
          !OR if it's just general!
          Access:GenShort.ClearKey(GENS:Lock_Down_Key)
          GENS:Audit_No = Audit_No
          GENS:Stock_Ref_No = sto:Ref_Number
          SET(GENS:Lock_Down_Key,GENS:Lock_Down_Key)
          LOOP
            IF Access:GenShort.Next()
              BREAK
            END
            IF GENS:Audit_No <> Audit_No
              BREAK
            END
            IF GENS:Stock_Ref_No <> sto:Ref_Number
              BREAK
            END
            IF GENS:Stock_Qty < 0
              CYCLE
            END
            !got one?!
            Serial_No_Rep = 'N/A'
            Grand_Available_Qty+=(GENS:Stock_Qty)
            Audit_Qty = (GENS:Stock_Qty)
            Line_Cost = sto:Purchase_Cost * Audit_Qty
            Total_Line += Line_Cost
            Total_No_Of_Lines += 1
            Percentage_Audited = (Total_Audited/RECORDS(Stock))*100
            PRINT(RPT:Detail)
          END
        END
        IF Total_No_Of_Lines = 0
          PRINT(RPT:Detail2)
        END
        BREAK
        
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(STOCK,'QUICKSCAN=off').
  IF ~ReportWasOpened
    DO OpenReportRoutine
  END
  IF ~CPCSDummyDetail
    SETTARGET(Report)
    CPCSDummyDetail = LASTFIELD()+1
    CREATE(CPCSDummyDetail,CREATE:DETAIL)
    UNHIDE(CPCSDummyDetail)
    SETPOSITION(CPCSDummyDetail,,,,10)
    CREATE((CPCSDummyDetail+1),CREATE:STRING,CPCSDummyDetail)
    UNHIDE((CPCSDummyDetail+1))
    SETPOSITION((CPCSDummyDetail+1),0,0,0,0)
    (CPCSDummyDetail+1){PROP:TEXT}='X'
    SETTARGET
    PRINT(Report,CPCSDummyDetail)
    LocalResponse=RequestCompleted
  END
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:GENSHORT.Close
    Relate:STOAUDIT.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  !After Opening The Report
  Total_No_Of_Lines = 0
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Stock_Audit_Report'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Stock_Audit_Report',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Stock_Audit_Report',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Stock_Audit_Report',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Stock_Audit_Report',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Stock_Audit_Report',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Stock_Audit_Report',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Stock_Audit_Report',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Stock_Audit_Report',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Stock_Audit_Report',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Stock_Audit_Report',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Stock_Audit_Report',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Stock_Audit_Report',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Stock_Audit_Report',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Stock_Audit_Report',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Stock_Audit_Report',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Stock_Audit_Report',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Stock_Audit_Report',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Stock_Audit_Report',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Stock_Audit_Report',1)
    SolaceViewVars('InitialPath',InitialPath,'Stock_Audit_Report',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Stock_Audit_Report',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Stock_Audit_Report',1)
    SolaceViewVars('Local_Stock_Type',Local_Stock_Type,'Stock_Audit_Report',1)
    SolaceViewVars('Stock_Type_Filter',Stock_Type_Filter,'Stock_Audit_Report',1)
    SolaceViewVars('Stock_Type_Filter_Display',Stock_Type_Filter_Display,'Stock_Audit_Report',1)
    SolaceViewVars('Grand_Total_Qty',Grand_Total_Qty,'Stock_Audit_Report',1)
    SolaceViewVars('Grand_Available_Qty',Grand_Available_Qty,'Stock_Audit_Report',1)
    SolaceViewVars('Stock_Category_filter',Stock_Category_filter,'Stock_Audit_Report',1)
    SolaceViewVars('Summary_Filter',Summary_Filter,'Stock_Audit_Report',1)
    SolaceViewVars('Total_No_Of_Lines',Total_No_Of_Lines,'Stock_Audit_Report',1)
    SolaceViewVars('Suppress_Zero',Suppress_Zero,'Stock_Audit_Report',1)
    SolaceViewVars('BarCode_String',BarCode_String,'Stock_Audit_Report',1)
    SolaceViewVars('Barcode_IMEI',Barcode_IMEI,'Stock_Audit_Report',1)
    SolaceViewVars('code_temp',code_temp,'Stock_Audit_Report',1)
    SolaceViewVars('Option_temp',Option_temp,'Stock_Audit_Report',1)
    SolaceViewVars('bar_code_temp_String',bar_code_temp_String,'Stock_Audit_Report',1)
    SolaceViewVars('bar_code_string',bar_code_string,'Stock_Audit_Report',1)
    SolaceViewVars('Serial_No_Rep',Serial_No_Rep,'Stock_Audit_Report',1)
    SolaceViewVars('Audit_Qty',Audit_Qty,'Stock_Audit_Report',1)
    SolaceViewVars('Line_Cost',Line_Cost,'Stock_Audit_Report',1)
    SolaceViewVars('Total_Line',Total_Line,'Stock_Audit_Report',1)
    SolaceViewVars('Total_Audited',Total_Audited,'Stock_Audit_Report',1)
    SolaceViewVars('Percentage_Audited',Percentage_Audited,'Stock_Audit_Report',1)
    SolaceViewVars('tmp:printedby',tmp:printedby,'Stock_Audit_Report',1)
    SolaceViewVars('tmp:TelephoneNumber',tmp:TelephoneNumber,'Stock_Audit_Report',1)
    SolaceViewVars('tmp:FaxNumber',tmp:FaxNumber,'Stock_Audit_Report',1)


BuildCtrlQueue      Routine












