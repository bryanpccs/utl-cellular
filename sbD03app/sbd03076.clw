

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03076.INC'),ONCE        !Local module procedure declarations
                     END








RepairExchangeReport PROCEDURE(func:StartDate,func:EndDate)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
tmp:ModelCountQueue  QUEUE,PRE()
tmp:ModelNumber      STRING(30)
tmp:Repaired         LONG
tmp:Exchanged        LONG
                     END
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
tmp:DateRange        STRING(25)
tmp:AccountNumber    STRING(30)
tmp:StartDate        DATE
tmp:EndDate          DATE
save_job_id          USHORT,AUTO
tmp:RepairedTotal    LONG
tmp:ExchangedTotal   LONG
tmp:AccountName      STRING(50)
tmp:GrandRepairedTotal LONG
tmp:GrandExchangedTotal LONG
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                     END
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1000),USE(?unnamed)
                         STRING('Date Range:'),AT(5000,156),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s25),AT(5885,156),USE(tmp:DateRange),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(5000,365),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,365),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,573),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,573),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,573),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,781),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,781),PAGENO,USE(?reportpageno),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,781),USE(?string26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6354,781,375,208),USE(?CPCSPgOfPgStr),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,229),USE(?detailband)
                           STRING(@s8),AT(5104,0),USE(tmp:Repaired),TRN,RIGHT
                           STRING(@s8),AT(6354,0),USE(tmp:Exchanged),TRN,RIGHT
                           STRING(@s30),AT(2448,0),USE(mod:Manufacturer),TRN,LEFT
                           STRING(@s30),AT(208,0),USE(tmp:ModelNumber),TRN,LEFT
                         END
TradeTitle               DETAIL,USE(?TradeTitle)
                           STRING('Trade Account: '),AT(208,0),USE(?String29),TRN
                           STRING(@s15),AT(1146,0),USE(sub:Account_Number),TRN,FONT(,,,FONT:bold)
                           LINE,AT(208,208,2031,0),USE(?Line1),COLOR(COLOR:Black)
                         END
TradeTotal               DETAIL,AT(,,,542),USE(?TradeTotal)
                           LINE,AT(5052,52,2031,0),USE(?Line1:2),COLOR(COLOR:Black)
                           STRING(@s8),AT(5104,104),USE(tmp:RepairedTotal),TRN,RIGHT
                           STRING(@s8),AT(6354,104),USE(tmp:ExchangedTotal),TRN,RIGHT
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           STRING(@s8),AT(5104,104),USE(tmp:GrandRepairedTotal),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(6354,104),USE(tmp:GrandExchangedTotal),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING('Total:'),AT(4271,104),USE(?String36),TRN,FONT(,,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('REPAIR/EXCHANGE REPORT'),AT(4063,0,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,1042),USE(tmp:TelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,1198),USE(tmp:FaxNumber),TRN,FONT(,9,,)
                         STRING(@s255),AT(573,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Model Number'),AT(208,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Qty Repaired'),AT(5104,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Qty Exchanged'),AT(6250,2083),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Manufacturer'),AT(2448,2083),USE(?string44:2),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('RepairExchangeReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:TRANTYPE.Open
  Access:SUBTRACC.UseFile
  Access:MODELNUM.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      RecordsToProcess = Records(glo:Queue)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        tmp:GrandExchangedTotal = 0
        tmp:GrandRepairedTotal  = 0
        Loop x# = 1 To Records(glo:Queue)
            Get(glo:Queue,x#)
            RecordsProcessed += 1
            Do DisplayProgress
        
            Clear(tmp:ModelCountQueue)
            Free(tmp:ModelCountQUeue)
            Save_job_ID = Access:JOBS.SaveFile()
            Access:JOBS.ClearKey(job:Date_Booked_Key)
            job:Date_Booked = tmp:StartDate
            Set(job:Date_Booked_Key,job:Date_Booked_Key)
            Loop
                If Access:JOBS.NEXT()
                   Break
                End !If
                If job:Date_Booked > tmp:EndDate      |
                    Then Break.  ! End If
                If job:Account_Number <> glo:Pointer
                    Cycle
                End!If job:Account_Number <> glo:Pointer
        
                !Does this Job's Model Number exists in the second Queue
                glo:Pointer2    = job:Model_Number
                Get(glo:Queue2,glo:Pointer2)
                If ~Error()
                    !If It does, check whether it exists in the temporary table
                    !If it does, add one. If not add a record for that Model Number
                    tmp:ModelNumber = job:Model_Number
                    Get(tmp:ModelCountQueue,tmp:ModelNumber)
                    If Error()
                        tmp:ModelNumber = job:Model_Number
                        If ToBeExchanged()
                            tmp:Exchanged   = 1
                            tmp:Repaired    = 0
                        Else!If ToBeExchanged()
                            tmp:Exchanged   = 0
                            tmp:Repaired    = 1
                        End!If ToBeExchanged()
                        Add(tmp:ModelCountQueue)
                    Else!If Error()
                        If ToBeExchanged()
                            tmp:Exchanged   += 1
                        Else!If ToBeExchanged()
                            tmp:Repaired    += 1
                        End!If ToBeExchanged()
                        Put(tmp:ModelCountQueue)
                    End!If Error()
                End!If ~Error()
            End !Loop
            Access:JOBS.RestoreFile(Save_job_ID)
            If Records(tmp:ModelCountQueue)
                Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                sub:Account_Number  = glo:Pointer
                If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Found
                    tmp:AccountName = Clip(sub:Account_Number) & ' - ' & sub:Company_Name
                Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                
                Print(rpt:TradeTitle)
                tmp:AccountNumber   = glo:Pointer
        
                tmp:ExchangedTotal  = 0
                tmp:RepairedTotal   = 0
                Loop y# = 1 To Records(tmp:ModelCountQueue)
                    Get(tmp:ModelCountQueue,y#)
                    Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                    mod:Model_Number    = tmp:ModelNumber
                    If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                        !Found
        
                    Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        
                    Print(rpt:Detail)
                    tmp:ExchangedTotal += tmp:Exchanged
                    tmp:RepairedTotal  += tmp:Repaired
                End!Loop x# = 1 To Records(tmp:ModelCountQueue)
                tmp:GrandExchangedTotal  += tmp:ExchangedTotal
                tmp:GrandRepairedTotal  += tmp:RepairedTotal
                Print(rpt:TradeTotal)
            End!If Records(tmp:ModelCountQueue)
        End!Loop x# = 1 To Records(glo:Queue)
        
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'c:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:TRANTYPE.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  tmp:StartDate   = func:StartDate
  tmp:EndDate     = func:EndDate
  tmp:DateRange = Format(tmp:StartDate,@d6) & ' - ' & Format(tmp:EndDate,@d6)
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='RepairExchangeReport'
  END
  report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'RepairExchangeReport',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'RepairExchangeReport',1)
    SolaceViewVars('LocalRequest',LocalRequest,'RepairExchangeReport',1)
    SolaceViewVars('tmp:ModelCountQueue:tmp:ModelNumber',tmp:ModelCountQueue:tmp:ModelNumber,'RepairExchangeReport',1)
    SolaceViewVars('tmp:ModelCountQueue:tmp:Repaired',tmp:ModelCountQueue:tmp:Repaired,'RepairExchangeReport',1)
    SolaceViewVars('tmp:ModelCountQueue:tmp:Exchanged',tmp:ModelCountQueue:tmp:Exchanged,'RepairExchangeReport',1)
    SolaceViewVars('LocalResponse',LocalResponse,'RepairExchangeReport',1)
    SolaceViewVars('FilesOpened',FilesOpened,'RepairExchangeReport',1)
    SolaceViewVars('WindowOpened',WindowOpened,'RepairExchangeReport',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'RepairExchangeReport',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'RepairExchangeReport',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'RepairExchangeReport',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'RepairExchangeReport',1)
    SolaceViewVars('PercentProgress',PercentProgress,'RepairExchangeReport',1)
    SolaceViewVars('RecordStatus',RecordStatus,'RepairExchangeReport',1)
    SolaceViewVars('EndOfReport',EndOfReport,'RepairExchangeReport',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'RepairExchangeReport',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'RepairExchangeReport',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'RepairExchangeReport',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'RepairExchangeReport',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'RepairExchangeReport',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'RepairExchangeReport',1)
    SolaceViewVars('InitialPath',InitialPath,'RepairExchangeReport',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'RepairExchangeReport',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'RepairExchangeReport',1)
    SolaceViewVars('tmp:printedby',tmp:printedby,'RepairExchangeReport',1)
    SolaceViewVars('tmp:TelephoneNumber',tmp:TelephoneNumber,'RepairExchangeReport',1)
    SolaceViewVars('tmp:FaxNumber',tmp:FaxNumber,'RepairExchangeReport',1)
    SolaceViewVars('tmp:DateRange',tmp:DateRange,'RepairExchangeReport',1)
    SolaceViewVars('tmp:AccountNumber',tmp:AccountNumber,'RepairExchangeReport',1)
    SolaceViewVars('tmp:StartDate',tmp:StartDate,'RepairExchangeReport',1)
    SolaceViewVars('tmp:EndDate',tmp:EndDate,'RepairExchangeReport',1)
    SolaceViewVars('save_job_id',save_job_id,'RepairExchangeReport',1)
    SolaceViewVars('tmp:RepairedTotal',tmp:RepairedTotal,'RepairExchangeReport',1)
    SolaceViewVars('tmp:ExchangedTotal',tmp:ExchangedTotal,'RepairExchangeReport',1)
    SolaceViewVars('tmp:AccountName',tmp:AccountName,'RepairExchangeReport',1)
    SolaceViewVars('tmp:GrandRepairedTotal',tmp:GrandRepairedTotal,'RepairExchangeReport',1)
    SolaceViewVars('tmp:GrandExchangedTotal',tmp:GrandExchangedTotal,'RepairExchangeReport',1)


BuildCtrlQueue      Routine







