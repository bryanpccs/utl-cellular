

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03001.INC'),ONCE        !Local module procedure declarations
                     END








Courier_Collection_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:PrintedBy        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
jobs_queue           QUEUE,PRE(jobque)
ref_number           REAL
surname              STRING(30)
type                 STRING(3)
                     END
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(30)
job_number_temp      STRING(9)
total_lines_temp     REAL
consignment_number_temp STRING(30)
address_line_temp    STRING(90)
customer_name_temp   STRING(40)
company_name_temp    STRING(30)
telephone_Number_temp STRING(15)
TextBoxLineNdx       SHORT
TextBoxHeight        LONG
TextBoxLines         LONG
TextBoxStartLine     LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:Company_Name_Delivery)
                       PROJECT(job:Postcode_Delivery)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Telephone_Delivery)
                       JOIN(jbn:RefNumberKey,job:Ref_Number)
                         PROJECT(jbn:Delivery_Text)
                       END
                     END
Report               REPORT('Courier Collection Report'),AT(438,2094,10917,4427),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),LANDSCAPE,THOUS
                       HEADER,AT(438,323,10917,1719),USE(?unnamed)
                         STRING(@s30),AT(156,104,3281,260),USE(def:User_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING(@s60),AT(156,365,3281,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s60),AT(156,521,3281,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s60),AT(156,677,3281,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,833,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Date To:'),AT(7396,990),USE(?String22:2),TRN
                         STRING('Tel:'),AT(156,1042),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(625,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?String19),TRN
                         STRING(@s20),AT(625,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING('Date From:'),AT(7396,781),USE(?String22),TRN
                         STRING(@d6b),AT(8646,781),USE(GLO:Select2),TRN,RIGHT,FONT(,,,FONT:bold)
                         STRING(@d6b),AT(8646,990),USE(GLO:Select3),TRN,RIGHT,FONT(,,,FONT:bold)
                         STRING(@n-7),AT(8646,1198),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,,,FONT:bold)
                         STRING(@s255),AT(625,1354,3281,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?String19:2),TRN
                         STRING('Page:'),AT(7396,1198),USE(?PagePrompt),TRN
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:2)
DETAIL                   DETAIL,AT(,,,354),USE(?DetailBand)
                           STRING(@s9),AT(104,0),USE(job:Ref_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@s50),AT(4271,0,3177,156),USE(address_line_temp),TRN,FONT(,8,,)
                           STRING(@s10),AT(7448,0),USE(job:Postcode_Delivery),TRN,LEFT,FONT(,8,,)
                           STRING(@s15),AT(8125,0),USE(job:Telephone_Delivery),TRN,LEFT,FONT(,8,,)
                           TEXT,AT(9115,0,1615,313),USE(jbn:Delivery_Text),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(729,0,1875,208),USE(customer_name_temp),TRN,FONT(,8,,)
                           STRING(@s25),AT(2656,0),USE(job:Company_Name_Delivery),TRN,FONT(,8,,)
                         END
                         FOOTER,AT(438,7000,,656),USE(?unnamed:3),ABSOLUTE
                           STRING('Total Lines: '),AT(7396,208),USE(?String36),TRN
                           STRING(@s9),AT(8646,208),USE(total_lines_temp),TRN,LEFT,FONT(,,,FONT:bold)
                         END
                       END
                       FOOTER,AT(438,6938,10917,708),USE(?unnamed:5)
                         TEXT,AT(104,52,7083,573),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(438,417,10917,7417),USE(?unnamed:4)
                         IMAGE,AT(0,0,10938,7448),USE(?Image1)
                         STRING('COURIER COLLECTION REPORT'),AT(7396,104),USE(?String35),TRN,FONT(,14,,FONT:bold)
                         STRING('Job No'),AT(104,1406),USE(?String17),TRN,FONT(,8,,FONT:bold)
                         STRING('Name'),AT(729,1406),USE(?String18),TRN,FONT(,8,,FONT:bold)
                         STRING('Company Name'),AT(2656,1406),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Address'),AT(4271,1406),USE(?String119),TRN,FONT(,8,,FONT:bold)
                         STRING('Postcode'),AT(7448,1406),USE(?String20),TRN,FONT(,8,,FONT:bold)
                         STRING('Delivery Text'),AT(9115,1406),USE(?String21),TRN,FONT(,8,,FONT:bold)
                         STRING('Telephone No'),AT(8125,1406),USE(?String43),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Courier_Collection_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  CASE MESSAGE(CPCS:AskPrvwDlgText,CPCS:AskPrvwDlgTitle,ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
    OF BUTTON:Yes
      PreviewReq = True
    OF BUTTON:No
      PreviewReq = False
    OF BUTTON:Cancel
       DO ProcedureReturn
  END
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  BIND('GLO:Select2',GLO:Select2)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:STANTEXT.Open
  Relate:TRANTYPE.Open
  Access:JOBNOTES.UseFile
  Access:USERS.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(job:Date_Booked_Key)
      Process:View{Prop:Filter} = |
      'job:date_booked >= GLO:Select1 AND job:date_booked <<= GLO:Select2'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        print# = 0
        If job:workshop <> 'YES'
            access:trantype.clearkey(trt:transit_type_key)
            trt:transit_type = job:transit_type
            if access:trantype.fetch(trt:transit_type_key) = Level:Benign
                If trt:courier_collection_report = 'Y'
                    print# = 1
                End!If trt:courier_collection_report = 'YES'
            end!if access:trantype.fetch(trt:transit_type_key) = Level:Benign
        End!If job:workshop <> 'YES'
        If print# = 1
            if job:title = '' and job:initial = ''
                customer_name_temp = clip(job:surname)
            elsif job:title = '' and job:initial <> ''
                customer_name_temp = clip(job:surname) & ', ' & clip(job:initial)
            elsif job:title <> '' and job:initial = ''
                customer_name_temp = Clip(job:surname) & ', ' & clip(job:title)
            elsif job:title <> '' and job:initial <> ''
                customer_name_temp = Clip(job:surname) & ', ' & clip(job:title) & ' ' & clip(job:initial)
            else
                customer_name_temp = ''
            end
        
            If job:address_line3_delivery = ''
                address_line_temp = Clip(job:address_line1_delivery) & ', ' & Clip(job:address_line2_delivery)
            Else
                address_line_temp = Clip(job:address_line1_delivery) & ', ' & CLip(job:address_line2_delivery) & ', ' & Clip(job:address_line3_delivery)
            End
            total_lines_temp += 1
            tmp:RecordsCount += 1
            Print(rpt:detail)
        End!If print# = 1
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:STANTEXT.Close
    Relate:TRANTYPE.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','COURIER COLLECTION REPORT')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'COURIER COLLECTION REPORT'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvlan.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'COURIER COLLECTION REPORT'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'COURIER COLLECTION REPORT'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Courier_Collection_Report'
  END
  Report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Courier_Collection_Report',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Courier_Collection_Report',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Courier_Collection_Report',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Courier_Collection_Report',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Courier_Collection_Report',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Courier_Collection_Report',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Courier_Collection_Report',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Courier_Collection_Report',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Courier_Collection_Report',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Courier_Collection_Report',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Courier_Collection_Report',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Courier_Collection_Report',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Courier_Collection_Report',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Courier_Collection_Report',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Courier_Collection_Report',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Courier_Collection_Report',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Courier_Collection_Report',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Courier_Collection_Report',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Courier_Collection_Report',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Courier_Collection_Report',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Courier_Collection_Report',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Courier_Collection_Report',1)
    SolaceViewVars('InitialPath',InitialPath,'Courier_Collection_Report',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Courier_Collection_Report',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Courier_Collection_Report',1)
    SolaceViewVars('jobs_queue:ref_number',jobs_queue:ref_number,'Courier_Collection_Report',1)
    SolaceViewVars('jobs_queue:surname',jobs_queue:surname,'Courier_Collection_Report',1)
    SolaceViewVars('jobs_queue:type',jobs_queue:type,'Courier_Collection_Report',1)
    SolaceViewVars('model_number_temp',model_number_temp,'Courier_Collection_Report',1)
    SolaceViewVars('unit_details_temp',unit_details_temp,'Courier_Collection_Report',1)
    SolaceViewVars('esn_temp',esn_temp,'Courier_Collection_Report',1)
    SolaceViewVars('job_number_temp',job_number_temp,'Courier_Collection_Report',1)
    SolaceViewVars('total_lines_temp',total_lines_temp,'Courier_Collection_Report',1)
    SolaceViewVars('consignment_number_temp',consignment_number_temp,'Courier_Collection_Report',1)
    SolaceViewVars('address_line_temp',address_line_temp,'Courier_Collection_Report',1)
    SolaceViewVars('customer_name_temp',customer_name_temp,'Courier_Collection_Report',1)
    SolaceViewVars('company_name_temp',company_name_temp,'Courier_Collection_Report',1)
    SolaceViewVars('telephone_Number_temp',telephone_Number_temp,'Courier_Collection_Report',1)
    SolaceViewVars('TextBoxLineNdx',TextBoxLineNdx,'Courier_Collection_Report',1)
    SolaceViewVars('TextBoxHeight',TextBoxHeight,'Courier_Collection_Report',1)
    SolaceViewVars('TextBoxLines',TextBoxLines,'Courier_Collection_Report',1)
    SolaceViewVars('TextBoxStartLine',TextBoxStartLine,'Courier_Collection_Report',1)


BuildCtrlQueue      Routine







