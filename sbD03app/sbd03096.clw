

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03096.INC'),ONCE        !Local module procedure declarations
                     END


VKEdiExport          PROCEDURE                        ! Declare Procedure
fname                STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
CSVFile FILE,DRIVER('BASIC'),PRE(csv),CREATE,BINDABLE,THREAD
RECORD      RECORD,PRE(csvrec)
LineType           STRING(1)     ! A
JobNumber          STRING(8)     ! B
Model              STRING(30)    ! C
ESN                STRING(20)    ! D
MSN                STRING(20)    ! E
OperationLevel     STRING(30)    ! F Repair Type Warranty Code
SymptomCode        STRING(30)    ! G Job Fault Code 1
RepairCode         STRING(30)    ! H Part Fault Code 1
PartAction         STRING(30)    ! I Part Fault Code 2
PartReference      STRING(30)    ! J
PartQuantity       STRING(8)     ! K
OriginalSWLevel    STRING(30)    ! L Job Fault Code 2
SWLevelAfterRepair STRING(30)    ! M Job Fault Code 3
InWorkshopDate     STRING(8)     ! N
RepairDate         STRING(8)     ! O
DespatchDate       STRING(8)     ! P
! Note: the following totals columns (Q, R, S)
! are not required for the EDI Exort Report -
! they are included in the warranty export (see SBI01APP.DLL)
!TotalLabourValue   STRING(11)    ! Q
!TotalPartsValue    STRING(11)    ! R
!TotalValue         STRING(11)    ! S
           END
        END
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'VKEdiExport')      !Add Procedure to Log
  end


   Relate:JOBS.Open
   Relate:JOBSE.Open
   Relate:REPTYDEF.Open
   Relate:DEFAULTS.Open
    ! Open File
    !savepath = path()
    set(defaults)
    access:defaults.next()

    TheDate" = FORMAT(TODAY(), @d06)
    IF (def:exportpath <> '') THEN
        !fname = Clip(def:exportpath) & '\VKExport.csv'
        fname = Clip(def:exportpath) & '\VK' & TheDate"[1 : 2] & TheDate"[4 : 5] & TheDate"[7 : 10] & '.csv'
    ELSE
        !fname = 'C:\VKExport.csv'
        ! Start Change 4292 BE(18/05/04)
        !fname = 'C:\VK' & TheDate"[1 : 2] & TheDate"[4 : 5] & TheDate"[7 : 10] & 'csv'
        fname = 'C:\VK' & TheDate"[1 : 2] & TheDate"[4 : 5] & TheDate"[7 : 10] & '.csv'
        ! End Change 4292 BE(18/05/04)
    END

    IF (FILEDIALOG('Choose File',fname,'CSV Files|*.CSV|All Files|*.*', |
                      file:keepdir + file:noerror + file:longname)) THEN
        !SETPATH(savepath)
        CSVFile{PROP:Name} = CLIP(fname);
        IF (NOT EXISTS(fname)) THEN
            CREATE(CSVfile)
        END
        OPEN(CSVfile)
        IF (ERRORCODE()) THEN
            MESSAGE(ERROR())
        ELSE
            EMPTY(CSVfile)

            RecordsPerCycle = 25
            RecordsProcessed = 0
            PercentProgress = 0

            OPEN(ProgressWindow)
            Progress:Thermometer = 0
            ?Progress:PctText{Prop:Text} = '0% Completed'
            ?progress:userstring{Prop:Text} = 'VK EDI EXPORT'

            start#=1
            end#=1000

            access:JOBS.clearkey(job:DateCompletedKey)
            job:Date_Completed = glo:select1
            SET(job:DateCompletedKey,job:DateCompletedKey)
            IF (access:jobs.next() = Level:Benign) THEN
                start#=job:ref_number
            END

            access:JOBS.clearkey(job:DateCompletedKey)
            job:Date_Completed = glo:select2
            SET(job:DateCompletedKey,job:DateCompletedKey)
            IF (access:jobs.previous() = Level:Benign) THEN
                end#=job:ref_number
            END

            RecordsToProcess = end#-start#
            IF (RecordsToProcess < 0) THEN
                RecordsToProcess = 1000
            END

            access:JOBS.clearkey(job:DateCompletedKey)
            job:Date_Completed = glo:select1
            SET(job:DateCompletedKey, job:DateCompletedKey)
            LOOP
                IF ((access:JOBS.next() <> Level:Benign) OR(job:Date_Completed > glo:select2)) THEN
                    BREAK
                END

                Do GetNextRecord2
                cancelcheck# += 1
                IF (cancelcheck# > (RecordsToProcess/100)) THEN
                    DO cancelcheck
                    IF (tmp:cancel = 1) THEN
                        BREAK
                    END
                    cancelcheck# = 0
                END

                IF ((job:Manufacturer <> 'VK') OR (job:Warranty_Job <> 'YES')) THEN
                    CYCLE
                END

                DO Export
            END

            CLOSE(ProgressWindow)
            CLOSE(CSVfile)
        END
    END
   Relate:JOBS.Close
   Relate:JOBSE.Close
   Relate:REPTYDEF.Close
   Relate:DEFAULTS.Close
Export            Routine
    CLEAR(csv:RECORD)
    ! Initialise Header Record
    csvrec:LineType = 'H'
    csvrec:JobNumber = job:Ref_Number
    csvrec:Model = job:Model_Number
    csvrec:ESN = job:ESN
    csvrec:MSN = job:MSN

    access:REPTYDEF.clearkey(rtd:Warranty_Key)
    rtd:Warranty = 'YES'
    rtd:Repair_Type = job:Repair_Type_Warranty
    IF (access:REPTYDEF.fetch(rtd:Warranty_Key) = Level:Benign) THEN
        csvrec:OperationLevel = rtd:WarrantyCode
    END

    csvrec:SymptomCode = job:Fault_Code1
    csvrec:OriginalSWLevel = job:Fault_Code2
    csvrec:SWLevelAfterRepair = job:Fault_Code3

    access:JOBSE.clearkey(jobe:RefNumberKey)
    jobe:refNumber = job:Ref_Number
    IF (access:JOBSE.fetch(jobe:RefNumberKey) = Level:Benign) THEN
        csvrec:InWorkshopDate = FORMAT(jobe:InWorkshopDate, @d012)
    END

    ! Start Change 4286 BE(17/05/04)
    !csvrec:RepairDate = FORMAT(job:Date_In_Repair, @d012)
    csvrec:RepairDate = FORMAT(job:Date_Completed, @d012)
    ! End Change 4286 BE(17/05/04)
    csvrec:DespatchDate = FORMAT(job:Date_Despatched, @d012)

    !TotalLabourValue$ = job:Labour_Cost_Warranty
    !TotalPartsValue$ = 0.0
    !TotalValue$ = 0.0

    !csvrec:TotalLabourValue = FORMAT(TotalLabourValue$, @n8.2)

    access:WARPARTS.clearkey(wpr:Part_Number_Key)
    wpr:ref_Number = job:Ref_Number
    SET(wpr:Part_Number_Key, wpr:Part_Number_Key)
    LOOP
        IF ((access:WARPARTS.next() <> Level:Benign) OR (wpr:ref_Number <> job:Ref_Number)) THEN
            BREAK
        END
        csvrec:RepairCode = wpr:Fault_Code1
        csvrec:PartAction = wpr:Fault_Code2
        csvrec:PartReference = wpr:Part_Number
        csvrec:PartQuantity = wpr:quantity

        !TotalPartsValue$ = wpr:Purchase_Cost * wpr:Quantity
        !TotalValue$ = TotalLabourValue$ + TotalPartsValue$

        !csvrec:TotalPartsValue = FORMAT(TotalPartsValue$, @n8.2)
        !csvrec:TotalValue = FORMAT(TotalValue$, @n8.2)

        ADD(CSVFile)

        csvrec:LineType = 'D'
        csvrec:OperationLevel = ''
        csvrec:SymptomCode = ''
        csvrec:OriginalSWLevel = ''
        csvrec:SWLevelAfterRepair = ''
        csvrec:InWorkshopDate = ''
        csvrec:RepairDate = ''
        csvrec:DespatchDate = ''
        csvrec:RepairCode = ''
        csvrec:PartAction = ''
        csvrec:PartReference = ''
        csvrec:PartQuantity = ''
        !csvrec:TotalLabourValue = ''
        !csvrec:TotalPartsValue = ''
        !csvrec:TotalValue = ''

        !TotalLabourValue$ = 0.0
        !TotalPartsValue$ = 0.0
        !TotalValue$ = 0.0
    END

    IF (csvrec:LineType = 'H') THEN
        !csvrec:TotalPartsValue = FORMAT(TotalPartsValue$, @n8.2)
        !csvrec:TotalValue = FORMAT(TotalValue$, @n8.2)
        ADD(CSVFile)
    END
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'VKEdiExport',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('fname',fname,'VKEdiExport',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
