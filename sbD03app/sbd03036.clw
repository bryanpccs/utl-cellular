

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03036.INC'),ONCE        !Local module procedure declarations
                     END








Third_Party_Returns_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_trb_id          USHORT,AUTO
tmp:batchqueue       QUEUE,PRE(trdque)
RecordNumber         LONG
Exchanged            STRING(3)
ModelNumber          STRING(30)
                     END
count_temp           LONG
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(TRDBATCH)
                       PROJECT(trb:Ref_Number)
                       JOIN(job:Ref_Number_Key,trb:Ref_Number)
                         PROJECT(job:Current_Status)
                         PROJECT(job:ESN)
                         PROJECT(job:MSN)
                         PROJECT(job:Manufacturer)
                         PROJECT(job:Model_Number)
                         PROJECT(job:Unit_Type)
                       END
                     END
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,865,7521,1396),USE(?unnamed)
                         STRING('3rd Party Agent:'),AT(5000,156),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,156),USE(GLO:Select3),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Date Range:'),AT(5000,365),USE(?string22:2),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5885,365),USE(GLO:Select1),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Printed By:'),AT(5000,573),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,573),USE(tmp:PrintedBy),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,833),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,833),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,833),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@d6b),AT(6719,365),USE(GLO:Select2),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,1042),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,1042),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,1042),USE(?String26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6354,1042,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                         STRING('To'),AT(6510,365,156,208),USE(?String26:2),TRN,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,229),USE(?detailband)
                           STRING(@s20),AT(1708,0),USE(job:Manufacturer),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s16),AT(208,0),USE(job:ESN),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s20),AT(2813,0),USE(job:Model_Number),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s20),AT(3906,0),USE(job:Unit_Type),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s8),AT(5510,0),USE(trb:Ref_Number),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s25),AT(6021,0),USE(job:Current_Status),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@s11),AT(1094,0),USE(job:MSN),TRN,FONT(,7,,)
                           STRING(@d6),AT(4969,0),USE(jot:DateDespatched),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           LINE,AT(260,52,6979,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Lines:'),AT(260,104),USE(?String38),TRN,FONT(,8,,FONT:bold)
                           STRING(@n-14),AT(1667,104),USE(count_temp),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
CustomerTitle          DETAIL,AT(,,,344),USE(?CustomerTitle)
                         STRING('CUSTOMER UNITS'),AT(260,0),USE(?String40),TRN,FONT(,9,,FONT:bold)
                       END
ExchangedTitle         DETAIL,AT(,,,365),USE(?ExchangeTitle)
                         STRING('EXCHANGED UNITS'),AT(260,0),USE(?String40:2),TRN,FONT(,9,,FONT:bold)
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('3RD PARTY RETURNS NOTE'),AT(4063,52,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING('Unit Type'),AT(3906,2083),USE(?string25:2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Sent'),AT(4969,2083),USE(?string25:3),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Job No'),AT(5510,2083),USE(?string25:4),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Current Status'),AT(6021,2083),USE(?String46),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING(@s255),AT(573,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('I.M.E.I. / E.S.N.'),AT(208,2083),USE(?string44),TRN,FONT(,7,,FONT:bold)
                         STRING('M.S.N.'),AT(1094,2083),USE(?string45),TRN,FONT(,7,,FONT:bold)
                         STRING('Manufacturer'),AT(1708,2083),USE(?string24),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Model Number'),AT(2813,2083),USE(?string25),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Third_Party_Returns_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select2',GLO:Select2)
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:TRDBATCH.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:STANTEXT.Open
  Access:JOBS.UseFile
  Access:JOBTHIRD.UseFile
  
  
  RecordsToProcess = RECORDS(TRDBATCH)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(TRDBATCH,'QUICKSCAN=on')
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      RecordsToProcess    = Records(trdbatch)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        !Add all the models into a queue
        Save_trb_ID = Access:TRDBATCH.SaveFile()
        Access:TRDBATCH.ClearKey(trb:CompanyDateKey)
        trb:Company_Name = glo:Select3
        trb:Status       = 'IN'
        trb:DateReturn   = glo:Select1
        Set(trb:CompanyDateKey,trb:CompanyDateKey)
        Loop
            If Access:TRDBATCH.NEXT()
               Break
            End !If
            If trb:Company_Name <> glo:Select3      |
            Or trb:Status       <> 'IN'      |
            Or trb:DateReturn    > glo:Select2      |
                Then Break.  ! End If
            RecordsProcessed    += 1
            Do DisplayProgress
            trdque:RecordNumber = trb:RecordNumber
            trdque:Exchanged    = trb:Exchanged
        
            access:JOBS.clearkey(job:Ref_Number_Key)
            job:Ref_Number  = trb:Ref_Number
            If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign
                !Found
                trdque:ModelNumber  = job:Model_Number
                Add(tmp:BatchQueue)
            Else! If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign
                !Error
            End! If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign
            
        End !Loop
        Access:TRDBATCH.RestoreFile(Save_trb_ID)
        
        !Resort the queue into Exchanged/Model Number order
        
        Sort(tmp:BatchQueue,trdque:Exchanged,trdque:Modelnumber)
        first_cust# = 1
        first_exch# = 1
        
        RecordsToProcess    = Records(tmp:BatchQueue)
        
        Loop x# = 1 To Records(tmp:BatchQueue)
            Get(tmp:BatchQueue,x#)
            RecordsProcessed    += 1
            Do DisplayProgress
            access:TRDBATCH.clearkey(trb:RecordNumberKey)
            trb:RecordNumber    = trdque:RecordNumber
            If access:TRDBATCH.tryfetch(trb:RecordNumberKey) = Level:Benign
                !Found
                access:JOBS.clearkey(job:Ref_Number_Key)
                job:Ref_Number  = trb:Ref_Number
                If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Found
                Else! If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Error
                End! If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign
        
                access:JOBTHIRD.clearkey(jot:ThirdPartyKey)
                jot:ThirdPartyNumber    = trb:RecordNumber
                If access:JOBTHIRD.tryfetch(jot:ThirdPartyKey) = Level:Benign
                    !Found
                    If trdque:exchanged = 'NO'
                        If first_cust# = 1
                            Print(rpt:customertitle)
                            count_temp = 0
                            first_cust# = 0
                        End!If first# = 1
                        count_temp += 1
                        Print(rpt:detail)
                    End!If trdque:exchanged = 'NO'
                    If trdque:exchanged = 'YES'
                        If first_exch# = 1
                            Print(rpt:exchangedtitle)
                            count_temp = 0
                            first_exch# = 0
                        End!If first_exch# = 1
                        count_temp += 1
                        Print(rpt:detail)
                    End!If trdque:exchanged = 'YES'
                Else! If access:JOBTHIRD.tryfetch(jot:ThirdPartyKey) = Level:Benign
                    !Error
                End! If access:JOBTHIRD.tryfetch(jot:ThirdPartyKey) = Level:Benign
            Else! If access:TRDBATCH.tryfetch(trb:RecordNumberKey) = Level:Benign
                !Error
            End! If access:TRDBATCH.tryfetch(trb:RecordNumberKey) = Level:Benign
        End!Loop x# = 1 To Records(tmp:batchqueue)
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(TRDBATCH,'QUICKSCAN=off').
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBTHIRD.Close
    Relate:STANTEXT.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','3RD PARTY RETURNS NOTE')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = '3RD PARTY RETURNS NOTE'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = '3RD PARTY RETURNS NOTE'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = '3RD PARTY RETURNS NOTE'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Third_Party_Returns_Report'
  END
  report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Third_Party_Returns_Report',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Third_Party_Returns_Report',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Third_Party_Returns_Report',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Third_Party_Returns_Report',1)
    SolaceViewVars('save_trb_id',save_trb_id,'Third_Party_Returns_Report',1)
    SolaceViewVars('tmp:batchqueue:RecordNumber',tmp:batchqueue:RecordNumber,'Third_Party_Returns_Report',1)
    SolaceViewVars('tmp:batchqueue:Exchanged',tmp:batchqueue:Exchanged,'Third_Party_Returns_Report',1)
    SolaceViewVars('tmp:batchqueue:ModelNumber',tmp:batchqueue:ModelNumber,'Third_Party_Returns_Report',1)
    SolaceViewVars('count_temp',count_temp,'Third_Party_Returns_Report',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Third_Party_Returns_Report',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Third_Party_Returns_Report',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Third_Party_Returns_Report',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Third_Party_Returns_Report',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Third_Party_Returns_Report',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Third_Party_Returns_Report',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Third_Party_Returns_Report',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Third_Party_Returns_Report',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Third_Party_Returns_Report',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Third_Party_Returns_Report',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Third_Party_Returns_Report',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Third_Party_Returns_Report',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Third_Party_Returns_Report',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Third_Party_Returns_Report',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Third_Party_Returns_Report',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Third_Party_Returns_Report',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Third_Party_Returns_Report',1)
    SolaceViewVars('InitialPath',InitialPath,'Third_Party_Returns_Report',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Third_Party_Returns_Report',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Third_Party_Returns_Report',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Third_Party_Returns_Report',1)


BuildCtrlQueue      Routine







