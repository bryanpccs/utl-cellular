

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03047.INC'),ONCE        !Local module procedure declarations
                     END








Special_Delivery_Report PROCEDURE(func:Summary,Func:New_only,func:StartTime,func:EndTime)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_job_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
jobs_queue           QUEUE,PRE(jobque)
ref_number           REAL
surname              STRING(30)
type                 STRING(3)
                     END
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(31)
Bar_Code_Temp        CSTRING(31)
Bar_Code2_Temp       CSTRING(31)
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(30)
job_number_temp      STRING(9)
total_lines_temp     REAL
consignment_number_temp STRING(30)
address_line_temp    STRING(90)
customer_name_temp   STRING(40)
tmp:PrintedBy        STRING(60)
tmp:StartTime        TIME
tmp:EndTime          TIME
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ConsignmentQueue     QUEUE,PRE(conque)
ConsignmentNumber    STRING(30)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(COURIER)
                       PROJECT(cou:Account_Number)
                       PROJECT(cou:Address_Line1)
                       PROJECT(cou:Address_Line2)
                       PROJECT(cou:Address_Line3)
                       PROJECT(cou:Courier)
                       PROJECT(cou:Fax_Number)
                       PROJECT(cou:Postcode)
                       PROJECT(cou:Telephone_Number)
                     END
Report               REPORT('Special Delivery Report'),AT(438,2094,10875,4427),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),LANDSCAPE,THOUS
                       HEADER,AT(438,323,10917,1719),USE(?unnamed)
                         STRING(@s30),AT(156,104,3281,260),USE(def:User_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(3750,260),USE(cou:Courier),TRN,FONT(,8,,)
                         STRING(@s60),AT(156,365,3281,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s60),AT(156,521,3281,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(3750,417),USE(cou:Address_Line1),TRN,FONT(,8,,)
                         STRING(@s60),AT(156,677,3281,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,833,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Date To:'),AT(7396,990),USE(?String22:2),TRN
                         STRING(@s30),AT(3750,573),USE(cou:Address_Line2),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(156,990),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,990),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(3750,1042),USE(?String40),TRN,FONT(,8,,)
                         STRING(@s30),AT(3750,729),USE(cou:Address_Line3),TRN,FONT(,8,,)
                         STRING('Fax: '),AT(156,1146),USE(?String19),TRN
                         STRING(@s20),AT(573,1146),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s15),AT(4010,1198),USE(cou:Fax_Number),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(3750,1198),USE(?String42),TRN,FONT(,8,,)
                         STRING(@s15),AT(4010,1042),USE(cou:Telephone_Number),TRN,FONT(,8,,)
                         STRING(@s10),AT(3750,885),USE(cou:Postcode),TRN,FONT(,8,,)
                         STRING('Account No:'),AT(7396,573),USE(?String6),TRN
                         STRING(@s15),AT(8646,573),USE(cou:Account_Number),TRN,FONT(,,,FONT:bold)
                         STRING('Date From:'),AT(7396,781),USE(?String22),TRN
                         STRING(@d6b),AT(8646,781),USE(GLO:Select2),TRN,RIGHT,FONT(,,,FONT:bold)
                         STRING(@d6b),AT(8646,990),USE(GLO:Select3),TRN,RIGHT,FONT(,,,FONT:bold)
                         STRING(@s4),AT(8646,1198),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,,,FONT:bold)
                         STRING(@s255),AT(573,1302,3281,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1302),USE(?String19:2),TRN
                         STRING('Page:'),AT(7396,1198),USE(?PagePrompt),TRN
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:2)
DETAIL                   DETAIL,AT(,,,354),USE(?DetailBand)
                           STRING(@p<<<<<<<<#p),AT(2917,0),USE(job:Ref_Number),TRN,LEFT,FONT(,7,,)
                           STRING(@s90),AT(3542,0,4219,156),USE(address_line_temp),TRN,FONT(,7,,)
                           STRING(@s10),AT(7813,0),USE(job:Postcode_Delivery),TRN,LEFT,FONT(,7,,)
                           STRING(@s20),AT(8490,0,2292,208),USE(Bar_Code_Temp),LEFT,FONT('C39 High 12pt LJ3',12,,)
                           STRING(@s55),AT(156,0,2708,208),USE(customer_name_temp),TRN,FONT(,7,,)
                           STRING(@s20),AT(8490,208),USE(consignment_number_temp),TRN,LEFT,FONT(,7,,)
                         END
                         FOOTER,AT(438,7000,,656),USE(?unnamed:3),ABSOLUTE
                           STRING('Total Lines: '),AT(7396,208),USE(?String36),TRN
                           STRING(@s9),AT(8646,208),USE(total_lines_temp),TRN,LEFT,FONT(,,,FONT:bold)
                         END
                       END
                       FOOTER,AT(438,6938,10917,708),USE(?unnamed:5)
                         TEXT,AT(104,52,7083,573),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(438,417,10917,7417),USE(?unnamed:4)
                         IMAGE('RINVLAN.GIF'),AT(0,0,10938,7448),USE(?Image1)
                         STRING('SPECIAL DELIVERY REPORT'),AT(7396,104),USE(?String35),TRN,FONT(,14,,FONT:bold)
                         STRING('Job No'),AT(2917,1406),USE(?String17),TRN,FONT(,8,,FONT:bold)
                         STRING('Name'),AT(104,1406),USE(?String18),TRN,FONT(,8,,FONT:bold)
                         STRING('Address'),AT(3542,1406),USE(?String119),TRN,FONT(,8,,FONT:bold)
                         STRING('Postcode'),AT(7813,1406),USE(?String20),TRN,FONT(,8,,FONT:bold)
                         STRING('Consignment Note Number'),AT(8490,1406),USE(?String21),TRN,FONT(,8,,FONT:bold)
                         STRING('Signed on behalf of'),AT(104,6198),USE(?signedonbehalf),TRN,HIDE,FONT(,10,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Special_Delivery_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:COURIER.Open
  Relate:DEFAULTS.Open
  Relate:STANTEXT.Open
  Access:JOBS.UseFile
  Access:USERS.UseFile
  Access:AUDIT.UseFile
  
  
  RecordsToProcess = RECORDS(COURIER)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(COURIER,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(cou:Courier_Key)
      Process:View{Prop:Filter} = |
      'UPPER(cou:Courier) = UPPER(GLO:Select1)'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        code_temp = 1
        option_temp = 0
        total_lines_temp = 0
        
        If func:Summary
            SetTarget(Report)
            ?SignedOnBehalf{prop:Hide} = 0
            ?SignedOnBehalf{prop:Text} = 'Signed On Behalf Of ' & Clip(cou:courier) & ': '
            SetTarget()
        End !tmp:Summary
        
        setcursor(cursor:wait)
        save_job_id = access:jobs.savefile()
        access:jobs.clearkey(job:DateDespatchKey)
        job:courier         = cou:courier
        job:date_despatched = glo:select2
        set(job:DateDespatchKey,job:DateDespatchKey)
        loop
            if access:jobs.next()
               break
            end !if
            if job:courier         <> cou:courier      |
            or job:date_despatched > glo:select3      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
        
            !Only include jobs between the selected times.
            !Let's hope there is only one SPECIAL DELIVERY entry
            !in the audit trail
        
            Skip# = 0
            If tmp:StartTime <> '' Or tmp:EndTime <> ''
                Access:Audit.ClearKey(aud:Action_Key)
                aud:Ref_Number = job:Ref_Number
                aud:Action = 'SPECIAL DELIVERY REPORT'
                SET(aud:Action_Key,aud:Action_Key)
                LOOP
                  IF Access:Audit.Next()
                    BREAK
                  END
                  IF aud:Ref_Number <> job:Ref_Number
                    BREAK
                  END
                  IF aud:Action <> 'SPECIAL DELIVERY REPORT'
                    BREAK
                  END
                  If ~Instring('JOB',aud:Notes,1,1)
                    Cycle
                  End !If ~Instring('EXCHANGE',aud:Notes,1,1)
        
                  If aud:Date = job:Date_Despatched
                    IF aud:Time < tmp:StartTime Or aud:Time > tmp:EndTime
                      Skip# = 1
                      Break
                    End !IF aud:Time < tmp:StartTime Or aud:Time > tmp:EndTime
                  End !If aud:Date = job:Date_Despatched
                END
        
            End !If tmp:StartTime <> '' Or tmp:EndTime <> ''
        
            If Skip# = 1
                Cycle
            End !If Skip# = 1
        
        
            !Check!
            IF func:New_only
              h# = 0
              Access:Audit.ClearKey(aud:Action_Key)
              aud:Ref_Number = job:Ref_Number
              aud:Action = 'SPECIAL DELIVERY REPORT'
              SET(aud:Action_Key,aud:Action_Key)
              LOOP
                IF Access:Audit.Next()
                  BREAK
                END
                IF aud:Ref_Number <> job:Ref_Number
                  BREAK
                END
                IF aud:Action <> 'SPECIAL DELIVERY REPORT'
                  BREAK
                END
                  If ~Instring('EXCHANGE',aud:Notes,1,1)
                    Cycle
                  End !If ~Instring('EXCHANGE',aud:Notes,1,1)
        
                h#=1
                BREAK
              END
              IF h# = 1
                CYCLE
              END
            END
        
        
            If func:Summary
                Sort(ConsignmentQueue,conque:ConsignmentNumber)
                conque:ConsignmentNumber    = job:Consignment_Number
                Get(ConsignmentQueue,conque:ConsignmentNumber)
                If Error()
                    conque:ConsignmentNumber    = job:Consignment_Number
                    Add(ConsignmentQueue)
                Else !If Error()
                    Cycle
                End !If Error()
        
            End !If func:Summary
        
            if job:title = '' and job:initial = ''
                customer_name_temp = clip(job:surname)
            elsif job:title = '' and job:initial <> ''
                customer_name_temp = clip(job:surname) & ', ' & clip(job:initial)
            elsif job:title <> '' and job:initial = ''
                customer_name_temp = Clip(job:surname) & ', ' & clip(job:title)
            elsif job:title <> '' and job:initial <> ''
                customer_name_temp = Clip(job:surname) & ', ' & clip(job:title) & ' ' & clip(job:initial)
            else
                customer_name_temp = ''
            end
            Customer_Name_Temp = Clip(Customer_Name_Temp) & ' - ' & Clip(job:Company_Name_Delivery)
        
            bar_code_string_temp = Clip(job:Consignment_number)
            consignment_number_temp = Clip(job:Consignment_number)
            SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
            If job:address_line3_delivery = ''
                address_line_temp = Clip(job:address_line1_delivery) & ', ' & Clip(job:address_line2_delivery)
            Else
                address_line_temp = Clip(job:address_line1_delivery) & ', ' & CLip(job:address_line2_delivery) & ', ' & Clip(job:address_line3_delivery)
            End
            total_lines_temp += 1
            tmp:RecordsCount += 1
            Print(rpt:detail)
            !Successful "Print"
            get(audit,0)
            if access:audit.primerecord() = level:benign
                aud:notes         = 'SPECIAL DELIVERY REPORT INCLUSION - JOB'
                aud:ref_number    = job:ref_number
                aud:date          = today()
                aud:time          = clock()
                aud:type          = 'JOB'
                access:users.clearkey(use:password_key)
                use:password =glo:password
                access:users.fetch(use:password_key)
                aud:user = use:user_code
                aud:action        = 'SPECIAL DELIVERY REPORT'
                access:audit.insert()
            end!�if access:audit.primerecord() = level:benign
        
        
        end !loop
        access:jobs.restorefile(save_job_id)
        setcursor()
        
        setcursor(cursor:wait)
        save_job_id = access:jobs.savefile()
        access:jobs.clearkey(job:DateDespLoaKey)
        job:loan_courier    = cou:courier
        job:loan_despatched = glo:select2
        set(job:DateDespLoaKey,job:DateDespLoaKey)
        loop
            !omemo(jobs)
            if access:jobs.next()
               break
            end !if
            if job:loan_courier    <> cou:courier      |
            or job:loan_despatched > glo:select3      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
        
            !Only include jobs between the selected times.
            !Let's hope there is only one SPECIAL DELIVERY entry
            !in the audit trail
        
            Skip# = 0
            If tmp:StartTime <> '' Or tmp:EndTime <> ''
                Access:Audit.ClearKey(aud:Action_Key)
                aud:Ref_Number = job:Ref_Number
                aud:Action = 'SPECIAL DELIVERY REPORT'
                SET(aud:Action_Key,aud:Action_Key)
                LOOP
                  IF Access:Audit.Next()
                    BREAK
                  END
                  IF aud:Ref_Number <> job:Ref_Number
                    BREAK
                  END
                  IF aud:Action <> 'SPECIAL DELIVERY REPORT'
                    BREAK
                  END
                  If ~Instring('LOAN',aud:Notes,1,1)
                    Cycle
                  End !If ~Instring('EXCHANGE',aud:Notes,1,1)
        
                  If aud:Date = job:Loan_Despatched
                    IF aud:Time < tmp:StartTime Or aud:Time > tmp:EndTime
                      Skip# = 1
                      Break
                    End !IF aud:Time < tmp:StartTime Or aud:Time > tmp:EndTime
                  End !If aud:Date = job:Loan_Despatched
        
                END
        
            End !If tmp:StartTime <> '' Or tmp:EndTime <> ''
        
            If Skip# = 1
                Cycle
            End !If Skip# = 1
        
            IF func:New_only
              h# = 0
              Access:Audit.ClearKey(aud:Action_Key)
              aud:Ref_Number = job:Ref_Number
              aud:Action = 'SPECIAL DELIVERY REPORT'
              SET(aud:Action_Key,aud:Action_Key)
              LOOP
                IF Access:Audit.Next()
                  BREAK
                END
                IF aud:Ref_Number <> job:Ref_Number
                  BREAK
                END
                IF aud:Action <> 'SPECIAL DELIVERY REPORT'
                  BREAK
                END
                  If ~Instring('LOAN',aud:Notes,1,1)
                    Cycle
                  End !If ~Instring('EXCHANGE',aud:Notes,1,1)
        
                h#=1
                BREAK
              END
              IF h# = 1
                CYCLE
              END
            END
        
        
            If func:Summary
                Sort(ConsignmentQueue,conque:ConsignmentNumber)
                conque:ConsignmentNumber    = job:Loan_Consignment_Number
                Get(ConsignmentQueue,conque:ConsignmentNumber)
                If Error()
                    conque:ConsignmentNumber    = job:Loan_Consignment_Number
                    Add(ConsignmentQueue)
                Else !If Error()
                    Cycle
                End !If Error()
        
            End !If func:Summary
        
            if job:title = '' and job:initial = ''
                customer_name_temp = clip(job:surname)
            elsif job:title = '' and job:initial <> ''
                customer_name_temp = clip(job:surname) & ', ' & clip(job:initial)
            elsif job:title <> '' and job:initial = ''
                customer_name_temp = Clip(job:surname) & ', ' & clip(job:title)
            elsif job:title <> '' and job:initial <> ''
                customer_name_temp = Clip(job:surname) & ', ' & clip(job:title) & ' ' & clip(job:initial)
            else
                customer_name_temp = ''
            end
        
            bar_code_string_temp = Clip(job:loan_Consignment_number)
            consignment_number_temp = Clip(job:loan_Consignment_number)
            SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
            If job:address_line3_delivery = ''
                address_line_temp = Clip(job:address_line1_delivery) & ', ' & Clip(job:address_line2_delivery)
            Else
                address_line_temp = Clip(job:address_line1_delivery) & ', ' & CLip(job:address_line2_delivery) & ', ' & Clip(job:address_line3_delivery)
            End
            total_lines_temp += 1
            tmp:RecordsCount += 1
            Print(rpt:detail)
            if access:audit.primerecord() = level:benign
                aud:notes         = 'SPECIAL DELIVERY REPORT INCLUSION - LOAN'
                aud:ref_number    = job:ref_number
                aud:date          = today()
                aud:time          = clock()
                aud:type          = 'JOB'
                access:users.clearkey(use:password_key)
                use:password =glo:password
                access:users.fetch(use:password_key)
                aud:user = use:user_code
                aud:action        = 'SPECIAL DELIVERY REPORT'
                access:audit.insert()
            end!�if access:audit.primerecord() = level:benign
        
        
        end !loop
        access:jobs.restorefile(save_job_id)
        setcursor()
        
        setcursor(cursor:wait)
        
        save_job_id = access:jobs.savefile()
        access:jobs.clearkey(job:DateDespExcKey)
        job:exchange_courier    = cou:courier
        job:exchange_despatched = glo:select2
        set(job:DateDespExcKey,job:DateDespExcKey)
        loop
            !omemo(jobs)
            if access:jobs.next()
               break
            end !if
            if job:exchange_courier    <> cou:courier      |
            or job:exchange_despatched > glo:select3      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
        
            !Only include jobs between the selected times.
            !Let's hope there is only one SPECIAL DELIVERY entry
            !in the audit trail
        
            Skip# = 0
            If tmp:StartTime <> '' Or tmp:EndTime <> ''
                Access:Audit.ClearKey(aud:Action_Key)
                aud:Ref_Number = job:Ref_Number
                aud:Action = 'SPECIAL DELIVERY REPORT'
                SET(aud:Action_Key,aud:Action_Key)
                LOOP
                  IF Access:Audit.Next()
                    BREAK
                  END
                  IF aud:Ref_Number <> job:Ref_Number
                    BREAK
                  END
                  IF aud:Action <> 'SPECIAL DELIVERY REPORT'
                    BREAK
                  END
                  If ~Instring('EXCHANGE',aud:Notes,1,1)
                    Cycle
                  End !If ~Instring('EXCHANGE',aud:Notes,1,1)
        
                  If aud:Date = job:Exchange_Despatched
                    IF aud:Time < tmp:StartTime Or aud:Time > tmp:EndTime
                      Skip# = 1
                      Break
                    End !IF aud:Time < tmp:StartTime Or aud:Time > tmp:EndTime
                  End !If aud:Date = job:Exchange_Despatched
        
                END
        
            End !If tmp:StartTime <> '' Or tmp:EndTime <> ''
        
            If Skip# = 1
                Cycle
            End !If Skip# = 1
        
            IF func:New_only
              h# = 0
              Access:Audit.ClearKey(aud:Action_Key)
              aud:Ref_Number = job:Ref_Number
              aud:Action = 'SPECIAL DELIVERY REPORT'
              SET(aud:Action_Key,aud:Action_Key)
              LOOP
                IF Access:Audit.Next()
                  BREAK
                END
                IF aud:Ref_Number <> job:Ref_Number
                  BREAK
                END
                IF aud:Action <> 'SPECIAL DELIVERY REPORT'
                  BREAK
                END
                  If ~Instring('EXCHANGE',aud:Notes,1,1)
                    Cycle
                  End !If ~Instring('EXCHANGE',aud:Notes,1,1)
        
                h#=1
                BREAK
              END
              IF h# = 1
                CYCLE
              END
            END
        
        
            If func:Summary
                Sort(ConsignmentQueue,conque:ConsignmentNumber)
                conque:ConsignmentNumber    = job:Exchange_Consignment_Number
                Get(ConsignmentQueue,conque:ConsignmentNumber)
                If Error()
                    conque:ConsignmentNumber    = job:Exchange_Consignment_Number
                    Add(ConsignmentQueue)
                Else !If Error()
                    Cycle
                End !If Error()
        
            End !If func:Summary
        
        
            if job:title = '' and job:initial = ''
                customer_name_temp = clip(job:surname)
            elsif job:title = '' and job:initial <> ''
                customer_name_temp = clip(job:surname) & ', ' & clip(job:initial)
            elsif job:title <> '' and job:initial = ''
                customer_name_temp = Clip(job:surname) & ', ' & clip(job:title)
            elsif job:title <> '' and job:initial <> ''
                customer_name_temp = Clip(job:surname) & ', ' & clip(job:title) & ' ' & clip(job:initial)
            else
                customer_name_temp = ''
            end
        
            bar_code_string_temp = Clip(job:exchange_Consignment_number)
            consignment_number_temp = Clip(job:exchange_Consignment_number)
            SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
            If job:address_line3_delivery = ''
                address_line_temp = Clip(job:address_line1_delivery) & ', ' & Clip(job:address_line2_delivery)
            Else
                address_line_temp = Clip(job:address_line1_delivery) & ', ' & CLip(job:address_line2_delivery) & ', ' & Clip(job:address_line3_delivery)
            End
            total_lines_temp += 1
            tmp:RecordsCount += 1
            Print(rpt:detail)
            if access:audit.primerecord() = level:benign
                aud:notes         = 'SPECIAL DELIVERY REPORT INCLUSION - EXCHANGE'
                aud:ref_number    = job:ref_number
                aud:date          = today()
                aud:time          = clock()
                aud:type          = 'JOB'
                access:users.clearkey(use:password_key)
                use:password =glo:password
                access:users.fetch(use:password_key)
                aud:user = use:user_code
                aud:action        = 'SPECIAL DELIVERY REPORT'
                access:audit.insert()
            end!�if access:audit.primerecord() = level:benign
        
        
        end !loop
        access:jobs.restorefile(save_job_id)
        setcursor()
        
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(COURIER,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:STANTEXT.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'COURIER')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  tmp:StartTime   = func:StartTime
  tmp:EndTime     = func:EndTime
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','SPECIAL DELIVERY REPORT')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'SPECIAL DELIVERY REPORT'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvlan.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'SPECIAL DELIVERY REPORT'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'SPECIAL DELIVERY REPORT'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Special_Delivery_Report'
  END
  Report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Special_Delivery_Report',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Special_Delivery_Report',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Special_Delivery_Report',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Special_Delivery_Report',1)
    SolaceViewVars('save_job_id',save_job_id,'Special_Delivery_Report',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Special_Delivery_Report',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Special_Delivery_Report',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Special_Delivery_Report',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Special_Delivery_Report',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Special_Delivery_Report',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Special_Delivery_Report',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Special_Delivery_Report',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Special_Delivery_Report',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Special_Delivery_Report',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Special_Delivery_Report',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Special_Delivery_Report',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Special_Delivery_Report',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Special_Delivery_Report',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Special_Delivery_Report',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Special_Delivery_Report',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Special_Delivery_Report',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Special_Delivery_Report',1)
    SolaceViewVars('InitialPath',InitialPath,'Special_Delivery_Report',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Special_Delivery_Report',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Special_Delivery_Report',1)
    SolaceViewVars('jobs_queue:ref_number',jobs_queue:ref_number,'Special_Delivery_Report',1)
    SolaceViewVars('jobs_queue:surname',jobs_queue:surname,'Special_Delivery_Report',1)
    SolaceViewVars('jobs_queue:type',jobs_queue:type,'Special_Delivery_Report',1)
    SolaceViewVars('code_temp',code_temp,'Special_Delivery_Report',1)
    SolaceViewVars('option_temp',option_temp,'Special_Delivery_Report',1)
    SolaceViewVars('Bar_code_string_temp',Bar_code_string_temp,'Special_Delivery_Report',1)
    SolaceViewVars('Bar_Code_Temp',Bar_Code_Temp,'Special_Delivery_Report',1)
    SolaceViewVars('Bar_Code2_Temp',Bar_Code2_Temp,'Special_Delivery_Report',1)
    SolaceViewVars('model_number_temp',model_number_temp,'Special_Delivery_Report',1)
    SolaceViewVars('unit_details_temp',unit_details_temp,'Special_Delivery_Report',1)
    SolaceViewVars('esn_temp',esn_temp,'Special_Delivery_Report',1)
    SolaceViewVars('job_number_temp',job_number_temp,'Special_Delivery_Report',1)
    SolaceViewVars('total_lines_temp',total_lines_temp,'Special_Delivery_Report',1)
    SolaceViewVars('consignment_number_temp',consignment_number_temp,'Special_Delivery_Report',1)
    SolaceViewVars('address_line_temp',address_line_temp,'Special_Delivery_Report',1)
    SolaceViewVars('customer_name_temp',customer_name_temp,'Special_Delivery_Report',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Special_Delivery_Report',1)
    SolaceViewVars('tmp:StartTime',tmp:StartTime,'Special_Delivery_Report',1)
    SolaceViewVars('tmp:EndTime',tmp:EndTime,'Special_Delivery_Report',1)
    SolaceViewVars('ConsignmentQueue:ConsignmentNumber',ConsignmentQueue:ConsignmentNumber,'Special_Delivery_Report',1)


BuildCtrlQueue      Routine







