

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03005.INC'),ONCE        !Local module procedure declarations
                     END









Status_Report PROCEDURE(f_type,f_order,func:Status,func:StatusType)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
save_aus_id          USHORT,AUTO
tmp:DefaultFax       STRING(20)
page_one_temp        LONG
count_temp           LONG
save_job_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
value_temp           REAL
invoice_job_type_temp STRING(30)
completed_job_type_temp STRING(30)
Date_range_Type_Temp STRING(30)
tmp:PrintedBy        STRING(60)
job_type_temp        STRING(60)
tmp:DespatchType     STRING(60)
tmp:custname         STRING(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:OldStatus        STRING(30)
tmp:StatusDate       DATE
tmp:StatusUser       STRING(3)
tmp:status           STRING(30)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:ESN)
                       PROJECT(job:Engineer)
                       PROJECT(job:Location)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:date_booked)
                     END
report               REPORT('Status Report'),AT(396,2771,7521,8156),PRE(RPT),FONT('Arial',10,,),THOUS
                       HEADER,AT(396,479,7521,1667),USE(?unnamed:2)
                         STRING(@s30),AT(94,52),USE(def:User_Name),TRN,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(94,313,3531,198),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING('Date Printed:'),AT(4948,573),USE(?RunPrompt),TRN,FONT(,8,,)
                         STRING(@d6),AT(5938,573),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(94,469,3531,198),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(94,625,3531,198),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(94,781),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Printed By:'),AT(4948,938,625,208),USE(?String67),TRN,FONT(,8,,)
                         STRING(@s60),AT(5938,938),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Tel: '),AT(94,990),USE(?String15),TRN,FONT(,9,,)
                         STRING(@s20),AT(563,990),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax:'),AT(94,1146),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(563,1146),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(573,1302,3531,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(104,1302),USE(?String16:2),TRN,FONT(,9,,)
                         STRING('Page:'),AT(4948,1302),USE(?PagePrompt),TRN,FONT(,8,,)
                         STRING(@s4),AT(5938,1302),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6302,1302),USE(?String72),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6563,1302,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                       END
break1                 BREAK(EndOfReport)
                         HEADER,PAGEAFTER(-1),AT(0,0,,7552),USE(?unnamed)
                           STRING('REPORT CRITERIA'),AT(1667,885),USE(?String32),TRN,FONT(,9,,FONT:bold)
                           STRING('Job Status:'),AT(1667,1250),USE(?String34),TRN
                           STRING(@s30),AT(3635,1250),USE(GLO:Select2),TRN
                           STRING(@s40),AT(3646,1458),USE(GLO:Select23),TRN
                           STRING(@s40),AT(3646,1667),USE(GLO:Select24),TRN
                           STRING('Exchange Status:'),AT(1667,1458),USE(?String34:2),TRN
                           STRING('Loan Status:'),AT(1667,1667),USE(?String34:3),TRN
                           STRING('Account Number: '),AT(1667,1875),USE(?String35),TRN
                           STRING(@s30),AT(3646,1875),USE(GLO:Select1),TRN
                           STRING(@s40),AT(3646,2083),USE(GLO:Select30),TRN
                           STRING('Main Account Number:'),AT(1667,2083),USE(?String35:2),TRN
                           STRING('Job Priority: '),AT(1667,2292),USE(?String36),TRN
                           STRING(@s30),AT(3646,2292),USE(GLO:Select3),TRN
                           STRING('Workshop: '),AT(1667,2500),USE(?String37),TRN
                           STRING(@s30),AT(3646,2500),USE(GLO:Select4),TRN
                           STRING('Location:'),AT(1667,2708),USE(?LocationTitle),TRN
                           STRING(@s30),AT(3646,2708),USE(GLO:Select5),TRN
                           STRING('Engineer: '),AT(1667,2917),USE(?String39),TRN
                           STRING(@s40),AT(3646,2917,2250,208),USE(GLO:Select6),TRN
                           STRING('Model Number: '),AT(1667,3125),USE(?String40),TRN
                           STRING(@s30),AT(3646,3125),USE(GLO:Select7),TRN
                           STRING('Manufacturer: '),AT(1667,3333),USE(?String41),TRN
                           STRING(@s30),AT(3646,3333),USE(GLO:Select8),TRN
                           STRING('Unit Type: '),AT(1667,3542),USE(?String42),TRN
                           STRING(@s30),AT(3646,3542),USE(GLO:Select9),TRN
                           STRING('Transit Type: '),AT(1667,3750),USE(?String43),TRN
                           STRING(@s30),AT(3646,3750),USE(GLO:Select10),TRN
                           STRING('Charge Type: '),AT(1667,3958),USE(?String44),TRN
                           STRING(@s30),AT(3646,3958),USE(GLO:Select11),TRN
                           STRING(@s40),AT(3646,4167),USE(GLO:Select15),TRN
                           STRING('Chargeable Repair Type:'),AT(1667,4167),USE(?String44:2),TRN
                           STRING('Warranty Charge Type:'),AT(1667,4375),USE(?String70),TRN
                           STRING(@s30),AT(3646,4375),USE(GLO:Select12),TRN
                           STRING(@s40),AT(3646,4583),USE(GLO:Select16),TRN
                           STRING('Warranty Repair Type:'),AT(1667,4583),USE(?String70:2),TRN
                           STRING('Invoice Job Type: '),AT(1667,4792),USE(?String47),TRN
                           STRING(@s30),AT(3646,4792),USE(invoice_job_type_temp),TRN
                           STRING('Completed Job Type:'),AT(1667,5000),USE(?String48),TRN
                           STRING(@s30),AT(3646,5000),USE(completed_job_type_temp),TRN
                           STRING(@s60),AT(3646,5208),USE(job_type_temp),TRN
                           STRING(@s60),AT(3646,5417),USE(tmp:DespatchType),TRN
                           STRING('Job Type:'),AT(1667,5208),USE(?String48:2),TRN
                           STRING('Despatch Type:'),AT(1667,5417),USE(?String48:3),TRN
                           STRING('Date Range Type: '),AT(1667,5635),USE(?String63),TRN
                           STRING(@s30),AT(3646,5635),USE(Date_range_Type_Temp),TRN
                           STRING('Start Date: '),AT(1667,5844),USE(?String65),TRN
                           STRING(@d6b),AT(3646,5844),USE(GLO:Select20),TRN
                           STRING('End Date: '),AT(1667,6052),USE(?String66),TRN
                           STRING(@d6b),AT(3646,6052),USE(GLO:Select21),TRN
                         END
DETAIL                   DETAIL,AT(,,,146),USE(?DetailBand)
                           STRING(@d6b),AT(156,0),USE(job:date_booked),TRN,FONT(,7,,)
                           STRING(@s9),AT(781,0),USE(job:Ref_Number),TRN,LEFT,FONT(,7,,)
                           STRING(@s12),AT(1406,0),USE(job:Account_Number),TRN,LEFT,FONT(,7,,)
                           STRING(@s15),AT(2240,0,990,156),USE(job:Model_Number),TRN,LEFT,FONT(,7,,)
                           STRING(@s20),AT(3333,0,1042,156),USE(tmp:custname),TRN,LEFT,FONT(,7,,)
                           STRING(@s16),AT(4479,0),USE(job:ESN),TRN,LEFT,FONT(,7,,)
                           STRING(@s30),AT(5625,0),USE(tmp:status),TRN,FONT(,7,,)
                         END
detail1                  DETAIL,AT(,,,146),USE(?unnamed:4)
                           STRING(@d6b),AT(156,0),USE(job:date_booked,,?job:date_booked:2),TRN,FONT(,7,,)
                           STRING(@s8),AT(729,0),USE(job:Ref_Number,,?job:Ref_Number:2),TRN,LEFT,FONT(,7,,)
                           STRING(@s15),AT(1250,0,729,156),USE(job:Model_Number,,?job:Model_Number:2),TRN,LEFT,FONT(,7,,)
                           STRING(@s30),AT(5833,0),USE(tmp:status,,?tmp:Status:2),TRN,FONT(,7,,)
                           STRING(@s3),AT(5521,0),USE(tmp:StatusUser),TRN,FONT(,7,,)
                           STRING(@d6),AT(5000,0),USE(tmp:StatusDate,,?tmp:StatusDate:2),TRN,LEFT,FONT(,7,,)
                           STRING(@s30),AT(3385,0),USE(tmp:OldStatus),TRN,FONT(,7,,)
                           STRING(@s3),AT(3125,0),USE(job:Engineer),TRN,FONT(,7,,)
                           STRING(@s20),AT(2031,0),USE(job:Location),TRN,FONT(,7,,)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:3)
                           LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(260,156),USE(?String68),TRN,FONT(,10,,FONT:bold)
                           STRING(@s9),AT(1198,156),USE(count_temp),TRN,FONT(,10,,FONT:bold)
                         END
                       END
                       FORM,AT(396,479,7521,11198)
                         IMAGE,AT(0,0,7521,11198),USE(?Image1)
                         STRING('STATUS REPORT'),AT(5729,52),USE(?String19),TRN,FONT(,14,,FONT:bold)
                         GROUP,AT(52,1927,7396,365),USE(?MainTitle),BOXED
                           STRING('Booked'),AT(156,2083),USE(?String23),TRN,FONT(,7,,FONT:bold)
                           STRING('Job No'),AT(833,2083),USE(?String20),TRN,FONT(,7,,FONT:bold)
                           STRING('Account No'),AT(1406,2083),USE(?String21),TRN,FONT(,7,,FONT:bold)
                           STRING('Model No'),AT(2240,2083),USE(?String25),TRN,FONT(,7,,FONT:bold)
                           STRING('Cust Details'),AT(3333,2083),USE(?String27),TRN,FONT(,7,,FONT:bold)
                           STRING('I.M.E.I. No'),AT(4479,2083),USE(?String29),TRN,FONT(,7,,FONT:bold)
                           STRING('Status'),AT(5625,2083),USE(?CurrentStatus:2),TRN,FONT(,7,,FONT:bold)
                         END
                         GROUP,AT(52,1927,7396,365),USE(?StatusTitle),BOXED
                           STRING('Booked'),AT(156,2083),USE(?String23:2),TRN,FONT(,7,,FONT:bold)
                           STRING('Job No'),AT(781,2083),USE(?String20:2),TRN,FONT(,7,,FONT:bold)
                           STRING('Model No'),AT(1250,2083),USE(?String25:2),TRN,FONT(,7,,FONT:bold)
                           STRING('Location'),AT(2031,2083),USE(?Location),TRN,FONT(,7,,FONT:bold)
                           STRING('Previous Status'),AT(3385,2083),USE(?String31:4),TRN,FONT(,7,,FONT:bold)
                           STRING('Changed'),AT(5000,2083),USE(?String31:3),TRN,FONT(,7,,FONT:bold)
                           STRING('Current Status'),AT(5833,2083),USE(?CurrentStatus),TRN,FONT(,7,,FONT:bold)
                           STRING('User'),AT(5521,2083),USE(?String31:6),TRN,FONT(,7,,FONT:bold)
                           STRING('Eng'),AT(3125,2083),USE(?String31:2),TRN,FONT(,7,,FONT:bold)
                         END
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5504
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Status_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Access:USERS.UseFile
  Access:SUBTRACC.UseFile
  Access:AUDSTATS.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      RecordsToProcess = Records(Jobs)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        save_job_id = access:jobs.savefile()
        Case f_order
            Of 0
                Case f_type
                    Of 'BOOKED'
                        access:jobs.clearkey(job:date_booked_key)
                        job:date_booked = glo:select20
                        set(job:date_booked_key,job:date_booked_key)
        
                    Of 'COMPLETED'
                        access:jobs.clearkey(job:DateCompletedKey)
                        job:date_completed = glo:select20
                        set(job:DateCompletedKey,job:DateCompletedKey)
        
                End!Case f_type
        
            Of 1 !Job number Order
                access:jobs.clearkey(job:ref_number_key)
                Set(job:ref_number_key)
        
        End!Case f_order
        loop
            if access:jobs.next()
               break
            end !if
            RecordsProcessed += 1
            Do DisplayProgress
            Case f_order
                Of 1
                    Case f_type
                        Of 'BOOKED'
                            If job:date_booked < glo:select20 Or job:date_booked > glo:select21
                                Cycle
                            End!If job:date_booked < glo:select20 Or job:date_booked > glo:select21
                        Of 'COMPLETED'
                            If job:date_completed < glo:select20 Or job:date_completed > glo:select21
                                Cycle
                            End!If job:date_completed < glo:select20 Or job:date_completed > glo:select21
                    End!Case f_type
                Of 0
                    Case f_type
                        Of 'BOOKED'
                            if job:date_booked > glo:select21    |
                                then break.  ! end if
        
                        Of 'COMPLETED'
                            if job:date_completed > glo:select21    |
                                then break.  ! end if
        
                    End!Case f_type
            End!Case f_order
        
            print# = 1
            Case glo:select22
                Of 'WAR'
                    If job:chargeable_job = 'YES' Or job:warranty_job <> 'YES'
                        print# = 0
                    End!If job:chargeable_job = 'YES'
                Of 'CHA'
                    IF job:warranty_job = 'YES' Or job:chargeable_job <> 'YES'
                        print# = 0
                    End!IF job:warranty_job = 'YES'
                Of 'SPL'
                    If job:warranty_job <> 'YES' Or job:chargeable_job <> 'YES'
                        print# = 0
                    End!If job:warranty_job <> 'YES' Or job:chargeable_job <> 'YES'
                Of 'WAS'
                    If job:warranty_job <> 'YES'
                        print# = 0
                    End!If job:warranty_job <> 'YES'
                Of 'CHS'
                    If job:chargeable_job <> 'YES'
                        print# = 0
                    End!If job:chargeable_job <> 'YES'
            End!Case glo:select22
        
            If glo:select1 <> ''
                If job:account_number <> glo:select1
                    print# = 0
                End
            End
            If glo:select30 <> ''
                access:subtracc.clearkey(sub:account_number_key)
                sub:account_number = job:account_number
                if access:subtracc.fetch(sub:account_number_key) = level:benign
                    If sub:main_account_number <> glo:select30
                        print# = 0
                    End!If sub:main_account_number <> glo:select30
                Else!if access:subtracc.fetch(sub:account_number_key) = level:benign
                    print# = 0
                end!if access:subtracc.fetch(sub:account_number_key) = level:benign
            End!If glo:select30 <> ''
        
            If glo:select2 <> ''
                If job:current_status <> glo:select2
                    print# = 0
                End
            End
        
            If glo:select3 <> ''
                If JOB:Turnaround_Time <> glo:select3
                    Print# = 0
                End
            End
        
            If glo:select4 <> ''
                If job:workshop <> glo:select4
                    print# = 0
                End
            End
        
            If glo:select5 <> ''
                If job:location <> glo:select5
                    print# = 0
                End
            End
        
            If glo:select6 <> ''
                If job:engineer <> glo:select6
                    print# = 0
                End
            End
        
            IF glo:select7 <> ''
                If job:model_number <> glo:select7
                    print# = 0
                End
            End
        
            If glo:select8 <> ''
                If job:manufacturer <> glo:select8
                    print# = 0
                End
            End
        
            If glo:select9 <> ''
                If job:unit_type <> glo:select9
                    print# = 0
                End
            End
        
            If glo:select10 <> ''
                If job:transit_type <> glo:select10
                    print# = 0
                End
            End
        
            If glo:select11 <> ''
                If job:chargeable_job = 'YES'
                    If job:charge_type <> glo:select11
                        print# = 0
                    End
                Else
                    if glo:select22 = 'CHA'
                        print# = 0
                    End!if glo:select22 = 'CHA'
                End!If job:chargeable_job = 'YES'
            End
        
            If glo:select12 <> ''
                If job:warranty_job = 'YES'
                    If job:warranty_charge_type <> glo:select12
                        print# = 0
                    Else
                        If glo:select22 = 'WAR'
                            print# = 0
                        End!If glo:select22 = 'WAR'
                    End
                End!If job:warranty_job = 'YES'
            End
        
            If glo:select15 <> ''
                If job:chargeable_job = 'YES'
                    If job:repair_type <> glo:select15
                        print# = 0
                    End
                Else!If job:chargeable_job = 'YES'
                    If glo:select22 = 'CHA'
                        print# = 0
                    End!If job:select22 = 'CHA'
                End!If job:chargeable_job = 'YES'
            End
        
            If glo:select16 <> ''
                If job:warranty_job = 'YES'
                    If job:repair_type_warranty <> glo:select16
                        print# = 0
                    End
                Else!If job:warranty_job = 'YES'
                    If glo:select22 = 'WAR'
                        print# = 0
                    End!If glo:select22 = 'WAR'
                End!If job:warranty_job = 'YES'
            End
        
            Case glo:select17
                Of 'INV'
                    Case glo:select22
                        Of 'WAR'
                            If job:invoice_number_warranty = ''
                                print# = 0
                            End!If job:invoice_number_warranty = ''
                        Of 'CHA'
                            If job:invoice_number = ''
                                print# = 0
                            End
                        Else
                            If (job:chargeable_job = 'YES' and job:invoice_number = '') Or |
                                (job:warranty_job = 'YES' and job:invoice_number_warranty = '')
                                print# = 0
                            End!If job:invoice_number = '' Of job:invoice_Number_warranty = ''
                    End!Case glo:select22
                Of 'NOT'
                    Case glo:select22
                        Of 'WAR'
                            If job:invoice_number_warranty <> ''
                                print# = 0
                            End!If job:invoice_number_warranty = ''
                        Of 'CHA'
                            If job:invoice_number <> ''
                                print# = 0
                            End
                        Else
                            If (job:chargeable_job = 'YES' and job:invoice_number <> '') Or |
                                (job:warranty_job = 'YES' and job:invoice_number_warranty <> '')
                                print# = 0
                            End!If job:invoice_number = '' Of job:invoice_Number_warranty = ''
                    End!Case glo:select22
            End
        
            Case glo:select19
                Of 'DES'
                    If job:consignment_number = ''
                        Print# = 0
                    End!If job:consignment_number = ''
                Of 'NOT'
                    If job:consignment_number <> ''
                        Print# = 0
                    End!If job:consignment_number <> ''
            End!Case glo:select19
        
            Case glo:select18
                Of 'COM'
                    If job:date_completed = ''
                        print# = 0
                    End
                Of 'NOT'
                    If job:date_completed <> ''
                        print# = 0
                    End
            End
        
            If glo:select23 <> ''
                If job:exchange_status <> glo:select23
                    print# = 0
                End!If job:exchange_status <> glo:select23
            End!If glo:select23
        
            If glo:select24 <> ''
                If job:loan_status <> glo:select24
                    print# = 0
                End!If job:loan_status <> glo:select24
            End!If glo:select24 <> ''
        
            If print# = 1
                count_temp += 1
                tmp:RecordsCount += 1
                If job:surname  = ''
                    access:subtracc.clearkey(sub:account_number_key)
                    sub:account_number = job:account_number
                    if access:subtracc.fetch(sub:account_number_key) = level:benign
                        access:tradeacc.clearkey(tra:account_number_key) 
                        tra:account_number = sub:main_account_number
                        if access:tradeacc.fetch(tra:account_number_key) = level:benign
                            if tra:use_sub_accounts = 'YES'
                                tmp:custname    = sub:company_name
                            else!if tra:use_sub_accounts = 'YES'
                                tmp:custname    = tra:company_name
                            end!if tra:use_sub_accounts = 'YES'
                        end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                    end!if access:subtracc.fetch(sub:account_number_key) = level:benign  
                Else!If job:surname  = ''
                    tmp:custname    = job:surname
                End!If job:surname  = ''
        
                tmp:OldStatus   = ''
                tmp:StatusDate  = ''
                tmp:StatusUser = ''
                tmp:Status  = ''
        
                Save_aus_ID = Access:AUDSTATS.SaveFile()
                Access:AUDSTATS.ClearKey(aus:DateChangedKey)
                aus:RefNumber   = job:Ref_Number
                Case func:StatusType
                    Of 1
                        aus:Type        = 'JOB'
                        tmp:Status      = job:Current_Status
                    Of 2
                        aus:Type        = 'EXC'
                        tmp:Status      = job:Exchange_Status
                    Of 3
                        aus:Type        = 'LOA'
                        tmp:Status      = job:Loan_Status
                End !Case func:StatusType
                
                aus:DateChanged = Today()
                Set(aus:DateChangedKey,aus:DateChangedKey)
                Loop
                    If Access:AUDSTATS.PREVIOUS()
                       Break
                    End !If
                    If aus:RefNumber   <> job:Ref_Number      |
                    Or aus:DateChanged > Today()      |
                        Then Break.  ! End If
                    Case func:StatusType
                        Of 1
                            If aus:Type <> 'JOB'
                                Break
                            End !If aus:Type <> 'JOB'
                            If aus:newstatus = job:Current_Status
                                tmp:OldStatus   = aus:OldStatus
                                tmp:StatusDate  = aus:DateChanged
                                tmp:StatusUser = aus:UserCode
                                Break
                            End!If aud:newstatus = job:Current_Status
        
                        Of 2
                            If aus:Type <> 'EXC'
                                Break
                            End !If aus:Type <> 'EXC'
                            If aus:newstatus = job:Exchange_Status
                                tmp:OldStatus   = aus:OldStatus
                                tmp:StatusDate  = aus:DateChanged
                                tmp:StatusUser = aus:UserCode
                                Break
                            End!If aud:newstatus = job:Current_Status
        
                        Of 3
                            If aus:Type <> 'LOA'
                                Break
                            End !If aus:Type <> 'LOA'
                            If aus:newstatus = job:Loan_Status
                                tmp:OldStatus   = aus:OldStatus
                                tmp:StatusDate  = aus:DateChanged
                                tmp:StatusUser = aus:UserCode
                                Break
                            End!If aud:newstatus = job:Current_Status
        
                    End !Case func:StatusType
                End !Loop
                Access:AUDSTATS.RestoreFile(Save_aus_ID)
        
                Case func:Status
                    Of 0
                        Print(rpt:detail)
                    Of 1
                        Print(rpt:detail1)
                End!Case func:Status
                
            End
        
        end !loop
        access:jobs.restorefile(save_job_id)
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDSTATS.Close
    Relate:DEFAULTS.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  Case glo:select17
      Of 'INV'
          invoice_job_type_temp = 'Invoiced Jobs Only'
      Of 'NOT'
          invoice_job_type_temp = 'Uninvoiced Jobs Only'
      ELse
          invoice_job_type_temp = 'Invoiced & Uninvoiced Jobs'
  End
  
  Case glo:select18
      Of 'COM'
          completed_job_type_temp = 'Completed Jobs Only'
      Of 'NOT'
          completed_job_type_temp = 'Incomplete Jobs Only'
      Else
          completed_job_type_temp = 'Completed & Incomplete Jobs'
  End
  Case glo:select19
      Of 'DES'
          tmp:DespatchType    = 'Despatched Jobs Only'
      Of 'NOT'
          tmp:DespatchType    = 'Not Despatched Jobs Only'
      Else
          tmp:DespatchType    = 'Despatched & Not Despatched Jobs'
  End!Case glo:select19
  
  Case glo:select22
      Of 'WAR'
          job_Type_temp = 'Warranty Jobs Only'
      Of 'CHA'
          job_type_temp   = 'Chargeable Jobs Only'
      Of 'CHS'
          job_type_temp   = 'Chargeable Jobs (Inc Split)'
      Of 'WAS'
          job_type_temp   = 'Warranty Jobs (Inc Split)'
      Of 'SPL'
          job_type_temp   = 'Split Jobs Only'
      Else
          job_type_temp   = 'Warranty & Chargeable Jobs'
  End!Case glo:select22
  
  Case f_type
      Of 'BOOKED'
          date_range_type_temp = 'Booking Date'
      Of 'COMPLETED'
          date_range_type_temp = 'Completed Date'
  End
  
  Settarget(Report)
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If def:HideLocation
      ?job:Location{prop:Hide} = 1
      ?location{prop:Hide} = 1
      ?LocationTitle{prop:Hide} = 1
      ?glo:Select5{prop:Hide} = 1
  End !def:HideLocation
  !I'm keeping the boxes visible in design mode, because the report formatter is crap
  ?MainTitle{prop:Boxed} = 0
  ?StatusTitle{prop:Boxed} = 0
  Case func:Status
      Of 0
          Hide(?StatusTitle)
          Unhide(?MainTitle)
      Of 1
          Unhide(?StatusTitle)
          Hide(?MainTitle)
          
  End!Case func:Status
  Case func:StatusType
      Of 1 !Job
          ?CurrentStatus{prop:Text} = 'Job Status'
          ?CurrentStatus:2{prop:Text} = 'Job Status'
      Of 2 !Exchange
          ?CurrentStatus{prop:Text} = 'Exchange Status'
          ?CurrentStatus:2{prop:Text} = 'Exchange Status'
      Of 3 !Loan
          ?CurrentStatus{prop:Text} = 'Loan Status'
          ?CurrentStatus:2{prop:Text} = 'Loan Status'
  End !func:StatusType
  Settarget()
  
  page_one_temp = 1
  
  count_temp = 0
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Status Report'
  END
  report{Prop:Preview} = PrintPreviewImage




SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Status_Report',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Status_Report',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Status_Report',1)
    SolaceViewVars('save_aus_id',save_aus_id,'Status_Report',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Status_Report',1)
    SolaceViewVars('page_one_temp',page_one_temp,'Status_Report',1)
    SolaceViewVars('count_temp',count_temp,'Status_Report',1)
    SolaceViewVars('save_job_id',save_job_id,'Status_Report',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Status_Report',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Status_Report',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Status_Report',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Status_Report',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Status_Report',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Status_Report',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Status_Report',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Status_Report',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Status_Report',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Status_Report',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Status_Report',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Status_Report',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Status_Report',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Status_Report',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Status_Report',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Status_Report',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Status_Report',1)
    SolaceViewVars('InitialPath',InitialPath,'Status_Report',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Status_Report',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Status_Report',1)
    SolaceViewVars('value_temp',value_temp,'Status_Report',1)
    SolaceViewVars('invoice_job_type_temp',invoice_job_type_temp,'Status_Report',1)
    SolaceViewVars('completed_job_type_temp',completed_job_type_temp,'Status_Report',1)
    SolaceViewVars('Date_range_Type_Temp',Date_range_Type_Temp,'Status_Report',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Status_Report',1)
    SolaceViewVars('job_type_temp',job_type_temp,'Status_Report',1)
    SolaceViewVars('tmp:DespatchType',tmp:DespatchType,'Status_Report',1)
    SolaceViewVars('tmp:custname',tmp:custname,'Status_Report',1)
    SolaceViewVars('tmp:OldStatus',tmp:OldStatus,'Status_Report',1)
    SolaceViewVars('tmp:StatusDate',tmp:StatusDate,'Status_Report',1)
    SolaceViewVars('tmp:StatusUser',tmp:StatusUser,'Status_Report',1)
    SolaceViewVars('tmp:status',tmp:status,'Status_Report',1)


BuildCtrlQueue      Routine







