

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03067.INC'),ONCE        !Local module procedure declarations
                     END








Unallocated_Jobs_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:printedby        STRING(60)
tmp:telephonenumber  STRING(20)
tmp:faxnumber        STRING(20)
tmp:accountno        STRING(30)
tmp:accountname      STRING(20)
tmp:modelno          STRING(30)
tmp:defaulttelephone STRING(20)
tmp:defaultfax       STRING(20)
tmp:count            LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:ESN)
                       PROJECT(job:Engineer)
                       PROJECT(job:Location)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:Workshop)
                       PROJECT(job:date_booked)
                     END
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,906,7521,1469),USE(?unnamed)
                         STRING('Account No:'),AT(5000,156),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,156),USE(tmp:accountno),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(5885,365),USE(tmp:modelno),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Model No:'),AT(5000,365),USE(?string22:2),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(5010,573),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5896,573),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5010,781),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5896,781),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6521,781),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5010,990),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5896,990),PAGENO,USE(?reportpageno),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6156,990),USE(?string26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6354,990,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,146),USE(?detailband)
                           STRING(@s8),AT(52,0),USE(job:Ref_Number),TRN,RIGHT(1),FONT(,7,,,CHARSET:ANSI)
                           STRING(@s16),AT(1563,0),USE(job:ESN),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s15),AT(2448,0),USE(job:Account_Number),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s20),AT(5208,0),USE(job:Model_Number),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s20),AT(6302,0),USE(job:Unit_Type),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s18),AT(573,0),USE(job:Location),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s20),AT(3281,0,1042,156),USE(tmp:accountname),TRN,FONT(,7,,,CHARSET:ANSI)
                           STRING(@d6b),AT(4583,0),USE(job:date_booked),TRN,FONT(,7,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           LINE,AT(167,52,7188,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total:'),AT(125,104),USE(?String40),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(573,104),CNT,RESET(endofreportbreak),USE(tmp:count),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('UNALLOCATED JOBS REPORT'),AT(4063,0,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:telephonenumber),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:faxnumber),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Job No'),AT(125,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('E.S.N.'),AT(1563,2083),USE(?string44:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Account No'),AT(2448,2083),USE(?string44:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Name'),AT(3281,2083),USE(?string44:4),TRN,FONT(,8,,FONT:bold)
                         STRING('Booked'),AT(4667,2083),USE(?string44:5),TRN,FONT(,8,,FONT:bold)
                         STRING('Model No'),AT(5208,2083),USE(?string44:6),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(6302,2083),USE(?string44:7),TRN,FONT(,8,,FONT:bold)
                         STRING('Location'),AT(573,2083),USE(?Location),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Unallocated_Jobs_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(job:EngWorkKey)
      Process:View{Prop:Filter} = |
      'Upper(job:engineer) = '''' And  Upper(job:workshop) = ''YES'''
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        print# = 1
        If glo:select1  = 1 and job:account_number <> glo:select2
            print# = 0
        End!If glo:select1  = 1 and job:account_number <> glo:select2
        
        If glo:select3  = 1 And job:model_number <> glo:select4 and print# = 1
            print# = 0
        End!If glo:select3  = 1 And job:model_number <> glo:select4 and print# = 1
        If print#   = 1
            access:subtracc.clearkey(sub:account_number_key)
            sub:account_number = job:account_number
            if access:subtracc.fetch(sub:account_number_key) = level:benign
                access:tradeacc.clearkey(tra:account_number_key) 
                tra:account_number = sub:main_account_number
                if access:tradeacc.fetch(tra:account_number_key) = level:benign
                    if tra:use_sub_accounts = 'YES'
                        tmp:accountname    = sub:company_name
                    else!if tra:use_sub_accounts = 'YES'
                        tmp:accountname    = tra:company_name
                    end!if tra:use_sub_accounts = 'YES'
                end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
            end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
            Print(rpt:detail)
        End!If print#   = 1
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBS.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  Case glo:select1
      Of 1
          tmp:accountno   = glo:select2
      Of 0
          tmp:accountno   = 'ALL'
  End!Case glo:select1
  Case glo:select3
      Of 1
          tmp:modelno     = glo:select4
      Of 0
          tmp:modelno     = 'ALL'
  End!Case glo:select3
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','UNALLOCATED JOBS REPORT')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'UNALLOCATED JOBS REPORT'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If def:HideLocation
      ?job:Location{prop:hide} = 1
      ?Location{prop:Hide} = 1
  End !def:HideLocation
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'UNALLOCATED JOBS REPORT'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'UNALLOCATED JOBS REPORT'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Unallocated_Jobs_Report'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Unallocated_Jobs_Report',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Unallocated_Jobs_Report',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Unallocated_Jobs_Report',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Unallocated_Jobs_Report',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Unallocated_Jobs_Report',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Unallocated_Jobs_Report',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Unallocated_Jobs_Report',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Unallocated_Jobs_Report',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Unallocated_Jobs_Report',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Unallocated_Jobs_Report',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Unallocated_Jobs_Report',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Unallocated_Jobs_Report',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Unallocated_Jobs_Report',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Unallocated_Jobs_Report',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Unallocated_Jobs_Report',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Unallocated_Jobs_Report',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Unallocated_Jobs_Report',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Unallocated_Jobs_Report',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Unallocated_Jobs_Report',1)
    SolaceViewVars('InitialPath',InitialPath,'Unallocated_Jobs_Report',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Unallocated_Jobs_Report',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Unallocated_Jobs_Report',1)
    SolaceViewVars('tmp:printedby',tmp:printedby,'Unallocated_Jobs_Report',1)
    SolaceViewVars('tmp:telephonenumber',tmp:telephonenumber,'Unallocated_Jobs_Report',1)
    SolaceViewVars('tmp:faxnumber',tmp:faxnumber,'Unallocated_Jobs_Report',1)
    SolaceViewVars('tmp:accountno',tmp:accountno,'Unallocated_Jobs_Report',1)
    SolaceViewVars('tmp:accountname',tmp:accountname,'Unallocated_Jobs_Report',1)
    SolaceViewVars('tmp:modelno',tmp:modelno,'Unallocated_Jobs_Report',1)
    SolaceViewVars('tmp:defaulttelephone',tmp:defaulttelephone,'Unallocated_Jobs_Report',1)
    SolaceViewVars('tmp:defaultfax',tmp:defaultfax,'Unallocated_Jobs_Report',1)
    SolaceViewVars('tmp:count',tmp:count,'Unallocated_Jobs_Report',1)


BuildCtrlQueue      Routine







