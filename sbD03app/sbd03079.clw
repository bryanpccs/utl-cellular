

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03079.INC'),ONCE        !Local module procedure declarations
                     END








CustomerEnquiryReport PROCEDURE(func:StartDate,func:EndDate,func:DespatchDays)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:CustName         STRING(30)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
tmp:JobType          STRING(1)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:IMEI             STRING(16)
tmp:Count            LONG
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:Current_Status)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:date_booked)
                     END
report               REPORT('Status Report'),AT(396,2771,7521,8156),PRE(RPT),FONT('Arial',10,,),THOUS
                       HEADER,AT(396,479,7521,1667),USE(?unnamed:2)
                         STRING(@s30),AT(94,52),USE(def:User_Name),TRN,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(94,313,3531,198),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING('Date Printed:'),AT(4948,573),USE(?RunPrompt),TRN,FONT(,8,,)
                         STRING(@d6),AT(5938,573),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(94,469,3531,198),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(94,625,3531,198),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(94,781),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Printed By:'),AT(4948,938,625,208),USE(?String67),TRN,FONT(,8,,)
                         STRING(@s60),AT(5938,938),USE(tmp:printedby),TRN,FONT(,8,,FONT:bold)
                         STRING('Tel: '),AT(94,990),USE(?String15),TRN,FONT(,9,,)
                         STRING(@s20),AT(563,990),USE(tmp:TelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax:'),AT(94,1146),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(563,1146),USE(tmp:FaxNumber),TRN,FONT(,9,,)
                         STRING('Email:'),AT(104,1302),USE(?String16:2),TRN,FONT(,9,,)
                         STRING(@s255),AT(573,1302,3531,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Page:'),AT(4948,1302),USE(?PagePrompt),TRN,FONT(,8,,)
                         STRING(@s4),AT(5938,1302),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6302,1302),USE(?String72),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6563,1302,375,208),USE(?CPCSPgOfPgStr),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
break1                 BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,156),USE(?DetailBand)
                           STRING(@d6b),AT(156,0),USE(job:date_booked),TRN,FONT(,7,,)
                           STRING(@s9),AT(781,0),USE(job:Ref_Number),TRN,RIGHT,FONT(,7,,)
                           STRING(@s1),AT(1510,0),USE(tmp:JobType),TRN,LEFT,FONT(,7,,)
                           STRING(@s20),AT(1771,0,833,156),USE(tmp:CustName),TRN,LEFT,FONT(,7,,)
                           STRING(@s16),AT(2760,0),USE(tmp:IMEI),TRN,LEFT,FONT(,7,,)
                           STRING(@s28),AT(3750,0),USE(job:Current_Status),TRN,FONT(,7,,)
                           STRING(@s15),AT(5313,0),USE(job:Unit_Type),TRN,FONT(,7,,)
                           STRING(@s30),AT(6198,0),USE(job:Order_Number),TRN,FONT(,7,,)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:3)
                           LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(260,156),USE(?String68),TRN,FONT(,10,,FONT:bold)
                           STRING(@s9),AT(1198,156),USE(tmp:Count),TRN,FONT(,10,,FONT:bold)
                         END
                       END
                       FORM,AT(396,479,7521,11198)
                         IMAGE,AT(0,0,7521,11198),USE(?Image1)
                         STRING('CUSTOMER ENQUIRY REPORT'),AT(4427,52),USE(?String19),TRN,FONT(,14,,FONT:bold)
                         STRING('Job No'),AT(906,2083),USE(?String20),TRN,FONT(,8,,FONT:bold)
                         STRING('Type'),AT(1406,2083),USE(?String21),TRN,FONT(,8,,FONT:bold)
                         STRING('Cust Name'),AT(1771,2083),USE(?String27),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(5313,2083),USE(?String27:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Order Number'),AT(6198,2083),USE(?String27:3),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. No'),AT(2760,2083),USE(?String29),TRN,FONT(,8,,FONT:bold)
                         STRING('Status'),AT(3750,2083),USE(?String31),TRN,FONT(,8,,FONT:bold)
                         STRING('Booked'),AT(240,2083),USE(?String23),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('CustomerEnquiryReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('tmp:StartDate',tmp:StartDate)
  BIND('tmp:EndDate',tmp:EndDate)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  ! Before Embed Point: %BeforeFileOpen) DESC(Beginning of Procedure, Before Opening Files) ARG()
  tmp:StartDate   = func:StartDate
  tmp:EndDate     = func:EndDate
  ! After Embed Point: %BeforeFileOpen) DESC(Beginning of Procedure, Before Opening Files) ARG()
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Access:USERS.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(job:Date_Booked_Key)
      Process:View{Prop:Filter} = |
      'job:date_booked >= tmp:StartDate AND job:date_booked <<= tmp:EndDate'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        error# = 0
        print# = 1
        glo:Pointer = job:Account_Number
        Get(glo:Queue,glo:Pointer)
        If Error()
            error# = 1
        End!If Error()
        
        If error# = 0
            If job:surname  = ''
                access:subtracc.clearkey(sub:account_number_key)
                sub:account_number = job:account_number
                if access:subtracc.fetch(sub:account_number_key) = level:benign
                    access:tradeacc.clearkey(tra:account_number_key) 
                    tra:account_number = sub:main_account_number
                    if access:tradeacc.fetch(tra:account_number_key) = level:benign
                        if tra:use_sub_accounts = 'YES'
                            tmp:custname    = sub:company_name
                        else!if tra:use_sub_accounts = 'YES'
                            tmp:custname    = tra:company_name
                        end!if tra:use_sub_accounts = 'YES'
                    end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                end!if access:subtracc.fetch(sub:account_number_key) = level:benign  
            Else!If job:surname  = ''
                tmp:custname    = job:surname
            End!If job:surname  = ''
        
            tmp:IMEI    = job:ESN
            tmp:JobType = 'J'
            If job:Exchange_Unit_Number <> ''
                Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                xch:Ref_Number  = job:Exchange_Unit_Number
                If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    !Found
                    tmp:IMEI    = xch:ESN
                    tmp:JobType = 'E'
                Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            End!If job:Exchange_Unit_Number <> ''
        
            If job:Date_Despatched <> ''
                If job:Date_Despatched < Today() - func:DespatchDays
                    print# = 0
                End!If job:Date_Despatched < Today() - func:DespatchDays
            End!If job:Date_Despatched <> ''
        
            If print# = 1
                tmp:Count += 1
                Print(rpt:Detail)
        
            End!If print# = 1
        End!If error# = 0
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='CustomerEnquiryReport'
  END
  report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CustomerEnquiryReport',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'CustomerEnquiryReport',1)
    SolaceViewVars('tmp:CustName',tmp:CustName,'CustomerEnquiryReport',1)
    SolaceViewVars('LocalRequest',LocalRequest,'CustomerEnquiryReport',1)
    SolaceViewVars('LocalResponse',LocalResponse,'CustomerEnquiryReport',1)
    SolaceViewVars('FilesOpened',FilesOpened,'CustomerEnquiryReport',1)
    SolaceViewVars('WindowOpened',WindowOpened,'CustomerEnquiryReport',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'CustomerEnquiryReport',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'CustomerEnquiryReport',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'CustomerEnquiryReport',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'CustomerEnquiryReport',1)
    SolaceViewVars('PercentProgress',PercentProgress,'CustomerEnquiryReport',1)
    SolaceViewVars('RecordStatus',RecordStatus,'CustomerEnquiryReport',1)
    SolaceViewVars('EndOfReport',EndOfReport,'CustomerEnquiryReport',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'CustomerEnquiryReport',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'CustomerEnquiryReport',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'CustomerEnquiryReport',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'CustomerEnquiryReport',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'CustomerEnquiryReport',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'CustomerEnquiryReport',1)
    SolaceViewVars('InitialPath',InitialPath,'CustomerEnquiryReport',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'CustomerEnquiryReport',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'CustomerEnquiryReport',1)
    SolaceViewVars('tmp:printedby',tmp:printedby,'CustomerEnquiryReport',1)
    SolaceViewVars('tmp:TelephoneNumber',tmp:TelephoneNumber,'CustomerEnquiryReport',1)
    SolaceViewVars('tmp:FaxNumber',tmp:FaxNumber,'CustomerEnquiryReport',1)
    SolaceViewVars('tmp:JobType',tmp:JobType,'CustomerEnquiryReport',1)
    SolaceViewVars('tmp:StartDate',tmp:StartDate,'CustomerEnquiryReport',1)
    SolaceViewVars('tmp:EndDate',tmp:EndDate,'CustomerEnquiryReport',1)
    SolaceViewVars('tmp:IMEI',tmp:IMEI,'CustomerEnquiryReport',1)
    SolaceViewVars('tmp:Count',tmp:Count,'CustomerEnquiryReport',1)


BuildCtrlQueue      Routine







