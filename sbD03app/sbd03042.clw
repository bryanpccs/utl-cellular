

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03042.INC'),ONCE        !Local module procedure declarations
                     END


Third_Party_Report_Criteria PROCEDURE                 !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select3
trd:Company_Name       LIKE(trd:Company_Name)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB3::View:FileDropCombo VIEW(TRDPARTY)
                       PROJECT(trd:Company_Name)
                     END
window               WINDOW('3rd Party Report Criteria'),AT(,,220,103),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,212,68),USE(?Sheet1),SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           COMBO(@s30),AT(84,20,124,10),USE(GLO:Select3),IMM,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Start Date'),AT(12,36),USE(?glo:select1:Prompt)
                           ENTRY(@d6b),AT(84,36,64,10),USE(GLO:Select1),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(152,36,10,10),USE(?PopCalendar),SKIP,ICON('calenda2.ico')
                           PROMPT('End Date'),AT(12,52),USE(?glo:select2:Prompt)
                           ENTRY(@d6b),AT(84,52,64,10),USE(GLO:Select2),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(152,52,10,10),USE(?PopCalendar:2),SKIP,ICON('calenda2.ico')
                           PROMPT('3rd Party Agent'),AT(12,20),USE(?Prompt1)
                         END
                       END
                       PANEL,AT(4,76,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,80,56,16),USE(?OkButton),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Close'),AT(156,80,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB3                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    If ?GLO:Select3{prop:ReadOnly} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 15066597
    Elsif ?GLO:Select3{prop:Req} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 8454143
    Else ! If ?GLO:Select3{prop:Req} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 16777215
    End ! If ?GLO:Select3{prop:Req} = True
    ?GLO:Select3{prop:Trn} = 0
    ?GLO:Select3{prop:FontStyle} = font:Bold
    ?glo:select1:Prompt{prop:FontColor} = -1
    ?glo:select1:Prompt{prop:Color} = 15066597
    If ?GLO:Select1{prop:ReadOnly} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 15066597
    Elsif ?GLO:Select1{prop:Req} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 8454143
    Else ! If ?GLO:Select1{prop:Req} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 16777215
    End ! If ?GLO:Select1{prop:Req} = True
    ?GLO:Select1{prop:Trn} = 0
    ?GLO:Select1{prop:FontStyle} = font:Bold
    ?glo:select2:Prompt{prop:FontColor} = -1
    ?glo:select2:Prompt{prop:Color} = 15066597
    If ?GLO:Select2{prop:ReadOnly} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 15066597
    Elsif ?GLO:Select2{prop:Req} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 8454143
    Else ! If ?GLO:Select2{prop:Req} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 16777215
    End ! If ?GLO:Select2{prop:Req} = True
    ?GLO:Select2{prop:Trn} = 0
    ?GLO:Select2{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Third_Party_Report_Criteria',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select3;  SolaceCtrlName = '?GLO:Select3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select1:Prompt;  SolaceCtrlName = '?glo:select1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select1;  SolaceCtrlName = '?GLO:Select1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select2:Prompt;  SolaceCtrlName = '?glo:select2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select2;  SolaceCtrlName = '?GLO:Select2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Third_Party_Report_Criteria')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Third_Party_Report_Criteria')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?GLO:Select3
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:TRDPARTY.Open
  SELF.FilesOpened = True
  glo:select1  = Today()
  glo:select2  = Today()
  glo:select3  = ''
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?glo:select1{Prop:Alrt,255} = MouseLeft2
  ?glo:select2{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  FDCB3.Init(GLO:Select3,?GLO:Select3,Queue:FileDropCombo.ViewPosition,FDCB3::View:FileDropCombo,Queue:FileDropCombo,Relate:TRDPARTY,ThisWindow,GlobalErrors,0,1,0)
  FDCB3.Q &= Queue:FileDropCombo
  FDCB3.AddSortOrder(trd:Company_Name_Key)
  FDCB3.AddField(trd:Company_Name,FDCB3.Q.trd:Company_Name)
  ThisWindow.AddItem(FDCB3.WindowComponent)
  FDCB3.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  glo:select1  = ''
  glo:select2  = ''
  glo:select3  = ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TRDPARTY.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Third_Party_Report_Criteria',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select1 = TINCALENDARStyle1(GLO:Select1)
          Display(?glo:select1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select2 = TINCALENDARStyle1(GLO:Select2)
          Display(?glo:select2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      error# = 0
      If glo:select1 = ''
          Select(?glo:select1)
          error# = 1
      End!If glo:select1 = ''
      If glo:select2 = '' and error# = 0
          Select(?glo:select2)
          error# = 1
      End!If glo:select2 = '' and error# = 0
      
      If error# = 0
          Third_Party_Returns_Report
      End!If error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Third_Party_Report_Criteria')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?GLO:Select1
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?GLO:Select2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

