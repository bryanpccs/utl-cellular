

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_fwiz.inc'),ONCE

                     MAP
                       INCLUDE('SBD03086.INC'),ONCE        !Local module procedure declarations
                     END


Status_Report_Criteria PROCEDURE                      !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::24:TAGFLAG         BYTE(0)
DASBRW::24:TAGMOUSE        BYTE(0)
DASBRW::24:TAGDISPSTATUS   BYTE(0)
DASBRW::24:QUEUE          QUEUE
Pointer                       LIKE(GLO:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::25:TAGFLAG         BYTE(0)
DASBRW::25:TAGMOUSE        BYTE(0)
DASBRW::25:TAGDISPSTATUS   BYTE(0)
DASBRW::25:QUEUE          QUEUE
Pointer2                      LIKE(GLO:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::26:TAGFLAG         BYTE(0)
DASBRW::26:TAGMOUSE        BYTE(0)
DASBRW::26:TAGDISPSTATUS   BYTE(0)
DASBRW::26:QUEUE          QUEUE
Pointer3                      LIKE(GLO:Pointer3)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::27:TAGFLAG         BYTE(0)
DASBRW::27:TAGMOUSE        BYTE(0)
DASBRW::27:TAGDISPSTATUS   BYTE(0)
DASBRW::27:QUEUE          QUEUE
Pointer4                      LIKE(GLO:Pointer4)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::28:TAGFLAG         BYTE(0)
DASBRW::28:TAGMOUSE        BYTE(0)
DASBRW::28:TAGDISPSTATUS   BYTE(0)
DASBRW::28:QUEUE          QUEUE
Pointer5                      LIKE(GLO:Pointer5)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::29:TAGFLAG         BYTE(0)
DASBRW::29:TAGMOUSE        BYTE(0)
DASBRW::29:TAGDISPSTATUS   BYTE(0)
DASBRW::29:QUEUE          QUEUE
Pointer6                      LIKE(GLO:Pointer6)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::30:TAGFLAG         BYTE(0)
DASBRW::30:TAGMOUSE        BYTE(0)
DASBRW::30:TAGDISPSTATUS   BYTE(0)
DASBRW::30:QUEUE          QUEUE
Pointer7                      LIKE(GLO:Pointer7)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::31:TAGFLAG         BYTE(0)
DASBRW::31:TAGMOUSE        BYTE(0)
DASBRW::31:TAGDISPSTATUS   BYTE(0)
DASBRW::31:QUEUE          QUEUE
Pointer8                      LIKE(GLO:Pointer8)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::32:TAGFLAG         BYTE(0)
DASBRW::32:TAGMOUSE        BYTE(0)
DASBRW::32:TAGDISPSTATUS   BYTE(0)
DASBRW::32:QUEUE          QUEUE
Pointer9                      LIKE(GLO:Pointer9)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::33:TAGFLAG         BYTE(0)
DASBRW::33:TAGMOUSE        BYTE(0)
DASBRW::33:TAGDISPSTATUS   BYTE(0)
DASBRW::33:QUEUE          QUEUE
Pointer10                     LIKE(GLO:Pointer10)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::35:TAGFLAG         BYTE(0)
DASBRW::35:TAGMOUSE        BYTE(0)
DASBRW::35:TAGDISPSTATUS   BYTE(0)
DASBRW::35:QUEUE          QUEUE
Pointer11                     LIKE(GLO:Pointer11)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::36:TAGFLAG         BYTE(0)
DASBRW::36:TAGMOUSE        BYTE(0)
DASBRW::36:TAGDISPSTATUS   BYTE(0)
DASBRW::36:QUEUE          QUEUE
Pointer12                     LIKE(GLO:Pointer12)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::37:TAGFLAG         BYTE(0)
DASBRW::37:TAGMOUSE        BYTE(0)
DASBRW::37:TAGDISPSTATUS   BYTE(0)
DASBRW::37:QUEUE          QUEUE
Pointer13                     LIKE(GLO:Pointer13)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::38:TAGFLAG         BYTE(0)
DASBRW::38:TAGMOUSE        BYTE(0)
DASBRW::38:TAGDISPSTATUS   BYTE(0)
DASBRW::38:QUEUE          QUEUE
Pointer14                     LIKE(GLO:Pointer14)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::43:TAGFLAG         BYTE(0)
DASBRW::43:TAGMOUSE        BYTE(0)
DASBRW::43:TAGDISPSTATUS   BYTE(0)
DASBRW::43:QUEUE          QUEUE
Pointer15                     LIKE(GLO:Pointer15)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
local                CLASS
GetStatusDate        Procedure(String,*Date,*Time),Byte
                     END
local:FileNameJobs   STRING(255),STATIC
InWorkShopQ          QUEUE,PRE(iwq)
DateValue            DATE
JobRefValue          LONG
                     END
local:FileNameAudit  STRING(255),STATIC
local:FileNameParts  STRING(255),STATIC
local:FileNameWarParts STRING(255),STATIC
save_job_id          USHORT,AUTO
save_aus_id          USHORT,AUTO
save_aud_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_stac_id         USHORT,AUTO
tmp:Description      STRING(60)
tmp:NewDescription   STRING(60)
tmp:OutputType       BYTE(0)
tmp:PaperReportType  BYTE(0)
tmp:ExportJobs       BYTE(0)
tmp:ExportAudit      BYTE(0)
tmp:ExportParts      BYTE(0)
tmp:ExportWarparts   BYTE(0)
tmp:ExportPath       STRING(255)
tmp:HeadAccountTag   STRING(1)
tmp:SubAccountTag    STRING(1)
tmp:No               STRING('NO {1}')
tmp:CChargeTypeTag   STRING(1)
tmp:Yes              STRING('YES')
tmp:WChargeType      STRING(3)
tmp:CRepairTypeTag   STRING(1)
tmp:WRepairTypeTag   STRING(1)
tmp:StatusType       BYTE(0)
tmp:LocationTag      STRING(1)
tmp:UserTag          STRING(1)
tmp:ENGINEER         STRING('ENGINEER {22}')
tmp:Manufacturer     STRING(30)
tmp:SelectManufacturer BYTE(0)
tmp:ManufacturerTag  STRING(1)
tmp:ModelNumberTag   STRING(1)
tmp:UnitTypeTag      STRING(1)
tmp:TransitTypeTag   STRING(1)
tmp:TurnaroundTimeTag STRING(1)
tmp:StatusTag        STRING(1)
tmp:SavedCriteria    STRING(60)
tmp:Workshop         BYTE(0)
tmp:JobType          BYTE(0)
tmp:CompletedType    BYTE(0)
tmp:DespatchedType   BYTE(0)
tmp:JobBatchNumber   LONG
tmp:EDIBatchNumber   LONG
tmp:ReportOrder      STRING(30)
tmp:DateRangeType    BYTE(0)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:InvoiceType      BYTE(0)
tmp:ActualPartsCost  REAL
tmp:FirstPart        LONG
tmp:CustomerStatus   BYTE(1)
tmp:ColourTag        STRING(1)
ConnectionStr        STRING(255),STATIC
INIFilePath          STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:Manufacturer
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:HeadAccountTag     LIKE(tmp:HeadAccountTag)       !List box control field - type derived from local data
tmp:HeadAccountTag_Icon LONG                          !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW5::View:Browse    VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:SubAccountTag      LIKE(tmp:SubAccountTag)        !List box control field - type derived from local data
tmp:SubAccountTag_Icon LONG                           !Entry's icon ID
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                       PROJECT(cha:Warranty)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
tmp:CChargeTypeTag     LIKE(tmp:CChargeTypeTag)       !List box control field - type derived from local data
tmp:CChargeTypeTag_Icon LONG                          !Entry's icon ID
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
cha:Warranty           LIKE(cha:Warranty)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW7::View:Browse    VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                       PROJECT(cha:Warranty)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:4
tmp:WChargeType        LIKE(tmp:WChargeType)          !List box control field - type derived from local data
tmp:WChargeType_Icon   LONG                           !Entry's icon ID
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
cha:Warranty           LIKE(cha:Warranty)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(REPTYDEF)
                       PROJECT(rtd:Repair_Type)
                       PROJECT(rtd:Chargeable)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?List:5
tmp:CRepairTypeTag     LIKE(tmp:CRepairTypeTag)       !List box control field - type derived from local data
tmp:CRepairTypeTag_Icon LONG                          !Entry's icon ID
rtd:Repair_Type        LIKE(rtd:Repair_Type)          !List box control field - type derived from field
rtd:Chargeable         LIKE(rtd:Chargeable)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW9::View:Browse    VIEW(REPTYDEF)
                       PROJECT(rtd:Repair_Type)
                       PROJECT(rtd:Warranty)
                     END
Queue:Browse:5       QUEUE                            !Queue declaration for browse/combo box using ?List:6
tmp:WRepairTypeTag     LIKE(tmp:WRepairTypeTag)       !List box control field - type derived from local data
tmp:WRepairTypeTag_Icon LONG                          !Entry's icon ID
rtd:Repair_Type        LIKE(rtd:Repair_Type)          !List box control field - type derived from field
rtd:Warranty           LIKE(rtd:Warranty)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                       PROJECT(sts:Job)
                       PROJECT(sts:Exchange)
                       PROJECT(sts:Loan)
                     END
Queue:Browse:6       QUEUE                            !Queue declaration for browse/combo box using ?List:7
tmp:StatusTag          LIKE(tmp:StatusTag)            !List box control field - type derived from local data
tmp:StatusTag_Icon     LONG                           !Entry's icon ID
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
sts:Job                LIKE(sts:Job)                  !Browse key field - type derived from field
sts:Exchange           LIKE(sts:Exchange)             !Browse key field - type derived from field
sts:Loan               LIKE(sts:Loan)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW14::View:Browse   VIEW(LOCINTER)
                       PROJECT(loi:Location)
                     END
Queue:Browse:7       QUEUE                            !Queue declaration for browse/combo box using ?List:8
tmp:LocationTag        LIKE(tmp:LocationTag)          !List box control field - type derived from local data
tmp:LocationTag_Icon   LONG                           !Entry's icon ID
loi:Location           LIKE(loi:Location)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW15::View:Browse   VIEW(USERS)
                       PROJECT(use:Surname)
                       PROJECT(use:Forename)
                       PROJECT(use:User_Code)
                       PROJECT(use:User_Type)
                     END
Queue:Browse:8       QUEUE                            !Queue declaration for browse/combo box using ?List:9
tmp:UserTag            LIKE(tmp:UserTag)              !List box control field - type derived from local data
tmp:UserTag_Icon       LONG                           !Entry's icon ID
use:Surname            LIKE(use:Surname)              !List box control field - type derived from field
use:Forename           LIKE(use:Forename)             !List box control field - type derived from field
use:User_Code          LIKE(use:User_Code)            !Primary key field - type derived from field
use:User_Type          LIKE(use:User_Type)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW16::View:Browse   VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
Queue:Browse:9       QUEUE                            !Queue declaration for browse/combo box using ?List:10
tmp:ManufacturerTag    LIKE(tmp:ManufacturerTag)      !List box control field - type derived from local data
tmp:ManufacturerTag_Icon LONG                         !Entry's icon ID
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW17::View:Browse   VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                       PROJECT(mod:Manufacturer)
                     END
Queue:Browse:10      QUEUE                            !Queue declaration for browse/combo box using ?List:11
tmp:ModelNumberTag     LIKE(tmp:ModelNumberTag)       !List box control field - type derived from local data
tmp:ModelNumberTag_Icon LONG                          !Entry's icon ID
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
mod:Manufacturer       LIKE(mod:Manufacturer)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW21::View:Browse   VIEW(UNITTYPE)
                       PROJECT(uni:Unit_Type)
                     END
Queue:Browse:11      QUEUE                            !Queue declaration for browse/combo box using ?List:12
tmp:UnitTypeTag        LIKE(tmp:UnitTypeTag)          !List box control field - type derived from local data
tmp:UnitTypeTag_Icon   LONG                           !Entry's icon ID
uni:Unit_Type          LIKE(uni:Unit_Type)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW22::View:Browse   VIEW(TRANTYPE)
                       PROJECT(trt:Transit_Type)
                     END
Queue:Browse:12      QUEUE                            !Queue declaration for browse/combo box using ?List:13
tmp:TransitTypeTag     LIKE(tmp:TransitTypeTag)       !List box control field - type derived from local data
tmp:TransitTypeTag_Icon LONG                          !Entry's icon ID
trt:Transit_Type       LIKE(trt:Transit_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW23::View:Browse   VIEW(TURNARND)
                       PROJECT(tur:Turnaround_Time)
                     END
Queue:Browse:13      QUEUE                            !Queue declaration for browse/combo box using ?List:14
tmp:TurnaroundTimeTag  LIKE(tmp:TurnaroundTimeTag)    !List box control field - type derived from local data
tmp:TurnaroundTimeTag_Icon LONG                       !Entry's icon ID
tur:Turnaround_Time    LIKE(tur:Turnaround_Time)      !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW42::View:Browse   VIEW(COLOUR)
                       PROJECT(col:Colour)
                     END
Queue:Browse:14      QUEUE                            !Queue declaration for browse/combo box using ?List:15
tmp:ColourTag          LIKE(tmp:ColourTag)            !List box control field - type derived from local data
tmp:ColourTag_Icon     LONG                           !Entry's icon ID
col:Colour             LIKE(col:Colour)               !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB18::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
window               WINDOW('Status Report / Export Criteria'),AT(,,367,343),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,360,308),USE(?Sheet1),SPREAD
                         TAB('1'),USE(?Tab1)
                           PROMPT('Load Previous Criteria'),AT(100,36),USE(?Prompt46),FONT(,,COLOR:Navy,FONT:underline)
                           BUTTON('&Select Critiera'),AT(226,45,56,16),USE(?SelectCriteria),LEFT,ICON('select.ico')
                           PROMPT('Select a previously saved criteria'),AT(100,48),USE(?Prompt47)
                           PROMPT('Criteria Used:'),AT(102,72),USE(?Prompt48)
                           STRING(@s60),AT(152,72),USE(tmp:SavedCriteria),FONT(,,,FONT:bold)
                           PROMPT('Status Report Type'),AT(100,92),USE(?Prompt2),FONT(,,COLOR:Navy,FONT:underline,CHARSET:ANSI)
                           PROMPT('Select how you want to output the results of this report:'),AT(100,108),USE(?Prompt3)
                           OPTION('Report Output Type'),AT(104,128,165,47),USE(tmp:OutputType),BOXED
                             RADIO('Paper Report'),AT(116,148),USE(?tmp:OutputType:Radio1),VALUE('0')
                             RADIO('Export File'),AT(204,148),USE(?tmp:OutputType:Radio2),VALUE('1')
                           END
                           PROMPT('Note: A tag list should be completely unticked to disregard that element from th' &|
   'e criteria. E.g. To run the Status Report regardless of location, leave the inte' &|
   'rnal location tag list completely unticked.'),AT(104,192,256,24),USE(?Prompt66)
                         END
                         TAB('2'),USE(?Tab2)
                           PROMPT('Paper Report Type'),AT(100,36),USE(?Prompt4),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select what type of report you want to print:'),AT(100,52,253,11),USE(?Prompt5)
                           PROMPT('Detailed'),AT(99,77),USE(?Prompt6),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Produces a report listing all the jobs that match the selected criteria.'),AT(99,93),USE(?Prompt7)
                           PROMPT('Gives a total of how many jobs match the selected critiera'),AT(100,128,236,24),USE(?Prompt7:2)
                           PROMPT('Produces a report listing all jobs that match the selected criteria, including P' &|
   'revious Status information'),AT(100,168,236,24),USE(?Prompt7:3)
                           OPTION('Paper Report Type'),AT(100,193,180,47),USE(tmp:PaperReportType),BOXED
                             RADIO('Detailed'),AT(108,212),USE(?tmp:PaperReportType:Radio1),VALUE('0')
                             RADIO('Summary'),AT(160,212),USE(?tmp:PaperReportType:Radio2),VALUE('1')
                             RADIO('Status Change'),AT(212,212),USE(?tmp:PaperReportType:Radio3),VALUE('2')
                           END
                           PROMPT('Summary'),AT(100,112),USE(?Prompt6:2),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Status Change'),AT(100,156),USE(?Prompt6:3),FONT(,,COLOR:Navy,FONT:underline)
                         END
                         TAB('3'),USE(?Tab3)
                           PROMPT('Export File Details'),AT(100,36),USE(?Prompt12),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('For evey job that matches the report criteria, select which of that job''s files ' &|
   'you want exported.'),AT(100,52,200,20),USE(?Prompt13)
                           PROMPT('Select the Name of the export files.'),AT(104,180,200,16),USE(?Prompt13:2)
                           BUTTON,AT(348,212,10,10),USE(?LookupExportPath),SKIP,ICON('List3.ico')
                           PROMPT('File Name Jobs'),AT(112,212),USE(?local:FileNameJobs:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(184,212,160,10),USE(local:FileNameJobs),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('L'),TIP('L'),UPR
                           PROMPT('File Name Audit'),AT(112,228),USE(?local:FileNameAudit:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(184,228,160,10),USE(local:FileNameAudit),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('File Name Audit'),TIP('File Name Audit'),UPR
                           BUTTON,AT(348,228,10,10),USE(?LookupAuditPath),SKIP,ICON('List3.ico')
                           PROMPT('File Name Char Parts'),AT(112,244),USE(?local:FileNameParts:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(184,244,160,10),USE(local:FileNameParts),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('File Name Parts'),TIP('File Name Parts'),UPR
                           BUTTON,AT(348,244,10,10),USE(?LookupCharPartsName),SKIP,ICON('List3.ico')
                           PROMPT('File Name War Parts'),AT(112,260),USE(?local:FileNameWarParts:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(184,260,160,10),USE(local:FileNameWarParts),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('File Name Warparts'),TIP('File Name Warparts'),UPR
                           BUTTON,AT(348,260,10,10),USE(?LookupWarPartsPath),SKIP,ICON('List3.ico')
                           PROMPT('Export File Path'),AT(104,164),USE(?Prompt12:2),FONT(,,COLOR:Navy,FONT:underline)
                           CHECK('Export Jobs File '),AT(116,92),USE(tmp:ExportJobs),MSG('Export Jobs'),TIP('Export Jobs'),VALUE('1','0')
                           CHECK('Export Audit File '),AT(116,108),USE(tmp:ExportAudit),MSG('Export Audit File'),TIP('Export Audit File'),VALUE('1','0')
                           CHECK('Export Chargeable'),AT(116,124),USE(tmp:ExportParts),MSG('Export Parts File'),TIP('Export Parts File'),VALUE('1','0')
                           CHECK('Export Warranty '),AT(116,140),USE(tmp:ExportWarparts),MSG('Export Warranty Parts File'),TIP('Export Warranty Parts File'),VALUE('1','0')
                         END
                         TAB('4'),USE(?Tab4)
                           STRING('Header Account Number'),AT(100,36),USE(?String1),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Trade Header Accounts to Include'),AT(100,52),USE(?Prompt17)
                           PROMPT('If you wish to include all Sub Accounts for a selected Head Account, select the ' &|
   'Head Account(s) you require below, but do NOT select any Sub Accounts from the n' &|
   'ext screen.'),AT(100,64,256,28),USE(?Prompt65)
                           LIST,AT(100,96,216,144),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)J@s1@67L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(21,188,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(21,204,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON('&Tag'),AT(100,244,56,16),USE(?DASTAG),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(156,244,56,16),USE(?DASTAGAll),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(212,244,56,16),USE(?DASUNTAGALL),LEFT,ICON('UnTag.gif')
                         END
                         TAB('5'),USE(?Tab5)
                           PROMPT('Sub Account Number'),AT(100,36),USE(?Prompt18),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Sub Trade Acount To Include'),AT(100,48),USE(?Prompt19)
                           LIST,AT(100,68,216,144),USE(?List:2),IMM,MSG('Browsing Records'),FORMAT('11L(2)J@s1@67L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(17,180,50,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(17,204,70,13),USE(?DASSHOWTAG:2),HIDE
                           BUTTON('&Tag'),AT(100,216,56,16),USE(?DASTAG:2),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(156,216,56,16),USE(?DASTAGAll:2),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(212,216,56,16),USE(?DASUNTAGALL:2),LEFT,ICON('UnTag.gif')
                         END
                         TAB('18'),USE(?Tab18)
                           PROMPT('Workshop Type'),AT(100,36),USE(?Prompt49),FONT(,,COLOR:Navy,FONT:underline)
                           OPTION('In Workshop'),AT(100,49,260,27),USE(tmp:Workshop),BOXED
                             RADIO('In Workshop Only'),AT(108,60),USE(?tmp:Workshop:Radio1),VALUE('1')
                             RADIO('Not In Workshop Only'),AT(184,60),USE(?tmp:Workshop:Radio2),VALUE('2')
                             RADIO('Both'),AT(275,60),USE(?tmp:Workshop:Radio3),VALUE('0')
                           END
                           PROMPT('Job Type'),AT(100,80),USE(?Prompt50),FONT(,,COLOR:Navy,FONT:underline)
                           OPTION('Job Type'),AT(102,96,258,48),USE(tmp:JobType),BOXED
                             RADIO('Warranty Only'),AT(108,108),USE(?tmp:JobType:Radio1),VALUE('1')
                             RADIO('Warranty (inc Split)'),AT(184,107),USE(?tmp:JobType:Radio2),VALUE('2')
                             RADIO('Split Only'),AT(275,107),USE(?tmp:JobType:Radio3),VALUE('3')
                             RADIO('Chargeable Only'),AT(108,124),USE(?tmp:JobType:Radio4),VALUE('4')
                             RADIO('Chargeable (inc Split)'),AT(184,124),USE(?tmp:JobType:Radio5),VALUE('5')
                             RADIO('All'),AT(276,124),USE(?tmp:JobType:Radio6),VALUE('0')
                           END
                           PROMPT('Completed Type'),AT(100,152),USE(?Prompt51),FONT(,,COLOR:Navy,FONT:underline)
                           OPTION('Completed Type'),AT(102,164,260,27),USE(tmp:CompletedType),BOXED
                             RADIO('Completed Only'),AT(108,176),USE(?tmp:CompletedType:Radio1),VALUE('1')
                             RADIO('Incomplete Only'),AT(184,176),USE(?tmp:CompletedType:Radio2),VALUE('2')
                             RADIO('Both'),AT(276,176),USE(?tmp:CompletedType:Radio3),VALUE('0')
                           END
                           PROMPT('Despatched Type'),AT(100,200),USE(?Prompt52),FONT(,,COLOR:Navy,FONT:underline)
                           OPTION('Despatched Type'),AT(100,216,256,32),USE(tmp:DespatchedType),BOXED
                             RADIO('Despatched Only'),AT(108,228),USE(?tmp:DespatchedType:Radio1),VALUE('1')
                             RADIO('Not Despatched Only'),AT(184,228),USE(?tmp:DespatchedType:Radio2),VALUE('2')
                             RADIO('Both'),AT(276,228),USE(?tmp:DespatchedType:Radio3),VALUE('0')
                           END
                           PROMPT('Invoice Type'),AT(100,256),USE(?Prompt67),FONT(,,COLOR:Navy,FONT:underline)
                           OPTION('Invoice Type'),AT(100,268,256,28),USE(tmp:InvoiceType),BOXED
                             RADIO('Invoiced Only'),AT(108,280),USE(?tmp:InvoiceType:Radio1),VALUE('1')
                             RADIO('Non-Invoiced Only'),AT(184,280),USE(?tmp:InvoiceType:Radio2),VALUE('2')
                             RADIO('Both'),AT(276,280),USE(?tmp:InvoiceType:Radio3),VALUE('0')
                           END
                         END
                         TAB('6'),USE(?Tab6)
                           PROMPT('Chargeable Charge Types'),AT(100,36),USE(?Prompt20),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select the Chargeable Charge Types to include'),AT(100,48),USE(?Prompt21)
                           LIST,AT(100,68,148,144),USE(?List:3),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L(2)|M~Charge Type~@s30@'),FROM(Queue:Browse:2)
                           BUTTON('&Rev tags'),AT(21,176,50,13),USE(?DASREVTAG:3),HIDE
                           BUTTON('sho&W tags'),AT(17,200,70,13),USE(?DASSHOWTAG:3),HIDE
                           BUTTON('&Tag'),AT(100,216,56,16),USE(?DASTAG:3),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(156,216,56,16),USE(?DASTAGAll:3),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(212,216,56,16),USE(?DASUNTAGALL:3),LEFT,ICON('UnTag.gif')
                         END
                         TAB('7'),USE(?Tab7)
                           PROMPT('Warranty Charge Types'),AT(100,36),USE(?Prompt22),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Warranty Charge Types To Include'),AT(100,52),USE(?Prompt23)
                           LIST,AT(100,68,148,144),USE(?List:4),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)J@s3@120L(2)|M~Charge Type~@s30@'),FROM(Queue:Browse:3)
                           BUTTON('&Rev tags'),AT(25,180,50,13),USE(?DASREVTAG:4),HIDE
                           BUTTON('sho&W tags'),AT(21,200,70,13),USE(?DASSHOWTAG:4),HIDE
                           BUTTON('&Tag'),AT(100,216,56,16),USE(?DASTAG:4),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(156,216,56,16),USE(?DASTAGAll:4),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(212,216,56,16),USE(?DASUNTAGALL:4),LEFT,ICON('UnTag.gif')
                         END
                         TAB('8'),USE(?Tab8)
                           PROMPT('Chargeable Repair Types'),AT(100,36),USE(?Prompt24),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Chargeable Repair Types To Include'),AT(100,52),USE(?Prompt25)
                           LIST,AT(100,68,148,144),USE(?List:5),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L(2)|M~Repair Type~@s30@'),FROM(Queue:Browse:4)
                           BUTTON('&Rev tags'),AT(13,180,50,13),USE(?DASREVTAG:5),HIDE
                           BUTTON('sho&W tags'),AT(17,200,70,13),USE(?DASSHOWTAG:5),HIDE
                           BUTTON('&Tag'),AT(100,216,56,16),USE(?DASTAG:5),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(156,216,56,16),USE(?DASTAGAll:5),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(212,216,56,16),USE(?DASUNTAGALL:5),LEFT,ICON('UnTag.gif')
                         END
                         TAB('9'),USE(?Tab9)
                           PROMPT('Warranty Repair Types'),AT(100,36),USE(?Prompt26),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Warranty Repair Types To Include'),AT(100,48),USE(?Prompt27)
                           LIST,AT(100,68,148,144),USE(?List:6),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L(2)|M~Repair Type~@s30@'),FROM(Queue:Browse:5)
                           BUTTON('&Rev tags'),AT(21,180,50,13),USE(?DASREVTAG:6),HIDE
                           BUTTON('sho&W tags'),AT(21,196,70,13),USE(?DASSHOWTAG:6),HIDE
                           BUTTON('&Tag'),AT(100,216,56,16),USE(?DASTAG:6),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(156,216,56,16),USE(?DASTAGAll:6),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(212,216,56,16),USE(?DASUNTAGALL:6),LEFT,ICON('UnTag.gif')
                         END
                         TAB('10'),USE(?Tab10)
                           PROMPT('Status Types'),AT(100,36),USE(?Prompt28),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Type and Status To Include.'),AT(100,48),USE(?Prompt29)
                           PROMPT('Note: This is the status which will appear on the paper report. If you select "C' &|
   'ustomer" Status, then the Job Status will be shown, unless the unit has an Excha' &|
   'nge attached. Then the Exchange Status will be shown.'),AT(100,60,256,32),USE(?Prompt30)
                           OPTION('Status Type'),AT(100,92,156,28),USE(tmp:StatusType),BOXED
                             RADIO('Job'),AT(108,104),USE(?tmp:StatusType:Radio1),VALUE('0')
                             RADIO('Exchange'),AT(151,104),USE(?tmp:StatusType:Radio2),VALUE('1')
                             RADIO('Loan'),AT(215,104),USE(?tmp:StatusType:Radio3),VALUE('2')
                           END
                           CHECK('Show "Customer" Status On Report'),AT(100,124),USE(tmp:CustomerStatus),MSG('Show "Customer" Status On Report'),TIP('Show "Customer" Status On Report'),VALUE('1','0')
                           LIST,AT(100,136,148,144),USE(?List:7),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L(2)|M~Status~@s30@'),FROM(Queue:Browse:6)
                           BUTTON('&Rev tags'),AT(21,188,50,13),USE(?DASREVTAG:7),HIDE
                           BUTTON('sho&W tags'),AT(17,208,70,13),USE(?DASSHOWTAG:7),HIDE
                           BUTTON('&Tag'),AT(100,284,56,16),USE(?DASTAG:7),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(156,284,56,16),USE(?DASTAGAll:7),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(212,284,56,16),USE(?DASUNTAGALL:7),LEFT,ICON('UnTag.gif')
                         END
                         TAB('11'),USE(?Tab11)
                           PROMPT('Internal Locations'),AT(100,36),USE(?Prompt31),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select Which Internal Locations To Include'),AT(100,52),USE(?Prompt32)
                           LIST,AT(100,68,148,144),USE(?List:8),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L(2)|M~Location~@s30@'),FROM(Queue:Browse:7)
                           BUTTON('&Rev tags'),AT(21,176,50,13),USE(?DASREVTAG:8),HIDE
                           BUTTON('sho&W tags'),AT(13,196,70,13),USE(?DASSHOWTAG:8),HIDE
                           BUTTON('&Tag'),AT(100,216,56,16),USE(?DASTAG:8),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(156,216,56,16),USE(?DASTAGAll:8),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(212,216,56,16),USE(?DASUNTAGALL:8),LEFT,ICON('UnTag.gif')
                         END
                         TAB('12'),USE(?Tab12)
                           PROMPT('Engineers'),AT(100,36),USE(?Prompt33),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select Which Engineers To Include'),AT(100,52),USE(?Prompt34)
                           LIST,AT(101,72,215,144),USE(?List:9),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)J@s1@109L(2)|M~Surname~@s30@120L(2)|M~Forename~@s30@'),FROM(Queue:Browse:8)
                           BUTTON('&Rev tags'),AT(17,180,50,13),USE(?DASREVTAG:9),HIDE
                           BUTTON('sho&W tags'),AT(17,204,70,13),USE(?DASSHOWTAG:9),HIDE
                           BUTTON('&Tag'),AT(100,220,56,16),USE(?DASTAG:9),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(156,220,56,16),USE(?DASTAGAll:9),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(212,220,56,16),USE(?DASUNTAGALL:9),LEFT,ICON('UnTag.gif')
                         END
                         TAB('13'),USE(?Tab13)
                           PROMPT('Manufacturers'),AT(100,36),USE(?Prompt35),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Manufacturers To Include'),AT(100,48),USE(?Prompt36)
                           PROMPT('If you wish to include all Model Numbers for a selected Manufacturer, select the' &|
   ' manufacturer(s) you require below, but do NOT select any Model Numbers from the' &|
   ' next screen.'),AT(100,64,249,24),USE(?Prompt64)
                           LIST,AT(100,100,148,144),USE(?List:10),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:9)
                           BUTTON('&Rev tags'),AT(25,188,50,13),USE(?DASREVTAG:10),HIDE
                           BUTTON('sho&W tags'),AT(25,204,70,13),USE(?DASSHOWTAG:10),HIDE
                           BUTTON('&Tag'),AT(100,248,56,16),USE(?DASTAG:10),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(156,248,56,16),USE(?DASTAGAll:10),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(212,248,56,16),USE(?DASUNTAGALL:10),LEFT,ICON('UnTag.gif')
                         END
                         TAB('14'),USE(?Tab14)
                           PROMPT('Model Numbers'),AT(100,36),USE(?Prompt37),FONT(,,COLOR:Navy,FONT:underline,CHARSET:ANSI)
                           PROMPT('Select Which Model Numbers To Include'),AT(100,52),USE(?Prompt38)
                           CHECK('Select Manufacturer'),AT(101,76),USE(tmp:SelectManufacturer),MSG('Select Manurfacturer'),TIP('Select Manurfacturer'),VALUE('1','0')
                           COMBO(@s20),AT(196,76,124,10),USE(tmp:Manufacturer),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Manufacturer'),AT(196,68),USE(?Prompt39),FONT(,7,,)
                           LIST,AT(100,92,148,144),USE(?List:11),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L(2)|M~Model Number~@s30@'),FROM(Queue:Browse:10)
                           BUTTON('&Rev tags'),AT(22,185,50,13),USE(?DASREVTAG:11),HIDE
                           BUTTON('sho&W tags'),AT(18,201,70,13),USE(?DASSHOWTAG:11),HIDE
                           BUTTON('&Tag'),AT(100,240,56,16),USE(?DASTAG:11),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(156,240,56,16),USE(?DASTAGAll:11),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(212,240,56,16),USE(?DASUNTAGALL:11),LEFT,ICON('UnTag.gif')
                         END
                         TAB('15'),USE(?Tab15)
                           PROMPT('Unit Types'),AT(100,36),USE(?Prompt40),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Unit Types to include'),AT(100,52),USE(?Prompt41)
                           LIST,AT(100,68,148,144),USE(?List:12),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L(2)|M~Unit Type~@s30@'),FROM(Queue:Browse:11)
                           BUTTON('&Rev tags'),AT(14,177,50,13),USE(?DASREVTAG:12),HIDE
                           BUTTON('sho&W tags'),AT(14,197,70,13),USE(?DASSHOWTAG:12),HIDE
                           BUTTON('&Tag'),AT(100,216,56,16),USE(?DASTAG:12),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(156,216,56,16),USE(?DASTAGAll:12),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(212,216,56,16),USE(?DASUNTAGALL:12),LEFT,ICON('UnTag.gif')
                         END
                         TAB('16'),USE(?Tab16)
                           PROMPT('Transit Types'),AT(100,36),USE(?Prompt42),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Transit Types to include'),AT(100,52),USE(?Prompt43)
                           LIST,AT(100,68,148,144),USE(?List:13),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L(2)|M~Transit Type~@s30@'),FROM(Queue:Browse:12)
                           BUTTON('&Rev tags'),AT(14,177,50,13),USE(?DASREVTAG:13),HIDE
                           BUTTON('sho&W tags'),AT(18,209,70,13),USE(?DASSHOWTAG:13),HIDE
                           BUTTON('&Tag'),AT(100,216,56,16),USE(?DASTAG:13),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(156,216,56,16),USE(?DASTAGAll:13),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(212,216,56,16),USE(?DASUNTAGALL:13),LEFT,ICON('UnTag.gif')
                         END
                         TAB('17'),USE(?Tab17)
                           PROMPT('Turnaround Times'),AT(100,36),USE(?Prompt44),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Turnaround Times to include'),AT(100,52),USE(?Prompt45)
                           LIST,AT(100,68,148,144),USE(?List:14),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L(2)|M~Turnaround Time~@s30@'),FROM(Queue:Browse:13)
                           BUTTON('&Rev tags'),AT(22,177,50,13),USE(?DASREVTAG:14),HIDE
                           BUTTON('sho&W tags'),AT(18,193,70,13),USE(?DASSHOWTAG:14),HIDE
                           BUTTON('&Tag'),AT(100,216,56,16),USE(?DASTAG:14),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(156,216,56,16),USE(?DASTAGAll:14),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(212,216,56,16),USE(?DASUNTAGALL:14),LEFT,ICON('UnTag.gif')
                         END
                         TAB('Tab 20'),USE(?Tab20)
                           PROMPT('Colour'),AT(100,36),USE(?ColourTitle),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which entries To Include'),AT(100,52),USE(?ColourText)
                           LIST,AT(100,68,148,144),USE(?List:15),IMM,MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Colour~@s30@'),FROM(Queue:Browse:14)
                           BUTTON('&Rev tags'),AT(18,202,50,13),USE(?DASREVTAG:15),HIDE
                           BUTTON('sho&W tags'),AT(18,218,70,13),USE(?DASSHOWTAG:15),HIDE
                           BUTTON('&Tag'),AT(100,216,56,16),USE(?DASTAG:15),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(156,216,56,16),USE(?DASTAGAll:15),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(212,216,56,16),USE(?DASUNTAGALL:15),LEFT,ICON('UnTag.gif')
                         END
                         TAB('Tab 19'),USE(?Tab19)
                           PROMPT('Batch Numbers'),AT(100,36),USE(?Prompt53),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which of the following batch numbers you wish to include'),AT(100,52),USE(?Prompt54)
                           PROMPT('Job Batch Number'),AT(108,76),USE(?tmp:JobBatchNumber:Prompt),TRN
                           ENTRY(@s8),AT(184,76,64,10),USE(tmp:JobBatchNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Job Batch Number'),TIP('Job Batch Number'),UPR
                           PROMPT('EDI Batch Number'),AT(108,96),USE(?tmp:EDIBatchNumber:Prompt),TRN
                           ENTRY(@s8),AT(184,96,64,10),USE(tmp:EDIBatchNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('EDI Batch Number'),TIP('EDI Batch Number'),UPR
                           PROMPT('Report/Export Order'),AT(100,121),USE(?Prompt57),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which order you wish your report to appear in. Note that Date Booked (if ' &|
   'using Date Booked date range) or Date Completed (if using Date Completed date ra' &|
   'nge) will produce the quickest report.'),AT(100,132,256,32),USE(?Prompt58)
                           PROMPT('Report Order'),AT(112,172),USE(?Prompt59)
                           LIST,AT(184,172,124,10),USE(tmp:ReportOrder),LEFT(2),FONT(,,,FONT:bold),DROP(10,124),FROM('DATE BOOKED|DATE COMPLETED|JOB NUMBER|I.M.E.I. NUMBER|ACCOUNT NUMBER|MODEL NUMBER|STATUS|M.S.N.|SURNAME|ENGINEER|MOBILE NUMBER|IN WORKSHOP')
                           PROMPT('Date Range'),AT(100,197),USE(?Prompt60),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select the Date Range type and date range the report will use'),AT(100,212),USE(?Prompt61)
                           OPTION('Date Range Type'),AT(102,225,250,31),USE(tmp:DateRangeType),BOXED
                             RADIO('Booking Date'),AT(124,237),USE(?tmp:DateRangeType:Radio1),VALUE('0')
                             RADIO('Completed Date'),AT(202,237),USE(?tmp:DateRangeType:Radio2),VALUE('1')
                             RADIO('In Workshop'),AT(288,237),USE(?tmp:DateRangeType:Radio3),VALUE('2')
                           END
                           PROMPT('Start Date'),AT(112,261),USE(?tmp:StartDate:Prompt)
                           ENTRY(@d6),AT(184,261,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(252,260,10,10),USE(?LookupStartDate),SKIP,ICON('Calenda2.ico')
                           PROMPT('End Date'),AT(112,276),USE(?tmp:EndDate:Prompt)
                           ENTRY(@d6),AT(184,277,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(252,276,10,10),USE(?LookupEndDate),SKIP,ICON('Calenda2.ico')
                         END
                       END
                       PROMPT('Status Report/Export Wizard'),AT(8,8),USE(?Prompt1),FONT(,14,,FONT:bold)
                       IMAGE('WIZ.GIF'),AT(12,116),USE(?Image1)
                       PANEL,AT(4,316,360,24),USE(?ButtonPanel),FILL(COLOR:Silver)
                       BUTTON('&Back'),AT(8,320,56,16),USE(?VSBackButton),LEFT,ICON(ICON:VCRrewind)
                       BUTTON('&Next'),AT(64,320,56,16),USE(?VSNextButton),LEFT,ICON(ICON:VCRfastforward)
                       BUTTON('Cancel'),AT(304,320,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       BUTTON('&Finish'),AT(120,320,56,16),USE(?Finish),LEFT,ICON('thumbs.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
Wizard13         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW5                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW6                 CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW7                 CLASS(BrowseClass)               !Browse using ?List:4
Q                      &Queue:Browse:3                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW8                 CLASS(BrowseClass)               !Browse using ?List:5
Q                      &Queue:Browse:4                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW9                 CLASS(BrowseClass)               !Browse using ?List:6
Q                      &Queue:Browse:5                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW10                CLASS(BrowseClass)               !Browse using ?List:7
Q                      &Queue:Browse:6                !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW10::Sort0:Locator StepLocatorClass                 !Default Locator
BRW10::Sort1:Locator StepLocatorClass                 !Conditional Locator - tmp:StatusType = 0
BRW10::Sort2:Locator StepLocatorClass                 !Conditional Locator - tmp:StatusType = 1
BRW10::Sort3:Locator StepLocatorClass                 !Conditional Locator - tmp:StatusType = 2
BRW14                CLASS(BrowseClass)               !Browse using ?List:8
Q                      &Queue:Browse:7                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW14::Sort0:Locator StepLocatorClass                 !Default Locator
BRW15                CLASS(BrowseClass)               !Browse using ?List:9
Q                      &Queue:Browse:8                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW15::Sort0:Locator StepLocatorClass                 !Default Locator
BRW16                CLASS(BrowseClass)               !Browse using ?List:10
Q                      &Queue:Browse:9                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW16::Sort0:Locator StepLocatorClass                 !Default Locator
BRW17                CLASS(BrowseClass)               !Browse using ?List:11
Q                      &Queue:Browse:10               !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW17::Sort0:Locator StepLocatorClass                 !Default Locator
BRW17::Sort1:Locator StepLocatorClass                 !Conditional Locator - tmp:SelectManufacturer = 0
BRW17::Sort2:Locator StepLocatorClass                 !Conditional Locator - tmp:SelectManufacturer = 1
BRW21                CLASS(BrowseClass)               !Browse using ?List:12
Q                      &Queue:Browse:11               !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW21::Sort0:Locator StepLocatorClass                 !Default Locator
BRW22                CLASS(BrowseClass)               !Browse using ?List:13
Q                      &Queue:Browse:12               !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW22::Sort0:Locator StepLocatorClass                 !Default Locator
BRW23                CLASS(BrowseClass)               !Browse using ?List:14
Q                      &Queue:Browse:13               !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW23::Sort0:Locator StepLocatorClass                 !Default Locator
BRW42                CLASS(BrowseClass)               !Browse using ?List:15
Q                      &Queue:Browse:14               !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW42::Sort0:Locator StepLocatorClass                 !Default Locator
FDCB18               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FileLookup34         SelectFileClass
FileLookup44         SelectFileClass
FileLookup45         SelectFileClass
FileLookup46         SelectFileClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
ExportFileJobs    File,Driver('ASCII'),Pre(expjobs),Name(local:FileNameJobs),Create,Bindable,Thread
Record                  Record
Line1                   String(10000)
                        End
                    End
ExportFileAudit    File,Driver('ASCII'),Pre(expaudit),Name(local:FileNameAudit),Create,Bindable,Thread
Record                  Record
Line1                   String(10000)
                        End
                    End
ExportFileParts    File,Driver('ASCII'),Pre(expparts),Name(local:FileNameParts),Create,Bindable,Thread
Record                  Record
Line1                   String(10000)
                        End
                    End
ExportFileWarparts    File,Driver('ASCII'),Pre(expwarparts),Name(local:FileNameWarParts),Create,Bindable,Thread
Record                  Record
Line1                   String(10000)
                        End
                    End

! Dummy SQL table
SQLFile             FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(SQL),BINDABLE,THREAD
Record                  RECORD
SQLSBJobNo                  Long, Name('SBJobNo')
SQLCollectionMadeToday      Byte(),Name('CollectionMadeToday')
                       END
                    END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt46{prop:FontColor} = -1
    ?Prompt46{prop:Color} = 15066597
    ?Prompt47{prop:FontColor} = -1
    ?Prompt47{prop:Color} = 15066597
    ?Prompt48{prop:FontColor} = -1
    ?Prompt48{prop:Color} = 15066597
    ?tmp:SavedCriteria{prop:FontColor} = -1
    ?tmp:SavedCriteria{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?tmp:OutputType{prop:Font,3} = -1
    ?tmp:OutputType{prop:Color} = 15066597
    ?tmp:OutputType{prop:Trn} = 0
    ?tmp:OutputType:Radio1{prop:Font,3} = -1
    ?tmp:OutputType:Radio1{prop:Color} = 15066597
    ?tmp:OutputType:Radio1{prop:Trn} = 0
    ?tmp:OutputType:Radio2{prop:Font,3} = -1
    ?tmp:OutputType:Radio2{prop:Color} = 15066597
    ?tmp:OutputType:Radio2{prop:Trn} = 0
    ?Prompt66{prop:FontColor} = -1
    ?Prompt66{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    ?Prompt7:2{prop:FontColor} = -1
    ?Prompt7:2{prop:Color} = 15066597
    ?Prompt7:3{prop:FontColor} = -1
    ?Prompt7:3{prop:Color} = 15066597
    ?tmp:PaperReportType{prop:Font,3} = -1
    ?tmp:PaperReportType{prop:Color} = 15066597
    ?tmp:PaperReportType{prop:Trn} = 0
    ?tmp:PaperReportType:Radio1{prop:Font,3} = -1
    ?tmp:PaperReportType:Radio1{prop:Color} = 15066597
    ?tmp:PaperReportType:Radio1{prop:Trn} = 0
    ?tmp:PaperReportType:Radio2{prop:Font,3} = -1
    ?tmp:PaperReportType:Radio2{prop:Color} = 15066597
    ?tmp:PaperReportType:Radio2{prop:Trn} = 0
    ?tmp:PaperReportType:Radio3{prop:Font,3} = -1
    ?tmp:PaperReportType:Radio3{prop:Color} = 15066597
    ?tmp:PaperReportType:Radio3{prop:Trn} = 0
    ?Prompt6:2{prop:FontColor} = -1
    ?Prompt6:2{prop:Color} = 15066597
    ?Prompt6:3{prop:FontColor} = -1
    ?Prompt6:3{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?Prompt12{prop:FontColor} = -1
    ?Prompt12{prop:Color} = 15066597
    ?Prompt13{prop:FontColor} = -1
    ?Prompt13{prop:Color} = 15066597
    ?Prompt13:2{prop:FontColor} = -1
    ?Prompt13:2{prop:Color} = 15066597
    ?local:FileNameJobs:Prompt{prop:FontColor} = -1
    ?local:FileNameJobs:Prompt{prop:Color} = 15066597
    If ?local:FileNameJobs{prop:ReadOnly} = True
        ?local:FileNameJobs{prop:FontColor} = 65793
        ?local:FileNameJobs{prop:Color} = 15066597
    Elsif ?local:FileNameJobs{prop:Req} = True
        ?local:FileNameJobs{prop:FontColor} = 65793
        ?local:FileNameJobs{prop:Color} = 8454143
    Else ! If ?local:FileNameJobs{prop:Req} = True
        ?local:FileNameJobs{prop:FontColor} = 65793
        ?local:FileNameJobs{prop:Color} = 16777215
    End ! If ?local:FileNameJobs{prop:Req} = True
    ?local:FileNameJobs{prop:Trn} = 0
    ?local:FileNameJobs{prop:FontStyle} = font:Bold
    ?local:FileNameAudit:Prompt{prop:FontColor} = -1
    ?local:FileNameAudit:Prompt{prop:Color} = 15066597
    If ?local:FileNameAudit{prop:ReadOnly} = True
        ?local:FileNameAudit{prop:FontColor} = 65793
        ?local:FileNameAudit{prop:Color} = 15066597
    Elsif ?local:FileNameAudit{prop:Req} = True
        ?local:FileNameAudit{prop:FontColor} = 65793
        ?local:FileNameAudit{prop:Color} = 8454143
    Else ! If ?local:FileNameAudit{prop:Req} = True
        ?local:FileNameAudit{prop:FontColor} = 65793
        ?local:FileNameAudit{prop:Color} = 16777215
    End ! If ?local:FileNameAudit{prop:Req} = True
    ?local:FileNameAudit{prop:Trn} = 0
    ?local:FileNameAudit{prop:FontStyle} = font:Bold
    ?local:FileNameParts:Prompt{prop:FontColor} = -1
    ?local:FileNameParts:Prompt{prop:Color} = 15066597
    If ?local:FileNameParts{prop:ReadOnly} = True
        ?local:FileNameParts{prop:FontColor} = 65793
        ?local:FileNameParts{prop:Color} = 15066597
    Elsif ?local:FileNameParts{prop:Req} = True
        ?local:FileNameParts{prop:FontColor} = 65793
        ?local:FileNameParts{prop:Color} = 8454143
    Else ! If ?local:FileNameParts{prop:Req} = True
        ?local:FileNameParts{prop:FontColor} = 65793
        ?local:FileNameParts{prop:Color} = 16777215
    End ! If ?local:FileNameParts{prop:Req} = True
    ?local:FileNameParts{prop:Trn} = 0
    ?local:FileNameParts{prop:FontStyle} = font:Bold
    ?local:FileNameWarParts:Prompt{prop:FontColor} = -1
    ?local:FileNameWarParts:Prompt{prop:Color} = 15066597
    If ?local:FileNameWarParts{prop:ReadOnly} = True
        ?local:FileNameWarParts{prop:FontColor} = 65793
        ?local:FileNameWarParts{prop:Color} = 15066597
    Elsif ?local:FileNameWarParts{prop:Req} = True
        ?local:FileNameWarParts{prop:FontColor} = 65793
        ?local:FileNameWarParts{prop:Color} = 8454143
    Else ! If ?local:FileNameWarParts{prop:Req} = True
        ?local:FileNameWarParts{prop:FontColor} = 65793
        ?local:FileNameWarParts{prop:Color} = 16777215
    End ! If ?local:FileNameWarParts{prop:Req} = True
    ?local:FileNameWarParts{prop:Trn} = 0
    ?local:FileNameWarParts{prop:FontStyle} = font:Bold
    ?Prompt12:2{prop:FontColor} = -1
    ?Prompt12:2{prop:Color} = 15066597
    ?tmp:ExportJobs{prop:Font,3} = -1
    ?tmp:ExportJobs{prop:Color} = 15066597
    ?tmp:ExportJobs{prop:Trn} = 0
    ?tmp:ExportAudit{prop:Font,3} = -1
    ?tmp:ExportAudit{prop:Color} = 15066597
    ?tmp:ExportAudit{prop:Trn} = 0
    ?tmp:ExportParts{prop:Font,3} = -1
    ?tmp:ExportParts{prop:Color} = 15066597
    ?tmp:ExportParts{prop:Trn} = 0
    ?tmp:ExportWarparts{prop:Font,3} = -1
    ?tmp:ExportWarparts{prop:Color} = 15066597
    ?tmp:ExportWarparts{prop:Trn} = 0
    ?Tab4{prop:Color} = 15066597
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    ?Prompt17{prop:FontColor} = -1
    ?Prompt17{prop:Color} = 15066597
    ?Prompt65{prop:FontColor} = -1
    ?Prompt65{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Tab5{prop:Color} = 15066597
    ?Prompt18{prop:FontColor} = -1
    ?Prompt18{prop:Color} = 15066597
    ?Prompt19{prop:FontColor} = -1
    ?Prompt19{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Tab18{prop:Color} = 15066597
    ?Prompt49{prop:FontColor} = -1
    ?Prompt49{prop:Color} = 15066597
    ?tmp:Workshop{prop:Font,3} = -1
    ?tmp:Workshop{prop:Color} = 15066597
    ?tmp:Workshop{prop:Trn} = 0
    ?tmp:Workshop:Radio1{prop:Font,3} = -1
    ?tmp:Workshop:Radio1{prop:Color} = 15066597
    ?tmp:Workshop:Radio1{prop:Trn} = 0
    ?tmp:Workshop:Radio2{prop:Font,3} = -1
    ?tmp:Workshop:Radio2{prop:Color} = 15066597
    ?tmp:Workshop:Radio2{prop:Trn} = 0
    ?tmp:Workshop:Radio3{prop:Font,3} = -1
    ?tmp:Workshop:Radio3{prop:Color} = 15066597
    ?tmp:Workshop:Radio3{prop:Trn} = 0
    ?Prompt50{prop:FontColor} = -1
    ?Prompt50{prop:Color} = 15066597
    ?tmp:JobType{prop:Font,3} = -1
    ?tmp:JobType{prop:Color} = 15066597
    ?tmp:JobType{prop:Trn} = 0
    ?tmp:JobType:Radio1{prop:Font,3} = -1
    ?tmp:JobType:Radio1{prop:Color} = 15066597
    ?tmp:JobType:Radio1{prop:Trn} = 0
    ?tmp:JobType:Radio2{prop:Font,3} = -1
    ?tmp:JobType:Radio2{prop:Color} = 15066597
    ?tmp:JobType:Radio2{prop:Trn} = 0
    ?tmp:JobType:Radio3{prop:Font,3} = -1
    ?tmp:JobType:Radio3{prop:Color} = 15066597
    ?tmp:JobType:Radio3{prop:Trn} = 0
    ?tmp:JobType:Radio4{prop:Font,3} = -1
    ?tmp:JobType:Radio4{prop:Color} = 15066597
    ?tmp:JobType:Radio4{prop:Trn} = 0
    ?tmp:JobType:Radio5{prop:Font,3} = -1
    ?tmp:JobType:Radio5{prop:Color} = 15066597
    ?tmp:JobType:Radio5{prop:Trn} = 0
    ?tmp:JobType:Radio6{prop:Font,3} = -1
    ?tmp:JobType:Radio6{prop:Color} = 15066597
    ?tmp:JobType:Radio6{prop:Trn} = 0
    ?Prompt51{prop:FontColor} = -1
    ?Prompt51{prop:Color} = 15066597
    ?tmp:CompletedType{prop:Font,3} = -1
    ?tmp:CompletedType{prop:Color} = 15066597
    ?tmp:CompletedType{prop:Trn} = 0
    ?tmp:CompletedType:Radio1{prop:Font,3} = -1
    ?tmp:CompletedType:Radio1{prop:Color} = 15066597
    ?tmp:CompletedType:Radio1{prop:Trn} = 0
    ?tmp:CompletedType:Radio2{prop:Font,3} = -1
    ?tmp:CompletedType:Radio2{prop:Color} = 15066597
    ?tmp:CompletedType:Radio2{prop:Trn} = 0
    ?tmp:CompletedType:Radio3{prop:Font,3} = -1
    ?tmp:CompletedType:Radio3{prop:Color} = 15066597
    ?tmp:CompletedType:Radio3{prop:Trn} = 0
    ?Prompt52{prop:FontColor} = -1
    ?Prompt52{prop:Color} = 15066597
    ?tmp:DespatchedType{prop:Font,3} = -1
    ?tmp:DespatchedType{prop:Color} = 15066597
    ?tmp:DespatchedType{prop:Trn} = 0
    ?tmp:DespatchedType:Radio1{prop:Font,3} = -1
    ?tmp:DespatchedType:Radio1{prop:Color} = 15066597
    ?tmp:DespatchedType:Radio1{prop:Trn} = 0
    ?tmp:DespatchedType:Radio2{prop:Font,3} = -1
    ?tmp:DespatchedType:Radio2{prop:Color} = 15066597
    ?tmp:DespatchedType:Radio2{prop:Trn} = 0
    ?tmp:DespatchedType:Radio3{prop:Font,3} = -1
    ?tmp:DespatchedType:Radio3{prop:Color} = 15066597
    ?tmp:DespatchedType:Radio3{prop:Trn} = 0
    ?Prompt67{prop:FontColor} = -1
    ?Prompt67{prop:Color} = 15066597
    ?tmp:InvoiceType{prop:Font,3} = -1
    ?tmp:InvoiceType{prop:Color} = 15066597
    ?tmp:InvoiceType{prop:Trn} = 0
    ?tmp:InvoiceType:Radio1{prop:Font,3} = -1
    ?tmp:InvoiceType:Radio1{prop:Color} = 15066597
    ?tmp:InvoiceType:Radio1{prop:Trn} = 0
    ?tmp:InvoiceType:Radio2{prop:Font,3} = -1
    ?tmp:InvoiceType:Radio2{prop:Color} = 15066597
    ?tmp:InvoiceType:Radio2{prop:Trn} = 0
    ?tmp:InvoiceType:Radio3{prop:Font,3} = -1
    ?tmp:InvoiceType:Radio3{prop:Color} = 15066597
    ?tmp:InvoiceType:Radio3{prop:Trn} = 0
    ?Tab6{prop:Color} = 15066597
    ?Prompt20{prop:FontColor} = -1
    ?Prompt20{prop:Color} = 15066597
    ?Prompt21{prop:FontColor} = -1
    ?Prompt21{prop:Color} = 15066597
    ?List:3{prop:FontColor} = 65793
    ?List:3{prop:Color}= 16777215
    ?List:3{prop:Color,2} = 16777215
    ?List:3{prop:Color,3} = 12937777
    ?Tab7{prop:Color} = 15066597
    ?Prompt22{prop:FontColor} = -1
    ?Prompt22{prop:Color} = 15066597
    ?Prompt23{prop:FontColor} = -1
    ?Prompt23{prop:Color} = 15066597
    ?List:4{prop:FontColor} = 65793
    ?List:4{prop:Color}= 16777215
    ?List:4{prop:Color,2} = 16777215
    ?List:4{prop:Color,3} = 12937777
    ?Tab8{prop:Color} = 15066597
    ?Prompt24{prop:FontColor} = -1
    ?Prompt24{prop:Color} = 15066597
    ?Prompt25{prop:FontColor} = -1
    ?Prompt25{prop:Color} = 15066597
    ?List:5{prop:FontColor} = 65793
    ?List:5{prop:Color}= 16777215
    ?List:5{prop:Color,2} = 16777215
    ?List:5{prop:Color,3} = 12937777
    ?Tab9{prop:Color} = 15066597
    ?Prompt26{prop:FontColor} = -1
    ?Prompt26{prop:Color} = 15066597
    ?Prompt27{prop:FontColor} = -1
    ?Prompt27{prop:Color} = 15066597
    ?List:6{prop:FontColor} = 65793
    ?List:6{prop:Color}= 16777215
    ?List:6{prop:Color,2} = 16777215
    ?List:6{prop:Color,3} = 12937777
    ?Tab10{prop:Color} = 15066597
    ?Prompt28{prop:FontColor} = -1
    ?Prompt28{prop:Color} = 15066597
    ?Prompt29{prop:FontColor} = -1
    ?Prompt29{prop:Color} = 15066597
    ?Prompt30{prop:FontColor} = -1
    ?Prompt30{prop:Color} = 15066597
    ?tmp:StatusType{prop:Font,3} = -1
    ?tmp:StatusType{prop:Color} = 15066597
    ?tmp:StatusType{prop:Trn} = 0
    ?tmp:StatusType:Radio1{prop:Font,3} = -1
    ?tmp:StatusType:Radio1{prop:Color} = 15066597
    ?tmp:StatusType:Radio1{prop:Trn} = 0
    ?tmp:StatusType:Radio2{prop:Font,3} = -1
    ?tmp:StatusType:Radio2{prop:Color} = 15066597
    ?tmp:StatusType:Radio2{prop:Trn} = 0
    ?tmp:StatusType:Radio3{prop:Font,3} = -1
    ?tmp:StatusType:Radio3{prop:Color} = 15066597
    ?tmp:StatusType:Radio3{prop:Trn} = 0
    ?tmp:CustomerStatus{prop:Font,3} = -1
    ?tmp:CustomerStatus{prop:Color} = 15066597
    ?tmp:CustomerStatus{prop:Trn} = 0
    ?List:7{prop:FontColor} = 65793
    ?List:7{prop:Color}= 16777215
    ?List:7{prop:Color,2} = 16777215
    ?List:7{prop:Color,3} = 12937777
    ?Tab11{prop:Color} = 15066597
    ?Prompt31{prop:FontColor} = -1
    ?Prompt31{prop:Color} = 15066597
    ?Prompt32{prop:FontColor} = -1
    ?Prompt32{prop:Color} = 15066597
    ?List:8{prop:FontColor} = 65793
    ?List:8{prop:Color}= 16777215
    ?List:8{prop:Color,2} = 16777215
    ?List:8{prop:Color,3} = 12937777
    ?Tab12{prop:Color} = 15066597
    ?Prompt33{prop:FontColor} = -1
    ?Prompt33{prop:Color} = 15066597
    ?Prompt34{prop:FontColor} = -1
    ?Prompt34{prop:Color} = 15066597
    ?List:9{prop:FontColor} = 65793
    ?List:9{prop:Color}= 16777215
    ?List:9{prop:Color,2} = 16777215
    ?List:9{prop:Color,3} = 12937777
    ?Tab13{prop:Color} = 15066597
    ?Prompt35{prop:FontColor} = -1
    ?Prompt35{prop:Color} = 15066597
    ?Prompt36{prop:FontColor} = -1
    ?Prompt36{prop:Color} = 15066597
    ?Prompt64{prop:FontColor} = -1
    ?Prompt64{prop:Color} = 15066597
    ?List:10{prop:FontColor} = 65793
    ?List:10{prop:Color}= 16777215
    ?List:10{prop:Color,2} = 16777215
    ?List:10{prop:Color,3} = 12937777
    ?Tab14{prop:Color} = 15066597
    ?Prompt37{prop:FontColor} = -1
    ?Prompt37{prop:Color} = 15066597
    ?Prompt38{prop:FontColor} = -1
    ?Prompt38{prop:Color} = 15066597
    ?tmp:SelectManufacturer{prop:Font,3} = -1
    ?tmp:SelectManufacturer{prop:Color} = 15066597
    ?tmp:SelectManufacturer{prop:Trn} = 0
    If ?tmp:Manufacturer{prop:ReadOnly} = True
        ?tmp:Manufacturer{prop:FontColor} = 65793
        ?tmp:Manufacturer{prop:Color} = 15066597
    Elsif ?tmp:Manufacturer{prop:Req} = True
        ?tmp:Manufacturer{prop:FontColor} = 65793
        ?tmp:Manufacturer{prop:Color} = 8454143
    Else ! If ?tmp:Manufacturer{prop:Req} = True
        ?tmp:Manufacturer{prop:FontColor} = 65793
        ?tmp:Manufacturer{prop:Color} = 16777215
    End ! If ?tmp:Manufacturer{prop:Req} = True
    ?tmp:Manufacturer{prop:Trn} = 0
    ?tmp:Manufacturer{prop:FontStyle} = font:Bold
    ?Prompt39{prop:FontColor} = -1
    ?Prompt39{prop:Color} = 15066597
    ?List:11{prop:FontColor} = 65793
    ?List:11{prop:Color}= 16777215
    ?List:11{prop:Color,2} = 16777215
    ?List:11{prop:Color,3} = 12937777
    ?Tab15{prop:Color} = 15066597
    ?Prompt40{prop:FontColor} = -1
    ?Prompt40{prop:Color} = 15066597
    ?Prompt41{prop:FontColor} = -1
    ?Prompt41{prop:Color} = 15066597
    ?List:12{prop:FontColor} = 65793
    ?List:12{prop:Color}= 16777215
    ?List:12{prop:Color,2} = 16777215
    ?List:12{prop:Color,3} = 12937777
    ?Tab16{prop:Color} = 15066597
    ?Prompt42{prop:FontColor} = -1
    ?Prompt42{prop:Color} = 15066597
    ?Prompt43{prop:FontColor} = -1
    ?Prompt43{prop:Color} = 15066597
    ?List:13{prop:FontColor} = 65793
    ?List:13{prop:Color}= 16777215
    ?List:13{prop:Color,2} = 16777215
    ?List:13{prop:Color,3} = 12937777
    ?Tab17{prop:Color} = 15066597
    ?Prompt44{prop:FontColor} = -1
    ?Prompt44{prop:Color} = 15066597
    ?Prompt45{prop:FontColor} = -1
    ?Prompt45{prop:Color} = 15066597
    ?List:14{prop:FontColor} = 65793
    ?List:14{prop:Color}= 16777215
    ?List:14{prop:Color,2} = 16777215
    ?List:14{prop:Color,3} = 12937777
    ?Tab20{prop:Color} = 15066597
    ?ColourTitle{prop:FontColor} = -1
    ?ColourTitle{prop:Color} = 15066597
    ?ColourText{prop:FontColor} = -1
    ?ColourText{prop:Color} = 15066597
    ?List:15{prop:FontColor} = 65793
    ?List:15{prop:Color}= 16777215
    ?List:15{prop:Color,2} = 16777215
    ?List:15{prop:Color,3} = 12937777
    ?Tab19{prop:Color} = 15066597
    ?Prompt53{prop:FontColor} = -1
    ?Prompt53{prop:Color} = 15066597
    ?Prompt54{prop:FontColor} = -1
    ?Prompt54{prop:Color} = 15066597
    ?tmp:JobBatchNumber:Prompt{prop:FontColor} = -1
    ?tmp:JobBatchNumber:Prompt{prop:Color} = 15066597
    If ?tmp:JobBatchNumber{prop:ReadOnly} = True
        ?tmp:JobBatchNumber{prop:FontColor} = 65793
        ?tmp:JobBatchNumber{prop:Color} = 15066597
    Elsif ?tmp:JobBatchNumber{prop:Req} = True
        ?tmp:JobBatchNumber{prop:FontColor} = 65793
        ?tmp:JobBatchNumber{prop:Color} = 8454143
    Else ! If ?tmp:JobBatchNumber{prop:Req} = True
        ?tmp:JobBatchNumber{prop:FontColor} = 65793
        ?tmp:JobBatchNumber{prop:Color} = 16777215
    End ! If ?tmp:JobBatchNumber{prop:Req} = True
    ?tmp:JobBatchNumber{prop:Trn} = 0
    ?tmp:JobBatchNumber{prop:FontStyle} = font:Bold
    ?tmp:EDIBatchNumber:Prompt{prop:FontColor} = -1
    ?tmp:EDIBatchNumber:Prompt{prop:Color} = 15066597
    If ?tmp:EDIBatchNumber{prop:ReadOnly} = True
        ?tmp:EDIBatchNumber{prop:FontColor} = 65793
        ?tmp:EDIBatchNumber{prop:Color} = 15066597
    Elsif ?tmp:EDIBatchNumber{prop:Req} = True
        ?tmp:EDIBatchNumber{prop:FontColor} = 65793
        ?tmp:EDIBatchNumber{prop:Color} = 8454143
    Else ! If ?tmp:EDIBatchNumber{prop:Req} = True
        ?tmp:EDIBatchNumber{prop:FontColor} = 65793
        ?tmp:EDIBatchNumber{prop:Color} = 16777215
    End ! If ?tmp:EDIBatchNumber{prop:Req} = True
    ?tmp:EDIBatchNumber{prop:Trn} = 0
    ?tmp:EDIBatchNumber{prop:FontStyle} = font:Bold
    ?Prompt57{prop:FontColor} = -1
    ?Prompt57{prop:Color} = 15066597
    ?Prompt58{prop:FontColor} = -1
    ?Prompt58{prop:Color} = 15066597
    ?Prompt59{prop:FontColor} = -1
    ?Prompt59{prop:Color} = 15066597
    ?tmp:ReportOrder{prop:FontColor} = 65793
    ?tmp:ReportOrder{prop:Color}= 16777215
    ?tmp:ReportOrder{prop:Color,2} = 16777215
    ?tmp:ReportOrder{prop:Color,3} = 12937777
    ?Prompt60{prop:FontColor} = -1
    ?Prompt60{prop:Color} = 15066597
    ?Prompt61{prop:FontColor} = -1
    ?Prompt61{prop:Color} = 15066597
    ?tmp:DateRangeType{prop:Font,3} = -1
    ?tmp:DateRangeType{prop:Color} = 15066597
    ?tmp:DateRangeType{prop:Trn} = 0
    ?tmp:DateRangeType:Radio1{prop:Font,3} = -1
    ?tmp:DateRangeType:Radio1{prop:Color} = 15066597
    ?tmp:DateRangeType:Radio1{prop:Trn} = 0
    ?tmp:DateRangeType:Radio2{prop:Font,3} = -1
    ?tmp:DateRangeType:Radio2{prop:Color} = 15066597
    ?tmp:DateRangeType:Radio2{prop:Trn} = 0
    ?tmp:DateRangeType:Radio3{prop:Font,3} = -1
    ?tmp:DateRangeType:Radio3{prop:Color} = 15066597
    ?tmp:DateRangeType:Radio3{prop:Trn} = 0
    ?tmp:StartDate:Prompt{prop:FontColor} = -1
    ?tmp:StartDate:Prompt{prop:Color} = 15066597
    If ?tmp:StartDate{prop:ReadOnly} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 15066597
    Elsif ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 8454143
    Else ! If ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 16777215
    End ! If ?tmp:StartDate{prop:Req} = True
    ?tmp:StartDate{prop:Trn} = 0
    ?tmp:StartDate{prop:FontStyle} = font:Bold
    ?tmp:EndDate:Prompt{prop:FontColor} = -1
    ?tmp:EndDate:Prompt{prop:Color} = 15066597
    If ?tmp:EndDate{prop:ReadOnly} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 15066597
    Elsif ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 8454143
    Else ! If ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 16777215
    End ! If ?tmp:EndDate{prop:Req} = True
    ?tmp:EndDate{prop:Trn} = 0
    ?tmp:EndDate{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?ButtonPanel{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::24:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW4.UpdateBuffer
   glo:Queue.Pointer = tra:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:HeadAccountTag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:HeadAccountTag = ''
  END
    Queue:Browse.tmp:HeadAccountTag = tmp:HeadAccountTag
  IF (tmp:HeadAccounttag = '*')
    Queue:Browse.tmp:HeadAccountTag_Icon = 2
  ELSE
    Queue:Browse.tmp:HeadAccountTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::24:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW4.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::24:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW4.Reset
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::24:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::24:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::24:QUEUE = glo:Queue
    ADD(DASBRW::24:QUEUE)
  END
  FREE(glo:Queue)
  BRW4.Reset
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::24:QUEUE.Pointer = tra:Account_Number
     GET(DASBRW::24:QUEUE,DASBRW::24:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = tra:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::24:DASSHOWTAG Routine
   CASE DASBRW::24:TAGDISPSTATUS
   OF 0
      DASBRW::24:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::24:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::24:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW4.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::25:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW5.UpdateBuffer
   glo:Queue2.Pointer2 = sub:Account_Number
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = sub:Account_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tmp:SubAccountTag = '*'
  ELSE
    DELETE(glo:Queue2)
    tmp:SubAccountTag = ''
  END
    Queue:Browse:1.tmp:SubAccountTag = tmp:SubAccountTag
  IF (tmp:SubAccounttag = '*')
    Queue:Browse:1.tmp:SubAccountTag_Icon = 2
  ELSE
    Queue:Browse:1.tmp:SubAccountTag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::25:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = sub:Account_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::25:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::25:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::25:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::25:QUEUE = glo:Queue2
    ADD(DASBRW::25:QUEUE)
  END
  FREE(glo:Queue2)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::25:QUEUE.Pointer2 = sub:Account_Number
     GET(DASBRW::25:QUEUE,DASBRW::25:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = sub:Account_Number
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::25:DASSHOWTAG Routine
   CASE DASBRW::25:TAGDISPSTATUS
   OF 0
      DASBRW::25:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::25:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::25:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::26:DASTAGONOFF Routine
  GET(Queue:Browse:2,CHOICE(?List:3))
  BRW6.UpdateBuffer
   glo:Queue3.Pointer3 = cha:Charge_Type
   GET(glo:Queue3,glo:Queue3.Pointer3)
  IF ERRORCODE()
     glo:Queue3.Pointer3 = cha:Charge_Type
     ADD(glo:Queue3,glo:Queue3.Pointer3)
    tmp:CChargeTypeTag = '*'
  ELSE
    DELETE(glo:Queue3)
    tmp:CChargeTypeTag = ''
  END
    Queue:Browse:2.tmp:CChargeTypeTag = tmp:CChargeTypeTag
  IF (tmp:CChargeTypetag = '*')
    Queue:Browse:2.tmp:CChargeTypeTag_Icon = 2
  ELSE
    Queue:Browse:2.tmp:CChargeTypeTag_Icon = 1
  END
  PUT(Queue:Browse:2)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::26:DASTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW6.Reset
  FREE(glo:Queue3)
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue3.Pointer3 = cha:Charge_Type
     ADD(glo:Queue3,glo:Queue3.Pointer3)
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::26:DASUNTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue3)
  BRW6.Reset
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::26:DASREVTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::26:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue3)
    GET(glo:Queue3,QR#)
    DASBRW::26:QUEUE = glo:Queue3
    ADD(DASBRW::26:QUEUE)
  END
  FREE(glo:Queue3)
  BRW6.Reset
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::26:QUEUE.Pointer3 = cha:Charge_Type
     GET(DASBRW::26:QUEUE,DASBRW::26:QUEUE.Pointer3)
    IF ERRORCODE()
       glo:Queue3.Pointer3 = cha:Charge_Type
       ADD(glo:Queue3,glo:Queue3.Pointer3)
    END
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::26:DASSHOWTAG Routine
   CASE DASBRW::26:TAGDISPSTATUS
   OF 0
      DASBRW::26:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::26:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::26:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:3{PROP:Text} = 'Show All'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:3{PROP:Text})
   BRW6.ResetSort(1)
   SELECT(?List:3,CHOICE(?List:3))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::27:DASTAGONOFF Routine
  GET(Queue:Browse:3,CHOICE(?List:4))
  BRW7.UpdateBuffer
   glo:Queue4.Pointer4 = cha:Charge_Type
   GET(glo:Queue4,glo:Queue4.Pointer4)
  IF ERRORCODE()
     glo:Queue4.Pointer4 = cha:Charge_Type
     ADD(glo:Queue4,glo:Queue4.Pointer4)
    tmp:WChargeType = '*'
  ELSE
    DELETE(glo:Queue4)
    tmp:WChargeType = ''
  END
    Queue:Browse:3.tmp:WChargeType = tmp:WChargeType
  IF (tmp:WChargeType = '*')
    Queue:Browse:3.tmp:WChargeType_Icon = 2
  ELSE
    Queue:Browse:3.tmp:WChargeType_Icon = 1
  END
  PUT(Queue:Browse:3)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::27:DASTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW7.Reset
  FREE(glo:Queue4)
  LOOP
    NEXT(BRW7::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue4.Pointer4 = cha:Charge_Type
     ADD(glo:Queue4,glo:Queue4.Pointer4)
  END
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::27:DASUNTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue4)
  BRW7.Reset
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::27:DASREVTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::27:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue4)
    GET(glo:Queue4,QR#)
    DASBRW::27:QUEUE = glo:Queue4
    ADD(DASBRW::27:QUEUE)
  END
  FREE(glo:Queue4)
  BRW7.Reset
  LOOP
    NEXT(BRW7::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::27:QUEUE.Pointer4 = cha:Charge_Type
     GET(DASBRW::27:QUEUE,DASBRW::27:QUEUE.Pointer4)
    IF ERRORCODE()
       glo:Queue4.Pointer4 = cha:Charge_Type
       ADD(glo:Queue4,glo:Queue4.Pointer4)
    END
  END
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::27:DASSHOWTAG Routine
   CASE DASBRW::27:TAGDISPSTATUS
   OF 0
      DASBRW::27:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:4{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::27:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:4{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::27:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:4{PROP:Text} = 'Show All'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:4{PROP:Text})
   BRW7.ResetSort(1)
   SELECT(?List:4,CHOICE(?List:4))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::28:DASTAGONOFF Routine
  GET(Queue:Browse:4,CHOICE(?List:5))
  BRW8.UpdateBuffer
   glo:Queue5.Pointer5 = rtd:Repair_Type
   GET(glo:Queue5,glo:Queue5.Pointer5)
  IF ERRORCODE()
     glo:Queue5.Pointer5 = rtd:Repair_Type
     ADD(glo:Queue5,glo:Queue5.Pointer5)
    tmp:CRepairTypeTag = '*'
  ELSE
    DELETE(glo:Queue5)
    tmp:CRepairTypeTag = ''
  END
    Queue:Browse:4.tmp:CRepairTypeTag = tmp:CRepairTypeTag
  IF (tmp:CRepairTypetag = '*')
    Queue:Browse:4.tmp:CRepairTypeTag_Icon = 2
  ELSE
    Queue:Browse:4.tmp:CRepairTypeTag_Icon = 1
  END
  PUT(Queue:Browse:4)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::28:DASTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW8.Reset
  FREE(glo:Queue5)
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue5.Pointer5 = rtd:Repair_Type
     ADD(glo:Queue5,glo:Queue5.Pointer5)
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::28:DASUNTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue5)
  BRW8.Reset
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::28:DASREVTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::28:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue5)
    GET(glo:Queue5,QR#)
    DASBRW::28:QUEUE = glo:Queue5
    ADD(DASBRW::28:QUEUE)
  END
  FREE(glo:Queue5)
  BRW8.Reset
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::28:QUEUE.Pointer5 = rtd:Repair_Type
     GET(DASBRW::28:QUEUE,DASBRW::28:QUEUE.Pointer5)
    IF ERRORCODE()
       glo:Queue5.Pointer5 = rtd:Repair_Type
       ADD(glo:Queue5,glo:Queue5.Pointer5)
    END
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::28:DASSHOWTAG Routine
   CASE DASBRW::28:TAGDISPSTATUS
   OF 0
      DASBRW::28:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:5{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:5{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:5{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::28:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:5{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:5{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:5{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::28:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:5{PROP:Text} = 'Show All'
      ?DASSHOWTAG:5{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:5{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:5{PROP:Text})
   BRW8.ResetSort(1)
   SELECT(?List:5,CHOICE(?List:5))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::29:DASTAGONOFF Routine
  GET(Queue:Browse:5,CHOICE(?List:6))
  BRW9.UpdateBuffer
   glo:Queue6.Pointer6 = rtd:Repair_Type
   GET(glo:Queue6,glo:Queue6.Pointer6)
  IF ERRORCODE()
     glo:Queue6.Pointer6 = rtd:Repair_Type
     ADD(glo:Queue6,glo:Queue6.Pointer6)
    tmp:WRepairTypeTag = '*'
  ELSE
    DELETE(glo:Queue6)
    tmp:WRepairTypeTag = ''
  END
    Queue:Browse:5.tmp:WRepairTypeTag = tmp:WRepairTypeTag
  IF (tmp:WRepairTypetag = '*')
    Queue:Browse:5.tmp:WRepairTypeTag_Icon = 2
  ELSE
    Queue:Browse:5.tmp:WRepairTypeTag_Icon = 1
  END
  PUT(Queue:Browse:5)
  SELECT(?List:6,CHOICE(?List:6))
DASBRW::29:DASTAGALL Routine
  ?List:6{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW9.Reset
  FREE(glo:Queue6)
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue6.Pointer6 = rtd:Repair_Type
     ADD(glo:Queue6,glo:Queue6.Pointer6)
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:6,CHOICE(?List:6))
DASBRW::29:DASUNTAGALL Routine
  ?List:6{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue6)
  BRW9.Reset
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:6,CHOICE(?List:6))
DASBRW::29:DASREVTAGALL Routine
  ?List:6{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::29:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue6)
    GET(glo:Queue6,QR#)
    DASBRW::29:QUEUE = glo:Queue6
    ADD(DASBRW::29:QUEUE)
  END
  FREE(glo:Queue6)
  BRW9.Reset
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::29:QUEUE.Pointer6 = rtd:Repair_Type
     GET(DASBRW::29:QUEUE,DASBRW::29:QUEUE.Pointer6)
    IF ERRORCODE()
       glo:Queue6.Pointer6 = rtd:Repair_Type
       ADD(glo:Queue6,glo:Queue6.Pointer6)
    END
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:6,CHOICE(?List:6))
DASBRW::29:DASSHOWTAG Routine
   CASE DASBRW::29:TAGDISPSTATUS
   OF 0
      DASBRW::29:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:6{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:6{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:6{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::29:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:6{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:6{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:6{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::29:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:6{PROP:Text} = 'Show All'
      ?DASSHOWTAG:6{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:6{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:6{PROP:Text})
   BRW9.ResetSort(1)
   SELECT(?List:6,CHOICE(?List:6))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::30:DASTAGONOFF Routine
  GET(Queue:Browse:6,CHOICE(?List:7))
  BRW10.UpdateBuffer
   glo:Queue7.Pointer7 = sts:Status
   GET(glo:Queue7,glo:Queue7.Pointer7)
  IF ERRORCODE()
     glo:Queue7.Pointer7 = sts:Status
     ADD(glo:Queue7,glo:Queue7.Pointer7)
    tmp:StatusTag = '*'
  ELSE
    DELETE(glo:Queue7)
    tmp:StatusTag = ''
  END
    Queue:Browse:6.tmp:StatusTag = tmp:StatusTag
  IF (tmp:Statustag = '*')
    Queue:Browse:6.tmp:StatusTag_Icon = 2
  ELSE
    Queue:Browse:6.tmp:StatusTag_Icon = 1
  END
  PUT(Queue:Browse:6)
  SELECT(?List:7,CHOICE(?List:7))
DASBRW::30:DASTAGALL Routine
  ?List:7{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW10.Reset
  FREE(glo:Queue7)
  LOOP
    NEXT(BRW10::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue7.Pointer7 = sts:Status
     ADD(glo:Queue7,glo:Queue7.Pointer7)
  END
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List:7,CHOICE(?List:7))
DASBRW::30:DASUNTAGALL Routine
  ?List:7{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue7)
  BRW10.Reset
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List:7,CHOICE(?List:7))
DASBRW::30:DASREVTAGALL Routine
  ?List:7{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::30:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue7)
    GET(glo:Queue7,QR#)
    DASBRW::30:QUEUE = glo:Queue7
    ADD(DASBRW::30:QUEUE)
  END
  FREE(glo:Queue7)
  BRW10.Reset
  LOOP
    NEXT(BRW10::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::30:QUEUE.Pointer7 = sts:Status
     GET(DASBRW::30:QUEUE,DASBRW::30:QUEUE.Pointer7)
    IF ERRORCODE()
       glo:Queue7.Pointer7 = sts:Status
       ADD(glo:Queue7,glo:Queue7.Pointer7)
    END
  END
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List:7,CHOICE(?List:7))
DASBRW::30:DASSHOWTAG Routine
   CASE DASBRW::30:TAGDISPSTATUS
   OF 0
      DASBRW::30:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:7{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:7{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:7{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::30:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:7{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:7{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:7{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::30:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:7{PROP:Text} = 'Show All'
      ?DASSHOWTAG:7{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:7{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:7{PROP:Text})
   BRW10.ResetSort(1)
   SELECT(?List:7,CHOICE(?List:7))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::31:DASTAGONOFF Routine
  GET(Queue:Browse:7,CHOICE(?List:8))
  BRW14.UpdateBuffer
   glo:Queue8.Pointer8 = loi:Location
   GET(glo:Queue8,glo:Queue8.Pointer8)
  IF ERRORCODE()
     glo:Queue8.Pointer8 = loi:Location
     ADD(glo:Queue8,glo:Queue8.Pointer8)
    tmp:LocationTag = '*'
  ELSE
    DELETE(glo:Queue8)
    tmp:LocationTag = ''
  END
    Queue:Browse:7.tmp:LocationTag = tmp:LocationTag
  IF (tmp:Locationtag = '*')
    Queue:Browse:7.tmp:LocationTag_Icon = 2
  ELSE
    Queue:Browse:7.tmp:LocationTag_Icon = 1
  END
  PUT(Queue:Browse:7)
  SELECT(?List:8,CHOICE(?List:8))
DASBRW::31:DASTAGALL Routine
  ?List:8{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW14.Reset
  FREE(glo:Queue8)
  LOOP
    NEXT(BRW14::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue8.Pointer8 = loi:Location
     ADD(glo:Queue8,glo:Queue8.Pointer8)
  END
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:8,CHOICE(?List:8))
DASBRW::31:DASUNTAGALL Routine
  ?List:8{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue8)
  BRW14.Reset
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:8,CHOICE(?List:8))
DASBRW::31:DASREVTAGALL Routine
  ?List:8{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::31:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue8)
    GET(glo:Queue8,QR#)
    DASBRW::31:QUEUE = glo:Queue8
    ADD(DASBRW::31:QUEUE)
  END
  FREE(glo:Queue8)
  BRW14.Reset
  LOOP
    NEXT(BRW14::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::31:QUEUE.Pointer8 = loi:Location
     GET(DASBRW::31:QUEUE,DASBRW::31:QUEUE.Pointer8)
    IF ERRORCODE()
       glo:Queue8.Pointer8 = loi:Location
       ADD(glo:Queue8,glo:Queue8.Pointer8)
    END
  END
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:8,CHOICE(?List:8))
DASBRW::31:DASSHOWTAG Routine
   CASE DASBRW::31:TAGDISPSTATUS
   OF 0
      DASBRW::31:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:8{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:8{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:8{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::31:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:8{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:8{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:8{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::31:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:8{PROP:Text} = 'Show All'
      ?DASSHOWTAG:8{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:8{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:8{PROP:Text})
   BRW14.ResetSort(1)
   SELECT(?List:8,CHOICE(?List:8))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::32:DASTAGONOFF Routine
  GET(Queue:Browse:8,CHOICE(?List:9))
  BRW15.UpdateBuffer
   glo:Queue9.Pointer9 = use:User_Code
   GET(glo:Queue9,glo:Queue9.Pointer9)
  IF ERRORCODE()
     glo:Queue9.Pointer9 = use:User_Code
     ADD(glo:Queue9,glo:Queue9.Pointer9)
    tmp:UserTag = '*'
  ELSE
    DELETE(glo:Queue9)
    tmp:UserTag = ''
  END
    Queue:Browse:8.tmp:UserTag = tmp:UserTag
  IF (tmp:Usertag = '*')
    Queue:Browse:8.tmp:UserTag_Icon = 2
  ELSE
    Queue:Browse:8.tmp:UserTag_Icon = 1
  END
  PUT(Queue:Browse:8)
  SELECT(?List:9,CHOICE(?List:9))
DASBRW::32:DASTAGALL Routine
  ?List:9{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW15.Reset
  FREE(glo:Queue9)
  LOOP
    NEXT(BRW15::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue9.Pointer9 = use:User_Code
     ADD(glo:Queue9,glo:Queue9.Pointer9)
  END
  SETCURSOR
  BRW15.ResetSort(1)
  SELECT(?List:9,CHOICE(?List:9))
DASBRW::32:DASUNTAGALL Routine
  ?List:9{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue9)
  BRW15.Reset
  SETCURSOR
  BRW15.ResetSort(1)
  SELECT(?List:9,CHOICE(?List:9))
DASBRW::32:DASREVTAGALL Routine
  ?List:9{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::32:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue9)
    GET(glo:Queue9,QR#)
    DASBRW::32:QUEUE = glo:Queue9
    ADD(DASBRW::32:QUEUE)
  END
  FREE(glo:Queue9)
  BRW15.Reset
  LOOP
    NEXT(BRW15::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::32:QUEUE.Pointer9 = use:User_Code
     GET(DASBRW::32:QUEUE,DASBRW::32:QUEUE.Pointer9)
    IF ERRORCODE()
       glo:Queue9.Pointer9 = use:User_Code
       ADD(glo:Queue9,glo:Queue9.Pointer9)
    END
  END
  SETCURSOR
  BRW15.ResetSort(1)
  SELECT(?List:9,CHOICE(?List:9))
DASBRW::32:DASSHOWTAG Routine
   CASE DASBRW::32:TAGDISPSTATUS
   OF 0
      DASBRW::32:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:9{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:9{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:9{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::32:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:9{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:9{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:9{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::32:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:9{PROP:Text} = 'Show All'
      ?DASSHOWTAG:9{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:9{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:9{PROP:Text})
   BRW15.ResetSort(1)
   SELECT(?List:9,CHOICE(?List:9))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::33:DASTAGONOFF Routine
  GET(Queue:Browse:9,CHOICE(?List:10))
  BRW16.UpdateBuffer
   glo:Queue10.Pointer10 = man:Manufacturer
   GET(glo:Queue10,glo:Queue10.Pointer10)
  IF ERRORCODE()
     glo:Queue10.Pointer10 = man:Manufacturer
     ADD(glo:Queue10,glo:Queue10.Pointer10)
    tmp:ManufacturerTag = '*'
  ELSE
    DELETE(glo:Queue10)
    tmp:ManufacturerTag = ''
  END
    Queue:Browse:9.tmp:ManufacturerTag = tmp:ManufacturerTag
  IF (tmp:Manufacturertag = '*')
    Queue:Browse:9.tmp:ManufacturerTag_Icon = 2
  ELSE
    Queue:Browse:9.tmp:ManufacturerTag_Icon = 1
  END
  PUT(Queue:Browse:9)
  SELECT(?List:10,CHOICE(?List:10))
DASBRW::33:DASTAGALL Routine
  ?List:10{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW16.Reset
  FREE(glo:Queue10)
  LOOP
    NEXT(BRW16::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue10.Pointer10 = man:Manufacturer
     ADD(glo:Queue10,glo:Queue10.Pointer10)
  END
  SETCURSOR
  BRW16.ResetSort(1)
  SELECT(?List:10,CHOICE(?List:10))
DASBRW::33:DASUNTAGALL Routine
  ?List:10{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue10)
  BRW16.Reset
  SETCURSOR
  BRW16.ResetSort(1)
  SELECT(?List:10,CHOICE(?List:10))
DASBRW::33:DASREVTAGALL Routine
  ?List:10{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::33:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue10)
    GET(glo:Queue10,QR#)
    DASBRW::33:QUEUE = glo:Queue10
    ADD(DASBRW::33:QUEUE)
  END
  FREE(glo:Queue10)
  BRW16.Reset
  LOOP
    NEXT(BRW16::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::33:QUEUE.Pointer10 = man:Manufacturer
     GET(DASBRW::33:QUEUE,DASBRW::33:QUEUE.Pointer10)
    IF ERRORCODE()
       glo:Queue10.Pointer10 = man:Manufacturer
       ADD(glo:Queue10,glo:Queue10.Pointer10)
    END
  END
  SETCURSOR
  BRW16.ResetSort(1)
  SELECT(?List:10,CHOICE(?List:10))
DASBRW::33:DASSHOWTAG Routine
   CASE DASBRW::33:TAGDISPSTATUS
   OF 0
      DASBRW::33:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:10{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:10{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:10{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::33:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:10{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:10{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:10{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::33:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:10{PROP:Text} = 'Show All'
      ?DASSHOWTAG:10{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:10{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:10{PROP:Text})
   BRW16.ResetSort(1)
   SELECT(?List:10,CHOICE(?List:10))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::35:DASTAGONOFF Routine
  GET(Queue:Browse:10,CHOICE(?List:11))
  BRW17.UpdateBuffer
   glo:Queue11.Pointer11 = mod:Model_Number
   GET(glo:Queue11,glo:Queue11.Pointer11)
  IF ERRORCODE()
     glo:Queue11.Pointer11 = mod:Model_Number
     ADD(glo:Queue11,glo:Queue11.Pointer11)
    tmp:ModelNumberTag = '*'
  ELSE
    DELETE(glo:Queue11)
    tmp:ModelNumberTag = ''
  END
    Queue:Browse:10.tmp:ModelNumberTag = tmp:ModelNumberTag
  IF (tmp:ModelNumbertag = '*')
    Queue:Browse:10.tmp:ModelNumberTag_Icon = 2
  ELSE
    Queue:Browse:10.tmp:ModelNumberTag_Icon = 1
  END
  PUT(Queue:Browse:10)
  SELECT(?List:11,CHOICE(?List:11))
DASBRW::35:DASTAGALL Routine
  ?List:11{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW17.Reset
  FREE(glo:Queue11)
  LOOP
    NEXT(BRW17::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue11.Pointer11 = mod:Model_Number
     ADD(glo:Queue11,glo:Queue11.Pointer11)
  END
  SETCURSOR
  BRW17.ResetSort(1)
  SELECT(?List:11,CHOICE(?List:11))
DASBRW::35:DASUNTAGALL Routine
  ?List:11{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue11)
  BRW17.Reset
  SETCURSOR
  BRW17.ResetSort(1)
  SELECT(?List:11,CHOICE(?List:11))
DASBRW::35:DASREVTAGALL Routine
  ?List:11{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::35:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue11)
    GET(glo:Queue11,QR#)
    DASBRW::35:QUEUE = glo:Queue11
    ADD(DASBRW::35:QUEUE)
  END
  FREE(glo:Queue11)
  BRW17.Reset
  LOOP
    NEXT(BRW17::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::35:QUEUE.Pointer11 = mod:Model_Number
     GET(DASBRW::35:QUEUE,DASBRW::35:QUEUE.Pointer11)
    IF ERRORCODE()
       glo:Queue11.Pointer11 = mod:Model_Number
       ADD(glo:Queue11,glo:Queue11.Pointer11)
    END
  END
  SETCURSOR
  BRW17.ResetSort(1)
  SELECT(?List:11,CHOICE(?List:11))
DASBRW::35:DASSHOWTAG Routine
   CASE DASBRW::35:TAGDISPSTATUS
   OF 0
      DASBRW::35:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:11{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:11{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:11{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::35:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:11{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:11{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:11{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::35:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:11{PROP:Text} = 'Show All'
      ?DASSHOWTAG:11{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:11{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:11{PROP:Text})
   BRW17.ResetSort(1)
   SELECT(?List:11,CHOICE(?List:11))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::36:DASTAGONOFF Routine
  GET(Queue:Browse:11,CHOICE(?List:12))
  BRW21.UpdateBuffer
   glo:Queue12.Pointer12 = uni:Unit_Type
   GET(glo:Queue12,glo:Queue12.Pointer12)
  IF ERRORCODE()
     glo:Queue12.Pointer12 = uni:Unit_Type
     ADD(glo:Queue12,glo:Queue12.Pointer12)
    tmp:UnitTypeTag = '*'
  ELSE
    DELETE(glo:Queue12)
    tmp:UnitTypeTag = ''
  END
    Queue:Browse:11.tmp:UnitTypeTag = tmp:UnitTypeTag
  IF (tmp:UnitTypetag = '*')
    Queue:Browse:11.tmp:UnitTypeTag_Icon = 2
  ELSE
    Queue:Browse:11.tmp:UnitTypeTag_Icon = 1
  END
  PUT(Queue:Browse:11)
  SELECT(?List:12,CHOICE(?List:12))
DASBRW::36:DASTAGALL Routine
  ?List:12{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW21.Reset
  FREE(glo:Queue12)
  LOOP
    NEXT(BRW21::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue12.Pointer12 = uni:Unit_Type
     ADD(glo:Queue12,glo:Queue12.Pointer12)
  END
  SETCURSOR
  BRW21.ResetSort(1)
  SELECT(?List:12,CHOICE(?List:12))
DASBRW::36:DASUNTAGALL Routine
  ?List:12{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue12)
  BRW21.Reset
  SETCURSOR
  BRW21.ResetSort(1)
  SELECT(?List:12,CHOICE(?List:12))
DASBRW::36:DASREVTAGALL Routine
  ?List:12{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::36:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue12)
    GET(glo:Queue12,QR#)
    DASBRW::36:QUEUE = glo:Queue12
    ADD(DASBRW::36:QUEUE)
  END
  FREE(glo:Queue12)
  BRW21.Reset
  LOOP
    NEXT(BRW21::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::36:QUEUE.Pointer12 = uni:Unit_Type
     GET(DASBRW::36:QUEUE,DASBRW::36:QUEUE.Pointer12)
    IF ERRORCODE()
       glo:Queue12.Pointer12 = uni:Unit_Type
       ADD(glo:Queue12,glo:Queue12.Pointer12)
    END
  END
  SETCURSOR
  BRW21.ResetSort(1)
  SELECT(?List:12,CHOICE(?List:12))
DASBRW::36:DASSHOWTAG Routine
   CASE DASBRW::36:TAGDISPSTATUS
   OF 0
      DASBRW::36:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:12{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:12{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:12{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::36:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:12{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:12{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:12{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::36:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:12{PROP:Text} = 'Show All'
      ?DASSHOWTAG:12{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:12{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:12{PROP:Text})
   BRW21.ResetSort(1)
   SELECT(?List:12,CHOICE(?List:12))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::37:DASTAGONOFF Routine
  GET(Queue:Browse:12,CHOICE(?List:13))
  BRW22.UpdateBuffer
   glo:Queue13.Pointer13 = trt:Transit_Type
   GET(glo:Queue13,glo:Queue13.Pointer13)
  IF ERRORCODE()
     glo:Queue13.Pointer13 = trt:Transit_Type
     ADD(glo:Queue13,glo:Queue13.Pointer13)
    tmp:TransitTypeTag = '*'
  ELSE
    DELETE(glo:Queue13)
    tmp:TransitTypeTag = ''
  END
    Queue:Browse:12.tmp:TransitTypeTag = tmp:TransitTypeTag
  IF (tmp:TransitTypetag = '*')
    Queue:Browse:12.tmp:TransitTypeTag_Icon = 2
  ELSE
    Queue:Browse:12.tmp:TransitTypeTag_Icon = 1
  END
  PUT(Queue:Browse:12)
  SELECT(?List:13,CHOICE(?List:13))
DASBRW::37:DASTAGALL Routine
  ?List:13{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW22.Reset
  FREE(glo:Queue13)
  LOOP
    NEXT(BRW22::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue13.Pointer13 = trt:Transit_Type
     ADD(glo:Queue13,glo:Queue13.Pointer13)
  END
  SETCURSOR
  BRW22.ResetSort(1)
  SELECT(?List:13,CHOICE(?List:13))
DASBRW::37:DASUNTAGALL Routine
  ?List:13{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue13)
  BRW22.Reset
  SETCURSOR
  BRW22.ResetSort(1)
  SELECT(?List:13,CHOICE(?List:13))
DASBRW::37:DASREVTAGALL Routine
  ?List:13{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::37:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue13)
    GET(glo:Queue13,QR#)
    DASBRW::37:QUEUE = glo:Queue13
    ADD(DASBRW::37:QUEUE)
  END
  FREE(glo:Queue13)
  BRW22.Reset
  LOOP
    NEXT(BRW22::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::37:QUEUE.Pointer13 = trt:Transit_Type
     GET(DASBRW::37:QUEUE,DASBRW::37:QUEUE.Pointer13)
    IF ERRORCODE()
       glo:Queue13.Pointer13 = trt:Transit_Type
       ADD(glo:Queue13,glo:Queue13.Pointer13)
    END
  END
  SETCURSOR
  BRW22.ResetSort(1)
  SELECT(?List:13,CHOICE(?List:13))
DASBRW::37:DASSHOWTAG Routine
   CASE DASBRW::37:TAGDISPSTATUS
   OF 0
      DASBRW::37:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:13{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:13{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:13{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::37:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:13{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:13{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:13{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::37:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:13{PROP:Text} = 'Show All'
      ?DASSHOWTAG:13{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:13{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:13{PROP:Text})
   BRW22.ResetSort(1)
   SELECT(?List:13,CHOICE(?List:13))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::38:DASTAGONOFF Routine
  GET(Queue:Browse:13,CHOICE(?List:14))
  BRW23.UpdateBuffer
   glo:Queue14.Pointer14 = tur:Turnaround_Time
   GET(glo:Queue14,glo:Queue14.Pointer14)
  IF ERRORCODE()
     glo:Queue14.Pointer14 = tur:Turnaround_Time
     ADD(glo:Queue14,glo:Queue14.Pointer14)
    tmp:TurnaroundTimeTag = '*'
  ELSE
    DELETE(glo:Queue14)
    tmp:TurnaroundTimeTag = ''
  END
    Queue:Browse:13.tmp:TurnaroundTimeTag = tmp:TurnaroundTimeTag
  IF (tmp:TurnaroundTimetag = '*')
    Queue:Browse:13.tmp:TurnaroundTimeTag_Icon = 2
  ELSE
    Queue:Browse:13.tmp:TurnaroundTimeTag_Icon = 1
  END
  PUT(Queue:Browse:13)
  SELECT(?List:14,CHOICE(?List:14))
DASBRW::38:DASTAGALL Routine
  ?List:14{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW23.Reset
  FREE(glo:Queue14)
  LOOP
    NEXT(BRW23::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue14.Pointer14 = tur:Turnaround_Time
     ADD(glo:Queue14,glo:Queue14.Pointer14)
  END
  SETCURSOR
  BRW23.ResetSort(1)
  SELECT(?List:14,CHOICE(?List:14))
DASBRW::38:DASUNTAGALL Routine
  ?List:14{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue14)
  BRW23.Reset
  SETCURSOR
  BRW23.ResetSort(1)
  SELECT(?List:14,CHOICE(?List:14))
DASBRW::38:DASREVTAGALL Routine
  ?List:14{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::38:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue14)
    GET(glo:Queue14,QR#)
    DASBRW::38:QUEUE = glo:Queue14
    ADD(DASBRW::38:QUEUE)
  END
  FREE(glo:Queue14)
  BRW23.Reset
  LOOP
    NEXT(BRW23::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::38:QUEUE.Pointer14 = tur:Turnaround_Time
     GET(DASBRW::38:QUEUE,DASBRW::38:QUEUE.Pointer14)
    IF ERRORCODE()
       glo:Queue14.Pointer14 = tur:Turnaround_Time
       ADD(glo:Queue14,glo:Queue14.Pointer14)
    END
  END
  SETCURSOR
  BRW23.ResetSort(1)
  SELECT(?List:14,CHOICE(?List:14))
DASBRW::38:DASSHOWTAG Routine
   CASE DASBRW::38:TAGDISPSTATUS
   OF 0
      DASBRW::38:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:14{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:14{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:14{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::38:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:14{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:14{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:14{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::38:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:14{PROP:Text} = 'Show All'
      ?DASSHOWTAG:14{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:14{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:14{PROP:Text})
   BRW23.ResetSort(1)
   SELECT(?List:14,CHOICE(?List:14))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::43:DASTAGONOFF Routine
  GET(Queue:Browse:14,CHOICE(?List:15))
  BRW42.UpdateBuffer
   glo:Queue15.Pointer15 = col:Colour
   GET(glo:Queue15,glo:Queue15.Pointer15)
  IF ERRORCODE()
     glo:Queue15.Pointer15 = col:Colour
     ADD(glo:Queue15,glo:Queue15.Pointer15)
    tmp:ColourTag = '*'
  ELSE
    DELETE(glo:Queue15)
    tmp:ColourTag = ''
  END
    Queue:Browse:14.tmp:ColourTag = tmp:ColourTag
  IF (tmp:Colourtag = '*')
    Queue:Browse:14.tmp:ColourTag_Icon = 2
  ELSE
    Queue:Browse:14.tmp:ColourTag_Icon = 1
  END
  PUT(Queue:Browse:14)
  SELECT(?List:15,CHOICE(?List:15))
DASBRW::43:DASTAGALL Routine
  ?List:15{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW42.Reset
  FREE(glo:Queue15)
  LOOP
    NEXT(BRW42::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue15.Pointer15 = col:Colour
     ADD(glo:Queue15,glo:Queue15.Pointer15)
  END
  SETCURSOR
  BRW42.ResetSort(1)
  SELECT(?List:15,CHOICE(?List:15))
DASBRW::43:DASUNTAGALL Routine
  ?List:15{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue15)
  BRW42.Reset
  SETCURSOR
  BRW42.ResetSort(1)
  SELECT(?List:15,CHOICE(?List:15))
DASBRW::43:DASREVTAGALL Routine
  ?List:15{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::43:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue15)
    GET(glo:Queue15,QR#)
    DASBRW::43:QUEUE = glo:Queue15
    ADD(DASBRW::43:QUEUE)
  END
  FREE(glo:Queue15)
  BRW42.Reset
  LOOP
    NEXT(BRW42::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::43:QUEUE.Pointer15 = col:Colour
     GET(DASBRW::43:QUEUE,DASBRW::43:QUEUE.Pointer15)
    IF ERRORCODE()
       glo:Queue15.Pointer15 = col:Colour
       ADD(glo:Queue15,glo:Queue15.Pointer15)
    END
  END
  SETCURSOR
  BRW42.ResetSort(1)
  SELECT(?List:15,CHOICE(?List:15))
DASBRW::43:DASSHOWTAG Routine
   CASE DASBRW::43:TAGDISPSTATUS
   OF 0
      DASBRW::43:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:15{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:15{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:15{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::43:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:15{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:15{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:15{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::43:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:15{PROP:Text} = 'Show All'
      ?DASSHOWTAG:15{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:15{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:15{PROP:Text})
   BRW42.ResetSort(1)
   SELECT(?List:15,CHOICE(?List:15))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
StatusReport        Routine
    StatusReport(tmp:DateRangeType,tmp:StartDate,tmp:EndDate,tmp:JobType,tmp:InvoiceType,tmp:DespatchedType,tmp:CompletedType,tmp:StatusType,tmp:ReportOrder,tmp:PaperReportType,tmp:JobBatchNumber,tmp:EDIBatchNumber,tmp:CustomerStatus)
SaveCriteria        Routine
    tmp:NewDescription = InsertCriteriaDescription()
    If tmp:NewDescription <> ''
        If Access:STATREP.PrimeRecord() = Level:Benign
            star:Description = tmp:NewDescription
            If Access:STATREP.TryInsert() = Level:Benign
                !Insert Successful
            Else !If Access:STATREP..TryInsert() = Level:Benign
                !Insert Failed
            End !If Access:STATREP..TryInsert() = Level:Benign

            Loop x# = 1 To Records(glo:Queue)
                Get(glo:Queue,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'HEAD ACCOUNT'
                    stac:FieldValue  = glo:Pointer
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue2)
                Get(glo:Queue2,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'SUB ACCOUNT'
                    stac:FieldValue  = glo:Pointer2
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue3)
                Get(glo:Queue3,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'CHA CHARGE TYPE'
                    stac:FieldValue  = glo:Pointer3
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue4)
                Get(glo:Queue4,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'WAR CHARGE TYPE'
                    stac:FieldValue  = glo:Pointer4
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue5)
                Get(glo:Queue5,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'CHA REPAIR TYPE'
                    stac:FieldValue  = glo:Pointer5
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue6)
                Get(glo:Queue6,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'WAR REPAIR TYPE'
                    stac:FieldValue  = glo:Pointer6
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue7)
                Get(glo:Queue7,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'STATUS'
                    stac:FieldValue  = glo:Pointer7
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue8)
                Get(glo:Queue8,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'LOCATION'
                    stac:FieldValue  = glo:Pointer8
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)


            Loop x# = 1 To Records(glo:Queue9)
                Get(glo:Queue9,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'ENGINEER'
                    stac:FieldValue  = glo:Pointer9
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue10)
                Get(glo:Queue10,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'MANUFACTURER'
                    stac:FieldValue  = glo:Pointer10
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue11)
                Get(glo:Queue11,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'MODEL NUMBER'
                    stac:FieldValue  = glo:Pointer11
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue12)
                Get(glo:Queue12,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'UNIT TYPE'
                    stac:FieldValue  = glo:Pointer12
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue13)
                Get(glo:Queue13,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'TRANSIT TYPE'
                    stac:FieldValue  = glo:Pointer13
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            Loop x# = 1 To Records(glo:Queue14)
                Get(glo:Queue14,x#)
                If Access:STATCRIT.PrimeRecord() = Level:Benign
                    stac:Description = tmp:NewDescription
                    stac:OptionType  = 'TURNAROUND TIME'
                    stac:FieldValue  = glo:Pointer14
                    If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:STATCRIT.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:STATCRIT.TryInsert() = Level:Benign
                End !If Access:STATCRIT.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(glo:Queue)

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'OUTPUT TYPE'
                stac:FieldValue     = tmp:OutputType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'PAPER REPORT TYPE'
                stac:FieldValue     = tmp:PaperReportType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'EXPORT JOBS'
                stac:FieldValue     = tmp:ExportJobs
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'EXPORT AUDIT'
                stac:FieldValue     = tmp:ExportAudit
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'EXPORT PARTS'
                stac:FieldValue     = tmp:ExportParts
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'EXPORT WARPARTS'
                stac:FieldValue     = tmp:ExportWarParts
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'STATUS TYPE'
                stac:FieldValue     = tmp:StatusType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'SELECT MANUFACTURER'
                stac:FieldValue     = tmp:SelectManufacturer
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'PICK MANUFACTURER'
                stac:FieldValue     = tmp:Manufacturer
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'WORKSHOP'
                stac:FieldValue     = tmp:Workshop
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'JOBTYPE'
                stac:FieldValue     = tmp:JobType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'COMPLETED TYPE'
                stac:FieldValue     = tmp:CompletedType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'DESPATCHED TYPE'
                stac:FieldValue     = tmp:DespatchedType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'JOB BATCH NUMBER'
                stac:FieldValue     = tmp:JobBatchNumber
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'EDI BATCH NUMBER'
                stac:FieldValue     = tmp:EDIBatchNumber
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'REPORT ORDER'
                stac:FieldValue     = tmp:ReportOrder
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

            If Access:STATCRIT.PrimeRecord() = Level:Benign
                stac:Description    = tmp:NewDescription
                stac:OptionType     = 'DATE RANGE TYPE'
                stac:FieldValue     = tmp:DateRangeType
                If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:STATCRIT.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:STATCRIT.TryInsert() = Level:Benign
            End !If Access:STATCRIT.PrimeRecord() = Level:Benign

        End !If Access:STATREP..PrimeRecord() = Level:Benign
    End !If tmp:NewDescription <> ''
Export      Routine
    !if this is for a summary report just go through all the checks
    !and give a total at the end, just don't export anything.
    If tmp:OutputType = 1 And tmp:PaperReportType = 1
        JobsError# = 1
        AuditError# = 1
        PartsError# = 1
        WarPartsError# = 1

    Else !If tmp:OutputType = 1 And tmp:PaperReportType = 1


        Remove(local:FileNameJobs)
        Remove(local:FileNameAudit)
        Remove(Local:FileNameParts)
        Remove(Local:FileNameWarParts)

        JobsError# = 0
        If tmp:ExportJobs
            Open(ExportFileJobs)
            If Error()
                Create(ExportFileJobs)
                Open(ExportFileJobs)
                If Error()
                    JobsError# = 1
                End !If Error()
            End !If Error()
        Else !If tmp:ExportJobs
            JobsError# = 1
        End !If tmp:ExportJobs

        AuditError# = 0
        If tmp:ExportAudit
            Open(ExportFileAudit)
            If Error()
                Create(ExportFileAudit)
                Open(ExportFileAudit)
                IF Error()
                    AuditError# = 1
                End !IF Error()
            End !If Error()
        Else !If tmp:ExportAudit
            AuditError# = 1
        End !If tmp:ExportAudit

        PartsError# = 0
        If tmp:ExportParts
            Open(ExportFileParts)
            If Error()
                Create(ExportFileParts)
                Open(ExportFileParts)
                If Error()
                    PartsError# = 1
                End !If Error()
            End !If Error()
        Else !If tmp:ExportParts
            PartsError# = 1
        End !If tmp:ExportParts

        WarPartsError# = 0
        If tmp:ExportWarparts
            Open(ExportFileWarparts)
            If Error()
                Create(ExportFileWarParts)
                Open(ExportFileWarparts)
                If Error()
                    WarPartsError# = 1
                End !If Error()
            End !If Error()
        Else !If tmp:ExportWarparts
            WarPartsError# = 1
        End !If tmp:ExportWarparts
    End !If tmp:OutputType = 1 And tmp:PaperReportType = 1

    If JobsError# = 0
        Do JobsFileTitle
    End !If JobsError# = 0

    If AuditError# = 0
        Do AuditFileTitle
    End !If AuditError# = 0

    If PartsError# = 0
        Do PartsFileTitle
    End !If PartsError# = 0

    If WarPartsError# = 0
        Do WarPartsFileTitle
    End !If PartsError# = 0
    Count# = 0
    recordspercycle         = 25
    recordsprocessed        = 0
    percentprogress         = 0
    progress:thermometer    = 0
    thiswindow.reset(1)
    open(progresswindow)

    ?progress:userstring{prop:text} = 'Running...'
    ?progress:pcttext{prop:text} = '0% Completed'

    recordstoprocess    = Records(JOBS)

    Save_job_ID = Access:JOBS.SaveFile()

    Case tmp:ReportOrder
        Of 'JOB NUMBER'
            Access:JOBS.Clearkey(job:Ref_Number_Key)
            set(job:ref_number_key,0)
        Of 'I.M.E.I. NUMBER'
            Access:JOBS.Clearkey(job:ESN_Key)
            set(job:esn_key,0)
        Of 'ACCOUNT NUMBER'
            Access:JOBS.Clearkey(job:AccountNumberKey)
            set(job:accountnumberkey,0)
        Of 'MODEL NUMBER'
            Access:JOBS.Clearkey(job:Model_Number_Key)
            set(job:model_number_key,0)
        Of 'STATUS'
            Case tmp:StatusType
                Of 0 !Job Status
                    Access:JOBS.Clearkey(job:By_Status)
                    set(job:by_status,0)
                Of 1 !Exchange Status
                    Access:JOBS.Clearkey(job:ExcStatusKey)
                    Set(job:ExcStatusKey)
                Of 2 !Loan Status
                    Access:JOBS.Clearkey(job:LoanStatusKey)
                    Set(job:LoanStatusKey)
            End !Case tmp:StatusType
        Of 'MSN'
            Access:JOBS.Clearkey(job:MSN_Key)
            set(job:msn_key,0)
        Of 'SURNAME'
            Access:JOBS.Clearkey(job:Surname_Key)
            set(job:surname_key,0)
        Of 'ENGINEER'
            Access:JOBS.Clearkey(job:Engineer_Key)
            set(job:engineer_key,0)
        Of 'MOBILE NUMBER'
            Access:JOBS.Clearkey(job:MobileNumberKey)
            set(job:mobilenumberkey,0)
        Of 'DATE BOOKED'
            Access:JOBS.Clearkey(job:Date_Booked_Key)
            Case tmp:DateRangeType
                Of 0 !Booking
                    job:date_booked = tmp:StartDate
                    set(job:date_booked_key,job:date_booked_key)
                ! Start Change 2223 BE(04/03/03)
                !Of 1 !Completed
                ELSE
                ! End Change 2223 BE(04/03/03)
                    Set(job:Date_Booked_Key,0)
            End !tmp:DateRangeType
        Of 'DATE COMPLETED'
            Access:JOBS.ClearKey(job:DateCompletedKey)
            Case tmp:DateRangeType
                ! Start Change 2223 BE(04/03/03)
                !Of 0 !Booking
                !    Set(job:DateCompletedKey,0)
                ! End Change 2223 BE(04/03/03)
                Of 1 !Completed
                    job:date_completed = tmp:StartDate
                    set(job:datecompletedkey,job:datecompletedkey)
                ! Start Change 2223 BE(04/03/03)
                ELSE
                    Set(job:DateCompletedKey,0)
                ! End Change 2223 BE(04/03/03)
            End !Case tmp:DateRangeType
        ! Start Change 2223 BE(04/03/03)
        OF 'IN WORKSHOP'
            ! Build In Workshop Key Queue
            FREE(InWorkshopQ)
            IF (tmp:DateRangeType = 2) THEN
                recordstoprocess  = recordstoprocess + RECORDS(JOBSE)
            ELSE
                recordstoprocess  = (2 * recordstoprocess) + RECORDS(JOBSE)
            END
            Access:JOBSE.ClearKey(jobe:RecordNumberKey)
            SET(jobe:RecordNumberKey, 0)
            LOOP
                IF (Access:JOBSE.NEXT()) THEN
                    BREAK
                END
                DO GetNextRecord2
               ! DO CancelCheck
                !IF (tmp:cancel = 1) THEN
                !    BREAK
               ! END
                iwq:DateValue = jobe:InWorkshopDate
                iwq:JobRefValue = jobe:RefNumber
                IF (tmp:DateRangeType = 2) THEN
                    IF (INRANGE(jobe:InWorkshopDate,tmp:StartDate,tmp:EndDate)) THEN
                        ADD(InWorkshopQ)
                    END
                ELSE
                    ADD(InWorkshopQ)
                END
            END
            IF (tmp:DateRangeType <> 2) THEN
                ! Now check for any Job Records without Job Extension records
                SORT(InWorkshopQ, iwq:JobRefValue)
                ! Access Job file in most efficient sequence depending upon
                ! DateRange setting.
                IF (tmp:DateRangeType = 0) THEN   !
                    job:date_booked = tmp:StartDate
                    set(job:date_booked_key,job:date_booked_key)
                ELSE
                    job:date_completed = tmp:StartDate
                    set(job:datecompletedkey,job:datecompletedkey)
                END
                LOOP
                    IF (Access:JOBS.NEXT()) THEN
                        BREAK
                    END
                    DO GetNextRecord2
                    ! DO CancelCheck
                    !IF (tmp:cancel = 1) THEN
                    !    BREAK
                    ! END
                    If (tmp:DateRangeType = 0) THEN
                        IF (job:Date_Booked > tmp:EndDate) THEN
                            BREAK
                        END
                    ELSE
                        IF (job:Date_Completed > tmp:EndDate) THEN
                            BREAK
                        END
                    END
                    iwq:DateValue = Date(1, 1, 1900)
                    iwq:JobRefValue = job:Ref_Number
                    GET(InWorkshopQ, iwq:JobRefValue)
                    IF (ERRORCODE()) THEN
                        ADD(InWorkshopQ, iwq:JobRefValue)
                    END
                END
            END
            SORT(InWorkshopQ, iwq:DateValue)
            IWRecordCount# =  Records(InWorkshopQ)
            IWIndex# = 1
        ! End Change 2223 BE(04/03/03)
    End !Case tmp:ReportOrder
    Loop
        ! Start Change 2223 BE(04/03/03)
        IF (tmp:ReportOrder = 'IN WORKSHOP') THEN
            IF (IWIndex# > IWRecordCount#) THEN
                BREAK
            END
            GET(InWorkshopQ, IWIndex#)
            IWIndex# = IWIndex#+1
            job:Ref_Number = iwq:JobRefValue
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key)) THEN
                ! Ignore Orphaned Job Extension Records
                CYCLE
            END !If
        ELSE
            IF (Access:JOBS.NEXT()) THEN
                Break
            END !If
        END
        !If Access:JOBS.NEXT()
        !   Break
        !End !If
        ! End Change 2223 BE(04/03/03)

        Do GetNextRecord2
        If tmp:cancel = 1
            Break
        End!If tmp:cancel = 1

        Case tmp:ReportOrder
            Of 'DATE BOOKED'
                If tmp:DateRangeType = 0
                    If job:Date_Booked > tmp:EndDate
                        Break
                    End !If job:Date_Booked > tmp:EndDate
                End !If tmp:DateRangeType = 0
            Of 'DATE COMPLETED'
                If tmp:DateRangeType = 1
                    If job:Date_Completed > tmp:EndDate
                        Break
                    End !If job:Date_Completed > tmp:EndDate
                End !If tmp:DateRangeType = 1
        End !Case tmp:ReportOrder

        If StatusReportValidation(tmp:DateRangeType,tmp:StartDate,tmp:EndDate,tmp:JobType,tmp:InvoiceType,tmp:DespatchedType,tmp:CompletedType,tmp:StatusType,tmp:JobBatchNumber,tmp:EDIBatchNumber)
            Cycle
        End !If StatusReportValidation(tmp:DateRangeType,tmp:StartDate,tmp:EndDate,tmp:JobType,tmp:InvoiceType,tmp:DespatchedType,tmp:CompletedType)

        If JobsError# = 0
            Do ExportJobs
        End !If JobsError# = 0

        If AuditError# = 0
            
            Save_aud_ID = Access:AUDIT.SaveFile()
            Access:AUDIT.ClearKey(aud:TypeRefKey)
            aud:Ref_Number = job:Ref_Number
            Set(aud:TypeRefKey,aud:TypeRefKey)
            Loop
                If Access:AUDIT.NEXT()
                   Break
                End !If
                If aud:Ref_Number <> job:Ref_Number|
                    Then Break.  ! End If
                Do ExportAudit
            End !Loop
            Access:AUDIT.RestoreFile(Save_aud_ID)
        End !If AuditError# = 0

        If PartsError# = 0
            tmp:FirstPart = 0
            Save_par_ID = Access:PARTS.SaveFile()
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = job:Ref_Number
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> job:Ref_Number      |
                    Then Break.  ! End If
                Do ExportParts
            End !Loop
            Access:PARTS.RestoreFile(Save_par_ID)
        End !If PartsError# = 0

        If WarpartsError# = 0
            tmp:FirstPart = 0
            Save_wpr_ID = Access:WARPARTS.SaveFile()
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number  = job:Ref_Number
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.NEXT()
                   Break
                End !If
                If wpr:Ref_Number  <> job:Ref_Number |
                    Then Break.  ! End If
                Do ExportWarparts
            End !Loop
            Access:WARPARTS.RestoreFile(Save_wpr_ID)
        End !If WarpartsError# = 0
        
        Count# += 1
        ?progress:userstring{prop:Text} = 'Records Found: ' & Count#

    End !Loop
    Access:JOBS.RestoreFile(Save_job_ID)

    Do EndPrintRun
    close(progresswindow)
    Case MessageEx('Routine Completed.'&|
      '<13,10>'&|
      '<13,10>' & Count# & ' record(s) matched the critiera.','ServiceBase 2000',|
                   'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
        Of 1 ! &OK Button
    End!Case MessageEx

    Close(ExportFileJobs)
    Close(ExportFileAudit)
    Close(ExportFileParts)
    Close(ExportFileWarParts)







JobsFileTitle       Routine
        Clear(expjobs:Record)
        expjobs:Line1   = 'Job No'
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Batch Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Who Booked')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Booked')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Time Booked')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Title')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Initial')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Surname')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Account Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Order Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Job')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Charge Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Repair Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Job')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Charge Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Repair Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Model Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Manufacturer')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Incoming I.M.E.I. Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Outgoing I.M.E.I. Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Incoming M.S.N.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Outgoing M.S.N.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Unit Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Mobile Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('In Workshop')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Location')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Authority Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Or Purchase')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Transit Type')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Intermittent Fault')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Turnaround Time')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Engineer')
        ! Start Change 2143 BE(16/01/04)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Engineer''s Team')
        ! End Change 2143 BE(16/01/04)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Postcode')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Company Name')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Address 1')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Address 2')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Address 3')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Telephone Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fax Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Collection Postcode')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Collection Company Name')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Collection Address 1')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Collection Address 2')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Collection Address 3')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Collection Telephone Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Delivery Postcode')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Delivery Company Name')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Delivery Address 1')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Delivery Address 2')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Delivery Address 3')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Delivery Telephone Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('In Repair')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('In Repair Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('In Repair Time')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('On Test')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('On Test Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('On Test Time')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Completed')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Completed')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Time Completed')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('QA Passed')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('QA Passed Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('QA Passed Time')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Estimate Job')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Estimate If Over')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Estimate Ready')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Estimate Accepted')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Estimate Rejected')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Estimate Courier Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Estimate Labour Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Estimate Parts Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Estimate Sub Total')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Courier Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Labour Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Parts Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Sub Total')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Actual Chargeable Parts Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Courier Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Labour Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Parts Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Sub Total')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Actual Warranty Parts Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Chargeable Paid')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Warranty Paid')
        ! Start Change 2145 BE(15/01/04)
        !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Current Job Status')
        ! End Change 2145 BE(15/01/04)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Status Changed Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Status Changed Time')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan Unit Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan I.M.E.I. Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan M.S.N.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan Issue Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan User')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan Courier')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan Consignment Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan Despatch Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Loan Despatch User')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange Unit Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange I.M.E.I. Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange M.S.N.')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange Issue Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange User')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange Courier')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange Consignment Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange Despatch Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange Despatch User')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Date Despatched')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Despatch Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Despatch User')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Courier')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Consignment Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Incoming Courier')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Incoming Consignment Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Incoming Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Despatched Flag')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Third Party Site')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 1')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 2')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 3')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 4')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 5')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 6')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 7')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 8')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 9')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 10')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 11')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Code 12')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 1')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 2')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 3')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 4')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 5')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 6')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 7')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 8')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 9')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 10')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 11')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Trade Fault Code 12')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('EDI Batch Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('EDI Flag')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Fault Description')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Engineers Notes')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Invoice Text')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Collection Text')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Delivery Text')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('SIM Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Job Skill Level')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Invoice Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Invoice Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Invoice Number')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Invoice Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Invoice Courier Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Invoice Labour Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Invoice Parts Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Invoice Sub Total')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Invoice Courier Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Invoice Labour Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Invoice Parts Cost')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Invoice Sub Total')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('In Workshop Date')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('In Workshop Time')

        !Change 2262 BE 28/02/03
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Job (at Bkg)')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Chargeable Charge Type (at Bkg)')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Job (at Bkg)')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Warranty Charge Type (at Bkg)')
        !Change 2262 BE 28/02/03

        !Change 2496 BE (16/04/03)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Bouncer')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange Issue Time')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Insurance Ref')
        !Change 2496 BE (16/04/03)

        ! Start Change 2145 BE(15/01/04)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Previous Status')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Changed(Date)')
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('User(Who Changed)')
        ! End Change 2145 BE(15/01/04)

        ! Start Change 3252 BE(29/01/04)
        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Exchange Reason')
        ! End Change 3252 BE(29/01/04)

        ! Start - Export status change date for CRC - TrkBs: 5646 (DBH: 05-04-2005)
        If Instring('C.R.C.',def:User_Name,1,1)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Customer Collection Date')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Due For Despatch From Service Centre')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Status Change to 135 DESPATCHED FROM STORE')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Time Status Change to 135 DESPATCHED FROM STORE')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Status Change to 305 AWAITING ALLOCATION')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Time Status Change to 305 AWAITING ALLOCATION')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Status Change to 315 IN REPAIR')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Time Status Change to 315 IN REPAIR')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Status Change to 705 COMPLETED')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Time Status Change to 705 COMPLETED')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Status Change to 901 DESPATCHED')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Time Status Change to 901 DESPATCHED')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Status Change to 930 ARRIVED BACK AT STORE')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Time Status Change to 930 ARRIVED BACK AT STORE')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Date Status Change to 940 PHONE RETURNED TO CUSTOMER')
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Time Status Change to 940 PHONE RETURNED TO CUSTOMER')
            ! Display "Tote Collected" flag - TrkBs: 5718 (DBH: 19-04-2005)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('Tote Collected')
        End ! If Instring('C.R.C.',def:User_Name,1,1)
        ! End   - Export status change date for CRC - TrkBs: 5646 (DBH: 05-04-2005)

        Add(ExportFileJobs)
ExportJobs      Routine
Data
local:TheDate       Date()
local:TheTime       Time()
local:NoSQL         Byte(0)
Code
            ! Inserting (DBH 11/21/2005) # - Open a connection to the SQL file. Should error if it doesn't exist
            IniFilepath =  CLIP(PATH()) & '\Webimp.ini'
            ConnectionStr = GETINI('Defaults', 'Connection', '', CLIP(IniFilepath))
            local:NoSQL = False
            Open(SQLFile)
            If Error()
                local:NoSQL = True
            End ! If Error()
            ! End (DBH 11/21/2005) #

            Clear(expjobs:Record)

            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            ! Start Change BE020 BE(01/12/03)
            ! Use Fetch to ensure record buffer cleared if record does not exist.
            !Access:JOBSE.TryFetch(jobe:RefNumberKey)
            Access:JOBSE.Fetch(jobe:RefNumberKey)
            ! End Change BE020 BE(01/12/03)

            expjobs:Line1   = StripComma(job:Ref_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Batch_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Who_Booked)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Date_Booked,@d06))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Time_Booked,@t01))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Title)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Initial)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Surname)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Account_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Order_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Chargeable_Job)

            If job:Chargeable_Job = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Charge_Type)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Repair_Type)
            Else !If job:Chargeable_Job = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            End !If job:Chargeable_Job = 'YES'
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Warranty_Job)

            If job:Warranty_Job = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Warranty_Charge_Type)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Repair_Type_Warranty)
            Else !If job:Warranty_Job = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            End !If job:Warranty_Job = 'YES'
            
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Model_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Manufacturer)

            !Start - Has there been an RF Swap? If so, show the original IMEI - TrkBs: 5382 (DBH: 18-02-2005)
            If jobe:Pre_RF_Board_IMEI <> ''
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:Pre_RF_Board_IMEI)
            Else ! If jobe:Pre_RF_Board_IMEI <> ''
                !Check the JOBTHIRD to see if there is an entry for this job.
                !If there is then that means it's been sent to third party,
                !therefore get the original IMEI /MSN incase it has changed.
                Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
                jot:RefNumber = job:Ref_Number
                Set(jot:RefNumberKey,jot:RefNumberKey)
                If Access:JOBTHIRD.Next() = Level:Benign
                    If jot:RefNumber = job:Ref_Number
                        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jot:OriginalIMEI)
                    Else !If jot:RefNumber = job:Ref_Number
                        expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:ESN)
                    End !If jot:RefNumber = job:Ref_Number
                Else !If Access:JOBTHIRD.Next() = Level:Benign
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:ESN)
                End !If Access:JOBTHIRD.Next() = Level:Benign
            End ! If jobe:Pre_RF_Board_IMEI <> ''
            !End   - Has there been an RF Swap? If so, show the original IMEI - TrkBs: 5382 (DBH: 18-02-2005)

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:ESN)

            Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
            jot:RefNumber = job:Ref_Number
            Set(jot:RefNumberKey,jot:RefNumberKey)
            If Access:JOBTHIRD.Next() = Level:Benign
                If jot:RefNumber = job:Ref_Number
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jot:OriginalMSN)
                Else !If jot:RefNumber = job:Ref_Number
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:MSN)
                End !If jot:RefNumber = job:Ref_Number
            Else !If Access:JOBTHIRD.Next() = Level:Benign
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:MSN)
            End !If Access:JOBTHIRD.Next() = Level:Benign
            
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:MSN)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Unit_Type)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Mobile_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Workshop)

            !Location only applies if the job is in the workshop
            If job:Workshop = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Location)
            Else !If job:Workshop = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            End !If job:Workshop = 'YES'
            
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Authority_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:DOP,@d06))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Transit_Type)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Intermittent_Fault)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Turnaround_Time)

            If job:Workshop = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Engineer)
                ! Start Change 2143 BE(16/01/04)
                access:users.clearkey(use:User_Code_Key)
                use:User_Code   = job:Engineer
                IF (access:users.fetch(use:User_Code_Key) = Level:Benign) THEN
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(use:Team)
                ELSE
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                END
                ! End Change 2143 BE(16/01/04)
            Else !If job:Workshop = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                ! Start Change 2143 BE(16/01/04)
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                ! End Change 2143 BE(16/01/04)
            End !If job:Workshop = 'YES'
            
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Postcode)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Company_Name)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Address_Line1)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Address_Line2)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Address_Line3)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Telephone_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fax_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Postcode_Collection)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Company_Name_Collection)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Address_Line1_Collection)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Address_Line2_Collection)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Address_Line3_Collection)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Telephone_Collection)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Postcode_Delivery)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Company_Name_Delivery)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Address_Line1_Delivery)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Address_Line2_Delivery)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Address_Line3_Delivery)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Telephone_Delivery)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:In_Repair)
            If job:In_Repair = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Date_In_Repair,@d06))
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Time_In_Repair,@t01))
            Else !If job:In_Repair = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ',' 
            End !If job:In_Repair = 'YES'
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:On_Test)

            If job:On_Test = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Date_On_Test,@d06))
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Time_In_Repair,@t01))
            Else !If job:On_Test = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ',' 
            End !If job:On_Test = 'YES'

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Completed)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Date_Completed,@d06))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Time_Completed,@t01))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:QA_Passed)

            If job:QA_Passed = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Date_QA_Passed,@d06))
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Time_QA_Passed,@t01))
            Else !If job:QA_Passed = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            End !If job:QA_Passed = 'YES'

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Estimate)

            If job:Estimate = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Estimate_If_Over)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Estimate_Ready)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Estimate_Accepted)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Estimate_Rejected)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Courier_Cost_Estimate)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Labour_Cost_Estimate)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Parts_Cost_Estimate)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Courier_Cost_Estimate + |
                                                                        job:Labour_Cost_Estimate + |
                                                                        job:Parts_Cost_Estimate)
            Else !If job:Estimate = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            End !If job:Estimate = 'YES'

            If job:Chargeable_Job = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Courier_Cost)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Labour_Cost)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Parts_Cost)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Courier_Cost + |
                                                                            job:Labour_Cost + |
                                                                            job:Parts_Cost)
                tmp:ActualPartsCost = 0
                Save_par_ID = Access:PARTS.SaveFile()
                Access:PARTS.ClearKey(par:Part_Number_Key)
                par:Ref_Number  = job:Ref_Number
                Set(par:Part_Number_Key,par:Part_Number_Key)
                Loop
                    If Access:PARTS.NEXT()
                       Break
                    End !If
                    If par:Ref_Number  <> job:Ref_Number       |
                        Then Break.  ! End If

                    ! Start Change 4779 BE(25/10/2004)
                    IF (((par:order_number <> '') AND (par:date_received = '')) OR |
                       (par:pending_ref_number <> '')) THEN
                        CYCLE
                    END
                    ! End Change 4779 BE(25/10/2004)

                    tmp:ActualPartsCost += par:Purchase_Cost * par:quantity
                End !Loop
                Access:PARTS.RestoreFile(Save_par_ID)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(tmp:ActualPartsCost)
            Else !If job:Chargeale_Job = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            End !If job:Chargeale_Job = 'YES'

            If job:Warranty_Job = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Courier_Cost_Warranty)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Labour_Cost_Warranty)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Parts_Cost_Warranty)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Courier_Cost_Warranty + |
                                                                            job:Labour_Cost_Warranty + |
                                                                            job:Parts_Cost_Warranty)
                tmp:ActualPartsCost = 0
                Save_wpr_ID = Access:WARPARTS.SaveFile()
                Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                wpr:Ref_Number  = job:Ref_Number
                Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                Loop
                    If Access:WARPARTS.NEXT()
                       Break
                    End !If
                    If wpr:Ref_Number  <> job:Ref_Number       |
                        Then Break.  ! End If

                    ! Start Change 4779 BE(25/10/2004)
                    IF (((wpr:order_number <> '') AND (wpr:date_received = '')) OR |
                       (wpr:pending_ref_number <> '')) THEN
                        CYCLE
                    END
                    ! End Change 4779 BE(25/10/2004)

                    tmp:ActualPartsCost += wpr:Purchase_Cost * wpr:quantity
                End !Loop
                Access:WARPARTS.RestoreFile(Save_wpr_ID)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(tmp:ActualPartsCost)
            Else !If job:Warranty_Job = 'YES'
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            End !If job:Warranty_Job = 'YES'

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Paid)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Paid_Warranty)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Current_Status)

            !Get the status time and date  BB:2603
            StatusDate$ = ''
            StatusTime$ = ''
            Save_aus_ID = Access:AUDSTATS.SaveFile()
            Access:AUDSTATS.ClearKey(aus:DateChangedKey)
            aus:RefNumber   = job:Ref_Number
            aus:Type        = 'JOB'
            aus:DateChanged = Today()
            Set(aus:DateChangedKey,aus:DateChangedKey)
            Loop
                If Access:AUDSTATS.PREVIOUS()
                   Break
                End !If
                If aus:RefNumber   <> job:Ref_Number      |
                Or aus:Type <> 'JOB'        |
                Or aus:DateChanged > Today()      |
                    Then Break.  ! End If
                If aus:newstatus = job:Current_Status
                    StatusDate$  = aus:DateChanged
                    StatusTime$  = aus:TimeChanged
                    Break
                End!If aud:newstatus = job:Current_Status
            End !Loop
            Access:AUDSTATS.RestoreFile(Save_aus_ID)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(StatusDate$,@d06b))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(StatusTime$,@t01b))

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Exchange_Status)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Loan_Status)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Loan_Unit_Number)
            If job:Loan_Unit_Number <> 0
                Access:LOAN.Clearkey(loa:Ref_Number_Key)
                loa:Ref_Number  = job:Loan_Unit_Number
                If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                    !Found
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(loa:ESN)
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(loa:MSN)
                Else! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                End! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign

                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Loan_Issued_date,@d06))
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Loan_User)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Loan_Courier)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Loan_Consignment_Number)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Loan_Despatch_Number)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Loan_Despatched_User)
            Else !If job:Loan_Unit_Number <> 0
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            End !If job:Loan_Unit_Number <> 0

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Exchange_Unit_Number)

            ! Start Change 2496 BE(16/04/03)
            ExchangeIssuedDate" = ''
            ExchangeIssuedTime" = ''
            ! Start Change 2496 BE(16/04/03)

            If job:Exchange_Unit_Number <> 0
                Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                xch:Ref_Number  = job:Exchange_Unit_Number
                If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    !Found
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(xch:ESN)
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(xch:MSN)
                Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign

                ! Start Change 2496 BE(16/04/03)
                !Access:JOBEXHIS.Clearkey(jxh:Ref_Number_Key)
                !jxh:Ref_Number  = job:Ref_Number
                !SET(jxh:Ref_Number_Key, jxh:Ref_Number_Key)
                !LOOP
                !    IF (Access:JOBEXHIS.Next() <> Level:Benign) THEN
                !        BREAK
                !    END
                !    IF (jxh:Ref_Number  <> job:Ref_Number) THEN
                !        BREAK
                !    END
                !    IF (jxh:Loan_Unit_Number  = job:Exchange_Unit_Number) THEN
                !        ExchangeIssuedDate" = Format(jxh:Date,@d06)
                !        ExchangeIssuedTime" = Format(jxh:Time,@t01)
                !        BREAK
                !    END
                !END
                !
                ! Above Code abandoned because Job Exchange History Records are not recorded
                ! when a unit is first issued !!! only when a change to an issued unit occurs.
                !
                ! Exchange History (Ref_Number_Key) is sequenced on Date/Time descending
                ! so first record read here should be the latest.  This is a bit dodgy because
                ! there is no job record foreign key to make absolutely certain but it should be OK.
                ! Exchange Unit is Issued in SBF01 and SBJ01 with Status Messages of either
                !
                ! "AWAITING QA. EXCHANGED ON JOB NO:" or
                ! "UNIT EXCHANGED ON JOB NO:"
                !
                ! The most recent occurrence of these messages for a job marked as having an exchange
                ! unit allocated is assumed to be the Issue History Record Date & Time.
                !
                Access:EXCHHIST.Clearkey(exh:Ref_Number_Key)
                exh:Ref_Number  = job:Exchange_Unit_Number
                SET(exh:Ref_Number_Key, exh:Ref_Number_Key)
                LOOP
                    IF (Access:EXCHHIST.TryNext() <> Level:Benign) THEN
                        BREAK
                    END

                    IF (exh:Ref_Number  <> job:Exchange_Unit_Number) THEN
                        BREAK
                    END

                    s" = CLIP(exh:Status)
                    IF ((s"[1:32] = 'AWAITING QA. EXCHANGED ON JOB NO') OR |
                        (s"[1:25] = 'UNIT EXCHANGED ON JOB NO:')) THEN
                        ExchangeIssuedDate" = Format(exh:Date,@d06)
                        ExchangeIssuedTime" = Format(exh:Time,@t01)
                        BREAK
                    END
                END
                ! End Change 2496 BE(16/04/03)

                ! Start Change 2784 BE(18/06/03)
                !expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Exchange_Issued_Date,@d06))
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(ExchangeIssuedDate")
                ! End Change 2784 BE(18/06/03)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Exchange_User)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Exchange_Courier)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Exchange_Consignment_Number)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Exchange_Despatch_Number)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Exchange_Despatched_User)
            Else !If job:Exchange_Unit_Number <> 0
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
                expjobs:Line1   = Clip(expjobs:Line1) & ','
            End !If job:Exchange_Unit_Number <> 0

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Date_Despatched,@d06))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Despatch_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Despatch_User)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Courier)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Consignment_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Incoming_Courier)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Incoming_Consignment_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Incoming_Date,@d06))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Despatched)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Third_Party_Site)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code1)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code2)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code3)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code4)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code5)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code6)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code7)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code8)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code9)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code10)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code11)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Fault_Code12)

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode1)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode2)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode3)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode4)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode5)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode6)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode7)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode8)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode9)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode10)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode11)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:TraFaultCode12)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:EDI_Batch_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:EDI)

            Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
            jbn:RefNumber = job:Ref_Number
            ! Start Change BE020 BE(01/12/03)
            ! Use Fetch to ensure record buffer cleared if record does not exist.
            !Access:JOBNOTES.TryFetch(jbn:RefNumberKey)
            Access:JOBNOTES.Fetch(jbn:RefNumberKey)
            ! End Change BE020 BE(01/12/03)

            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StripReturn(jbn:Fault_Description))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StripReturn(jbn:Engineers_Notes))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StripReturn(jbn:Invoice_Text))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StripReturn(jbn:Collection_Text))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(StripReturn(jbn:Delivery_Text))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:SIMNumber)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(jobe:SkillLevel)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Invoice_Number)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Invoice_Date,@d06))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Invoice_Number_Warranty)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(job:Invoice_Date_Warranty,@d06))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Invoice_Courier_Cost)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Invoice_Labour_Cost)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Invoice_Parts_Cost)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:Invoice_Courier_Cost + |
                                                                            job:Invoice_Labour_Cost + |
                                                                            job:Invoice_Parts_Cost)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:WInvoice_Courier_Cost)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:WInvoice_Labour_Cost)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:WInvoice_Parts_Cost)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(job:WInvoice_Courier_Cost + |
                                                                        job:WInvoice_Labour_Cost + |
                                                                        job:WInvoice_Parts_Cost)
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(jobe:InWorkshopDate,@d06))
            expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(jobe:InWorkshopTime,@T01))


            ! Change 2262 (BE 28/02/03)
            IsBkgJobChangeType# = 0
            BkgJobChangeType" = ''
            IsBkgWarrantyChangeType# = 0
            BkgWarrantyChangeType" = ''

            Save_aud_ID = Access:AUDIT.SaveFile()
            Access:AUDIT.ClearKey(aud:TypeRefKey)
            aud:Ref_Number = job:Ref_Number
            SET(aud:TypeRefKey, aud:TypeRefKey)
            LOOP
                IF (Access:AUDIT.NEXT()) THEN
                   BREAK
                END
                IF (aud:Ref_Number <> job:Ref_Number) THEN
                   BREAK
                END
                IF (aud:Type = 'JOB') THEN
                    IsFound# = 0
                    !
                    ! Check for the various manifestations of Chargeable Job Type
                    !
                    P# = INSTRING('CHARGEABLE CHARGE TYPE: ', aud:Notes, 1, 1)       ! Generated in CELRAPID/CELRAPML
                    IF (P#) THEN
                        P# = P#+23
                        Q# = INSTRING('<13>', aud:Notes, 1, P#)
                        IF (Q# = 0) THEN
                            Q# = LEN(CLIP(aud:Notes))
                        ELSE
                            Q# = Q#-1
                        END
                        IsBkgJobChangeType# = 1
                        BkgJobChangeType" = aud:Notes[P#:Q#]
                        IsFound# = 1
                    ELSIF INSTRING('JOB CREATED BY WEBMASTER', aud:Notes, 1, 1) THEN  ! Generated by Web Input
                        P# = INSTRING('CHARGE TYPE: ', aud:Notes, 1, 1)
                        IF (P#) THEN
                            P# = P#+13
                            Q# = INSTRING('<13>', aud:Notes, 1, P#)
                            IF (Q# = 0) THEN
                                Q# = LEN(CLIP(aud:Notes))
                            ELSE
                                Q# = Q#-1
                            END
                            TempString" = aud:Notes[P#:Q#]
                            P# = INSTRING('CHARGEABLE JOB: ', aud:Notes, 1, 1)
                            IF (P# AND (aud:Notes[P#+16:P#+18] = 'YES')) THEN
                                IsBkgJobChangeType# = 1
                                BkgJobChangeType" = TempString"
                            ELSE
                                IsBkgWarrantyChangeType# = 1
                                BkgWarrantyChangeType" = TempString"
                            END
                            IsFound# = 1
                        END
                    ELSE
                        P# = INSTRING('CHARGEABLE JOB: ', aud:Notes, 1, 1)           ! Generated in SBJ01
                        IF (P# AND (aud:Notes[P#+16:P#+18] = 'YES')) THEN
                            P# = P#+19
                            P# = INSTRING('CHARGE TYPE: ', aud:Notes, 1, P#)
                            IF (P#) THEN
                                P# = P#+13
                                Q# = INSTRING('<13>', aud:Notes, 1, P#)
                                IF (Q# = 0) THEN
                                    Q# = LEN(CLIP(aud:Notes))
                                ELSE
                                    Q# = Q#-1
                                END
                                IsBkgJobChangeType# = 1
                                BkgJobChangeType" = aud:Notes[P#:Q#]
                                IsFound# = 1
                            END
                        END
                    END
                    !
                    ! Check for the various manifestations of Warranty Job Type
                    !
                    P# = INSTRING('WARRANTY CHARGE TYPE: ', aud:Notes, 1, 1)         ! Generated in CELRAPID/CELRAPML
                    IF (P#) THEN
                        P# = P#+22
                        Q# = INSTRING('<13>', aud:Notes, 1, P#)
                        IF (Q# = 0) THEN
                            Q# = LEN(CLIP(aud:Notes))
                        ELSE
                            Q# = Q#-1
                        END
                        IsBkgWarrantyChangeType# = 1
                        BkgWarrantyChangeType" = aud:Notes[P#:Q#]
                        IsFound# = 1
                    ELSE
                        P# = INSTRING('WARRANTY JOB: ', aud:Notes, 1, 1)             ! Generated in BJ01
                        IF (P# AND (aud:Notes[P#+14:P#+16] = 'YES')) THEN
                            P# = P#+17
                            P# = INSTRING('CHARGE TYPE: ', aud:Notes, 1, P#)
                            IF (P#) THEN
                                P# = P#+13
                                Q# = INSTRING('<13>', aud:Notes, 1, P#)
                                IF (Q# = 0) THEN
                                    Q# = LEN(CLIP(aud:Notes))
                                ELSE
                                    Q# = Q#-1
                                END
                                IsBkgWarrantyChangeType# = 1
                                BkgWarrantyChangeType" = aud:Notes[P#:Q#]
                                IsFound# = 1
                            END
                        END
                    END

                    ! Now check if either or both  entries were found
                    IF (IsFound#) THEN
                        BREAK
                    END
                END
            END
            Access:AUDIT.RestoreFile(Save_aud_ID)

            IF (IsBkgJobChangeType#) THEN
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('YES')
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(BkgJobChangeType")
            ELSE
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('NO')
                expjobs:Line1   = Clip(expjobs:Line1) & ',' 
            END

            IF (IsBkgWarrantyChangeType#) THEN
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('YES')
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(BkgWarrantyChangeType")
            ELSE
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('NO')
                expjobs:Line1   = Clip(expjobs:Line1) & ',' 
            END
            ! Change 2262 (BE 28/02/03)

            ! Start Change 2496 (BE 16/04/03)
            IF (CountBouncer(job:ref_number, job:date_booked, job:esn) > 0) THEN
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('YES')
            ELSE
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma('NO')
            END
            expjobs:Line1   = Clip(expjobs:Line1) & ','  & StripComma(ExchangeIssuedTime")
            expjobs:Line1   = Clip(expjobs:Line1) & ','  & StripComma(job:Insurance_Reference_Number)
            ! end Change 2496 (BE 16/04/03)

            ! Start Change 2145 BE(15/01/04)
            expjobs:Line1   = CLIP(expjobs:Line1) & ',' & StripComma(job:PreviousStatus)

            PreviousStatusDate" = ''
            WhoChanged" = ''
            Save_aus_ID = Access:AUDSTATS.SaveFile()
            Access:AUDSTATS.ClearKey(aus:DateChangedKey)
            aus:RefNumber   = job:Ref_Number
            aus:Type        = 'JOB'
            aus:DateChanged = Today()
            SET(aus:DateChangedKey,aus:DateChangedKey)
            LOOP
                IF ((Access:AUDSTATS.PREVIOUS() <> Level:Benign) OR  |
                    (aus:RefNumber   <> job:Ref_Number) OR |
                    (aus:Type <> 'JOB')) THEN
                    BREAK
                END
                IF (aus:newstatus = job:PreviousStatus) THEN
                    PreviousStatusDate"  = FORMAT(aus:DateChanged, @d06b)
                    WhoChanged"          = aus:UserCode
                    BREAK
                END
            END
            Access:AUDSTATS.RestoreFile(Save_aus_ID)
            expjobs:Line1   = CLIP(expjobs:Line1) & ',' & StripComma(PreviousStatusDate")
            expjobs:Line1   = CLIP(expjobs:Line1) & ',' & StripComma(WhoChanged")
            ! End Change 2145 BE(15/01/04)

            ! Start Change 3252 BE(29/01/04)
            expjobs:Line1   = CLIP(expjobs:Line1) & ',' & StripComma(jobe:ExchangeReason)
            ! End Change 3252 BE(29/01/04)

            ! Start - Export status change date for CRC - TrkBs: 5646 (DBH: 05-04-2005)
            If Instring('C.R.C.',def:User_Name,1,1)
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(jobe:CustomerCollectionDate,@d6b))
                expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(jobe:EstimatedDespatchDate,@d6b))

                If local.GetStatusDate('135',local:TheDate,local:TheTime)
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(local:TheDate,@d6b))
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(local:TheTime,@t1b))
                Else ! If GetStatusDate('135',local:TheDate,local:TheTime)
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                End ! If GetStatusDate('135',local:TheDate,local:TheTime)
                If local.GetStatusDate('305',local:TheDate,local:TheTime)
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(local:TheDate,@d6b))
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(local:TheTime,@t1b))
                Else ! If GetStatusDate('135',local:TheDate,local:TheTime)
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                End ! If GetStatusDate('135',local:TheDate,local:TheTime)
                If local.GetStatusDate('315',local:TheDate,local:TheTime)
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(local:TheDate,@d6b))
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(local:TheTime,@t1b))
                Else ! If GetStatusDate('135',local:TheDate,local:TheTime)
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                End ! If GetStatusDate('135',local:TheDate,local:TheTime)
                If local.GetStatusDate('705',local:TheDate,local:TheTime)
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(local:TheDate,@d6b))
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(local:TheTime,@t1b))
                Else ! If GetStatusDate('135',local:TheDate,local:TheTime)
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                End ! If GetStatusDate('135',local:TheDate,local:TheTime)
                If local.GetStatusDate('901',local:TheDate,local:TheTime)
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(local:TheDate,@d6b))
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(local:TheTime,@t1b))
                Else ! If GetStatusDate('135',local:TheDate,local:TheTime)
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                End ! If GetStatusDate('135',local:TheDate,local:TheTime)
                If local.GetStatusDate('930',local:TheDate,local:TheTime)
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(local:TheDate,@d6b))
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(local:TheTime,@t1b))
                Else ! If GetStatusDate('135',local:TheDate,local:TheTime)
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                End ! If GetStatusDate('135',local:TheDate,local:TheTime)
                If local.GetStatusDate('940',local:TheDate,local:TheTime)
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(local:TheDate,@d6b))
                    expjobs:Line1   = Clip(expjobs:Line1) & ',' & StripComma(Format(local:TheTime,@t1b))
                Else ! If GetStatusDate('135',local:TheDate,local:TheTime)
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                End ! If GetStatusDate('135',local:TheDate,local:TheTime)
! Deleted (DBH 11/21/2005) # - The flag is getting truncated, so look at the WebJob file instead
!                 ! Start - Get the "Tote Collected" flag from the Audit Trail - TrkBs: 5718 (DBH: 19-04-2005)
!                 Found# = False
!                 Save_aud_ID = Access:AUDIT.SaveFile()
!                 Access:AUDIT.ClearKey(aud:Action_Key)
!                 aud:Ref_Number = job:Ref_Number
!                 aud:Action     = 'NEW JOB INITIAL BOOKING'
!                 Set(aud:Action_Key,aud:Action_Key)
!                 Loop
!                     If Access:AUDIT.NEXT()
!                        Break
!                     End !If
!                     If aud:Ref_Number <> job:Ref_Number      |
!                     Or aud:Action     <> 'NEW JOB INITIAL BOOKING'      |
!                         Then Break.  ! End If
!                     If Instring('JOB CREATED BY WEBMASTER',aud:Notes,1,1)
!                         x# = Instring('TOTE COLLECTED:',aud:Notes,1,1)
!                         If x# > 1
!                             expjobs:Line1   = Clip(expjobs:Line1) & ',' & Sub(aud:Notes,x# + 16,3)
!                             Found# = True
!                         End ! If x# > 1
!                     End ! If Instring('JOB CREATED BY WEBMASTER',aud:Notes,1,1)
!                 End !Loop
!                 Access:AUDIT.RestoreFile(Save_aud_ID)
!                 If Found# = False
!                     expjobs:Line1   = Clip(expjobs:Line1) & ','
!                 End ! If Found# = False
!                 ! End   - Get the "Tote Collected" flag from the Audit Trail - TrkBs: 5718 (DBH: 19-04-2005)
! End (DBH 11/21/2005) #
                ! Inserting (DBH 11/21/2005) # - Look in the Webjob for the Tote Collection value
                FoundTote# = False
                If local:NoSQL = False
                    SQLFile{prop:SQL} = 'SELECT SBJobNo,CollectionMadeToday FROM WebJob Where SBJobNo = ' & job:Ref_Number
                    Next(SQLFile)
                    If ~Error()
                        If sql:SQLSBJobNo = job:Ref_Number
                            FoundTote# = True
                            Case sql:SQLCollectionMadeToday
                            Of 0
                                expjobs:Line1   = Clip(expjobs:Line1) & ',NO'
                            Of 1
                                expjobs:Line1   = Clip(expjobs:Line1) & ',YES'
                            Of 2
                                expjobs:Line1   = Clip(expjobs:Line1) & ',T'
                            End ! Case sql:SQLCollectionMadeToday
                            
                        End ! If sql:SQLSBJobNo = job:Ref_Number
                    End ! If ~Error()
                End ! If local:NoSQL = Falsee
                If FoundTote# = False
                    expjobs:Line1   = Clip(expjobs:Line1) & ','
                End ! If FoundTote# = False
                ! End (DBH 11/21/2005) #

            End ! If Instring('C.R.C.',def:User_Name,1,1)
            ! End   - Export status change date for CRC - TrkBs: 5646 (DBH: 05-04-2005)

            expjobs:line1   = StripQuotes(expjobs:Line1)
            Add(ExportFileJobs)

            If local:NoSQL = False
                Close(SQLFile)
            End ! If local:NoSQL = False
            
AuditFileTitle      Routine
        Clear(expaudit:Record)
        expaudit:Line1    = 'Job Number'
        expaudit:Line1    = Clip(expaudit:Line1) & ',Date'
        expaudit:Line1    = Clip(expaudit:Line1) & ',Time'
        expaudit:Line1    = Clip(expaudit:Line1) & ',User'
        expaudit:Line1    = Clip(expaudit:Line1) & ',Action'
        expaudit:Line1    = Clip(expaudit:Line1) & ',Notes'
        expaudit:Line1    = Clip(expaudit:Line1) & ',Type'
        Add(ExportFileAudit)
ExportAudit      Routine
        Clear(expaudit:Record)
        expaudit:Line1    = StripComma(aud:Ref_Number)
        expaudit:Line1    = Clip(expaudit:Line1) & ',' & StripComma(Format(aud:Date,@d06))
        expaudit:Line1    = Clip(expaudit:Line1) & ',' & StripComma(Format(aud:Time,@t01))
        expaudit:Line1    = Clip(expaudit:Line1) & ',' & StripComma(aud:User)
        expaudit:Line1    = Clip(expaudit:Line1) & ',' & StripComma(aud:Action)
        expaudit:Line1    = Clip(expaudit:Line1) & ',' & StripComma(aud:Notes)
        expaudit:Line1    = Clip(expaudit:Line1) & ',' & StripComma(aud:Type)
        expaudit:Line1    = StripQuotes(expaudit:Line1)
        Add(ExportFileAudit)
PartsFileTitle      Routine
        Clear(expparts:Record)
        expparts:Line1    = 'Job Number'
        expparts:Line1    = Clip(expparts:Line1) & ',Part Number'
        expparts:Line1    = Clip(expparts:Line1) & ',Description'
        expparts:Line1    = Clip(expparts:Line1) & ',Supplier'
        expparts:Line1    = Clip(expparts:Line1) & ',Model Number'
        expparts:Line1    = Clip(expparts:Line1) & ',Purchase Cost'
        expparts:Line1    = Clip(expparts:Line1) & ',Sale Cost'
        expparts:Line1    = Clip(expparts:Line1) & ',Retail Cost'
        expparts:Line1    = Clip(expparts:Line1) & ',Quantity'
        expparts:Line1    = Clip(expparts:Line1) & ',Exclude From Order'
        expparts:Line1    = Clip(expparts:Line1) & ',Despatch Note Number'
        expparts:Line1    = Clip(expparts:Line1) & ',Order Number'
        expparts:Line1    = Clip(expparts:Line1) & ',Date Received'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Codes Checked'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 1'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 2'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 3'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 4'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 5'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 6'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 7'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 8'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 9'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 10'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 11'
        expparts:Line1    = Clip(expparts:Line1) & ',Fault Code 12'
        expparts:Line1    = Clip(expparts:Line1) & ',First Part'
        Add(ExportFileParts)
ExportParts      Routine
        Clear(expparts:Record)
        expparts:Line1    = Stripcomma(par:Ref_Number)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Part_Number)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Description)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Supplier)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(job:Model_Number)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Purchase_Cost)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Sale_Cost)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(Par:Retail_Cost)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Quantity)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Exclude_From_Order)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Despatch_Note_Number)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Order_Number)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(Format(par:Date_Received,@d06))
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Codes_Checked)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code1)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code2)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code3)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code4)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code5)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code6)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code7)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code8)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code9)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code10)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code11)
        expparts:Line1    = Clip(expparts:Line1) & ',' & StripComma(par:Fault_Code12)
        If tmp:FirstPart = 0
            expparts:Line1    = Clip(expparts:Line1) & ',1'
            tmp:FirstPart = 1
        Else !If tmp:FirstPart = 0
            expparts:Line1    = Clip(expparts:Line1) & ',0'
        End !If tmp:FirstPart = 0
        expparts:Line1      = StripQuotes(expparts:Line1)
        Add(ExportFileParts)
WarPartsFileTitle       Routine
        Clear(expwarparts:Record)
        expwarparts:Line1    = 'Job Number'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Part Number'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Description'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Supplier'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Model Number'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Purchase Cost'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Sale Cost'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Retail Cost'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Quantity'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Exclude From Order'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Despatch Note Number'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Order Number'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Date Received'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Fault Codes Checked'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Fault Code 1'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Fault Code 2'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Fault Code 3'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Fault Code 4'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Fault Code 5'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Fault Code 6'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Fault Code 7'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Fault Code 8'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Fault Code 9'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Fault Code 10'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Fault Code 11'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',Fault Code 12'
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',First Part'
        Add(ExportFileWarParts)
ExportWarParts      Routine
        Clear(expwarparts:Record)
        expwarparts:Line1    = Stripcomma(wpr:Ref_Number)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Part_Number)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Description)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Supplier)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(job:Model_Number)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Purchase_Cost)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Sale_Cost)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Retail_Cost)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Quantity)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Exclude_From_Order)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Despatch_Note_Number)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Order_Number)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(Format(wpr:Date_Received,@d06))
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Codes_Checked)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code1)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code2)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code3)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code4)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code5)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code6)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code7)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code8)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code9)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code10)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code11)
        expwarparts:Line1    = Clip(expwarparts:Line1) & ',' & StripComma(wpr:Fault_Code12)
        If tmp:FirstPart = 0
            expwarparts:Line1    = Clip(expwarparts:Line1) & ',1'
            tmp:FirstPart = 1
        Else !If tmp:FirstPart = 0
            expwarparts:Line1    = Clip(expwarparts:Line1) & ',0'
        End !If tmp:FirstPart = 0
        expwarparts:Line1       = StripQuotes(expwarparts:Line1)
        Add(ExportFileWarParts)
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    progresswindow{prop:Text} = ?progress:pcttext{prop:text}
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Status_Report_Criteria',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('local',local,'Status_Report_Criteria',1)
    SolaceViewVars('local:FileNameJobs',local:FileNameJobs,'Status_Report_Criteria',1)
    SolaceViewVars('InWorkShopQ:DateValue',InWorkShopQ:DateValue,'Status_Report_Criteria',1)
    SolaceViewVars('InWorkShopQ:JobRefValue',InWorkShopQ:JobRefValue,'Status_Report_Criteria',1)
    SolaceViewVars('local:FileNameAudit',local:FileNameAudit,'Status_Report_Criteria',1)
    SolaceViewVars('local:FileNameParts',local:FileNameParts,'Status_Report_Criteria',1)
    SolaceViewVars('local:FileNameWarParts',local:FileNameWarParts,'Status_Report_Criteria',1)
    SolaceViewVars('save_job_id',save_job_id,'Status_Report_Criteria',1)
    SolaceViewVars('save_aus_id',save_aus_id,'Status_Report_Criteria',1)
    SolaceViewVars('save_aud_id',save_aud_id,'Status_Report_Criteria',1)
    SolaceViewVars('save_par_id',save_par_id,'Status_Report_Criteria',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Status_Report_Criteria',1)
    SolaceViewVars('save_stac_id',save_stac_id,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:Description',tmp:Description,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:NewDescription',tmp:NewDescription,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:OutputType',tmp:OutputType,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:PaperReportType',tmp:PaperReportType,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:ExportJobs',tmp:ExportJobs,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:ExportAudit',tmp:ExportAudit,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:ExportParts',tmp:ExportParts,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:ExportWarparts',tmp:ExportWarparts,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:ExportPath',tmp:ExportPath,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:HeadAccountTag',tmp:HeadAccountTag,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:SubAccountTag',tmp:SubAccountTag,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:No',tmp:No,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:CChargeTypeTag',tmp:CChargeTypeTag,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:Yes',tmp:Yes,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:WChargeType',tmp:WChargeType,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:CRepairTypeTag',tmp:CRepairTypeTag,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:WRepairTypeTag',tmp:WRepairTypeTag,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:StatusType',tmp:StatusType,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:LocationTag',tmp:LocationTag,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:UserTag',tmp:UserTag,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:ENGINEER',tmp:ENGINEER,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:Manufacturer',tmp:Manufacturer,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:SelectManufacturer',tmp:SelectManufacturer,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:ManufacturerTag',tmp:ManufacturerTag,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:ModelNumberTag',tmp:ModelNumberTag,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:UnitTypeTag',tmp:UnitTypeTag,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:TransitTypeTag',tmp:TransitTypeTag,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:TurnaroundTimeTag',tmp:TurnaroundTimeTag,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:StatusTag',tmp:StatusTag,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:SavedCriteria',tmp:SavedCriteria,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:Workshop',tmp:Workshop,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:JobType',tmp:JobType,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:CompletedType',tmp:CompletedType,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:DespatchedType',tmp:DespatchedType,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:JobBatchNumber',tmp:JobBatchNumber,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:EDIBatchNumber',tmp:EDIBatchNumber,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:ReportOrder',tmp:ReportOrder,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:DateRangeType',tmp:DateRangeType,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:StartDate',tmp:StartDate,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:EndDate',tmp:EndDate,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:InvoiceType',tmp:InvoiceType,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:ActualPartsCost',tmp:ActualPartsCost,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:FirstPart',tmp:FirstPart,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:CustomerStatus',tmp:CustomerStatus,'Status_Report_Criteria',1)
    SolaceViewVars('tmp:ColourTag',tmp:ColourTag,'Status_Report_Criteria',1)
    SolaceViewVars('ConnectionStr',ConnectionStr,'Status_Report_Criteria',1)
    SolaceViewVars('INIFilePath',INIFilePath,'Status_Report_Criteria',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt46;  SolaceCtrlName = '?Prompt46';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SelectCriteria;  SolaceCtrlName = '?SelectCriteria';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt47;  SolaceCtrlName = '?Prompt47';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt48;  SolaceCtrlName = '?Prompt48';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SavedCriteria;  SolaceCtrlName = '?tmp:SavedCriteria';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:OutputType;  SolaceCtrlName = '?tmp:OutputType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:OutputType:Radio1;  SolaceCtrlName = '?tmp:OutputType:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:OutputType:Radio2;  SolaceCtrlName = '?tmp:OutputType:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt66;  SolaceCtrlName = '?Prompt66';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7:2;  SolaceCtrlName = '?Prompt7:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7:3;  SolaceCtrlName = '?Prompt7:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:PaperReportType;  SolaceCtrlName = '?tmp:PaperReportType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:PaperReportType:Radio1;  SolaceCtrlName = '?tmp:PaperReportType:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:PaperReportType:Radio2;  SolaceCtrlName = '?tmp:PaperReportType:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:PaperReportType:Radio3;  SolaceCtrlName = '?tmp:PaperReportType:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6:2;  SolaceCtrlName = '?Prompt6:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6:3;  SolaceCtrlName = '?Prompt6:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt12;  SolaceCtrlName = '?Prompt12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt13;  SolaceCtrlName = '?Prompt13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt13:2;  SolaceCtrlName = '?Prompt13:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupExportPath;  SolaceCtrlName = '?LookupExportPath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?local:FileNameJobs:Prompt;  SolaceCtrlName = '?local:FileNameJobs:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?local:FileNameJobs;  SolaceCtrlName = '?local:FileNameJobs';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?local:FileNameAudit:Prompt;  SolaceCtrlName = '?local:FileNameAudit:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?local:FileNameAudit;  SolaceCtrlName = '?local:FileNameAudit';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupAuditPath;  SolaceCtrlName = '?LookupAuditPath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?local:FileNameParts:Prompt;  SolaceCtrlName = '?local:FileNameParts:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?local:FileNameParts;  SolaceCtrlName = '?local:FileNameParts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupCharPartsName;  SolaceCtrlName = '?LookupCharPartsName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?local:FileNameWarParts:Prompt;  SolaceCtrlName = '?local:FileNameWarParts:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?local:FileNameWarParts;  SolaceCtrlName = '?local:FileNameWarParts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupWarPartsPath;  SolaceCtrlName = '?LookupWarPartsPath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt12:2;  SolaceCtrlName = '?Prompt12:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExportJobs;  SolaceCtrlName = '?tmp:ExportJobs';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExportAudit;  SolaceCtrlName = '?tmp:ExportAudit';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExportParts;  SolaceCtrlName = '?tmp:ExportParts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExportWarparts;  SolaceCtrlName = '?tmp:ExportWarparts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1;  SolaceCtrlName = '?String1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt17;  SolaceCtrlName = '?Prompt17';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt65;  SolaceCtrlName = '?Prompt65';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt18;  SolaceCtrlName = '?Prompt18';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt19;  SolaceCtrlName = '?Prompt19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:2;  SolaceCtrlName = '?DASREVTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:2;  SolaceCtrlName = '?DASSHOWTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:2;  SolaceCtrlName = '?DASTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:2;  SolaceCtrlName = '?DASTAGAll:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:2;  SolaceCtrlName = '?DASUNTAGALL:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab18;  SolaceCtrlName = '?Tab18';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt49;  SolaceCtrlName = '?Prompt49';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Workshop;  SolaceCtrlName = '?tmp:Workshop';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Workshop:Radio1;  SolaceCtrlName = '?tmp:Workshop:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Workshop:Radio2;  SolaceCtrlName = '?tmp:Workshop:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Workshop:Radio3;  SolaceCtrlName = '?tmp:Workshop:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt50;  SolaceCtrlName = '?Prompt50';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobType;  SolaceCtrlName = '?tmp:JobType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobType:Radio1;  SolaceCtrlName = '?tmp:JobType:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobType:Radio2;  SolaceCtrlName = '?tmp:JobType:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobType:Radio3;  SolaceCtrlName = '?tmp:JobType:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobType:Radio4;  SolaceCtrlName = '?tmp:JobType:Radio4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobType:Radio5;  SolaceCtrlName = '?tmp:JobType:Radio5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobType:Radio6;  SolaceCtrlName = '?tmp:JobType:Radio6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt51;  SolaceCtrlName = '?Prompt51';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CompletedType;  SolaceCtrlName = '?tmp:CompletedType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CompletedType:Radio1;  SolaceCtrlName = '?tmp:CompletedType:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CompletedType:Radio2;  SolaceCtrlName = '?tmp:CompletedType:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CompletedType:Radio3;  SolaceCtrlName = '?tmp:CompletedType:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt52;  SolaceCtrlName = '?Prompt52';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DespatchedType;  SolaceCtrlName = '?tmp:DespatchedType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DespatchedType:Radio1;  SolaceCtrlName = '?tmp:DespatchedType:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DespatchedType:Radio2;  SolaceCtrlName = '?tmp:DespatchedType:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DespatchedType:Radio3;  SolaceCtrlName = '?tmp:DespatchedType:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt67;  SolaceCtrlName = '?Prompt67';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:InvoiceType;  SolaceCtrlName = '?tmp:InvoiceType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:InvoiceType:Radio1;  SolaceCtrlName = '?tmp:InvoiceType:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:InvoiceType:Radio2;  SolaceCtrlName = '?tmp:InvoiceType:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:InvoiceType:Radio3;  SolaceCtrlName = '?tmp:InvoiceType:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab6;  SolaceCtrlName = '?Tab6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt20;  SolaceCtrlName = '?Prompt20';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt21;  SolaceCtrlName = '?Prompt21';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:3;  SolaceCtrlName = '?List:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:3;  SolaceCtrlName = '?DASREVTAG:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:3;  SolaceCtrlName = '?DASSHOWTAG:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:3;  SolaceCtrlName = '?DASTAG:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:3;  SolaceCtrlName = '?DASTAGAll:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:3;  SolaceCtrlName = '?DASUNTAGALL:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab7;  SolaceCtrlName = '?Tab7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt22;  SolaceCtrlName = '?Prompt22';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt23;  SolaceCtrlName = '?Prompt23';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:4;  SolaceCtrlName = '?List:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:4;  SolaceCtrlName = '?DASREVTAG:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:4;  SolaceCtrlName = '?DASSHOWTAG:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:4;  SolaceCtrlName = '?DASTAG:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:4;  SolaceCtrlName = '?DASTAGAll:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:4;  SolaceCtrlName = '?DASUNTAGALL:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab8;  SolaceCtrlName = '?Tab8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt24;  SolaceCtrlName = '?Prompt24';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt25;  SolaceCtrlName = '?Prompt25';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:5;  SolaceCtrlName = '?List:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:5;  SolaceCtrlName = '?DASREVTAG:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:5;  SolaceCtrlName = '?DASSHOWTAG:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:5;  SolaceCtrlName = '?DASTAG:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:5;  SolaceCtrlName = '?DASTAGAll:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:5;  SolaceCtrlName = '?DASUNTAGALL:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab9;  SolaceCtrlName = '?Tab9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt26;  SolaceCtrlName = '?Prompt26';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt27;  SolaceCtrlName = '?Prompt27';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:6;  SolaceCtrlName = '?List:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:6;  SolaceCtrlName = '?DASREVTAG:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:6;  SolaceCtrlName = '?DASSHOWTAG:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:6;  SolaceCtrlName = '?DASTAG:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:6;  SolaceCtrlName = '?DASTAGAll:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:6;  SolaceCtrlName = '?DASUNTAGALL:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab10;  SolaceCtrlName = '?Tab10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt28;  SolaceCtrlName = '?Prompt28';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt29;  SolaceCtrlName = '?Prompt29';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt30;  SolaceCtrlName = '?Prompt30';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StatusType;  SolaceCtrlName = '?tmp:StatusType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StatusType:Radio1;  SolaceCtrlName = '?tmp:StatusType:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StatusType:Radio2;  SolaceCtrlName = '?tmp:StatusType:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StatusType:Radio3;  SolaceCtrlName = '?tmp:StatusType:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CustomerStatus;  SolaceCtrlName = '?tmp:CustomerStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:7;  SolaceCtrlName = '?List:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:7;  SolaceCtrlName = '?DASREVTAG:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:7;  SolaceCtrlName = '?DASSHOWTAG:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:7;  SolaceCtrlName = '?DASTAG:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:7;  SolaceCtrlName = '?DASTAGAll:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:7;  SolaceCtrlName = '?DASUNTAGALL:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab11;  SolaceCtrlName = '?Tab11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt31;  SolaceCtrlName = '?Prompt31';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt32;  SolaceCtrlName = '?Prompt32';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:8;  SolaceCtrlName = '?List:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:8;  SolaceCtrlName = '?DASREVTAG:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:8;  SolaceCtrlName = '?DASSHOWTAG:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:8;  SolaceCtrlName = '?DASTAG:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:8;  SolaceCtrlName = '?DASTAGAll:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:8;  SolaceCtrlName = '?DASUNTAGALL:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab12;  SolaceCtrlName = '?Tab12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt33;  SolaceCtrlName = '?Prompt33';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt34;  SolaceCtrlName = '?Prompt34';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:9;  SolaceCtrlName = '?List:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:9;  SolaceCtrlName = '?DASREVTAG:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:9;  SolaceCtrlName = '?DASSHOWTAG:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:9;  SolaceCtrlName = '?DASTAG:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:9;  SolaceCtrlName = '?DASTAGAll:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:9;  SolaceCtrlName = '?DASUNTAGALL:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab13;  SolaceCtrlName = '?Tab13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt35;  SolaceCtrlName = '?Prompt35';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt36;  SolaceCtrlName = '?Prompt36';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt64;  SolaceCtrlName = '?Prompt64';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:10;  SolaceCtrlName = '?List:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:10;  SolaceCtrlName = '?DASREVTAG:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:10;  SolaceCtrlName = '?DASSHOWTAG:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:10;  SolaceCtrlName = '?DASTAG:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:10;  SolaceCtrlName = '?DASTAGAll:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:10;  SolaceCtrlName = '?DASUNTAGALL:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab14;  SolaceCtrlName = '?Tab14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt37;  SolaceCtrlName = '?Prompt37';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt38;  SolaceCtrlName = '?Prompt38';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SelectManufacturer;  SolaceCtrlName = '?tmp:SelectManufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Manufacturer;  SolaceCtrlName = '?tmp:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt39;  SolaceCtrlName = '?Prompt39';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:11;  SolaceCtrlName = '?List:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:11;  SolaceCtrlName = '?DASREVTAG:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:11;  SolaceCtrlName = '?DASSHOWTAG:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:11;  SolaceCtrlName = '?DASTAG:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:11;  SolaceCtrlName = '?DASTAGAll:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:11;  SolaceCtrlName = '?DASUNTAGALL:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab15;  SolaceCtrlName = '?Tab15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt40;  SolaceCtrlName = '?Prompt40';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt41;  SolaceCtrlName = '?Prompt41';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:12;  SolaceCtrlName = '?List:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:12;  SolaceCtrlName = '?DASREVTAG:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:12;  SolaceCtrlName = '?DASSHOWTAG:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:12;  SolaceCtrlName = '?DASTAG:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:12;  SolaceCtrlName = '?DASTAGAll:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:12;  SolaceCtrlName = '?DASUNTAGALL:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab16;  SolaceCtrlName = '?Tab16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt42;  SolaceCtrlName = '?Prompt42';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt43;  SolaceCtrlName = '?Prompt43';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:13;  SolaceCtrlName = '?List:13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:13;  SolaceCtrlName = '?DASREVTAG:13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:13;  SolaceCtrlName = '?DASSHOWTAG:13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:13;  SolaceCtrlName = '?DASTAG:13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:13;  SolaceCtrlName = '?DASTAGAll:13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:13;  SolaceCtrlName = '?DASUNTAGALL:13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab17;  SolaceCtrlName = '?Tab17';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt44;  SolaceCtrlName = '?Prompt44';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt45;  SolaceCtrlName = '?Prompt45';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:14;  SolaceCtrlName = '?List:14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:14;  SolaceCtrlName = '?DASREVTAG:14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:14;  SolaceCtrlName = '?DASSHOWTAG:14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:14;  SolaceCtrlName = '?DASTAG:14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:14;  SolaceCtrlName = '?DASTAGAll:14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:14;  SolaceCtrlName = '?DASUNTAGALL:14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab20;  SolaceCtrlName = '?Tab20';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ColourTitle;  SolaceCtrlName = '?ColourTitle';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ColourText;  SolaceCtrlName = '?ColourText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:15;  SolaceCtrlName = '?List:15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:15;  SolaceCtrlName = '?DASREVTAG:15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:15;  SolaceCtrlName = '?DASSHOWTAG:15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:15;  SolaceCtrlName = '?DASTAG:15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:15;  SolaceCtrlName = '?DASTAGAll:15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:15;  SolaceCtrlName = '?DASUNTAGALL:15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab19;  SolaceCtrlName = '?Tab19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt53;  SolaceCtrlName = '?Prompt53';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt54;  SolaceCtrlName = '?Prompt54';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobBatchNumber:Prompt;  SolaceCtrlName = '?tmp:JobBatchNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobBatchNumber;  SolaceCtrlName = '?tmp:JobBatchNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EDIBatchNumber:Prompt;  SolaceCtrlName = '?tmp:EDIBatchNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EDIBatchNumber;  SolaceCtrlName = '?tmp:EDIBatchNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt57;  SolaceCtrlName = '?Prompt57';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt58;  SolaceCtrlName = '?Prompt58';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt59;  SolaceCtrlName = '?Prompt59';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ReportOrder;  SolaceCtrlName = '?tmp:ReportOrder';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt60;  SolaceCtrlName = '?Prompt60';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt61;  SolaceCtrlName = '?Prompt61';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DateRangeType;  SolaceCtrlName = '?tmp:DateRangeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DateRangeType:Radio1;  SolaceCtrlName = '?tmp:DateRangeType:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DateRangeType:Radio2;  SolaceCtrlName = '?tmp:DateRangeType:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DateRangeType:Radio3;  SolaceCtrlName = '?tmp:DateRangeType:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate:Prompt;  SolaceCtrlName = '?tmp:StartDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate;  SolaceCtrlName = '?tmp:StartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupStartDate;  SolaceCtrlName = '?LookupStartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate:Prompt;  SolaceCtrlName = '?tmp:EndDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate;  SolaceCtrlName = '?tmp:EndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupEndDate;  SolaceCtrlName = '?LookupEndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1;  SolaceCtrlName = '?Image1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ButtonPanel;  SolaceCtrlName = '?ButtonPanel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VSBackButton;  SolaceCtrlName = '?VSBackButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VSNextButton;  SolaceCtrlName = '?VSNextButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Finish;  SolaceCtrlName = '?Finish';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Status_Report_Criteria')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Status_Report_Criteria')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt46
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:COLOUR.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:JOBS2_ALIAS.Open
  Relate:REPTYDEF.Open
  Relate:STATCRIT.Open
  Relate:STATUS.Open
  Relate:TRANTYPE.Open
  Relate:TURNARND.Open
  Access:STATREP.UseFile
  Access:JOBS.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBTHIRD.UseFile
  Access:JOBSE.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:LOAN.UseFile
  Access:EXCHHIST.UseFile
  Access:JOBEXHIS.UseFile
  Access:AUDSTATS.UseFile
  SELF.FilesOpened = True
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If Sub(Clip(def:ExportPath),1,-1) <> '\'
      def:ExportPath = Clip(def:ExportPath) & '\'
  End !Sub(def:ExportPath,1,-1) = '\'
  local:FileNameJobs  = Clip(def:ExportPath) & 'JOBSDATA.CSV'
  local:FileNameAudit = Clip(def:ExportPath) & 'AUDITDATA.CSV'
  local:FileNameParts = Clip(def:ExportPath) & 'PARTSDATA.CSV'
  local:FileNameWarParts   = Clip(def:ExportPath) & 'WARPARTS.CSV'
  
  tmp:StartDate = Deformat('1/1/1990',@d6)
  tmp:EndDate = Today()
  tmp:ReportOrder = 'DATE BOOKED'
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  BRW5.Init(?List:2,Queue:Browse:1.ViewPosition,BRW5::View:Browse,Queue:Browse:1,Relate:SUBTRACC,SELF)
  BRW6.Init(?List:3,Queue:Browse:2.ViewPosition,BRW6::View:Browse,Queue:Browse:2,Relate:CHARTYPE,SELF)
  BRW7.Init(?List:4,Queue:Browse:3.ViewPosition,BRW7::View:Browse,Queue:Browse:3,Relate:CHARTYPE,SELF)
  BRW8.Init(?List:5,Queue:Browse:4.ViewPosition,BRW8::View:Browse,Queue:Browse:4,Relate:REPTYDEF,SELF)
  BRW9.Init(?List:6,Queue:Browse:5.ViewPosition,BRW9::View:Browse,Queue:Browse:5,Relate:REPTYDEF,SELF)
  BRW10.Init(?List:7,Queue:Browse:6.ViewPosition,BRW10::View:Browse,Queue:Browse:6,Relate:STATUS,SELF)
  BRW14.Init(?List:8,Queue:Browse:7.ViewPosition,BRW14::View:Browse,Queue:Browse:7,Relate:LOCINTER,SELF)
  BRW15.Init(?List:9,Queue:Browse:8.ViewPosition,BRW15::View:Browse,Queue:Browse:8,Relate:USERS,SELF)
  BRW16.Init(?List:10,Queue:Browse:9.ViewPosition,BRW16::View:Browse,Queue:Browse:9,Relate:MANUFACT,SELF)
  BRW17.Init(?List:11,Queue:Browse:10.ViewPosition,BRW17::View:Browse,Queue:Browse:10,Relate:MODELNUM,SELF)
  BRW21.Init(?List:12,Queue:Browse:11.ViewPosition,BRW21::View:Browse,Queue:Browse:11,Relate:UNITTYPE,SELF)
  BRW22.Init(?List:13,Queue:Browse:12.ViewPosition,BRW22::View:Browse,Queue:Browse:12,Relate:TRANTYPE,SELF)
  BRW23.Init(?List:14,Queue:Browse:13.ViewPosition,BRW23::View:Browse,Queue:Browse:13,Relate:TURNARND,SELF)
  BRW42.Init(?List:15,Queue:Browse:14.ViewPosition,BRW42::View:Browse,Queue:Browse:14,Relate:COLOUR,SELF)
  OPEN(window)
  SELF.Opened=True
  !Rename colour fields
  If Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
      ?ColourTitle{prop:Text} = Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI'))
      ?List:15{prop:Format} = '11L(2)I@s1@120L(2)|M~' & Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI')) & '~@s30@'
  End !Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ?List:4{prop:vcr} = TRUE
  ?List:5{prop:vcr} = TRUE
  ?List:6{prop:vcr} = TRUE
  ?List:7{prop:vcr} = TRUE
  ?List:8{prop:vcr} = TRUE
  ?List:9{prop:vcr} = TRUE
  ?List:10{prop:vcr} = TRUE
  ?List:11{prop:vcr} = TRUE
  ?List:12{prop:vcr} = TRUE
  ?List:13{prop:vcr} = TRUE
  ?List:14{prop:vcr} = TRUE
  ?List:15{prop:vcr} = TRUE
  ?tmp:ReportOrder{prop:vcr} = TRUE
    Wizard13.Init(?Sheet1, |                          ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Finish, |                       ! OK button
                     ?Cancel, |                       ! Cancel button
                     1, |                             ! Skip hidden tabs
                     1, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  BRW4.Q &= Queue:Browse
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,tra:Account_Number_Key)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,tra:Account_Number,1,BRW4)
  BIND('tmp:HeadAccountTag',tmp:HeadAccountTag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW4.AddField(tmp:HeadAccountTag,BRW4.Q.tmp:HeadAccountTag)
  BRW4.AddField(tra:Account_Number,BRW4.Q.tra:Account_Number)
  BRW4.AddField(tra:Company_Name,BRW4.Q.tra:Company_Name)
  BRW4.AddField(tra:RecordNumber,BRW4.Q.tra:RecordNumber)
  BRW5.Q &= Queue:Browse:1
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,sub:Account_Number_Key)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,sub:Account_Number,1,BRW5)
  BIND('tmp:SubAccountTag',tmp:SubAccountTag)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW5.AddField(tmp:SubAccountTag,BRW5.Q.tmp:SubAccountTag)
  BRW5.AddField(sub:Account_Number,BRW5.Q.sub:Account_Number)
  BRW5.AddField(sub:Company_Name,BRW5.Q.sub:Company_Name)
  BRW5.AddField(sub:RecordNumber,BRW5.Q.sub:RecordNumber)
  BRW6.Q &= Queue:Browse:2
  BRW6.RetainRow = 0
  BRW6.AddSortOrder(,cha:Warranty_Key)
  BRW6.AddRange(cha:Warranty,tmp:No)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,cha:Charge_Type,1,BRW6)
  BIND('tmp:CChargeTypeTag',tmp:CChargeTypeTag)
  ?List:3{PROP:IconList,1} = '~notick1.ico'
  ?List:3{PROP:IconList,2} = '~tick1.ico'
  BRW6.AddField(tmp:CChargeTypeTag,BRW6.Q.tmp:CChargeTypeTag)
  BRW6.AddField(cha:Charge_Type,BRW6.Q.cha:Charge_Type)
  BRW6.AddField(cha:Warranty,BRW6.Q.cha:Warranty)
  BRW7.Q &= Queue:Browse:3
  BRW7.RetainRow = 0
  BRW7.AddSortOrder(,cha:Warranty_Key)
  BRW7.AddRange(cha:Warranty,tmp:Yes)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,cha:Charge_Type,1,BRW7)
  BIND('tmp:WChargeType',tmp:WChargeType)
  ?List:4{PROP:IconList,1} = '~notick1.ico'
  ?List:4{PROP:IconList,2} = '~tick1.ico'
  BRW7.AddField(tmp:WChargeType,BRW7.Q.tmp:WChargeType)
  BRW7.AddField(cha:Charge_Type,BRW7.Q.cha:Charge_Type)
  BRW7.AddField(cha:Warranty,BRW7.Q.cha:Warranty)
  BRW8.Q &= Queue:Browse:4
  BRW8.RetainRow = 0
  BRW8.AddSortOrder(,rtd:Chargeable_Key)
  BRW8.AddRange(rtd:Chargeable,tmp:Yes)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,rtd:Repair_Type,1,BRW8)
  BIND('tmp:CRepairTypeTag',tmp:CRepairTypeTag)
  ?List:5{PROP:IconList,1} = '~notick1.ico'
  ?List:5{PROP:IconList,2} = '~tick1.ico'
  BRW8.AddField(tmp:CRepairTypeTag,BRW8.Q.tmp:CRepairTypeTag)
  BRW8.AddField(rtd:Repair_Type,BRW8.Q.rtd:Repair_Type)
  BRW8.AddField(rtd:Chargeable,BRW8.Q.rtd:Chargeable)
  BRW9.Q &= Queue:Browse:5
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,rtd:Warranty_Key)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,rtd:Warranty,1,BRW9)
  BIND('tmp:WRepairTypeTag',tmp:WRepairTypeTag)
  ?List:6{PROP:IconList,1} = '~notick1.ico'
  ?List:6{PROP:IconList,2} = '~tick1.ico'
  BRW9.AddField(tmp:WRepairTypeTag,BRW9.Q.tmp:WRepairTypeTag)
  BRW9.AddField(rtd:Repair_Type,BRW9.Q.rtd:Repair_Type)
  BRW9.AddField(rtd:Warranty,BRW9.Q.rtd:Warranty)
  BRW10.Q &= Queue:Browse:6
  BRW10.RetainRow = 0
  BRW10.AddSortOrder(,sts:JobKey)
  BRW10.AddRange(sts:Job,tmp:Yes)
  BRW10.AddLocator(BRW10::Sort1:Locator)
  BRW10::Sort1:Locator.Init(,sts:Status,1,BRW10)
  BRW10.AddSortOrder(,sts:ExchangeKey)
  BRW10.AddRange(sts:Exchange,tmp:Yes)
  BRW10.AddLocator(BRW10::Sort2:Locator)
  BRW10::Sort2:Locator.Init(,sts:Status,1,BRW10)
  BRW10.AddSortOrder(,sts:LoanKey)
  BRW10.AddRange(sts:Loan,tmp:Yes)
  BRW10.AddLocator(BRW10::Sort3:Locator)
  BRW10::Sort3:Locator.Init(,sts:Status,1,BRW10)
  BRW10.AddSortOrder(,sts:Status_Key)
  BRW10.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(,sts:Status,1,BRW10)
  BIND('tmp:StatusTag',tmp:StatusTag)
  ?List:7{PROP:IconList,1} = '~notick1.ico'
  ?List:7{PROP:IconList,2} = '~tick1.ico'
  BRW10.AddField(tmp:StatusTag,BRW10.Q.tmp:StatusTag)
  BRW10.AddField(sts:Status,BRW10.Q.sts:Status)
  BRW10.AddField(sts:Ref_Number,BRW10.Q.sts:Ref_Number)
  BRW10.AddField(sts:Job,BRW10.Q.sts:Job)
  BRW10.AddField(sts:Exchange,BRW10.Q.sts:Exchange)
  BRW10.AddField(sts:Loan,BRW10.Q.sts:Loan)
  BRW14.Q &= Queue:Browse:7
  BRW14.RetainRow = 0
  BRW14.AddSortOrder(,loi:Location_Key)
  BRW14.AddLocator(BRW14::Sort0:Locator)
  BRW14::Sort0:Locator.Init(,loi:Location,1,BRW14)
  BIND('tmp:LocationTag',tmp:LocationTag)
  ?List:8{PROP:IconList,1} = '~notick1.ico'
  ?List:8{PROP:IconList,2} = '~tick1.ico'
  BRW14.AddField(tmp:LocationTag,BRW14.Q.tmp:LocationTag)
  BRW14.AddField(loi:Location,BRW14.Q.loi:Location)
  BRW15.Q &= Queue:Browse:8
  BRW15.RetainRow = 0
  BRW15.AddSortOrder(,use:User_Type_Key)
  BRW15.AddRange(use:User_Type,tmp:ENGINEER)
  BRW15.AddLocator(BRW15::Sort0:Locator)
  BRW15::Sort0:Locator.Init(,use:Surname,1,BRW15)
  BIND('tmp:UserTag',tmp:UserTag)
  ?List:9{PROP:IconList,1} = '~notick1.ico'
  ?List:9{PROP:IconList,2} = '~tick1.ico'
  BRW15.AddField(tmp:UserTag,BRW15.Q.tmp:UserTag)
  BRW15.AddField(use:Surname,BRW15.Q.use:Surname)
  BRW15.AddField(use:Forename,BRW15.Q.use:Forename)
  BRW15.AddField(use:User_Code,BRW15.Q.use:User_Code)
  BRW15.AddField(use:User_Type,BRW15.Q.use:User_Type)
  BRW16.Q &= Queue:Browse:9
  BRW16.RetainRow = 0
  BRW16.AddSortOrder(,man:Manufacturer_Key)
  BRW16.AddLocator(BRW16::Sort0:Locator)
  BRW16::Sort0:Locator.Init(,man:Manufacturer,1,BRW16)
  BIND('tmp:ManufacturerTag',tmp:ManufacturerTag)
  ?List:10{PROP:IconList,1} = '~notick1.ico'
  ?List:10{PROP:IconList,2} = '~tick1.ico'
  BRW16.AddField(tmp:ManufacturerTag,BRW16.Q.tmp:ManufacturerTag)
  BRW16.AddField(man:Manufacturer,BRW16.Q.man:Manufacturer)
  BRW16.AddField(man:RecordNumber,BRW16.Q.man:RecordNumber)
  BRW17.Q &= Queue:Browse:10
  BRW17.RetainRow = 0
  BRW17.AddSortOrder(,mod:Model_Number_Key)
  BRW17.AddLocator(BRW17::Sort1:Locator)
  BRW17::Sort1:Locator.Init(,mod:Model_Number,1,BRW17)
  BRW17.AddSortOrder(,mod:Manufacturer_Key)
  BRW17.AddRange(mod:Manufacturer,tmp:Manufacturer)
  BRW17.AddLocator(BRW17::Sort2:Locator)
  BRW17::Sort2:Locator.Init(,mod:Model_Number,1,BRW17)
  BRW17.AddSortOrder(,mod:Manufacturer_Key)
  BRW17.AddLocator(BRW17::Sort0:Locator)
  BRW17::Sort0:Locator.Init(,mod:Manufacturer,1,BRW17)
  BIND('tmp:ModelNumberTag',tmp:ModelNumberTag)
  ?List:11{PROP:IconList,1} = '~notick1.ico'
  ?List:11{PROP:IconList,2} = '~tick1.ico'
  BRW17.AddField(tmp:ModelNumberTag,BRW17.Q.tmp:ModelNumberTag)
  BRW17.AddField(mod:Model_Number,BRW17.Q.mod:Model_Number)
  BRW17.AddField(mod:Manufacturer,BRW17.Q.mod:Manufacturer)
  BRW21.Q &= Queue:Browse:11
  BRW21.RetainRow = 0
  BRW21.AddSortOrder(,uni:Unit_Type_Key)
  BRW21.AddLocator(BRW21::Sort0:Locator)
  BRW21::Sort0:Locator.Init(,uni:Unit_Type,1,BRW21)
  BIND('tmp:UnitTypeTag',tmp:UnitTypeTag)
  ?List:12{PROP:IconList,1} = '~notick1.ico'
  ?List:12{PROP:IconList,2} = '~tick1.ico'
  BRW21.AddField(tmp:UnitTypeTag,BRW21.Q.tmp:UnitTypeTag)
  BRW21.AddField(uni:Unit_Type,BRW21.Q.uni:Unit_Type)
  BRW22.Q &= Queue:Browse:12
  BRW22.RetainRow = 0
  BRW22.AddSortOrder(,trt:Transit_Type_Key)
  BRW22.AddLocator(BRW22::Sort0:Locator)
  BRW22::Sort0:Locator.Init(,trt:Transit_Type,1,BRW22)
  BIND('tmp:TransitTypeTag',tmp:TransitTypeTag)
  ?List:13{PROP:IconList,1} = '~notick1.ico'
  ?List:13{PROP:IconList,2} = '~tick1.ico'
  BRW22.AddField(tmp:TransitTypeTag,BRW22.Q.tmp:TransitTypeTag)
  BRW22.AddField(trt:Transit_Type,BRW22.Q.trt:Transit_Type)
  BRW23.Q &= Queue:Browse:13
  BRW23.RetainRow = 0
  BRW23.AddSortOrder(,tur:Turnaround_Time_Key)
  BRW23.AddLocator(BRW23::Sort0:Locator)
  BRW23::Sort0:Locator.Init(,tur:Turnaround_Time,1,BRW23)
  BIND('tmp:TurnaroundTimeTag',tmp:TurnaroundTimeTag)
  ?List:14{PROP:IconList,1} = '~notick1.ico'
  ?List:14{PROP:IconList,2} = '~tick1.ico'
  BRW23.AddField(tmp:TurnaroundTimeTag,BRW23.Q.tmp:TurnaroundTimeTag)
  BRW23.AddField(tur:Turnaround_Time,BRW23.Q.tur:Turnaround_Time)
  BRW42.Q &= Queue:Browse:14
  BRW42.RetainRow = 0
  BRW42.AddSortOrder(,col:Colour_Key)
  BRW42.AddLocator(BRW42::Sort0:Locator)
  BRW42::Sort0:Locator.Init(,col:Colour,1,BRW42)
  BIND('tmp:ColourTag',tmp:ColourTag)
  ?List:15{PROP:IconList,1} = '~notick1.ico'
  ?List:15{PROP:IconList,2} = '~tick1.ico'
  BRW42.AddField(tmp:ColourTag,BRW42.Q.tmp:ColourTag)
  BRW42.AddField(col:Colour,BRW42.Q.col:Colour)
  IF ?tmp:SelectManufacturer{Prop:Checked} = True
    ENABLE(?Prompt39)
    ENABLE(?tmp:Manufacturer)
  END
  IF ?tmp:SelectManufacturer{Prop:Checked} = False
    DISABLE(?Prompt39)
    DISABLE(?tmp:Manufacturer)
  END
  FDCB18.Init(tmp:Manufacturer,?tmp:Manufacturer,Queue:FileDropCombo.ViewPosition,FDCB18::View:FileDropCombo,Queue:FileDropCombo,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB18.Q &= Queue:FileDropCombo
  FDCB18.AddSortOrder(man:Manufacturer_Key)
  FDCB18.AddField(man:Manufacturer,FDCB18.Q.man:Manufacturer)
  FDCB18.AddField(man:RecordNumber,FDCB18.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB18.WindowComponent)
  FDCB18.DefaultFill = 0
  FileLookup34.Init
  FileLookup34.Flags=BOR(FileLookup34.Flags,FILE:LongName)
  FileLookup34.Flags=BOR(FileLookup34.Flags,FILE:Directory)
  FileLookup34.SetMask('CSV Files','*.CSV')
  FileLookup34.WindowTitle='Export File Name'
  FileLookup44.Init
  FileLookup44.Flags=BOR(FileLookup44.Flags,FILE:LongName)
  FileLookup44.Flags=BOR(FileLookup44.Flags,FILE:Directory)
  FileLookup44.SetMask('CSV Files','*.CSV')
  FileLookup44.WindowTitle='Export File Name'
  FileLookup45.Init
  FileLookup45.Flags=BOR(FileLookup45.Flags,FILE:LongName)
  FileLookup45.Flags=BOR(FileLookup45.Flags,FILE:Directory)
  FileLookup45.SetMask('CSV Files','*.CSV')
  FileLookup45.WindowTitle='Export File Name'
  FileLookup46.Init
  FileLookup46.Flags=BOR(FileLookup46.Flags,FILE:LongName)
  FileLookup46.Flags=BOR(FileLookup46.Flags,FILE:Directory)
  FileLookup46.SetMask('CSV Files','*.CSV')
  FileLookup46.WindowTitle='Export File Name'
  BRW4.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW4.AskProcedure = 0
      CLEAR(BRW4.AskProcedure, 1)
    END
  END
  BRW5.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW5.AskProcedure = 0
      CLEAR(BRW5.AskProcedure, 1)
    END
  END
  BRW6.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW6.AskProcedure = 0
      CLEAR(BRW6.AskProcedure, 1)
    END
  END
  BRW7.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW7.AskProcedure = 0
      CLEAR(BRW7.AskProcedure, 1)
    END
  END
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  BRW10.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW10.AskProcedure = 0
      CLEAR(BRW10.AskProcedure, 1)
    END
  END
  BRW14.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW14.AskProcedure = 0
      CLEAR(BRW14.AskProcedure, 1)
    END
  END
  BRW15.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW15.AskProcedure = 0
      CLEAR(BRW15.AskProcedure, 1)
    END
  END
  BRW16.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW16.AskProcedure = 0
      CLEAR(BRW16.AskProcedure, 1)
    END
  END
  BRW17.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW17.AskProcedure = 0
      CLEAR(BRW17.AskProcedure, 1)
    END
  END
  BRW21.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW21.AskProcedure = 0
      CLEAR(BRW21.AskProcedure, 1)
    END
  END
  BRW22.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW22.AskProcedure = 0
      CLEAR(BRW22.AskProcedure, 1)
    END
  END
  BRW23.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW23.AskProcedure = 0
      CLEAR(BRW23.AskProcedure, 1)
    END
  END
  BRW42.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW42.AskProcedure = 0
      CLEAR(BRW42.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue3)
  ?DASSHOWTAG:3{PROP:Text} = 'Show All'
  ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue4)
  ?DASSHOWTAG:4{PROP:Text} = 'Show All'
  ?DASSHOWTAG:4{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:4{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue5)
  ?DASSHOWTAG:5{PROP:Text} = 'Show All'
  ?DASSHOWTAG:5{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:5{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue6)
  ?DASSHOWTAG:6{PROP:Text} = 'Show All'
  ?DASSHOWTAG:6{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:6{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue7)
  ?DASSHOWTAG:7{PROP:Text} = 'Show All'
  ?DASSHOWTAG:7{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:7{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue8)
  ?DASSHOWTAG:8{PROP:Text} = 'Show All'
  ?DASSHOWTAG:8{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:8{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue9)
  ?DASSHOWTAG:9{PROP:Text} = 'Show All'
  ?DASSHOWTAG:9{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:9{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue10)
  ?DASSHOWTAG:10{PROP:Text} = 'Show All'
  ?DASSHOWTAG:10{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:10{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue11)
  ?DASSHOWTAG:11{PROP:Text} = 'Show All'
  ?DASSHOWTAG:11{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:11{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue12)
  ?DASSHOWTAG:12{PROP:Text} = 'Show All'
  ?DASSHOWTAG:12{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:12{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue13)
  ?DASSHOWTAG:13{PROP:Text} = 'Show All'
  ?DASSHOWTAG:13{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:13{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue14)
  ?DASSHOWTAG:14{PROP:Text} = 'Show All'
  ?DASSHOWTAG:14{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:14{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue15)
  ?DASSHOWTAG:15{PROP:Text} = 'Show All'
  ?DASSHOWTAG:15{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:15{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue3)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue4)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue5)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue6)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue7)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue8)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue9)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue10)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue11)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue12)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue13)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue14)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:COLOUR.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:JOBS2_ALIAS.Close
    Relate:REPTYDEF.Close
    Relate:STATCRIT.Close
    Relate:STATUS.Close
    Relate:TRANTYPE.Close
    Relate:TURNARND.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Status_Report_Criteria',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard13.Validate()
        DISABLE(Wizard13.NextControl())
     ELSE
        ENABLE(Wizard13.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectCriteria
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseStatusCriteria
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SelectCriteria, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              tmp:SavedCriteria   =   star:Description
      
              Free(glo:Queue)
              Free(glo:Queue2)
              Free(glo:Queue3)
              Free(glo:Queue4)
              Free(glo:Queue5)
              Free(glo:Queue6)
              Free(glo:Queue7)
              Free(glo:Queue8)
              Free(glo:Queue9)
              Free(glo:Queue10)
              Free(glo:Queue11)
              Free(glo:Queue12)
              Free(glo:Queue13)
              Free(glo:Queue14)
      
      
              Setcursor(Cursor:Wait)
              Save_STAC_ID = Access:STATCRIT.SaveFile()
              Access:STATCRIT.ClearKey(STAC:DescriptionKey)
              STAC:Description = tmp:SavedCriteria
              Set(STAC:DescriptionKey,STAC:DescriptionKey)
              Loop
                  If Access:STATCRIT.NEXT()
                     Break
                  End !If
                  If STAC:Description <> tmp:SavedCriteria      |
                      Then Break.  ! End If
      
                  Case stac:OptionType
                      Of 'HEAD ACCOUNT'
                          glo:Pointer = stac:FieldValue
                          Add(glo:Queue)
                      Of 'SUB ACCOUNT'
                          glo:Pointer2 = stac:FieldValue
                          Add(glo:Queue2)
                      Of 'CHA CHARGE TYPE'
                          glo:Pointer3 = stac:FieldValue
                          Add(glo:Queue3)
                      Of 'WAR CHARGE TYPE'
                          glo:Pointer4 = stac:FieldValue
                          Add(glo:Queue4)
                      Of 'CHA REPAIR TYPE'
                          glo:Pointer5 = stac:FieldValue
                          Add(glo:Queue5)
                      Of 'WAR REPAIR TYPE'
                          glo:Pointer6 = stac:FieldValue
                          Add(glo:Queue6)
                      Of 'STATUS'
                          glo:Pointer7 = stac:FieldValue
                          Add(glo:Queue7)
                      Of 'LOCATION'
                          glo:Pointer8 = stac:FieldValue
                          Add(glo:Queue8)
                      Of 'ENGINEER'
                          glo:Pointer9 = stac:FieldValue
                          Add(glo:Queue9)
                      Of 'MANUFACTURER'
                          glo:Pointer10 = stac:FieldValue
                          Add(glo:Queue10)
                      Of 'MODEL NUMBER'
                          glo:Pointer11 = stac:FieldValue
                          Add(glo:Queue11)
                      Of 'UNIT TYPE'
                          glo:Pointer12 = stac:FieldValue
                          Add(glo:Queue12)
                      Of 'TRANSIT TYPE'
                          glo:Pointer13 = stac:FieldValue
                          Add(glo:Queue13)
                      Of 'TURNAROUND TIME'
                          glo:Pointer14 = stac:FieldValue
                          Add(glo:Queue14)
                      Of 'OUTPUT TYPE'
                          tmp:OutputType  = stac:FieldValue
                      Of 'PAPER REPORT TYPE'
                          tmp:PaperReportType = stac:FieldValue
                      Of 'EXPORT JOBS'
                          tmp:ExportJobs  = stac:FieldValue
                      Of 'EXPORT AUDIT'
                          tmp:ExportAudit = stac:FieldValue
                      Of 'EXPORT PARTS'
                          tmp:ExportParts = stac:FieldValue
                      Of 'EXPORT WARPARTS'
                          tmp:ExportWarParts = stac:FieldValue
                      Of 'STATUS TYPE'
                          tmp:StatusType = stac:FieldValue
                      Of 'SELECT MANUFACTURER'
                          tmp:SelectManufacturer  = stac:FieldValue
                      Of 'PICK MANUFACTURER'
                          tmp:Manufacturer    = stac:FieldValue
                      Of 'WORKSHOP'
                          tmp:Workshop = stac:FieldValue
                      Of 'JOBTYPE'
                          tmp:JobType = stac:FieldValue
                      Of 'COMPLETED TYPE'
                          tmp:Completedtype = stac:FieldValue
                      Of 'DESPATCHED TYPE'
                          tmp:DespatchedType = stac:FieldValue
                      Of 'JOB BATCH NUMBER'
                          tmp:JobBatchNumber  = stac:FieldValue
                      Of 'EDI BATCH NUMBER'
                          tmp:EDIBatchNumber  = stac:FieldValue
                      Of 'REPORT ORDER'
                          tmp:ReportOrder = stac:FieldValue
                      Of 'DATE RANGE TYPE'
                          tmp:DateRangeType = stac:FieldValue
                  End !Case stac:OptionType
              End !Loop
              Access:STATCRIT.RestoreFile(Save_STAC_ID)
              Setcursor()
      
              Post(Event:Accepted,?tmp:SelectManufacturer)
      
          Of Requestcancelled
      End!Case Globalreponse
      Display()
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SelectCriteria, Accepted)
    OF ?LookupExportPath
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupExportPath, Accepted)
      local:FileNameJobs = Upper(FileLookup34.Ask(1)  )
      DISPLAY
      If Sub(Clip(local:FileNameJobs),1,-1) <> '\'
          local:FileNameJobs = Clip(local:FileNameJobs) & '\'
      End !Sub(local:FileNameJobs,1,-1) <> '\'
      local:FileNameJobs = Clip(local:FileNameJobs) & 'JOBSDATA.CSV'
      Display(?local:FileNameJobs)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupExportPath, Accepted)
    OF ?LookupAuditPath
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupAuditPath, Accepted)
      local:FileNameAudit = Upper(FileLookup44.Ask(1)  )
      DISPLAY
      If Sub(Clip(local:FileNameAUDIT),1,-1) <> '\'
          local:FileNameAUDIT = Clip(local:FileNameAUDIT) & '\'
      End !Sub(local:FileNameAUDIT,1,-1) <> '\'
      local:FileNameAUDIT = Clip(local:FileNameAUDIT) & 'AUDITDATA.CSV'
      Display(?local:FileNameAUDIT)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupAuditPath, Accepted)
    OF ?LookupCharPartsName
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupCharPartsName, Accepted)
      local:FileNameParts = Upper(FileLookup45.Ask(1)  )
      DISPLAY
      If Sub(Clip(local:FileNamePARTS),1,-1) <> '\'
          local:FileNamePARTS = Clip(local:FileNamePARTS) & '\'
      End !Sub(local:FileNamePARTS,1,-1) <> '\'
      local:FileNamePARTS = Clip(local:FileNamePARTS) & 'PARTSDATA.CSV'
      Display(?local:FileNamePARTS)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupCharPartsName, Accepted)
    OF ?LookupWarPartsPath
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupWarPartsPath, Accepted)
      local:FileNameWarParts = Upper(FileLookup46.Ask(1)  )
      DISPLAY
      If Sub(Clip(local:FileNameWARPARTS),1,-1) <> '\'
          local:FileNameWARPARTS = Clip(local:FileNameWARPARTS) & '\'
      End !Sub(local:FileNameWARPARTS,1,-1) <> '\'
      local:FileNameWARPARTS = Clip(local:FileNameWARPARTS) & 'WARPARTSDATA.CSV'
      Display(?local:FileNameWARPARTS)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupWarPartsPath, Accepted)
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::24:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::24:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::24:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::24:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::24:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::27:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::27:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::27:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::27:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::27:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::28:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::28:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::28:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::28:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::28:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::29:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::29:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::29:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::29:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::29:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:StatusType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:StatusType, Accepted)
      BRW10.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:StatusType, Accepted)
    OF ?DASREVTAG:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::30:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::30:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::30:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::30:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::30:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::31:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::31:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::31:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::31:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::31:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::33:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::33:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::33:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::33:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::33:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:SelectManufacturer
      IF ?tmp:SelectManufacturer{Prop:Checked} = True
        ENABLE(?Prompt39)
        ENABLE(?tmp:Manufacturer)
      END
      IF ?tmp:SelectManufacturer{Prop:Checked} = False
        DISABLE(?Prompt39)
        DISABLE(?tmp:Manufacturer)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SelectManufacturer, Accepted)
      BRW17.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SelectManufacturer, Accepted)
    OF ?tmp:Manufacturer
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
      BRW17.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
    OF ?DASREVTAG:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::35:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::35:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::35:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::35:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::35:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::36:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::36:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::36:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::36:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::36:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:13
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::37:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:13
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::37:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:13
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::37:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:13
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::37:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:13
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::37:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:14
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::38:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:14
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::38:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:14
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::38:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:14
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::38:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:14
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::38:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:15
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::43:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:15
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::43:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:15
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::43:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:15
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::43:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:15
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::43:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?VSBackButton
      ThisWindow.Update
         Wizard13.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VSNextButton, Accepted)
      DoNext# = 1
      Case Choice(?Sheet1)
          Of 1
              Case tmp:OutputType
                  Of 0
                      ?Tab3{prop:Hide} = 1
                      ?Tab2{prop:Hide} = 0
                  Of 1
                      ?Tab2{prop:Hide} = 1
                      ?Tab3{prop:Hide} = 0
              End !Case tmp:OutputType
          Of 3
              If tmp:ExportJobs = 0 And |
                  tmp:ExportAudit = 0 And |
                  tmp:ExportParts = 0 And |
                  tmp:ExportWarPArts = 0
                  Case MessageEx('Please select at least one file to export.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  DoNext# = 0
              End !tmp:ExportWarPArts = 0
          Of 6 !Job Types etc
              If tmp:Workshop = 2
                  ?Tab11{prop:Hide} = 1
                  ?Tab12{prop:Hide} = 1
              Else !If tmp:Workshop = 2
                  ?Tab11{prop:Hide} = 0
                  ?Tab12{prop:Hide} = 0
              End !If tmp:Workshop = 2
      
              Case tmp:JobType
                  Of 1 !Warranty Only
                      ?Tab6{prop:Hide} = 1
                      ?Tab8{prop:Hide} = 1
                      ?Tab7{prop:Hide} = 0
                      ?Tab9{prop:Hide} = 0
                      ?tmp:EDIBatchNumber{prop:Hide} = 0
                      ?tmp:EDIBatchNumber:Prompt{prop:Hide} = 0
                  Of 4 !Chargeable Only
                      ?Tab7{prop:Hide} = 1
                      ?Tab9{prop:Hide} = 1
                      ?Tab6{prop:Hide} = 0
                      ?Tab8{prop:Hide} = 0
                      ?tmp:EDIBatchNumber{prop:Hide} = 1
                      ?tmp:EDIBatchNumber:Prompt{prop:Hide} = 1
                  Else
                      ?Tab7{prop:Hide} = 0
                      ?Tab9{prop:Hide} = 0
                      ?Tab6{prop:Hide} = 0
                      ?Tab8{prop:Hide} = 0
                      ?tmp:EDIBatchNumber{prop:Hide} = 0
                      ?tmp:EDIBatchNumber:Prompt{prop:Hide} = 0
              End !Case tmp:JobType
      
              If tmp:CompletedType = 2
                  tmp:ReportOrder = 2
                  ?tmp:DateRangeType:Radio2{prop:Disable} = 1
                  ! Start Change 2223 BE(05/03/03)
                  !?tmp:ReportOrder{prop:From} = 'DATE BOOKED|JOB NUMBER|I.M.E.I. NUMBER|ACCOUNT NUMBER|MODEL NUMBER|STATUS|M.S.N.|SURNAME|ENGINEER|MOBILE NUMBER'
                  ?tmp:ReportOrder{prop:From} = 'DATE BOOKED|JOB NUMBER|I.M.E.I. NUMBER|ACCOUNT NUMBER|MODEL NUMBER|STATUS|M.S.N.|SURNAME|ENGINEER|MOBILE NUMBER|IN WORKSHOP'
                  ! End Change 2223 BE(05/03/03)
             Else !If tmp:CompltedType = 2
                  ?tmp:DateRangeType:Radio2{prop:Disable} = 0
                  ! Start Change 2223 BE(05/03/03)
                  !?tmp:ReportOrder{prop:From} = 'DATE BOOKED|DATE COMPLETED|JOB NUMBER|I.M.E.I. NUMBER|ACCOUNT NUMBER|MODEL NUMBER|STATUS|M.S.N.|SURNAME|ENGINEER|MOBILE NUMBER'
                  ?tmp:ReportOrder{prop:From} = 'DATE BOOKED|DATE COMPLETED|JOB NUMBER|I.M.E.I. NUMBER|ACCOUNT NUMBER|MODEL NUMBER|STATUS|M.S.N.|SURNAME|ENGINEER|MOBILE NUMBER|IN WORKSHOP'
                  ! End Change 2223 BE(05/03/03)
             End !If tmp:CompltedType = 2
      End !Case Choice(?CurrentTab)
      If DoNext# = 1
         Wizard13.TakeAccepted()
      End !If DoNext# = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VSNextButton, Accepted)
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      Case MessageEx('Do you want to save your criteria for future use, or run your report/export?','ServiceBase 2000',|
                     'Styles\question.ico','|&Save Criteria|&Save And Run|&Run|&Cancel',4,4,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Save Criteria Button
              Do SaveCriteria
              Post(Event:CloseWindow)
          Of 2 ! &Save And Run Button
              Do SaveCriteria
      
              Case tmp:OutputType
                  Of 0 !Report
                      Case tmp:PaperReportType
                          Of 1
                              Do Export
                          Else
                              Do StatusReport
                      End !Case tmp:PaperReportType
                  Of 1 !Export
                      Do Export
              End !Case tmp:OutputType
          Of 3 ! &Run Button
              Case tmp:OutputType
                  Of 0 !Report
                      Case tmp:PaperReportType
                          Of 1
                              Do Export
                          Else
                              Do StatusReport
                      End !Case tmp:PaperReportType
                  Of 1 !Export
                      Do Export
              End !Case tmp:OutputType
          Of 4 ! &Cancel Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Status_Report_Criteria')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::24:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::25:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:3
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:3{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:3{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::26:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:3)
               ?List:3{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:4
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:4{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:4{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::27:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:4)
               ?List:4{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:5
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:5{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:5{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::28:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:5)
               ?List:5{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:6
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:6{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:6{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::29:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:6)
               ?List:6{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:7
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:7{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:7{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::30:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:7)
               ?List:7{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:8
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:8{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:8{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::31:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:8)
               ?List:8{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:9
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:9{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:9{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::32:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:9)
               ?List:9{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:10
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:10{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:10{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::33:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:10)
               ?List:10{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:11
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:11{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:11{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::35:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:11)
               ?List:11{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:12
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:12{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:12{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::36:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:12)
               ?List:12{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:13
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:13{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:13{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::37:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:13)
               ?List:13{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:14
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:14{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:14{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::38:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:14)
               ?List:14{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:15
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:15{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:15{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::43:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:15)
               ?List:15{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
local.GetStatusDate           Procedure(String func:Status,*Date func:TheDate,*Time func:TheTime)
local:ReturnDate        Date()
local:ReturnTime        Time()
Code
    local:ReturnDate = 0
    local:ReturnTime = 0
    Found# = False
    ! Find and return the date of the specified status change - TrkBs: 5646 (DBH: 05-04-2005)
    Save_aus_ID = Access:AUDSTATS.SaveFile()
    Access:AUDSTATS.ClearKey(aus:NewStatusKey)
    aus:RefNumber = job:Ref_Number
    aus:Type      = 'JOB'
    Set(aus:NewStatusKey,aus:NewStatusKey)
    Loop
        If Access:AUDSTATS.NEXT()
           Break
        End !If
        If aus:RefNumber <> job:Ref_Number      |
        Or aus:Type      <> 'JOB'      |
            Then Break.  ! End If
        If Sub(aus:NewStatus,1,3) = func:Status
            local:ReturnDate = aus:DateChanged
            local:ReturnTime = aus:TimeChanged
            Found# = True
            ! Don't break out, incase there is more than one occurance of the status - TrkBs: 5646 (DBH: 05-04-2005)
        End ! If Sub(aus:NewStatus,1,3) = func:Status
    End !Loop
    Access:AUDSTATS.RestoreFile(Save_aus_ID)

    If Found# = True
        func:TheDate = local:ReturnDate
        func:TheTime = local:ReturnTime
        Return True
    Else
        Return False
    End ! If Found# = True





BRW4.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:HeadAccountTag = ''
    ELSE
      tmp:HeadAccountTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:HeadAccounttag = '*')
    SELF.Q.tmp:HeadAccountTag_Icon = 2
  ELSE
    SELF.Q.tmp:HeadAccountTag_Icon = 1
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW4.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW4::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW4::RecordStatus=ReturnValue
  IF BRW4::RecordStatus NOT=Record:OK THEN RETURN BRW4::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::24:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW4::RecordStatus
  RETURN ReturnValue


BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = sub:Account_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:SubAccountTag = ''
    ELSE
      tmp:SubAccountTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:SubAccounttag = '*')
    SELF.Q.tmp:SubAccountTag_Icon = 2
  ELSE
    SELF.Q.tmp:SubAccountTag_Icon = 1
  END


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = sub:Account_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::25:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  RETURN ReturnValue


BRW6.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue3.Pointer3 = cha:Charge_Type
     GET(glo:Queue3,glo:Queue3.Pointer3)
    IF ERRORCODE()
      tmp:CChargeTypeTag = ''
    ELSE
      tmp:CChargeTypeTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:CChargeTypetag = '*')
    SELF.Q.tmp:CChargeTypeTag_Icon = 2
  ELSE
    SELF.Q.tmp:CChargeTypeTag_Icon = 1
  END


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW6::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW6::RecordStatus=ReturnValue
  IF BRW6::RecordStatus NOT=Record:OK THEN RETURN BRW6::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue3.Pointer3 = cha:Charge_Type
     GET(glo:Queue3,glo:Queue3.Pointer3)
    EXECUTE DASBRW::26:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW6::RecordStatus
  RETURN ReturnValue


BRW7.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue4.Pointer4 = cha:Charge_Type
     GET(glo:Queue4,glo:Queue4.Pointer4)
    IF ERRORCODE()
      tmp:WChargeType = ''
    ELSE
      tmp:WChargeType = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:WChargeType = '*')
    SELF.Q.tmp:WChargeType_Icon = 2
  ELSE
    SELF.Q.tmp:WChargeType_Icon = 1
  END


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW7.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW7::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW7::RecordStatus=ReturnValue
  IF BRW7::RecordStatus NOT=Record:OK THEN RETURN BRW7::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue4.Pointer4 = cha:Charge_Type
     GET(glo:Queue4,glo:Queue4.Pointer4)
    EXECUTE DASBRW::27:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW7::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW7::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW7::RecordStatus
  RETURN ReturnValue


BRW8.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue5.Pointer5 = rtd:Repair_Type
     GET(glo:Queue5,glo:Queue5.Pointer5)
    IF ERRORCODE()
      tmp:CRepairTypeTag = ''
    ELSE
      tmp:CRepairTypeTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:CRepairTypetag = '*')
    SELF.Q.tmp:CRepairTypeTag_Icon = 2
  ELSE
    SELF.Q.tmp:CRepairTypeTag_Icon = 1
  END


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW8::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW8::RecordStatus=ReturnValue
  IF BRW8::RecordStatus NOT=Record:OK THEN RETURN BRW8::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue5.Pointer5 = rtd:Repair_Type
     GET(glo:Queue5,glo:Queue5.Pointer5)
    EXECUTE DASBRW::28:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW8::RecordStatus
  RETURN ReturnValue


BRW9.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue6.Pointer6 = rtd:Repair_Type
     GET(glo:Queue6,glo:Queue6.Pointer6)
    IF ERRORCODE()
      tmp:WRepairTypeTag = ''
    ELSE
      tmp:WRepairTypeTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:WRepairTypetag = '*')
    SELF.Q.tmp:WRepairTypeTag_Icon = 2
  ELSE
    SELF.Q.tmp:WRepairTypeTag_Icon = 1
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW9::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW9::RecordStatus=ReturnValue
  IF BRW9::RecordStatus NOT=Record:OK THEN RETURN BRW9::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue6.Pointer6 = rtd:Repair_Type
     GET(glo:Queue6,glo:Queue6.Pointer6)
    EXECUTE DASBRW::29:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW9::RecordStatus
  RETURN ReturnValue


BRW10.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF tmp:StatusType = 0
    RETURN SELF.SetSort(1,Force)
  ELSIF tmp:StatusType = 1
    RETURN SELF.SetSort(2,Force)
  ELSIF tmp:StatusType = 2
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW10.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue7.Pointer7 = sts:Status
     GET(glo:Queue7,glo:Queue7.Pointer7)
    IF ERRORCODE()
      tmp:StatusTag = ''
    ELSE
      tmp:StatusTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Statustag = '*')
    SELF.Q.tmp:StatusTag_Icon = 2
  ELSE
    SELF.Q.tmp:StatusTag_Icon = 1
  END


BRW10.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW10::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW10::RecordStatus=ReturnValue
  IF BRW10::RecordStatus NOT=Record:OK THEN RETURN BRW10::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue7.Pointer7 = sts:Status
     GET(glo:Queue7,glo:Queue7.Pointer7)
    EXECUTE DASBRW::30:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW10::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW10::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW10::RecordStatus
  RETURN ReturnValue


BRW14.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue8.Pointer8 = loi:Location
     GET(glo:Queue8,glo:Queue8.Pointer8)
    IF ERRORCODE()
      tmp:LocationTag = ''
    ELSE
      tmp:LocationTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Locationtag = '*')
    SELF.Q.tmp:LocationTag_Icon = 2
  ELSE
    SELF.Q.tmp:LocationTag_Icon = 1
  END


BRW14.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW14.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW14::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW14::RecordStatus=ReturnValue
  IF BRW14::RecordStatus NOT=Record:OK THEN RETURN BRW14::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue8.Pointer8 = loi:Location
     GET(glo:Queue8,glo:Queue8.Pointer8)
    EXECUTE DASBRW::31:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW14::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW14::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW14::RecordStatus
  RETURN ReturnValue


BRW15.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue9.Pointer9 = use:User_Code
     GET(glo:Queue9,glo:Queue9.Pointer9)
    IF ERRORCODE()
      tmp:UserTag = ''
    ELSE
      tmp:UserTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Usertag = '*')
    SELF.Q.tmp:UserTag_Icon = 2
  ELSE
    SELF.Q.tmp:UserTag_Icon = 1
  END


BRW15.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW15.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW15::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW15::RecordStatus=ReturnValue
  IF BRW15::RecordStatus NOT=Record:OK THEN RETURN BRW15::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue9.Pointer9 = use:User_Code
     GET(glo:Queue9,glo:Queue9.Pointer9)
    EXECUTE DASBRW::32:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW15::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW15::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW15::RecordStatus
  RETURN ReturnValue


BRW16.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue10.Pointer10 = man:Manufacturer
     GET(glo:Queue10,glo:Queue10.Pointer10)
    IF ERRORCODE()
      tmp:ManufacturerTag = ''
    ELSE
      tmp:ManufacturerTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Manufacturertag = '*')
    SELF.Q.tmp:ManufacturerTag_Icon = 2
  ELSE
    SELF.Q.tmp:ManufacturerTag_Icon = 1
  END


BRW16.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW16.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW16::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW16::RecordStatus=ReturnValue
  IF BRW16::RecordStatus NOT=Record:OK THEN RETURN BRW16::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue10.Pointer10 = man:Manufacturer
     GET(glo:Queue10,glo:Queue10.Pointer10)
    EXECUTE DASBRW::33:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW16::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW16::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW16::RecordStatus
  RETURN ReturnValue


BRW17.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF tmp:SelectManufacturer = 0
    RETURN SELF.SetSort(1,Force)
  ELSIF tmp:SelectManufacturer = 1
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW17.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue11.Pointer11 = mod:Model_Number
     GET(glo:Queue11,glo:Queue11.Pointer11)
    IF ERRORCODE()
      tmp:ModelNumberTag = ''
    ELSE
      tmp:ModelNumberTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:ModelNumbertag = '*')
    SELF.Q.tmp:ModelNumberTag_Icon = 2
  ELSE
    SELF.Q.tmp:ModelNumberTag_Icon = 1
  END


BRW17.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW17.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW17::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW17::RecordStatus=ReturnValue
  IF BRW17::RecordStatus NOT=Record:OK THEN RETURN BRW17::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue11.Pointer11 = mod:Model_Number
     GET(glo:Queue11,glo:Queue11.Pointer11)
    EXECUTE DASBRW::35:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW17::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW17::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW17::RecordStatus
  RETURN ReturnValue


BRW21.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue12.Pointer12 = uni:Unit_Type
     GET(glo:Queue12,glo:Queue12.Pointer12)
    IF ERRORCODE()
      tmp:UnitTypeTag = ''
    ELSE
      tmp:UnitTypeTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:UnitTypetag = '*')
    SELF.Q.tmp:UnitTypeTag_Icon = 2
  ELSE
    SELF.Q.tmp:UnitTypeTag_Icon = 1
  END


BRW21.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW21.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW21::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW21::RecordStatus=ReturnValue
  IF BRW21::RecordStatus NOT=Record:OK THEN RETURN BRW21::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue12.Pointer12 = uni:Unit_Type
     GET(glo:Queue12,glo:Queue12.Pointer12)
    EXECUTE DASBRW::36:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW21::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW21::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW21::RecordStatus
  RETURN ReturnValue


BRW22.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue13.Pointer13 = trt:Transit_Type
     GET(glo:Queue13,glo:Queue13.Pointer13)
    IF ERRORCODE()
      tmp:TransitTypeTag = ''
    ELSE
      tmp:TransitTypeTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:TransitTypetag = '*')
    SELF.Q.tmp:TransitTypeTag_Icon = 2
  ELSE
    SELF.Q.tmp:TransitTypeTag_Icon = 1
  END


BRW22.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW22.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW22::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW22::RecordStatus=ReturnValue
  IF BRW22::RecordStatus NOT=Record:OK THEN RETURN BRW22::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue13.Pointer13 = trt:Transit_Type
     GET(glo:Queue13,glo:Queue13.Pointer13)
    EXECUTE DASBRW::37:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW22::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW22::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW22::RecordStatus
  RETURN ReturnValue


BRW23.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue14.Pointer14 = tur:Turnaround_Time
     GET(glo:Queue14,glo:Queue14.Pointer14)
    IF ERRORCODE()
      tmp:TurnaroundTimeTag = ''
    ELSE
      tmp:TurnaroundTimeTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:TurnaroundTimetag = '*')
    SELF.Q.tmp:TurnaroundTimeTag_Icon = 2
  ELSE
    SELF.Q.tmp:TurnaroundTimeTag_Icon = 1
  END


BRW23.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW23.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW23::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW23::RecordStatus=ReturnValue
  IF BRW23::RecordStatus NOT=Record:OK THEN RETURN BRW23::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue14.Pointer14 = tur:Turnaround_Time
     GET(glo:Queue14,glo:Queue14.Pointer14)
    EXECUTE DASBRW::38:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW23::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW23::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW23::RecordStatus
  RETURN ReturnValue


BRW42.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue15.Pointer15 = col:Colour
     GET(glo:Queue15,glo:Queue15.Pointer15)
    IF ERRORCODE()
      tmp:ColourTag = ''
    ELSE
      tmp:ColourTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Colourtag = '*')
    SELF.Q.tmp:ColourTag_Icon = 2
  ELSE
    SELF.Q.tmp:ColourTag_Icon = 1
  END


BRW42.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW42.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW42::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW42::RecordStatus=ReturnValue
  IF BRW42::RecordStatus NOT=Record:OK THEN RETURN BRW42::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue15.Pointer15 = col:Colour
     GET(glo:Queue15,glo:Queue15.Pointer15)
    EXECUTE DASBRW::43:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW42::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW42::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW42::RecordStatus
  RETURN ReturnValue

Wizard13.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW4.Q &= NULL) ! Has Browse Object been initialized?
       BRW4.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW5.Q &= NULL) ! Has Browse Object been initialized?
       BRW5.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW6.Q &= NULL) ! Has Browse Object been initialized?
       BRW6.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW7.Q &= NULL) ! Has Browse Object been initialized?
       BRW7.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW8.Q &= NULL) ! Has Browse Object been initialized?
       BRW8.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW9.Q &= NULL) ! Has Browse Object been initialized?
       BRW9.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW10.Q &= NULL) ! Has Browse Object been initialized?
       BRW10.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW14.Q &= NULL) ! Has Browse Object been initialized?
       BRW14.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW15.Q &= NULL) ! Has Browse Object been initialized?
       BRW15.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW16.Q &= NULL) ! Has Browse Object been initialized?
       BRW16.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW17.Q &= NULL) ! Has Browse Object been initialized?
       BRW17.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW21.Q &= NULL) ! Has Browse Object been initialized?
       BRW21.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW22.Q &= NULL) ! Has Browse Object been initialized?
       BRW22.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW23.Q &= NULL) ! Has Browse Object been initialized?
       BRW23.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW42.Q &= NULL) ! Has Browse Object been initialized?
       BRW42.ResetQueue(Reset:Queue)
    END

Wizard13.TakeBackEmbed PROCEDURE
   CODE

Wizard13.TakeNextEmbed PROCEDURE
   CODE

Wizard13.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
