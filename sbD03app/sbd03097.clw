

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03097.INC'),ONCE        !Local module procedure declarations
                     END


SIDExtract PROCEDURE (Byte f:Mode)                    !Generated from procedure template - Window

tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:ExportFile       STRING(255),STATIC
tmp:SavePath         CSTRING(255)
tmp:Owner            STRING(255),STATIC
tmp:HandsetOwner     STRING(255),STATIC
tmp:StatusOwner      STRING(255),STATIC
tmp:LocationOwner    STRING(255),STATIC
tmp:CPMOwner         STRING(255),STATIC
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:WriteSQL         BYTE
tmp:InsertSQL        BYTE
tmp:MSSQLConnection  STRING(255),STATIC
window               WINDOW('SID Extract Criteria'),AT(,,220,82),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,2,212,52),USE(?Sheet1),SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           PROMPT('Start Date'),AT(8,20),USE(?tmp:StartDate:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@d6b),AT(84,20,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(152,20,10,10),USE(?PopCalendar),ICON('calenda2.ico')
                           PROMPT('End Date'),AT(8,36),USE(?tmp:EndDate:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@d6b),AT(84,36,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(152,36,10,10),USE(?PopCalendar:2),ICON('calenda2.ico')
                         END
                       END
                       BUTTON('Cancel'),AT(156,60,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       BUTTON('OK'),AT(96,60,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       PANEL,AT(4,56,212,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

Prog        Class
ProgressSetup      Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(),Byte
ProgressText       Procedure(String func:String)
ProgressFinish     Procedure()
            End
! moving bar window
prog:RecordsToProcess   Long,Auto
prog:RecordsPerCycle    Long,Auto
prog:RecordsProcessed   Long,Auto
prog:RecordsThisCycle   Long,Auto
prog:PercentProgress    Byte
prog:Cancel             Byte
prog:ProgressThermometer    Byte


prog:thermometer byte
prog:SkipRecords        Long
omit('***',ClarionetUsed=0)
CNprog:thermometer byte
CNprog:Text         String(60)
CNprog:pctText      String(60)
***z
progwindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(prog:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?prog:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?prog:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),LEFT,ICON('cancel.gif')
     END

omit('***',ClarionetUsed=0)

!static webjob window
CN:progwindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(cnprog:Text),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?cnprog:Prompt),FONT(,14,,FONT:bold)
     END
***
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
ExportFile  File,Driver('ASCII'),Pre(expfil),Name(tmp:ExportFile),Create,Bindable,Thread
Record          Record
Line                String(10000)      ! Increased from 5000 chars (was getting cut off)
                End ! Record
            End ! File

tmp:TempFilePath    CString(255)
!SQLFile File,Driver('Scalable'),OEM,Owner(tmp:Owner),Pre(sql),Name('WebJob'),Bindable,Thread
!Record          Record
!SBJobNo             Long(), Name('SBJobNo')
!CollectionMadeToday Byte(),Name('CollectionMadeToday')
!BookingType         String(1),Name('BookingType')
!Ref_Number          Long(), Name('Ref_Number')
!IMEINumber          String(30), Name('ESN')
!Manufacturer        String(30), Name('Manufacturer')
!ModelNumber         String(30), Name('Model_Number')
!Colour              String(30), Name('Colour')
!ContractPeriod      Long(), Name('ContractPeriod')
!PrimaryFault        String(255), Name('PrimaryFault')
!RepairType          String(2), Name('RepairType')
!InsuranceRefNo      String(30), Name('InsuranceRefNo')
!Reason              String(255), Name('Reason')
!IntermittentFault   Byte(),Name('Intermittent_Fault')
!FreeTextFault       String(255), Name('FreeTextFault')
!CPAllocated         String(1),Name('CPAllocated')
!CPIssueReason       String(30),Name('CPIssueReason')
!DepositValue        PDECIMAL(8,2), NAME('CPDepositValue')
!                End ! Record
!            End ! File

SQLFile             FILE,DRIVER('Scalable'),OEM,OWNER(tmp:Owner),NAME('WEBJOB'),PRE(SQL),BINDABLE,THREAD
Record                  RECORD, PRE()
Ref_Number          Long(), Name('Ref_Number')
IMEINumber          String(30), Name('ESN')
Manufacturer        String(30), Name('Manufacturer')
ModelNumber         String(30), Name('Model_Number')
Colour              String(30), Name('Colour')
PrimaryFault        String(255), Name('PrimaryFault')
FreeTextFault       String(255), Name('FreeTextFault')
IntermittentFault   Byte(),Name('Intermittent_Fault')
RepairType          String(2), Name('RepairType')
InsuranceRefNo      String(30), Name('InsuranceRefNo')
SBJobNo             Long(), Name('SBJobNo')
ContractPeriod      Long(), Name('ContractPeriod')
Reason              String(255), Name('Reason')
BookingType         String(1),Name('BookingType')
CollectionMadeToday Byte(),Name('CollectionMadeToday')
CPIssueReason       String(30),Name('CPIssueReason')
CPAllocated         String(1),Name('CPAllocated')
DepositValue        PDecimal(8,2),Name('CPDepositValue')
Battery             Byte(),Name('Battery')
BatterCover         Byte(),Name('BatteryCover')
Charger             Byte(),Name('Charger')
MemoryCard          Byte(),Name('MemoryCard')
CPIMEI              String(20),Name('CPIMEI')
ValueSegment        String(20),Name('ValueSegment')
Phone_Lock          String(30),Name('Phone_Lock')
SMSReq              Long(),Name('SMSReq')
EmailReq            Byte(),Name('EmailReq')
SMSAlertNo          String(15),Name('SMSAlertNo')
                        END
                    END

WebJobExFile File,Driver('Scalable'),OEM,Owner(tmp:Owner),Pre(webex),Name('WebJobEx'),Bindable,Thread
Record      Record
CPChargeValue       PDecimal(8,2),Name('CPChargeValue')
CPChargeSKUCode     CString(31),Name('CPChargeSKUCode')
CPExchanged         Byte(),Name('CPExchanged')
CPExChargeValue     PDecimal(8,2),Name('CPExChargeValue')
CPExChargeSKUCode   CString(31),Name('CPExChargeSKUCode')
CancelReason        CString(81),Name('CancelReason')
InterTimeFrame      CString(31),Name('InterTimeFrame')
ServiceCharge       PDecimal(8,2),Name('ServiceCharge')
BERCharge           PDecimal(8,2),Name('BERCharge')
StartDate           Date,Name('StartDate')
StartTime           Time,Name('StartTime')
DiagAvailable       Byte(),Name('DiagAvailable')
KnownIssueID        Long(), Name('KnownIssueID')
KIResolutionCode    CString(7), Name('KIResolutionCode')
KIResolutionName    CString(51), Name('KIResolutionName')
DiagResolutionCode  CString(7), Name('DiagResolutionCode')
DiagResolutionName  CString(51), Name('DiagResolutionName')
ManualCTNEntry      Byte(),Name('ManualCTNEntry')
ManualIMEIEntry     Byte(),Name('ManualIMEIEntry')
RepairAdminCharge   PDecimal(8,2),Name('RepairAdminCharge')
SoftwareUpToDate    CString(2), Name('SoftwareUpToDate')
COMRef              CString(51), Name('COMRef')
NoExceptionDate     Date,Name('NoExceptionDate')
DiagFieldName       CString(51), Name('DiagFieldName')
DiagFieldText       CString(251), Name('DiagFieldText')
OriginalValueSegment String(20), Name('OriginalValueSegment')
CTNUserName          CString(31), Name('CTNUserName')
CTNEscalationReason  CString(41), Name('CTNEscalationReason')
SIDVersion          CString(2), Name('SIDVersion')
            End
        End

CCExchangeFile File,Driver('Scalable'),OEM,Owner(tmp:Owner),Pre(ccex),Name('CCExchange'),Bindable,Thread
Record      Record
TimeSlotID        Long(),Name('TimeSlotID')
ExchangeDate      Date,Name('ExchangeDate')
DeliveryCharge    PDecimal(8,2),Name('DeliveryCharge')
            End
        End

WebRetFile File,Driver('Scalable'),OEM,Owner(tmp:Owner),Pre(webr),Name('WebRet'),Bindable,Thread
Record      Record
RetReasonCode       CString(7), Name('RetReasonCode')
RetReasonName       CString(51), Name('RetReasonName')
ExceptionCode       CString(7), Name('ExceptionCode')
ManagersName        CString(31), Name('ManagersName')
ManagersID          CString(31), Name('ManagersID')
ReturnDescription   CString(255), Name('ReturnDescription')
ReturnException     Byte(),Name('ReturnException')
OtherRetOrigin      CString(51), Name('OtherRetOrigin')
            End
        End

JobTxtFile File,Driver('Scalable'),OEM,Owner(tmp:Owner),Pre(jobt),Name('JobTxt'),Bindable,Thread
Record      Record
Text1           CString(255),Name('Text1')
Text2           CString(255),Name('Text2')
            End
        End

ReturnCodesFile File,Driver('Scalable'),OEM,Owner(tmp:Owner),Pre(retc),Name('ReturnCodes'),Bindable,Thread
Record      Record
UserName      CString(31), Name('UserName')
            End
        End

TimeSlotFile File,Driver('Scalable'),OEM,Owner(tmp:Owner),Pre(tmsl),Name('TimeSlot'),Bindable,Thread
Record          Record
TimeSlotName        CString(31), name('TimeSlotName')
                End
             End

CTNSegmentFile File,Driver('Scalable'),OEM,Owner(tmp:Owner),Pre(ctseg),Name('CTNSegment'),Bindable,Thread
Record          Record
OriginalSalesAgent CString(11), name('OriginalSalesAgent')
                End
             End

HandsetFile File,Driver('Scalable'),OEM,Owner(tmp:CPMOwner),Pre(shan),Name('Handset'),Bindable,Thread
Record      Record
HandsetID       Long,Name('HandsetID')
IMEINumber      String(30),Name('IMEI')
Manufacturer    String(30),Name('MakeName')
ModelNumber     String(30),Name('ModelName')
ReturnDate      Date,Name('ReturnDate')
            End
        End

HistoryFile    File,Driver('Scalable'),OEM,Owner(tmp:CPMOwner),Pre(hist),Name('History'),Bindable,Thread
Record              Record
EntryDate               Date(),Name('EntryDate')
                    End
               End

OscarExtFile   File,Driver('Scalable'),OEM,Owner(tmp:CPMOwner),Pre(osce),Name('OscarExt'),Bindable,Thread
Record              Record
CPChargeSKU             CString(23),Name('CPChargeSKU')
CPChargeAmt             PDecimal(8,2),Name('CPChargeAmt')
                    End
               End
SBJobs               FILE,DRIVER('MSSQL', '/TRUSTEDCONNECTION=FALSE'),OWNER(tmp:MSSQLConnection),NAME('dbo.SBJobs'),PRE(SBJ),BINDABLE,THREAD
PK_SBJobs                KEY(SBJ:ServiceBaseJobNo),NAME('PK_SBJobs'),PRIMARY
IdxSIDJobNo              KEY(SBJ:SIDJobNo),DUP,NAME('IdxSIDJobNo')
Record                   RECORD,PRE()
ServiceBaseJobNo            LONG
SIDJobNo                    LONG
BookedDateTime              STRING(8)
BookedDateTime_GROUP        GROUP,OVER(BookedDateTime)
BookedDateTime_DATE           DATE
BookedDateTime_TIME           TIME
                            END
BookingType                 CSTRING(2)
RetailToteCollected         CSTRING(4)
BookingAccount              CSTRING(16)
BookingAgent                CSTRING(31)
Title                       CSTRING(5)
Initial                     CSTRING(2)
Surname                     CSTRING(31)
CTN                         CSTRING(16)
OriginalIMEI                CSTRING(21)
Manufacturer                CSTRING(31)
ModelNo                     CSTRING(31)
Colour                      CSTRING(31)
HandsetType                 CSTRING(2)
ContractLength              LONG
ReceivedIMEI                CSTRING(21)
ChargeType                  CSTRING(41)
InsuranceRefNo              CSTRING(31)
OutOfWarrantyNotChargeableReason CSTRING(256)
DOP                         STRING(8)
DOP_GROUP                   GROUP,OVER(DOP)
DOP_DATE                      DATE
DOP_TIME                      TIME
                            END
CustHouseNumberName         CSTRING(31)
CustAddressLine1            CSTRING(31)
CustAddressLine2            CSTRING(31)
CustAddressLine3            CSTRING(31)
CustPostcode                CSTRING(31)
CustTelNo                   CSTRING(16)
CustEmailAddress            CSTRING(256)
CollHouseNumberName         CSTRING(31)
CollAddressLine1            CSTRING(31)
CollAddressLine2            CSTRING(31)
CollAddressLine3            CSTRING(31)
CollPostcode                CSTRING(31)
CollTelNo                   CSTRING(16)
DelHouseNumberName          CSTRING(31)
DelAddressLine1             CSTRING(31)
DelAddressLine2             CSTRING(31)
DelAddressLine3             CSTRING(31)
DelPostcode                 CSTRING(31)
DelTelNo                    CSTRING(16)
ExchangeIMEI                CSTRING(21)
OutgoingConsignmentNo       CSTRING(21)
PrimaryFault                CSTRING(256)
IntermittentFault           CSTRING(4)
FreeTextFault               CSTRING(256) ! 501 - TrackerBase 11065 GK 07/09/2009
VFResolutionCode            CSTRING(6)
IMEIBouncer                 CSTRING(31)
CustCollectionDate          STRING(8)
CustCollectionDate_GROUP    GROUP,OVER(CustCollectionDate)
CustCollectionDate_DATE       DATE
CustCollectionDate_TIME       TIME
                            END
DespatchDueFromSCDate       STRING(8)
DespatchDueFromSCDate_GROUP GROUP,OVER(DespatchDueFromSCDate)
DespatchDueFromSCDate_DATE    DATE
DespatchDueFromSCDate_TIME    TIME
                            END
StatusChangeTo111DateTime   STRING(8)
StatusChangeTo111DateTime_GROUP GROUP,OVER(StatusChangeTo111DateTime)
StatusChangeTo111DateTime_DATE DATE
StatusChangeTo111DateTime_TIME TIME
                            END
StatusChangeTo125DateTime   STRING(8)
StatusChangeTo125DateTime_GROUP GROUP,OVER(StatusChangeTo125DateTime)
StatusChangeTo125DateTime_DATE DATE
StatusChangeTo125DateTime_TIME TIME
                            END
StatusChangeTo135DateTime   STRING(8)
StatusChangeTo135DateTime_GROUP GROUP,OVER(StatusChangeTo135DateTime)
StatusChangeTo135DateTime_DATE DATE
StatusChangeTo135DateTime_TIME TIME
                            END
StatusChangeTo136DateTime   STRING(8)
StatusChangeTo136DateTime_GROUP GROUP,OVER(StatusChangeTo136DateTime)
StatusChangeTo136DateTime_DATE DATE
StatusChangeTo136DateTime_TIME TIME
                            END
StatusChangeTo140DateTime   STRING(8)
StatusChangeTo140DateTime_GROUP GROUP,OVER(StatusChangeTo140DateTime)
StatusChangeTo140DateTime_DATE DATE
StatusChangeTo140DateTime_TIME TIME
                            END
NumOfTimesEnvelopesSent     LONG
InitialDateSent             STRING(8)
InitialDateSent_GROUP       GROUP,OVER(InitialDateSent)
InitialDateSent_DATE          DATE
InitialDateSent_TIME          TIME
                            END
LastDateSent                STRING(8)
LastDateSent_GROUP          GROUP,OVER(LastDateSent)
LastDateSent_DATE             DATE
LastDateSent_TIME             TIME
                            END
StatusChangeTo145DateTime   STRING(8)
StatusChangeTo145DateTime_GROUP GROUP,OVER(StatusChangeTo145DateTime)
StatusChangeTo145DateTime_DATE DATE
StatusChangeTo145DateTime_TIME TIME
                            END
InitialDateRequested        STRING(8)
InitialDateRequested_GROUP  GROUP,OVER(InitialDateRequested)
InitialDateRequested_DATE     DATE
InitialDateRequested_TIME     TIME
                            END
LastDateRequested           STRING(8)
LastDateRequested_GROUP     GROUP,OVER(LastDateRequested)
LastDateRequested_DATE        DATE
LastDateRequested_TIME        TIME
                            END
StatusChangeTo305DateTime   STRING(8)
StatusChangeTo305DateTime_GROUP GROUP,OVER(StatusChangeTo305DateTime)
StatusChangeTo305DateTime_DATE DATE
StatusChangeTo305DateTime_TIME TIME
                            END
StatusChangeTo315DateTime   STRING(8)
StatusChangeTo315DateTime_GROUP GROUP,OVER(StatusChangeTo315DateTime)
StatusChangeTo315DateTime_DATE DATE
StatusChangeTo315DateTime_TIME TIME
                            END
StatusChangeTo350DateTime   STRING(8)
StatusChangeTo350DateTime_GROUP GROUP,OVER(StatusChangeTo350DateTime)
StatusChangeTo350DateTime_DATE DATE
StatusChangeTo350DateTime_TIME TIME
                            END
StatusChangeTo351DateTime   STRING(8)
StatusChangeTo351DateTime_GROUP GROUP,OVER(StatusChangeTo351DateTime)
StatusChangeTo351DateTime_DATE DATE
StatusChangeTo351DateTime_TIME TIME
                            END
StatusChangeTo352DateTime   STRING(8)
StatusChangeTo352DateTime_GROUP GROUP,OVER(StatusChangeTo352DateTime)
StatusChangeTo352DateTime_DATE DATE
StatusChangeTo352DateTime_TIME TIME
                            END
StatusChangeTo355DateTime   STRING(8)
StatusChangeTo355DateTime_GROUP GROUP,OVER(StatusChangeTo355DateTime)
StatusChangeTo355DateTime_DATE DATE
StatusChangeTo355DateTime_TIME TIME
                            END
StatusChangeTo356DateTime   STRING(8)
StatusChangeTo356DateTime_GROUP GROUP,OVER(StatusChangeTo356DateTime)
StatusChangeTo356DateTime_DATE DATE
StatusChangeTo356DateTime_TIME TIME
                            END
StatusChangeTo357DateTime   STRING(8)
StatusChangeTo357DateTime_GROUP GROUP,OVER(StatusChangeTo357DateTime)
StatusChangeTo357DateTime_DATE DATE
StatusChangeTo357DateTime_TIME TIME
                            END
StatusChangeTo358DateTime   STRING(8)
StatusChangeTo358DateTime_GROUP GROUP,OVER(StatusChangeTo358DateTime)
StatusChangeTo358DateTime_DATE DATE
StatusChangeTo358DateTime_TIME TIME
                            END
StatusChangeTo360DateTime   STRING(8)
StatusChangeTo360DateTime_GROUP GROUP,OVER(StatusChangeTo360DateTime)
StatusChangeTo360DateTime_DATE DATE
StatusChangeTo360DateTime_TIME TIME
                            END
StatusChangeTo505DateTime   STRING(8)
StatusChangeTo505DateTime_GROUP GROUP,OVER(StatusChangeTo505DateTime)
StatusChangeTo505DateTime_DATE DATE
StatusChangeTo505DateTime_TIME TIME
                            END
StatusChangeTo520DateTime   STRING(8)
StatusChangeTo520DateTime_GROUP GROUP,OVER(StatusChangeTo520DateTime)
StatusChangeTo520DateTime_DATE DATE
StatusChangeTo520DateTime_TIME TIME
                            END
StatusChangeTo525DateTime   STRING(8)
StatusChangeTo525DateTime_GROUP GROUP,OVER(StatusChangeTo525DateTime)
StatusChangeTo525DateTime_DATE DATE
StatusChangeTo525DateTime_TIME TIME
                            END
StatusChangeTo530DateTime   STRING(8)
StatusChangeTo530DateTime_GROUP GROUP,OVER(StatusChangeTo530DateTime)
StatusChangeTo530DateTime_DATE DATE
StatusChangeTo530DateTime_TIME TIME
                            END
StatusChangeTo535DateTime   STRING(8)
StatusChangeTo535DateTime_GROUP GROUP,OVER(StatusChangeTo535DateTime)
StatusChangeTo535DateTime_DATE DATE
StatusChangeTo535DateTime_TIME TIME
                            END
StatusChangeTo540DateTime   STRING(8)
StatusChangeTo540DateTime_GROUP GROUP,OVER(StatusChangeTo540DateTime)
StatusChangeTo540DateTime_DATE DATE
StatusChangeTo540DateTime_TIME TIME
                            END
StatusChangeTo705DateTime   STRING(8)
StatusChangeTo705DateTime_GROUP GROUP,OVER(StatusChangeTo705DateTime)
StatusChangeTo705DateTime_DATE DATE
StatusChangeTo705DateTime_TIME TIME
                            END
StatusChangeTo797DateTime   STRING(8)
StatusChangeTo797DateTime_GROUP GROUP,OVER(StatusChangeTo797DateTime)
StatusChangeTo797DateTime_DATE DATE
StatusChangeTo797DateTime_TIME TIME
                            END
StatusChangeTo798DateTime   STRING(8)
StatusChangeTo798DateTime_GROUP GROUP,OVER(StatusChangeTo798DateTime)
StatusChangeTo798DateTime_DATE DATE
StatusChangeTo798DateTime_TIME TIME
                            END
StatusChangeTo799DateTime   STRING(8)
StatusChangeTo799DateTime_GROUP GROUP,OVER(StatusChangeTo799DateTime)
StatusChangeTo799DateTime_DATE DATE
StatusChangeTo799DateTime_TIME TIME
                            END
StatusChangeTo901DateTime   STRING(8)
StatusChangeTo901DateTime_GROUP GROUP,OVER(StatusChangeTo901DateTime)
StatusChangeTo901DateTime_DATE DATE
StatusChangeTo901DateTime_TIME TIME
                            END
StatusChangeTo930DateTime   STRING(8)
StatusChangeTo930DateTime_GROUP GROUP,OVER(StatusChangeTo930DateTime)
StatusChangeTo930DateTime_DATE DATE
StatusChangeTo930DateTime_TIME TIME
                            END
StatusChangeTo940DateTime   STRING(8)
StatusChangeTo940DateTime_GROUP GROUP,OVER(StatusChangeTo940DateTime)
StatusChangeTo940DateTime_DATE DATE
StatusChangeTo940DateTime_TIME TIME
                            END
CurrentStatus               CSTRING(31)
IncomingConsignmentNo       CSTRING(31)
Battery                     CSTRING(2)
BatteryCover                CSTRING(2)
Charger                     CSTRING(2)
MemoryCard                  CSTRING(2)
CPAllocated                 CSTRING(2)
CPIssueDate                 STRING(8)
CPIssueDate_GROUP           GROUP,OVER(CPIssueDate)
CPIssueDate_DATE              DATE
CPIssueDate_TIME              TIME
                            END
CPNonIssueReason            CSTRING(31)
CPIMEI                      CSTRING(21)
CPManufacturer              CSTRING(31)
CPModelNo                   CSTRING(31)
StockOut                    CSTRING(2)
ExpectedReturnDate          STRING(8)
ExpectedReturnDate_GROUP    GROUP,OVER(ExpectedReturnDate)
ExpectedReturnDate_DATE       DATE
ExpectedReturnDate_TIME       TIME
                            END
DepositValue                DECIMAL(9,2)
SIDCPCharge                 DECIMAL(9,2)
OscarCPCharge               DECIMAL(9,2)
SIDSKUCode                  CSTRING(31)
OscarSKUCode                CSTRING(31)
ValueSegment                CSTRING(21)
ExchangeDate                STRING(8)
ExchangeDate_GROUP          GROUP,OVER(ExchangeDate)
ExchangeDate_DATE             DATE
ExchangeDate_TIME             TIME
                            END
ExchangeDeliveryType        CSTRING(31)
InterTimeFrame              CSTRING(31)
PhoneLock                   CSTRING(31)
ServiceCharge               DECIMAL(9,2)
DeliveryCharge              DECIMAL(9,2)
BERCharge                   DECIMAL(9,2)
CancelReason                CSTRING(81)
StatusChangeTo796DateTime   STRING(8)
StatusChangeTo796DateTime_GROUP GROUP,OVER(StatusChangeTo796DateTime)
StatusChangeTo796DateTime_DATE DATE
StatusChangeTo796DateTime_TIME TIME
                            END
StatusChangeTo794DateTime   STRING(8)
StatusChangeTo794DateTime_GROUP GROUP,OVER(StatusChangeTo794DateTime)
StatusChangeTo794DateTime_DATE DATE
StatusChangeTo794DateTime_TIME TIME
                            END
StatusChangeTo102DateTime   STRING(8)
StatusChangeTo102DateTime_GROUP GROUP,OVER(StatusChangeTo102DateTime)
StatusChangeTo102DateTime_DATE DATE
StatusChangeTo102DateTime_TIME TIME
                            END
StatusChangeTo104DateTime   STRING(8)
StatusChangeTo104DateTime_GROUP GROUP,OVER(StatusChangeTo104DateTime)
StatusChangeTo104DateTime_DATE DATE
StatusChangeTo104DateTime_TIME TIME
                            END
StatusChangeTo795DateTime   STRING(8)
StatusChangeTo795DateTime_GROUP GROUP,OVER(StatusChangeTo795DateTime)
StatusChangeTo795DateTime_DATE DATE
StatusChangeTo795DateTime_TIME TIME
                            END
StatusChangeTo112DateTime   STRING(8)
StatusChangeTo112DateTime_GROUP GROUP,OVER(StatusChangeTo112DateTime)
StatusChangeTo112DateTime_DATE DATE
StatusChangeTo112DateTime_TIME TIME
                            END
StatusChangeTo113DateTime   STRING(8)
StatusChangeTo113DateTime_GROUP GROUP,OVER(StatusChangeTo113DateTime)
StatusChangeTo113DateTime_DATE DATE
StatusChangeTo113DateTime_TIME TIME
                            END
StatusChangeTo931DateTime   STRING(8)
StatusChangeTo931DateTime_GROUP GROUP,OVER(StatusChangeTo931DateTime)
StatusChangeTo931DateTime_DATE DATE
StatusChangeTo931DateTime_TIME TIME
                            END
StatusChangeTo941DateTime   STRING(8)
StatusChangeTo941DateTime_GROUP GROUP,OVER(StatusChangeTo941DateTime)
StatusChangeTo941DateTime_DATE DATE
StatusChangeTo941DateTime_TIME TIME
                            END
StatusChangeTo101DateTime   STRING(8)
StatusChangeTo101DateTime_GROUP GROUP,OVER(StatusChangeTo101DateTime)
StatusChangeTo101DateTime_DATE DATE
StatusChangeTo101DateTime_TIME TIME
                            END
NewInsuranceCustomer        CSTRING(2)
StatusChangeTo114DateTime   STRING(8)
StatusChangeTo114DateTime_GROUP GROUP,OVER(StatusChangeTo114DateTime)
StatusChangeTo114DateTime_DATE DATE
StatusChangeTo114DateTime_TIME TIME
                            END
DiagnosisAvailable          CSTRING(2)
KIReference                 CSTRING(11)
StatusChangeTo250DateTime   STRING(8)
StatusChangeTo250DateTime_GROUP GROUP,OVER(StatusChangeTo250DateTime)
StatusChangeTo250DateTime_DATE DATE
StatusChangeTo250DateTime_TIME TIME
                            END
StatusChangeTo251DateTime   STRING(8)
StatusChangeTo251DateTime_GROUP GROUP,OVER(StatusChangeTo251DateTime)
StatusChangeTo251DateTime_DATE DATE
StatusChangeTo251DateTime_TIME TIME
                            END
StatusChangeTo252DateTime   STRING(8)
StatusChangeTo252DateTime_GROUP GROUP,OVER(StatusChangeTo252DateTime)
StatusChangeTo252DateTime_DATE DATE
StatusChangeTo252DateTime_TIME TIME
                            END
StatusChangeTo248DateTime   STRING(8)
StatusChangeTo248DateTime_GROUP GROUP,OVER(StatusChangeTo248DateTime)
StatusChangeTo248DateTime_DATE DATE
StatusChangeTo248DateTime_TIME TIME
                            END
StatusChangeTo106DateTime   STRING(8)
StatusChangeTo106DateTime_GROUP GROUP,OVER(StatusChangeTo106DateTime)
StatusChangeTo106DateTime_DATE DATE
StatusChangeTo106DateTime_TIME TIME
                            END
KnownIssueResolutionCode    CSTRING(61)
DiagnosticResolutionCode    CSTRING(61)
EmailRequested              CSTRING(2)
SMSRequested                CSTRING(2)
SMSAlertNo                  CSTRING(16)
ReturnReasonCode            CSTRING(61)
ExceptionReturnCode         CSTRING(7)
ExceptionReturnCodeIssuedBy CSTRING(31)
ReturnExceptionManagerName  CSTRING(31)
ReturnExceptionManagerID    CSTRING(31)
ReturnDescription           CSTRING(255)
IMEIIntervention            CSTRING(2)
CTNIntervention             CSTRING(2)
StatusChangeTo121DateTime   STRING(8)
StatusChangeTo121DateTime_GROUP GROUP,OVER(StatusChangeTo121DateTime)
StatusChangeTo121DateTime_DATE DATE
StatusChangeTo121DateTime_TIME TIME
                            END
StatusChangeTo120DateTime   STRING(8)
StatusChangeTo120DateTime_GROUP GROUP,OVER(StatusChangeTo120DateTime)
StatusChangeTo120DateTime_DATE DATE
StatusChangeTo120DateTime_TIME TIME
                            END
RepairAdminCharge           DECIMAL(9,2)
ExistingInsuranceCustomer   CSTRING(2)
OriginalSalesAgent          CSTRING(11)
OracleCode                  CSTRING(31)
SoftwareVersionUpToDate     CSTRING(2)
StatusChangeTo109DateTime   STRING(8)
StatusChangeTo109DateTime_GROUP GROUP,OVER(StatusChangeTo109DateTime)
StatusChangeTo109DateTime_DATE DATE
StatusChangeTo109DateTime_TIME TIME
                            END
StatusChangeTo260DateTime   STRING(8)
StatusChangeTo260DateTime_GROUP GROUP,OVER(StatusChangeTo260DateTime)
StatusChangeTo260DateTime_DATE DATE
StatusChangeTo260DateTime_TIME TIME
                            END
COMRef                      CSTRING(51)
RevisedTurnaroundExceptionDate STRING(8)
RevisedTurnaroundExceptionDate_GROUP GROUP,OVER(RevisedTurnaroundExceptionDate)
RevisedTurnaroundExceptionDate_DATE DATE
RevisedTurnaroundExceptionDate_TIME TIME
                            END
DiagnosticFieldName         CSTRING(51)
DiagnosticFieldText         CSTRING(251)
OtherReturnOrigin           CSTRING(51)
StatusChangeTo910DateTime STRING(8)
StatusChangeTo910DateTime_GROUP GROUP,OVER(StatusChangeTo910DateTime)
StatusChangeTo910DateTime_DATE DATE
StatusChangeTo910DateTime_TIME TIME
                            END
CTNInterventionUserName     CSTRING(31)
CTNInterventionEscalationReason CSTRING(41)
CTNInterventionOriginalValueSegment CSTRING(21)
SIDLiteBooking              CSTRING(2)
StatusChangeTo119DateTime   STRING(8)
StatusChangeTo119DateTime_GROUP GROUP,OVER(StatusChangeTo119DateTime)
StatusChangeTo119DateTime_DATE DATE
StatusChangeTo119DateTime_TIME TIME
                            END
                         END
                     END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:ExportFile)

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?tmp:StartDate:Prompt{prop:FontColor} = -1
    ?tmp:StartDate:Prompt{prop:Color} = 15066597
    If ?tmp:StartDate{prop:ReadOnly} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 15066597
    Elsif ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 8454143
    Else ! If ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 16777215
    End ! If ?tmp:StartDate{prop:Req} = True
    ?tmp:StartDate{prop:Trn} = 0
    ?tmp:StartDate{prop:FontStyle} = font:Bold
    ?tmp:EndDate:Prompt{prop:FontColor} = -1
    ?tmp:EndDate:Prompt{prop:Color} = 15066597
    If ?tmp:EndDate{prop:ReadOnly} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 15066597
    Elsif ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 8454143
    Else ! If ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 16777215
    End ! If ?tmp:EndDate{prop:Req} = True
    ?tmp:EndDate{prop:Trn} = 0
    ?tmp:EndDate{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Export      Routine
Data
StatusGroup     Group(),Pre()
local:Status111Date     String(10)
local:Status111Time     String(5)
local:Status125Date     String(10)
local:Status125Time     String(5)
local:Status135Date     String(10)
local:Status135Time     String(5)
local:Status136Date     String(10)
local:Status136Time     String(5)
local:Status140DateFirst    String(10)
local:Status140TimeFirst    String(5)
local:Status140Times        Long()
local:Status140DateLast     String(10)
local:Status140TimeLast     String(5)
local:Status145DateFirst    String(10)
local:Status145TimeFirst    String(5)
local:Status145DateLast     String(10)
local:Status145TimeLast     String(5)
local:Status145Times        Long()
local:Status305Date     String(10)
local:Status305Time     String(5)
local:Status315Date     String(10)
local:Status315Time     String(5)
local:Status350Date     String(10)
local:Status350Time     String(5)
local:Status351Date     String(10)
local:Status351Time     String(5)
local:Status352Date     String(10)
local:Status352Time     String(5)
local:Status355Date     String(10)
local:Status355Time     String(5)
local:Status356Date     String(10)
local:Status356Time     String(5)
local:Status357Date     String(10)
local:Status357Time     String(5)
local:Status358Date     String(10)
local:Status358Time     String(5)
local:Status360Date     String(10)
local:Status360Time     String(5)
local:Status505Date     String(10)
local:Status505Time     String(5)
local:Status520Date     String(10)
local:Status520Time     String(5)
local:Status525Date     String(10)
local:Status525Time     String(5)
local:Status530Date     String(10)
local:Status530Time     String(5)
local:Status535Date     String(10)
local:Status535Time     String(5)
local:Status540Date     String(10)
local:Status540Time     String(5)
local:Status705Date     String(10)
local:Status705Time     String(5)
local:Status797Date     String(10)
local:Status797Time     String(5)
local:Status798Date     String(10)
local:Status798Time     String(5)
local:Status799Date     String(10)
local:Status799Time     String(5)
local:Status901Date     String(10)
local:Status901Time     String(5)
local:Status930Date     String(10)
local:Status930Time     String(5)
local:Status940Date     String(10)
local:Status940Time     String(5)
local:Status796Date     String(10)
local:Status796Time     String(5)
local:Status794Date     String(10)
local:Status794Time     String(5)
local:Status102Date     String(10)
local:Status102Time     String(5)
local:Status104Date     String(10)
local:Status104Time     String(5)
local:Status795Date     String(10)
local:Status795Time     String(5)
local:Status112Date     String(10)
local:Status112Time     String(5)
local:Status113Date     String(10)
local:Status113Time     String(5)
local:Status931Date     String(10)
local:Status931Time     String(5)
local:Status941Date     String(10)
local:Status941Time     String(5)
local:Status101Date     String(10)
local:Status101Time     String(5)
local:Status114Date     String(10)
local:Status114Time     String(5)
local:Status250Date     String(10)
local:Status250Time     String(5)
local:Status251Date     String(10)
local:Status251Time     String(5)
local:Status252Date     String(10)
local:Status252Time     String(5)
local:Status248Date     String(10)
local:Status248Time     String(5)
local:Status106Date     String(10)
local:Status106Time     String(5)
local:Status121Date     String(10)
local:Status121Time     String(5)
local:Status120Date     String(10)
local:Status120Time     String(5)
local:Status109Date     String(10)
local:Status109Time     String(5)
local:Status260Date     String(10)
local:Status260Time     String(5)
local:Status910Date     String(10)
local:Status910Time     String(5)
local:Status119Date     String(10)
local:Status119Time     String(5)

                End ! StatusGroup         Group(),Pre()
local:FoundHandset          Byte(0)
local:FoundWebJobEx         Byte(0)
local:FoundWebRet           Byte(0)
local:FoundReturnCodes      Byte(0)
local:FoundCCExchange       Byte(0)
local:FoundOscarExt         Byte(0)
local:FoundCTNSegment       Byte(0)
local:TimeSlotName          CString(31)
local:OracleCode            String(30)
local:AdditionalSymptoms    String(255)  ! 500  - TrackerBase 11065 GK 07/09/2009
Code

    tmp:ExportFile = 'SIDJobsExtract ' & Format(Today(),@d012) & Format(Clock(),@t02) & '.csv'
    If f:Mode = 1
        If GetTempPathA(255,tmp:TempFilePath)
            If Sub(tmp:TempFilePath,-1,1) = '\'
                tmp:ExportFile = Clip(tmp:TempFilePath) & Clip(tmp:ExportFile)
            Else !If Sub(TempFilePath,-1,1) = '\'
                tmp:ExportFile = Clip(tmp:TempFilePath) & '\' & Clip(tmp:ExportFile)
            End !If Sub(tmp:TempFilePath,-1,1) = '\'
        End
    Else
        If f:Mode = 0
            tmp:SavePath = Path()
            If not filedialog ('Choose File',tmp:ExportFile,'CSV Files|*.CSV', |
                        file:keepdir + file:noerror + file:longname)
                !Failed
                Setpath(tmp:SavePath)
                Exit
            else!If not filedialog
                !Found
                Setpath(tmp:SavePath)

            End!If not filedialog
        End
    End

    tmp:Owner   = GETINI('DEFAULTS','Connection',,Clip(Path()) & '\WEBIMP.INI')
    tmp:CPMOwner = GETINI('DEFAULTS','CPMConnection',,Clip(Path()) & '\WEBIMP.INI')
    tmp:MSSQLConnection = GETINI('DEFAULTS','MSSQLConnection',,Clip(Path()) & '\WEBIMP.INI')

    Open(SQLFile)
    If Error()
        Exit
    End ! If Error()

    Open(HandsetFile)
    If Error()
        Exit
    End ! If Error()

    Open(HistoryFile)
    If Error()
        Exit
    End ! If Error

    Open(WebJobExFile)
    If Error()
        Exit
    End ! If Error

    Open(WebRetFile)
    If Error()
        Exit
    End ! If Error

    Open(JobTxtFile)
    If Error()
        Exit
    End ! If Error

    Open(ReturnCodesFile)
    If Error()
        Exit
    End ! If Error

    Open(CCExchangeFile)
    If Error()
        Exit
    End ! If Error

    Open(TimeSlotFile)
    If Error()
        Exit
    End ! If Error

    Open(CTNSegmentFile)
    If Error()
        Exit
    End ! If Error()

    Open(OscarExtFile)
    If Error()
        Exit
    End ! If Error

    If tmp:WriteSQL = false

        Remove(tmp:ExportFile)
        Create(ExportFile)
        Open(ExportFile)
        If Error()
            Exit                                                                         
        End ! If Error()

        Clear(expfil:Record)
        expfil:Line = '"ServiceBase Job No'
        expfil:Line = Clip(expfil:Line) & '","SID Job No'
        expfil:Line = Clip(expfil:Line) & '","Date Booked'
        expfil:Line = Clip(expfil:Line) & '","Time Booked'
        expfil:Line = Clip(expfil:Line) & '","Booking Type'
        expfil:Line = Clip(expfil:Line) & '","Retail Tote Collected'
        expfil:Line = Clip(expfil:Line) & '","Booking Account'
        expfil:Line = Clip(expfil:Line) & '","Booking Agent'
        expfil:Line = Clip(expfil:Line) & '","Title'
        expfil:Line = Clip(expfil:Line) & '","Initial'
        expfil:Line = Clip(expfil:Line) & '","Surname'
        expfil:Line = Clip(expfil:Line) & '","Mobile Number (CTN)'
        expfil:Line = Clip(expfil:Line) & '","Original IMEI Number As Booked On SID'
        expfil:Line = Clip(expfil:Line) & '","Manufacturer'
        expfil:Line = Clip(expfil:Line) & '","Model Number'
        expfil:Line = Clip(expfil:Line) & '","Colour'
        expfil:Line = Clip(expfil:Line) & '","Handset Type'
        expfil:Line = Clip(expfil:Line) & '","Contract Length'
        expfil:Line = Clip(expfil:Line) & '","Actual IMEI Number Received'
        expfil:Line = Clip(expfil:Line) & '","Charge Type On Booking'
        expfil:Line = Clip(expfil:Line) & '","Insurance Ref Number'
        expfil:Line = Clip(expfil:Line) & '","Out Of Warranty But Not Chargeable Reason'
        expfil:Line = Clip(expfil:Line) & '","Date Of Purchase'
        expfil:Line = Clip(expfil:Line) & '","Customer Home Number / Name'
        expfil:Line = Clip(expfil:Line) & '","Customer Address 1'
        expfil:Line = Clip(expfil:Line) & '","Customer Address 2'
        expfil:Line = Clip(expfil:Line) & '","Customer Address 3'
        expfil:Line = Clip(expfil:Line) & '","Customer Postcode'
        expfil:Line = Clip(expfil:Line) & '","Telephone Number'
        expfil:Line = Clip(expfil:Line) & '","Customer Email Address'
        expfil:Line = Clip(expfil:Line) & '","Collection House Number/ Name'
        expfil:Line = Clip(expfil:Line) & '","Collection Address 1'
        expfil:Line = Clip(expfil:Line) & '","Collection Address 2'
        expfil:Line = Clip(expfil:Line) & '","Collection Address 3'
        expfil:Line = Clip(expfil:Line) & '","Collection Postcode'
        expfil:Line = Clip(expfil:Line) & '","Collection Telephone Number'
        expfil:Line = Clip(expfil:Line) & '","Delivery House Number / Name'
        expfil:Line = Clip(expfil:Line) & '","Delivery Address 1'
        expfil:Line = Clip(expfil:Line) & '","Delivery Address 2'
        expfil:Line = Clip(expfil:Line) & '","Delivery Address 3'
        expfil:Line = Clip(expfil:Line) & '","Delivery Postcode'
        expfil:Line = Clip(expfil:Line) & '","Delivery Telephone Number'
        expfil:Line = Clip(expfil:Line) & '","Exchange IMEI Number'
        expfil:Line = Clip(expfil:Line) & '","Outgoing Consignment Number'
        expfil:Line = Clip(expfil:Line) & '","Primary Fault Description'
        expfil:Line = Clip(expfil:Line) & '","Intermittent Fault'
        expfil:Line = Clip(expfil:Line) & '","Free Text Fault Description'
        expfil:Line = Clip(expfil:Line) & '","Vodafone Resolution Code / Description'
        expfil:Line = Clip(expfil:Line) & '","IMEI Bouncer (Withing 30 Days)'
        expfil:Line = Clip(expfil:Line) & '","Customer Collection Date (Retail)'
        expfil:Line = Clip(expfil:Line) & '","Date Due For Despatch From Service Centre (Retail)'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 111 SID JOB AWAITING PICK'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 111 SID JOB AWAITING PICK'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 125 EXCHANGE IMEI CONFIRMED'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 125 EXCHANGE IMEI CONFIRMED'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 135 DESPATCHED FROM STORE'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 135 DESPATCHED FROM STORE'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 136 AWAITING ENVELOPE DESPATCH'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 136 AWAITING ENVELOPE DESPATCH'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 140 REPAIR ENVELOPE SENT'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 140 REPAIR ENVELOPE SENT'
        expfil:Line = Clip(expfil:Line) & '","Number Of Times REPAIR ENVELOPE SENT'
        expfil:Line = Clip(expfil:Line) & '","Initial Date Sent'
        expfil:Line = Clip(expfil:Line) & '","Last Date Sent'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 145 RE-SEND ENVELOPE REQUESTED'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 145 RE-SEND ENVELOPE REQUESTED'
        expfil:Line = Clip(expfil:Line) & '","Initial Date Requested'
        expfil:Line = Clip(expfil:Line) & '","Last Date Requested'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 305 AWAITING ALLOCATION'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 305 AWAITING ALLOCATION'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 315 IN REPAIR'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 315 IN REPAIR'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 350 NFF - CONTACT CUSTOMER'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 350 NFF - CONTACT CUSTOMER'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 351 NFF - CUST. UNAVAIL'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 351 NFF - CUST. UNAVAIL'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 352 NFF - RETEST - STILL NFF'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 352 NFF - RETEST - STILL NFF'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 355 UNLIKE - CONTACT CUSTOMER'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 355 UNLIKE - CONTACT CUSTOMER'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 356 UNLIKE - CUST CONTACTED'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 356 UNLIKE - CUST CONTACTED'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 357 UNLIKE - RETURN TO CUST'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 357 UNLIKE - RETURN TO CUST'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 358 UNLIKE - CUST UNAVAILABLE'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 358 UNLIKE - CUST UNAVAILABLE'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 360 B.E.R.'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 360 B.E.R.'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 505 ESTIMATE REQUIRED'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 505 ESTIMATE REQUIRED'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 520 ESTIMATE SENT'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 520 ESTIMATE SENT'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 525 2ND ESTIMATE SENT'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 525 2ND ESTIMATE SENT'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 530 ESTIMATE EXPIRED'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 530 ESTIMATE EXPIRED'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 535 ESTIMATE ACCEPTED'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 535 ESTIMATE ACCEPTED'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 540 ESTIMATE REFUSED'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 540 ESTIMATE REFUSED'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 705 COMPLETED'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 705 COMPLETED'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 797 SID B2B REQUEST EXPIRED'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 797 SID B2B REQUEST EXPIRED'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 798 JOB CANCELLED BY VODAFONE'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 798 JOB CANCELLED BY VODAFONE'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 799 JOB CANCELLED'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 799 JOB CANCELLED'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 901 DESPATCHED'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 901 DESPATCHED'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 930 ARRIVED BACK AT STORE'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 930 ARRIVED BACK AT STORE'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 940 PHONE RETURNED TO CUSTOMER'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 940 PHONE RETURNED TO CUSTOMER'
        expfil:Line = Clip(expfil:Line) & '","Job Current Status'
        expfil:Line = Clip(expfil:Line) & '","Incoming Consignment Number'
        expfil:Line = Clip(expfil:Line) & '","Battery'
        expfil:Line = Clip(expfil:Line) & '","Battery Cover'
        expfil:Line = Clip(expfil:Line) & '","Charger'
        expfil:Line = Clip(expfil:Line) & '","Memory Card'
        expfil:Line = Clip(expfil:Line) & '","Courtesy Phone Allocated'
        expfil:Line = Clip(expfil:Line) & '","CP Issue Date'
        expfil:Line = Clip(expfil:Line) & '","CP Non Issue Reason'
        expfil:Line = Clip(expfil:Line) & '","Courtesy Phone IMEI Number'
        expfil:Line = Clip(expfil:Line) & '","Courtesy Phone Manufacturer'
        expfil:Line = Clip(expfil:Line) & '","Courtesy Phone Model Number'
        expfil:Line = Clip(expfil:Line) & '","Stock Out Flag'
        expfil:Line = Clip(expfil:Line) & '","Expected Return Date'
        expfil:Line = Clip(expfil:Line) & '","Deposit Value'
        expfil:Line = Clip(expfil:Line) & '","SID Courtesy Phone Charge'
        expfil:Line = Clip(expfil:Line) & '","Oscar Courtesy Phone Charge'
        expfil:Line = Clip(expfil:Line) & '","SID SKU Code'
        expfil:Line = Clip(expfil:Line) & '","Oscar SKU Code'
        expfil:Line = Clip(expfil:Line) & '","Value Segment'
        expfil:Line = Clip(expfil:Line) & '","Exchange Date'
        expfil:Line = Clip(expfil:Line) & '","Exchange Delivery Type'
        ! New fields - GK 13/11/2007 TrackerBase 9287
        expfil:Line = Clip(expfil:Line) & '","Intermittent Timeframe'
        expfil:Line = Clip(expfil:Line) & '","Key Lock Password'
        expfil:Line = Clip(expfil:Line) & '","Service Charge'
        expfil:Line = Clip(expfil:Line) & '","Delivery Charge'
        expfil:Line = Clip(expfil:Line) & '","BER Charge'
        expfil:Line = Clip(expfil:Line) & '","Cancellation Reason'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 796 BER BOOKING ATTEMPT'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 796 BER BOOKING ATTEMPT'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 794 PENDING JOB - EXPIRED'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 794 PENDING JOB - EXPIRED'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 102 TAC CODE MISMATCH'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 102 TAC CODE MISMATCH'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 104 TAC CODE FAILURE'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 104 TAC CODE FAILURE'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 795 CUSTOMER CANCELLED'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 795 CUSTOMER CANCELLED'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 112 IN-STORE EXCHANGE REQUEST'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 112 IN-STORE EXCHANGE REQUEST'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 113 EXTERNAL EXCHANGE REQUEST'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 113 EXTERNAL EXCHANGE REQUEST'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 931 EXCHANGE ARRIVED IN STORE'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 931 EXCHANGE ARRIVED IN STORE'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 941 EXCHANGE DEVICE ISSUED'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 941 EXCHANGE DEVICE ISSUED'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 101 SID BOOKING INITIATED'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 101 SID BOOKING INITIATED'
        expfil:Line = Clip(expfil:Line) & '","New Insurance Customer'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 114 AWAITING DESPATCH'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 114 AWAITING DESPATCH'
        expfil:Line = Clip(expfil:Line) & '","Diagnosis Available'
        expfil:Line = Clip(expfil:Line) & '","K I Reference'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 250 KNOWN ISSUE RESOLVED'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 250 KNOWN ISSUE RESOLVED'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 251 DIAGNOSIS INITIATED'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 251 DIAGNOSIS INITIATED'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 252 DIAGNOSIS RESOLUTION'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 252 DIAGNOSIS RESOLUTION'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 248 IMEI UNAVAILABLE'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 248 IMEI UNAVAILABLE'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 106 TAC MISMATCH � REDIAGNOSE'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 106 TAC MISMATCH � REDIAGNOSE'
        expfil:Line = Clip(expfil:Line) & '","Known Issue Resolution Code'
        expfil:Line = Clip(expfil:Line) & '","Diagnostic Resolution Code'
        expfil:Line = Clip(expfil:Line) & '","Email Notifications Requested'
        expfil:Line = Clip(expfil:Line) & '","SMS Notifications Requested'
        expfil:Line = Clip(expfil:Line) & '","SMS Alert Number'
        expfil:Line = Clip(expfil:Line) & '","Return Reason Code'
        expfil:Line = Clip(expfil:Line) & '","Exception Return Code'
        expfil:Line = Clip(expfil:Line) & '","Exception Return Code Issued By'
        expfil:Line = Clip(expfil:Line) & '","Return Exception Managers Name'
        expfil:Line = Clip(expfil:Line) & '","Return Exception Managers ID'
        expfil:Line = Clip(expfil:Line) & '","Returns Description'
        expfil:Line = Clip(expfil:Line) & '","IMEI Intervention'
        expfil:Line = Clip(expfil:Line) & '","CTN Intervention'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 121 RETURN JOB BOOKED'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 121 RETURN JOB BOOKED'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 120 7 DAY REPLACEMENT'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 120 7 DAY REPLACEMENT'
        expfil:Line = Clip(expfil:Line) & '","Repair Admin Charge'
        expfil:Line = Clip(expfil:Line) & '","Existing Insurance Customer'
        expfil:Line = Clip(expfil:Line) & '","Original Sales Agent'
        expfil:Line = Clip(expfil:Line) & '","Oracle Code'
        expfil:Line = Clip(expfil:Line) & '","Software Version Up To Date'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 109 CUSTOMER UPDATING SOFTWARE'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 109 CUSTOMER UPDATING SOFTWARE'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 260 INSURANCE CLAIM PROCESSED'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 260 INSURANCE CLAIM PROCESSED'
        expfil:Line = Clip(expfil:Line) & '","COM Ref'
        expfil:Line = Clip(expfil:Line) & '","Revised Turnaround Exception Date'
        expfil:Line = Clip(expfil:Line) & '","Diagnostic Field Name'
        expfil:Line = Clip(expfil:Line) & '","Diagnostic Field Text'
        expfil:Line = Clip(expfil:Line) & '","Other Return Origin'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 910 TRACKING DATA AVAILABLE'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 910 TRACKING DATA AVAILABLE'
        expfil:Line = Clip(expfil:Line) & '","CTN Intervention User Name'
        expfil:Line = Clip(expfil:Line) & '","CTN Intervention Escalation Reason'
        expfil:Line = Clip(expfil:Line) & '","CTN Intervention Original Value Segment'
        expfil:Line = Clip(expfil:Line) & '","SID Lite Booking'
        expfil:Line = Clip(expfil:Line) & '","Date Status Change To 119 7 DAY EXCHANGE'
        expfil:Line = Clip(expfil:Line) & '","Time Status Change To 119 7 DAY EXCHANGE"'

        Add(ExportFile)
    Else ! SQL
        Open(SBJobs)
        If Error()
            tmp:ExportFile = clip(error()) & ', ' & clip(fileerror())
            Exit
        End ! If Error
    End

    If f:Mode = 3
        Prog.ProgressSetup(Records(SIDEXUPD))

        set(seu:SBJobNoKey)
    Else
        Prog.ProgressSetup(Records(JOBS))

        Access:JOBS.Clearkey(job:Date_Booked_Key)
        job:Date_Booked = tmp:StartDate
        Set(job:Date_Booked_Key,job:Date_Booked_Key)
    End

    Loop
        If f:Mode = 3
            If Access:SIDEXUPD.Next()
                Break
            End
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = seu:SBJobNo
            If Access:JOBS.Fetch(job:Ref_Number_Key) <> Level:Benign
                Access:SIDEXUPD.DeleteRecord(0)
                Set(seu:SBJobNoKey)
                cycle
            End
        Else
            If Access:JOBS.Next()
                Break
            End ! If !Access
            If job:Date_Booked > tmp:EndDate
                Break
            End ! If
        End

        If Prog.InsideLoop()
            Break
        End ! If Prog.InsideLoop()

        SQLFile{prop:SQL} = 'Select Ref_Number, ESN, Manufacturer, Model_Number, Colour, PrimaryFault, FreeTextFault, Intermittent_Fault, RepairType, InsuranceRefNo, SBJobNo, ContractPeriod, Reason, BookingType, CollectionMadeToday, CPIssueReason, CPAllocated, CPDepositValue, Battery, BatteryCover, Charger, MemoryCard, CPIMEI, ValueSegment, Phone_Lock, SMSReq, EmailReq, SMSAlertNo From WEBJOB Where SBJobNo = ' & job:Ref_Number
        Next(SQLFile)
        If Error()
            Cycle
        End ! If Error()

        local:FoundWebJobEx = 0
        WebJobExFile{Prop:SQL} = 'SELECT CPChargeValue, CPChargeSKUCode, CPExchanged, CPExChargeValue, CPExChargeSKUCode, CancelReason, InterTimeFrame, ServiceCharge, BERCharge, StartDate, StartTime, DiagAvailable, KnownIssueID, KIResolutionCode, KIResolutionName, DiagResolutionCode, DiagResolutionName, ManualCTNEntry, ManualIMEIEntry, RepairAdminCharge, SoftwareUpToDate, COMRef, NoExceptionDate, DiagFieldName, DiagFieldText, OriginalValueSegment, CTNUserName, CTNEscalationReason, SIDVersion FROM WebJobEx WHERE WebJobNo = ' & sql:Ref_Number
        next(WebJobExFile)
        if not error()
            local:FoundWebJobEx = 1
        end

        local:FoundCCExchange = 0
        CCExchangeFile{Prop:SQL} = 'SELECT TimeSlotID, ExchangeDate, DeliveryCharge FROM CCExchange WHERE WebJobNo = ' & sql:Ref_Number
        next(CCExchangeFile)
        if not error()
            local:FoundCCExchange = 1
        end

        local:FoundWebRet = 0
        local:FoundReturnCodes = 0
        WebRetFile{Prop:SQL} = 'SELECT RetReasonCode, RetReasonName, ExceptionCode, ManagersName, ManagersID, ReturnDescription, ReturnException, OtherRetOrigin FROM WebRet WHERE WebJobNo = ' & sql:Ref_Number
        next(WebRetFile)
        if not error()
            local:FoundWebRet = 1
            if clip(webr:ExceptionCode) <> ''
                ReturnCodesFile{Prop:SQL} = 'SELECT UserName FROM ReturnCodes WHERE ReturnCode = ''' & clip(webr:ExceptionCode) & ''''
                next(ReturnCodesFile)
                if not error()
                    local:FoundReturnCodes = 1
                end
            end
        end

        local:AdditionalSymptoms = ''
        JobTxtFile{Prop:SQL} = 'SELECT Text1, Text2 FROM JobTxt WHERE TxtType = 1 AND WebJobNo = ' & sql:Ref_Number
        next(JobTxtFile)
        if not error()
            ! Additional symptoms is split over several fields
            if clip(jobt:Text1) <> ''
                local:AdditionalSymptoms = clip(jobt:Text1)
            end
            ! TrackerBase 11065 GK 07/09/2009
            !if clip(jobt:Text2) <> ''
            !    local:AdditionalSymptoms = clip(local:AdditionalSymptoms) & clip(jobt:Text2)
            !end
        end

        local:FoundCTNSegment = 0
        CTNSegmentFile{Prop:SQL} = 'SELECT OriginalSalesAgent FROM CTNSegment WHERE CTN = ''' & clip(job:Mobile_Number) & ''''
        next(CTNSegmentFile)
        if not error()
            local:FoundCTNSegment = 1
        end

        local:TimeSlotName = ''
        if local:FoundCCExchange = 1
            if ccex:TimeSlotID > 0
                TimeSlotFile{Prop:SQL} = 'SELECT TimeSlotName FROM TimeSlot WHERE TimeSlotID = ' & ccex:TimeSlotID
                next(TimeSlotFile)
                if error()
                    local:TimeSlotName = 'NEXT DAY'
                else
                    local:TimeSlotName = clip(tmsl:TimeSlotName)
                end
            else
                local:TimeSlotName = 'NEXT DAY'
            end
        end

        local:FoundOscarExt = 0
        local:FoundHandset = 1
        !HandsetFile{prop:SQL} = 'SELECT Handset.CurrentSIDRef, Handset.IMEI, Handset.MakeName, Handset.ModelName, Status.StatusType, Location.LocationName, Handset.ReturnDate FROM Handset LEFT JOIN Status ON Status.StatusID = Handset.StatusID LEFT JOIN Location ON Location.LocationID = Handset.LocationID WHERE Handset.CurrentSIDRef = ' & sql:Ref_Number
! Changing (DBH 14/08/2006) # 7861 - Get the Handset ID from the IMEI on the webjob
!        HandsetFile{prop:SQL} = 'SELECT CurrentSIDRef,IMEI,MakeName,ModelName,StatusID,StatusDate,LocationID,ReturnDate FROM Handset Where CurrentSIDRef = ' & sql:Ref_Number
! to (DBH 14/08/2006) # 7861
        HandsetFile{prop:SQL} = 'SELECT HandsetID, IMEI, MakeName, ModelName, ReturnDate From Handset Where IMEI = ''' & sql:CPIMEI & ''''
! End (DBH 14/08/2006) #7861
        Next(HandsetFile)
        If Error()
            local:FoundHandset = 0
        End ! If Error()

        If local:FoundHandset = 1
            HistoryFile{prop:SQL} = 'Select EntryDate From History Where HandsetID = ' & shan:HandsetID & ' And SIDRef = ' & sql:Ref_Number & |
                                    ' And (StatusID = 3 Or StatusID = 4) Order By EntryDate DESC, EntryTime DESC'
            Next(HistoryFile)

            OscarExtFile{prop:SQL} = 'SELECT DISTINCT OscarExt.CPChargeSKU, OscarExt.CPChargeAmt FROM History LEFT JOIN OscarExt ON OscarExt.HistoryID = History.HistoryID WHERE History.HandsetID = ' & shan:HandsetID & |
                                     ' AND History.SIDRef = ' & sql:Ref_Number & ' AND LENGTH(OscarExt.CPChargeSKU) > 0'

            !OscarExtFile{prop:SQL} = 'SELECT DISTINCT CPChargeSKU, CPChargeAmt FROM OscarExt WHERE (OscarExt.HistoryID IN (SELECT History.HistoryID FROM History WHERE History.HandsetID = ' & shan:HandsetID & |
            !                         ' AND History.SIDRef = ' & sql:Ref_Number & '))'
            Next(OscarExtFile)
            if not error()
                local:FoundOscarExt = 1
            end
        End ! If local:FoundHandset = 1

!        If local:FoundHandset = 1
!            StatusFile{prop:SQL} = 'Select StatusID, StatusName From Status Where StatusID = ''' & Clip(shan:StatusID) & ''''
!            Next(StatusFile)
!            If Error()
!                local:FoundHandset = 0
!            End ! If Error()
!        End ! If local:FoundHandset = 1

!        If local:FoundHandset = 1
!            LocationFile{prop:SQL} = 'Select LocationID, LocationName From Location Where LocationID = ''' & Clip(shan:PhoneLocation) & ''''
!            Next(LocationFile)
!            If Error()
!                local:FoundHandset = 0
!            End ! If Error()
!        End ! If local:FoundHandset = 1

        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            ! Found

        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            ! Error
            Cycle
        End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

        Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
        jbn:RefNumber   = job:Ref_Number
        If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
            ! Found

        Else ! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
            ! Error
            Cycle
        End ! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign

        local:OracleCode = ''
        Access:CARISMA.ClearKey(cma:ManufactModColourKey)
        cma:Manufacturer = Clip(job:Manufacturer)
        cma:ModelNo      = Clip(job:Model_Number)
        cma:Colour       = Clip(job:Colour)
        if Access:CARISMA.Fetch(cma:ManufactModColourKey) = Level:Benign
            If Sub(SQL:PrimaryFault,1,1) = 'C'
                If cma:ContractActive
                    local:OracleCode = cma:ContractOracleCode
                Else ! If cma:OracleActive
                    If cma:PAYTActive
                        local:OracleCode = cma:PAYTOracleCode
                    End ! If cma:PAYTActive
                End ! If cma:OracleActive
            Else
                If Sub(SQL:PrimaryFault,1,1) = 'P'
                    If cma:PAYTActive
                        local:OracleCode = cma:PAYTOracleCode
                    Else ! If cma:PAYTActive
                        If cma:ContractActive
                            local:OracleCode = cma:ContractOracleCode
                        End ! If cma:ContractActive
                    End ! If cma:PAYTActive
                End
            End
        end

        If tmp:WriteSQL = false
            Clear(expfil:Record)
        Else
            tmp:InsertSQL = true
            Clear(sbj:Record)
            sbj:ServiceBaseJobNo = job:Ref_Number
            Get(SBJobs, sbj:PK_SBJobs)
            If Not Error()
                tmp:InsertSQL = false
            End
            Clear(sbj:Record)
        End

        If tmp:WriteSQL = false
            !ServiceBase Job No
            expfil:Line = '"' & job:Ref_Number
            !SID Job Number
            expfil:Line = Clip(expfil:Line) & '","' & Clip(sql:Ref_Number)
            !Date Booked
            expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(job:Date_Booked,@d06))
            !Time Booked
            expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(job:Time_Booked,@t01))
            !Booking Type
            expfil:Line = Clip(expfil:Line) & '","' & Clip(sql:BookingType)
            !Retail Tote Collected
            If sql:BookingType = 'R'
                Case sql:CollectionMadeToday
                Of 0
                    expfil:Line = Clip(expfil:Line) & '","' & Clip('NO')
                Of 1
                    expfil:Line = Clip(expfil:Line) & '","' & Clip('YES')
                Of 2
                    expfil:Line = Clip(expfil:Line) & '","' & Clip('T')
                End ! Case sql:CollectionMadeToday
            Else ! If sql:BookingType = 'R'
                expfil:Line = Clip(expfil:Line) & '","' & Clip('')
            End ! If sql:BookingType = 'R'
            !Booking Account
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Account_Number)
            !Booking Agent
            expfil:Line = Clip(expfil:Line) & '","' & Clip(jobe:TraFaultCode4)
            !Title
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Title)
            !Initial
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Initial)
            !Surname
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Surname)
            !Mobile Number (CTN)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Mobile_Number)
            !Original IMEI Number As Booked On SID
            expfil:Line = Clip(expfil:Line) & '","' & Clip(sql:IMEINumber)
            !Manufacturer
            expfil:Line = Clip(expfil:Line) & '","' & Clip(sql:Manufacturer)
            !Model Number
            expfil:Line = Clip(expfil:Line) & '","' & Clip(sql:ModelNumber)
            !Colour
            expfil:Line = Clip(expfil:Line) & '","' & Clip(sql:Colour)
            !Handset Type
            expfil:Line = Clip(expfil:Line) & '","' & Clip(Sub(sql:PrimaryFault,1,1))
            !Contract Length
            expfil:Line = Clip(expfil:Line) & '","' & Clip(sql:ContractPeriod)
            !Actual IMEI Number Recieved
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:ESN)
            !Charge Type On Booking
            Case sql:RepairType
            Of 'IW'
                expfil:Line = Clip(expfil:Line) & '","' & Clip('IN MANUFACTURERS WARRANTY')
            Of 'IV'
                expfil:Line = Clip(expfil:Line) & '","' & Clip('IN VODAFONE WARRANTY')
            Of 'ON'
                expfil:Line = Clip(expfil:Line) & '","' & Clip('OUT OF WARRANTY BUT NOT CHARGEABLE')
            Of 'IR'
                expfil:Line = Clip(expfil:Line) & '","' & Clip('INSURANCE REPAIR')
            Of 'OC'
                expfil:Line = Clip(expfil:Line) & '","' & Clip('OUT OF WARRANTY AND CHARGEABLE')
            Else
                expfil:Line = Clip(expfil:Line) & '","' & Clip('')
            End ! Case sql:RepairType
            !Insurance Ref Number
            If sql:BookingType = 'R'
                expfil:Line = Clip(expfil:Line) & '","' & Clip(sql:InsuranceRefNo)
            Else ! If sql:BookingType = 'R'
                expfil:Line = Clip(expfil:Line) & '","' & Clip('')
            End ! If sql:BookingType = 'R'
            !Out Of Warranty But Not Chargeable Reason
            If sql:BookingType = 'R'
                expfil:Line = Clip(expfil:Line) & '","' & Clip(sql:Reason)
            Else ! If sql:BookingType = 'R'
                expfil:Line = Clip(expfil:Line) & '","' & Clip('')
            End ! If sql:BookingType = 'R'
            !Date Of Purchase
            expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(job:DOP,@d06b))
            !Customer House Number / Name
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Company_Name)
            !Customer Address 1
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Address_Line1)
            !Customer Address 2
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Address_Line2)
            !Customer Address 3
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Address_Line3)
            !Customer Postcode
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Postcode)
            !Telephone Number
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Telephone_Number)
            !Customer Email Address
            expfil:Line = Clip(expfil:Line) & '","' & Clip(jobe:EndUserEmailAddress)
            !Collection House Number / Name
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Company_Name_Collection)
            !Collection Address 1
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Address_Line1_Collection)
            !Collection Address 2
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Address_Line2_Collection)
            !Collection Address 3
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Address_Line3_Collection)
            !Collection Postcode
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Postcode_Collection)
            !Collection Telephone Number
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Telephone_Collection)
            !Delivery House Number / Name
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Company_Name_Delivery)
            !Delivery Address 1
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Address_Line1_Delivery)
            !Delivery Address 2
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Address_Line2_Delivery)
            !DeliveryAddress 3
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Address_Line3_Delivery)
            !Delivery Postcode
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Postcode_Delivery)
            !Delivery Telephone
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Telephone_Delivery)
            !Exchange IMEI Number
            If job:Exchange_Unit_Number > 0
                Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                xch:Ref_Number  = job:Exchange_Unit_Number
                If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    ! Found
                    expfil:Line = Clip(expfil:Line) & '","' & Clip(xch:ESN)
                Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    ! Error
                    expfil:Line = Clip(expfil:Line) & '","' & Clip('')
                End ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            Else ! If job:Exchange_Unit_Number > 0
                expfil:Line = Clip(expfil:Line) & '","' & Clip('')
            End ! If job:Exchange_Unit_Number > 0
            !Outgoing Consignment Number
            If job:Exchange_Unit_Number > 0
                expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Exchange_Consignment_Number)
            Else ! If job:Exchange_Unit_Number > 0
                expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Consignment_Number)
            End ! If job:Exchange_Unit_Number > 0
            !PrimaryFaultDescription
            expfil:Line = Clip(expfil:Line) & '","' & Clip(BHStripNonAlphaNum(sql:PrimaryFault,' '))
            !Intermittent Fault
            Case sql:IntermittentFault
            Of 0
                expfil:Line = Clip(expfil:Line) & '","' & Clip('NO')
            Of 1
                expfil:Line = Clip(expfil:Line) & '","' & Clip('YES')
            End ! Case sql:IntermittentFault
            ! Additional Symptoms - TrackerBase 10978 GK 12/08/2009
            If clip(local:AdditionalSymptoms) <> ''
                expfil:Line = Clip(expfil:Line) & '","' & Clip(BHStripNonAlphaNum(local:AdditionalSymptoms,' '))
            Else ! Old way
                expfil:Line = Clip(expfil:Line) & '","' & Clip(BHStripNonAlphaNum(sql:FreeTextFault,' '))
            End
            !Vodafone Resolution Code / Description
            expfil:Line = Clip(expfil:Line) & '","' & Clip(Left((BHStripNonAlphaNum(jbn:Invoice_Text,' '))))
            !IMEI Bouncer (exclude exchange CBU/EBU)
            If sql:BookingType = 'E' or sql:BookingType = 'F'
                expfil:Line = Clip(expfil:Line) & '","' & Clip('')
            Else ! If sql:BookingType = 'E'
                LastDate# = 0
                JobNumber# = 0
                Access:JOBS_ALIAS.ClearKey(job_ali:ESN_Key)
                job_ali:ESN = job:ESN
                Set(job_ali:ESN_Key,job_ali:ESN_Key)
                Loop
                    If Access:JOBS_ALIAS.NEXT()
                       Break
                    End !If
                    If job_ali:ESN <> job:ESN      |
                        Then Break.  ! End If
                    If job_ali:Ref_Number <> job:Ref_Number
                        If job_ali:Date_Booked > job:Date_Booked - 30
                            If job_ali:Date_Booked > LastDate#
                                LastDate# = job_ali:Date_Booked
                                JobNumber# = job_ali:Ref_Number
                            End ! If job_ali:Date_Booked > LastDate#
                        End ! If job_ali:Date_Booked < job:Date_Booked - 30
                    End ! If job_ali:Ref_Number <> job:Ref_Number
                End !Loop
                If LastDate# > 0
                    expfil:Line = Clip(expfil:Line) & '","' & Clip(JobNumber#)
                Else ! If LastDate# > 0
                    expfil:Line = Clip(expfil:Line) & '","' & Clip('')
                End ! If LastDate# > 0
            End ! If sql:BookingType = 'E'
            !Customer Collection Date
            If sql:BookingType = 'R'
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(jobe:CustomerCollectionDate,@d06b))
            Else ! If sql:BookingType = 'R'
                expfil:Line = Clip(expfil:Line) & '","' & Clip('')
            End ! If sql:BookingType = 'R'
            !Date Due For Despatch From Service Centre
            If sql:BookingType = 'R'
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(jobe:EstimatedDespatchDate,@d06b))
            Else ! If sql:BookingType = 'R'
                expfil:Line = Clip(expfil:Line) & '","' & Clip('')
            End ! If sql:BookingType = 'R'

        Else ! SQL write
            sbj:ServiceBaseJobNo = job:Ref_Number
            sbj:SIDJobNo = sql:Ref_Number
            sbj:BookedDateTime_DATE = job:Date_Booked
            sbj:BookedDateTime_TIME = job:Time_Booked
            sbj:BookingType = sql:BookingType
            If sql:BookingType = 'R'
                Case sql:CollectionMadeToday
                Of 0
                    sbj:RetailToteCollected = 'NO'
                Of 1
                    sbj:RetailToteCollected = 'YES'
                Of 2
                    sbj:RetailToteCollected = 'T'
                End ! Case sql:CollectionMadeToday
            End
            sbj:BookingAccount = Clip(job:Account_Number)
            sbj:BookingAgent   = Clip(jobe:TraFaultCode4)
            sbj:Title          = Clip(job:Title)
            sbj:Initial        = Clip(job:Initial)
            sbj:Surname        = Clip(job:Surname)
            sbj:CTN            = Clip(job:Mobile_Number)
            sbj:OriginalIMEI   = Clip(sql:IMEINumber)
            sbj:Manufacturer   = Clip(sql:Manufacturer)
            sbj:ModelNo        = Clip(sql:ModelNumber)
            sbj:Colour         = Clip(sql:Colour)
            sbj:HandsetType    = Clip(Sub(sql:PrimaryFault,1,1))
            sbj:ContractLength = sql:ContractPeriod
            sbj:ReceivedIMEI   = Clip(job:ESN)
            !Charge Type On Booking
            Case sql:RepairType
            Of 'IW'
                sbj:ChargeType = 'IN MANUFACTURERS WARRANTY'
            Of 'IV'
                sbj:ChargeType = 'IN VODAFONE WARRANTY'
            Of 'ON'
                sbj:ChargeType = 'OUT OF WARRANTY BUT NOT CHARGEABLE'
            Of 'IR'
                sbj:ChargeType = 'INSURANCE REPAIR'
            Of 'OC'
                sbj:ChargeType = 'OUT OF WARRANTY AND CHARGEABLE'
            End ! Case sql:RepairType
            !Insurance Ref Number
            If sql:BookingType = 'R'
                sbj:InsuranceRefNo = Clip(sql:InsuranceRefNo)
            End ! If sql:BookingType = 'R'
            !Out Of Warranty But Not Chargeable Reason
            If sql:BookingType = 'R'
                sbj:OutOfWarrantyNotChargeableReason = Clip(sql:Reason)
            End ! If sql:BookingType = 'R'
            !Date Of Purchase
            sbj:DOP_DATE = job:DOP
            !Customer House Number / Name
            sbj:CustHouseNumberName = Clip(job:Company_Name)
            !Customer Address 1
            sbj:CustAddressLine1 = Clip(job:Address_Line1)
            !Customer Address 2
            sbj:CustAddressLine2 = Clip(job:Address_Line2)
            !Customer Address 3
            sbj:CustAddressLine3 = Clip(job:Address_Line3)
            !Customer Postcode
            sbj:CustPostcode = Clip(job:Postcode)
            !Telephone Number
            sbj:CustTelNo = Clip(job:Telephone_Number)
            !Customer Email Address
            sbj:CustEmailAddress = Clip(jobe:EndUserEmailAddress)
            !Collection House Number / Name
            sbj:CollHouseNumberName = Clip(job:Company_Name_Collection)
            !Collection Address 1
            sbj:CollAddressLine1 = Clip(job:Address_Line1_Collection)
            !Collection Address 2
            sbj:CollAddressLine2 = Clip(job:Address_Line2_Collection)
            !Collection Address 3
            sbj:CollAddressLine3 = Clip(job:Address_Line3_Collection)
            !Collection Postcode
            sbj:CollPostcode = Clip(job:Postcode_Collection)
            !Collection Telephone Number
            sbj:CollTelNo = Clip(job:Telephone_Collection)
            !Delivery House Number / Name
            sbj:DelHouseNumberName = Clip(job:Company_Name_Delivery)
            !Delivery Address 1
            sbj:DelAddressLine1 = Clip(job:Address_Line1_Delivery)
            !Delivery Address 2
            sbj:DelAddressLine2 = Clip(job:Address_Line2_Delivery)
            !DeliveryAddress 3
            sbj:DelAddressLine3 = Clip(job:Address_Line3_Delivery)
            !Delivery Postcode
            sbj:DelPostcode = Clip(job:Postcode_Delivery)
            !Delivery Telephone
            sbj:DelTelNo =  Clip(job:Telephone_Delivery)
            !Exchange IMEI Number
            If job:Exchange_Unit_Number > 0
                Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                xch:Ref_Number  = job:Exchange_Unit_Number
                If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    ! Found
                    sbj:ExchangeIMEI = Clip(xch:ESN)
                End ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            End ! If job:Exchange_Unit_Number > 0
            !Outgoing Consignment Number
            If job:Exchange_Unit_Number > 0
                sbj:OutgoingConsignmentNo = Clip(job:Exchange_Consignment_Number)
            Else ! If job:Exchange_Unit_Number > 0
                sbj:OutgoingConsignmentNo = Clip(job:Consignment_Number)
            End ! If job:Exchange_Unit_Number > 0
            !PrimaryFaultDescription
            sbj:PrimaryFault = Clip(BHStripNonAlphaNum(sql:PrimaryFault,' '))
            !Intermittent Fault
            Case sql:IntermittentFault
            Of 0
                sbj:IntermittentFault = 'NO'
            Of 1
                sbj:IntermittentFault = 'YES'
            End ! Case sql:IntermittentFault
            ! Additional Symptoms - TrackerBase 10978 GK 12/08/2009
            If clip(local:AdditionalSymptoms) <> ''
                sbj:FreeTextFault = Clip(BHStripNonAlphaNum(local:AdditionalSymptoms,' '))
            Else ! Old way
                sbj:FreeTextFault = Clip(BHStripNonAlphaNum(sql:FreeTextFault,' '))
            End
            !Vodafone Resolution Code / Description
            sbj:VFResolutionCode = Clip(Left((BHStripNonAlphaNum(jbn:Invoice_Text,' '))))
            !IMEI Bouncer (exclude exchange CBU/EBU)
            If sql:BookingType <> 'E' and sql:BookingType <> 'F'
                LastDate# = 0
                JobNumber# = 0
                Access:JOBS_ALIAS.ClearKey(job_ali:ESN_Key)
                job_ali:ESN = job:ESN
                Set(job_ali:ESN_Key,job_ali:ESN_Key)
                Loop
                    If Access:JOBS_ALIAS.NEXT()
                       Break
                    End !If
                    If job_ali:ESN <> job:ESN      |
                        Then Break.  ! End If
                    If job_ali:Ref_Number <> job:Ref_Number
                        If job_ali:Date_Booked > job:Date_Booked - 30
                            If job_ali:Date_Booked > LastDate#
                                LastDate# = job_ali:Date_Booked
                                JobNumber# = job_ali:Ref_Number
                            End ! If job_ali:Date_Booked > LastDate#
                        End ! If job_ali:Date_Booked < job:Date_Booked - 30
                    End ! If job_ali:Ref_Number <> job:Ref_Number
                End !Loop
                If LastDate# > 0
                    sbj:IMEIBouncer = Clip(JobNumber#)
                End ! If LastDate# > 0
            End ! If sql:BookingType = 'E'
            !Customer Collection Date
            If sql:BookingType = 'R'
                sbj:CustCollectionDate_DATE = jobe:CustomerCollectionDate
            End ! If sql:BookingType = 'R'
            !Date Due For Despatch From Service Centre
            If sql:BookingType = 'R'
                sbj:DespatchDueFromSCDate_DATE = jobe:EstimatedDespatchDate
            End ! If sql:BookingType = 'R'

        End

        Clear(StatusGroup)
        Access:AUDSTATS.ClearKey(aus:DateChangedKey)
        aus:RefNumber   = job:Ref_Number
        Set(aus:DateChangedKey,aus:DateChangedKey)
        Loop
            If Access:AUDSTATS.NEXT()
               Break
            End !If
            If aus:RefNumber   <> job:Ref_Number      |
                Then Break.  ! End If
            Case Sub(aus:NewStatus,1,3)
            Of '102'
                local:Status102Date = Format(aus:DateChanged,@d06b)
                local:Status102Time = Format(aus:TimeChanged,@t01b)
            Of '104'
                local:Status104Date = Format(aus:DateChanged,@d06b)
                local:Status104Time = Format(aus:TimeChanged,@t01b)
            Of '106'
                local:Status106Date = Format(aus:DateChanged,@d06b)
                local:Status106Time = Format(aus:TimeChanged,@t01b)
            Of '109'
                local:Status109Date = Format(aus:DateChanged,@d06b)
                local:Status109Time = Format(aus:TimeChanged,@t01b)
            Of '111'
                local:Status111Date = Format(aus:DateChanged,@d06b)
                local:Status111Time = Format(aus:TimeChanged,@t01b)
            Of '112'
                local:Status112Date = Format(aus:DateChanged,@d06b)
                local:Status112Time = Format(aus:TimeChanged,@t01b)
            Of '113'
                local:Status113Date = Format(aus:DateChanged,@d06b)
                local:Status113Time = Format(aus:TimeChanged,@t01b)
            Of '114'
                local:Status114Date = Format(aus:DateChanged,@d06b)
                local:Status114Time = Format(aus:TimeChanged,@t01b)
            Of '119'
                local:Status119Date = Format(aus:DateChanged,@d06b)
                local:Status119Time = Format(aus:TimeChanged,@t01b)
            Of '120'
                local:Status120Date = Format(aus:DateChanged,@d06b)
                local:Status120Time = Format(aus:TimeChanged,@t01b)
            Of '121'
                local:Status121Date = Format(aus:DateChanged,@d06b)
                local:Status121Time = Format(aus:TimeChanged,@t01b)
            Of '125'
                local:Status125Date = Format(aus:DateChanged,@d06b)
                local:Status125Time = Format(aus:TimeChanged,@t01b)
            Of '135'
                local:Status135Date = Format(aus:DateChanged,@d06b)
                local:Status135Time = Format(aus:TimeChanged,@t01b)
            Of '136'
                local:Status136Date = Format(aus:DateChanged,@d06b)
                local:Status136Time = Format(aus:TimeChanged,@t01b)
            Of '140'
                If local:Status140DateFirst = 0
                    local:Status140DateFirst = Format(aus:DateChanged,@d06b)
                    local:Status140TimeFirst = Format(aus:TimeChanged,@t01b)
                End ! If local:Status140 = 0
                local:Status140DateLast = Format(aus:DateChanged,@d06b)
                local:Status140TimeLast = Format(aus:TimeChanged,@t01b)
                local:Status140Times += 1
            Of '145'
                If local:Status145DateFirst = 0
                    local:Status145DateFirst = Format(aus:DateChanged,@d06b)
                    local:Status145TimeFirst = Format(aus:TimeChanged,@t01b)
                End ! If local:Status140 = 0
                local:Status145DateLast = Format(aus:DateChanged,@d06b)
                local:Status145TimeLast = Format(aus:TimeChanged,@t01b)
                local:Status145Times += 1
            Of '248'
                local:Status248Date = Format(aus:DateChanged,@d06b)
                local:Status248Time = Format(aus:TimeChanged,@t01b)
            Of '250'
                local:Status250Date = Format(aus:DateChanged,@d06b)
                local:Status250Time = Format(aus:TimeChanged,@t01b)
            Of '260'
                local:Status260Date = Format(aus:DateChanged,@d06b)
                local:Status260Time = Format(aus:TimeChanged,@t01b)
            Of '251'
                local:Status251Date = Format(aus:DateChanged,@d06b)
                local:Status251Time = Format(aus:TimeChanged,@t01b)
            Of '252'
                local:Status252Date = Format(aus:DateChanged,@d06b)
                local:Status252Time = Format(aus:TimeChanged,@t01b)
            Of '305'
                local:Status305Date = Format(aus:DateChanged,@d06b)
                local:Status305Time = Format(aus:TimeChanged,@t01b)
            Of '315'
                local:Status315Date = Format(aus:DateChanged,@d06b)
                local:Status315Time = Format(aus:TimeChanged,@t01b)
            Of '350'
                local:Status350Date = Format(aus:DateChanged,@d06b)
                local:Status350Time = Format(aus:TimeChanged,@t01b)
            Of '351'
                local:Status351Date = Format(aus:DateChanged,@d06b)
                local:Status351Time = Format(aus:TimeChanged,@t01b)
            Of '352'
                local:Status352Date = Format(aus:DateChanged,@d06b)
                local:Status352Time = Format(aus:TimeChanged,@t01b)
            Of '355'
                local:Status355Date = Format(aus:DateChanged,@d06b)
                local:Status355Time = Format(aus:TimeChanged,@t01b)
            Of '356'
                local:Status356Date = Format(aus:DateChanged,@d06b)
                local:Status356Time = Format(aus:TimeChanged,@t01b)
            Of '357'
                local:Status357Date = Format(aus:DateChanged,@d06b)
                local:Status357Time = Format(aus:TimeChanged,@t01b)
            Of '358'
                local:Status358Date = Format(aus:DateChanged,@d06b)
                local:Status358Time = Format(aus:TimeChanged,@t01b)
            Of '360'
                local:Status360Date = Format(aus:DateChanged,@d06b)
                local:Status360Time = Format(aus:TimeChanged,@t01b)
            Of '505'
                local:Status505Date = Format(aus:DateChanged,@d06b)
                local:Status505Time = Format(aus:TimeChanged,@t01b)
            Of '520'
                local:Status520Date = Format(aus:DateChanged,@d06b)
                local:Status520Time = Format(aus:TimeChanged,@t01b)
            Of '525'
                local:Status525Date = Format(aus:DateChanged,@d06b)
                local:Status525Time = Format(aus:TimeChanged,@t01b)
            Of '530'
                local:Status530Date = Format(aus:DateChanged,@d06b)
                local:Status530Time = Format(aus:TimeChanged,@t01b)
            Of '535'
                local:Status535Date = Format(aus:DateChanged,@d06b)
                local:Status535Time = Format(aus:TimeChanged,@t01b)
            Of '540'
                local:Status540Date = Format(aus:DateChanged,@d06b)
                local:Status540Time = Format(aus:TimeChanged,@t01b)
            Of '705'
                local:Status705Date = Format(aus:DateChanged,@d06b)
                local:Status705Time = Format(aus:TimeChanged,@t01b)
            Of '794'
                local:Status794Date = Format(aus:DateChanged,@d06b)
                local:Status794Time = Format(aus:TimeChanged,@t01b)
            Of '795'
                local:Status795Date = Format(aus:DateChanged,@d06b)
                local:Status795Time = Format(aus:TimeChanged,@t01b)
            Of '796'
                local:Status796Date = Format(aus:DateChanged,@d06b)
                local:Status796Time = Format(aus:TimeChanged,@t01b)
            Of '797'
                local:Status797Date = Format(aus:DateChanged,@d06b)
                local:Status797Time = Format(aus:TimeChanged,@t01b)
            Of '798'
                local:Status798Date = Format(aus:DateChanged,@d06b)
                local:Status798Time = Format(aus:TimeChanged,@t01b)
            Of '799'
                local:Status799Date = Format(aus:DateChanged,@d06b)
                local:Status799Time = Format(aus:TimeChanged,@t01b)
            Of '901'
                local:Status901Date = Format(aus:DateChanged,@d06b)
                local:Status901Time = Format(aus:TimeChanged,@t01b)
            Of '910'
                local:Status910Date = Format(aus:DateChanged,@d06b)
                local:Status910Time = Format(aus:TimeChanged,@t01b)
            Of '930'
                local:Status930Date = Format(aus:DateChanged,@d06b)
                local:Status930Time = Format(aus:TimeChanged,@t01b)
            Of '931'
                local:Status931Date = Format(aus:DateChanged,@d06b)
                local:Status931Time = Format(aus:TimeChanged,@t01b)
            Of '940'
                local:Status940Date = Format(aus:DateChanged,@d06b)
                local:Status940Time = Format(aus:TimeChanged,@t01b)
            Of '941'
                local:Status941Date = Format(aus:DateChanged,@d06b)
                local:Status941Time = Format(aus:TimeChanged,@t01b)
            End ! Case Sub(aus:NewStatus,1,3)
        End !Loop

        ! The 101 SID BOOKING INITIATED status is the start date/time from WebJobEx
        local:Status101Date = Format(webex:StartDate,@d06b)
        local:Status101Time = Format(webex:StartTime,@t01b)
        ! Continuation of above
        If tmp:WriteSQL = false
            !Date Status Changed to 111 SID JOB AWAITING PICK
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status111Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status111Time)
            !Date Status Change To 125 EXCHANGE IMEI CONFIRMED
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status125Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status125Time)
            !Date Status Change To 135 DESPATCH FROM STORE
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status135Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status135Time)
            !Date Status Change To 136 AWAITING ENVELOPE DESPATCH
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status136Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status136Time)
            !Date Status Change To 140 REPAIR ENVELOPE SENT
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status140DateLast)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status140TimeLast)
            !Number Of Times REPAIR ENVELOPE SENT
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status140Times)
            !Initial Date Requested
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status140DateFirst)
            !Last Date Requested
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status140DateLast)
            !Date Status Changed To 145 RESEND ENVELOPE REQUESTED
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status145DateLast)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status145TimeLast)
            !Initial Date Requested
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status145DateFirst)
            !Last Date Requested
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status145DateLast)
            !Date Status Changed To 305 AWAITING ALLOCATION
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status305Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status305Time)
            !Date Status Change To 315 IN REPAIR
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status315Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status315Time)
            !Date Status Change To 350 NFF - CONTACT CUSTOMER
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status350Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status350Time)
            !Date Status Change To 351 NFF - CUST UNAVAIL
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status351Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status351Time)
            !Date Status Change To 352 NFF - RETEST - STILL NFF
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status352Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status352Time)
            !Date Status Change To 355 UNLIKE - CONTACT CUSTOMER
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status355Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status355Time)
            !Date Status Change To 356 UNLIKE - CUST CONTACTED
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status356Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status356Time)
            !Date Status Change To 357 UNLIKE - CONTACT CUSTOMER
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status357Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status357Time)
            !Date Status Change To 358 UNLIKE - CUST UNAVAILABLE
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status358Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status358Time)
            !Date Status Change To 360 BER
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status360Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status360Time)
            !Date Status Change To 505 ESTIMATE REQUIRED
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status505Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status505Time)
            !Date Status Change To 520 ESTIMATE SENT
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status520Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status520Time)
            !Date Status Change To 525 2nd ESTIMATE SENT
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status525Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status525Time)
            !Date Status Change To 530 ESTIMATE EXPIRED
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status530Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status530Time)
            !Date Status Change To 535 ESTIMATE ACCEPTED
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status535Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status535Time)
            !Date Status Change To 540 ESTIMATE REFUSED
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status540Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status540Time)
            !Date Status Change To 705 COMPLETED
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status705Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status705Time)
            !Date Status Change To 797 SID B2B REQUEST EXPIRED
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status797Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status797Time)
            !Date Status Change To 798 JOB CANCELLED BY VODAFONE
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status798Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status798Time)
            !Date Status Change To 799 JOB CANCELLED
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status799Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status799Time)
            !Date Status Change To 901 DESPATCHED
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status901Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status901Time)
            !Date Status Change To 930 ARRIVED BACK AT STORE
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status930Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status930Time)
            !Date Status Changed To 940 PHONE RETURNED TO CUSTOMER
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status940Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status940Time)
        else ! SQL write
        
            !Date Status Changed to 111 SID JOB AWAITING PICK
            sbj:StatusChangeTo111DateTime_DATE = Deformat(Clip(local:Status111Date), @d06b)
            sbj:StatusChangeTo111DateTime_TIME = Deformat(Clip(local:Status111Time), @t01b)
            !Date Status Change To 125 EXCHANGE IMEI CONFIRMED
            sbj:StatusChangeTo125DateTime_DATE = Deformat(Clip(local:Status125Date), @d06b)
            sbj:StatusChangeTo125DateTime_TIME = Deformat(Clip(local:Status125Time), @t01b)
            !Date Status Change To 135 DESPATCH FROM STORE
            sbj:StatusChangeTo135DateTime_DATE = Deformat(Clip(local:Status135Date), @d06b)
            sbj:StatusChangeTo135DateTime_TIME = Deformat(Clip(local:Status135Time), @t01b)
            !Date Status Change To 136 AWAITING ENVELOPE DESPATCH
            sbj:StatusChangeTo136DateTime_DATE = Deformat(Clip(local:Status136Date), @d06b)
            sbj:StatusChangeTo136DateTime_TIME = Deformat(Clip(local:Status136Time), @t01b)
            !Date Status Change To 140 REPAIR ENVELOPE SENT
            sbj:StatusChangeTo140DateTime_DATE = Deformat(Clip(local:Status140DateLast), @d06b)
            sbj:StatusChangeTo140DateTime_TIME = Deformat(Clip(local:Status140TimeLast), @t01b)
            !Number Of Times REPAIR ENVELOPE SENT
            sbj:NumOfTimesEnvelopesSent = Clip(local:Status140Times)
            !Initial Date Requested
            sbj:InitialDateSent_DATE = Deformat(Clip(local:Status140DateFirst), @d06b)
            !Last Date Requested
            sbj:LastDateSent_DATE = Deformat(Clip(local:Status140DateLast), @d06b)
            !Date Status Changed To 145 RESEND ENVELOPE REQUESTED
            sbj:StatusChangeTo145DateTime_DATE = Deformat(Clip(local:Status145DateLast), @d06b)
            sbj:StatusChangeTo145DateTime_TIME = Deformat(Clip(local:Status145TimeLast), @t01b)
            !Initial Date Requested
            sbj:InitialDateRequested_DATE = Deformat(Clip(local:Status145DateFirst), @d06b)
            !Last Date Requested
            sbj:LastDateRequested_DATE = Deformat(Clip(local:Status145DateLast), @d06b)
            !Date Status Changed To 305 AWAITING ALLOCATION
            sbj:StatusChangeTo305DateTime_DATE = Deformat(Clip(local:Status305Date), @d06b)
            sbj:StatusChangeTo305DateTime_TIME = Deformat(Clip(local:Status305Time), @t01b)
            !Date Status Change To 315 IN REPAIR
            sbj:StatusChangeTo315DateTime_DATE = Deformat(Clip(local:Status315Date), @d06b)
            sbj:StatusChangeTo315DateTime_TIME = Deformat(Clip(local:Status315Time), @t01b)
            !Date Status Change To 350 NFF - CONTACT CUSTOMER
            sbj:StatusChangeTo350DateTime_DATE = Deformat(Clip(local:Status350Date), @d06b)
            sbj:StatusChangeTo350DateTime_TIME = Deformat(Clip(local:Status350Time), @t01b)
            !Date Status Change To 351 NFF - CUST UNAVAIL
            sbj:StatusChangeTo351DateTime_DATE = Deformat(Clip(local:Status351Date), @d06b)
            sbj:StatusChangeTo351DateTime_TIME = Deformat(Clip(local:Status351Time), @t01b)
            !Date Status Change To 352 NFF - RETEST - STILL NFF
            sbj:StatusChangeTo352DateTime_DATE = Deformat(Clip(local:Status352Date), @d06b)
            sbj:StatusChangeTo352DateTime_TIME = Deformat(Clip(local:Status352Time), @t01b)
            !Date Status Change To 355 UNLIKE - CONTACT CUSTOMER
            sbj:StatusChangeTo355DateTime_DATE = Deformat(Clip(local:Status355Date), @d06b)
            sbj:StatusChangeTo355DateTime_TIME = Deformat(Clip(local:Status355Time), @t01b)
            !Date Status Change To 356 UNLIKE - CUST CONTACTED
            sbj:StatusChangeTo356DateTime_DATE = Deformat(Clip(local:Status356Date), @d06b)
            sbj:StatusChangeTo356DateTime_TIME = Deformat(Clip(local:Status356Time), @t01b)
            !Date Status Change To 357 UNLIKE - CONTACT CUSTOMER
            sbj:StatusChangeTo357DateTime_DATE = Deformat(Clip(local:Status357Date), @d06b)
            sbj:StatusChangeTo357DateTime_TIME = Deformat(Clip(local:Status357Time), @t01b)
            !Date Status Change To 358 UNLIKE - CUST UNAVAILABLE
            sbj:StatusChangeTo358DateTime_DATE = Deformat(Clip(local:Status358Date), @d06b)
            sbj:StatusChangeTo358DateTime_TIME = Deformat(Clip(local:Status358Time), @t01b)
            !Date Status Change To 360 BER
            sbj:StatusChangeTo360DateTime_DATE = Deformat(Clip(local:Status360Date), @d06b)
            sbj:StatusChangeTo360DateTime_TIME = Deformat(Clip(local:Status360Time), @t01b)
            !Date Status Change To 505 ESTIMATE REQUIRED
            sbj:StatusChangeTo505DateTime_DATE = Deformat(Clip(local:Status505Date), @d06b)
            sbj:StatusChangeTo505DateTime_TIME = Deformat(Clip(local:Status505Time), @t01b)
            !Date Status Change To 520 ESTIMATE SENT
            sbj:StatusChangeTo520DateTime_DATE = Deformat(Clip(local:Status520Date), @d06b)
            sbj:StatusChangeTo520DateTime_DATE = Deformat(Clip(local:Status520Time), @t01b)
            !Date Status Change To 525 2nd ESTIMATE SENT
            sbj:StatusChangeTo525DateTime_DATE = Deformat(Clip(local:Status525Date), @d06b)
            sbj:StatusChangeTo525DateTime_TIME = Deformat(Clip(local:Status525Time), @t01b)
            !Date Status Change To 530 ESTIMATE EXPIRED
            sbj:StatusChangeTo530DateTime_DATE = Deformat(Clip(local:Status530Date), @d06b)
            sbj:StatusChangeTo530DateTime_TIME = Deformat(Clip(local:Status530Time), @t01b)
            !Date Status Change To 535 ESTIMATE ACCEPTED
            sbj:StatusChangeTo535DateTime_DATE = Deformat(Clip(local:Status535Date), @d06b)
            sbj:StatusChangeTo535DateTime_TIME = Deformat(Clip(local:Status535Time), @t01b)
            !Date Status Change To 540 ESTIMATE REFUSED
            sbj:StatusChangeTo540DateTime_DATE = Deformat(Clip(local:Status540Date), @d06b)
            sbj:StatusChangeTo540DateTime_TIME = Deformat(Clip(local:Status540Time), @t01b)
            !Date Status Change To 705 COMPLETED
            sbj:StatusChangeTo705DateTime_DATE = Deformat(Clip(local:Status705Date), @d06b)
            sbj:StatusChangeTo705DateTime_TIME = Deformat(Clip(local:Status705Time), @t01b)
            !Date Status Change To 797 SID B2B REQUEST EXPIRED
            sbj:StatusChangeTo797DateTime_DATE = Deformat(Clip(local:Status797Date), @d06b)
            sbj:StatusChangeTo797DateTime_TIME = Deformat(Clip(local:Status797Time), @t01b)
            !Date Status Change To 798 JOB CANCELLED BY VODAFONE
            sbj:StatusChangeTo798DateTime_DATE = Deformat(Clip(local:Status798Date), @d06b)
            sbj:StatusChangeTo798DateTime_TIME = Deformat(Clip(local:Status798Time), @t01b)
            !Date Status Change To 799 JOB CANCELLED
            sbj:StatusChangeTo799DateTime_DATE = Deformat(Clip(local:Status799Date), @d06b)
            sbj:StatusChangeTo799DateTime_TIME = Deformat(Clip(local:Status799Time), @t01b)
            !Date Status Change To 901 DESPATCHED
            sbj:StatusChangeTo901DateTime_DATE = Deformat(Clip(local:Status901Date), @d06b)
            sbj:StatusChangeTo901DateTime_TIME = Deformat(Clip(local:Status901Time), @t01b)
            !Date Status Change To 930 ARRIVED BACK AT STORE
            sbj:StatusChangeTo930DateTime_DATE = Deformat(Clip(local:Status930Date), @d06b)
            sbj:StatusChangeTo930DateTime_TIME = Deformat(Clip(local:Status930Time), @t01b)
            !Date Status Changed To 940 PHONE RETURNED TO CUSTOMER
            sbj:StatusChangeTo940DateTime_DATE = Deformat(Clip(local:Status940Date), @d06b)
            sbj:StatusChangeTo940DateTime_TIME = Deformat(Clip(local:Status940Time), @t01b)
        end
        ! Continuation of above routine ...

        If tmp:WriteSQL = false

            !Current Job Status
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Current_Status)
            !Incoming Consignment Number
            expfil:Line = Clip(expfil:Line) & '","' & Clip(job:Incoming_Consignment_Number)
            !Battery
            If Clip(sql:Battery) = 1
                expfil:Line = Clip(expfil:Line) & '","Y'
            Else ! If Clip(sql:Battery) = 1
                expfil:Line = Clip(expfil:Line) & '","N'
            End ! If Clip(sql:Battery) = 1
            !Battery Cover
            If CLip(sql:BatterCover) = 1
                expfil:Line = Clip(expfil:Line) & '","Y'
            Else ! If Clip(sql:Battery) = 1
                expfil:Line = Clip(expfil:Line) & '","N'
            End ! If Clip(sql:Battery) = 1
            !Charger
            If Clip(sql:Charger) = 1
                expfil:Line = Clip(expfil:Line) & '","Y'
            Else ! If Clip(sql:Battery) = 1
                expfil:Line = Clip(expfil:Line) & '","N'
            End ! If Clip(sql:Battery) = 1
            !Memory Card
            If Clip(sql:MemoryCard) = 1
                expfil:Line = Clip(expfil:Line) & '","Y'
            Else ! If Clip(sql:Battery) = 1
                expfil:Line = Clip(expfil:Line) & '","N'
            End ! If Clip(sql:Battery) = 1
            !Couresty Phone Allocated
            expfil:Line = Clip(expfil:Line) & '","' & Clip(SQL:CPAllocated)
            !CP Issue Date
            If local:FoundHandset
                expfil:Line = Clip(expfil:Line) & '","' & Format(hist:EntryDate,@d06)
            Else ! If local:FoundHandset
                expfil:Line = Clip(expfil:Line) & '","'
            End ! If local:FoundHandset
            !CP Non Issue Reason
            If Upper(Clip(SQL:CPAllocated)) = 'N'
                expfil:Line = Clip(expfil:Line) & '","' & Clip(sql:CPIssueReason)
            Else ! If local:FoundHandset
                expfil:Line = Clip(expfil:Line) & '","'
            End ! If local:FoundHandset
            If local:FoundHandset
                !Courtesy Phone IMEI
                expfil:Line = Clip(expfil:Line) & '","' & Clip(shan:IMEINumber)
                !Courtest Phone Make
                expfil:Line = Clip(expfil:Line) & '","' & Clip(shan:Manufacturer)
                !Courtesy Phone Model
                expfil:Line = Clip(expfil:Line) & '","' & Clip(shan:ModelNumber)
                !Stock Out Flag
                if sql:CPAllocated = 'S'
                    expfil:Line = Clip(expfil:Line) & '","Y'
                else
                    expfil:Line = Clip(expfil:Line) & '","'
                end
                !Expected Return Date
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(shan:ReturnDate,@d06b))
                !Deposit Value
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(sql:DepositValue,@n_11.2))
                !SID Courtesy Phone Charge
                if local:FoundWebJobEx = 1
                    expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(webex:CPChargeValue,@n_11.2))
                else
                    expfil:Line = Clip(expfil:Line) & '","'
                end
                !Oscar Courtesy Phone Charge
                if local:FoundOscarExt = 1
                    expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(osce:CPChargeAmt,@n_11.2))
                else
                    expfil:Line = Clip(expfil:Line) & '","'
                end
                !SID SKU Code
                if local:FoundWebJobEx = 1
                    expfil:Line = Clip(expfil:Line) & '","' & Clip(webex:CPChargeSKUCode)
                else
                    expfil:Line = Clip(expfil:Line) & '","'
                end
                !Oscar SKU Code
                if local:FoundOscarExt = 1
                    expfil:Line = Clip(expfil:Line) & '","' & Clip(osce:CPChargeSKU)
                else
                    expfil:Line = Clip(expfil:Line) & '","'
                end
            Else ! If local:HandsetFound
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
            End ! If local:HandsetFound

            !Value Segment
            expfil:Line = Clip(expfil:Line) & '","' & Clip(sql:ValueSegment)
            !Exchange Date
            if local:FoundCCExchange = 1
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(ccex:ExchangeDate,@d06b))
            else
                expfil:Line = Clip(expfil:Line) & '","'
            end
            !Exchange Delivery Type
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:TimeSlotName)
            !Intermittent Timeframe
            if local:FoundWebJobEx = 1
                expfil:Line = Clip(expfil:Line) & '","' & Clip(webex:InterTimeFrame)
            else
                expfil:Line = Clip(expfil:Line) & '","'
            end
            !Key Lock Password
            expfil:Line = Clip(expfil:Line) & '","' & Clip(sql:Phone_Lock)
            !Service Charge
            if local:FoundWebJobEx = 1
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(webex:ServiceCharge,@n_11.2))
            else
                expfil:Line = Clip(expfil:Line) & '","'
            end
            !Delivery Charge
            if local:FoundCCExchange = 1
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(ccex:DeliveryCharge,@n_11.2))
            else
                expfil:Line = Clip(expfil:Line) & '","'
            end
            !BER Charge
            if local:FoundWebJobEx = 1
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(webex:BERCharge,@n_11.2))
            else
                expfil:Line = Clip(expfil:Line) & '","'
            end
            !Cancellation Reason
            if local:FoundWebJobEx = 1
                expfil:Line = Clip(expfil:Line) & '","' & Clip(webex:CancelReason)
            else
                expfil:Line = Clip(expfil:Line) & '","'
            end

            !Date Status Changed to 796 BER BOOKING ATTEMPT
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status796Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status796Time)
            !Date Status Changed to 794 PENDING JOB - EXPIRED
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status794Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status794Time)
            !Date Status Changed to 102 TAC CODE MISMATCH
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status102Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status102Time)
            !Date Status Changed to 104 TAC CODE FAILURE
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status104Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status104Time)
            !Date Status Changed to 795 CUSTOMER CANCELLED
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status795Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status795Time)
            !Date Status Changed to 112 IN-STORE EXCHANGE REQUEST
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status112Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status112Time)
            !Date Status Changed to 113 EXTERNAL EXCHANGE REQUEST
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status113Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status113Time)
            !Date Status Changed to 931 EXCHANGE ARRIVED IN STORE
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status931Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status931Time)
            !Date Status Changed to 941 EXCHANGE DEVICE ISSUED
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status941Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status941Time)
            !Date Status Changed to 101 SID BOOKING INITIATED
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status101Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status101Time)

            ! New insurance customer - no longer used
            expfil:Line = Clip(expfil:Line) & '","'

            !Date Status Changed to 114 AWAITING DESPATCH
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status114Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status114Time)

            if local:FoundWebJobEx = 1
                if webex:DiagAvailable = 1
                    expfil:Line = Clip(expfil:Line) & '","Y'
                else
                    expfil:Line = Clip(expfil:Line) & '","N'
                end
                expfil:Line = Clip(expfil:Line) & '","' & Clip(webex:KnownIssueID)
            else
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
            end

            !Date Status Changed to 250 KNOWN ISSUE RESOLVED
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status250Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status250Time)

            !Date Status Changed to 251 DIAGNOSIS INITIATED
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status251Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status251Time)

            !Date Status Changed to 252 DIAGNOSIS RESOLUTION
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status252Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status252Time)

            !Date Status Changed to 248 IMEI UNAVAILABLE
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status248Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status248Time)

            !Date Status Changed to 106 TAC MISMATCH � REDIAGNOSE
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status106Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status106Time)

            if local:FoundWebJobEx = 1
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Clip(webex:KIResolutionCode) & ' ' & Clip(webex:KIResolutionName))
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Clip(webex:DiagResolutionCode) & ' ' & Clip(webex:DiagResolutionName))
            else
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
            end

            If Clip(sql:EmailReq) = 1
                expfil:Line = Clip(expfil:Line) & '","Y'
            Else ! If Clip(sql:EmailReq) = 1
                expfil:Line = Clip(expfil:Line) & '","N'
            End ! If Clip(sql:EmailReq) = 1

            If Clip(sql:SMSReq) = 1
                expfil:Line = Clip(expfil:Line) & '","Y'
            Else ! If Clip(sql:SMSReq) = 1
                expfil:Line = Clip(expfil:Line) & '","N'
            End ! If Clip(sql:SMSReq) = 1

            expfil:Line = Clip(expfil:Line) & '","' & Clip(sql:SMSAlertNo)

            if local:FoundWebRet = 1
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Clip(webr:RetReasonCode) & ' ' & Clip(webr:RetReasonName))
                expfil:Line = Clip(expfil:Line) & '","' & Clip(webr:ExceptionCode)
                if local:FoundReturnCodes = 1
                    expfil:Line = Clip(expfil:Line) & '","' & Clip(retc:UserName)
                else
                    expfil:Line = Clip(expfil:Line) & '","'
                end
                expfil:Line = Clip(expfil:Line) & '","' & Clip(webr:ManagersName)
                expfil:Line = Clip(expfil:Line) & '","' & Clip(webr:ManagersID)
                expfil:Line = Clip(expfil:Line) & '","' & Clip(BHStripNonAlphaNum(webr:ReturnDescription,' '))
            else
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
            end

            if local:FoundWebJobEx = 1
                If Clip(webex:ManualIMEIEntry) = 1
                    expfil:Line = Clip(expfil:Line) & '","Y'
                Else ! If Clip(sql:SMSReq) = 1
                    expfil:Line = Clip(expfil:Line) & '","N'
                End ! If Clip(sql:SMSReq) = 1

                If Clip(webex:ManualCTNEntry) = 1
                    expfil:Line = Clip(expfil:Line) & '","Y'
                Else ! If Clip(sql:SMSReq) = 1
                    expfil:Line = Clip(expfil:Line) & '","N'
                End ! If Clip(sql:SMSReq) = 1
            else
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
            end

            !Date Status Changed to 121 RETURN JOB BOOKED
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status121Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status121Time)

            !Date Status Changed to 120 7 DAY REPLACEMENT
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status120Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status120Time)

            ! Repair Admin Charge
            if local:FoundWebJobEx = 1
                expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(webex:RepairAdminCharge,@n_11.2))
            else
                expfil:Line = Clip(expfil:Line) & '","'
            end

            ! Existing Insurance Customer (Y or N)
            if Instring('INSURANCE', Clip(Upper(sql:ValueSegment)), 1, 1) = 0
                expfil:Line = Clip(expfil:Line) & '","N'
            else
                expfil:Line = Clip(expfil:Line) & '","Y'
            end

            ! Original Sales Agent
            if local:FoundCTNSegment = 1
                expfil:Line = Clip(expfil:Line) & '","' & Clip(ctseg:OriginalSalesAgent)
            else
                expfil:Line = Clip(expfil:Line) & '","'
            end

            ! Oracle Code
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:OracleCode)

            ! Software Version Up To Date
            if local:FoundWebJobEx = 1
                expfil:Line = Clip(expfil:Line) & '","' & Clip(webex:SoftwareUpToDate)
            else
                expfil:Line = Clip(expfil:Line) & '","'
            end

            !Date Status Changed to 109 CUSTOMER UPDATING SOFTWARE
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status109Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status109Time)

            !Date Status Changed to 260 INSURANCE CLAIM PROCESSED
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status260Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status260Time) 

            ! COM Ref
            if local:FoundWebJobEx = 1
                expfil:Line = Clip(expfil:Line) & '","' & Clip(webex:COMRef)
            else
                expfil:Line = Clip(expfil:Line) & '","'
            end

            ! No exception date
            if local:FoundWebJobEx = 1
                if webex:NoExceptionDate <> ''
                    expfil:Line = Clip(expfil:Line) & '","' & Clip(Format(webex:NoExceptionDate,@d06b))
                else
                    expfil:Line = Clip(expfil:Line) & '","'
                end
            else
                expfil:Line = Clip(expfil:Line) & '","'
            end

            ! Diagnostic data field
            if local:FoundWebJobEx = 1
                if webex:DiagFieldName <> ''
                    expfil:Line = Clip(expfil:Line) & '","' & Clip(webex:DiagFieldName)
                else
                    expfil:Line = Clip(expfil:Line) & '","'
                end
                if webex:DiagFieldText <> ''
                    expfil:Line = Clip(expfil:Line) & '","' & Clip(webex:DiagFieldText)
                else
                    expfil:Line = Clip(expfil:Line) & '","'
                end
            else
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
            end

            ! Other return origin
            if local:FoundWebRet = 1
                expfil:Line = Clip(expfil:Line) & '","' & Clip(webr:OtherRetOrigin)
            else
                expfil:Line = Clip(expfil:Line) & '","'
            end

            ! Date Status Changed to 910 TRACKING DATA AVAILABLE
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status910Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status910Time)

            if local:FoundWebJobEx = 1
                ! CTN Intervention User Name
                if webex:CTNUserName <> ''
                    expfil:Line = Clip(expfil:Line) & '","' & Clip(webex:CTNUserName)
                else
                    expfil:Line = Clip(expfil:Line) & '","'
                end
                ! CTN Intervention Reason For Escalation
                if webex:CTNEscalationReason <> ''
                    expfil:Line = Clip(expfil:Line) & '","' & Clip(webex:CTNEscalationReason)
                else
                    expfil:Line = Clip(expfil:Line) & '","'
                end
                ! CTN Intervention Original Value Segment
                if webex:OriginalValueSegment <> ''
                    expfil:Line = Clip(expfil:Line) & '","' & Clip(webex:OriginalValueSegment)
                else
                    expfil:Line = Clip(expfil:Line) & '","'
                end
                ! SID Lite Booking
                if Clip(webex:SIDVersion) = 'L'
                    expfil:Line = Clip(expfil:Line) & '","Y'
                else
                    expfil:Line = Clip(expfil:Line) & '","N'
                end
            else
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
                expfil:Line = Clip(expfil:Line) & '","'
            end

            !Date Status Changed to 119 7 DAY EXCHANGE
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status119Date)
            expfil:Line = Clip(expfil:Line) & '","' & Clip(local:Status119Time) & '"'

            Add(ExportFile)
        else ! SQL write
            !Current Job Status
            sbj:CurrentStatus = Clip(job:Current_Status)
            !Incoming Consignment Number
            sbj:IncomingConsignmentNo = Clip(job:Incoming_Consignment_Number)
            !Battery
            If Clip(sql:Battery) = 1
                sbj:Battery = 'Y'
            Else ! If Clip(sql:Battery) = 1
                sbj:Battery = 'N'
            End ! If Clip(sql:Battery) = 1
            !Battery Cover
            If CLip(sql:BatterCover) = 1
                sbj:BatteryCover = 'Y'
            Else ! If Clip(sql:Battery) = 1
                sbj:BatteryCover = 'N'
            End ! If Clip(sql:Battery) = 1
            !Charger
            If Clip(sql:Charger) = 1
                sbj:Charger = 'Y'
            Else ! If Clip(sql:Battery) = 1
                sbj:Charger = 'N'
            End ! If Clip(sql:Battery) = 1
            !Memory Card
            If Clip(sql:MemoryCard) = 1
                sbj:MemoryCard = 'Y'
            Else ! If Clip(sql:Battery) = 1
                sbj:MemoryCard = 'N'
            End ! If Clip(sql:Battery) = 1
            !Couresty Phone Allocated
            sbj:CPAllocated = Clip(SQL:CPAllocated)
            !CP Issue Date
            If local:FoundHandset
                sbj:CPIssueDate_DATE = hist:EntryDate
            End ! If local:FoundHandset
            !CP Non Issue Reason
            If Upper(Clip(SQL:CPAllocated)) = 'N'
                sbj:CPNonIssueReason = Clip(sql:CPIssueReason)
            End ! If local:FoundHandset
            If local:FoundHandset
                !Courtesy Phone IMEI
                sbj:CPIMEI = Clip(shan:IMEINumber)
                !Courtest Phone Make
                sbj:CPManufacturer = Clip(shan:Manufacturer)
                !Courtesy Phone Model
                sbj:CPModelNo = Clip(shan:ModelNumber)
                !Stock Out Flag
                if sql:CPAllocated = 'S'
                    sbj:StockOut = 'Y'
                end
                !Expected Return Date
                sbj:ExpectedReturnDate_DATE = shan:ReturnDate
                !Deposit Value
                sbj:DepositValue = sql:DepositValue
                !SID Courtesy Phone Charge
                if local:FoundWebJobEx = 1
                    sbj:SIDCPCharge = webex:CPChargeValue
                end
                !Oscar Courtesy Phone Charge
                if local:FoundOscarExt = 1
                    sbj:OscarCPCharge = osce:CPChargeAmt
                end
                !SID SKU Code
                if local:FoundWebJobEx = 1
                    sbj:SIDSKUCode = webex:CPChargeSKUCode
                end
                !Oscar SKU Code
                if local:FoundOscarExt = 1
                    sbj:OscarSKUCode = Clip(osce:CPChargeSKU)
                end
            End ! If local:HandsetFound

            !Value Segment
            sbj:ValueSegment = Clip(sql:ValueSegment)
            !Exchange Date
            if local:FoundCCExchange = 1
                sbj:ExchangeDate_DATE = ccex:ExchangeDate
            end
            !Exchange Delivery Type
            sbj:ExchangeDeliveryType = Clip(local:TimeSlotName)

            !Intermittent Timeframe
            if local:FoundWebJobEx = 1
                sbj:InterTimeFrame = Clip(webex:InterTimeFrame)
            end
            !Key Lock Password
            sbj:PhoneLock = Clip(sql:Phone_Lock)
            !Service Charge
            if local:FoundWebJobEx = 1
                sbj:ServiceCharge = webex:ServiceCharge
            end
            !Delivery Charge
            if local:FoundCCExchange = 1
                sbj:DeliveryCharge = ccex:DeliveryCharge
            end
            !BER Charge
            if local:FoundWebJobEx = 1
                sbj:BERCharge = webex:BERCharge
            end
            !Cancellation Reason
            if local:FoundWebJobEx = 1
                sbj:CancelReason = Clip(webex:CancelReason)
            end

            !Date Status Changed to 796 BER BOOKING ATTEMPT
            sbj:StatusChangeTo796DateTime_DATE = Deformat(Clip(local:Status796Date), @d06b)
            sbj:StatusChangeTo796DateTime_TIME = Deformat(Clip(local:Status796Time), @t01b)
            !Date Status Changed to 794 PENDING JOB - EXPIRED
            sbj:StatusChangeTo794DateTime_DATE = Deformat(Clip(local:Status794Date), @d06b)
            sbj:StatusChangeTo794DateTime_TIME = Deformat(Clip(local:Status794Time), @t01b)
            !Date Status Changed to 102 TAC CODE MISMATCH
            sbj:StatusChangeTo102DateTime_DATE = Deformat(Clip(local:Status102Date), @d06b)
            sbj:StatusChangeTo102DateTime_TIME = Deformat(Clip(local:Status102Time), @t01b)
            !Date Status Changed to 104 TAC CODE FAILURE
            sbj:StatusChangeTo104DateTime_DATE = Deformat(Clip(local:Status104Date), @d06b)
            sbj:StatusChangeTo104DateTime_TIME = Deformat(Clip(local:Status104Time), @t01b)
            !Date Status Changed to 795 CUSTOMER CANCELLED
            sbj:StatusChangeTo795DateTime_DATE = Deformat(Clip(local:Status795Date), @d06b)
            sbj:StatusChangeTo795DateTime_TIME = Deformat(Clip(local:Status795Time), @t01b)
            !Date Status Changed to 112 IN-STORE EXCHANGE REQUEST
            sbj:StatusChangeTo112DateTime_DATE = Deformat(Clip(local:Status112Date), @d06b)
            sbj:StatusChangeTo112DateTime_TIME = Deformat(Clip(local:Status112Time), @t01b)
            !Date Status Changed to 113 EXTERNAL EXCHANGE REQUEST
            sbj:StatusChangeTo113DateTime_DATE = Deformat(Clip(local:Status113Date), @d06b)
            sbj:StatusChangeTo113DateTime_TIME = Deformat(Clip(local:Status113Time), @t01b)
            !Date Status Changed to 931 EXCHANGE ARRIVED IN STORE
            sbj:StatusChangeTo931DateTime_DATE = Deformat(Clip(local:Status931Date), @d06b)
            sbj:StatusChangeTo931DateTime_TIME = Deformat(Clip(local:Status931Time), @t01b)
            !Date Status Changed to 941 EXCHANGE DEVICE ISSUED
            sbj:StatusChangeTo941DateTime_DATE = Deformat(Clip(local:Status941Date), @d06b)
            sbj:StatusChangeTo941DateTime_TIME = Deformat(Clip(local:Status941Time), @t01b)
            !Date Status Changed to 101 SID BOOKING INITIATED
            sbj:StatusChangeTo101DateTime_DATE = Deformat(Clip(local:Status101Date), @d06b)
            sbj:StatusChangeTo101DateTime_TIME = Deformat(Clip(local:Status101Time), @t01b)
            !Date Status Changed to 114 AWAITING DESPATCH
            sbj:StatusChangeTo114DateTime_DATE = Deformat(Clip(local:Status114Date), @d06b)
            sbj:StatusChangeTo114DateTime_TIME = Deformat(Clip(local:Status114Time), @t01b)

            if local:FoundWebJobEx = 1
                if webex:DiagAvailable = 1
                    sbj:DiagnosisAvailable = 'Y'
                else
                    sbj:DiagnosisAvailable = 'N'
                end
                if webex:KnownIssueID > 0
                    sbj:KIReference = webex:KnownIssueID
                end
            end

            !Date Status Changed to 250 KNOWN ISSUE RESOLVED
            sbj:StatusChangeTo250DateTime_DATE = Deformat(Clip(local:Status250Date), @d06b)
            sbj:StatusChangeTo250DateTime_TIME = Deformat(Clip(local:Status250Time), @t01b)

            !Date Status Changed to 251 DIAGNOSIS INITIATED
            sbj:StatusChangeTo251DateTime_DATE = Deformat(Clip(local:Status251Date), @d06b)
            sbj:StatusChangeTo251DateTime_TIME = Deformat(Clip(local:Status251Time), @t01b)

            !Date Status Changed to 252 DIAGNOSIS RESOLUTION
            sbj:StatusChangeTo252DateTime_DATE = Deformat(Clip(local:Status252Date), @d06b)
            sbj:StatusChangeTo252DateTime_TIME = Deformat(Clip(local:Status252Time), @t01b)

            !Date Status Changed to 248 IMEI UNAVAILABLE
            sbj:StatusChangeTo248DateTime_DATE = Deformat(Clip(local:Status248Date), @d06b)
            sbj:StatusChangeTo248DateTime_TIME = Deformat(Clip(local:Status248Time), @t01b)

            !Date Status Changed to 106 TAC MISMATCH � REDIAGNOSE
            sbj:StatusChangeTo106DateTime_DATE = Deformat(Clip(local:Status106Date), @d06b)
            sbj:StatusChangeTo106DateTime_TIME = Deformat(Clip(local:Status106Time), @t01b)

            if local:FoundWebJobEx = 1
                sbj:KnownIssueResolutionCode = Clip(Clip(webex:KIResolutionCode) & ' ' & Clip(webex:KIResolutionName))
                sbj:DiagnosticResolutionCode = Clip(Clip(webex:DiagResolutionCode) & ' ' & Clip(webex:DiagResolutionName))
            end

            If Clip(sql:EmailReq) = 1
                sbj:EmailRequested = 'Y'
            Else
                sbj:EmailRequested = 'N'
            End

            If Clip(sql:SMSReq) = 1
                sbj:SMSRequested = 'Y'
            Else
                sbj:SMSRequested = 'N'
            End

            sbj:SMSAlertNo = Clip(sql:SMSAlertNo)

            if local:FoundWebRet = 1
                sbj:ReturnReasonCode = Clip(Clip(webr:RetReasonCode) & ' ' & Clip(webr:RetReasonName))
                sbj:ExceptionReturnCode = Clip(webr:ExceptionCode)
                if local:FoundReturnCodes = 1
                    sbj:ExceptionReturnCodeIssuedBy = Clip(retc:UserName)
                end
                sbj:ReturnExceptionManagerName = Clip(webr:ManagersName)
                sbj:ReturnExceptionManagerID = Clip(webr:ManagersID)
                sbj:ReturnDescription = Clip(webr:ReturnDescription)
            end

            if local:FoundWebJobEx = 1
                If Clip(webex:ManualIMEIEntry) = 1
                    sbj:IMEIIntervention = 'Y'
                Else
                    sbj:IMEIIntervention = 'N'
                End

                If Clip(webex:ManualCTNEntry) = 1
                    sbj:CTNIntervention = 'Y'
                Else
                    sbj:CTNIntervention = 'N'
                End
            end

            !Date Status Changed to 121 RETURN JOB BOOKED
            sbj:StatusChangeTo121DateTime_DATE = Deformat(Clip(local:Status121Date), @d06b)
            sbj:StatusChangeTo121DateTime_TIME = Deformat(Clip(local:Status121Time), @t01b)

            !Date Status Changed to 120 7 DAY REPLACEMENT
            sbj:StatusChangeTo120DateTime_DATE = Deformat(Clip(local:Status120Date), @d06b)
            sbj:StatusChangeTo120DateTime_TIME = Deformat(Clip(local:Status120Time), @t01b)

            ! Repair Admin Charge
            if local:FoundWebJobEx = 1
                sbj:RepairAdminCharge = webex:RepairAdminCharge
            end
 
            ! Existing Insurance Customer (Y or N)
            if Instring('INSURANCE', Clip(Upper(sql:ValueSegment)), 1, 1) = 0
                sbj:ExistingInsuranceCustomer = 'N'
            else
                sbj:ExistingInsuranceCustomer = 'Y'
            end
            
            ! Original Sales Agent
            if local:FoundCTNSegment = 1
                sbj:OriginalSalesAgent = Clip(ctseg:OriginalSalesAgent)
            end

            ! Oracle Code
            sbj:OracleCode = Clip(local:OracleCode)

            ! Software Version Up To Date
            if local:FoundWebJobEx = 1
                sbj:SoftwareVersionUpToDate = Clip(webex:SoftwareUpToDate)
            end

            !Date Status Changed to 109 CUSTOMER UPDATING SOFTWARE
            sbj:StatusChangeTo109DateTime_DATE = Deformat(Clip(local:Status109Date), @d06b)
            sbj:StatusChangeTo109DateTime_TIME = Deformat(Clip(local:Status109Time), @t01b)

            !Date Status Changed to 260 INSURANCE CLAIM PROCESSED
            sbj:StatusChangeTo260DateTime_DATE = Deformat(Clip(local:Status260Date), @d06b)
            sbj:StatusChangeTo260DateTime_TIME = Deformat(Clip(local:Status260Time), @t01b)

            ! COM Ref, No exception date, diagnostic data fields
            if local:FoundWebJobEx = 1
                sbj:COMRef = Clip(webex:COMRef)
                sbj:RevisedTurnaroundExceptionDate_DATE = webex:NoExceptionDate
                sbj:DiagnosticFieldName = clip(webex:DiagFieldName)
                sbj:DiagnosticFieldText = clip(webex:DiagFieldText)
            end

            if local:FoundWebRet = 1
                sbj:OtherReturnOrigin = clip(webr:OtherRetOrigin)
            end

            !Date Status Changed to 910 TRACKING DATA AVAILABLE
            sbj:StatusChangeTo910DateTime_DATE = Deformat(Clip(local:Status910Date), @d06b)
            sbj:StatusChangeTo910DateTime_TIME = Deformat(Clip(local:Status910Time), @t01b)

            if local:FoundWebJobEx = 1
                ! CTN Intervention User Name
                sbj:CTNInterventionUserName = clip(webex:CTNUserName)
                ! CTN Intervention Reason For Escalation
                sbj:CTNInterventionEscalationReason = clip(webex:CTNEscalationReason)
                ! CTN Intervention Original Value Segment
                sbj:CTNInterventionOriginalValueSegment = clip(webex:OriginalValueSegment)

                ! SID Lite Booking
                if Clip(webex:SIDVersion) = 'L'
                    sbj:SIDLiteBooking = 'Y'
                else
                    sbj:SIDLiteBooking = 'N'
                end
            end

            !Date Status Changed to 119 7 DAY EXCHANGE
            sbj:StatusChangeTo119DateTime_DATE = Deformat(Clip(local:Status119Date), @d06b)
            sbj:StatusChangeTo119DateTime_TIME = Deformat(Clip(local:Status119Time), @t01b)

            if tmp:InsertSQL = true
                Add(SBJobs)
            else
                Put(SBJobs)
            end

            Access:SIDEXUPD.DeleteRecord(0)
            Set(seu:SBJobNoKey)

        end

        CountJobs# += 1

    End ! End JOBS Loop

    Prog.ProgressFinish()

    If tmp:WriteSQL = false
        Close(ExportFile)
    Else
        Close(SBJobs)
    End

    Close(SQLFile)
    Close(HistoryFile)
    Close(HandsetFile)
    Close(WebJobExFile)
    Close(WebRetFile)
    Close(JobTxtFile)
    Close(ReturnCodesFile)
    Close(CCExchangeFile)
    Close(TimeSlotFile)
    Close(CTNSegmentFile)
    Close(OscarExtFile)

    If f:Mode = 0
        Beep(Beep:SystemAsterisk)  ;  Yield()
        Case Message('Export Completed.'&|
            '|'&|
            '|Jobs Exported: ' & Clip(CountJobs#) & '.','ServiceBase 2000',|
                       icon:Asterisk,'&OK',1,1) 
            Of 1 ! &OK Button
        End!Case Message
    End ! If f:Auto <> True

    If f:Mode = 2 or f:Mode = 3
        ! As the tmp:ExportFile is returned from this procedure, send back to the
        ! caller the number of jobs exported. The text export file name is not needed
        ! or used for the SQL options.
        tmp:ExportFile = 'Exported: ' & Clip(CountJobs#) & ' to the SQL server.'
    End
Prog:nextrecord      routine
    Yield()
    prog:recordsprocessed += 1
    prog:recordsthiscycle += 1
    If prog:percentprogress < 100
        prog:percentprogress = (prog:recordsprocessed / prog:recordstoprocess)*100
        If prog:percentprogress > 100
            prog:percentprogress = 100
        End
        If prog:percentprogress <> prog:thermometer then
            prog:thermometer = prog:percentprogress

Omit('***',ClarionetUsed=0)
            If ClarionetServer:Active()
                cnprog:thermometer = prog:thermometer
            Else ! If ClarionetServer:Active()
***
                ?prog:pcttext{prop:text} = format(prog:percentprogress,@n3) & '% Completed'
Omit('***',ClarionetUsed=0)
            End ! If ClarionetServer:Active()
***
        End
    End
    Display()

prog:cancelcheck         routine
    cancel# = 0
    prog:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:Cancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','Cancel Pressed',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
            Of 1 ! &Yes Button
                prog:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


prog:Finish         routine
Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
        CNprog:thermometer = 100
    Else ! If ClarionetServer:Active()
***
        prog:thermometer = 100
        ?prog:pcttext{prop:text} = '100% Completed'
        display()
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'SIDExtract',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:StartDate',tmp:StartDate,'SIDExtract',1)
    SolaceViewVars('tmp:EndDate',tmp:EndDate,'SIDExtract',1)
    SolaceViewVars('tmp:ExportFile',tmp:ExportFile,'SIDExtract',1)
    SolaceViewVars('tmp:SavePath',tmp:SavePath,'SIDExtract',1)
    SolaceViewVars('tmp:Owner',tmp:Owner,'SIDExtract',1)
    SolaceViewVars('tmp:HandsetOwner',tmp:HandsetOwner,'SIDExtract',1)
    SolaceViewVars('tmp:StatusOwner',tmp:StatusOwner,'SIDExtract',1)
    SolaceViewVars('tmp:LocationOwner',tmp:LocationOwner,'SIDExtract',1)
    SolaceViewVars('tmp:CPMOwner',tmp:CPMOwner,'SIDExtract',1)
    SolaceViewVars('tmp:WriteSQL',tmp:WriteSQL,'SIDExtract',1)
    SolaceViewVars('tmp:InsertSQL',tmp:InsertSQL,'SIDExtract',1)
    SolaceViewVars('tmp:MSSQLConnection',tmp:MSSQLConnection,'SIDExtract',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate:Prompt;  SolaceCtrlName = '?tmp:StartDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate;  SolaceCtrlName = '?tmp:StartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate:Prompt;  SolaceCtrlName = '?tmp:EndDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate;  SolaceCtrlName = '?tmp:EndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('SIDExtract')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'SIDExtract')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:StartDate:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDSTATS.Open
  Relate:DEFSIDEX.Open
  Relate:EXCHANGE.Open
  Relate:SIDEXUPD.Open
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:JOBNOTES.UseFile
  Access:CARISMA.UseFile
  SELF.FilesOpened = True
  ! Modes
  
  ! f:Mode = 0 - Extract with GUI, exports to text file
  ! f:Mode = 1 - Extracts without GUI (last X days from default), exports to text file
  ! f:Mode = 2 - Extracts without GUI (last X days from default), exports to MSSQL table (Changed from last 30 days at Nick's request 15/05/2008 GK)
  ! f:Mode = 3 - Extracts without GUI (New / Changed Jobs), exports to MSSQL table
  
  ! GK 13/06/2007 TrackerBase 8830 - SID Extract SQL interface (Modes 2 and 3)
  ! Mode 3 uses the SIDEXUPD table. This table is written to in sb2wilfred.exe and wilfredtosb.exe
  ! when a new job is sent to wilfred or when we receive a job update back.
  
  tmp:StartDate = Today()
  tmp:EndDate = Today()
  
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  SELF.SetAlerts()
  tmp:WriteSQL = false
  
  case f:Mode
      of 1
          Set(DEFSIDEX,0)
          Access:DEFSIDEX.Next()
          tmp:StartDate = Today() - defe:ExportDays
          tmp:EndDate = Today()
          Display()
          Post(Event:Accepted,?OK)
          Post(Event:CloseWindow)
  
      of 2
          tmp:WriteSQL = true
          Set(DEFSIDEX,0)
          Access:DEFSIDEX.Next()
          tmp:StartDate = Today() - defe:ExportDays
          tmp:EndDate = Today()
          Display()
          Post(Event:Accepted,?OK)
          Post(Event:CloseWindow)
  
      of 3
          0{Prop:Hide} = true
          tmp:WriteSQL = true
          Display()
          Post(Event:Accepted,?OK)
          Post(Event:CloseWindow)
  
  End
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDSTATS.Close
    Relate:DEFSIDEX.Close
    Relate:EXCHANGE.Close
    Relate:SIDEXUPD.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'SIDExtract',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      Do Export
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'SIDExtract')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Prog.ProgressSetup        Procedure(Long func:Records)
Code
    prog:SkipRecords = 0

Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(CN:ProgWindow)
    Else ! If ClarionetServer:Active()
***
        Open(ProgWindow)
        Prog.ResetProgress(func:Records)
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
Code
    prog:recordspercycle         = 25
    prog:recordsprocessed        = 0
    prog:percentprogress         = 0
    prog:thermometer    = 0
    prog:recordstoprocess    = func:Records

Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        ?prog:pcttext{prop:text} = '0% Completed'
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

Prog.InsideLoop         Procedure()
Code

Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Do Prog:NextRecord
        Do Prog:CancelCheck
        If Prog:Cancel = 1
            Return 1
        End !If Prog:Cancel = 1
        Return 0
Omit('***',ClarionetUsed=0)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
Code
Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        ?prog:UserString{prop:Text} = Clip(func:String)
        Display()
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

Prog.ProgressFinish     Procedure()
Code
    Do Prog:Finish
Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(CN:progwindow)
    Else ! If ClarionetServer:Active()
***
        close(progwindow)
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

