

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03038.INC'),ONCE        !Local module procedure declarations
                     END








Stock_Value_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
save_stm_id          USHORT,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_sto_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
stock_queue_temp     QUEUE,PRE(stoque)
ref_number           LONG
part_number          STRING(30)
description          STRING(30)
Manufacturer         STRING(30)
                     END
site_location_temp   STRING(30)
count_temp           STRING(9)
average_price_temp   REAL
count_average_temp   REAL
job_count_temp       REAL
total_jobs_temp      REAL
Manufacturer_Temp    STRING(30)
total_stock_temp     STRING(12)
sale_value_temp      REAL
purchase_value_temp  REAL
purchase_value_total_temp REAL
sale_value_total_temp REAL
total_stock_total_temp STRING(12)
location_temp        STRING(30)
tmp:PrintedBy        STRING(60)
line_cost_temp       REAL
line_cost_total_temp REAL
unit_cost_total_temp REAL
quantity_total_temp  LONG
line_cost_man_temp   REAL
unit_cost_man_temp   REAL
quantity_man_temp    LONG
tmp:firstmodel       STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(LOCATION)
                     END
Report               REPORT('Stock Value Report'),AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,781,7521,2094),USE(?unnamed)
                         STRING('Inc. Accessories:'),AT(4948,313),USE(?String22),TRN,FONT(,8,,)
                         STRING(@s40),AT(5885,313),USE(GLO:Select2),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(5885,469),USE(GLO:Select3),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Suppress Zeros:'),AT(4948,469),USE(?String22:3),TRN,FONT(,8,,)
                         STRING(@s40),AT(5885,156),USE(GLO:Select1),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Site Location:'),AT(4948,156),USE(?String22:2),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(4948,833),USE(?String27),TRN,FONT(,8,,)
                         STRING('Page Number:'),AT(4948,1250),USE(?String59),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,1250),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6198,1250),USE(?String34),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6458,1250,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s60),AT(5885,833),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Printed:'),AT(4948,1042),USE(?ReportDatePrompt),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5885,1042),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,229),USE(?DetailBand)
                           STRING(@s25),AT(156,0),USE(sto:Part_Number),TRN,FONT(,7,,)
                           STRING(@s25),AT(1510,0),USE(sto:Description),TRN,FONT(,7,,)
                           STRING(@s30),AT(2865,0,1094,156),USE(tmp:firstmodel),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@S6),AT(4063,0),USE(sto:Minimum_Level),TRN,RIGHT,FONT(,7,,)
                           STRING(@S6),AT(4531,0),USE(sto:Reorder_Level),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@N10.2),AT(4948,0),USE(sto:Sale_Cost),TRN,RIGHT,FONT(,7,,)
                           STRING(@N10.2),AT(5469,0),USE(sto:Retail_Cost),TRN,RIGHT,FONT(,7,,)
                           STRING(@S8),AT(5990,0),USE(sto:Quantity_Stock),TRN,RIGHT,FONT(,7,,)
                           STRING(@N10.2),AT(6354,0),USE(sto:Purchase_Cost),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(6865,0),USE(line_cost_temp),TRN,RIGHT,FONT(,7,,)
                         END
Totals                   DETAIL,AT(,,,458),USE(?totals)
                           LINE,AT(208,52,7083,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Lines:'),AT(208,104),USE(?String26),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(5573,104),USE(quantity_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@n10.2),AT(6771,104),USE(line_cost_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@s9),AT(2083,104),USE(count_temp),TRN,FONT(,8,,FONT:bold)
                         END
ManufacturerTotal        DETAIL,WITHPRIOR(2),AT(,,,229),USE(?unnamed:6)
                           STRING(@s8),AT(5573,52),USE(quantity_man_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@n10.2),AT(6771,52),USE(line_cost_man_temp),TRN,FONT(,8,,FONT:bold)
                           STRING(@s30),AT(156,52),USE(Manufacturer_Temp,,?Manufacturer_Temp:2),TRN,FONT(,8,,FONT:bold)
                         END
ManufacturerHeader       DETAIL,WITHNEXT(2),AT(,,,229),USE(?unnamed:5)
                           STRING(@s30),AT(156,52),USE(Manufacturer_Temp),TRN,FONT(,8,,FONT:bold)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('STOCK VALUE REPORT'),AT(4271,0,3177,260),USE(?String20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?String19:2),TRN,FONT(,9,,)
                         STRING('Part Number'),AT(1510,2083),USE(?String44),TRN,FONT(,7,,FONT:bold)
                         STRING('Trade'),AT(5208,2083),USE(?String44:3),TRN,FONT(,7,,FONT:bold)
                         STRING('If Below'),AT(4010,2135),USE(?String24:2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Up To'),AT(4531,2135),USE(?String24:3),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('First Model No'),AT(2865,2083),USE(?String44:7),TRN,FONT(,7,,FONT:bold)
                         STRING('Purchase'),AT(6438,2083),USE(?String44:6),TRN,FONT(,7,,FONT:bold)
                         STRING('In Stock'),AT(6042,2083),USE(?String44:5),TRN,FONT(,7,,FONT:bold)
                         STRING('Retail'),AT(5719,2083),USE(?String44:4),TRN,FONT(,7,,FONT:bold)
                         STRING('Part Number'),AT(156,2083),USE(?String44:2),TRN,FONT(,7,,FONT:bold)
                         STRING('Order'),AT(4063,2031),USE(?String45),TRN,FONT(,7,,FONT:bold)
                         STRING('Level Make '),AT(4427,2031),USE(?String24),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Line Cost'),AT(6927,2083),USE(?String25),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Stock_Value_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  CASE MESSAGE(CPCS:AskPrvwDlgText,CPCS:AskPrvwDlgTitle,ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
    OF BUTTON:Yes
      PreviewReq = True
    OF BUTTON:No
      PreviewReq = False
    OF BUTTON:Cancel
       DO ProcedureReturn
  END
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:LOCATION.Open
  Relate:DEFAULTS.Open
  Access:STOCK.UseFile
  Access:USERS.UseFile
  Access:STOMODEL.UseFile
  
  
  RecordsToProcess = RECORDS(LOCATION)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(LOCATION,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      RecordsToProcess = Records(glo:Queue)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        setcursor(cursor:wait)
        
        Loop x# = 1 To Records(glo:Queue)
            Get(glo:Queue,x#)
            save_sto_id = access:stock.savefile()
            access:stock.clearkey(sto:shelf_location_key)
            sto:location       = glo:select1
            sto:shelf_location = glo:pointer
            set(sto:shelf_location_key,sto:shelf_location_key)
            loop
                if access:stock.next()
                   break
                end !if
                if sto:location       <> glo:select1      |
                or sto:shelf_location <> glo:pointer      |
                    then break.  ! end if
                RecordsProcessed += 1
                Do DisplayProgress
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                If glo:select2 <> 'YES'
                    If sto:accessory = 'YES'
                        Cycle
                    End!If sto:accessory = 'YES'
                End!If glo:select2 = 'YES'
                If glo:select3 = 'YES'
                    If sto:quantity_stock = 0
                        Cycle
                    End!If sto:quantity_stock = 0
                End!If glo:select3 = 'YES'
                ! Start Change 2082 BE(16/10/03)
                IF (glo:select5 <> '') THEN
                    IF (sto:manufacturer <> CLIP(glo:select5)) THEN
                        CYCLE
                    END
                END
                ! End Change 2082 BE(16/10/03)
                stoque:ref_number   = sto:ref_number
                stoque:part_number  = sto:part_number
                stoque:description  = sto:description
                stoque:Manufacturer = sto:Manufacturer
                add(stock_queue_temp)
            end !loop
            access:stock.restorefile(save_sto_id)
        End!Loop x# = 1 To Records(glo:Queue)
        
        setcursor()
        line_cost_total_temp = 0
        unit_cost_total_temp = 0
        quantity_total_temp = 0
        ! Start Change 2082 BE(07/07/2003)
        line_cost_man_temp = 0
        unit_cost_man_temp = 0
        quantity_man_temp = 0
        Manufacturer_temp = ''
        ! End Change 2082 BE(07/07/2003)
        
        RecordsToProcess = Records(stock_queue_temp)
        
        !Group my manufacturer or part number?
        If glo:Select4 = 1
            Sort(stock_queue_temp,stoque:Manufacturer,stoque:part_number,stoque:description)
        Else !glo:Select4 = 1
            Sort(stock_queue_temp,stoque:part_number,stoque:description)
        End !glo:Select4 = 1
        
        Loop x# = 1 To Records(stock_queue_temp)
            Get(stock_queue_temp,x#)
        
                ! Start Change 2082 BE(07/07/2003)
            IF ((glo:Select4 = 1) AND (stoque:Manufacturer <> Manufacturer_temp)) THEN
                IF (Manufacturer_temp <> '') THEN
                    ! O/P Manufacturer Total
                    Print(rpt:ManufacturerTotal)
                END
                ! O/P Manufacturer Header
                Manufacturer_temp = stoque:Manufacturer
                Print(rpt:ManufacturerHeader)
                line_cost_man_temp = 0
                unit_cost_man_temp = 0
                quantity_man_temp = 0
            END
            ! End Change 2082 BE(07/07/2003)
        
            RecordsProcessed += 1
            Do DisplayProgress
            line_cost_temp = 0
            access:stock.clearkey(sto:ref_number_key)
            sto:ref_number = stoque:ref_number
            if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                line_cost_temp = sto:purchase_cost * sto:quantity_stock
                line_cost_total_temp += line_cost_temp
                unit_cost_total_temp += sto:purchase_cost
                quantity_total_temp += sto:quantity_stock
        
                ! Start Change 2082 BE(07/07/2003)
                line_cost_man_temp += line_cost_temp
                unit_cost_man_temp += sto:purchase_cost
                quantity_man_temp += sto:quantity_stock
                ! End Change 2082 BE(07/07/2003)
        
        !Get First Model
                tmp:FirstModel = ''
                save_stm_id = access:stomodel.savefile()
                access:stomodel.clearkey(stm:model_number_key)
                stm:ref_number   = sto:ref_number
                stm:manufacturer = sto:manufacturer
                set(stm:model_number_key,stm:model_number_key)
                loop
                    if access:stomodel.next()
                       break
                    end !if
                    if stm:ref_number   <> sto:ref_number      |
                    or stm:manufacturer <> sto:manufacturer      |
                        then break.  ! end if
                    tmp:FirstModel = stm:model_number
                    Break
                end !loop
                access:stomodel.restorefile(save_stm_id)
        
                Print(rpt:detail)
            end!if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
        End!Loop x# = 1 To Records(stock_queue_temp)
        If records(stock_queue_temp)
            ! Start Change 2082 BE(07/07/2003)
            IF (glo:Select4 = 1) THEN
                Print(rpt:ManufacturerTotal)
            END
            ! End Change 2082 BE(07/07/2003)
            Print(rpt:totals)
        End!If records(stock_queue_temp)
        
        
        
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(LOCATION,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:LOCATION.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Stock_Value_Report'
  END
  Report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Stock_Value_Report',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Stock_Value_Report',1)
    SolaceViewVars('save_stm_id',save_stm_id,'Stock_Value_Report',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Stock_Value_Report',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Stock_Value_Report',1)
    SolaceViewVars('save_sto_id',save_sto_id,'Stock_Value_Report',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Stock_Value_Report',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Stock_Value_Report',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Stock_Value_Report',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Stock_Value_Report',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Stock_Value_Report',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Stock_Value_Report',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Stock_Value_Report',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Stock_Value_Report',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Stock_Value_Report',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Stock_Value_Report',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Stock_Value_Report',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Stock_Value_Report',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Stock_Value_Report',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Stock_Value_Report',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Stock_Value_Report',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Stock_Value_Report',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Stock_Value_Report',1)
    SolaceViewVars('InitialPath',InitialPath,'Stock_Value_Report',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Stock_Value_Report',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Stock_Value_Report',1)
    SolaceViewVars('stock_queue_temp:ref_number',stock_queue_temp:ref_number,'Stock_Value_Report',1)
    SolaceViewVars('stock_queue_temp:part_number',stock_queue_temp:part_number,'Stock_Value_Report',1)
    SolaceViewVars('stock_queue_temp:description',stock_queue_temp:description,'Stock_Value_Report',1)
    SolaceViewVars('stock_queue_temp:Manufacturer',stock_queue_temp:Manufacturer,'Stock_Value_Report',1)
    SolaceViewVars('site_location_temp',site_location_temp,'Stock_Value_Report',1)
    SolaceViewVars('count_temp',count_temp,'Stock_Value_Report',1)
    SolaceViewVars('average_price_temp',average_price_temp,'Stock_Value_Report',1)
    SolaceViewVars('count_average_temp',count_average_temp,'Stock_Value_Report',1)
    SolaceViewVars('job_count_temp',job_count_temp,'Stock_Value_Report',1)
    SolaceViewVars('total_jobs_temp',total_jobs_temp,'Stock_Value_Report',1)
    SolaceViewVars('Manufacturer_Temp',Manufacturer_Temp,'Stock_Value_Report',1)
    SolaceViewVars('total_stock_temp',total_stock_temp,'Stock_Value_Report',1)
    SolaceViewVars('sale_value_temp',sale_value_temp,'Stock_Value_Report',1)
    SolaceViewVars('purchase_value_temp',purchase_value_temp,'Stock_Value_Report',1)
    SolaceViewVars('purchase_value_total_temp',purchase_value_total_temp,'Stock_Value_Report',1)
    SolaceViewVars('sale_value_total_temp',sale_value_total_temp,'Stock_Value_Report',1)
    SolaceViewVars('total_stock_total_temp',total_stock_total_temp,'Stock_Value_Report',1)
    SolaceViewVars('location_temp',location_temp,'Stock_Value_Report',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Stock_Value_Report',1)
    SolaceViewVars('line_cost_temp',line_cost_temp,'Stock_Value_Report',1)
    SolaceViewVars('line_cost_total_temp',line_cost_total_temp,'Stock_Value_Report',1)
    SolaceViewVars('unit_cost_total_temp',unit_cost_total_temp,'Stock_Value_Report',1)
    SolaceViewVars('quantity_total_temp',quantity_total_temp,'Stock_Value_Report',1)
    SolaceViewVars('line_cost_man_temp',line_cost_man_temp,'Stock_Value_Report',1)
    SolaceViewVars('unit_cost_man_temp',unit_cost_man_temp,'Stock_Value_Report',1)
    SolaceViewVars('quantity_man_temp',quantity_man_temp,'Stock_Value_Report',1)
    SolaceViewVars('tmp:firstmodel',tmp:firstmodel,'Stock_Value_Report',1)


BuildCtrlQueue      Routine







