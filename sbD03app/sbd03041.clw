

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03041.INC'),ONCE        !Local module procedure declarations
                     END








Third_Party_Failed_Returns PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
count_temp           LONG
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(TRDBATCH)
                       PROJECT(trb:ESN)
                       PROJECT(trb:Ref_Number)
                       JOIN(job:Ref_Number_Key,trb:Ref_Number)
                         PROJECT(job:Account_Number)
                         PROJECT(job:MSN)
                         PROJECT(job:Manufacturer)
                         PROJECT(job:Model_Number)
                         PROJECT(job:Unit_Type)
                       END
                     END
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(375,865,7521,1396),USE(?unnamed)
                         STRING('3rd Party Agent:'),AT(5000,0),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,0),USE(GLO:Select1),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(5885,208),USE(GLO:Select2),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Report Type:'),AT(5000,417),USE(?string22:3),TRN,FONT(,8,,)
                         STRING('Report Type'),AT(5885,417),USE(?Report_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Exchange Type'),AT(5885,625),USE(?Exchanged_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Exchanged Type:'),AT(5000,625),USE(?string22:4),TRN,FONT(,8,,)
                         STRING('Batch No:'),AT(5000,208),USE(?string22:2),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(5000,833),USE(?ExchangeType:2),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,833),USE(tmp:PrintedBy),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,1042),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,1042),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,1042),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,1250),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,1250),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,1250),USE(?String26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6354,1250,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,188),USE(?detailband)
                           STRING(@s15),AT(5208,0),USE(job:Account_Number),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(1823,0),USE(job:Manufacturer),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s16),AT(208,0),USE(trb:ESN),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s11),AT(1146,0),USE(job:MSN),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s20),AT(2969,0),USE(job:Model_Number),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s20),AT(4115,0),USE(job:Unit_Type),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s8),AT(6927,0),USE(trb:Ref_Number),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@d6),AT(6354,0),USE(jot:DateDespatched),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           LINE,AT(260,52,6979,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Lines:'),AT(260,104),USE(?String38),TRN,FONT(,8,,FONT:bold)
                           STRING(@n-14),AT(1667,104),USE(count_temp),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('3RD PARTY FAILED RETURNS'),AT(4063,0,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING('Unit Type'),AT(4115,2083),USE(?string25:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Sent'),AT(6333,2083),USE(?string25:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Job No'),AT(6990,2083),USE(?string25:4),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Account No'),AT(5208,2083),USE(?string25:5),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('I.M.E.I. Number'),AT(208,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(1146,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Manufacturer'),AT(1823,2083),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Model Number'),AT(2969,2083),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Third_Party_Failed_Returns')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:TRDBATCH.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:STANTEXT.Open
  Access:JOBS.UseFile
  Access:TRDPARTY.UseFile
  Access:JOBTHIRD.UseFile
  
  
  RecordsToProcess = RECORDS(TRDBATCH)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(TRDBATCH,'QUICKSCAN=on')
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(trb:Company_Batch_ESN_Key)
      Process:View{Prop:Filter} = |
      'UPPER(trb:Company_Name) = UPPER(GLO:Select1) AND (Upper(trb:status) = ' & |
      '''OUT'')'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        print# = 1
        
        Access:JOBTHIRD.Clearkey(jot:ThirdPartyKey)
        jot:ThirdPartyNumber    = trb:RecordNumber
        If Access:JOBTHIRD.Tryfetch(jot:ThirdPartyKey) = Level:Benign
            !Found
        Else! If Access:JOBTHIRD.Tryfetch(jot:ThirdPartyKey) = Level:Benign
            !Error
            Assert(0,'<13,10>Cannot Find JOBTHIRD Record.<13,10>')
        End! If Access:.Tryfetch(jot:ThirdPartyKey) = Level:Benign
        
        IF glo:select3 <> 'ALL'
            access:jobs.clearkey(job:ref_number_key)
            job:ref_number = trb:ref_number
            if access:jobs.tryfetch(job:ref_number_key) = Level:Benign
                If job:ThirdPartyDateDesp <> ''
                    If trb:turntime <> ''
                        If job:ThirdPartyDateDesp + trb:turntime => Today()
                            print# = 0
                        End!If JOB:Third_Party_Despatch_Date + trd:turnaround_time < Today()
                    Else!If trb:turntime <> ''
                        If job:ThirdPartyDateDesp+ trd:turnaround_time => Today()
                            print# = 0
                        End!If JOB:Third_Party_Despatch_Date + trd:turnaround_time < Today()
                    End!If trb:turntime <> ''
                End!If job:third_party_despatch_date <> ''
            end!if access:jobs.tryfetch(job:ref_number_key) = Level:Benign
        End!IF glo:select3 = 'ALL'
        
        Case glo:select4
            Of 1 !Exchanged Only
                If trb:exchanged <> 'YES'
                    print# = 0
                End!If trb:exchanged <> 'YES'
            Of 2 !Not Exchange Only
                If trb:exchanged = 'YES'
                    print# = 0
                End!If trb:exchanged = 'YES'
            Of 3
        End!Case glo:select4
        
        If print# = 1
            count_temp += 1
            Print(rpt:detail)
        End!If print# = 1
        
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(TRDBATCH,'QUICKSCAN=off').
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBTHIRD.Close
    Relate:STANTEXT.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'TRDBATCH')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','3RD PARTY FAILED RETURNS')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = '3RD PARTY FAILED RETURNS'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  access:trdparty.clearkey(trd:company_name_key)
  trd:company_name = glo:select1
  access:trdparty.tryfetch(trd:company_name_key)
  
  
  Settarget(Report)
  If glo:select3   = 'ALL'
      ?report_type{prop:text} = 'All Outstanding Units'
  Else!If glo:select3   = 'ALL'
      ?report_type{prop:text} = 'Overdue Returns'
  End!If glo:select3   = 'ALL'
  Case glo:select4
      Of 1
          ?exchanged_type{prop:text}  = 'Exchange Units Only'
      Of 2
          ?Exchanged_type{prop:text}  = 'Not Exchanged Units Only'
      Of 3
          ?Exchanged_type{prop:text}  = 'All Units'
  End!Case glo:select4
  Settarget()
  
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = '3RD PARTY FAILED RETURNS'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = '3RD PARTY FAILED RETURNS'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Third_Party_Failed_Returns'
  END
  report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Third_Party_Failed_Returns',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Third_Party_Failed_Returns',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Third_Party_Failed_Returns',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Third_Party_Failed_Returns',1)
    SolaceViewVars('count_temp',count_temp,'Third_Party_Failed_Returns',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Third_Party_Failed_Returns',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Third_Party_Failed_Returns',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Third_Party_Failed_Returns',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Third_Party_Failed_Returns',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Third_Party_Failed_Returns',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Third_Party_Failed_Returns',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Third_Party_Failed_Returns',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Third_Party_Failed_Returns',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Third_Party_Failed_Returns',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Third_Party_Failed_Returns',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Third_Party_Failed_Returns',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Third_Party_Failed_Returns',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Third_Party_Failed_Returns',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Third_Party_Failed_Returns',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Third_Party_Failed_Returns',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Third_Party_Failed_Returns',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Third_Party_Failed_Returns',1)
    SolaceViewVars('InitialPath',InitialPath,'Third_Party_Failed_Returns',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Third_Party_Failed_Returns',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Third_Party_Failed_Returns',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Third_Party_Failed_Returns',1)


BuildCtrlQueue      Routine







