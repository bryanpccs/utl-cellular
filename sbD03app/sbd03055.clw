

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03055.INC'),ONCE        !Local module procedure declarations
                     END








QA_Failed_Loan PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_aud_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
code_temp            BYTE
option_temp          BYTE
bar_code_string_temp CSTRING(21)
bar_code_temp        CSTRING(21)
qa_reason_temp       STRING(255)
engineer_temp        STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(LOAN_ALIAS)
                       PROJECT(loa_ali:Date_Booked)
                       PROJECT(loa_ali:ESN)
                       PROJECT(loa_ali:Job_Number)
                       PROJECT(loa_ali:Manufacturer)
                       PROJECT(loa_ali:Model_Number)
                       PROJECT(loa_ali:Ref_Number)
                     END
report               REPORT,AT(406,1021,7521,9802),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
endofreportbreak       BREAK(endofreport)
                         HEADER,AT(0,0,,583),USE(?unnamed)
                           STRING('Loan Unit'),AT(2615,0,2292,417),USE(?Third_Party_Repair),TRN,CENTER,FONT(,22,,FONT:bold)
                         END
detail                   DETAIL,AT(,,,9115),USE(?detailband)
                           STRING('Job No:'),AT(313,208),USE(?String2),TRN,FONT(,20,,FONT:bold,CHARSET:ANSI)
                           STRING(@s20),AT(2917,313,1771,208),USE(bar_code_temp),LEFT,FONT('C128 High 12pt LJ3',12,,FONT:regular,CHARSET:ANSI)
                           STRING(@s8),AT(4792,313),USE(loa_ali:Job_Number),TRN,LEFT,FONT(,12,,FONT:bold)
                           STRING('Unit Details:'),AT(313,2292),USE(?String2:2),TRN,FONT(,20,,FONT:bold,CHARSET:ANSI)
                           STRING('Date Booked:'),AT(2917,2292),USE(?String6),TRN,FONT(,12,,FONT:bold)
                           STRING(@d6b),AT(4792,2292),USE(loa_ali:Date_Booked),LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Manufacturer:'),AT(2917,2604),USE(?String6:2),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,2604),USE(loa_ali:Manufacturer),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Model Number:'),AT(2917,2969),USE(?String6:3),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,2969),USE(loa_ali:Model_Number),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('E.S.N. / I.M.E.I.'),AT(2917,3333),USE(?String6:5),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,3333),USE(loa_ali:ESN),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING(@s8),AT(4792,3646),USE(loa_ali:Ref_Number),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Unit Number:'),AT(2917,3646),USE(?String6:4),TRN,FONT(,12,,FONT:bold)
                           STRING(@s60),AT(4792,5469),USE(tmp:PrintedBy),FONT(,12,,)
                           STRING('Inspector:'),AT(2917,5469),USE(?String6:11),TRN,FONT(,12,,FONT:bold)
                           STRING('Failure Reason:'),AT(2917,5781),USE(?String6:12),TRN,FONT(,12,,FONT:bold)
                           TEXT,AT(4792,5781,2552,2083),USE(glo:Notes_Global),FONT(,12,,)
                           STRING('QA Details:'),AT(313,5469),USE(?String2:4),TRN,FONT(,20,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       FORM,AT(396,479,7521,10552),USE(?unnamed:3)
                         STRING('QA FAILURE'),AT(2177,52,3177,417),USE(?string20),TRN,CENTER,FONT(,24,,FONT:bold)
                         BOX,AT(104,521,7292,9844),USE(?Box1),COLOR(COLOR:Black)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('QA_Failed_Loan')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:LOAN_ALIAS.Open
  Relate:AUDIT.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE_ALIAS.Open
  
  
  RecordsToProcess = RECORDS(LOAN_ALIAS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(LOAN_ALIAS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(loa_ali:Ref_Number_Key)
      Process:View{Prop:Filter} = |
      'loa_ali:Ref_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(rpt:detail)
        END
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(LOAN_ALIAS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDIT.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE_ALIAS.Close
    Relate:LOAN_ALIAS.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'LOAN_ALIAS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','QA FAILED NOTE')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'QA FAILED NOTE'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  !barcode bit
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = CLip(loa_ali:job_number)
  sequence2(code_temp,option_temp,bar_code_string_temp,bar_code_temp)
  
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'QA FAILED NOTE'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='QA_Failed_Loan'
  END
  report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'QA_Failed_Loan',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'QA_Failed_Loan',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'QA_Failed_Loan',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'QA_Failed_Loan',1)
    SolaceViewVars('save_aud_id',save_aud_id,'QA_Failed_Loan',1)
    SolaceViewVars('LocalRequest',LocalRequest,'QA_Failed_Loan',1)
    SolaceViewVars('LocalResponse',LocalResponse,'QA_Failed_Loan',1)
    SolaceViewVars('FilesOpened',FilesOpened,'QA_Failed_Loan',1)
    SolaceViewVars('WindowOpened',WindowOpened,'QA_Failed_Loan',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'QA_Failed_Loan',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'QA_Failed_Loan',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'QA_Failed_Loan',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'QA_Failed_Loan',1)
    SolaceViewVars('PercentProgress',PercentProgress,'QA_Failed_Loan',1)
    SolaceViewVars('RecordStatus',RecordStatus,'QA_Failed_Loan',1)
    SolaceViewVars('EndOfReport',EndOfReport,'QA_Failed_Loan',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'QA_Failed_Loan',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'QA_Failed_Loan',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'QA_Failed_Loan',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'QA_Failed_Loan',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'QA_Failed_Loan',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'QA_Failed_Loan',1)
    SolaceViewVars('InitialPath',InitialPath,'QA_Failed_Loan',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'QA_Failed_Loan',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'QA_Failed_Loan',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'QA_Failed_Loan',1)
    SolaceViewVars('code_temp',code_temp,'QA_Failed_Loan',1)
    SolaceViewVars('option_temp',option_temp,'QA_Failed_Loan',1)
    SolaceViewVars('bar_code_string_temp',bar_code_string_temp,'QA_Failed_Loan',1)
    SolaceViewVars('bar_code_temp',bar_code_temp,'QA_Failed_Loan',1)
    SolaceViewVars('qa_reason_temp',qa_reason_temp,'QA_Failed_Loan',1)
    SolaceViewVars('engineer_temp',engineer_temp,'QA_Failed_Loan',1)


BuildCtrlQueue      Routine







