

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03019.INC'),ONCE        !Local module procedure declarations
                     END


Days_Between_Routine PROCEDURE  (f_start_date,f_end_date,f_days) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Days_Between_Routine')      !Add Procedure to Log
  end


    Set(defaults)
    access:defaults.next()
    f_days = 0
    Loop
        f_start_date += 1
        If def:include_saturday <> 'YES'
            If (f_start_date) % 7 = 6
                Cycle
            End!If (f_start_date + count#) % 7 = 6
        End!If def:include_saturday <> 'YES'
        If def:include_sunday <> 'YES'
            If (f_start_date) % 7 = 0
                Cycle
            End!If (f_start_date + count#) % 7 = 6
        End!If def:include_saturday <> 'YES'
        f_days += 1
        If f_start_date > = f_end_date
            Break
        End!If f_start_date > = f_end_date
    End!Loop


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Days_Between_Routine',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
