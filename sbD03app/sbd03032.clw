

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03032.INC'),ONCE        !Local module procedure declarations
                     END








Retail_Sales_Valuation_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_ret_id          USHORT,AUTO
save_res_id          USHORT,AUTO
retail_queue         QUEUE,PRE(retque)
User_Code            STRING(3)
type                 STRING(3)
record_number        LONG
                     END
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
item_cost_temp       REAL
line_cost_temp       REAL
quantity_total_temp  LONG
line_cost_total_temp REAL
item_cost_total_temp REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(RETSTOCK)
                       PROJECT(res:Description)
                       PROJECT(res:Part_Number)
                       PROJECT(res:Quantity)
                     END
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,854,7521,1427),USE(?unnamed)
                         STRING('Date From:'),AT(5000,208),USE(?string22),TRN,FONT(,8,,)
                         STRING(@d6),AT(5781,208),USE(GLO:Select1),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Date To:'),AT(5000,417),USE(?string22:2),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(5000,625),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5833,625),USE(tmp:PrintedBy),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,833),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5781,833),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6458,833),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@d6),AT(5781,417),USE(GLO:Select2),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(5000,1042),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5833,1042),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6094,1042),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6302,1042,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,229),USE(?detailband)
                           STRING(@s30),AT(990,-10),USE(res:Part_Number),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s30),AT(2917,0),USE(res:Description),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n8),AT(4844,0),USE(res:Quantity),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(5885,0),USE(item_cost_temp),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(6708,0),USE(line_cost_temp),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s3),AT(208,0),USE(retque:User_Code),TRN,FONT(,8,,,CHARSET:ANSI)
                         END
Back_Orders_Title        DETAIL,AT(,,,271),USE(?unnamed:5)
                           STRING('BACK ORDERS'),AT(104,52),USE(?String26),TRN,FONT(,9,,FONT:bold)
                         END
Sales_Transactions_Title DETAIL,AT(,,,260),USE(?unnamed:6)
                           STRING('SALES TRANSACTIONS'),AT(104,52),USE(?String26:2),TRN,FONT(,9,,FONT:bold)
                         END
totals                   DETAIL,USE(?unnamed:7)
                           LINE,AT(4792,52,2604,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING(@n8),AT(4844,104),USE(quantity_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING(@n10.2),AT(6708,104),USE(line_cost_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING('Totals (Ex V.A.T.)'),AT(3750,104),USE(?String37),TRN,FONT(,8,,FONT:bold)
                           STRING(@n10.2),AT(5885,104),USE(item_cost_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('RETAIL SALES VALUATION REPORT'),AT(3854,0,3594,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('User Code'),AT(208,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(990,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(2917,2083),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Qty'),AT(5052,2083),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Item Cost '),AT(5906,2083),USE(?string25:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Line Cost'),AT(6771,2083),USE(?string25:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Retail_Sales_Valuation_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  CASE MESSAGE(CPCS:AskPrvwDlgText,CPCS:AskPrvwDlgTitle,ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
    OF BUTTON:Yes
      PreviewReq = True
    OF BUTTON:No
      PreviewReq = False
    OF BUTTON:Cancel
       DO ProcedureReturn
  END
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:RETSTOCK.Open
  Relate:DEFAULTS.Open
  Relate:USERS.Open
  Access:RETSALES.UseFile
  
  
  RecordsToProcess = RECORDS(RETSTOCK)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(RETSTOCK,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        setcursor(cursor:wait)
        save_ret_id = access:retsales.savefile()
        access:retsales.clearkey(ret:date_booked_key)
        ret:date_booked = glo:select1
        set(ret:date_booked_key,ret:date_booked_key)
        loop
            if access:retsales.next()
               break
            end !if
            if ret:date_booked > glo:select2      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            glo:pointer  = ret:who_booked
            Get(glo:Queue,glo:pointer)
            IF error()
                Cycle
            End!IF error()
        
            save_res_id = access:retstock.savefile()
            access:retstock.clearkey(res:part_number_key)
            res:ref_number  = ret:Ref_number
            set(res:part_number_key,res:part_number_key)
            loop
                if access:retstock.next()
                   break
                end !if
                if res:ref_number  <> ret:ref_number      |
                    then break.  ! end if
                retque:user_code    = ret:who_booked
                If res:despatched = 'YES'
                    retque:type = 'SAL'
                Else!If res:despatched = 'YES'
                    retque:type = 'BAK'
                End!If res:despatched = 'YES'
                retque:record_number    = res:record_number
                Add(retail_queue)
            end !loop
            access:retstock.restorefile(save_res_id)
        
        end !loop
        access:retsales.restorefile(save_ret_id)
        setcursor()
        
        Sort(retail_queue,retque:type,retque:user_code)
        
        back_order# = 0
        sales# = 0
        quantity_total_temp = 0
        line_cost_total_temp = 0
        item_cost_total_temp = 0
        
        Loop x# = 1 To Records(retail_queue)
            Get(retail_queue,x#)
            access:retstock.clearkey(res:record_number_key)
            res:record_number = retque:record_number
            If access:retstock.tryfetch(res:record_number_key) = Level:Benign
        
                If retque:type = 'BAK'
                    If back_order# = 0
                        Print(rpt:back_orders_title)
                        back_order# = 1
                        quantity_total_temp = 0
                        line_cost_total_temp = 0
                        item_cost_total_temp = 0
                    End!If back_order# = 0
                End!End!If retque:type = 'BAK'
        
                If retque:type = 'SAL'
                    If sales# = 0
                        Print(rpt:totals)
                        Print(rpt:sales_transactions_title)
                        sales# = 1
                        quantity_total_temp = 0
                        line_cost_total_temp = 0
                        item_cost_total_temp = 0
                    End!If back_order# = 0
                End!If retque:type = 'SAL'
                item_cost_temp  = res:item_cost
                line_cost_temp  = res:quantity * item_cost_temp
        
                quantity_total_temp += res:quantity
                item_cost_total_temp += res:item_cost
                line_cost_total_temp += (res:quantity * res:item_cost)
        
                Print(rpt:detail)
            end!If access:retstock.tryfetch(res:record_number_key) = Level:Benign
        
        End!Loop x# = 1 To Records(retail_queue)
        Print(rpt:totals)
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(RETSTOCK,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:RETSALES.Close
    Relate:USERS.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Retail_Sales_Valuation_Report'
  END
  report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Retail_Sales_Valuation_Report',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('save_ret_id',save_ret_id,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('save_res_id',save_res_id,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('retail_queue:User_Code',retail_queue:User_Code,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('retail_queue:type',retail_queue:type,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('retail_queue:record_number',retail_queue:record_number,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('InitialPath',InitialPath,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('item_cost_temp',item_cost_temp,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('line_cost_temp',line_cost_temp,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('quantity_total_temp',quantity_total_temp,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('line_cost_total_temp',line_cost_total_temp,'Retail_Sales_Valuation_Report',1)
    SolaceViewVars('item_cost_total_temp',item_cost_total_temp,'Retail_Sales_Valuation_Report',1)


BuildCtrlQueue      Routine







