

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03009.INC'),ONCE        !Local module procedure declarations
                     END








QA_Failure_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_aud_id          USHORT,AUTO
Reason_Queue_Temp    QUEUE,PRE(REAQUE)
Reason               STRING(60)
Number               LONG
                     END
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
total_temp           LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(QAREASON)
                       PROJECT(qar:Reason)
                     END
Report               REPORT('Status Summary Report'),AT(396,2760,7521,6958),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1000),USE(?unnamed)
                         STRING('Date Range: '),AT(5000,156),USE(?String22),TRN,FONT(,8,,)
                         STRING(@s3),AT(5781,781),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(5781,156),USE(GLO:Select1),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(6615,156),USE(GLO:Select2),TRN,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(5000,365),USE(?String24),TRN,FONT(,8,,)
                         STRING(@s60),AT(5781,365),USE(tmp:PrintedBy),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,573),USE(?ReportDatePrompt),TRN,FONT(,8,,)
                         STRING(@d6),AT(5781,573),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(' to '),AT(6406,156),USE(?String23),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(5000,781),USE(?String59),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,229),USE(?DetailBand)
                           STRING(@s60),AT(2115,0),USE(qar:Reason),TRN,LEFT
                           STRING(@s8),AT(4667,0),USE(count#),TRN,RIGHT
                         END
                         FOOTER,AT(0,0),USE(?unnamed:2)
                           LINE,AT(2031,52,3438,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of QA Failures'),AT(2115,125),USE(?String21),TRN
                           STRING(@n-14),AT(4260,125),USE(total_temp),TRN,RIGHT,FONT(,,,FONT:bold)
                         END
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING('QA FAILURE REPORT'),AT(4271,0,3177,260),USE(?String20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,885),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,885),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1042),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING('Failure Reason'),AT(2135,2083),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('No Of Occurances'),AT(4792,2083),USE(?String45),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('QA_Failure_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:QAREASON.Open
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  
  
  RecordsToProcess = RECORDS(QAREASON)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(QAREASON,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(qar:ReasonKey)
      Process:View{Prop:Filter} = ''
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        count# = 0
        save_aud_id = access:audit.savefile()
        access:audit.clearkey(aud:actiononlykey)
        aud:action = 'QA REJECTION: ' & Clip(qar:reason)
        aud:date   = glo:select1
        set(aud:actiononlykey,aud:actiononlykey)
        loop
            if access:audit.next()
               break
            end !if
            if aud:action <> 'QA REJECTION: ' & Clip(qar:reason)      |
            or aud:date   > glo:select2      |
                then break.  ! end if
            count# += 1
        end !loop
        access:audit.restorefile(save_aud_id)
        If count# <> 0
            Print(rpt:detail)
            total_temp += count#
        End!If count# <> 0
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(QAREASON,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:QAREASON.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'QAREASON')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='QA_Failure_Report'
  END
  Report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'QA_Failure_Report',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'QA_Failure_Report',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'QA_Failure_Report',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'QA_Failure_Report',1)
    SolaceViewVars('save_aud_id',save_aud_id,'QA_Failure_Report',1)
    SolaceViewVars('Reason_Queue_Temp:Reason',Reason_Queue_Temp:Reason,'QA_Failure_Report',1)
    SolaceViewVars('Reason_Queue_Temp:Number',Reason_Queue_Temp:Number,'QA_Failure_Report',1)
    SolaceViewVars('LocalRequest',LocalRequest,'QA_Failure_Report',1)
    SolaceViewVars('LocalResponse',LocalResponse,'QA_Failure_Report',1)
    SolaceViewVars('FilesOpened',FilesOpened,'QA_Failure_Report',1)
    SolaceViewVars('WindowOpened',WindowOpened,'QA_Failure_Report',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'QA_Failure_Report',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'QA_Failure_Report',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'QA_Failure_Report',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'QA_Failure_Report',1)
    SolaceViewVars('PercentProgress',PercentProgress,'QA_Failure_Report',1)
    SolaceViewVars('RecordStatus',RecordStatus,'QA_Failure_Report',1)
    SolaceViewVars('EndOfReport',EndOfReport,'QA_Failure_Report',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'QA_Failure_Report',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'QA_Failure_Report',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'QA_Failure_Report',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'QA_Failure_Report',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'QA_Failure_Report',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'QA_Failure_Report',1)
    SolaceViewVars('InitialPath',InitialPath,'QA_Failure_Report',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'QA_Failure_Report',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'QA_Failure_Report',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'QA_Failure_Report',1)
    SolaceViewVars('total_temp',total_temp,'QA_Failure_Report',1)


BuildCtrlQueue      Routine







