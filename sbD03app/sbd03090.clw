

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03090.INC'),ONCE        !Local module procedure declarations
                     END


StatusReportValidation PROCEDURE  (func:DateRangeType,func:StartDate,func:EndDate,func:JobType,func:InvoiceType,func:DespatchedType,func:CompletedType,func:StatusType,func:JobBatchNumber,func:EDIBatchNumber) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'StatusReportValidation')      !Add Procedure to Log
  end


        Case func:DateRangeType
            Of 0
                If ~INRANGE(job:Date_Booked,func:StartDate,func:EndDate)
                    Return Level:Fatal
                End !If ~INRANGE(job:Date_Booked,func:Start_Date,func:End_Date)
            Of 1
                If ~INRANGE(job:Date_Completed,func:StartDate,func:EndDate)
                    Return Level:Fatal
                End !If ~INRANGE(job:Date_Booked,func:Start_Date,func:End_Date)
            ! Start Change 2223 BE(05/03/03)
            OF 2
                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = job:Ref_Number
                IF (Access:JOBSE.TryFetch(jobe:RefNumberKey)) THEN
                    ! Ignore Job Records without Extension Record
                    RETURN Level:Fatal
                ELSE
                    IF (~INRANGE(jobe:InWorkshopDate,func:StartDate,func:EndDate)) THEN
                        RETURN Level:Fatal
                    END
                END
            ! Start Change 2223 BE(05/03/03)
        End !Case func:DateRangeType

        Case func:JobType
            Of 1 !Warranty Only
                If job:chargeable_job = 'YES' Or job:warranty_job <> 'YES'
                    Return Level:Fatal
                End!If job:chargeable_job = 'YES'
            Of 4 !Chargeable Only
                IF job:warranty_job = 'YES' Or job:chargeable_job <> 'YES'
                    Return Level:Fatal
                End!IF job:warranty_job = 'YES'
            Of 3 !Split Only
                If job:warranty_job <> 'YES' Or job:chargeable_job <> 'YES'
                    Return Level:Fatal
                End!If job:warranty_job <> 'YES' Or job:chargeable_job <> 'YES'
            Of 2 !Warranty Split
                If job:warranty_job <> 'YES'
                    Return Level:Fatal
                End!If job:warranty_job <> 'YES'
            Of 5 !Chargeable Split
                If job:chargeable_job <> 'YES'
                    Return Level:Fatal
                End!If job:chargeable_job <> 'YES'
        End!Case glo:select22

        If func:JobBatchNumber <> 0
            If func:JobBatchNumber <> job:Batch_Number
                Return Level:Fatal
            End !If func:JobBatchNumber <> job:Batch_Number
        End !If func:JobBatchNumber <> 0

        If func:EDIBatchNumber <> 0
            If func:EDIBatchNumber <> job:EDI_Batch_Number
                Return Level:Fatal
            End !If func:EDIBatchNumber <> job:EDI_Batch_Number
        End !If func:EDIBatchNumber <> 0

        Case func:InvoiceType
            Of 1 !Invoiced Only
                Case func:JobType
                    Of 1 Orof 2 !Warranty Only, or Warranty (Split)
                        If job:invoice_number_warranty = ''
                            Return Level:Fatal
                        End!If job:invoice_number_warranty = ''
                    Of 4 Orof 5 !Chargeable Only, or Chargeable (Split)
                        If job:invoice_number = ''
                            Return Level:Fatal
                        End
                    Else
                        If (job:chargeable_job = 'YES' and job:invoice_number = '') Or |
                            (job:warranty_job = 'YES' and job:invoice_number_warranty = '')
                            Return Level:Fatal
                        End!If job:invoice_number = '' Of job:invoice_Number_warranty = ''
                End!Case glo:select22
            Of 2 !UnInvoiced Only
                Case func:JobType
                    Of 1 Orof 2 !Warranty Only, or Warranty (Split)
                        If job:invoice_number_warranty <> ''
                            Return Level:Fatal
                        End!If job:invoice_number_warranty = ''
                    Of 4 Orof 5 !Chargealble Only, or Chargeable (Split)
                        If job:invoice_number <> ''
                            Return Level:Fatal
                        End
                    Else
                        If (job:chargeable_job = 'YES' and job:invoice_number <> '') Or |
                            (job:warranty_job = 'YES' and job:invoice_number_warranty <> '')
                            Return Level:Fatal
                        End!If job:invoice_number = '' Of job:invoice_Number_warranty = ''
                End!Case glo:select22
        End

        Case func:DespatchedType
            Of 1 !Despatched Only
                If job:consignment_number = ''
                    Return Level:Fatal
                End!If job:consignment_number = ''
            Of 2 !Non-Despatched Only
                If job:consignment_number <> ''
                    Return Level:Fatal
                End!If job:consignment_number <> ''
        End!Case glo:select19

        Case func:CompletedType
            Of 1 !Completed Only
                If job:date_completed = ''
                    Return Level:Fatal
                End
            Of 2 !Incomplete Only
                If job:date_completed <> ''
                    Return Level:Fatal
                End
        End


        !Check Head Account
        If Records(glo:Queue)
            Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
            sub:Account_Number   = job:Account_Number
            If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                !Found
                Clear(glo:Queue)
                Sort(glo:queue,glo:Pointer)
                glo:Pointer = sub:Main_Account_Number
                Get(glo:queue,glo:Pointer)
                If Error()
                    Return Level:Fatal
                End !If Error()
            Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        End !If Records(glo:Queue)

            !Check Sub Account
        If Records(glo:queue2)
            Clear(glo:Queue2)
            Sort(glo:Queue2,glo:Pointer2)
            glo:Pointer2    = job:Account_Number
            Get(glo:Queue2,glo:Pointer2)
            If Error()
                Return Level:Fatal
            End !If Error()
        End !If Records(glo:queue2)

        If job:Chargeable_Job = 'YES'
            !Check Chargeable Charge Type
            If Records(glo:Queue3)
                Clear(glo:Queue3)
                Sort(glo:Queue3,glo:Pointer3)
                glo:Pointer3    = job:Charge_Type
                Get(glo:Queue3,glo:Pointer3)
                If Error()
                    Return Level:Fatal
                End !If Error()
            End !If Records(glo:Queue3)

            !Check Chargeable Repair Type
            If Records(glo:queue5)
                Clear(glo:Queue5)
                Sort(glo:Queue5,glo:Pointer5)
                glo:Pointer5    = job:Repair_Type
                Get(glo:Queue5,glo:Pointer5)
                If Error()
                    Return Level:Fatal
                End !If Error()
            End !If Records(glo:queue5)
        End !If job:Chargeable_Job = 'YES'

        If job:Warranty_Job = 'YES'
            !Check Warranty Charge Type
            If Records(glo:Queue4)
                Clear(glo:Queue4)
                Sort(glo:Queue4,glo:Pointer4)
                glo:Pointer4 = job:Warranty_Charge_Type
                Get(glo:Queue4,glo:Pointer4)
                If Error()
                    Return Level:Fatal
                End !If Error()
            End !If Records(glo:Queue4)

            !Check Warranty Repair Type
            If Records(glo:Queue6)
                Clear(glo:Queue6)
                Sort(glo:Queue6,glo:Pointer6)
                glo:Pointer6    = job:Repair_Type_Warranty
                Get(glo:Queue6,glo:Pointer6)
                If Error()
                    Return Level:Fatal
                End !If Error()
            End !If Records(glo:Queue6)
        End !If job:Warranty_Job = 'YES'

        !Check Status
        If Records(glo:queue7)
            Clear(glo:Queue7)
            Sort(glo:Queue7,glo:Pointer7)

            Case func:StatusType
                Of 0 !Job Status
                    glo:Pointer7    = job:Current_Status
                Of 1 !Exchange Status
                    glo:Pointer7    = job:Exchange_Status
                Of 2 !Loan Status
                    glo:Pointer7    = job:Loan_Status
            End !Case func:StatusType

            Get(glo:Queue7,glo:Pointer7)
            If Error()
                Return Level:Fatal
            End !If Error()
        End !If Records(glo:queue7)
        !Check Locations

        If Records(glo:queue8)
            Clear(glo:Queue8)
            Sort(glo:Queue8,glo:Pointer8)
            glo:Pointer8    = job:Location
            Get(glo:Queue8,glo:Pointer8)
            If Error()
                Return Level:Fatal
            End !If Error()
        End !If Records(glo:queue8)

        !Check Engineers

        If Records(glo:Queue9)
            Clear(glo:Queue9)
            Sort(glo:Queue9,glo:Pointer9)
            glo:Pointer9    = job:engineer
            Get(glo:Queue9,glo:Pointer9)
            If Error()
                Return Level:Fatal
            End !If Error()
        End !If Records(glo:Queue9)

        !Check Manufacturer
        If Records(glo:Queue10)
            Clear(glo:Queue10)
            Sort(glo:Queue10,glo:Pointer10)
            glo:Pointer10   = job:Manufacturer
            Get(glo:Queue10,glo:Pointer10)
            If Error()
                Return Level:Fatal
            End !If Error()
        End !If Records(glo:Queue10)

        
        If Records(glo:queue11)
            Clear(glo:Queue11)
            Sort(glo:Queue11,glo:Pointer11)
            glo:Pointer11   = job:Model_Number
            Get(glo:Queue11,glo:Pointer11)
            If Error()
                Return Level:Fatal
            End !If Error()
        End !If Records(glo:queue11)

        !Check Unit Types
        If Records(glo:queue12)
            Clear(glo:Queue12)
            Sort(glo:Queue12,glo:Pointer12)
            glo:Pointer12   = job:Unit_Type
            Get(glo:Queue12,glo:Pointer12)
            If Error()
                Return Level:Fatal
            End !If Error()
        End !If Records(glo:queue12)

        !Check Transit Type
        If Records(glo:queue13)
            Clear(glo:Queue13)
            Sort(glo:Queue13,glo:Pointer13)
            glo:Pointer13   = job:Transit_Type
            Get(glo:Queue13,glo:Pointer13)
            If Error()
                Return Level:Fatal
            End !If Error()
        End !If Records(glo:queue13)

        !Check Turnaround Time
        If Records(glo:queue14)
            Clear(glo:Queue14)
            Sort(glo:Queue14,glo:Pointer14)
            glo:Pointer14   = job:Turnaround_Time
            Get(glo:Queue14,glo:Pointer14)
            If Error()
                Return Level:Fatal
            End !If Error()
        End !If Records(glo:queue14)

        !Check Colour
        If Records(glo:queue15)
            Clear(glo:Queue15)
            Sort(glo:Queue15,glo:Pointer15)
            glo:Pointer15   = job:Colour
            Get(glo:Queue15,glo:Pointer15)
            If Error()
                Return Level:Fatal
            End !If Error()
        End !If Records(glo:queue14)

    Return Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'StatusReportValidation',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
