

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03007.INC'),ONCE        !Local module procedure declarations
                     END


Completed_Job_Stats_Export PROCEDURE                  ! Declare Procedure
savepath             STRING(255)
tmp:LabourValue      REAL,DIM(50)
tmp:PartsValue       REAL,DIM(50)
tmp:WarLabourValue   REAL,DIM(50)
tmp:WarPartsValue    REAL,DIM(50)
tmp:Booked           LONG,DIM(50)
tmp:LineLabour       REAL
tmp:LineParts        REAL
tmp:LineWarLabour    REAL
tmp:LineWarParts     REAL
tmp:LineCost         REAL
tmp:LineWarCost      REAL
tmp:TotalLabour      REAL
tmp:TotalParts       REAL
tmp:TotalChar        REAL
tmp:TotalWarLabour   REAL
tmp:TotalWarParts    REAL
tmp:TotalWar         REAL
save_job_id          USHORT,AUTO
save_mod_id          USHORT,AUTO
tmp:ChargeType       STRING(60),DIM(50)
tmp:JobTotal         LONG,DIM(50)
save_cha_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Arial',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Arial',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Arial',8,,)
     end
!Shell Execute Variables
AssocFile    CString(255)
Operation    CString(255)
Param        CString(255) 
Dir          CString(255)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Completed_Job_Stats_Export')      !Add Procedure to Log
  end


   Relate:CHARTYPE.Open
   Relate:JOBS.Open
   Relate:MANUFACT.Open
   Relate:MODELNUM.Open
   Relate:DEFAULTS.Open
   Relate:SUBTRACC.Open
   Relate:TRADEACC.Open
    Set(defaults)
    access:Defaults.next()
    savepath = path()
    If def:exportpath <> ''
        glo:file_name = Clip(def:exportpath) & '\JOBSTATS.CSV'
    Else!If def:exportpath <> ''
        glo:file_name = 'C:\JOBSTATS.CSV'
    End!If def:exportpath <> ''
        if not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
                file:keepdir + file:noerror + file:longname)
        !failed
        setpath(savepath)
    else!if not filedialog
        !found
        setpath(savepath)
        Remove(glo:file_name)
        access:expgendm.open()
        access:expgendm.usefile()
        Display()
        recordspercycle     = 25
        recordsprocessed    = 0
        percentprogress     = 0
        setcursor(cursor:wait)
        open(progresswindow)
        progress:thermometer    = 0
        ?progress:pcttext{prop:text} = '0% Completed'

        recordstoprocess    = Records(Modelnum)

        tmp:TotalLabour     = 0
        tmp:TotalParts      = 0
        tmp:TotalChar       = 0
        tmp:TotalWarLabour  = 0
        tmp:TotalWarParts   = 0
        tmp:TotalWar        = 0

        Clear(exp:record)
        exp:line1[1] = 'Completed Jobs Statistics Export'
        access:expgendm.insert()
        exp:line1[1] = 'Jobs Completed Between: '
        exp:line1[2] = Format(glo:select1,@d6)
        exp:line1[3] = Format(glo:select2,@d6)
        access:expgendm.insert()

        Clear(exp:record)
        access:expgendm.insert()

        If glo:select3 = 1
            Clear(exp:record)
            exp:line1[1] = 'Account Number:'
            exp:line1[2] = glo:select4
            access:expgendm.insert()
        End!If glo:select3 = 1

        Clear(exp:record)
        exp:line1[1] = 'Make/Model'
        x# = 1
        save_cha_id = access:chartype.savefile()
        access:chartype.clearkey(cha:warranty_key)
        set(cha:warranty_key)
        loop
            if access:chartype.next()
               break
            end !if
            x# += 1
            exp:line1[x#] = Clip(cha:charge_type)
            tmp:ChargeType[x#] = exp:line1[x#]
        end !loop
        access:chartype.restorefile(save_cha_id)
        exp:line1[x#+1]     = 'Total'
        exp:line1[x#+2]     = 'Labour (Chargeable)'
        exp:line1[x#+3]     = 'Parts (Chargeable)'
        exp:line1[x#+4]     = 'Total (Chargeable Ex V.A.T.)'
        exp:line1[x#+5]     = 'Labour (Warranty)'
        exp:line1[x#+6]     = 'Parts (Warranty)'
        exp:line1[x#+7]     = 'Total (Warranty Ex V.A.T.)'
        all_jobs# = 0

        access:expgendm.insert()

!loop through models
        
        Clear(exp:record)
        save_mod_id = access:modelnum.savefile()
        access:modelnum.clearkey(mod:manufacturer_key)
        set(mod:manufacturer_key)
        loop
            if access:modelnum.next()
               break
            end !if
            Do GetNextRecord2
            exp:line1[1] = Clip(mod:manufacturer) & ' - ' & CLip(mod:model_number)
            line_count# = 0
            tmp:LineLabour      = 0
            tmp:LineParts       = 0
            tmp:LineWarLabour   = 0
            tmp:LineWarParts    = 0
            tmp:LineCost        = 0
            tmp:LineWarCost     = 0

!loop through the saved charge types
            Loop y# = 2 to x#
                count_jobs# = 0

!loop through jobs in completed date order and count
                save_job_id = access:jobs.savefile()
                access:jobs.clearkey(job:modelcompkey)
                job:model_number   = mod:model_number
                job:date_completed = glo:select1
                set(job:modelcompkey,job:modelcompkey)
                loop
                    if access:jobs.next()
                       break
                    end !if
                    if job:model_number   <> mod:model_number      |
                    or job:date_completed > glo:select2      |
                        then break.  ! end if

                    If glo:select3 = 1
                        Case glo:select5
                            Of 'MAI'
                                !The selected account was a main account
                                access:subtracc.clearkey(sub:account_number_key)
                                sub:account_number  = job:account_number
                                If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign
                                    !Found
                                    !Get the Sub Account and see if the Sub's Main matched the criteria
                                    If sub:main_account_number <> glo:select4
                                        Cycle
                                    End!If sub:main_account_number <> glo:select4
                                Else! If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign
                                    !Error
                                End! If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign
                                
                            Of 'SUB'
                                If job:account_number <> glo:select4
                                    Cycle
                                End!If job:account_number <> glo:select4
                        End!Case glo:select5
                    End!If glo:select3 = 1
                    If job:chargeable_job = 'YES' and job:warranty_job = 'YES'
                        If job:charge_type <> tmp:ChargeType[y#] And |
                            job:warranty_charge_type <> tmp:ChargeType[y#]
                            Cycle
                        End!job:warranty_charge_type <> tmp:ChargeType[y#]
                    Else!If job:chargeable_job = 'YES' and job:warranty_job = 'YES'
                        If job:chargeable_job = 'YES'
                            If job:charge_type <> tmp:ChargeType[y#]
                                Cycle
                            End!If job:charge_type <> tmp:ChargeType[y#]
                        End!If job:chargeable_job = 'YES'
                        If job:warranty_job = 'YES'
                            If job:warranty_charge_type <> tmp:ChargeType[y#]
                                Cycle
                            End!If job:warranty_charge_type <> tmp:ChargeType[y#]
                        End!If job:warranty_job = 'YES'

                    End!If job:chargeable_job = 'YES' and job:warranty_job = 'YES'

                    count_jobs# += 1
                    tmp:LineLabour      += Round(job:labour_cost,.01)
                    tmp:LineParts       += Round(job:parts_cost,.01)
                    tmp:LineWarLabour   += Round(job:labour_cost_warranty,.01)
                    tmp:LineWarParts    += Round(job:parts_cost_warranty,.01)
                    tmp:LineCost        += Round(job:sub_total,.01)
                    tmp:LineWarCost     += Round(job:sub_total_Warranty,.01)
                    If job:date_booked => glo:select1 And job:date_booked <= glo:select2
                        tmp:booked[y#] += 1
                    End!If job:date_booked => glo:select1 And job:date_booked <= glo:select2
                    tmp:LabourValue[y#]     += Round(job:labour_cost,.01)
                    tmp:PartsValue[y#]      += Round(job:parts_cost,.01)
                    tmp:WarLabourValue[y#]  += Round(job:labour_cost_warranty,.01)
                    tmp:WarPartsValue[y#]   += Round(job:parts_cost_warranty,.01)

                end !loop
                access:jobs.restorefile(save_job_id)
                exp:line1[y#]   = count_jobs#
                tmp:JobTotal[y#]    += count_jobs#
                line_count# += count_jobs#
            End!Loop y# = 2 to x#
            exp:line1[x#+1]     = line_count#
            exp:line1[x#+2]     = tmp:LineLabour
            exp:line1[x#+3]     = tmp:LineParts
            exp:line1[x#+4]     = tmp:LineCost 
            exp:line1[x#+5]     = tmp:LineWarLabour
            exp:line1[x#+6]     = tmp:LineWarParts
            exp:line1[x#+7]     = tmp:LineWarCost

            access:expgendm.insert()

            tmp:TotalLabour     += tmp:linelabour
            tmp:TotalParts      += tmp:lineparts
            tmp:TotalChar       += tmp:linecost
            tmp:TotalWarLabour  += tmp:linewarlabour
            tmp:TotalWarParts   += tmp:linewarparts
            tmp:TotalWar        += tmp:linewarcost
            all_jobs#           += line_count#

        end !loop
        access:modelnum.restorefile(save_mod_id)

        Clear(exp:record)
        exp:line1[1] = 'Totals'
        Loop y# = 2 to x#
            exp:line1[y#]   = tmp:JobTotal[y#]
        End!Loop y# = 2 to x#
        exp:line1[x#+1]     = all_jobs#
        exp:line1[x#+2]     = tmp:TotalLabour
        exp:line1[x#+3]     = tmp:TotalParts
        exp:line1[x#+4]     = tmp:TotalChar
        exp:line1[x#+5]     = tmp:TotalWarLabour
        exp:line1[x#+6]     = tmp:TotalWarParts
        exp:line1[x#+7]     = tmp:TotalWar

        access:expgendm.insert()

        Clear(exp:record)
        exp:line1[1]    = 'Labour Value (Chargeable)'
        Loop y# = 2 to x#
            exp:line1[y#] = tmp:LabourValue[y#]
        End!Loop y# = 2 to x#

        access:expgendm.insert()

        Clear(exp:record)
        exp:line1[1]    = 'Parts Value (Chargeable)'
        Loop y# = 2 to x#
            exp:line1[y#] = tmp:PartsValue[y#]
        End!Loop y# = 2 to x#
        access:expgendm.insert()

        Clear(exp:record)
        exp:line1[1]    = 'Total Value (Chargeable Ex V.A.T.)'
        Loop y# = 2 to x#
            exp:line1[y#] = tmp:PartsValue[y#] + tmp:LabourValue[y#]
        End!Loop y# = 2 to x#
        access:expgendm.insert()

        Clear(exp:record)
        exp:line1[1]    = 'Labour Value (Warranty)'
        Loop y# = 2 to x#
            exp:line1[y#] = tmp:WarLabourValue[y#]
        End!Loop y# = 2 to x#

        access:expgendm.insert()

        Clear(exp:record)
        exp:line1[1]    = 'Parts Value (Warranty)'
        Loop y# = 2 to x#
            exp:line1[y#] = tmp:WarPartsValue[y#]
        End!Loop y# = 2 to x#
        access:expgendm.insert()

        Clear(exp:record)
        exp:line1[1]    = 'Total Value (Chargeable Ex V.A.T.)'
        Loop y# = 2 to x#
            exp:line1[y#] = tmp:WarPartsValue[y#] + tmp:WarLabourValue[y#]
        End!Loop y# = 2 to x#
        access:expgendm.insert()

        Clear(exp:record)
        exp:line1[1]    = 'Total Booked'
        Loop y# = 2 to x#
            exp:line1[y#] = tmp:Booked[y#]
        End!Loop y# = 2 to x#
        access:expgendm.insert()


        setcursor()
        close(progresswindow)
        access:expgendm.close()

        Case MessageEx('Export Completed. <13,10><13,10>Do you wish to view the export file now?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                AssocFile = Clip(glo:file_name)
                Param = ''
                Dir = ''
                Operation = 'Open'
                ShellExecute(GetDesktopWindow(), Operation, AssocFile, Param, Dir, 5)
            Of 2 ! &No Button
        End!Case MessageEx

    end!if not filedialog

   Relate:CHARTYPE.Close
   Relate:JOBS.Close
   Relate:MANUFACT.Close
   Relate:MODELNUM.Close
   Relate:DEFAULTS.Close
   Relate:SUBTRACC.Close
   Relate:TRADEACC.Close
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Completed_Job_Stats_Export',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('savepath',savepath,'Completed_Job_Stats_Export',1)
    
      Loop SolaceDim1# = 1 to 50
        SolaceFieldName" = 'tmp:LabourValue' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tmp:LabourValue[SolaceDim1#],'Completed_Job_Stats_Export',1)
      End
    
    
    
      Loop SolaceDim1# = 1 to 50
        SolaceFieldName" = 'tmp:PartsValue' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tmp:PartsValue[SolaceDim1#],'Completed_Job_Stats_Export',1)
      End
    
    
    
      Loop SolaceDim1# = 1 to 50
        SolaceFieldName" = 'tmp:WarLabourValue' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tmp:WarLabourValue[SolaceDim1#],'Completed_Job_Stats_Export',1)
      End
    
    
    
      Loop SolaceDim1# = 1 to 50
        SolaceFieldName" = 'tmp:WarPartsValue' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tmp:WarPartsValue[SolaceDim1#],'Completed_Job_Stats_Export',1)
      End
    
    
    
      Loop SolaceDim1# = 1 to 50
        SolaceFieldName" = 'tmp:Booked' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tmp:Booked[SolaceDim1#],'Completed_Job_Stats_Export',1)
      End
    
    
    SolaceViewVars('tmp:LineLabour',tmp:LineLabour,'Completed_Job_Stats_Export',1)
    SolaceViewVars('tmp:LineParts',tmp:LineParts,'Completed_Job_Stats_Export',1)
    SolaceViewVars('tmp:LineWarLabour',tmp:LineWarLabour,'Completed_Job_Stats_Export',1)
    SolaceViewVars('tmp:LineWarParts',tmp:LineWarParts,'Completed_Job_Stats_Export',1)
    SolaceViewVars('tmp:LineCost',tmp:LineCost,'Completed_Job_Stats_Export',1)
    SolaceViewVars('tmp:LineWarCost',tmp:LineWarCost,'Completed_Job_Stats_Export',1)
    SolaceViewVars('tmp:TotalLabour',tmp:TotalLabour,'Completed_Job_Stats_Export',1)
    SolaceViewVars('tmp:TotalParts',tmp:TotalParts,'Completed_Job_Stats_Export',1)
    SolaceViewVars('tmp:TotalChar',tmp:TotalChar,'Completed_Job_Stats_Export',1)
    SolaceViewVars('tmp:TotalWarLabour',tmp:TotalWarLabour,'Completed_Job_Stats_Export',1)
    SolaceViewVars('tmp:TotalWarParts',tmp:TotalWarParts,'Completed_Job_Stats_Export',1)
    SolaceViewVars('tmp:TotalWar',tmp:TotalWar,'Completed_Job_Stats_Export',1)
    SolaceViewVars('save_job_id',save_job_id,'Completed_Job_Stats_Export',1)
    SolaceViewVars('save_mod_id',save_mod_id,'Completed_Job_Stats_Export',1)
    
      Loop SolaceDim1# = 1 to 50
        SolaceFieldName" = 'tmp:ChargeType' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tmp:ChargeType[SolaceDim1#],'Completed_Job_Stats_Export',1)
      End
    
    
    
      Loop SolaceDim1# = 1 to 50
        SolaceFieldName" = 'tmp:JobTotal' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tmp:JobTotal[SolaceDim1#],'Completed_Job_Stats_Export',1)
      End
    
    
    SolaceViewVars('save_cha_id',save_cha_id,'Completed_Job_Stats_Export',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
