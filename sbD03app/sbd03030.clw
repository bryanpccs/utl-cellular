

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03030.INC'),ONCE        !Local module procedure declarations
                     END








Exchange_Units_In_Channel_Fix_Later PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_job_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
Exchange_Queue       QUEUE,PRE(excque)
model_number         STRING(30)
request_received     REAL
same_day             REAL
other_day            REAL
received_in_workshop REAL
                     END
In_Repair_Queue      QUEUE,PRE(repque)
model_number         STRING(30)
in_stock             REAL
in_workshop          REAL
awaiting_parts       REAL
RTM                  REAL
                     END
first_page_temp      BYTE(1)
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_xch_id          USHORT,AUTO
tmp:PrintedBy        STRING(60)
account_number_temp  STRING(60)
model_number_temp    STRING(30)
requests_received_temp STRING(6)
same_day_temp        STRING(6)
other_day_temp       STRING(6)
Failed_Exchange_Temp STRING(6)
Despatched_Temp      STRING(6)
In_Workshop_Temp     STRING(6)
Awaiting_Arrival_Temp STRING(6)
In_Stock_Temp        STRING(6)
Units_In_Workshop_Temp STRING(6)
Received_In_Workshop_Temp STRING(6)
Awaiting_Parts_Temp  STRING(6)
RTM_Temp             STRING(6)
In_Repair_Total      STRING(6)
In_Channel_Temp      STRING(6)
Removed_From_Stock_Temp STRING(6)
In_Scheme_Temp       STRING(6)
Manufacturer_Temp    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                     END
report               REPORT('Exchange Units In Channel Report'),AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,750,7521,1771),USE(?unnamed)
                         STRING('Trade Account:'),AT(4948,208),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s60),AT(5729,208),USE(account_number_temp),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Date Range:'),AT(4948,469),USE(?String23),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5729,469),USE(GLO:Select1),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(6406,469),USE(GLO:Select2),TRN,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(4948,729),USE(?String26),TRN,FONT(,8,,)
                         STRING(@s60),AT(5729,729),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Printed:'),AT(4948,990),USE(?String27),TRN,FONT(,8,,)
                         STRING('Page Number:'),AT(4948,1250),USE(?string59),TRN,FONT(,8,,)
                         STRING(@s3),AT(5729,1250),USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(5729,990),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,7208),USE(?detailband)
                           STRING('MODEL NUMBER:'),AT(677,0),USE(?model_string),TRN,FONT(,9,,FONT:bold)
                           STRING(@s30),AT(1875,0),USE(model_number_temp),TRN,FONT(,9,,FONT:bold)
                           STRING('EXCHANGE TRANSACTION'),AT(677,365),USE(?String28),TRN,FONT(,9,,FONT:bold)
                           STRING(@s6),AT(5906,677),USE(requests_received_temp),TRN,RIGHT
                           STRING('IN TRANSIT'),AT(698,2135),USE(?String28:2),TRN,FONT(,9,,FONT:bold)
                           STRING(@s6),AT(5906,2396),USE(Despatched_Temp),TRN,RIGHT
                           STRING('IN REPAIR'),AT(698,3750),USE(?String28:3),TRN,FONT(,9,,FONT:bold)
                           STRING(@s6),AT(5906,3958),USE(In_Stock_Temp),TRN,RIGHT
                           STRING('GENERAL DETAILS'),AT(677,5729),USE(?String28:4),TRN,FONT(,9,,FONT:bold)
                           STRING('Exchange Units In Channel:'),AT(1875,5990),USE(?String41),TRN,FONT(,,,FONT:bold)
                           STRING(@s6),AT(5906,5990),USE(In_Channel_Temp),TRN,RIGHT,FONT(,,,FONT:bold)
                           LINE,AT(5938,6198,500,0),USE(?Line1:5),COLOR(COLOR:Black)
                           STRING('Exchange Units In Stock:'),AT(1875,3958),USE(?String36),TRN
                           STRING('Exchange Units In Workshop:'),AT(1875,4271),USE(?String37),TRN
                           STRING(@s6),AT(5906,4271),USE(In_Workshop_Temp),TRN,RIGHT
                           STRING('Exchange Units Awaiting Parts:'),AT(1875,4583),USE(?String38),TRN
                           STRING(@s6),AT(5906,4583),USE(Awaiting_Parts_Temp),TRN,RIGHT
                           STRING('Exchange Units R.T.M.:'),AT(1875,4896),USE(?String39),TRN
                           STRING(@s6),AT(5906,4896),USE(RTM_Temp),TRN,RIGHT
                           STRING('Total:'),AT(1875,5208),USE(?String45),TRN,FONT(,10,,FONT:bold)
                           STRING(@s6),AT(5906,5208),USE(In_Repair_Total),TRN,RIGHT,FONT(,,,FONT:bold)
                           STRING('Exchange Units Despatched:'),AT(1875,2396),USE(?String33),TRN
                           STRING('Exchange Units Received Into Workshop:'),AT(1875,2708),USE(?String34),TRN
                           STRING(@s6),AT(5906,2708),USE(Received_In_Workshop_Temp),TRN,RIGHT
                           STRING('Exchange Units Awaiting Arrival:'),AT(1875,3021),USE(?String35),TRN,FONT(,,,FONT:bold)
                           STRING(@s6),AT(5906,3021),USE(Awaiting_Arrival_Temp),TRN,RIGHT,FONT(,,,FONT:bold)
                           STRING('Exchange Requests Received:'),AT(1875,677),USE(?String29),TRN
                           STRING('Exchange Units Despatched (Same Day):'),AT(1875,990),USE(?String30),TRN
                           STRING(@s6),AT(5906,990),USE(same_day_temp),TRN,RIGHT
                           STRING('Exchange Units Despatched (Not Same Day):'),AT(1875,1354),USE(?String31),TRN
                           STRING(@s6),AT(5906,1354),USE(other_day_temp),TRN,RIGHT
                           LINE,AT(5938,1667,500,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Failed Exchange Transactions:'),AT(1875,1719),USE(?String32),TRN,FONT(,,,FONT:bold)
                           STRING(@s6),AT(5906,1719),USE(Failed_Exchange_Temp),TRN,RIGHT,FONT(,,,FONT:bold)
                           LINE,AT(5938,2969,500,0),USE(?Line1:2),COLOR(COLOR:Black)
                           LINE,AT(5938,5156,500,0),USE(?Line1:3),COLOR(COLOR:Black)
                           LINE,AT(5938,5938,500,0),USE(?Line1:4),COLOR(COLOR:Black)
                         END
new_page                 DETAIL,PAGEBEFORE(-1),AT(,,,52),USE(?new_page)
                         END
first_page_title         DETAIL,AT(,,,656),USE(?first_page_title)
                           STRING('SELECTED MODEL NUMBERS'),AT(677,104),USE(?String28:5),TRN,FONT(,9,,FONT:bold)
                         END
first_page_detail        DETAIL,AT(,,,167),USE(?first_page_detail)
                           STRING(@s20),AT(1875,0),USE(GLO:Pointer),TRN,FONT(,8,,)
                           STRING(@s30),AT(3385,0),USE(Manufacturer_Temp),TRN,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('EXCHANGE UNITS IN CHANNEL'),AT(4271,0,3177,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(135,250,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(135,406,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         IMAGE,AT(-21,-10,7521,11156),USE(?Image1)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,885),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,885),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1042),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,1042),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(573,1198,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1198),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Description'),AT(698,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Units In Channel'),AT(5906,2083),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Exchange_Units_In_Channel_Fix_Later')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  CASE MESSAGE(CPCS:AskPrvwDlgText,CPCS:AskPrvwDlgTitle,ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
    OF BUTTON:Yes
      PreviewReq = True
    OF BUTTON:No
      PreviewReq = False
    OF BUTTON:Cancel
       DO ProcedureReturn
  END
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:TRANTYPE.Open
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      RecordsToProcess = Records(job:date_booked_key)  + Records(Exchange)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        setcursor(cursor:wait)
        save_job_id = access:jobs.savefile()
        access:jobs.clearkey(job:date_booked_key)
        job:date_booked = glo:select1
        set(job:date_booked_key,job:date_booked_key)
        loop
            if access:jobs.next()
               break
            end !if
            if job:date_booked > glo:select2      |
                then break.  ! end if
        
            recordsprocessed += 1
            Do DisplayProgress
        
            error# = 0
            If glo:select3 <> ''
                If job:account_number <> glo:select3
                    error# = 1
                End!If job:account_number <> glo:select3
                access:subtracc.clearkey(sub:account_number_key)
                sub:account_number = glo:select3
                if access:subtracc.fetch(sub:account_number_key) = Level:Benign
                    account_number_temp = Clip(sub:account_number) & '-' & Clip(sub:company_name)
                end
            Else!If glo:select <> ''
                account_number_temp = 'ALL'
            End!If glo:select <> ''
        
            If error# = 0
            !IF glo:select4 = YES, i.e. one model per page. Don't print the first (summary) page.
            !Otherwise, show the model on top of each page
                If glo:select4 <> 'YES'
                    Settarget(report)
                    Hide(?model_string)
                    model_number_temp = ''
                    Settarget()
                    If first_page_temp = 1
                        first_page_temp = 0
                        Print(rpt:first_page_title)
                        Sort(glo:Queue,glo:pointer)
                        Loop x# = 1 To Records(glo:Queue)
                            Get(glo:Queue,x#)
                            access:modelnum.clearkey(mod:model_number_key)
                            mod:model_number = glo:pointer
                            if access:modelnum.fetch(mod:model_number_key) = Level:Benign
                                manufacturer_temp = mod:manufacturer
                            end!if access:modelnum.fetch(mod:model_number_key) = Level:Benign
                            Print(rpt:first_page_detail)
                        End!Loop x# = 1 To Records(glo:Queue)
                        Print(rpt:new_page)
                    End!If first_page_temp = 1
                End!If glo:select4 = 'YES'
        
            !Looping through jobs in date order.
        
            !First If glo:select4 = 'YES', means one page per model.
        
            !Check the queue of models and see if the job model is in it
                Sort(glo:Queue,glo:pointer)
                glo:pointer  = job:model_number
                Get(glo:Queue,glo:pointer)                                                        
                If ~Error()
                    request# = 0
                    access:trantype.clearkey(trt:transit_type_key)                                      !Check the transit type to see if the
                    trt:transit_type = job:transit_type                                                 !default status is '108 Exchange Unit
                    if access:trantype.fetch(trt:transit_type_key) = Level:benign                       !Required'.
                        If sub(trt:initial_status,1,3) = '108'
                            request# = 1                                                                !Request Received
                        End!If sub(trt:initial_status,1,3) = '108'
                    end!if access:trantype.fetch(trt:transit_type_key) = Level:benign
                    If glo:select4 = 'YES'
                        Clear(exchange_queue)                                                       !to the job. Hopefully this should tie
                        Sort(exchange_queue,excque:model_number)                                    !up with the requests, but there may be
                        excque:model_number = job:model_number                                      !a mismatch due to transit types being
                        Get(exchange_queue,excque:model_number)                                     !changed on the job.
                        If Error()                                                                  !Model Number doesn't exist
                            excque:model_number = job:model_number                                  
                            If request# = 1
                                excque:request_received = 1
                            Else!If request# = 1
                                excque:request_received = 0
                            End!If request# = 1
                            If job:exchange_unit_number <> ''
                                If job:exchange_despatched <> ''
                                    If job:exchange_despatched = job:date_booked
                                        excque:same_day = 1
                                        excque:other_day = 0
                                    Else!If job:exchange_despatched = job:date_booked
                                        excque:other_day = 1
                                        excque:same_day = 0
                                    End!If job:exchange_despatched = job:date_booked
                                End!If job:exchange_despatched <> ''
        
                                If job:workshop = 'YES'
                                    excque:received_in_workshop = 1
                                Else!If job:workshop = 'YES'
                                    excque:received_in_workshop = 0
                                End!If job:workshop = 'YES'
                            End!If job:exchange_unit_number <> ''
                            Add(Exchange_Queue)
                            
                        Else!If Error()                                                             !Model number does exist
                            If request# = 1                                                         
                                excque:request_received += 1
                            End
                            If job:exchange_unit_number <> ''
                                If job:exchange_despatched <> ''
                                    If job:exchange_despatched = job:date_booked
                                        excque:same_day += 1
                                    Else!If job:exchange_despatched = job:date_booked
                                        excque:other_day += 1
                                    End!If job:exchange_despatched = job:date_booked
                                End!If job:exchange_despatched <> ''
        
                                If job:workshop = 'YES'
                                    excque:received_in_workshop += 1
                                End!If job:workshop = 'YES'
                            End!If job:exchange_unit_number <> ''
                            Put(Exchange_Queue)
                        End!If Error()
                    Else!If glo:select4 = 'YES'
                        If request# = 1
                            requests_received_temp += 1
                        End!If request# = 1
                        If job:exchange_unit_number <> ''
                            If job:exchange_despatched <> ''
                                If job:exchange_despatched = job:date_booked
                                    same_day_temp += 1
                                Else!If job:exchange_despatched = job:date_booked
                                    other_day_temp += 1
                                End!If job:exchange_despatched = job:date_booked
                            End!If job:exchange_despatched <> ''
                            If job:workshop = 'YES'
                                Received_In_Workshop_Temp += 1
                            End!If job:workshop = 'YES'
                        End!If job:exchange_unit_number <> ''
                    End!If glo:select4 = 'YES'
                End!If ~Error()
            End!If error# = 0
        end !loop
        access:jobs.restorefile(save_job_id)
        setcursor()
        in_stock_temp = 0
        in_workshop_temp = 0
        Awaiting_Parts_Temp = 0
        RTM_Temp = 0
        setcursor(cursor:wait)
        
        save_xch_id = access:exchange.savefile()
        set(xch:ref_number_key)
        loop
            if access:exchange.next()
               break
            end !if
            RecordsProcessed += 1
            Do DisplayProgress
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            Sort(glo:Queue,glo:pointer)
            glo:pointer = xch:model_number
            Get(glo:Queue,glo:pointer)
            If error()
                Cycle
            End
            If glo:select4 = 'YES'                                                               !If One Model Per Page. Build up the
                Clear(in_repair_queue)
                Sort(in_repair_queue,repque:model_number)                                           !In Repair Queue.
                repque:model_number = xch:model_number
                Get(in_repair_queue,repque:model_number)
                If Error()                                                                          !Check the model number of the exchange
                    repque:model_number = xch:model_number                                          !unit. Add if not found, put if found.
                    If xch:available = 'AVL'
                        repque:in_stock = 1
                    Else
                        repque:in_stock = 0
                    End
                    Add(in_repair_queue)
                Else
                    If xch:available = 'AVL'
                        repque:in_stock += 1
                        Put(in_repair_queue)
                    End
                End!If Error()
            Else!If glo:select4 = 'YES'
                If xch:available = 'AVL'
                    in_stock_temp += 1
                End!If xch:job_number <> 0
            End!If glo:select4 = 'YES'
        
        
            access:jobs.clearkey(job:ref_number_key)
            job:ref_number = xch:job_number
            if access:jobs.fetch(job:ref_number_key) = Level:Benign
                If job:date_completed = ''
        !Model Dependent
                    workshop# = 0
                    If job:workshop = 'YES'
                        workshop# = 1
                    End!If job:workshop = 'YES'
                    setcursor(cursor:wait)
                    rtm# = 0
                    If job:third_party_site <> '' And job:workshop <> 'YES'
                        rtm# = 1
                    Else!If job:third_party_site <> '' And job:workshop <> 'YES'
                        found# = 0
                        save_wpr_id = access:warparts.savefile()
                        access:warparts.clearkey(wpr:part_number_key)
                        wpr:ref_number  = job:ref_number
                        set(wpr:part_number_key,wpr:part_number_key)
                        loop
                            if access:warparts.next()
                               break
                            end !if
                            if wpr:ref_number  <> job:ref_number      |
                                then break.  ! end if
                            yldcnt# += 1
                            if yldcnt# > 25
                               yield() ; yldcnt# = 0
                            end !if
                            If wpr:date_ordered <> '' and wpr:date_received = ''
                                found# = 1
                            Else!If wpr:date_ordered <> and wpr:date_received = 'YES'
                                If wpr:pending_ref_number <> '' and wpr:date_received = ''
                                    found# = 1
                                End!If wpr:pending_ref_number <> ''
                            End!If wpr:date_ordered <> and wpr:date_received = 'YES'
                        end !loop
                        access:warparts.restorefile(save_wpr_id)
                        setcursor()
        
                        If found# = 0
                            setcursor(cursor:wait)
                            save_par_id = access:parts.savefile()
                            access:parts.clearkey(par:part_number_key)
                            par:ref_number  = job:ref_number
                            set(par:part_number_key,par:part_number_key)
                            loop
                                if access:parts.next()
                                   break
                                end !if
                                if par:ref_number  <> job:ref_number      |
                                    then break.  ! end if
                                yldcnt# += 1
                                if yldcnt# > 25
                                   yield() ; yldcnt# = 0
                                end !if
                                If par:date_ordered <> '' and par:date_received = ''
                                    found# = 1
                                Else!If wpr:date_ordered <> and wpr:date_received = 'YES'
                                    If par:pending_ref_number <> '' and par:date_received = ''
                                        found# = 1
                                    End!If wpr:pending_ref_number <> ''
                                End!If wpr:date_ordered <> and wpr:date_received = 'YES'
                            end !loop
                            access:parts.restorefile(save_par_id)
                            setcursor()
                        End!If found# = 0
        
                    End!If job:third_party_site <> '' And job:workshop <> 'YES'
        
                    If glo:select4 = 'YES'
                        Clear(in_repair_queue)
                        Sort(in_repair_queue,repque:model_number)
                        repque:model_number = xch:model_number
                        Get(in_repair_queue,repque:model_number)
                        If workshop# = 1
                            repque:in_workshop += 1
                        End!If job:workshop = 'YES'
                        If rtm# = 1
                            repque:rtm += 1
                        End!If rtm# = 1
                        If found# = 1
                            repque:awaiting_parts += 1
                        End!If found# = 1
                        Put(in_repair_queue)
                    Else!If glo:select4 = 'YES'
                        If workshop# = 1
                            in_workshop_temp += 1
                        End!If job:workshop = 'YES'
                        If rtm# = 1
                            rtm_temp += 1
                        End!If rtm# = 1
                        If found# = 1
                            awaiting_parts_temp += 1
                        End!If found# = 1
        
                    End!If glo:select4 = 'YES'
        
                End!If job:date_completed = ''
            end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
        
        end !loop
        access:exchange.restorefile(save_xch_id)
        setcursor()
        
        RecordsToProcess += Records(glo:Queue)
        !If One Model Per Page
        If glo:select4 = 'YES'
        
            first_page# = 1
            Sort(glo:Queue,glo:pointer)                                                       !Loop through the global list of models
            Loop x# = 1 to Records(glo:Queue)                                                    !so that even if there aren't any records
                Get(glo:Queue,x#)
                recordsprocessed += 1
                Do DisplayProgress                                                   !in the exchange queue, a page will still
                in_stock_temp = 0
                in_workshop_temp = 0
                Awaiting_Parts_Temp = 0
                RTM_Temp = 0
                requests_received_temp  = 0
                same_day_temp           = 0
                other_day_temp          = 0
                received_in_workshop_temp = 0
                Clear(exchange_queue)
                Sort(exchange_queue,excque:model_number)                                            !be printed for the in stock bit.
                excque:model_number = glo:pointer
                Get(exchange_queue,excque:model_number)
                model_number_temp       = excque:model_number
                requests_received_temp  = excque:request_received
                same_day_temp           = excque:same_day
                other_day_temp          = excque:other_day
                received_in_workshop_temp = excque:received_in_workshop
                Failed_Exchange_Temp = requests_received_temp - same_day_temp - other_day_temp
                Despatched_Temp = same_day_temp + other_day_temp
                Awaiting_Arrival_Temp = Despatched_Temp - Received_In_Workshop_Temp
                Clear(in_repair_queue)
                Sort(in_repair_queue,repque:model_number)
                Loop y# = 1 To Records(in_repair_queue)
                    Get(in_repair_queue,y#)
                    If repque:model_number <> excque:model_number
                        Cycle
                    End
                    in_stock_temp   += repque:in_stock
                    in_workshop_temp    += repque:in_workshop
                    awaiting_parts_temp += repque:awaiting_parts
                    rtm_temp    += repque:RTM
                End!Loop x# = 1 To Records(in_repair_queue)
                In_Repair_Total = In_Stock_Temp + In_Workshop_Temp + Awaiting_Parts_Temp + RTM_Temp
                In_Channel_Temp = awaiting_arrival_temp + in_repair_total
                tmp:RecordsCount += 1
                Print(rpt:Detail)
                If x# <> Records(glo:Queue)
                    Print(rpt:new_page)
                End!If first_page# = 1
            End!Loop x# = 1 To Records(exchange_queue)
        
        Else!If glo:select4 = 'YES'
            Failed_Exchange_Temp = requests_received_temp - same_day_temp - other_day_temp
            Despatched_Temp = same_day_temp + other_day_temp
            Awaiting_Arrival_Temp = Despatched_Temp - Received_In_Workshop_Temp
            In_Repair_Total = In_Stock_Temp + In_Workshop_Temp + Awaiting_Parts_Temp + RTM_Temp
            In_Channel_Temp = awaiting_arrival_temp + in_repair_total
            tmp:RecordsCount += 1
            Print(rpt:Detail)
        End!If glo:select4 = 'YES'
        
        setcursor()
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
    Relate:TRANTYPE.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Exchange_Units_In_Channel_Fix_Later'
  END
  report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Exchange_Units_In_Channel_Fix_Later',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('save_job_id',save_job_id,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('InitialPath',InitialPath,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('Exchange_Queue:model_number',Exchange_Queue:model_number,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('Exchange_Queue:request_received',Exchange_Queue:request_received,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('Exchange_Queue:same_day',Exchange_Queue:same_day,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('Exchange_Queue:other_day',Exchange_Queue:other_day,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('Exchange_Queue:received_in_workshop',Exchange_Queue:received_in_workshop,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('In_Repair_Queue:model_number',In_Repair_Queue:model_number,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('In_Repair_Queue:in_stock',In_Repair_Queue:in_stock,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('In_Repair_Queue:in_workshop',In_Repair_Queue:in_workshop,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('In_Repair_Queue:awaiting_parts',In_Repair_Queue:awaiting_parts,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('In_Repair_Queue:RTM',In_Repair_Queue:RTM,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('first_page_temp',first_page_temp,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('save_par_id',save_par_id,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('save_xch_id',save_xch_id,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('account_number_temp',account_number_temp,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('model_number_temp',model_number_temp,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('requests_received_temp',requests_received_temp,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('same_day_temp',same_day_temp,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('other_day_temp',other_day_temp,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('Failed_Exchange_Temp',Failed_Exchange_Temp,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('Despatched_Temp',Despatched_Temp,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('In_Workshop_Temp',In_Workshop_Temp,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('Awaiting_Arrival_Temp',Awaiting_Arrival_Temp,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('In_Stock_Temp',In_Stock_Temp,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('Units_In_Workshop_Temp',Units_In_Workshop_Temp,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('Received_In_Workshop_Temp',Received_In_Workshop_Temp,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('Awaiting_Parts_Temp',Awaiting_Parts_Temp,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('RTM_Temp',RTM_Temp,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('In_Repair_Total',In_Repair_Total,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('In_Channel_Temp',In_Channel_Temp,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('Removed_From_Stock_Temp',Removed_From_Stock_Temp,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('In_Scheme_Temp',In_Scheme_Temp,'Exchange_Units_In_Channel_Fix_Later',1)
    SolaceViewVars('Manufacturer_Temp',Manufacturer_Temp,'Exchange_Units_In_Channel_Fix_Later',1)


BuildCtrlQueue      Routine







