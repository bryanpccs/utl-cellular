

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03045.INC'),ONCE        !Local module procedure declarations
                     END








Nokia_accessories_claim_report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
AccessoryExport      BYTE
AccessoryExportPath  STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
parts_rate_temp      REAL
job_count_temp       REAL
save_wpr_id          USHORT,AUTO
total_jobs_temp      REAL
raised_by_temp       STRING(60)
total_count_temp     REAL
total_cost_temp      REAL
vat_temp             REAL
claim_value_temp     REAL
line_cost_temp       REAL
tmp:PrintedBy        STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:ESN)
                       PROJECT(job:MSN)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Ref_Number)
                     END
Report               REPORT('Accessories Claim Report'),AT(396,2760,7521,8500),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1625),USE(?unnamed)
                         STRING(@s3),AT(5781,1094),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6094,1094),USE(?String48),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6354,1094,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,52),USE(?ReportDatePrompt),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5781,52),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING('Raised By:'),AT(5000,313),USE(?String31),TRN,FONT(,8,,)
                         STRING(@s60),AT(5781,323),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Manufacturer:'),AT(5000,573),USE(?String32),TRN,FONT(,8,,)
                         STRING(@s40),AT(5781,573),USE(GLO:Select3),TRN,FONT(,8,,FONT:bold)
                         STRING('Batch Number:'),AT(5000,823),USE(?String46),TRN,FONT(,8,,)
                         STRING(@s10),AT(5781,833),USE(GLO:Select1),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(5000,1094),USE(?String59),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,167),USE(?DetailBand)
                           STRING(@s8),AT(52,0),USE(job:Ref_Number),TRN,RIGHT,FONT(,7,,)
                           STRING(@s8),AT(677,0),USE(wpr:Quantity),TRN,RIGHT,FONT(,7,,)
                           STRING(@s20),AT(1302,0),USE(wpr:Part_Number),TRN,LEFT,FONT(,7,,)
                           STRING(@s20),AT(2448,0),USE(wpr:Description),TRN,FONT(,7,,)
                           STRING(@s20),AT(3594,0),USE(job:Model_Number),TRN,LEFT,FONT(,7,,)
                           STRING(@s16),AT(4740,0),USE(job:ESN),TRN,LEFT,FONT(,7,,)
                           STRING(@n-14.2),AT(6656,0),USE(line_cost_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@s16),AT(5677,0),USE(job:MSN),TRN,LEFT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,2490),USE(?unnamed:2)
                           LINE,AT(167,52,7188,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Quantity: '),AT(208,156),USE(?String34),TRN,FONT(,8,,)
                           STRING(@s9),AT(1146,156),USE(total_count_temp),TRN,LEFT,FONT(,8,,)
                           STRING('Total:'),AT(5469,156),USE(?Total),TRN,FONT(,8,,)
                           STRING(@n-14.2),AT(6583,156),USE(total_cost_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('V.A.T.'),AT(5469,365),USE(?VAT),TRN,FONT(,8,,)
                           STRING(@n-14.2),AT(6583,365),USE(vat_temp),TRN,RIGHT,FONT(,8,,)
                           LINE,AT(5417,573,1979,0),USE(?Line2),COLOR(COLOR:Black)
                           STRING('Claim Value:'),AT(5469,625),USE(?ClaimValue),TRN,FONT(,8,,)
                           STRING(@n-14.2),AT(6583,625),USE(claim_value_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('I confirm that the accessories listed above were replaced under the terms of the' &|
   ' manufacturer''s warranty '),AT(281,1344),USE(?String41),TRN,FONT(,10,,FONT:bold)
                           STRING('claim process.'),AT(281,1552),USE(?String42),TRN,FONT(,10,,FONT:bold)
                           STRING('Signed By'),AT(281,2177),USE(?String43),TRN,FONT(,,,FONT:bold)
                           STRING('Write Name'),AT(3250,2177),USE(?String44),TRN,FONT(,10,,FONT:bold)
                           STRING('Date'),AT(5906,2177),USE(?String45),TRN,FONT(,10,,FONT:bold)
                           LINE,AT(260,2396,7083,0),USE(?Line3),COLOR(COLOR:Black)
                         END
                       END
                       FOOTER,AT(406,11417,7521,240),USE(?unnamed:4)
                       END
                       FORM,AT(396,469,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('WARRANTY ACCESSORIES CLAIM REPORT'),AT(3906,52,3594,260),USE(?String20),TRN,RIGHT,FONT(,12,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,240),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,240),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,240),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1094),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1094),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1250),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1250),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1406,3844,240),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1406),USE(?String19:2),TRN,FONT(,9,,)
                         STRING('Job No'),AT(156,2083),USE(?String24),TRN,FONT(,8,,FONT:bold)
                         STRING('Qty'),AT(781,2083),USE(?String25),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(1302,2083),USE(?String26),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(2448,2083),USE(?String27),TRN,FONT(,8,,FONT:bold)
                         STRING('Model Number'),AT(3594,2083),USE(?String28),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. No.'),AT(4740,2083),USE(?String29),TRN,FONT(,8,,FONT:bold)
                         STRING('Cost'),AT(7135,2083),USE(?Cost),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(5677,2083),USE(?MSN),TRN,FONT(,8,,FONT:bold)
                       END
                     END
! Before Embed Point: %DataSectionAfterReport) DESC(Data Section, After Report Declaration) ARG()
! Logfile Declarations                          ! Start Change 4591 BE(24/09/2004)
CSVFILE               FILE,DRIVER('BASIC'),OEM,PRE(csv),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
JobNo                     STRING(8)
PartNumber                STRING(30)
PartDescription           STRING(30)
PurchaseCost              STRING(14)
TradePrice                STRING(14)
ModelNo                   STRING(30)
IMEI                      STRING(20)
MSN                       STRING(20)
DoP                       STRING(10)
                         END
                     END

! End Change 4591 BE(24/09/2004)
! After Embed Point: %DataSectionAfterReport) DESC(Data Section, After Report Declaration) ARG()
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
SpinCnt1                BYTE
SpinCnt2                BYTE
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Nokia_accessories_claim_report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  BIND('GLO:Select3',GLO:Select3)
  BIND('GLO:Select3',GLO:Select3)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:VATCODE.Open
  Access:WARPARTS.UseFile
  Access:STOCK.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  
  
  RecordsToProcess = 100
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Spinner:Ctl)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  ! Before Embed Point: %AfterOpeningWindow) DESC(After Opening Progress Window) ARG()
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  ?Progress:UserString{prop:text} = 'Working... Please be patient.'
  ! After Embed Point: %AfterOpeningWindow) DESC(After Opening Progress Window) ARG()
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(job:EDI_Key)
      Process:View{Prop:Filter} = |
      'UPPER(job:Manufacturer) = UPPER(GLO:Select3) AND (job:edi = ''YES'' AND ' & |
      'job:edi_batch_number = GLO:Select1)'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        if access:subtracc.fetch(sub:account_number_key) = level:benign
            access:tradeacc.clearkey(tra:account_number_key) 
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = level:benign
                if tra:invoice_sub_accounts = 'YES'
                    access:vatcode.clearkey(vat:vat_code_key)
                    vat:vat_code = sub:parts_vat_code
                    if access:vatcode.fetch(vat:vat_code_key) = level:benign
                        parts_rate_temp = vat:vat_rate
                    end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                else!if tra:use_sub_accounts = 'YES'
                    access:vatcode.clearkey(vat:vat_code_key)
                    vat:vat_code = tra:parts_vat_code
                    if access:vatcode.fetch(vat:vat_code_key) = level:benign
                        parts_rate_temp = vat:vat_rate
                    end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                end!if tra:use_sub_accounts = 'YES'
            end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
        end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
        
        setcursor(cursor:wait)
        
        save_wpr_id = access:warparts.savefile()
        access:warparts.clearkey(wpr:part_number_key)
        wpr:ref_number  = job:ref_number
        set(wpr:part_number_key,wpr:part_number_key)
        loop
            if access:warparts.next()
               break
            end !if
            if wpr:ref_number  <> job:ref_number      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            access:stock.clearkey(sto:ref_number_key)
            sto:ref_number = wpr:part_ref_number
            if access:stock.fetch(sto:ref_number_key) = Level:Benign
                If sto:accessory = 'YES'
                    total_count_temp += 1
                    line_cost_temp = wpr:quantity * wpr:purchase_cost
                    total_cost_temp += line_cost_temp
                    vat_temp += (line_cost_temp * parts_rate_temp/100)
                    claim_value_temp += line_cost_temp + (line_cost_temp * parts_rate_temp/100)
                    tmp:RecordsCount += 1
                    Print(rpt:detail)
        
                    ! Start Change 4591 BE(24/09/2004)
                    CLEAR(CSVFILE)
                    csv:JobNo           = job:ref_number
                    csv:PartNumber      = sto:part_number
                    csv:PartDescription = sto:description
                    csv:PurchaseCost    = LEFT(FORMAT(sto:purchase_cost, @n14.2))
                    csv:TradePrice      = LEFT(FORMAT(sto:sale_cost, @n14.2))
                    csv:ModelNo         = job:model_number
                    csv:IMEI            = job:ESN
                    csv:MSN             = job:MSN
                    csv:DoP             = FORMAT(job:dop, @d06b)
                    ADD(CSVFILE)
                    ! End Change 4591 BE(24/09/2004)
                End
            end
        end !loop
        access:warparts.restorefile(save_wpr_id)
        setcursor()
        
        
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        OMIT('**End Omit Nothing Message**')
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
        OMIT('**End Omit Nothing Message**')
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        OMIT('**End Omit Nothing Message**')
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
        OMIT('**End Omit Nothing Message**')
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  ! Before Embed Point: %AfterClosingReport) DESC(After Closing Report) ARG()
  ! Start Change 4591 BE(24/09/2004)
  IF (AccessoryExport) THEN
      CLOSE(CSVFILE)
      IF (tmp:RecordsCount = 0) THEN
          REMOVE(CSVFILE)
      END
  END
  ! End Change 4591 BE(24/09/2004)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  ! After Embed Point: %AfterClosingReport) DESC(After Closing Report) ARG()
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:VATCODE.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  ?Progress:PctText{Prop:Text} = RecordsProcessed & '  ' & CPCS:ProgWinRecText
  DisplayProgress = True
  SpinCnt1 += 1
  IF SpinCnt1 = 1
    SpinCnt1 = 0
    SpinCnt2 += 1
    IF SpinCnt2 > 4; SpinCnt2 = 1.
    EXECUTE SpinCnt2
      ?Spinner:Ctl{PROP:Text}='/'
      ?Spinner:Ctl{PROP:Text}='---'
      ?Spinner:Ctl{PROP:Text}='\'
      ?Spinner:Ctl{PROP:Text}='|'
    END
    DisplayProgress = True
  END
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
  man:Manufacturer    = glo:Select3
  If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      !Found
      If man:RemAccCosts
          Settarget(Report)
          ?line_cost_temp{prop:hide} = 1
          ?Total{prop:hide} = 1
          ?VAT{prop:hide} = 1
          ?ClaimValue{prop:hide} = 1
          ?Total_Cost_Temp{prop:hide} = 1
          ?Vat_Temp{prop:hide} = 1
          ?Claim_Value_Temp{prop:hide} = 1
          ?Line2{prop:hide} = 1
          ?Cost{prop:hide} = 1
          Settarget()
      End!If man:RemAccCosts
  
      ! Start Change 4591 BE(24/09/2004)
      AccessoryExport = GETINI(CLIP(man:Manufacturer), 'AccessoryExport',, CLIP(PATH()) & '\SB2KDEF.INI')
      AccessoryExportPath = GETINI(CLIP(man:Manufacturer), 'AccessoryExportPath',, CLIP(PATH()) & '\SB2KDEF.INI')
  
      IF (AccessoryExport) THEN
          AccessoryExportPath = CLIP(AccessoryExportPath) & '\' & CLIP(man:Manufacturer) & CLIP(glo:select1) & '.csv'
  
          CSVFILE{PROP:NAME} = CLIP(AccessoryExportPath)
  
          IF (EXISTS(AccessoryExportPath)) THEN
              REMOVE(CSVFILE)
              CREATE(CSVFILE)
          ELSE
              CREATE(CSVFILE)
          END
  
          OPEN(CSVFILE)
          IF (ERRORCODE()) THEN
              MESSAGE(ERROR())
          END
  
      END
      ! End Change 4591 BE(24/09/2004)
  
  Else! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Nokia_accessories_claim_report'
  END
  Report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Nokia_accessories_claim_report',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('AccessoryExport',AccessoryExport,'Nokia_accessories_claim_report',1)
    SolaceViewVars('AccessoryExportPath',AccessoryExportPath,'Nokia_accessories_claim_report',1)
    SolaceViewVars('RejectRecord',RejectRecord,'Nokia_accessories_claim_report',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Nokia_accessories_claim_report',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Nokia_accessories_claim_report',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Nokia_accessories_claim_report',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Nokia_accessories_claim_report',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Nokia_accessories_claim_report',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Nokia_accessories_claim_report',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Nokia_accessories_claim_report',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Nokia_accessories_claim_report',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Nokia_accessories_claim_report',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Nokia_accessories_claim_report',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Nokia_accessories_claim_report',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Nokia_accessories_claim_report',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Nokia_accessories_claim_report',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Nokia_accessories_claim_report',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Nokia_accessories_claim_report',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Nokia_accessories_claim_report',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Nokia_accessories_claim_report',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Nokia_accessories_claim_report',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Nokia_accessories_claim_report',1)
    SolaceViewVars('InitialPath',InitialPath,'Nokia_accessories_claim_report',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Nokia_accessories_claim_report',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Nokia_accessories_claim_report',1)
    SolaceViewVars('parts_rate_temp',parts_rate_temp,'Nokia_accessories_claim_report',1)
    SolaceViewVars('job_count_temp',job_count_temp,'Nokia_accessories_claim_report',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Nokia_accessories_claim_report',1)
    SolaceViewVars('total_jobs_temp',total_jobs_temp,'Nokia_accessories_claim_report',1)
    SolaceViewVars('raised_by_temp',raised_by_temp,'Nokia_accessories_claim_report',1)
    SolaceViewVars('total_count_temp',total_count_temp,'Nokia_accessories_claim_report',1)
    SolaceViewVars('total_cost_temp',total_cost_temp,'Nokia_accessories_claim_report',1)
    SolaceViewVars('vat_temp',vat_temp,'Nokia_accessories_claim_report',1)
    SolaceViewVars('claim_value_temp',claim_value_temp,'Nokia_accessories_claim_report',1)
    SolaceViewVars('line_cost_temp',line_cost_temp,'Nokia_accessories_claim_report',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Nokia_accessories_claim_report',1)


BuildCtrlQueue      Routine







