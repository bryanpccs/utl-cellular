

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03011.INC'),ONCE        !Local module procedure declarations
                     END








Status_Summary_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:status           STRING(30)
tmp:number           LONG
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_job_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
status_queue_temp    QUEUE,PRE(STAQUE)
Status               STRING(30)
Number_Temp          REAL
                     END
tmp:ExchangeStatusQueue QUEUE,PRE(excque)
Status               STRING(30)
StatusNumber         LONG
                     END
tmp:LoanStatusQueue  QUEUE,PRE(loaque)
Status               STRING(30)
StatusNumber         LONG
                     END
job_count_temp       REAL
total_jobs_temp      REAL
tmp:PrintedBy        STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                     END
Report               REPORT('Status Summary Report'),AT(396,2760,7521,8531),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1000),USE(?unnamed)
                         STRING('Date Range: '),AT(5000,156),USE(?String22),TRN,FONT(,8,,)
                         STRING(@s3),AT(5781,781),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(5781,156),USE(GLO:Select1),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(6615,156),USE(GLO:Select2),TRN,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(5000,365),USE(?String24),TRN,FONT(,8,,)
                         STRING(@s60),AT(5781,365),USE(tmp:PrintedBy),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,573),USE(?ReportDatePrompt),TRN,FONT(,8,,)
                         STRING(@d6),AT(5781,573),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(' to '),AT(6406,156),USE(?String23),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(5000,781),USE(?String59),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,229),USE(?DetailBand)
                           STRING(@s30),AT(2115,0),USE(tmp:status),TRN,LEFT
                           STRING(@s6),AT(4813,0),USE(tmp:number),TRN,RIGHT
                         END
totals                   DETAIL,USE(?totals)
                           LINE,AT(2031,52,3438,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Jobs:'),AT(2115,135),USE(?String21),TRN
                           STRING(@s9),AT(4594,135),USE(total_jobs_temp),TRN,RIGHT,FONT(,,,FONT:bold)
                         END
job_status_title         DETAIL,AT(,,,438),USE(?job_status_title)
                           STRING('Job Status'),AT(3417,0),USE(?String21:2),TRN,FONT(,,,FONT:bold)
                         END
exchange_status_title    DETAIL,PAGEBEFORE(-1),AT(,,,490),USE(?exchange_status_title)
                           STRING('Exchange Status'),AT(3208,0),USE(?String21:3),TRN,FONT(,,,FONT:bold)
                         END
loan_status_title        DETAIL,PAGEBEFORE(-1),AT(,,,469),USE(?Loan_status_title)
                           STRING('Loan Status'),AT(3375,0),USE(?String21:4),TRN,FONT(,,,FONT:bold)
                         END
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING('SUMMARY STATUS REPORT'),AT(4271,0,3177,260),USE(?String20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,885),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,885),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1042),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1198,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?String19:2),TRN,FONT(,9,,)
                         STRING('Status Type'),AT(2135,2083),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('No Of Entries'),AT(4792,2083),USE(?String45),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Status_Summary_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      RecordsToProcess = Records(Jobs)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        save_job_id = access:jobs.savefile()
        access:jobs.clearkey(job:date_booked_key)
        job:date_booked = glo:select1
        set(job:date_booked_key,job:date_booked_key)
        loop
            if access:jobs.next()
               break
            end !if
            RecordsProcessed += 1
            Do DisplayProgress
            if job:date_booked > glo:select2     |
                then break.  ! end if
        
            Sort(status_queue_temp,STAQUE:Status)
            staque:status = job:current_status
            Get(status_queue_temp,staque:status)
            If Error()
                staque:status = job:current_status
                staque:number_temp = 1
                Add(status_queue_temp)
            Else!If Error()
                staque:number_temp += 1
                Put(status_queue_temp)
            End!If Error()
        
            Sort(tmp:ExchangeStatusQueue,excque:status)
            excque:status   = job:exchange_status
            Get(tmp:exchangeStatusQueue,excque:status)
            if Error()
                excque:status   = job:exchange_status
                excque:StatusNumber = 1
                Add(tmp:ExchangeStatusQueue)
            Else!if Error()
                excque:StatusNumber += 1
                Put(tmp:ExchangeStatusQueue)
            End!if Error()
        
            Sort(tmp:LoanStatusQueue,loaque:status)
            loaque:status   = job:loan_status
            Get(tmp:LoanStatusQueue,loaque:status)
            if Error()
                loaque:status   = job:Loan_status
                loaque:StatusNumber = 1
                Add(tmp:LoanStatusQueue)
            Else!if Error()
                loaque:StatusNumber += 1
                Put(tmp:LoanStatusQueue)
            End!if Error()
        
        
        end !loop
        access:jobs.restorefile(save_job_id)
        
        RecordsToProcess = Records(status_queue_temp) + Records(tmp:ExchangeStatusQueue) + Records(tmp:LoanStatusQueue)
        
        If Records(status_queue_temp)
            Print(rpt:job_status_title)
            total_jobs_temp = 0
            Loop x# = 1 To Records(status_queue_temp)
                Get(status_queue_temp,x#)
                RecordsProcessed += 1
                Do DisplayProgress
                total_jobs_temp += staque:number_temp
                tmp:RecordsCount += 1
                tmp:status  = staque:status
                tmp:number  = staque:number_temp
                Print(rpt:detail)
            End!Loop x# = 1 To Records(status_queue_temp)
            Print(rpt:totals)
        End!If Records(status_queue_temp)
        
        If Records(tmp:ExchangeStatusQueue)
            Print(rpt:exchange_status_title)
            total_jobs_temp = 0
            Loop x# = 1 To Records(tmp:ExchangeStatusQueue)
                RecordsProcessed += 1
                Do DisplayProgress
                Get(tmp:ExchangeStatusQueue,x#)
                total_jobs_temp += excque:StatusNUmber
                tmp:RecordsCount += 1
                tmp:status  = excque:status
                tmp:number  = excque:statusNumber
                Print(rpt:detail)
            End!Loop x# = 1 To Records(tmp:ExchangeStatusQueueu)
            Print(rpt:totals)
        End!If Records(tmp:ExchangeStatusQueue)
        
        If Records(tmp:LoanStatusQueue)
            Print(rpt:Loan_status_title)
            total_jobs_temp = 0
            Loop x# = 1 To Records(tmp:LoanStatusQueue)
                RecordsProcessed += 1
                Do DisplayProgress
                Get(tmp:LoanStatusQueue,x#)
                total_jobs_temp += loaque:StatusNUmber
                tmp:RecordsCount += 1
                tmp:status  = loaque:status
                tmp:number  = loaque:statusnumber
                Print(rpt:detail)
            End!Loop x# = 1 To Records(tmp:ExchangeStatusQueueu)
            Print(rpt:totals)
        End!If Records(tmp:ExchangeStatusQueue)
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Status_Summary_Report'
  END
  Report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Status_Summary_Report',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Status_Summary_Report',1)
    SolaceViewVars('tmp:status',tmp:status,'Status_Summary_Report',1)
    SolaceViewVars('tmp:number',tmp:number,'Status_Summary_Report',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Status_Summary_Report',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Status_Summary_Report',1)
    SolaceViewVars('save_job_id',save_job_id,'Status_Summary_Report',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Status_Summary_Report',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Status_Summary_Report',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Status_Summary_Report',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Status_Summary_Report',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Status_Summary_Report',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Status_Summary_Report',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Status_Summary_Report',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Status_Summary_Report',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Status_Summary_Report',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Status_Summary_Report',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Status_Summary_Report',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Status_Summary_Report',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Status_Summary_Report',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Status_Summary_Report',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Status_Summary_Report',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Status_Summary_Report',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Status_Summary_Report',1)
    SolaceViewVars('InitialPath',InitialPath,'Status_Summary_Report',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Status_Summary_Report',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Status_Summary_Report',1)
    SolaceViewVars('status_queue_temp:Status',status_queue_temp:Status,'Status_Summary_Report',1)
    SolaceViewVars('status_queue_temp:Number_Temp',status_queue_temp:Number_Temp,'Status_Summary_Report',1)
    SolaceViewVars('tmp:ExchangeStatusQueue:Status',tmp:ExchangeStatusQueue:Status,'Status_Summary_Report',1)
    SolaceViewVars('tmp:ExchangeStatusQueue:StatusNumber',tmp:ExchangeStatusQueue:StatusNumber,'Status_Summary_Report',1)
    SolaceViewVars('tmp:LoanStatusQueue:Status',tmp:LoanStatusQueue:Status,'Status_Summary_Report',1)
    SolaceViewVars('tmp:LoanStatusQueue:StatusNumber',tmp:LoanStatusQueue:StatusNumber,'Status_Summary_Report',1)
    SolaceViewVars('job_count_temp',job_count_temp,'Status_Summary_Report',1)
    SolaceViewVars('total_jobs_temp',total_jobs_temp,'Status_Summary_Report',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Status_Summary_Report',1)


BuildCtrlQueue      Routine







