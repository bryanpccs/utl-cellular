

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03052.INC'),ONCE        !Local module procedure declarations
                     END








Standard_Letter PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:printedby        STRING(60)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:CustomerName     STRING(60)
NameAddrPrtGroup     GROUP,PRE()
NameAddrPrtLine1     STRING(50)
NameAddrPrtLine2     STRING(50)
NameAddrPrtLine3     STRING(50)
NameAddrPrtLine4     STRING(50)
NameAddrPrtLine5     STRING(50)
NameAddrPrtLine6     STRING(50)
                     END
tmp:UnitDetails      STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(LETTERS)
                       PROJECT(let:FaxNumber)
                       PROJECT(let:LetterText)
                       PROJECT(let:Subject)
                       PROJECT(let:TelephoneNumber)
                     END
report               REPORT,AT(406,167,7521,11250),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
endofreportbreak       BREAK(endofreport),USE(?unnamed)
detail                   DETAIL,AT(,,,11198),USE(?detailband)
                           STRING(@s60),AT(365,1719),USE(tmp:CustomerName),TRN,FONT(,12,,,CHARSET:ANSI)
                           STRING(@d6b),AT(6333,1667),USE(ReportRunDate),TRN,RIGHT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Date:'),AT(5521,1667),USE(?String26),TRN,FONT(,12,,,CHARSET:ANSI)
                           GROUP,AT(156,625,3125,625),USE(?NameAddrPrtGroup)
                             STRING(@s50),AT(365,2156,2917,229),USE(NameAddrPrtLine2),TRN,FONT(,12,,,CHARSET:ANSI)
                             STRING(@s50),AT(365,2365,2917,229),USE(NameAddrPrtLine3),TRN,FONT(,12,,,CHARSET:ANSI)
                             STRING(@s50),AT(365,2573,2917,229),USE(NameAddrPrtLine4),TRN,FONT(,12,,,CHARSET:ANSI)
                             STRING(@s50),AT(365,2781,2917,229),USE(NameAddrPrtLine5),TRN,FONT(,12,,,CHARSET:ANSI)
                             STRING(@s50),AT(365,1948,2917,229),USE(NameAddrPrtLine1),TRN,FONT(,12,,,CHARSET:ANSI)
                             STRING(@s50),AT(365,2990,2917,229),USE(NameAddrPrtLine6),TRN,FONT(,12,,,CHARSET:ANSI)
                           END
                           STRING('Subject:'),AT(365,3333),USE(?String17),TRN,FONT(,12,,FONT:underline)
                           STRING(@s255),AT(1302,3333,6302,208),USE(let:Subject),LEFT,FONT(,12,,FONT:bold+FONT:underline)
                           STRING('Unit Details:'),AT(365,3646),USE(?String20),TRN,FONT(,12,,,CHARSET:ANSI)
                           STRING(@s80),AT(1302,3646),USE(tmp:UnitDetails),FONT(,12,,FONT:bold,CHARSET:ANSI)
                           STRING('Mobile No:'),AT(365,3906),USE(?String20:2),TRN,FONT(,12,,,CHARSET:ANSI)
                           STRING(@s15),AT(1302,3906),USE(job:Mobile_Number),TRN,LEFT,FONT(,12,,FONT:bold,CHARSET:ANSI)
                           STRING('Our Ref:'),AT(365,4167),USE(?String20:3),TRN,FONT(,12,,,CHARSET:ANSI)
                           STRING(@s8),AT(1302,4167),USE(job:Ref_Number),TRN,LEFT,FONT(,12,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(3802,52,3438,208),USE(def:User_Name),TRN,RIGHT,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4323,313,2917,229),USE(def:Address_Line1),TRN,RIGHT,FONT(,12,,)
                           STRING(@s30),AT(4323,521,2917,229),USE(def:Address_Line2),TRN,RIGHT,FONT(,12,,)
                           STRING(@s30),AT(4323,729,2917,229),USE(def:Address_Line3),TRN,RIGHT,FONT(,12,,)
                           STRING(@s15),AT(4323,938,2917,229),USE(def:Postcode),TRN,RIGHT,FONT(,12,,)
                           TEXT,AT(365,4531,6875,6563),USE(let:LetterText),TRN,FONT(,12,,,CHARSET:ANSI)
                           STRING('Tel:'),AT(5521,1250),USE(?string16),TRN,FONT(,12,,)
                           STRING(@s15),AT(5521,1250,1719,260),USE(let:TelephoneNumber),TRN,RIGHT,FONT(,12,,)
                           STRING('Fax: '),AT(5521,1458),USE(?string19),TRN,FONT(,12,,)
                           STRING(@s15),AT(5521,1458,1719,260),USE(let:FaxNumber),TRN,RIGHT,FONT(,12,,)
                         END
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Standard_Letter')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select2',GLO:Select2)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:LETTERS.Open
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Access:JOBS.UseFile
  
  
  RecordsToProcess = RECORDS(LETTERS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(LETTERS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(let:DescriptionKey)
      Process:View{Prop:Filter} = |
      'UPPER(let:Description) = UPPER(GLO:Select2)'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        DO NameAddrPrt1Assign
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(rpt:detail)
        END
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(LETTERS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:LETTERS.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'LETTERS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','LETTER')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'LETTER'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  
  
  If glo:select1 <> 'TEST'
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number  = glo:select1
      If access:jobs.tryfetch(job:ref_number_key)
          Do ProcedureReturn
      End!If access:jobs.tryfetch(job:ref_number_key)
  
      tmp:UnitDetails = Clip(job:manufacturer) & ' ' & Clip(job:model_number) & ' - ' & CLip(job:esn)
  
      if job:title = '' and job:initial = ''
          tmp:customername = clip(job:surname)
      elsif job:title = '' and job:initial <> ''
          tmp:customername = clip(job:initial) & ', ' & clip(job:surname)
      elsif job:title <> '' and job:initial = ''
          tmp:customername = clip(job:title) & ', ' & clip(job:surname)
      elsif job:title <> '' and job:initial <> ''
          tmp:customername = clip(job:title) & ', ' & clip(job:initial) & ' ' & clip(job:surname)
      else
          tmp:customername = ''
      end
  End!If glo:select1 <> 'TEST'
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'LETTER'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Standard_Letter'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
NameAddrPrt1Assign     ROUTINE
  CLEAR(NameAddrPrtGroup)
  NameAddrPrtLine1 = job:Company_Name
  NameAddrPrtLine2 = job:Address_Line1
  NameAddrPrtLine3 = job:Address_Line2
  NameAddrPrtLine4 = job:Address_Line3
  NameAddrPrtLine5 = job:Postcode
  IF ~NameAddrPrtLine5
    NameAddrPrtLine5 = NameAddrPrtLine6
    NameAddrPrtLine6 = ''
  END
  IF ~NameAddrPrtLine4
    NameAddrPrtLine4 = NameAddrPrtLine5
    NameAddrPrtLine5 = NameAddrPrtLine6
    NameAddrPrtLine6 = ''
  END
  IF ~NameAddrPrtLine3
    NameAddrPrtLine3 = NameAddrPrtLine4
    NameAddrPrtLine4 = NameAddrPrtLine5
    NameAddrPrtLine5 = NameAddrPrtLine6
    NameAddrPrtLine6 = ''
  END
  IF ~NameAddrPrtLine2
    NameAddrPrtLine2 = NameAddrPrtLine3
    NameAddrPrtLine3 = NameAddrPrtLine4
    NameAddrPrtLine4 = NameAddrPrtLine5
    NameAddrPrtLine5 = NameAddrPrtLine6
    NameAddrPrtLine6 = ''
  END
  IF ~NameAddrPrtLine1
    NameAddrPrtLine1 = NameAddrPrtLine2
    NameAddrPrtLine2 = NameAddrPrtLine3
    NameAddrPrtLine3 = NameAddrPrtLine4
    NameAddrPrtLine4 = NameAddrPrtLine5
    NameAddrPrtLine5 = NameAddrPrtLine6
    NameAddrPrtLine6 = ''
  END



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Standard_Letter',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Standard_Letter',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Standard_Letter',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Standard_Letter',1)
    SolaceViewVars('tmp:printedby',tmp:printedby,'Standard_Letter',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Standard_Letter',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Standard_Letter',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Standard_Letter',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Standard_Letter',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Standard_Letter',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Standard_Letter',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Standard_Letter',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Standard_Letter',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Standard_Letter',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Standard_Letter',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Standard_Letter',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Standard_Letter',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Standard_Letter',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Standard_Letter',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Standard_Letter',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Standard_Letter',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Standard_Letter',1)
    SolaceViewVars('InitialPath',InitialPath,'Standard_Letter',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Standard_Letter',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Standard_Letter',1)
    SolaceViewVars('tmp:CustomerName',tmp:CustomerName,'Standard_Letter',1)
    SolaceViewVars('NameAddrPrtGroup:NameAddrPrtLine1',NameAddrPrtGroup:NameAddrPrtLine1,'Standard_Letter',1)
    SolaceViewVars('NameAddrPrtGroup:NameAddrPrtLine2',NameAddrPrtGroup:NameAddrPrtLine2,'Standard_Letter',1)
    SolaceViewVars('NameAddrPrtGroup:NameAddrPrtLine3',NameAddrPrtGroup:NameAddrPrtLine3,'Standard_Letter',1)
    SolaceViewVars('NameAddrPrtGroup:NameAddrPrtLine4',NameAddrPrtGroup:NameAddrPrtLine4,'Standard_Letter',1)
    SolaceViewVars('NameAddrPrtGroup:NameAddrPrtLine5',NameAddrPrtGroup:NameAddrPrtLine5,'Standard_Letter',1)
    SolaceViewVars('NameAddrPrtGroup:NameAddrPrtLine6',NameAddrPrtGroup:NameAddrPrtLine6,'Standard_Letter',1)
    SolaceViewVars('tmp:UnitDetails',tmp:UnitDetails,'Standard_Letter',1)


BuildCtrlQueue      Routine







