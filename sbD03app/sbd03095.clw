

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03095.INC'),ONCE        !Local module procedure declarations
                     END








ExchangeReasonReport PROCEDURE(func:StartDate,func:EndDate)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:UnitDetails      STRING(100)
tmp:ExchangeUnitDetails STRING(100)
tmp:TotalJobs        LONG
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBSE)
                       PROJECT(jobe:ExchangeReason)
                       PROJECT(jobe:RefNumber)
                     END
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1000),USE(?unnamed)
                         STRING('Start Date:'),AT(5000,0),USE(?string22),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,0),USE(tmp:StartDate),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@d6),AT(5885,156),USE(tmp:EndDate),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('End Date:'),AT(5000,156),USE(?string22:2),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(5000,313),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,313),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,469),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,469),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,469),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,625),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,625),PAGENO,USE(?reportpageno),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6094,625),USE(?string26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6250,625,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,229),USE(?detailband)
                           STRING(@s100),AT(677,0),USE(tmp:UnitDetails),TRN,LEFT,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@s255),AT(5260,0),USE(jobe:ExchangeReason),LEFT,FONT(,7,,)
                           STRING(@s8),AT(156,0),USE(jobe:RefNumber),TRN,RIGHT(1),FONT('Arial',7,,)
                           STRING(@s100),AT(3021,0),USE(tmp:ExchangeUnitDetails),TRN,LEFT,FONT('Arial',7,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           LINE,AT(260,52,6979,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Jobs:'),AT(208,104),USE(?String32),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(938,104),USE(tmp:TotalJobs),FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('EXCHANGE REASON REPORT'),AT(4063,0,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:TelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:FaxNumber),TRN,FONT(,9,,)
                         STRING('Job No'),AT(156,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Details'),AT(677,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Exchange Reason'),AT(5260,2083),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Exchange Unit Details'),AT(3021,2083),USE(?string45:2),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('ExchangeReasonReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('tmp:StartDate',tmp:StartDate)
  BIND('tmp:EndDate',tmp:EndDate)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  ! Before Embed Point: %BeforeFileOpen) DESC(Beginning of Procedure, Before Opening Files) ARG()
  tmp:StartDate   = func:StartDate
  tmp:EndDate     = func:EndDate
  ! After Embed Point: %BeforeFileOpen) DESC(Beginning of Procedure, Before Opening Files) ARG()
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBSE.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE.Open
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:JOBS.UseFile
  
  
  RecordsToProcess = RECORDS(JOBSE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBSE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(jobe:InWorkshopDateKey)
      Process:View{Prop:Filter} = |
      'jobe:InWorkshopDate >= tmp:StartDate AND jobe:InWorkshopDate <<= tmp:En' & |
      'dDate'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        ! Start Change 2995 BE(29/01/04)
        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number  = jobe:RefNumber
        IF (Access:JOBS.fetch(job:Ref_Number_Key) = Level:Benign) THEN
        ! End Change 2995 BE(29/01/04)
            If job:Exchange_Unit_Number <> 0
                Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                sub:Account_Number  = job:Account_Number
                If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Found
                    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                    tra:Account_Number  = sub:Main_Account_Number
                    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        !Found
                        glo:Pointer = tra:Account_Number
                        Get(glo:Queue,glo:Pointer)
                        If ~Error()
                            tmp:UnitDetails = Clip(job:ESN) & '  ' & Clip(job:Model_Number) & '-' & Clip(job:Manufacturer)
        
                            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                            xch:Ref_Number  = job:Exchange_Unit_Number
                            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                !Found
                                tmp:ExchangeUnitDetails = Clip(xch:ESN) & '  ' & Clip(xch:Model_Number) & '-' & Clip(xch:Manufacturer)
                            Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                !Error
                            End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
        
                            ! Start Change 2995 BE(29/01/04)
                            !Access:JOBSE.Clearkey(jobe:RefNumberKey)
                            !jobe:RefNumber  = job:Ref_Number
                            !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            ! End Change 2995 BE(29/01/04)
                                !Found
                                tmp:TotalJobs += 1
                                Print(rpt:Detail)
                            ! Start Change 2995 BE(29/01/04)
                            !Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            !    !Error
                            !End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            ! End Change 2995 BE(29/01/04)
                        End !If ~Error()
                    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        !Error
                    End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Error
                End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            End !job:Exchange_Unit_Number = 0
        ! Start Change 2995 BE(29/01/04)
        END
        ! End Change 2995 BE(29/01/04)
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBSE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBSE')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Exchange Unit Reason Report'
  END
  report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ExchangeReasonReport',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'ExchangeReasonReport',1)
    SolaceViewVars('LocalRequest',LocalRequest,'ExchangeReasonReport',1)
    SolaceViewVars('LocalResponse',LocalResponse,'ExchangeReasonReport',1)
    SolaceViewVars('FilesOpened',FilesOpened,'ExchangeReasonReport',1)
    SolaceViewVars('WindowOpened',WindowOpened,'ExchangeReasonReport',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'ExchangeReasonReport',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'ExchangeReasonReport',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'ExchangeReasonReport',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'ExchangeReasonReport',1)
    SolaceViewVars('PercentProgress',PercentProgress,'ExchangeReasonReport',1)
    SolaceViewVars('RecordStatus',RecordStatus,'ExchangeReasonReport',1)
    SolaceViewVars('EndOfReport',EndOfReport,'ExchangeReasonReport',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'ExchangeReasonReport',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'ExchangeReasonReport',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'ExchangeReasonReport',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'ExchangeReasonReport',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'ExchangeReasonReport',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'ExchangeReasonReport',1)
    SolaceViewVars('InitialPath',InitialPath,'ExchangeReasonReport',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'ExchangeReasonReport',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'ExchangeReasonReport',1)
    SolaceViewVars('tmp:printedby',tmp:printedby,'ExchangeReasonReport',1)
    SolaceViewVars('tmp:TelephoneNumber',tmp:TelephoneNumber,'ExchangeReasonReport',1)
    SolaceViewVars('tmp:FaxNumber',tmp:FaxNumber,'ExchangeReasonReport',1)
    SolaceViewVars('tmp:StartDate',tmp:StartDate,'ExchangeReasonReport',1)
    SolaceViewVars('tmp:EndDate',tmp:EndDate,'ExchangeReasonReport',1)
    SolaceViewVars('tmp:UnitDetails',tmp:UnitDetails,'ExchangeReasonReport',1)
    SolaceViewVars('tmp:ExchangeUnitDetails',tmp:ExchangeUnitDetails,'ExchangeReasonReport',1)
    SolaceViewVars('tmp:TotalJobs',tmp:TotalJobs,'ExchangeReasonReport',1)


BuildCtrlQueue      Routine







