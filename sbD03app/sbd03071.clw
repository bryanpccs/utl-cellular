

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03071.INC'),ONCE        !Local module procedure declarations
                     END








RepairPerformanceReport PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:defaulttelephone STRING(20)
tmp:defaultfax       STRING(20)
save_job_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:printedby        STRING(60)
tmp:telephonenumber  STRING(20)
tmp:faxnumber        STRING(20)
tmp:daterange        STRING(30)
tmp:total1           LONG
tmp:total2           LONG
tmp:detail           STRING(25),DIM(4)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:total3           LONG
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                     END
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1000),USE(?unnamed)
                         STRING('Printed By:'),AT(5000,365),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,365),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Range:'),AT(5000,156),USE(?string27:4),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,156),USE(tmp:daterange),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,573),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,573),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,573),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
FrontPageTop             DETAIL,AT(,,,2198),USE(?detail1)
                           STRING('REPORT CRITERIA'),AT(3229,104),USE(?String60),TRN,FONT(,9,,FONT:bold)
                           STRING('GENERAL EXCLUSIONS'),AT(365,417),USE(?String60:2),TRN,FONT(,9,,FONT:bold)
                           STRING('REPAIR TYPES EXCLUDED'),AT(365,1875),USE(?String60:3),TRN,FONT(,9,,FONT:bold)
                           STRING('Third Party Repairs:'),AT(469,729),USE(?String62),TRN
                           STRING(@s3),AT(2083,729),USE(GLO:Select20),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s3),AT(2083,938),USE(GLO:Select21),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s3),AT(2083,1146),USE(GLO:Select22),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s3),AT(2083,1354),USE(GLO:Select23),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s3),AT(2083,1563),USE(GLO:Select24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING('Awaiting Parts:'),AT(469,938),USE(?String62:2),TRN
                           STRING('Exchanged:'),AT(469,1146),USE(?String62:3),TRN
                           STRING('Loaned:'),AT(469,1354),USE(?String62:4),TRN
                           STRING('Estimate:'),AT(469,1563),USE(?String62:5),TRN
                         END
detail1                  DETAIL,USE(?unnamed:5)
                           STRING(@s25),AT(469,0),USE(tmp:detail[1]),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s25),AT(2135,0),USE(tmp:detail[2]),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s25),AT(3854,0),USE(tmp:detail[3]),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s25),AT(5573,0),USE(tmp:detail[4]),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         END
TradeTitle               DETAIL,AT(,,,292),USE(?TradeTitle)
                           STRING('TRADE ACCOUNTS INCLUDED'),AT(365,52),USE(?String60:4),TRN,FONT(,9,,FONT:bold)
                         END
ManufactTitle            DETAIL,USE(?ManufactTitle)
                           STRING('MANUFACTURERS INCLUDED'),AT(365,52),USE(?String60:5),TRN,FONT(,9,,FONT:bold)
                         END
TeamsTitle               DETAIL,USE(?TeamsTitle)
                           STRING('TEAMS INCLUDED'),AT(365,52),USE(?String60:6),TRN,FONT(,9,,FONT:bold)
                         END
EngineerTitle            DETAIL,USE(?EngineerTitle)
                           STRING('ENGINEERS INCLUDED'),AT(365,104),USE(?String60:7),TRN,FONT(,9,,FONT:bold)
                         END
FrontPageEnd             DETAIL,PAGEAFTER(-1),AT(,,,52),USE(?FrontPageEnd)
                         END
detail                   DETAIL,AT(,,,7990)
                           STRING('PERIOD'),AT(1823,313),USE(?String35),TRN,FONT(,,,FONT:bold+FONT:underline)
                           STRING('COMPLETED'),AT(4167,313),USE(?String35:2),TRN,FONT(,,,FONT:bold+FONT:underline)
                           STRING('COMPLETED'),AT(3344,2958),USE(?String35:4),TRN,FONT(,,,FONT:bold+FONT:underline)
                           STRING('INCOMPLETE'),AT(4688,2969),USE(?String35:6),TRN,FONT(,,,FONT:bold+FONT:underline)
                           STRING('SUMMARY ANALYSIS'),AT(1823,5833),USE(?String35:5),TRN,FONT(,,,FONT:bold+FONT:underline)
                           STRING('EXCLUSIONS'),AT(1823,2969),USE(?String35:3),TRN,FONT(,,,FONT:bold+FONT:underline)
                           STRING(@s30),AT(4167,573,646,208),USE(GLO:Select1)
                           STRING('Day One:'),AT(1823,833),USE(?String37:2),TRN
                           STRING('Same Day:'),AT(1823,573),USE(?String37),TRN
                           STRING(@s40),AT(4167,833,646,208),USE(GLO:Select2)
                           STRING('Day Two:'),AT(1823,1094),USE(?String37:3),TRN
                           STRING(@s40),AT(4167,1094,646,208),USE(GLO:Select3)
                           STRING('Day Three:'),AT(1823,1354),USE(?String37:4),TRN
                           STRING(@s40),AT(4167,1354,646,208),USE(GLO:Select4)
                           STRING('Day Four:'),AT(1823,1615),USE(?String37:5),TRN
                           STRING('Over 5 Days:'),AT(1823,1875),USE(?String37:6),TRN
                           STRING(@s40),AT(4167,1615,646,208),USE(GLO:Select5)
                           STRING(@s40),AT(4167,1875,646,208),USE(GLO:Select6)
                           LINE,AT(4167,2083,1042,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING(@s8),AT(4167,2135),USE(tmp:total1),LEFT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(4167,6615),USE(tmp:total1,,?tmp:total1:2),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(3333,4740,2135,0),USE(?Line1:2),COLOR(COLOR:Black)
                           STRING(@s8),AT(3333,4844),USE(tmp:total2),LEFT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(4688,4844),USE(tmp:total3),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(4167,6875),USE(tmp:total2,,?tmp:total2:2),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(4167,7135),USE(GLO:Select17),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(5052,6146),USE(GLO:Select16),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(4167,7500),USE(GLO:Select16,,?glo:select16:2),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING('Sent To 3rd Party:'),AT(1823,3229),USE(?String37:7),TRN
                           STRING(@s40),AT(3333,3229,646,208),USE(GLO:Select8)
                           STRING('Awaiting Parts:'),AT(1823,3490),USE(?String37:8),TRN
                           STRING(@s40),AT(3344,3490,646,208),USE(GLO:Select9)
                           STRING(@s40),AT(4688,3750,646,208),USE(GLO:Select27),TRN
                           STRING('Exchanged:'),AT(1823,3750),USE(?String37:9),TRN
                           STRING(@s40),AT(3333,3750,646,208),USE(GLO:Select10)
                           STRING(@s40),AT(4688,4010,646,208),USE(GLO:Select28),TRN
                           STRING(@s40),AT(4688,3229,646,208),USE(GLO:Select25),TRN
                           STRING(@s40),AT(4688,3490,646,208),USE(GLO:Select26),TRN
                           STRING('Loan:'),AT(1823,4010),USE(?String37:10),TRN
                           STRING(@s40),AT(3333,4010,646,208),USE(GLO:Select11)
                           STRING(@s40),AT(4688,4271,646,208),USE(GLO:Select29),TRN
                           STRING('B.E.R.:'),AT(1823,4271),USE(?String37:11),TRN
                           STRING(@s40),AT(3344,4271,646,208),USE(GLO:Select12)
                           STRING(@s40),AT(4688,4531,646,208),USE(GLO:Select30),TRN
                           STRING('Estimated:'),AT(1823,4531),USE(?String37:12),TRN
                           STRING(@s40),AT(3333,4531,646,208),USE(GLO:Select13)
                           STRING('Total Jobs Booked:'),AT(1823,6146),USE(?String37:13),TRN
                           STRING('Completed:'),AT(1823,6615),USE(?String37:14),TRN
                           STRING('Excluded:'),AT(1823,6875),USE(?String37:15),TRN
                           STRING('Incomplete:'),AT(1823,7135),USE(?String37:16),TRN
                           STRING('Total:'),AT(1823,7552),USE(?String37:17),TRN
                           STRING(@s8),AT(5000,7500),USE(GLO:Select16,,?glo:select16:3),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(4167,7396,1563,0),USE(?Line1:3),COLOR(COLOR:Black)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('REPAIR PERFORMANCE REPORT'),AT(4063,0,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s15),AT(521,1042),USE(def:Telephone_Number),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s15),AT(521,1198),USE(def:Fax_Number),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('RepairPerformanceReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Access:USERS.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        Print(rpt:FrontPageTop)
        a# = 0
        Clear(tmp:detail)
        Loop x# = 1 To records(glo:queue)
            Get(glo:queue,x#)
            a# += 1
            If a# > 4
                Print(rpt:detail1)
                Clear(tmp:detail)
                a# = 1
            End!If a# > 4
            tmp:detail[a#]  = glo:pointer
        End!Loop x# = 1 To records(glo:queue)
        If Records(glo:queue)
            Print(rpt:detail1)
        End!If a# < 4
        
        Print(rpt:tradetitle)
        a# = 0
        Clear(tmp:detail)
        Loop x# = 1 To records(glo:queue2)
            Get(glo:queue2,x#)
            a# += 1
            If a# > 4
                Print(rpt:detail1)
                Clear(tmp:detail)
                a# = 1
            End!If a# > 4
            tmp:detail[a#]  = glo:pointer2
        End!Loop x# = 1 To records(glo:queue)
        If Records(glo:queue2)
            Print(rpt:detail1)
        End!If a# < 4
        
        Print(rpt:manufacttitle)
        a# = 0
        Clear(tmp:detail)
        Loop x# = 1 To records(glo:queue3)
            Get(glo:queue3,x#)
            a# += 1
            If a# > 4
                Print(rpt:detail1)
                Clear(tmp:detail)
                a# = 1
            End!If a# > 4
            tmp:detail[a#]  = glo:pointer3
        End!Loop x# = 1 To records(glo:queue)
        If Records(glo:queue3)
            Print(rpt:detail1)
        End!If a# < 4
        
        Print(rpt:teamstitle)
        a# = 0
        Clear(tmp:detail)
        Loop x# = 1 To records(glo:queue4)
            Get(glo:queue4,x#)
            a# += 1
            If a# > 4
                Print(rpt:detail1)
                Clear(tmp:detail)
                a# = 1
            End!If a# > 4
            tmp:detail[a#]  = glo:pointer4
        End!Loop x# = 1 To records(glo:queue)
        If Records(glo:queue4)
            Print(rpt:detail1)
        End!If a# < 4
        
        Print(rpt:engineertitle)
        a# = 0
        Clear(tmp:detail)
        Loop x# = 1 To records(glo:queue5)
            Get(glo:queue5,x#)
            a# += 1
            If a# > 4
                Print(rpt:detail1)
                Clear(tmp:detail)
                a# = 1
            End!If a# > 4
            access:users.clearkey(use:user_code_key)
            use:user_code = glo:pointer5
            if access:users.tryfetch(use:user_code_key) = Level:Benign
                tmp:detail[a#]  = Clip(glo:pointer5) & ' - ' & Clip(use:surname) & ', ' & Clip(use:forename)
            End!if access:users.tryfetch(use:user_code_key) = Level:Benign
        End!Loop x# = 1 To records(glo:queue)
        If Records(glo:queue5)
            Print(rpt:detail1)
        End!If a# < 4
        
        
        Print(rpt:FrontPageEnd)
        Print(rpt:detail)
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBS.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  tmp:daterange   = Format(glo:select14,@d6) & ' - ' & Format(glo:select15,@d6)
  tmp:total1  = glo:select1 + glo:select2 + glo:select3 + glo:select4 + glo:select5 + glo:select6
  tmp:total2  = glo:select8 + glo:select9 + glo:select10 + glo:select11 + glo:select12 + glo:select13
  tmp:total3  = glo:select25 + glo:Select26 + glo:select27 + glo:select28 + glo:select29 + glo:select30
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='RepairPerformanceReport'
  END
  report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'RepairPerformanceReport',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'RepairPerformanceReport',1)
    SolaceViewVars('tmp:defaulttelephone',tmp:defaulttelephone,'RepairPerformanceReport',1)
    SolaceViewVars('tmp:defaultfax',tmp:defaultfax,'RepairPerformanceReport',1)
    SolaceViewVars('save_job_id',save_job_id,'RepairPerformanceReport',1)
    SolaceViewVars('save_par_id',save_par_id,'RepairPerformanceReport',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'RepairPerformanceReport',1)
    SolaceViewVars('LocalRequest',LocalRequest,'RepairPerformanceReport',1)
    SolaceViewVars('LocalResponse',LocalResponse,'RepairPerformanceReport',1)
    SolaceViewVars('FilesOpened',FilesOpened,'RepairPerformanceReport',1)
    SolaceViewVars('WindowOpened',WindowOpened,'RepairPerformanceReport',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'RepairPerformanceReport',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'RepairPerformanceReport',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'RepairPerformanceReport',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'RepairPerformanceReport',1)
    SolaceViewVars('PercentProgress',PercentProgress,'RepairPerformanceReport',1)
    SolaceViewVars('RecordStatus',RecordStatus,'RepairPerformanceReport',1)
    SolaceViewVars('EndOfReport',EndOfReport,'RepairPerformanceReport',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'RepairPerformanceReport',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'RepairPerformanceReport',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'RepairPerformanceReport',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'RepairPerformanceReport',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'RepairPerformanceReport',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'RepairPerformanceReport',1)
    SolaceViewVars('InitialPath',InitialPath,'RepairPerformanceReport',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'RepairPerformanceReport',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'RepairPerformanceReport',1)
    SolaceViewVars('tmp:printedby',tmp:printedby,'RepairPerformanceReport',1)
    SolaceViewVars('tmp:telephonenumber',tmp:telephonenumber,'RepairPerformanceReport',1)
    SolaceViewVars('tmp:faxnumber',tmp:faxnumber,'RepairPerformanceReport',1)
    SolaceViewVars('tmp:daterange',tmp:daterange,'RepairPerformanceReport',1)
    SolaceViewVars('tmp:total1',tmp:total1,'RepairPerformanceReport',1)
    SolaceViewVars('tmp:total2',tmp:total2,'RepairPerformanceReport',1)
    
      Loop SolaceDim1# = 1 to 4
        SolaceFieldName" = 'tmp:detail' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tmp:detail[SolaceDim1#],'RepairPerformanceReport',1)
      End
    
    
    SolaceViewVars('tmp:total3',tmp:total3,'RepairPerformanceReport',1)


BuildCtrlQueue      Routine







