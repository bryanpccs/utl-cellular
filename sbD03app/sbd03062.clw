

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03062.INC'),ONCE        !Local module procedure declarations
                     END


NEC_Export_Criteria PROCEDURE                         !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
tmp:StartDate        DATE
savepath             STRING(255)
save_job_id          USHORT,AUTO
tmp:EndDate          DATE
tmp:Completed        STRING(1)
tmp:Authorised       STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('NEC Export Criteria'),AT(,,220,220),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,212,184),USE(?Sheet1),SPREAD
                         TAB('NEC Export Criteria'),USE(?Tab1)
                           PROMPT('Start Date'),AT(8,20),USE(?tmp:StartDate:Prompt)
                           ENTRY(@d6),AT(84,20,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('End Date'),AT(8,36),USE(?tmp:EndDate:Prompt)
                           ENTRY(@d6b),AT(84,36,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           OPTION('Include Completed Jobs'),AT(84,52,120,64),USE(tmp:Completed),BOXED
                             RADIO('All Jobs'),AT(96,64),USE(?tmp:Completed:Radio1),VALUE('1')
                             RADIO('Incompleted Jobs Only'),AT(96,80),USE(?tmp:Completed:Radio2),VALUE('2')
                             RADIO('Completed Jobs Only'),AT(96,96),USE(?tmp:Completed:Radio3),VALUE('3')
                           END
                           OPTION('Include Authorised Jobs'),AT(84,120,120,64),USE(tmp:Authorised),BOXED
                             RADIO('All Jobs'),AT(96,132),USE(?tmp:Authorised:Radio1),VALUE('1')
                             RADIO('Only Jobs Without Auth. No.'),AT(96,148),USE(?tmp:Authorised:Radio2),VALUE('2')
                             RADIO('Only Jobs With Auth. No.'),AT(96,164),USE(?tmp:Authorised:Radio3),VALUE('3')
                           END
                         END
                       END
                       PANEL,AT(4,192,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Export'),AT(96,196,56,16),USE(?Export),LEFT,ICON('Disk.gif')
                       BUTTON('Close'),AT(156,196,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?tmp:StartDate:Prompt{prop:FontColor} = -1
    ?tmp:StartDate:Prompt{prop:Color} = 15066597
    If ?tmp:StartDate{prop:ReadOnly} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 15066597
    Elsif ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 8454143
    Else ! If ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 16777215
    End ! If ?tmp:StartDate{prop:Req} = True
    ?tmp:StartDate{prop:Trn} = 0
    ?tmp:StartDate{prop:FontStyle} = font:Bold
    ?tmp:EndDate:Prompt{prop:FontColor} = -1
    ?tmp:EndDate:Prompt{prop:Color} = 15066597
    If ?tmp:EndDate{prop:ReadOnly} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 15066597
    Elsif ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 8454143
    Else ! If ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 16777215
    End ! If ?tmp:EndDate{prop:Req} = True
    ?tmp:EndDate{prop:Trn} = 0
    ?tmp:EndDate{prop:FontStyle} = font:Bold
    ?tmp:Completed{prop:Font,3} = -1
    ?tmp:Completed{prop:Color} = 15066597
    ?tmp:Completed{prop:Trn} = 0
    ?tmp:Completed:Radio1{prop:Font,3} = -1
    ?tmp:Completed:Radio1{prop:Color} = 15066597
    ?tmp:Completed:Radio1{prop:Trn} = 0
    ?tmp:Completed:Radio2{prop:Font,3} = -1
    ?tmp:Completed:Radio2{prop:Color} = 15066597
    ?tmp:Completed:Radio2{prop:Trn} = 0
    ?tmp:Completed:Radio3{prop:Font,3} = -1
    ?tmp:Completed:Radio3{prop:Color} = 15066597
    ?tmp:Completed:Radio3{prop:Trn} = 0
    ?tmp:Authorised{prop:Font,3} = -1
    ?tmp:Authorised{prop:Color} = 15066597
    ?tmp:Authorised{prop:Trn} = 0
    ?tmp:Authorised:Radio1{prop:Font,3} = -1
    ?tmp:Authorised:Radio1{prop:Color} = 15066597
    ?tmp:Authorised:Radio1{prop:Trn} = 0
    ?tmp:Authorised:Radio2{prop:Font,3} = -1
    ?tmp:Authorised:Radio2{prop:Color} = 15066597
    ?tmp:Authorised:Radio2{prop:Trn} = 0
    ?tmp:Authorised:Radio3{prop:Font,3} = -1
    ?tmp:Authorised:Radio3{prop:Color} = 15066597
    ?tmp:Authorised:Radio3{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'NEC_Export_Criteria',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:StartDate',tmp:StartDate,'NEC_Export_Criteria',1)
    SolaceViewVars('savepath',savepath,'NEC_Export_Criteria',1)
    SolaceViewVars('save_job_id',save_job_id,'NEC_Export_Criteria',1)
    SolaceViewVars('tmp:EndDate',tmp:EndDate,'NEC_Export_Criteria',1)
    SolaceViewVars('tmp:Completed',tmp:Completed,'NEC_Export_Criteria',1)
    SolaceViewVars('tmp:Authorised',tmp:Authorised,'NEC_Export_Criteria',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate:Prompt;  SolaceCtrlName = '?tmp:StartDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate;  SolaceCtrlName = '?tmp:StartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate:Prompt;  SolaceCtrlName = '?tmp:EndDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate;  SolaceCtrlName = '?tmp:EndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Completed;  SolaceCtrlName = '?tmp:Completed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Completed:Radio1;  SolaceCtrlName = '?tmp:Completed:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Completed:Radio2;  SolaceCtrlName = '?tmp:Completed:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Completed:Radio3;  SolaceCtrlName = '?tmp:Completed:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Authorised;  SolaceCtrlName = '?tmp:Authorised';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Authorised:Radio1;  SolaceCtrlName = '?tmp:Authorised:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Authorised:Radio2;  SolaceCtrlName = '?tmp:Authorised:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Authorised:Radio3;  SolaceCtrlName = '?tmp:Authorised:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Export;  SolaceCtrlName = '?Export';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('NEC_Export_Criteria')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'NEC_Export_Criteria')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:StartDate:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:JOBS.Open
  SELF.FilesOpened = True
  tmp:StartDate = Deformat('1/1/1999',@d6)
  tmp:EndDate = Today()
  tmp:Completed = 1
  tmp:Authorised = 1
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'NEC_Export_Criteria',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Export
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export, Accepted)
      savepath = path()
      set(defaults)
      access:defaults.next()
      If def:exportpath <> ''
          glo:file_name = Clip(def:exportpath) & '\NECEXP.CSV'
      Else!If def:exportpath <> ''
          glo:file_name = 'C:\NECEXP.CSV'
      End!If def:exportpath <> ''
      if not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
                  file:keepdir + file:noerror + file:longname)
          !failed
          setpath(savepath)
      else!if not filedialog
          !found
          setpath(savepath)
      
          access:expgen.open()
          access:expgen.usefile()
      
          Clear(gen:record)
          gen:line1   = 'Job Number,Serial Number,Equipment Type,Booking Date'
          access:expgen.insert()
      
          recordspercycle     = 25
          recordsprocessed    = 0
          percentprogress     = 0
          setcursor(cursor:wait)
          open(progresswindow)
          progress:thermometer    = 0
          ?progress:pcttext{prop:text} = '0% Completed'
      
          recordstoprocess    = Records(Jobs)
      
          save_job_id = access:jobs.savefile()
          access:jobs.clearkey(job:date_booked_key)
          job:date_booked = tmp:StartDate
          set(job:date_booked_key,job:date_booked_key)
          loop
              if access:jobs.next()
                 break
              end !if
              if job:date_booked > tmp:EndDate      |
                  then break.  ! end if
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              Do getnextrecord2
              If job:warranty_job <> 'YES'
                  Cycle
              End!If job:warranty_job <> 'YES'
              If job:manufacturer <> 'NEC'
                  Cycle
              End!If job:manufacturer <> 'NEC'
      
              Case tmp:completed
                  Of 2
                      If job:date_completed <> ''
                          Cycle
                      End!If job:date_completed = ''
                  Of 3
                      If job:date_completed = ''
                          Cycle
                      End!If job:date_completed = ''
              End!Case tmp:completed
      
              Case tmp:authorised
                  Of 2
                      If job:authority_number <> ''
                          Cycle
                      End!If job:authorisation_number <> ''
                  of 3
                      If job:authority_number = ''
                          Cycle
                      End!If job:authorisation_number = ''
              End!Case tmp:authorised
      
              Clear(gen:record)
              gen:line1   = Clip(job:ref_number) & ',' & Clip(job:esn) & ',MAIN,' & Clip(Format(job:date_booked,@d6))
              access:expgen.insert()
          end !loop
          access:jobs.restorefile(save_job_id)
          setcursor()
          close(progresswindow)
      
          access:expgen.close()
          Case MessageEx('Export Completed.','ServiceBase 2000',|
                         'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      end!if not filedialog
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'NEC_Export_Criteria')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

