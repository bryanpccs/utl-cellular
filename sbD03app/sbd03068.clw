

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03068.INC'),ONCE        !Local module procedure declarations
                     END


Unallocated_Criteria PROCEDURE                        !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select2
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?GLO:Select4
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB3::View:FileDropCombo VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
FDCB4::View:FileDropCombo VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                     END
window               WINDOW('Unallocated Jobs Report Criteria'),AT(,,220,123),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,212,88),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           COMBO(@s40),AT(84,28,124,10),USE(GLO:Select2),IMM,DISABLE,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('65L(2)|M@s15@120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           OPTION('Model Number'),AT(8,54,204,32),USE(GLO:Select3),BOXED,COLOR(COLOR:Silver)
                             RADIO('All'),AT(14,68),USE(?GLO:Select3:Radio3),VALUE('0')
                             RADIO('Select'),AT(48,68),USE(?GLO:Select3:Radio4),VALUE('1')
                           END
                           OPTION('Trade Account'),AT(8,16,204,32),USE(GLO:Select1),BOXED,COLOR(COLOR:Silver)
                             RADIO('All'),AT(16,28),USE(?GLO:Select1:Radio1),VALUE('0')
                             RADIO('Select'),AT(44,28),USE(?GLO:Select1:Radio2),VALUE('1')
                           END
                           COMBO(@s40),AT(84,68,124,10),USE(GLO:Select4),IMM,DISABLE,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                         END
                       END
                       PANEL,AT(4,96,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,100,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Close'),AT(156,100,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB3                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    If ?GLO:Select2{prop:ReadOnly} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 15066597
    Elsif ?GLO:Select2{prop:Req} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 8454143
    Else ! If ?GLO:Select2{prop:Req} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 16777215
    End ! If ?GLO:Select2{prop:Req} = True
    ?GLO:Select2{prop:Trn} = 0
    ?GLO:Select2{prop:FontStyle} = font:Bold
    ?GLO:Select3{prop:Font,3} = -1
    ?GLO:Select3{prop:Color} = 15066597
    ?GLO:Select3{prop:Trn} = 0
    ?GLO:Select3:Radio3{prop:Font,3} = -1
    ?GLO:Select3:Radio3{prop:Color} = 15066597
    ?GLO:Select3:Radio3{prop:Trn} = 0
    ?GLO:Select3:Radio4{prop:Font,3} = -1
    ?GLO:Select3:Radio4{prop:Color} = 15066597
    ?GLO:Select3:Radio4{prop:Trn} = 0
    ?GLO:Select1{prop:Font,3} = -1
    ?GLO:Select1{prop:Color} = 15066597
    ?GLO:Select1{prop:Trn} = 0
    ?GLO:Select1:Radio1{prop:Font,3} = -1
    ?GLO:Select1:Radio1{prop:Color} = 15066597
    ?GLO:Select1:Radio1{prop:Trn} = 0
    ?GLO:Select1:Radio2{prop:Font,3} = -1
    ?GLO:Select1:Radio2{prop:Color} = 15066597
    ?GLO:Select1:Radio2{prop:Trn} = 0
    If ?GLO:Select4{prop:ReadOnly} = True
        ?GLO:Select4{prop:FontColor} = 65793
        ?GLO:Select4{prop:Color} = 15066597
    Elsif ?GLO:Select4{prop:Req} = True
        ?GLO:Select4{prop:FontColor} = 65793
        ?GLO:Select4{prop:Color} = 8454143
    Else ! If ?GLO:Select4{prop:Req} = True
        ?GLO:Select4{prop:FontColor} = 65793
        ?GLO:Select4{prop:Color} = 16777215
    End ! If ?GLO:Select4{prop:Req} = True
    ?GLO:Select4{prop:Trn} = 0
    ?GLO:Select4{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Unallocated_Criteria',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select2;  SolaceCtrlName = '?GLO:Select2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select3;  SolaceCtrlName = '?GLO:Select3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select3:Radio3;  SolaceCtrlName = '?GLO:Select3:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select3:Radio4;  SolaceCtrlName = '?GLO:Select3:Radio4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select1;  SolaceCtrlName = '?GLO:Select1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select1:Radio1;  SolaceCtrlName = '?GLO:Select1:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select1:Radio2;  SolaceCtrlName = '?GLO:Select1:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select4;  SolaceCtrlName = '?GLO:Select4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Unallocated_Criteria')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Unallocated_Criteria')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?GLO:Select2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MODELNUM.Open
  SELF.FilesOpened = True
  glo:select1 = 0
  glo:select2 = ''
  glo:select3 = 0
  glo:select4 = ''
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  FDCB3.Init(GLO:Select2,?GLO:Select2,Queue:FileDropCombo.ViewPosition,FDCB3::View:FileDropCombo,Queue:FileDropCombo,Relate:SUBTRACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB3.Q &= Queue:FileDropCombo
  FDCB3.AddSortOrder(sub:Account_Number_Key)
  FDCB3.AddField(sub:Account_Number,FDCB3.Q.sub:Account_Number)
  FDCB3.AddField(sub:Company_Name,FDCB3.Q.sub:Company_Name)
  FDCB3.AddField(sub:RecordNumber,FDCB3.Q.sub:RecordNumber)
  ThisWindow.AddItem(FDCB3.WindowComponent)
  FDCB3.DefaultFill = 0
  FDCB4.Init(GLO:Select4,?GLO:Select4,Queue:FileDropCombo:1.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo:1,Relate:MODELNUM,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo:1
  FDCB4.AddSortOrder(mod:Model_Number_Key)
  FDCB4.AddField(mod:Model_Number,FDCB4.Q.mod:Model_Number)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  glo:select1 = ''
  glo:select2 = ''
  glo:select3 = ''
  glo:select4 = ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MODELNUM.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Unallocated_Criteria',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?GLO:Select3
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select3, Accepted)
      Case glo:select3
          Of 1
              Enable(?Glo:select4)
          Of 0
              Disable(?glo:select4)
      End!Case glo:select1
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select3, Accepted)
    OF ?GLO:Select1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select1, Accepted)
      Case glo:select1
          Of 1
              Enable(?Glo:select2)
          Of 0
              Disable(?glo:select2)
      End!Case glo:select1
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select1, Accepted)
    OF ?OK
      ThisWindow.Update
      Unallocated_Jobs_Report
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Unallocated_Criteria')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

