

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_fwiz.inc'),ONCE

                     MAP
                       INCLUDE('SBD03070.INC'),ONCE        !Local module procedure declarations
                     END


RepairPerformanceCriteria PROCEDURE                   !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::7:TAGFLAG          BYTE(0)
DASBRW::7:TAGMOUSE         BYTE(0)
DASBRW::7:TAGDISPSTATUS    BYTE(0)
DASBRW::7:QUEUE           QUEUE
Pointer                       LIKE(GLO:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::9:TAGFLAG          BYTE(0)
DASBRW::9:TAGMOUSE         BYTE(0)
DASBRW::9:TAGDISPSTATUS    BYTE(0)
DASBRW::9:QUEUE           QUEUE
Pointer2                      LIKE(GLO:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::11:TAGFLAG         BYTE(0)
DASBRW::11:TAGMOUSE        BYTE(0)
DASBRW::11:TAGDISPSTATUS   BYTE(0)
DASBRW::11:QUEUE          QUEUE
Pointer3                      LIKE(GLO:Pointer3)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::13:TAGFLAG         BYTE(0)
DASBRW::13:TAGMOUSE        BYTE(0)
DASBRW::13:TAGDISPSTATUS   BYTE(0)
DASBRW::13:QUEUE          QUEUE
Pointer4                      LIKE(GLO:Pointer4)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::15:TAGFLAG         BYTE(0)
DASBRW::15:TAGMOUSE        BYTE(0)
DASBRW::15:TAGDISPSTATUS   BYTE(0)
DASBRW::15:QUEUE          QUEUE
Pointer5                      LIKE(GLO:Pointer5)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:ThirdParty       BYTE(0)
tmp:count            LONG
sav:path             STRING(255)
savepath             STRING(255)
save_job_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_tra_id          USHORT,AUTO
save_sub_id          USHORT,AUTO
tmp:AwaitingParts    BYTE(0)
tmp:Exchanged        BYTE(0)
tmp:Loaned           BYTE(0)
tmp:Estimate         BYTE(0)
tmp:selectall        BYTE(0)
tmp:tag              STRING(1)
DisplayString        STRING(255)
tmp:tag2             STRING(1)
tmp:tag3             STRING(1)
tmp:tag4             STRING(1)
tmp:tag5             STRING(1)
tmp:startdate        DATE
tmp:enddate          DATE
tmp:summary          BYTE(0)
tmp:csvcomp          BYTE(0)
tmp:csvincomp        BYTE(0)
tmp:csvexc           BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW6::View:Browse    VIEW(REPTYDEF)
                       PROJECT(rtd:Repair_Type)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:tag                LIKE(tmp:tag)                  !List box control field - type derived from local data
tmp:tag_Icon           LONG                           !Entry's icon ID
rtd:Repair_Type        LIKE(rtd:Repair_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(TRADETMP)
                       PROJECT(tratmp:Account_Number)
                       PROJECT(tratmp:Company_Name)
                       PROJECT(tratmp:RefNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:tag2               LIKE(tmp:tag2)                 !List box control field - type derived from local data
tmp:tag2_Icon          LONG                           !Entry's icon ID
tratmp:Account_Number  LIKE(tratmp:Account_Number)    !List box control field - type derived from field
tratmp:Company_Name    LIKE(tratmp:Company_Name)      !List box control field - type derived from field
tratmp:RefNumber       LIKE(tratmp:RefNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
tmp:tag3               LIKE(tmp:tag3)                 !List box control field - type derived from local data
tmp:tag3_Icon          LONG                           !Entry's icon ID
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW12::View:Browse   VIEW(TEAMS)
                       PROJECT(tea:Team)
                       PROJECT(tea:Record_Number)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:4
tmp:tag4               LIKE(tmp:tag4)                 !List box control field - type derived from local data
tmp:tag4_Icon          LONG                           !Entry's icon ID
tea:Team               LIKE(tea:Team)                 !List box control field - type derived from field
tea:Record_Number      LIKE(tea:Record_Number)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW14::View:Browse   VIEW(USERS)
                       PROJECT(use:Surname)
                       PROJECT(use:Forename)
                       PROJECT(use:User_Code)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?List:5
tmp:tag5               LIKE(tmp:tag5)                 !List box control field - type derived from local data
tmp:tag5_Icon          LONG                           !Entry's icon ID
use:Surname            LIKE(use:Surname)              !List box control field - type derived from field
use:Forename           LIKE(use:Forename)             !List box control field - type derived from field
use:User_Code          LIKE(use:User_Code)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Repair Performance Criteria'),AT(,,300,287),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,292,252),USE(?Sheet1)
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('This report will give you a break down of how long repairs are taking to be comp' &|
   'leted.'),AT(88,24,200,20),USE(?Prompt2)
                           PROMPT('Select which type of jobs you wish to exclude:'),AT(88,68,200,16),USE(?Prompt2:2)
                           CHECK('Third Party Repairs'),AT(132,92),USE(tmp:ThirdParty),VALUE('1','0')
                           CHECK('Awaiting Parts'),AT(132,108),USE(tmp:AwaitingParts),VALUE('1','0')
                           CHECK('Exchanged'),AT(132,124),USE(tmp:Exchanged),VALUE('1','0')
                           CHECK('Loaned'),AT(132,140),USE(tmp:Loaned),VALUE('1','0')
                           CHECK('Estimate'),AT(132,156),USE(tmp:Estimate),VALUE('1','0')
                           CHECK('Select All'),AT(132,176),USE(tmp:selectall),VALUE('1','0')
                           PROMPT('Exclusions'),AT(88,52),USE(?Prompt3),FONT(,,COLOR:Navy,FONT:underline)
                         END
                         TAB('Tab 2'),USE(?Tab2)
                           PROMPT('Exclusions Continued..'),AT(88,28),USE(?Prompt3:2),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Repair Types (that indicate BER) to exclude (if required)'),AT(88,40,204,16),USE(?Prompt6)
                           LIST,AT(112,68,146,144),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Repair Type~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(163,73,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(163,105,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON('&Tag'),AT(112,216,48,16),USE(?DASTAG),LEFT,ICON('tag.gif')
                           BUTTON('T&ag All'),AT(160,216,48,16),USE(?DASTAGAll),LEFT,ICON('tagall.gif')
                           BUTTON('&Untag All'),AT(208,216,48,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                         END
                         TAB('Tab 3'),USE(?Tab3)
                           PROMPT('Inclusions'),AT(92,28),USE(?Prompt7),FONT(,8,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Trade Accounts you wish to include'),AT(92,44),USE(?Prompt8)
                           LIST,AT(92,64,196,144),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@63L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(184,104,50,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(180,136,70,13),USE(?DASSHOWTAG:2),HIDE
                           BUTTON('&Tag'),AT(92,212,48,16),USE(?DASTAG:2),LEFT,ICON('tag.gif')
                           BUTTON('T&ag All'),AT(140,212,48,16),USE(?DASTAGAll:2),LEFT,ICON('tagall.gif')
                           BUTTON('&Untag All'),AT(188,212,48,16),USE(?DASUNTAGALL:2),LEFT,ICON('untag.gif')
                         END
                         TAB('Tab 4'),USE(?Tab4)
                           PROMPT('Inclusions'),AT(92,28),USE(?Prompt7:2),FONT(,8,COLOR:Navy,FONT:underline)
                           PROMPT('Select The Manufacturers you wish to include.'),AT(92,44),USE(?Prompt8:2)
                           LIST,AT(112,68,146,144),USE(?List:3),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:2)
                           BUTTON('&Rev tags'),AT(179,113,50,13),USE(?DASREVTAG:3),HIDE
                           BUTTON('sho&W tags'),AT(179,145,70,13),USE(?DASSHOWTAG:3),HIDE
                           BUTTON('&Tag'),AT(112,216,48,16),USE(?DASTAG:3),LEFT,ICON('tag.gif')
                           BUTTON('T&ag All'),AT(160,216,48,16),USE(?DASTAGAll:3),LEFT,ICON('tagall.gif')
                           BUTTON('&Untag All'),AT(208,216,48,16),USE(?DASUNTAGALL:3),LEFT,ICON('untag.gif')
                         END
                         TAB('Tab 5'),USE(?Tab5)
                           PROMPT('Inclusions'),AT(92,28),USE(?Prompt7:3),FONT(,8,COLOR:Navy,FONT:underline)
                           PROMPT('Select The Teams you wish to include'),AT(92,44),USE(?Prompt8:3)
                           LIST,AT(112,68,146,144),USE(?List:4),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Team~@s30@'),FROM(Queue:Browse:3)
                           BUTTON('&Rev tags'),AT(163,125,50,13),USE(?DASREVTAG:4),HIDE
                           BUTTON('sho&W tags'),AT(171,153,70,13),USE(?DASSHOWTAG:4),HIDE
                           BUTTON('&Tag'),AT(112,216,48,16),USE(?DASTAG:4),LEFT,ICON('tag.gif')
                           BUTTON('T&ag All'),AT(160,216,48,16),USE(?DASTAGAll:4),LEFT,ICON('tagall.gif')
                           BUTTON('&Untag All'),AT(208,216,48,16),USE(?DASUNTAGALL:4),LEFT,ICON('untag.gif')
                         END
                         TAB('Tab 6'),USE(?Tab6)
                           PROMPT('Inclusions'),AT(92,28),USE(?Prompt7:4),FONT(,8,COLOR:Navy,FONT:underline)
                           PROMPT('Select The Engineers you wish to include'),AT(92,44),USE(?Prompt8:4)
                           LIST,AT(92,64,196,144),USE(?List:5),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Surname~@s30@120L(2)|M~Forename~@s30@12L(2)|M~User Code~@s3' &|
   '@'),FROM(Queue:Browse:4)
                           BUTTON('&Rev tags'),AT(159,101,50,13),USE(?DASREVTAG:5),HIDE
                           BUTTON('sho&W tags'),AT(167,141,70,13),USE(?DASSHOWTAG:5),HIDE
                           BUTTON('&Tag'),AT(92,212,48,16),USE(?DASTAG:5),LEFT,ICON('tag.gif')
                           BUTTON('T&ag All'),AT(140,212,48,16),USE(?DASTAGAll:5),LEFT,ICON('tagall.gif')
                           BUTTON('&Untag All'),AT(188,212,48,16),USE(?DASUNTAGALL:5),LEFT,ICON('untag.gif')
                         END
                         TAB('Tab 7'),USE(?Tab7)
                           PROMPT('Date Range'),AT(91,64),USE(?Prompt15),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Report Type'),AT(92,128),USE(?Prompt15:2),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select whether you want a summary report, and/or export files'),AT(92,140,200,24),USE(?Prompt19)
                           CHECK('Print Summary Report'),AT(172,168),USE(tmp:summary),VALUE('1','0')
                           GROUP('Export Types'),AT(164,184,108,60),USE(?Group1),BOXED,TRN
                             CHECK('Completed Jobs'),AT(172,196),USE(tmp:csvcomp),VALUE('1','0')
                             CHECK('Incomplete Jobs'),AT(172,212),USE(tmp:csvincomp),VALUE('1','0')
                             CHECK('Excluded Jobs'),AT(172,228),USE(tmp:csvexc),VALUE('1','0')
                           END
                           BUTTON('Export Criteria'),AT(8,236,56,16),USE(?ExportCriteria)
                           PROMPT('Start Date'),AT(92,84),USE(?tmp:startdate:Prompt)
                           ENTRY(@d6),AT(172,84,64,10),USE(tmp:startdate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(240,84,10,10),USE(?PopCalendar),ICON('calenda2.ico')
                           PROMPT('End Date'),AT(92,104),USE(?tmp:enddate:Prompt)
                           ENTRY(@d6),AT(172,104,64,10),USE(tmp:enddate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(240,104,10,10),USE(?PopCalendar:2),ICON('calenda2.ico')
                         END
                       END
                       PROMPT('Repair Performance Report'),AT(8,8),USE(?Prompt1),FONT(,14,,FONT:bold)
                       IMAGE('wizard.gif'),AT(8,84),USE(?Image1)
                       PANEL,AT(4,260,292,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Back'),AT(8,264,56,16),USE(?VSBackButton),LEFT,ICON(ICON:VCRrewind)
                       BUTTON('&Next'),AT(64,264,56,16),USE(?VSNextButton),LEFT,ICON(ICON:VCRfastforward)
                       BUTTON('Finish'),AT(180,264,56,16),USE(?Finish),LEFT,ICON('thumbs.gif')
                       BUTTON('Close'),AT(236,264,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,imm,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
       button('Cancel'),at(54,44,56,16),use(?cancel),left,icon('cancel.gif')
     end
Wizard5         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW6                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW8                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW10                CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW10::Sort0:Locator StepLocatorClass                 !Default Locator
BRW12                CLASS(BrowseClass)               !Browse using ?List:4
Q                      &Queue:Browse:3                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW12::Sort0:Locator StepLocatorClass                 !Default Locator
BRW14                CLASS(BrowseClass)               !Browse using ?List:5
Q                      &Queue:Browse:4                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW14::Sort0:Locator StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
Criteria FILE,DRIVER('ASCII'),PRE(crit),NAME(glo:CriteriaFile),CREATE,BINDABLE,THREAD
Record      Record
Field1          String(2000)
            End
        End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?Prompt2:2{prop:FontColor} = -1
    ?Prompt2:2{prop:Color} = 15066597
    ?tmp:ThirdParty{prop:Font,3} = -1
    ?tmp:ThirdParty{prop:Color} = 15066597
    ?tmp:ThirdParty{prop:Trn} = 0
    ?tmp:AwaitingParts{prop:Font,3} = -1
    ?tmp:AwaitingParts{prop:Color} = 15066597
    ?tmp:AwaitingParts{prop:Trn} = 0
    ?tmp:Exchanged{prop:Font,3} = -1
    ?tmp:Exchanged{prop:Color} = 15066597
    ?tmp:Exchanged{prop:Trn} = 0
    ?tmp:Loaned{prop:Font,3} = -1
    ?tmp:Loaned{prop:Color} = 15066597
    ?tmp:Loaned{prop:Trn} = 0
    ?tmp:Estimate{prop:Font,3} = -1
    ?tmp:Estimate{prop:Color} = 15066597
    ?tmp:Estimate{prop:Trn} = 0
    ?tmp:selectall{prop:Font,3} = -1
    ?tmp:selectall{prop:Color} = 15066597
    ?tmp:selectall{prop:Trn} = 0
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?Prompt3:2{prop:FontColor} = -1
    ?Prompt3:2{prop:Color} = 15066597
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Tab3{prop:Color} = 15066597
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Tab4{prop:Color} = 15066597
    ?Prompt7:2{prop:FontColor} = -1
    ?Prompt7:2{prop:Color} = 15066597
    ?Prompt8:2{prop:FontColor} = -1
    ?Prompt8:2{prop:Color} = 15066597
    ?List:3{prop:FontColor} = 65793
    ?List:3{prop:Color}= 16777215
    ?List:3{prop:Color,2} = 16777215
    ?List:3{prop:Color,3} = 12937777
    ?Tab5{prop:Color} = 15066597
    ?Prompt7:3{prop:FontColor} = -1
    ?Prompt7:3{prop:Color} = 15066597
    ?Prompt8:3{prop:FontColor} = -1
    ?Prompt8:3{prop:Color} = 15066597
    ?List:4{prop:FontColor} = 65793
    ?List:4{prop:Color}= 16777215
    ?List:4{prop:Color,2} = 16777215
    ?List:4{prop:Color,3} = 12937777
    ?Tab6{prop:Color} = 15066597
    ?Prompt7:4{prop:FontColor} = -1
    ?Prompt7:4{prop:Color} = 15066597
    ?Prompt8:4{prop:FontColor} = -1
    ?Prompt8:4{prop:Color} = 15066597
    ?List:5{prop:FontColor} = 65793
    ?List:5{prop:Color}= 16777215
    ?List:5{prop:Color,2} = 16777215
    ?List:5{prop:Color,3} = 12937777
    ?Tab7{prop:Color} = 15066597
    ?Prompt15{prop:FontColor} = -1
    ?Prompt15{prop:Color} = 15066597
    ?Prompt15:2{prop:FontColor} = -1
    ?Prompt15:2{prop:Color} = 15066597
    ?Prompt19{prop:FontColor} = -1
    ?Prompt19{prop:Color} = 15066597
    ?tmp:summary{prop:Font,3} = -1
    ?tmp:summary{prop:Color} = 15066597
    ?tmp:summary{prop:Trn} = 0
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?tmp:csvcomp{prop:Font,3} = -1
    ?tmp:csvcomp{prop:Color} = 15066597
    ?tmp:csvcomp{prop:Trn} = 0
    ?tmp:csvincomp{prop:Font,3} = -1
    ?tmp:csvincomp{prop:Color} = 15066597
    ?tmp:csvincomp{prop:Trn} = 0
    ?tmp:csvexc{prop:Font,3} = -1
    ?tmp:csvexc{prop:Color} = 15066597
    ?tmp:csvexc{prop:Trn} = 0
    ?tmp:startdate:Prompt{prop:FontColor} = -1
    ?tmp:startdate:Prompt{prop:Color} = 15066597
    If ?tmp:startdate{prop:ReadOnly} = True
        ?tmp:startdate{prop:FontColor} = 65793
        ?tmp:startdate{prop:Color} = 15066597
    Elsif ?tmp:startdate{prop:Req} = True
        ?tmp:startdate{prop:FontColor} = 65793
        ?tmp:startdate{prop:Color} = 8454143
    Else ! If ?tmp:startdate{prop:Req} = True
        ?tmp:startdate{prop:FontColor} = 65793
        ?tmp:startdate{prop:Color} = 16777215
    End ! If ?tmp:startdate{prop:Req} = True
    ?tmp:startdate{prop:Trn} = 0
    ?tmp:startdate{prop:FontStyle} = font:Bold
    ?tmp:enddate:Prompt{prop:FontColor} = -1
    ?tmp:enddate:Prompt{prop:Color} = 15066597
    If ?tmp:enddate{prop:ReadOnly} = True
        ?tmp:enddate{prop:FontColor} = 65793
        ?tmp:enddate{prop:Color} = 15066597
    Elsif ?tmp:enddate{prop:Req} = True
        ?tmp:enddate{prop:FontColor} = 65793
        ?tmp:enddate{prop:Color} = 8454143
    Else ! If ?tmp:enddate{prop:Req} = True
        ?tmp:enddate{prop:FontColor} = 65793
        ?tmp:enddate{prop:Color} = 16777215
    End ! If ?tmp:enddate{prop:Req} = True
    ?tmp:enddate{prop:Trn} = 0
    ?tmp:enddate{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::7:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW6.UpdateBuffer
   glo:Queue.Pointer = rtd:Repair_Type
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = rtd:Repair_Type
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:tag = ''
  END
    Queue:Browse.tmp:tag = tmp:tag
  IF (tmp:tag = '*')
    Queue:Browse.tmp:tag_Icon = 2
  ELSE
    Queue:Browse.tmp:tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW6.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = rtd:Repair_Type
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW6.Reset
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::7:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::7:QUEUE = glo:Queue
    ADD(DASBRW::7:QUEUE)
  END
  FREE(glo:Queue)
  BRW6.Reset
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::7:QUEUE.Pointer = rtd:Repair_Type
     GET(DASBRW::7:QUEUE,DASBRW::7:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = rtd:Repair_Type
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASSHOWTAG Routine
   CASE DASBRW::7:TAGDISPSTATUS
   OF 0
      DASBRW::7:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::7:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::7:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW6.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::9:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW8.UpdateBuffer
   glo:Queue2.Pointer2 = tratmp:Account_Number
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = tratmp:Account_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tmp:tag2 = '*'
  ELSE
    DELETE(glo:Queue2)
    tmp:tag2 = ''
  END
    Queue:Browse:1.tmp:tag2 = tmp:tag2
  IF (tmp:tag2 = '*')
    Queue:Browse:1.tmp:tag2_Icon = 2
  ELSE
    Queue:Browse:1.tmp:tag2_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::9:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW8.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = tratmp:Account_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::9:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW8.Reset
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::9:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::9:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::9:QUEUE = glo:Queue2
    ADD(DASBRW::9:QUEUE)
  END
  FREE(glo:Queue2)
  BRW8.Reset
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::9:QUEUE.Pointer2 = tratmp:Account_Number
     GET(DASBRW::9:QUEUE,DASBRW::9:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = tratmp:Account_Number
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::9:DASSHOWTAG Routine
   CASE DASBRW::9:TAGDISPSTATUS
   OF 0
      DASBRW::9:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::9:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::9:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW8.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::11:DASTAGONOFF Routine
  GET(Queue:Browse:2,CHOICE(?List:3))
  BRW10.UpdateBuffer
   glo:Queue3.Pointer3 = man:Manufacturer
   GET(glo:Queue3,glo:Queue3.Pointer3)
  IF ERRORCODE()
     glo:Queue3.Pointer3 = man:Manufacturer
     ADD(glo:Queue3,glo:Queue3.Pointer3)
    tmp:tag3 = '*'
  ELSE
    DELETE(glo:Queue3)
    tmp:tag3 = ''
  END
    Queue:Browse:2.tmp:tag3 = tmp:tag3
  IF (tmp:tag3 = '*')
    Queue:Browse:2.tmp:tag3_Icon = 2
  ELSE
    Queue:Browse:2.tmp:tag3_Icon = 1
  END
  PUT(Queue:Browse:2)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::11:DASTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW10.Reset
  FREE(glo:Queue3)
  LOOP
    NEXT(BRW10::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue3.Pointer3 = man:Manufacturer
     ADD(glo:Queue3,glo:Queue3.Pointer3)
  END
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::11:DASUNTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue3)
  BRW10.Reset
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::11:DASREVTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::11:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue3)
    GET(glo:Queue3,QR#)
    DASBRW::11:QUEUE = glo:Queue3
    ADD(DASBRW::11:QUEUE)
  END
  FREE(glo:Queue3)
  BRW10.Reset
  LOOP
    NEXT(BRW10::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::11:QUEUE.Pointer3 = man:Manufacturer
     GET(DASBRW::11:QUEUE,DASBRW::11:QUEUE.Pointer3)
    IF ERRORCODE()
       glo:Queue3.Pointer3 = man:Manufacturer
       ADD(glo:Queue3,glo:Queue3.Pointer3)
    END
  END
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::11:DASSHOWTAG Routine
   CASE DASBRW::11:TAGDISPSTATUS
   OF 0
      DASBRW::11:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::11:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::11:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:3{PROP:Text} = 'Show All'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:3{PROP:Text})
   BRW10.ResetSort(1)
   SELECT(?List:3,CHOICE(?List:3))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::13:DASTAGONOFF Routine
  GET(Queue:Browse:3,CHOICE(?List:4))
  BRW12.UpdateBuffer
   glo:Queue4.Pointer4 = tea:Team
   GET(glo:Queue4,glo:Queue4.Pointer4)
  IF ERRORCODE()
     glo:Queue4.Pointer4 = tea:Team
     ADD(glo:Queue4,glo:Queue4.Pointer4)
    tmp:tag4 = '*'
  ELSE
    DELETE(glo:Queue4)
    tmp:tag4 = ''
  END
    Queue:Browse:3.tmp:tag4 = tmp:tag4
  IF (tmp:tag4 = '*')
    Queue:Browse:3.tmp:tag4_Icon = 2
  ELSE
    Queue:Browse:3.tmp:tag4_Icon = 1
  END
  PUT(Queue:Browse:3)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::13:DASTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW12.Reset
  FREE(glo:Queue4)
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue4.Pointer4 = tea:Team
     ADD(glo:Queue4,glo:Queue4.Pointer4)
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::13:DASUNTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue4)
  BRW12.Reset
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::13:DASREVTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::13:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue4)
    GET(glo:Queue4,QR#)
    DASBRW::13:QUEUE = glo:Queue4
    ADD(DASBRW::13:QUEUE)
  END
  FREE(glo:Queue4)
  BRW12.Reset
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::13:QUEUE.Pointer4 = tea:Team
     GET(DASBRW::13:QUEUE,DASBRW::13:QUEUE.Pointer4)
    IF ERRORCODE()
       glo:Queue4.Pointer4 = tea:Team
       ADD(glo:Queue4,glo:Queue4.Pointer4)
    END
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::13:DASSHOWTAG Routine
   CASE DASBRW::13:TAGDISPSTATUS
   OF 0
      DASBRW::13:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:4{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::13:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:4{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::13:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:4{PROP:Text} = 'Show All'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:4{PROP:Text})
   BRW12.ResetSort(1)
   SELECT(?List:4,CHOICE(?List:4))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::15:DASTAGONOFF Routine
  GET(Queue:Browse:4,CHOICE(?List:5))
  BRW14.UpdateBuffer
   glo:Queue5.Pointer5 = use:User_Code
   GET(glo:Queue5,glo:Queue5.Pointer5)
  IF ERRORCODE()
     glo:Queue5.Pointer5 = use:User_Code
     ADD(glo:Queue5,glo:Queue5.Pointer5)
    tmp:tag5 = '*'
  ELSE
    DELETE(glo:Queue5)
    tmp:tag5 = ''
  END
    Queue:Browse:4.tmp:tag5 = tmp:tag5
  IF (tmp:tag5 = '*')
    Queue:Browse:4.tmp:tag5_Icon = 2
  ELSE
    Queue:Browse:4.tmp:tag5_Icon = 1
  END
  PUT(Queue:Browse:4)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::15:DASTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW14.Reset
  FREE(glo:Queue5)
  LOOP
    NEXT(BRW14::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue5.Pointer5 = use:User_Code
     ADD(glo:Queue5,glo:Queue5.Pointer5)
  END
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::15:DASUNTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue5)
  BRW14.Reset
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::15:DASREVTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::15:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue5)
    GET(glo:Queue5,QR#)
    DASBRW::15:QUEUE = glo:Queue5
    ADD(DASBRW::15:QUEUE)
  END
  FREE(glo:Queue5)
  BRW14.Reset
  LOOP
    NEXT(BRW14::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::15:QUEUE.Pointer5 = use:User_Code
     GET(DASBRW::15:QUEUE,DASBRW::15:QUEUE.Pointer5)
    IF ERRORCODE()
       glo:Queue5.Pointer5 = use:User_Code
       ADD(glo:Queue5,glo:Queue5.Pointer5)
    END
  END
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::15:DASSHOWTAG Routine
   CASE DASBRW::15:TAGDISPSTATUS
   OF 0
      DASBRW::15:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:5{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:5{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:5{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::15:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:5{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:5{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:5{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::15:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:5{PROP:Text} = 'Show All'
      ?DASSHOWTAG:5{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:5{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:5{PROP:Text})
   BRW14.ResetSort(1)
   SELECT(?List:5,CHOICE(?List:5))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    display()
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
        
      end
    end

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        case event()
            of event:timer
                break
            of event:closewindow
                cancel# = 1
                break
            of event:accepted
                if field() = ?cancel
                    cancel# = 1
                    break
                end!if field() = ?button1
        end!case event()
    end!accept
    if cancel# = 1
        case messageex('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,charset:ansi,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            of 1 ! &yes button
                tmp:cancel = 1
            of 2 ! &no button
        end!case messageex
    end!if cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
Insert_Export       Routine
    Clear(gen:record)
    gen:line1   = Format(job:date_booked,@d6)
    gen:line1   = Clip(gen:line1) & ',' & Format(job:date_completed,@d6)
    gen:line1   = Clip(gen:line1) & ',' & Clip(job:ref_number)
    gen:line1   = Clip(gen:line1) & ',' & Clip(job:manufacturer)
    gen:line1   = Clip(gen:line1) & ',' & Clip(job:model_number)
    gen:line1   = Clip(gen:line1) & ',' & Clip(job:account_number)
    access:subtracc.clearkey(sub:account_number_key)
    sub:account_number = job:account_number
    if access:subtracc.fetch(sub:account_number_key) = level:benign
        access:tradeacc.clearkey(tra:account_number_key) 
        tra:account_number = sub:main_account_number
        if access:tradeacc.fetch(tra:account_number_key) = level:benign
            gen:line1   = Clip(gen:line1) & ',' & Clip(tra:company_name)
        ! Start Change 2647 BE(17/06/03)
        else
            gen:line1   = Clip(gen:line1) & ','
        ! End Change 2647 BE(17/06/03)
        end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
    ! Start Change 2647 BE(17/06/03)
    else
        gen:line1   = Clip(gen:line1) & ','
    ! End Change 2647 BE(17/06/03)
    end!if access:subtracc.fetch(sub:account_number_key) = level:benign
    gen:line1   = Clip(gen:line1) & ',' & CLip(job:current_status)
    gen:line1   = Clip(gen:line1) & ',' & Clip(job:engineer)
    gen:line1   = Clip(gen:line1) & ',' & Clip(job:location)
    gen:line1   = Clip(gen:line1) & ',' & Clip(job:third_party_site)
    gen:line1   = Clip(gen:line1) & ',' & Format(job:ThirdPartyDateDesp,@d6)
    gen:line1   = Clip(gen:line1) & ',' & Format(job:date_paid,@d6)
    If job:third_party_site <> ''
        gen:line1   = Clip(gen:line1) & ',YES'
    Else!If job:third_party_site <> ''
        gen:line1   = Clip(gen:line1) & ',NO'
    End!If job:third_party_site <> ''

    found# = 0
    save_par_id = access:parts.savefile()
    access:parts.clearkey(par:order_number_key)
    par:ref_number   = job:ref_number
    set(par:order_number_key,par:order_number_key)
    loop
        if access:parts.next()
           break
        end !if
        if par:ref_number   <> job:ref_number      |
            then break.  ! end if
        if par:order_number <> ''
            found# = 1
            Break
        End!if par:order_number <> ''
    end !loop
    access:parts.restorefile(save_par_id)

    If found# = 0
        save_wpr_id = access:warparts.savefile()
        access:warparts.clearkey(wpr:order_number_key)
        wpr:ref_number   = job:ref_number
        set(wpr:order_number_key,wpr:order_number_key)
        loop
            if access:warparts.next()
               break
            end !if
            if wpr:ref_number   <> job:ref_number      |
                then break.  ! end if
            If wpr:order_number <> ''
                found# = 1
                Break
            End!If wpr:order_number <> ''
        end !loop
        access:warparts.restorefile(save_wpr_id)
    End!If found# = 0
    If found# = 1
        gen:line1   = Clip(gen:line1) & ',YES'
    Else!If found# = 1
        gen:line1   = Clip(gen:line1) & ',NO'
    End!If found# = 1

    If job:exchange_unit_number <> ''
        gen:line1   = Clip(gen:line1) & ',YES'
    Else!If job:exchange_unit_number <> ''
        gen:line1   = Clip(gen:line1) & ',NO'
    End!If job:exchange_unit_number <> ''

    If job:loan_unit_number <> ''
        gen:line1   = Clip(gen:line1) & ',YES'
    Else!If job:loan_unit_number <> ''
        gen:line1   = Clip(gen:line1) & ',NO'
    End!If job:loan_unit_number <> ''

    found# = 0
    Sort(glo:queue,glo:pointer)
    glo:pointer = job:repair_type
    Get(glo:queue,glo:pointer)
    If Error()
        Sort(glo:queue,glo:pointer)
        glo:pointer = job:repair_type_warranty
        Get(glo:queue,glo:pointer)
        If Error()

        Else!If Error()
            found# = 1
        End!If Error()
    Else!If Error()
        found# = 1
    End!If Error()
    if found# = 1
        gen:line1   = Clip(gen:line1) & ',YES'
    Else!if found# = 1
        gen:line1   = Clip(gen:line1) & ',NO'
    End!if found# = 1
    If job:estimate = 'YES'
        gen:line1   = Clip(gen:line1) & ',YES'
    Else!If job:estimate = 'YES'
        gen:line1   = Clip(gen:line1) & ',NO'
    End!If job:estimate = 'YES'

    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Found

    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
    gen:Line1   = Clip(gen:Line1) & ',' & Format(jobe:InWorkshopDate,@d06)
    gen:Line1   = Clip(gen:Line1) & ',' & Format(jobe:InWorkshopTime,@t01)
    tmp:count += 1
    access:expgen.insert()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'RepairPerformanceCriteria',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:ThirdParty',tmp:ThirdParty,'RepairPerformanceCriteria',1)
    SolaceViewVars('tmp:count',tmp:count,'RepairPerformanceCriteria',1)
    SolaceViewVars('sav:path',sav:path,'RepairPerformanceCriteria',1)
    SolaceViewVars('savepath',savepath,'RepairPerformanceCriteria',1)
    SolaceViewVars('save_job_id',save_job_id,'RepairPerformanceCriteria',1)
    SolaceViewVars('save_par_id',save_par_id,'RepairPerformanceCriteria',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'RepairPerformanceCriteria',1)
    SolaceViewVars('save_tra_id',save_tra_id,'RepairPerformanceCriteria',1)
    SolaceViewVars('save_sub_id',save_sub_id,'RepairPerformanceCriteria',1)
    SolaceViewVars('tmp:AwaitingParts',tmp:AwaitingParts,'RepairPerformanceCriteria',1)
    SolaceViewVars('tmp:Exchanged',tmp:Exchanged,'RepairPerformanceCriteria',1)
    SolaceViewVars('tmp:Loaned',tmp:Loaned,'RepairPerformanceCriteria',1)
    SolaceViewVars('tmp:Estimate',tmp:Estimate,'RepairPerformanceCriteria',1)
    SolaceViewVars('tmp:selectall',tmp:selectall,'RepairPerformanceCriteria',1)
    SolaceViewVars('tmp:tag',tmp:tag,'RepairPerformanceCriteria',1)
    SolaceViewVars('DisplayString',DisplayString,'RepairPerformanceCriteria',1)
    SolaceViewVars('tmp:tag2',tmp:tag2,'RepairPerformanceCriteria',1)
    SolaceViewVars('tmp:tag3',tmp:tag3,'RepairPerformanceCriteria',1)
    SolaceViewVars('tmp:tag4',tmp:tag4,'RepairPerformanceCriteria',1)
    SolaceViewVars('tmp:tag5',tmp:tag5,'RepairPerformanceCriteria',1)
    SolaceViewVars('tmp:startdate',tmp:startdate,'RepairPerformanceCriteria',1)
    SolaceViewVars('tmp:enddate',tmp:enddate,'RepairPerformanceCriteria',1)
    SolaceViewVars('tmp:summary',tmp:summary,'RepairPerformanceCriteria',1)
    SolaceViewVars('tmp:csvcomp',tmp:csvcomp,'RepairPerformanceCriteria',1)
    SolaceViewVars('tmp:csvincomp',tmp:csvincomp,'RepairPerformanceCriteria',1)
    SolaceViewVars('tmp:csvexc',tmp:csvexc,'RepairPerformanceCriteria',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2:2;  SolaceCtrlName = '?Prompt2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ThirdParty;  SolaceCtrlName = '?tmp:ThirdParty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:AwaitingParts;  SolaceCtrlName = '?tmp:AwaitingParts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Exchanged;  SolaceCtrlName = '?tmp:Exchanged';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Loaned;  SolaceCtrlName = '?tmp:Loaned';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Estimate;  SolaceCtrlName = '?tmp:Estimate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:selectall;  SolaceCtrlName = '?tmp:selectall';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3:2;  SolaceCtrlName = '?Prompt3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:2;  SolaceCtrlName = '?DASREVTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:2;  SolaceCtrlName = '?DASSHOWTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:2;  SolaceCtrlName = '?DASTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:2;  SolaceCtrlName = '?DASTAGAll:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:2;  SolaceCtrlName = '?DASUNTAGALL:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7:2;  SolaceCtrlName = '?Prompt7:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8:2;  SolaceCtrlName = '?Prompt8:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:3;  SolaceCtrlName = '?List:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:3;  SolaceCtrlName = '?DASREVTAG:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:3;  SolaceCtrlName = '?DASSHOWTAG:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:3;  SolaceCtrlName = '?DASTAG:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:3;  SolaceCtrlName = '?DASTAGAll:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:3;  SolaceCtrlName = '?DASUNTAGALL:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7:3;  SolaceCtrlName = '?Prompt7:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8:3;  SolaceCtrlName = '?Prompt8:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:4;  SolaceCtrlName = '?List:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:4;  SolaceCtrlName = '?DASREVTAG:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:4;  SolaceCtrlName = '?DASSHOWTAG:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:4;  SolaceCtrlName = '?DASTAG:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:4;  SolaceCtrlName = '?DASTAGAll:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:4;  SolaceCtrlName = '?DASUNTAGALL:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab6;  SolaceCtrlName = '?Tab6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7:4;  SolaceCtrlName = '?Prompt7:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8:4;  SolaceCtrlName = '?Prompt8:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:5;  SolaceCtrlName = '?List:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:5;  SolaceCtrlName = '?DASREVTAG:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:5;  SolaceCtrlName = '?DASSHOWTAG:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:5;  SolaceCtrlName = '?DASTAG:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:5;  SolaceCtrlName = '?DASTAGAll:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:5;  SolaceCtrlName = '?DASUNTAGALL:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab7;  SolaceCtrlName = '?Tab7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt15;  SolaceCtrlName = '?Prompt15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt15:2;  SolaceCtrlName = '?Prompt15:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt19;  SolaceCtrlName = '?Prompt19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:summary;  SolaceCtrlName = '?tmp:summary';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group1;  SolaceCtrlName = '?Group1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:csvcomp;  SolaceCtrlName = '?tmp:csvcomp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:csvincomp;  SolaceCtrlName = '?tmp:csvincomp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:csvexc;  SolaceCtrlName = '?tmp:csvexc';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExportCriteria;  SolaceCtrlName = '?ExportCriteria';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:startdate:Prompt;  SolaceCtrlName = '?tmp:startdate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:startdate;  SolaceCtrlName = '?tmp:startdate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:enddate:Prompt;  SolaceCtrlName = '?tmp:enddate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:enddate;  SolaceCtrlName = '?tmp:enddate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1;  SolaceCtrlName = '?Image1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VSBackButton;  SolaceCtrlName = '?VSBackButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VSNextButton;  SolaceCtrlName = '?VSNextButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Finish;  SolaceCtrlName = '?Finish';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('RepairPerformanceCriteria')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'RepairPerformanceCriteria')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:JOBS.Open
  Relate:REPTYDEF.Open
  Relate:TRADETMP.Open
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  include('tradelst.inc')
  Clear(GLO:G_Select1)
  tmp:startdate   = Deformat('1/1/1990',@d6)
  tmp:enddate     = Today()
  BRW6.Init(?List,Queue:Browse.ViewPosition,BRW6::View:Browse,Queue:Browse,Relate:REPTYDEF,SELF)
  BRW8.Init(?List:2,Queue:Browse:1.ViewPosition,BRW8::View:Browse,Queue:Browse:1,Relate:TRADETMP,SELF)
  BRW10.Init(?List:3,Queue:Browse:2.ViewPosition,BRW10::View:Browse,Queue:Browse:2,Relate:MANUFACT,SELF)
  BRW12.Init(?List:4,Queue:Browse:3.ViewPosition,BRW12::View:Browse,Queue:Browse:3,Relate:TEAMS,SELF)
  BRW14.Init(?List:5,Queue:Browse:4.ViewPosition,BRW14::View:Browse,Queue:Browse:4,Relate:USERS,SELF)
  OPEN(window)
  SELF.Opened=True
    ! Start Change 2647 BE(16/06/03)
    set(defaults)
    access:defaults.next()
    IF (INSTRING('INTEC',CLIP(UPPER(def:User_Name)),1,1)) THEN
        ?tmp:startdate:Prompt{prop:text} = 'Start In Workshop Date'
        ?tmp:enddate:Prompt{prop:text} = 'End In Workshop Date'
    ELSE
        ?tmp:startdate:Prompt{prop:text} = 'Start Booking Date'
        ?tmp:enddate:Prompt{prop:text} = 'End Booking Date'
    END
    ! End Change 2647 BE(16/06/03)
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ?List:4{prop:vcr} = TRUE
  ?List:5{prop:vcr} = TRUE
    Wizard5.Init(?Sheet1, |                           ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Finish, |                       ! OK button
                     ?Close, |                        ! Cancel button
                     1, |                             ! Skip hidden tabs
                     1, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  ?tmp:startdate{Prop:Alrt,255} = MouseLeft2
  ?tmp:enddate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  BRW6.Q &= Queue:Browse
  BRW6.RetainRow = 0
  BRW6.AddSortOrder(,rtd:Repair_Type_Key)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,rtd:Repair_Type,1,BRW6)
  BIND('tmp:tag',tmp:tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW6.AddField(tmp:tag,BRW6.Q.tmp:tag)
  BRW6.AddField(rtd:Repair_Type,BRW6.Q.rtd:Repair_Type)
  BRW8.Q &= Queue:Browse:1
  BRW8.RetainRow = 0
  BRW8.AddSortOrder(,tratmp:AccountNoKey)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,tratmp:Account_Number,1,BRW8)
  BIND('tmp:tag2',tmp:tag2)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW8.AddField(tmp:tag2,BRW8.Q.tmp:tag2)
  BRW8.AddField(tratmp:Account_Number,BRW8.Q.tratmp:Account_Number)
  BRW8.AddField(tratmp:Company_Name,BRW8.Q.tratmp:Company_Name)
  BRW8.AddField(tratmp:RefNumber,BRW8.Q.tratmp:RefNumber)
  BRW10.Q &= Queue:Browse:2
  BRW10.RetainRow = 0
  BRW10.AddSortOrder(,man:Manufacturer_Key)
  BRW10.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(,man:Manufacturer,1,BRW10)
  BIND('tmp:tag3',tmp:tag3)
  ?List:3{PROP:IconList,1} = '~notick1.ico'
  ?List:3{PROP:IconList,2} = '~tick1.ico'
  BRW10.AddField(tmp:tag3,BRW10.Q.tmp:tag3)
  BRW10.AddField(man:Manufacturer,BRW10.Q.man:Manufacturer)
  BRW10.AddField(man:RecordNumber,BRW10.Q.man:RecordNumber)
  BRW12.Q &= Queue:Browse:3
  BRW12.RetainRow = 0
  BRW12.AddSortOrder(,tea:Team_Key)
  BRW12.AddLocator(BRW12::Sort0:Locator)
  BRW12::Sort0:Locator.Init(,tea:Team,1,BRW12)
  BIND('tmp:tag4',tmp:tag4)
  ?List:4{PROP:IconList,1} = '~notick1.ico'
  ?List:4{PROP:IconList,2} = '~tick1.ico'
  BRW12.AddField(tmp:tag4,BRW12.Q.tmp:tag4)
  BRW12.AddField(tea:Team,BRW12.Q.tea:Team)
  BRW12.AddField(tea:Record_Number,BRW12.Q.tea:Record_Number)
  BRW14.Q &= Queue:Browse:4
  BRW14.RetainRow = 0
  BRW14.AddSortOrder(,use:surname_key)
  BRW14.AddLocator(BRW14::Sort0:Locator)
  BRW14::Sort0:Locator.Init(,use:Surname,1,BRW14)
  BIND('tmp:tag5',tmp:tag5)
  ?List:5{PROP:IconList,1} = '~notick1.ico'
  ?List:5{PROP:IconList,2} = '~tick1.ico'
  BRW14.AddField(tmp:tag5,BRW14.Q.tmp:tag5)
  BRW14.AddField(use:Surname,BRW14.Q.use:Surname)
  BRW14.AddField(use:Forename,BRW14.Q.use:Forename)
  BRW14.AddField(use:User_Code,BRW14.Q.use:User_Code)
  IF ?tmp:selectall{Prop:Checked} = True
    tmp:ThirdParty = 1
    tmp:AwaitingParts = 1
    tmp:Exchanged = 1
    tmp:Loaned = 1
    tmp:Estimate = 1
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW6.AskProcedure = 0
      CLEAR(BRW6.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW10.AskProcedure = 0
      CLEAR(BRW10.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW12.AskProcedure = 0
      CLEAR(BRW12.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW14.AskProcedure = 0
      CLEAR(BRW14.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue3)
  ?DASSHOWTAG:3{PROP:Text} = 'Show All'
  ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue4)
  ?DASSHOWTAG:4{PROP:Text} = 'Show All'
  ?DASSHOWTAG:4{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:4{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue5)
  ?DASSHOWTAG:5{PROP:Text} = 'Show All'
  ?DASSHOWTAG:5{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:5{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:REPTYDEF.Close
    Relate:TRADETMP.Close
  END
  Remove(glo:tradetmp)
  If Error()
      access:tradetmp.close()
      Remove(glo:tradetmp)
  End!If Error()
  Clear(GLO:G_Select1)
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'RepairPerformanceCriteria',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard5.Validate()
        DISABLE(Wizard5.NextControl())
     ELSE
        ENABLE(Wizard5.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:selectall
      IF ?tmp:selectall{Prop:Checked} = True
        tmp:ThirdParty = 1
        tmp:AwaitingParts = 1
        tmp:Exchanged = 1
        tmp:Loaned = 1
        tmp:Estimate = 1
      END
      ThisWindow.Reset
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ExportCriteria
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ExportCriteria, Accepted)
      glo:CriteriaFile   = 'REPPERF.TXT'
      Remove(Criteria)
      Open(Criteria)
      If Error()
          Create(Criteria)
          Open(Criteria)
      End!If Error()
      
      !Dates
      Clear(crit:Record)
      crit:Field1 = Format(tmp:Startdate,@d6)
      Add(Criteria)
      Clear(crit:Record)
      crit:Field1 = Format(tmp:EndDate,@d6)
      Add(Criteria)
      If Error()
          Stop('Add Error: ' & error())
      End!If Error()
      !Teams
      Clear(crit:Record)
      Loop x# = 1 To Records(glo:Queue4)
          Get(glo:Queue4,x#)
          If x# = 1
              crit:Field1 = Clip(glo:Pointer4)
          Else!If x# = 1
              crit:Field1 = Clip(crit:Field1) & '|' & Clip(glo:Pointer4)
          End!If x# = 1
      End!Loop x# = 1 To Records(glo:Queue4)
      Add(Criteria)
      !Manufacturers
      Clear(crit:Record)
      Loop x# = 1 To Records(glo:Queue3)
          Get(glo:Queue3,x#)
          If x# = 1
              crit:Field1 = Clip(glo:Pointer3)
          Else!If x# = 1
              crit:Field1 = Clip(crit:Field1) & '|' & Clip(glo:Pointer3)
          End!If x# = 1
      End!Loop x# = 1 To Records(glo:Queue4)
      Add(Criteria)
      !Engineers
      Clear(crit:Record)
      Loop x# = 1 To Records(glo:Queue5)
          Get(glo:Queue5,x#)
          If x# = 1
              crit:Field1 = Clip(glo:Pointer5)
          Else!If x# = 1
              crit:Field1 = Clip(crit:Field1) & '|' & Clip(glo:Pointer5)
          End!If x# = 1
      End!Loop x# = 1 To Records(glo:Queue4)
      Add(Criteria)
      !Exclusions
      Clear(crit:Record)
      If tmp:ThirdParty = 1
          crit:Field1 = 'Y|'
      Else!If tmp:ThirdParty = 1
          crit:Field1 = 'N|'
      End!If tmp:ThirdParty = 1
      If tmp:AwaitingParts = 1
          crit:Field1 = CLip(crit:Field1) & 'Y|'
      Else!If tmp:AwaitingParts = 1
          crit:Field1 = CLip(crit:Field1) & 'N|'
      End!If tmp:AwaitingParts = 1
      If tmp:Exchanged = 1
          crit:Field1 = CLip(crit:Field1) & 'Y|'
      Else!If tmp:Exchanged = 1
          crit:Field1 = CLip(crit:Field1) & 'N|'
      End!If tmp:Exchanged = 1
      If tmp:Loaned = 1
          crit:Field1 = CLip(crit:Field1) & 'Y|'
      Else!If tmp:Loaned = 1
          crit:Field1 = CLip(crit:Field1) & 'N|'
      End!If tmp:Loaned = 1
      If tmp:Estimate = 1
          crit:Field1 = CLip(crit:Field1) & 'Y'
      Else!If tmp:Estimate = 1
          crit:Field1 = CLip(crit:Field1) & 'N'
      End!If tmp:Estimate = 1
      Add(Criteria)
      !Repair Types
      Clear(crit:Record)
      Loop x# = 1 To Records(glo:Queue)
          Get(glo:Queue,x#)
          If x# = 1
               crit:Field1 = Clip(glo:Pointer)
          Else!If x# = 1
              crit:Field1 = Clip(crit:Field1) & '|' & Clip(glo:Pointer)
          End!If x# = 1
      End!Loop x# = 1 To Records(glo:Queue4)
      Add(Criteria)
      !Accounts
      Clear(crit:Record)
      Loop x# = 1 To Records(glo:Queue2)
          Get(glo:Queue2,x#)
          If x# = 1
              crit:Field1 = Clip(glo:Pointer2)
          Else!If x# = 1
              crit:Field1 = Clip(crit:Field1) & '|' & Clip(glo:Pointer2)
          End!If x# = 1
      End!Loop x# = 1 To Records(glo:Queue4)
      Add(Criteria)
      Close(Criteria)
      Run('REP_PERF.EXE')
      
      
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ExportCriteria, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:startdate = TINCALENDARStyle1(tmp:startdate)
          Display(?tmp:startdate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:enddate = TINCALENDARStyle1(tmp:enddate)
          Display(?tmp:enddate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?VSBackButton
      ThisWindow.Update
         Wizard5.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
         Wizard5.TakeAccepted()
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      IF tmp:csvcomp = 0 And tmp:csvincomp = 0 And tmp:csvexc = 0 And tmp:summary = 0
          Case MessageEx('You must select to either print a report, or to create an export type.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!!IF tmp:csvcomp = 0 And tmp:csvincomp = 0 And tmp:csvexc = 0 And tmp:summary = 0
      
          If tmp:csvcomp = 1 Or tmp:csvincomp = 1 Or tmp:csvexc = 1
              tmp:count   = 0
              sav:path    = glo:file_name
              savepath = path()
              set(defaults)
              access:defaults.next()
              If def:exportpath <> ''
                  glo:file_name = Clip(def:exportpath) & '\REPPERFM.CSV'
              Else!If def:exportpath <> ''
                  glo:file_name = 'C:\REPPERFM.CSV'
              End!If def:exportpath <> ''
      
              if not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
                          file:keepdir + file:noerror + file:longname)
                  !failed
                  setpath(savepath)
              else!if not filedialog
                  !found
                  setpath(savepath)
                  Remove(expgen)
                  access:expgen.open()
                  access:expgen.usefile()
      
                  !Write The Title If An Export Is Required
                  Clear(gen:record)
                  gen:line1   = 'Repair Performance Report'
                  access:expgen.insert()
                  Clear(gen:record)
                  access:expgen.insert()
                  Clear(gen:record)
                  gen:line1   = 'Criteria: '
                  If tmp:csvcomp = 1
                      gen:line1   = Clip(gen:line1) & ',Completed Jobs'
                  End!If tmp:csvcomp = 1
                  If tmp:csvincomp = 1
                      gen:line1   = Clip(gen:line1) & ',Incomplete Jobs'
                  End!If tmp:csvincomp = 1
                  If tmp:csvexc = 1
                      gen:line1   = Clip(gen:line1) & ',Excluded Jobs'
                  End!If tmp:csvexc = 1
                  access:expgen.insert()
                  Clear(gen:record)
                  gen:line1   = 'Booking Date,Completion Date,Job Number,Manufacturer,Model,Account Number,Account Name,'
                  gen:line1   = Clip(gen:line1) & 'Job Status,Engineer,Location,3rd Party Agent,Date Despatched,'
                  gen:line1   = Clip(gen:line1) & 'Date Returned,3rd Party Agent,Awaiting Parts,Exchanged,Loaned,'
                  gen:line1   = Clip(gen:line1) & 'BER,Estimated,In Workshop Date,In Workshop Time'
                  access:expgen.insert()
              end!if not filedialog
          End!If tmp:csvcomp = 1 Or tmp:csvincomp = 1 Or tmp:csvexc = 1
      
              Clear(GLO:G_Select1)
              ! Start Change 2647 BE(16/06/03)
              IsIntec# = INSTRING('INTEC',CLIP(UPPER(def:User_Name)),1,1)
      
              recordspercycle         = 25
              recordsprocessed        = 0
              percentprogress         = 0
              progress:thermometer    = 0
              thiswindow.reset(1)
              open(progresswindow)
              count# = 0
              IF (IsIntec#) THEN
                  count# = Records(jobse)
              ELSE
                  ?progress:userstring{prop:text} = 'Preparing To Run...'
                  ?progress:pcttext{prop:hide} = 1
      
                  recordstoprocess    = 5000
      
                  count# = 0
                  ! Work out a counter
                  setcursor(cursor:wait)
                  save_job_id = access:jobs.savefile()
                  access:jobs.clearkey(job:date_booked_key)
                  job:date_booked = tmp:startdate
                  set(job:date_booked_key,job:date_booked_key)
                  loop
                      if access:jobs.next()
                          break
                      end !if
                      if job:date_booked > tmp:enddate      |
                          then break.  ! end if
                      Do GetNextRecord2
                      count# += 1
                      If count# > 5000
                          count# = Records(jobs)
                          Break
                      End!If count# > 5000
                  end !loop
                  access:jobs.restorefile(save_job_id)
                  setcursor()
      
                  recordsprocessed = 0
                  percentprogress  = 0
                  progress:thermometer    = 0
              END
              ! End Change 2647 BE(16/06/03)
      
              ?progress:pcttext{prop:text} = '0% Completed'
              ?progress:pcttext{prop:hide} = 0
              ?progress:userstring{prop:text} = 'Running....'
      
              recordstoprocess    = count#
      
          !---before routine
              !Go through jobs in date order
      !        setcursor(cursor:wait)
              save_job_id = access:jobs.savefile()
      
              ! Start Change 2647 BE(16/06/03)
              !access:jobs.clearkey(job:date_booked_key)
              !job:date_booked = tmp:startdate
              !set(job:date_booked_key,job:date_booked_key)
              IF (IsIntec#) THEN
                  set(jobse)
              ELSE
                  access:jobs.clearkey(job:date_booked_key)
                  job:date_booked = tmp:startdate
                  set(job:date_booked_key,job:date_booked_key)
              END
              ! End Change 2647 BE(16/06/03)
      
              loop
      
                  ! Start Change 2647 BE(16/06/03)
                  !if access:jobs.next()
                  !   break
                  !end !if
                  !if job:date_booked > tmp:enddate      |
                  !    then break.  ! end if
                  IF (IsIntec#) THEN
                      IF (access:jobse.next() <> level:benign) THEN
                          BREAK
                      END
                      IF ((jobe:InWorkshopDate < tmp:startdate) OR (jobe:InWorkshopDate > tmp:enddate)) THEN
                          CYCLE
                      END
                      access:jobs.clearkey(job:Ref_Number_key)
                      job:Ref_Number = jobe:RefNumber
                      IF (access:jobs.fetch(job:Ref_Number_key) <> level:benign) THEN
                          CYCLE
                      END
                  ELSE
                      IF ((access:jobs.next() <> level:benign) OR (job:date_booked > tmp:enddate)) THEN
                          BREAK
                      END
                  END
                  ! End Change 2647 BE(16/06/03)
      
                  do getnextrecord2
                  cancelcheck# += 1
                  if cancelcheck# > (recordstoprocess/100)
                      do cancelcheck
                      if tmp:cancel = 1
                          break
                      end!if tmp:cancel = 1
                      cancelcheck# = 0
                  end!if cancelcheck# > 50
      
                  !Is the job's account number in the inclusions list?
                  Sort(glo:queue2,glo:pointer2)
                  glo:pointer2    = job:account_number
                  Get(glo:queue2,glo:pointer2)
                  If Error()
                      access:subtracc.clearkey(sub:account_number_key)
                      sub:account_number = job:account_number
                      if access:subtracc.fetch(sub:account_number_key) = level:benign
      !                    access:tradeacc.clearkey(tra:account_number_key)
      !                    tra:account_number = sub:main_account_number
      !                    if access:tradeacc.fetch(tra:account_number_key) = level:benign
                              Sort(glo:queue2,glo:pointer2)
      !                        glo:pointer2    = tra:account_number
                              glo:Pointer2    = sub:Main_Account_Number
                              Get(glo:queue2,glo:pointer2)
                              If Error()
                                  Cycle
                              End!If Error()
      !                    end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                      end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
                  End!If Error()
      
                  !Is the job's manufacturer in the inclusions list?
                  Sort(glo:queue3,glo:pointer3)
                  glo:pointer3    = job:manufacturer
                  Get(glo:queue3,glo:pointer3)
                  If error()
                      Cycle
                  End!If error()
      
                  !Is the job's engineer team in the inclusions list?
                  access:users.clearkey(use:user_code_key)
                  use:user_code   = job:engineer
                  if access:users.tryfetch(use:user_code_key) = LeveL:Benign
                      Sort(glo:queue4,glo:pointer4)
                      glo:pointer4    = use:team
                      Get(glo:queue4,glo:pointer4)
                      If error()
                          Cycle
                      End!If error()
                  End!if access:users.tryfetch(use:user_code_key) = LeveL:Benign
      
                  !Is the job's engineer team in the inclusions list?
                  Sort(glo:queue5,glo:queue5)
                  glo:queue5      = job:engineer
                  Get(glo:queue5,glo:pointer5)
                  if Error()
                      Cycle
                  End!if Error()
      
                  !Count Total Jobs
                  glo:select16 +=     1       !Total jobs
                  If job:date_completed = ''
                      !Count Total Incomplete Jobs
                      glo:select17    += 1        !Incomplete Jobs
                      If tmp:csvincomp = 1
                          Do insert_export
                      End!If tmp:csvincomp = 1
                  End!If job:date_completed = ''
      
                  !If third party is exclusion
                  If tmp:ThirdParty = 1
                      If job:third_party_site <> ''
                          If job:date_completed <> ''
                              glo:select8 += 1
                              If tmp:csvexc = 1 And tmp:csvcomp = 1
                                  Do insert_export
                              End!If tmp:csvexc = 1 And tmp:csvcomp = 1
                          Else!If job:date_completed <> ''
                              glo:select25 += 1
                              If tmp:csvexc = 1 And tmp:csvincomp = 1
                                  Do insert_export
                              End!If tmp:csvexc = 1 And tmp:csvcomp = 1
                          End!If job:date_completed <> ''
                          Cycle
                      End!If job:third_party_site <> ''
                  End!If tmp:ThirdParty = 1
      
                  !If awaiting parts is an exlusion
                  If tmp:AwaitingParts = 1
                      found# = 0
                      save_par_id = access:parts.savefile()
                      access:parts.clearkey(par:order_number_key)
                      par:ref_number   = job:ref_number
                      set(par:order_number_key,par:order_number_key)
                      loop
                          if access:parts.next()
                             break
                          end !if
                          if par:ref_number   <> job:ref_number      |
                              then break.  ! end if
                          if par:order_number <> ''
                              found# = 1
                              Break
                          End!if par:order_number <> ''
                      end !loop
                      access:parts.restorefile(save_par_id)
      
                      If found# = 0
                          save_wpr_id = access:warparts.savefile()
                          access:warparts.clearkey(wpr:order_number_key)
                          wpr:ref_number   = job:ref_number
                          set(wpr:order_number_key,wpr:order_number_key)
                          loop
                              if access:warparts.next()
                                 break
                              end !if
                              if wpr:ref_number   <> job:ref_number      |
                                  then break.  ! end if
                              If wpr:order_number <> ''
                                  found# = 1
                                  Break
                              End!If wpr:order_number <> ''
                          end !loop
                          access:warparts.restorefile(save_wpr_id)
                      End!If found# = 0
                      If found# = 1
                          If job:date_completed <> ''
                              glo:select9 += 1
                              If tmp:csvexc = 1 And tmp:csvcomp = 1
                                  Do insert_export
                              End!If tmp:csvexc = 1 And tmp:csvcomp = 1
                          Else!If job:date_completed <> ''
                              glo:select26 += 1
                              If tmp:csvexc = 1 And tmp:csvincomp = 1
                                  Do insert_export
                              End!If tmp:csvexc = 1 And tmp:csvcomp = 1
                          End!If job:date_completed <> ''
                          Cycle
                      End!If found# = 1
                  End!If tmp:AwaitingParts = 1
      
                  !If job exchanged is an exlusion
                  If tmp:exchanged = 1
                      found# = 0
                      If job:exchange_unit_number <> ''
                          If job:date_completed <> ''
                              glo:select10 += 1
                              If tmp:csvexc = 1 And tmp:csvcomp = 1
                                  Do insert_export
                              End!If tmp:csvexc = 1 And tmp:csvcomp = 1
                          Else!If job:date_completed <> ''
                              glo:select27 += 1
                              If tmp:csvexc = 1 And tmp:csvincomp = 1
                                  Do insert_export
                              End!If tmp:csvexc = 1 And tmp:csvcomp = 1
                          End!If job:date_completed <> ''
                          Cycle
                      End!If job:exchange_unit_number <> ''
                  End!If tmp:exchanged = 1
      
                  !If job loaned is an exclusion
                  if tmp:loaned = 1
                      If job:loan_unit_number <> ''
                          If job:date_completed <> ''
                              If tmp:csvexc = 1 And tmp:csvcomp = 1
                                  Do insert_export
                              End!If tmp:csvexc = 1 And tmp:csvcomp = 1
                              glo:select11    += 1
                          Else!If job:date_completed <> ''
                              glo:select28 += 1
                              If tmp:csvexc = 1 And tmp:csvincomp = 1
                                  Do insert_export
                              End!If tmp:csvexc = 1 And tmp:csvcomp = 1
                          End!If job:date_completed <> ''
                          Cycle
                      End!If job:loan_unit_number <> ''
                  End!if tmp:loaned = 1
      
                  !If repair types are excluded
                  found# = 0
                  Sort(glo:queue,glo:pointer)
                  glo:pointer = job:repair_type
                  Get(glo:queue,glo:pointer)
                  If Error()
                      Sort(glo:queue,glo:pointer)
                      glo:pointer = job:repair_type_warranty
                      Get(glo:queue,glo:pointer)
                      If Error()
      
                      Else!If Error()
                          found# = 1
                      End!If Error()
                  Else!If Error()
                      found# = 1
                  End!If Error()
                  if found# = 1
                      If job:date_Completed <> ''
                          glo:select12    += 1
                          If tmp:csvexc = 1 And tmp:csvcomp = 1
                              Do insert_export
                          End!If tmp:csvexc = 1 And tmp:csvcomp = 1
                      Else!If job:date_Completed <> ''
                          glo:select29 += 1
                          If tmp:csvexc = 1 And tmp:csvincomp = 1
                              Do insert_export
                          End!If tmp:csvexc = 1 And tmp:csvcomp = 1
                      End!If job:date_Completed <> ''
                      Cycle
                  End!if found# = 1
      
                  !If job has been estimated
                  If tmp:Estimate = 1
                      If job:estimate = 'YES'
                          If job:date_completed <> ''
                              glo:select13    += 1
                              If tmp:csvexc = 1 And tmp:csvcomp = 1
                                  Do insert_export
                              End!If tmp:csvexc = 1 And tmp:csvcomp = 1
                          Else!If job:date_completed <> ''
                              glo:select30    += 1
                              If tmp:csvexc = 1 And tmp:csvincomp = 1
                                  Do insert_export
                              End!If tmp:csvexc = 1 And tmp:csvcomp = 1
                          End!If job:date_completed <> ''
                          Cycle
                      End!If job:estimate = 'YES'
                  End!If tmp:Estimate = 1
      
                  If job:date_completed <> ''
                      If tmp:csvcomp = 1
                          Do insert_export
                      End!If tmp:csvcomp = 1
      
      
                      ! Start Change 2647 BE(22/05/2003)
                      !If job:date_completed = job:date_booked
                      !    glo:select1 += 1
                      !End!If job:date_completed = job:date_booked
                      !If job:date_completed = job:date_booked + 1
                      !    glo:select2 += 1
                      !End!If job:date_complete = job:date_booked + 1
                      !If job:date_completed = job:date_booked + 2
                      !    glo:select3 += 1
                      !End!If job:date_completed = job:date_booked + 2
                      !If job:date_completed = job:date_booked + 3
                      !    glo:select4 += 1
                      !End!If job:date_completed = job:date_booked + 3
                      !If job:date_completed = job:date_booked + 4
                      !    glo:select5 += 1
                      !End!If job:date_completed = job:date_booked + 4
                      !If job:date_completed => job:date_booked + 5
                      !    glo:select6 += 1
                      !End!If job:date_completed => job:date_booked + 5
      
                      IF (INSTRING('INTEC',CLIP(UPPER(def:User_Name)),1,1)) THEN
                          Access:JOBSE.Clearkey(jobe:RefNumberKey)
                          jobe:RefNumber  = job:Ref_Number
                          IF (Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign) THEN
                              IF (jobe:InWorkshopDate <> '') THEN
                                  IF (job:date_completed = jobe:InWorkshopDate) THEN
                                      glo:select1 += 1
                                  ELSIF (job:date_completed = jobe:InWorkshopDate + 1) THEN
                                      glo:select2 += 1
                                  ELSIF (job:date_completed = jobe:InWorkshopDate + 2) THEN
                                      glo:select3 += 1
                                  ELSIF (job:date_completed = jobe:InWorkshopDate + 3) THEN
                                      glo:select4 += 1
                                  ELSIF (job:date_completed = jobe:InWorkshopDate + 4) THEN
                                      glo:select5 += 1
                                  ELSIF (job:date_completed => jobe:InWorkshopDate + 5) THEN
                                      glo:select6 += 1
                                  END
                              END
                          END
                      ELSE
                          IF (job:date_completed = job:date_booked) THEN
                              glo:select1 += 1
                          ELSIF (job:date_completed = job:date_booked + 1) THEN
                              glo:select2 += 1
                          ELSIF (job:date_completed = job:date_booked + 2) THEN
                              glo:select3 += 1
                          ELSIF (job:date_completed = job:date_booked + 3) THEN
                              glo:select4 += 1
                          ELSIF (job:date_completed = job:date_booked + 4) THEN
                              glo:select5 += 1
                          ELSIF (job:date_completed => job:date_booked + 5) THEN
                              glo:select6 += 1
                          END
                      END
                      ! End Change 2647 BE(22/05/2003)
                  End!If job:date_completed <> ''
      
              end !loop
              access:jobs.restorefile(save_job_id)
      !        setcursor()
          !---insert routine
      
          !---after routine
              do endprintrun
              close(progresswindow)
              glo:select14    = tmp:startdate
              glo:select15    = tmp:enddate
      
          If tmp:csvcomp = 1 Or tmp:csvincomp = 1 Or tmp:csvexc = 1
              Clear(gen:record)
              gen:line1   = 'Total:,' & Clip(tmp:count)
              access:expgen.insert()
              access:expgen.close()
              glo:file_name   = sav:path
              Case MessageEx('Export Completed.','ServiceBase 2000',|
                             'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          End!If tmp:csvcomp = 1 Or tmp:csvincomp = 1 Or tmp:csvexc = 1
      
          If tmp:summary = 1
              glo:select20    = 'NO'
              glo:select21    = 'NO'
              glo:select22    = 'NO'
              glo:select23    = 'NO'
              glo:select24    = 'NO'
      
              If tmp:thirdparty = 1
                  glo:select20    = 'YES'
              End!If tmp:thirdparty = 1
              If tmp:AwaitingParts = 1
                  glo:select21    = 'YES'
              End!If tmp:AwaitingParts = 1
              If tmp:exchanged = 1
                  glo:select22    = 'YES'
              End!If tmp:exchanged = 1
              If tmp:loaned   = 1
                  glo:select23    = 'YES'
              End!If tmp:loaned   = 1
              If tmp:estimate = 1
                  glo:select24    = 'YES'
              End!If tmp:estimate = 1
      
              RepairPerformanceReport
              glo:select20    = ''
              glo:select21    = ''
              glo:select22    = ''
              glo:select23    = ''
              glo:select24    = ''
          End!If tmp:summary = 1
      Compile('***',Debug=1)
          Message('glo:select16: ' & glo:select16 &|
                  '|glo:select17: ' & glo:select17 & |
                  '|tmp:count: ' & tmp:count,'Debug Message',icon:exclamation)
      ***
      End!IF tmp:csvcomp = 0 And tmp:csvincomp = 0 And tmp:csvexc = 0 And tmp:summary = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'RepairPerformanceCriteria')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?tmp:startdate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?tmp:enddate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::7:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::9:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:3
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:3{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:3{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::11:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:3)
               ?List:3{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:4
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:4{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:4{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::13:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:4)
               ?List:4{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:5
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:5{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:5{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::15:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:5)
               ?List:5{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::9:DASUNTAGALL
      Do DASBRW::11:DASUNTAGALL
      Do DASBRW::13:DASUNTAGALL
      Do DASBRW::15:DASUNTAGALL
      Do DASBRW::7:DASUNTAGALL
      Select(?sheet1,1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW6.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = rtd:Repair_Type
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:tag = ''
    ELSE
      tmp:tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag = '*')
    SELF.Q.tmp:tag_Icon = 2
  ELSE
    SELF.Q.tmp:tag_Icon = 1
  END


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW6::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW6::RecordStatus=ReturnValue
  IF BRW6::RecordStatus NOT=Record:OK THEN RETURN BRW6::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = rtd:Repair_Type
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::7:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW6::RecordStatus
  RETURN ReturnValue


BRW8.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tratmp:Account_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:tag2 = ''
    ELSE
      tmp:tag2 = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag2 = '*')
    SELF.Q.tmp:tag2_Icon = 2
  ELSE
    SELF.Q.tmp:tag2_Icon = 1
  END


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW8::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW8::RecordStatus=ReturnValue
  IF BRW8::RecordStatus NOT=Record:OK THEN RETURN BRW8::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tratmp:Account_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::9:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW8::RecordStatus
  RETURN ReturnValue


BRW10.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue3.Pointer3 = man:Manufacturer
     GET(glo:Queue3,glo:Queue3.Pointer3)
    IF ERRORCODE()
      tmp:tag3 = ''
    ELSE
      tmp:tag3 = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag3 = '*')
    SELF.Q.tmp:tag3_Icon = 2
  ELSE
    SELF.Q.tmp:tag3_Icon = 1
  END


BRW10.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW10::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW10::RecordStatus=ReturnValue
  IF BRW10::RecordStatus NOT=Record:OK THEN RETURN BRW10::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue3.Pointer3 = man:Manufacturer
     GET(glo:Queue3,glo:Queue3.Pointer3)
    EXECUTE DASBRW::11:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW10::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW10::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW10::RecordStatus
  RETURN ReturnValue


BRW12.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue4.Pointer4 = tea:Team
     GET(glo:Queue4,glo:Queue4.Pointer4)
    IF ERRORCODE()
      tmp:tag4 = ''
    ELSE
      tmp:tag4 = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag4 = '*')
    SELF.Q.tmp:tag4_Icon = 2
  ELSE
    SELF.Q.tmp:tag4_Icon = 1
  END


BRW12.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW12.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW12::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW12::RecordStatus=ReturnValue
  IF BRW12::RecordStatus NOT=Record:OK THEN RETURN BRW12::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue4.Pointer4 = tea:Team
     GET(glo:Queue4,glo:Queue4.Pointer4)
    EXECUTE DASBRW::13:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW12::RecordStatus
  RETURN ReturnValue


BRW14.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue5.Pointer5 = use:User_Code
     GET(glo:Queue5,glo:Queue5.Pointer5)
    IF ERRORCODE()
      tmp:tag5 = ''
    ELSE
      tmp:tag5 = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag5 = '*')
    SELF.Q.tmp:tag5_Icon = 2
  ELSE
    SELF.Q.tmp:tag5_Icon = 1
  END


BRW14.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW14.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW14::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW14::RecordStatus=ReturnValue
  IF BRW14::RecordStatus NOT=Record:OK THEN RETURN BRW14::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue5.Pointer5 = use:User_Code
     GET(glo:Queue5,glo:Queue5.Pointer5)
    EXECUTE DASBRW::15:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW14::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW14::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW14::RecordStatus
  RETURN ReturnValue

Wizard5.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW6.Q &= NULL) ! Has Browse Object been initialized?
       BRW6.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW8.Q &= NULL) ! Has Browse Object been initialized?
       BRW8.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW10.Q &= NULL) ! Has Browse Object been initialized?
       BRW10.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW12.Q &= NULL) ! Has Browse Object been initialized?
       BRW12.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW14.Q &= NULL) ! Has Browse Object been initialized?
       BRW14.ResetQueue(Reset:Queue)
    END

Wizard5.TakeBackEmbed PROCEDURE
   CODE

Wizard5.TakeNextEmbed PROCEDURE
   CODE

Wizard5.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
