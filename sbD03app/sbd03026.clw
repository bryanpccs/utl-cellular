

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03026.INC'),ONCE        !Local module procedure declarations
                     END








Exchange_Unit_Audit_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
Unit_Group           GROUP,PRE()
stock_unit_temp      STRING(60)
replacement_unit_temp STRING(60)
stock_model_temp     STRING(30)
stock_status_temp    STRING(30)
replacement_model_temp STRING(30)
replacement_status_temp STRING(30)
                     END
lines_total_temp     REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(EXCAUDIT)
                       PROJECT(exa:Audit_Number)
                     END
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1000),USE(?unnamed)
                         STRING('Stock Type:'),AT(5000,156),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s30),AT(5781,156),USE(GLO:Select1),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(5000,365),USE(?String28),TRN,FONT(,8,,)
                         STRING(@s60),AT(5781,365),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(5000,781),USE(?String32),TRN,FONT(,8,,)
                         STRING('Date Printed:'),AT(5000,573),USE(?ReportDatePrompt),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5781,573),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@n-7),AT(5781,781),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6250,781),USE(?String34),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6458,781,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,188),USE(?detailband)
                           STRING(@s8),AT(104,0),USE(exa:Audit_Number),TRN,RIGHT,FONT(,7,,)
                           STRING(@s16),AT(729,0),USE(stock_unit_temp),TRN,LEFT,FONT(,7,,)
                           STRING(@s16),AT(3958,0),USE(replacement_unit_temp),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s18),AT(4844,0),USE(replacement_model_temp),TRN,FONT(,7,,)
                           STRING(@s24),AT(5833,0),USE(replacement_status_temp),TRN,LEFT,FONT(,7,,)
                           STRING(@s24),AT(2604,0),USE(stock_status_temp),TRN,FONT(,7,,)
                           STRING(@s18),AT(1615,0),USE(stock_model_temp),TRN,LEFT,FONT(,7,,)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           LINE,AT(219,52,7083,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Lines:'),AT(208,156),USE(?String27),TRN
                           STRING(@s8),AT(1667,156),USE(lines_total_temp),TRN,LEFT,FONT(,,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('EXCHANGE UNIT AUDIT REPORT'),AT(4271,0,3177,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Audit No'),AT(156,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Stock Unit'),AT(729,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Replacement Unit'),AT(3958,2083),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Status'),AT(5833,2083),USE(?string24:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Status'),AT(2604,2083),USE(?string45:3),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Exchange_Unit_Audit_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:EXCAUDIT.Open
  Relate:DEFAULTS.Open
  Access:EXCHANGE.UseFile
  Access:EXCHANGE_ALIAS.UseFile
  
  
  RecordsToProcess = RECORDS(EXCAUDIT)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(EXCAUDIT,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(exa:Audit_Number_Key)
      Process:View{Prop:Filter} = |
      'UPPER(exa:Stock_Type) = UPPER(GLO:Select1)'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        print# = 1
        Clear(Unit_Group)
        
        If exa:stock_unit_number <> ''
            access:exchange.clearkey(xch:ref_number_stock_key)
            xch:stock_type = glo:select1
            xch:ref_number = exa:stock_unit_number
            if access:exchange.tryfetch(xch:ref_number_stock_key) = Level:Benign
                stock_unit_temp  = xch:esn
                stock_model_temp = xch:model_number
                Case xch:available
                    Of 'AVL'
                        If glo:select2 <> 'YES'
                            print# = 0
                        End!If glo:select2 <> 'YES'
                        stock_status_temp = 'AVAILABLE'
                    Of 'INC'
                        If glo:select3 <> 'YES'
                            print# = 0
                        End!If glo:select2 <> 'YES'
                        stock_status_temp = 'INCOMING - JOB: ' & CLip(xch:job_number)
                    Of 'REP'
                        If glo:select4 <> 'YES'
                            print# = 0
                        End!If glo:select4 <> 'YES'
                        stock_status_temp = 'IN REPAIR - JOB: ' & Clip(xch:job_number)
                    Of 'EXC'
                        If glo:select5 <> 'YES'
                            print# = 0
                        End!If glo:select2 <> 'YES'
                        stock_status_temp = 'EXCHANGED - JOB: ' & CLip(xch:job_number)
                    Of 'DES'
                        If glo:select6 <> 'YES'
                            print# = 0
                        End!If glo:select2 <> 'YES'
                        stock_status_temp = 'DESPATCHED - JOB: ' & Clip(xch:job_number)
                    Of 'SUS'
                        If glo:select7 <> 'YES'
                            print# = 0
                        End!If glo:select2 <> 'YES'
                        stock_status_temp = 'SUSPENDED'
                End!Case xch:available
        
            end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
        
            If exa:replacement_unit_number <> ''
                access:exchange.clearkey(xch:ref_number_stock_key)
                xch:stock_type = glo:select1
                xch:ref_number = exa:replacement_unit_number
                if access:exchange.fetch(xch:ref_number_stock_key) = Level:Benign
                    replacement_unit_temp  = xch:esn
                    replacement_model_temp = xch:model_number
                    Case xch:available
                        Of 'AVL'
                            replacement_status_temp = 'AVAILABLE'
                        Of 'INC'
                            replacement_status_temp = 'INCOMING - JOB: ' & CLip(xch:job_number)
                        Of 'REP'
                            replacement_status_temp = 'IN REPAIR - JOB: ' & CLip(xch:Job_number)
                        Of 'EXC'
                            replacement_status_temp = 'EXCHANGED - JOB: ' & CLip(xch:job_number)
                        Of 'DES'
                            replacement_status_temp = 'DESPATCHED - JOB: ' & Clip(xch:job_number)
                        Of 'SUS'
                            replacement_status_temp = 'SUSPENDED'
                    End!Case xch:available
                end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
            Else
                replacement_status_temp = ''
            End!If exa:replacement_unit_number <> ''
        
            If print# = 1
                lines_total_temp += 1
                tmp:RecordsCount += 1
                Print(rpt:detail)
            End!If print# = 1
        End!If exa:stock_unit_number <> ''
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(EXCAUDIT,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:EXCAUDIT.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'EXCAUDIT')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Exchange_Unit_Audit_Report'
  END
  report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Exchange_Unit_Audit_Report',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('InitialPath',InitialPath,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('Unit_Group:stock_unit_temp',Unit_Group:stock_unit_temp,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('Unit_Group:replacement_unit_temp',Unit_Group:replacement_unit_temp,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('Unit_Group:stock_model_temp',Unit_Group:stock_model_temp,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('Unit_Group:stock_status_temp',Unit_Group:stock_status_temp,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('Unit_Group:replacement_model_temp',Unit_Group:replacement_model_temp,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('Unit_Group:replacement_status_temp',Unit_Group:replacement_status_temp,'Exchange_Unit_Audit_Report',1)
    SolaceViewVars('lines_total_temp',lines_total_temp,'Exchange_Unit_Audit_Report',1)


BuildCtrlQueue      Routine







