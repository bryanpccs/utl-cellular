

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03092.INC'),ONCE        !Local module procedure declarations
                     END








ToteReport PROCEDURE(func:StartDate,func:EndDate,func:Detailed,func:ExportPath)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:ExportPath       STRING(255),STATIC
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
save_cou_id          USHORT,AUTO
save_job_id          USHORT,AUTO
AccountQueue         QUEUE,PRE(accque)
AccountNumber        STRING(30)
Count                LONG
                     END
JobNumberQueue       QUEUE,PRE(jobque)
AccountNumber        STRING(30),NAME('jobque:AccountNumber')
JobNumber            LONG,NAME('jobque:JobNumber')
                     END
tmp:Pouches          LONG
tmp:JobNumbers       STRING(5000)
tmp:TotalPouches     LONG
tmp:ShowPreview      BYTE(0)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                     END
! Before Embed Point: %DataSectionBeforeReport) DESC(Data Section, Before Report Declaration) ARG()
ToteExport    File,Driver('BASIC'),Pre(tote),Name(tmp:ExportPath),Create,Bindable,Thread
Record                  Record
AccountNumber               String(30)
JobNumber                   String(30)
                        End
                    End

! After Embed Point: %DataSectionBeforeReport) DESC(Data Section, Before Report Declaration) ARG()
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1104,7521,1000)
                         STRING('Date From'),AT(5000,0),USE(?string22),TRN,FONT(,8,,)
                         STRING(@d6),AT(5833,0),USE(func:StartDate),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Date To'),AT(5000,156),USE(?string22:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5833,156),USE(func:EndDate),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(5000,313),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5833,313),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,469),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5833,469),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,469),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,625),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5833,625),PAGENO,USE(?reportpageno),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6094,625),USE(?string26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6302,625,375,208),USE(?CPCSPgOfPgStr),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,146),USE(?detailband)
                           STRING(@s30),AT(208,0),USE(accque:AccountNumber),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s8),AT(2865,0),USE(tmp:Pouches),FONT(,8,,)
                         END
                         FOOTER,AT(0,0,,813),USE(?unnamed:2)
                           STRING('Total Number Of Pouches:'),AT(208,104),USE(?String29),TRN,FONT(,8,,FONT:bold)
                           LINE,AT(198,52,7135,0),USE(?Line2),COLOR(COLOR:Black)
                           STRING(@s8),AT(2865,104),USE(tmp:TotalPouches),FONT(,8,,FONT:bold)
                           STRING('Collected By (Signed)'),AT(208,521),USE(?CollectedBy),TRN,FONT(,10,,)
                           LINE,AT(1563,677,5677,0),USE(?Line1),COLOR(COLOR:Black)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7500,11198),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('TOTE REPORT'),AT(4063,0,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s15),AT(625,1042),USE(def:Telephone_Number),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s15),AT(625,1198),USE(def:Fax_Number),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING(@s255),AT(625,1354,3750,208),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Store Code'),AT(208,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Number Of Pouches'),AT(2760,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('ToteReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Access:COURIER.UseFile
  Access:USERS.UseFile
  
  
  RecordsToProcess = BYTES(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      RecordsToProcess = 0
      Save_cou_ID = Access:COURIER.SaveFile()
      Access:COURIER.ClearKey(cou:Courier_Type_Key)
      cou:Courier_Type = 'TOTE'
      Set(cou:Courier_Type_Key,cou:Courier_Type_Key)
      Loop
          If Access:COURIER.NEXT()
             Break
          End !If
          If cou:Courier_Type <> 'TOTE'      |
              Then Break.  ! End If
          RecordsToProcess += 1
      End !Loop
      Access:COURIER.RestoreFile(Save_cou_ID)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        Clear(AccountQueue)
        Free(AccountQueue)
        !Go through all the TOTE couriers and find any despatches (loan/exchange/job)
        !in the date range.
        
        !Show the "no records" message, only if printing report.
        If func:Detailed = 0
            tmp:ShowPreview = 1
        End !tmp:Detailed = 0
        
        tmp:TotalPouches = 0
        Save_cou_ID = Access:COURIER.SaveFile()
        Access:COURIER.ClearKey(cou:Courier_Type_Key)
        cou:Courier_Type = 'TOTE'
        Set(cou:Courier_Type_Key,cou:Courier_Type_Key)
        Loop
            If Access:COURIER.NEXT()
               Break
            End !If
            If cou:Courier_Type <> 'TOTE'      |
                Then Break.  ! End If
            RecordsProcessed += 1
            Do DisplayProgress
            Save_job_ID = Access:JOBS.SaveFile()
            Access:JOBS.ClearKey(job:DateDespatchKey)
            job:Courier         = cou:Courier
            job:Date_Despatched = func:StartDate
            Set(job:DateDespatchKey,job:DateDespatchKey)
            Loop
                If Access:JOBS.NEXT()
                   Break
                End !If
                If job:Courier         <> cou:Courier      |
                Or job:Date_Despatched > func:EndDate      |
                    Then Break.  ! End If
                !Only allow one occurance of each Account Number in the queue,
                !and add up the number of jobs for each Account.
        
                accque:AccountNumber    = job:Account_Number
                Get(AccountQueue,accque:AccountNumber)
                If Error()
                    accque:AccountNumber    = job:Account_Number
                    accque:Count            = 1
                    Add(AccountQueue)
                Else !If Error()
                    accque:Count += 1
                    Put(AccountQueue)
                End !If Error()
        
                !Put all the job numbers into a Queue for the Detailed Report.
        
                If func:Detailed
                    jobque:AccountNumber    = job:Account_Number
                    jobque:JobNumber        = job:Ref_Number
                    Add(JobNumberQueue)
                End !If func:Detailed
        
            End !Loop
            Access:JOBS.RestoreFile(Save_job_ID)
        
            Save_job_ID = Access:JOBS.SaveFile()
            Access:JOBS.ClearKey(job:DateDespLoaKey)
            job:Loan_Courier    = cou:Courier
            job:Loan_Despatched = func:StartDate
            Set(job:DateDespLoaKey,job:DateDespLoaKey)
            Loop
                If Access:JOBS.NEXT()
                   Break
                End !If
                If job:Loan_Courier    <> cou:Courier      |
                Or job:Loan_Despatched > func:EndDate      |
                    Then Break.  ! End If
                accque:AccountNumber    = job:Account_Number
                Get(AccountQueue,accque:AccountNumber)
                If Error()
                    accque:AccountNumber    = job:Account_Number
                    accque:Count            = 1
                    Add(AccountQueue)
                Else !If Error()
                    accque:Count += 1
                    Put(AccountQueue)
                End !If Error()
        
                If func:Detailed
                    jobque:AccountNumber    = job:Account_Number
                    jobque:JobNumber        = job:Ref_Number
                    Add(JobNumberQueue)
                End !If func:Detailed
            End !Loop
            Access:JOBS.RestoreFile(Save_job_ID)
        
            Save_job_ID = Access:JOBS.SaveFile()
            Access:JOBS.ClearKey(job:DateDespExcKey)
            job:Exchange_Courier    = cou:Courier
            job:Exchange_Despatched = func:StartDate
            Set(job:DateDespExcKey,job:DateDespExcKey)
            Loop
                If Access:JOBS.NEXT()
                   Break
                End !If
                If job:Exchange_Courier    <> cou:Courier      |
                Or job:Exchange_Despatched > func:EndDate       |
                Then Break.  ! End If
                accque:AccountNumber    = job:Account_Number
                Get(AccountQueue,accque:AccountNumber)
                If Error()
                    accque:AccountNumber    = job:Account_Number
                    accque:Count            = 1
                    Add(AccountQueue)
                Else !If Error()
                    accque:Count += 1
                    Put(AccountQueue)
                End !If Error()
        
                If func:Detailed
                    jobque:AccountNumber    = job:Account_Number
                    jobque:JobNumber        = job:Ref_Number
                    Add(JobNumberQueue)
                End !If func:Detailed
        
            End !Loop
            Access:JOBS.RestoreFile(Save_job_ID)
        End !Loop
        Access:COURIER.RestoreFile(Save_cou_ID)
        
        RecordsToProcess = Records(AccountQueue)
        
        Sort(AccountQueue,accque:AccountNumber)
        Loop x# = 1 To Records(AccountQueue)
            Get(AccountQueue,x#)
            tmp:Pouches = INT(accque:Count/5)
            If tmp:Pouches < accque:Count
                tmp:Pouches += 1
            End !If tmp:Pouches < accque:Count
        
            tmp:TotalPouches += tmp:Pouches
        
            RecordsProcessed += 1
            Do DisplayProgress
        
            tmp:JobNumbers = ''
            If func:Detailed <> 0
                Sort(JobNumberQueue,'jobque:AccountNumber,jobque:JobNumber')
                Loop y# = 1 To Records(JobNumberQueue)
                    Get(JobNumberqueue,y#)
                    If jobque:AccountNumber = accque:AccountNumber
                        Clear(tote:Record)
                        tote:AccountNumber = accque:AccountNumber
                        tote:JobNumber     = jobque:JobNumber
                        Add(ToteExport)
                    End !If jobque:AccountNumber = accque:AccountNumber
                End !Loop y# = 1 To Records(JobNumberQueue)
            End
            If func:Detailed = 0 or func:Detailed = 2
                Print(rpt:Detail)
            End !If func:Detailed
        End !x# = 1 To Records(AccountQueue)
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        IF tmp:ShowPreview = TRUE
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        END
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
        IF tmp:ShowPreview = TRUE
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
        END
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        IF tmp:ShowPreview = TRUE
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        END
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
        IF tmp:ShowPreview = TRUE
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        END
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  ! Before Embed Point: %AfterClosingReport) DESC(After Closing Report) ARG()
  If func:Detailed
      Close(ToteExport)
  End !func:Detailed
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  ! After Embed Point: %AfterClosingReport) DESC(After Closing Report) ARG()
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:COURIER.Close
    Relate:DEFAULTS.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  !Open Export File
  If func:Detailed <> 0
      If Sub(Clip(func:ExportPath),-1,1) = '\'
          tmp:ExportPath   = Clip(func:ExportPath) & 'TOTEEXP.CSV'
      Else !If Sub(Clip(func:ExportPath),-1,1) = '\'
          tmp:ExportPath   = Clip(func:ExportPath) & '\TOTEEXP.CSV'
      End !If Sub(Clip(func:ExportPath),-1,1) = '\'
      Remove(ToteExport)
      
      Open(ToteExport)
      If Error()
          Create(ToteExport)
          Open(ToteExport)
          If Error()
              Stop(Error())
          End !If Error()
      End !If Error()
  
      Clear(tote:Record)
      tote:AccountNumber  = 'Account Number'
      tote:JobNumber      = 'Job Number'
      Add(ToteExport)
  End !func:Detailed <> 0
  
  
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Tote Report'
  END
  report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ToteReport',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'ToteReport',1)
    SolaceViewVars('tmp:ExportPath',tmp:ExportPath,'ToteReport',1)
    SolaceViewVars('LocalRequest',LocalRequest,'ToteReport',1)
    SolaceViewVars('LocalResponse',LocalResponse,'ToteReport',1)
    SolaceViewVars('FilesOpened',FilesOpened,'ToteReport',1)
    SolaceViewVars('WindowOpened',WindowOpened,'ToteReport',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'ToteReport',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'ToteReport',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'ToteReport',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'ToteReport',1)
    SolaceViewVars('PercentProgress',PercentProgress,'ToteReport',1)
    SolaceViewVars('RecordStatus',RecordStatus,'ToteReport',1)
    SolaceViewVars('EndOfReport',EndOfReport,'ToteReport',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'ToteReport',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'ToteReport',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'ToteReport',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'ToteReport',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'ToteReport',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'ToteReport',1)
    SolaceViewVars('InitialPath',InitialPath,'ToteReport',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'ToteReport',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'ToteReport',1)
    SolaceViewVars('tmp:printedby',tmp:printedby,'ToteReport',1)
    SolaceViewVars('tmp:TelephoneNumber',tmp:TelephoneNumber,'ToteReport',1)
    SolaceViewVars('tmp:FaxNumber',tmp:FaxNumber,'ToteReport',1)
    SolaceViewVars('save_cou_id',save_cou_id,'ToteReport',1)
    SolaceViewVars('save_job_id',save_job_id,'ToteReport',1)
    SolaceViewVars('AccountQueue:AccountNumber',AccountQueue:AccountNumber,'ToteReport',1)
    SolaceViewVars('AccountQueue:Count',AccountQueue:Count,'ToteReport',1)
    SolaceViewVars('JobNumberQueue:AccountNumber',JobNumberQueue:AccountNumber,'ToteReport',1)
    SolaceViewVars('JobNumberQueue:JobNumber',JobNumberQueue:JobNumber,'ToteReport',1)
    SolaceViewVars('tmp:Pouches',tmp:Pouches,'ToteReport',1)
    SolaceViewVars('tmp:JobNumbers',tmp:JobNumbers,'ToteReport',1)
    SolaceViewVars('tmp:TotalPouches',tmp:TotalPouches,'ToteReport',1)
    SolaceViewVars('tmp:ShowPreview',tmp:ShowPreview,'ToteReport',1)


BuildCtrlQueue      Routine







