

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03050.INC'),ONCE        !Local module procedure declarations
                     END








Stock_Forecast_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
save_stm_id          USHORT,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
save_shi_id          USHORT,AUTO
count_temp           REAL
tmp:PrintedBy        STRING(60)
thirty_days_temp     REAL
sixty_days_temp      REAL
ninty_days_temp      REAL
days_to_reorder_temp REAL
tmp:FirstModel       STRING(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(STOCK)
                       PROJECT(sto:Description)
                       PROJECT(sto:Part_Number)
                       PROJECT(sto:Quantity_On_Order)
                       PROJECT(sto:Quantity_Stock)
                     END
report               REPORT('Stock Forecast Report'),AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,792,7521,1521)
                         STRING('Site Location:'),AT(5000,156),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,156),USE(GLO:Select1),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(5885,365),USE(GLO:Select3),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Manufacturer:'),AT(5000,365),USE(?string22:2),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5896,990),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING('Suppress Zeros'),AT(5010,573),USE(?String27),TRN,FONT(,8,,)
                         STRING(@s40),AT(5896,573),USE(GLO:Select2),TRN,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(5010,781),USE(?String29),TRN,FONT(,8,,)
                         STRING(@s60),AT(5896,781),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Printed:'),AT(5010,990),USE(?String31),TRN,FONT(,8,,)
                         STRING('Page Number:'),AT(5010,1198),USE(?string59),TRN,FONT(,8,,)
                         STRING(@s3),AT(5896,1198),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Of'),AT(6208,1198),USE(?String40),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6417,1198,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,229),USE(?detailband)
                           STRING(@n8b),AT(5802,0),USE(sixty_days_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@n8b),AT(6406,0),USE(ninty_days_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@n8b),AT(6896,0),USE(days_to_reorder_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@s20),AT(3177,0),USE(tmp:FirstModel),TRN,LEFT,FONT(,7,,)
                           STRING(@s25),AT(208,0),USE(sto:Part_Number),TRN,FONT(,7,,)
                           STRING(@s30),AT(1563,0),USE(sto:Description),TRN,LEFT,FONT(,7,,)
                           STRING(@N8b),AT(4271,0),USE(sto:Quantity_Stock),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@N8b),AT(4740,0),USE(sto:Quantity_On_Order),TRN,RIGHT,FONT(,7,,)
                           STRING(@n8b),AT(5260,0),USE(thirty_days_temp),TRN,RIGHT,FONT(,7,,)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Lines:'),AT(208,104),USE(?String38),TRN
                           STRING(@s8),AT(1719,104),USE(count_temp),TRN,LEFT,FONT(,,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('STOCK FORECAST REPORT'),AT(4271,0,3177,260),USE(?string20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,885),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,885),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1042),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1198),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Part Number'),AT(208,2083),USE(?string44),TRN,FONT(,7,,FONT:bold)
                         STRING('Description'),AT(1563,2083),USE(?string45),TRN,FONT(,7,,FONT:bold)
                         STRING('In Stock'),AT(4323,2083),USE(?string24),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('On Order'),AT(4740,2083),USE(?string25),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('0-30 Days'),AT(5250,2083),USE(?String33),TRN,FONT(,7,,FONT:bold)
                         STRING('31-60 Days'),AT(5740,2083),USE(?String33:2),TRN,FONT(,7,,FONT:bold)
                         STRING('61-90 Days'),AT(6344,2083),USE(?String33:3),TRN,FONT(,7,,FONT:bold)
                         STRING('Reorder'),AT(7031,2135),USE(?String36),TRN,FONT(,7,,FONT:bold)
                         STRING('First Model'),AT(2917,2083),USE(?string45:2),TRN,FONT(,7,,FONT:bold)
                         STRING('Days To'),AT(7031,2031),USE(?String36:2),TRN,FONT(,7,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Stock_Forecast_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:STOCK.Open
  Relate:DEFAULTS.Open
  Access:USERS.UseFile
  Access:STOHIST.UseFile
  Access:STOMODEL.UseFile
  
  
  RecordsToProcess = RECORDS(STOCK)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(STOCK,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(sto:Location_Part_Description_Key)
      Process:View{Prop:Filter} = |
      'UPPER(sto:Location) = UPPER(GLO:Select1)'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        continue# = 1
        If glo:select3 <> ''
            If sto:manufacturer <> glo:select3
                continue# = 0
            End!If sto:manufacturer <> glo:select3
        End!If glo:select3 <> ''
        
        If continue# = 1
            save_stm_id = access:stomodel.savefile()
            access:stomodel.clearkey(stm:model_number_key)
            stm:ref_number   = sto:ref_number
            stm:manufacturer = sto:manufacturer
            set(stm:model_number_key,stm:model_number_key)
            loop
                if access:stomodel.next()
                   break
                end !if
                if stm:ref_number   <> sto:ref_number      |
                or stm:manufacturer <> sto:manufacturer      |
                    then break.  ! end if
                tmp:FirstModel  = stm:model_number
                Break
            end !loop
            access:stomodel.restorefile(save_stm_id)
        
            print# = 1
            thirty_days_temp = 0
            sixty_days_temp = 0
            ninty_days_temp = 0
            save_shi_id = access:stohist.savefile()
            access:stohist.clearkey(shi:ref_number_key)
            shi:ref_number = sto:ref_number
            set(shi:ref_number_key,shi:ref_number_key)
            loop
                if access:stohist.next()
                   break
                end !if
                if shi:ref_number <> sto:ref_number      |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                If shi:job_number = ''
                    Cycle
                End
                If shi:date => Today() - 30
                    If shi:transaction_type = 'DEC'
                        thirty_days_temp += shi:quantity
                    End!If shi:transaction_type = 'DEC'
                    If shi:transaction_type = 'REC'
                        thirty_days_temp -= shi:quantity
                    End!If shi:transaction_type = 'REC'
                End!If shi:date => Today() - 30
                If shi:date => Today() - 60 And shi:date < Today() - 30
                    If shi:transaction_type = 'DEC'
                        sixty_days_temp += shi:quantity
                    End!If shi:transaction_type = 'DEC'
                    If shi:transaction_type = 'REC'
                        sixty_days_temp -= shi:quantity
                    End!If shi:transaction_type = 'REC'
                End!If shi:date => Today() - 30
                If shi:date => Today() - 90 And shi:date < Today() - 60
                    If shi:transaction_type = 'DEC'
                        ninty_days_temp += shi:quantity
                    End!If shi:transaction_type = 'DEC'
                    If shi:transaction_type = 'REC'
                        ninty_days_temp -= shi:quantity
                    End!If shi:transaction_type = 'REC'
                End!If shi:date => Today() - 30
            end !loop
            access:stohist.restorefile(save_shi_id)
            If glo:select2 = 'YES'
                If thirty_days_temp = 0 And sixty_days_temp = 0 And ninty_days_temp = 0
                    print# = 0
                End!If used_temp = 0
            End!If glo:select5 = 'YES'
            If print# = 1
                days_to_reorder_temp = Int(sto:quantity_stock / ((thirty_days_temp + sixty_days_temp + ninty_days_temp) / 90))
                count_temp += 1
                tmp:RecordsCount += 1
                Print(rpt:detail)
            End!If print# = 1
        
        End!If continue# = 1
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(STOCK,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:STOCK.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'STOCK')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Stock_Forecast_Report'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Stock_Forecast_Report',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Stock_Forecast_Report',1)
    SolaceViewVars('save_stm_id',save_stm_id,'Stock_Forecast_Report',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Stock_Forecast_Report',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Stock_Forecast_Report',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Stock_Forecast_Report',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Stock_Forecast_Report',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Stock_Forecast_Report',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Stock_Forecast_Report',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Stock_Forecast_Report',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Stock_Forecast_Report',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Stock_Forecast_Report',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Stock_Forecast_Report',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Stock_Forecast_Report',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Stock_Forecast_Report',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Stock_Forecast_Report',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Stock_Forecast_Report',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Stock_Forecast_Report',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Stock_Forecast_Report',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Stock_Forecast_Report',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Stock_Forecast_Report',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Stock_Forecast_Report',1)
    SolaceViewVars('InitialPath',InitialPath,'Stock_Forecast_Report',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Stock_Forecast_Report',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Stock_Forecast_Report',1)
    SolaceViewVars('save_shi_id',save_shi_id,'Stock_Forecast_Report',1)
    SolaceViewVars('count_temp',count_temp,'Stock_Forecast_Report',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Stock_Forecast_Report',1)
    SolaceViewVars('thirty_days_temp',thirty_days_temp,'Stock_Forecast_Report',1)
    SolaceViewVars('sixty_days_temp',sixty_days_temp,'Stock_Forecast_Report',1)
    SolaceViewVars('ninty_days_temp',ninty_days_temp,'Stock_Forecast_Report',1)
    SolaceViewVars('days_to_reorder_temp',days_to_reorder_temp,'Stock_Forecast_Report',1)
    SolaceViewVars('tmp:FirstModel',tmp:FirstModel,'Stock_Forecast_Report',1)


BuildCtrlQueue      Routine







