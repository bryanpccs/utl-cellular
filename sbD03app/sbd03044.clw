

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03044.INC'),ONCE        !Local module procedure declarations
                     END








Job_Pending_Parts_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_ope_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
save_orp_id          USHORT,AUTO
save_WPR_id          USHORT,AUTO
save_par_id          USHORT,AUTO
ordpend_queue        QUEUE,PRE(ordque)
Ref_Number           REAL
Part_Number          STRING(30)
Description          STRING(30)
Model_Number         STRING(30)
number_of_jobs       REAL
quantity             REAL
quantity_on_order    REAL
                     END
count_temp           REAL
account_number_temp  STRING(15)
tmp:PrintedBy        STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(ORDPEND)
                     END
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1000),USE(?unnamed)
                         STRING('Account No:'),AT(5000,156),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s15),AT(5833,156),USE(account_number_temp),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(5000,365),USE(?string22:2),TRN,FONT(,8,,)
                         STRING(@s60),AT(5833,365),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Report Date:'),AT(5000,573),USE(?ReportDatePrompt),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5833,573),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,229),USE(?detailband)
                           STRING(@s25),AT(208,0),USE(ordque:Model_Number),TRN,FONT(,8,,)
                           STRING(@s25),AT(1823,0),USE(ordque:Part_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@s25),AT(3490,0),USE(ordque:Description),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s8),AT(6823,0),USE(ordque:quantity_on_order),TRN,RIGHT,FONT(,8,,)
                           STRING(@s8),AT(6042,0),USE(ordque:quantity),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s8),AT(5313,0),USE(ordque:number_of_jobs),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                         END
totals                   DETAIL,AT(,,,458),USE(?totals)
                           LINE,AT(208,52,7083,0),USE(?line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Lines'),AT(208,104),USE(?string26),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(1563,104),USE(count_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('JOB PENDING PARTS REPORT'),AT(3958,0,3490,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Model Number'),AT(208,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(1823,2083),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(3490,2083),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Jobs'),AT(5521,2083),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('No Required'),AT(6042,2083),USE(?string25:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('On Order'),AT(6875,2083),USE(?string25:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Job_Pending_Parts_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:ORDPEND.Open
  Relate:DEFAULTS.Open
  Access:JOBS.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:ORDPARTS.UseFile
  
  
  RecordsToProcess = RECORDS(ORDPEND)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(ORDPEND,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      RecordsToProcess = Records(ordpend)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        !Go through the ordpend file. If it has a job number, it is a job order.
        !See if the account number on that job is the account number selected.
        !There shouldn't be that many pending orders, so this is probably, the
        !quickest way to do it.
        
        !Build a queue of ordpend records, model number (from the job) and part number
        !so the report will be in model/part number order.
        save_ope_id = access:ordpend.savefile()
        set(ope:ref_number_key)
        loop
            if access:ordpend.next()
               break
            end !if
        
            If ope:job_number <> ''
                access:jobs.clearkey(job:ref_number_key)
                job:ref_number  = ope:job_number
                If access:jobs.fetch(job:ref_number_key) = Level:Benign
                    print# = 1
                    If glo:select1 <> ''
                        If job:account_number <> glo:select1
                            print# = 0
                        End!If job:account_number <> glo:select1
                    End!If glo:select1 <> ''
                    If print# = 1
                        Clear(glo:Q_ModelNumber)
                        glo:model_number_pointer    = job:model_number
                        Get(glo:Q_ModelNumber,glo:model_number_pointer)
                        IF error()
                            print# = 0
                        End!IF error()
                    End!If print# = 1
                    If print# = 1
                        quantity$ = 0
                        description" = 0
                        Case ope:part_type                                                              !Find the part on the job, and
                            Of 'JOB'                                                                    !get the quantity
                                setcursor(cursor:wait)
                                save_par_id = access:parts.savefile()
                                access:parts.clearkey(par:PendingRefNoKey)
                                par:ref_number         = job:ref_number
                                par:pending_ref_number = ope:ref_number
                                set(par:PendingRefNoKey,par:PendingRefNoKey)
                                loop
                                    if access:parts.next()
                                       break
                                    end !if
                                    if par:ref_number         <> job:ref_number      |
                                    or par:pending_ref_number <> ope:Ref_number      |
                                        then break.  ! end if
                                    If par:part_number  = ope:part_number
                                        quantity$   = par:quantity
                                        description"    = par:description
                                        Break
                                    End!If par:part_number  = ope:part_number
                                end !loop
                                access:parts.restorefile(save_par_id)
                                setcursor()
                            Of 'WAR'
                                setcursor(cursor:wait)
                                save_wpr_id = access:warparts.savefile()
                                access:warparts.clearkey(wpr:PendingRefNoKey)
                                wpr:ref_number         = job:ref_number
                                wpr:pending_ref_number = ope:ref_number
                                set(wpr:PendingRefNoKey,wpr:PendingRefNoKey)
                                loop
                                    if access:warparts.next()
                                       break
                                    end !if
                                    if wpr:ref_number         <> job:ref_number      |
                                    or wpr:pending_ref_number <> ope:Ref_number      |
                                        then break.  ! end if
                                    If wpr:part_number  = ope:part_number
                                        quantity$   = wpr:quantity
                                        description"    = wpr:description
                                        Break
                                    End!If wpr:part_number  = ope:part_number
                                end !loop
                                access:warparts.restorefile(save_wpr_id)
                                setcursor()
                        End!Case ope:part_type
                        quantity_on_order$ = 0
                        save_orp_id = access:ordparts.savefile()
                        access:ordparts.clearkey(orp:received_part_number_key)
                        orp:all_received = 'NO'
                        orp:part_number  = ope:part_number
                        set(orp:received_part_number_key,orp:received_part_number_key)
                        loop
                            if access:ordparts.next()
                               break
                            end !if
                            if orp:all_received <> 'NO'      |
                            or orp:part_number  <> ope:part_number      |
                                then break.  ! end if
                            quantity_on_order$  += orp:quantity
                        end !loop
                        access:ordparts.restorefile(save_orp_id)
        
                        ordque:part_number  = ope:part_number
                        ordque:model_number = job:model_number
                        Get(ordpend_queue,ordque:part_number,ordque:model_number)
                        If error()
                            ordque:ref_number   = ope:ref_number
                            ordque:part_number  = ope:part_number
                            ordque:model_Number = job:model_number
                            ordque:number_of_jobs   = 1
                            ordque:quantity     = quantity$
                            ordque:quantity_on_order   = quantity_on_order$
                            ordque:description  = description"
                            Add(ordpend_queue)
                        Else!If error
                            ordque:number_of_jobs   += 1
                            ordque:quantity += quantity$
                            ordque:quantity_on_order += quantity_on_order$
                            Put(ordpend_queue)
                        End!If error
        
                    End!If print# = 1
                End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
            End!If ope:job_number <> ''
        end !loop
        access:ordpend.restorefile(save_ope_id)
        
        RecordsToPRocess    = Records(ordpend_queue)
        !Print
        If Records(ordpend_queue)
        
            count_temp = 0
            Clear(ordpend_queue)
            Sort(ordpend_queue,ordque:Model_Number,ordque:Part_Number)
            Loop x# = 1 To Records(ordpend_queue)
                Get(ordpend_queue,x#)
                RecordsProcessed += 1
                Do DisplayProgress
                access:ordpend.clearkey(ope:ref_number_key)
                ope:ref_number = ordque:ref_number
                if access:ordpend.fetch(ope:ref_number_key) = Level:Benign
                    count_temp += 1
                    tmp:RecordsCount += 1
                    Print(rpt:detail)
                End!if access:ordpend.fetch(ope:ref_number_key)
            End!Loop x# = 1 To Records(ordpend_queue)
            Print(rpt:totals)
        
        End!If Records(ordpend_queue)
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(ORDPEND,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  If glo:select1 = ''
      account_number_temp  = 'ALL ACCOUNTS'
  Else!If glo:select1 = ''
      account_number_temp = glo:select1
  End!If glo:select1 = ''
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Job_Pending_Parts_Report'
  END
  report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Job_Pending_Parts_Report',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Job_Pending_Parts_Report',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Job_Pending_Parts_Report',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Job_Pending_Parts_Report',1)
    SolaceViewVars('save_ope_id',save_ope_id,'Job_Pending_Parts_Report',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Job_Pending_Parts_Report',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Job_Pending_Parts_Report',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Job_Pending_Parts_Report',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Job_Pending_Parts_Report',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Job_Pending_Parts_Report',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Job_Pending_Parts_Report',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Job_Pending_Parts_Report',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Job_Pending_Parts_Report',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Job_Pending_Parts_Report',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Job_Pending_Parts_Report',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Job_Pending_Parts_Report',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Job_Pending_Parts_Report',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Job_Pending_Parts_Report',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Job_Pending_Parts_Report',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Job_Pending_Parts_Report',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Job_Pending_Parts_Report',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Job_Pending_Parts_Report',1)
    SolaceViewVars('InitialPath',InitialPath,'Job_Pending_Parts_Report',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Job_Pending_Parts_Report',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Job_Pending_Parts_Report',1)
    SolaceViewVars('save_orp_id',save_orp_id,'Job_Pending_Parts_Report',1)
    SolaceViewVars('save_WPR_id',save_WPR_id,'Job_Pending_Parts_Report',1)
    SolaceViewVars('save_par_id',save_par_id,'Job_Pending_Parts_Report',1)
    SolaceViewVars('ordpend_queue:Ref_Number',ordpend_queue:Ref_Number,'Job_Pending_Parts_Report',1)
    SolaceViewVars('ordpend_queue:Part_Number',ordpend_queue:Part_Number,'Job_Pending_Parts_Report',1)
    SolaceViewVars('ordpend_queue:Description',ordpend_queue:Description,'Job_Pending_Parts_Report',1)
    SolaceViewVars('ordpend_queue:Model_Number',ordpend_queue:Model_Number,'Job_Pending_Parts_Report',1)
    SolaceViewVars('ordpend_queue:number_of_jobs',ordpend_queue:number_of_jobs,'Job_Pending_Parts_Report',1)
    SolaceViewVars('ordpend_queue:quantity',ordpend_queue:quantity,'Job_Pending_Parts_Report',1)
    SolaceViewVars('ordpend_queue:quantity_on_order',ordpend_queue:quantity_on_order,'Job_Pending_Parts_Report',1)
    SolaceViewVars('count_temp',count_temp,'Job_Pending_Parts_Report',1)
    SolaceViewVars('account_number_temp',account_number_temp,'Job_Pending_Parts_Report',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Job_Pending_Parts_Report',1)


BuildCtrlQueue      Routine







