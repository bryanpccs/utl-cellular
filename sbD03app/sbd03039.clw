

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03039.INC'),ONCE        !Local module procedure declarations
                     END








Stock_Check_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
save_shi_id          USHORT,AUTO
used_temp            LONG
save_stm_id          USHORT,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
line_temp            LONG
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
quantity_total_temp  LONG
save_sto_id          USHORT,AUTO
count_temp           LONG
stock_queue_temp     QUEUE,PRE(stoque)
ref_number           LONG
part_number          STRING(30)
description          STRING(30)
second_location      STRING(30)
                     END
tmp:PrintedBy        STRING(60)
code_temp            BYTE
option_temp          BYTE
bar_code_string_temp CSTRING(21)
bar_code_temp        CSTRING(21)
tmp:FirstModel       STRING(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(STOCK)
                       PROJECT(sto:Description)
                       PROJECT(sto:Part_Number)
                       PROJECT(sto:Purchase_Cost)
                       PROJECT(sto:Quantity_Stock)
                       PROJECT(sto:Second_Location)
                       PROJECT(sto:Shelf_Location)
                     END
Report               REPORT('Stock Value Report'),AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,781,7521,2094),USE(?unnamed:2)
                         STRING('Inc. Accessories:'),AT(4948,469),USE(?String22),TRN,FONT(,8,,)
                         STRING(@s40),AT(5885,469),USE(GLO:Select2),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(5885,625),USE(GLO:Select3),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Suppress Zeros:'),AT(4948,625),USE(?String22:3),TRN,FONT(,8,,)
                         STRING(@s40),AT(5885,156),USE(GLO:Select1),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(5885,313),USE(GLO:Select4),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Manufacturer:'),AT(4948,313),USE(?String22:4),TRN,FONT(,8,,)
                         STRING('Site Location:'),AT(4948,156),USE(?String22:2),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(4948,833),USE(?String27),TRN,FONT(,8,,)
                         STRING('Page Number:'),AT(4948,1146),USE(?String59),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,1146),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,1146,156,208),USE(?String38),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('?PP?'),AT(6354,1146,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s60),AT(5885,833),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Report Date:'),AT(4948,990),USE(?ReportDatePrompt),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING(@d6b),AT(5885,990),USE(ReportRunDate),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed)
DETAIL                   DETAIL,AT(,,,177),USE(?DetailBand)
                           STRING(@s15),AT(2344,0,1354,208),USE(bar_code_temp),LEFT,FONT('C128 High 12pt LJ3',10,,,CHARSET:ANSI)
                           STRING(@s12),AT(156,0),USE(sto:Shelf_Location),TRN,FONT(,7,,)
                           STRING(@s12),AT(833,0),USE(sto:Second_Location),TRN,FONT(,7,,)
                           STRING(@s15),AT(1510,0),USE(sto:Part_Number),TRN,FONT(,7,,)
                           STRING(@s20),AT(4531,0),USE(sto:Description),TRN,FONT(,7,,)
                           STRING(@s6),AT(6563,0),USE(sto:Quantity_Stock),TRN,RIGHT,FONT(,7,,)
                           BOX,AT(6979,0,156,150),USE(?Box1),COLOR(COLOR:Black)
                           STRING(@n10.2),AT(5625,0,521,104),USE(sto:Purchase_Cost),TRN,RIGHT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@s5),AT(6198,0),USE(used_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@s14),AT(3750,0),USE(tmp:FirstModel),TRN,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           LINE,AT(7188,104,208,0),USE(?Line2),COLOR(COLOR:Black)
                         END
Totals                   DETAIL,AT(,,,458),USE(?totals)
                           LINE,AT(208,52,7083,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Lines:'),AT(208,104),USE(?String26),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(6365,104),USE(quantity_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('Total Items:'),AT(5573,104),USE(?totalitems),TRN,FONT(,8,,FONT:bold)
                           STRING(@s9),AT(2083,104),USE(count_temp),TRN,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('STOCK CHECK REPORT'),AT(4271,0,3177,260),USE(?String20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?String19:2),TRN,FONT(,9,,)
                         STRING('2nd Loc'),AT(833,2083),USE(?String44),TRN,FONT(,7,,FONT:bold)
                         STRING('Part Number'),AT(1510,2083),USE(?String44:3),TRN,FONT(,7,,FONT:bold)
                         STRING('Description'),AT(4531,2083),USE(?String44:4),TRN,FONT(,7,,FONT:bold)
                         STRING('Shelf Loc'),AT(156,2083),USE(?String44:2),TRN,FONT(,7,,FONT:bold)
                         STRING('In Stock'),AT(6667,2083),USE(?Instock),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Count'),AT(7083,2083),USE(?String25:2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Purch. Cost'),AT(5583,2083),USE(?String44:7),TRN,FONT(,7,,FONT:bold)
                         STRING('Usage'),AT(6302,2083),USE(?Instock:2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Part Number'),AT(2344,2083),USE(?String44:6),TRN,FONT(,7,,FONT:bold)
                         STRING('1st Model'),AT(3750,2083),USE(?String44:5),TRN,FONT(,7,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Stock_Check_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:STOCK.Open
  Relate:DEFAULTS.Open
  Access:STOMODEL.UseFile
  
  
  RecordsToProcess = RECORDS(STOCK)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(STOCK,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      RecordsToProcess    = Records(Stock)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        !barcode bit
        If glo:select6 = 0
            Settarget(Report)
                Hide(?instock)
                Hide(?sto:quantity_stock)
                Hide(?totalitems)
                HIde(?quantity_total_temp)
            Settarget()
        
        End!If glo:select6 = 0
        
        code_temp = 3
        option_temp = 0
        quantity_total_temp = 0
        count_temp = 0
        
        Case glo:select5
            Of 1 !Location Order
                Sort(glo:Queue,glo:pointer)
                Loop x# = 1 To Records(glo:Queue)
                    Get(glo:Queue,x#)
        
                    save_sto_id = access:stock.savefile()
                    access:stock.clearkey(sto:SecondLockey)
                    sto:location       = glo:select1
                    sto:shelf_location = glo:pointer
                    set(sto:SecondLockey,sto:SecondLockey)
        
                    loop
                        if access:stock.next()
                           break
                        end !if
                        if sto:location       <> glo:select1
                            Break
                        End!if sto:location       <> glo:select1
                        If sto:shelf_location <> glo:pointer
                            Break
                        End!If sto:shelf_location <> glo:pointer
                        Include('RepStkCk.inc')
                    end !loop
                    access:stock.restorefile(save_sto_id)
                End!Loop x# = 1 To Records(glo:Queue)
        
            Of 2 !Description Order
        
                save_sto_id = access:stock.savefile()
                access:stock.clearkey(sto:description_key)
                sto:location       = glo:select1
                set(sto:description_key,sto:description_key)
                Loop
                    if access:stock.next()
                       break
                    end !if
                    if sto:location       <> glo:select1
                        Break
                    End!if sto:location       <> glo:select1
        
                    Sort(glo:Queue,glo:pointer)
                    glo:pointer  = sto:shelf_location
                    Get(glo:Queue,glo:pointer)
                    If Error()
                        Cycle
                    End!If Error()
                    Include('RepStkCk.inc')
                End!Loop
        
        End!Case glo:select5
        
        If count_temp <> 0
            Print(rpt:totals)
        End!If records(stock_queue_temp)
        
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(STOCK,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:STOCK.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Stock_Check_Report'
  END
  Report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Stock_Check_Report',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Stock_Check_Report',1)
    SolaceViewVars('save_shi_id',save_shi_id,'Stock_Check_Report',1)
    SolaceViewVars('used_temp',used_temp,'Stock_Check_Report',1)
    SolaceViewVars('save_stm_id',save_stm_id,'Stock_Check_Report',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Stock_Check_Report',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Stock_Check_Report',1)
    SolaceViewVars('line_temp',line_temp,'Stock_Check_Report',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Stock_Check_Report',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Stock_Check_Report',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Stock_Check_Report',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Stock_Check_Report',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Stock_Check_Report',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Stock_Check_Report',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Stock_Check_Report',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Stock_Check_Report',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Stock_Check_Report',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Stock_Check_Report',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Stock_Check_Report',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Stock_Check_Report',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Stock_Check_Report',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Stock_Check_Report',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Stock_Check_Report',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Stock_Check_Report',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Stock_Check_Report',1)
    SolaceViewVars('InitialPath',InitialPath,'Stock_Check_Report',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Stock_Check_Report',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Stock_Check_Report',1)
    SolaceViewVars('quantity_total_temp',quantity_total_temp,'Stock_Check_Report',1)
    SolaceViewVars('save_sto_id',save_sto_id,'Stock_Check_Report',1)
    SolaceViewVars('count_temp',count_temp,'Stock_Check_Report',1)
    SolaceViewVars('stock_queue_temp:ref_number',stock_queue_temp:ref_number,'Stock_Check_Report',1)
    SolaceViewVars('stock_queue_temp:part_number',stock_queue_temp:part_number,'Stock_Check_Report',1)
    SolaceViewVars('stock_queue_temp:description',stock_queue_temp:description,'Stock_Check_Report',1)
    SolaceViewVars('stock_queue_temp:second_location',stock_queue_temp:second_location,'Stock_Check_Report',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Stock_Check_Report',1)
    SolaceViewVars('code_temp',code_temp,'Stock_Check_Report',1)
    SolaceViewVars('option_temp',option_temp,'Stock_Check_Report',1)
    SolaceViewVars('bar_code_string_temp',bar_code_string_temp,'Stock_Check_Report',1)
    SolaceViewVars('bar_code_temp',bar_code_temp,'Stock_Check_Report',1)
    SolaceViewVars('tmp:FirstModel',tmp:FirstModel,'Stock_Check_Report',1)


BuildCtrlQueue      Routine







