

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_fwiz.inc'),ONCE

                     MAP
                       INCLUDE('SBD03075.INC'),ONCE        !Local module procedure declarations
                     END


RepairExchangeReportCriteria PROCEDURE                !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::5:TAGFLAG          BYTE(0)
DASBRW::5:TAGMOUSE         BYTE(0)
DASBRW::5:TAGDISPSTATUS    BYTE(0)
DASBRW::5:QUEUE           QUEUE
Pointer                       LIKE(GLO:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::12:TAGFLAG         BYTE(0)
DASBRW::12:TAGMOUSE        BYTE(0)
DASBRW::12:TAGDISPSTATUS   BYTE(0)
DASBRW::12:QUEUE          QUEUE
Pointer2                      LIKE(GLO:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:tag              STRING(1)
tmp:Manufacturer     STRING(30)
tmp:tag2             STRING(1)
tmp:StartDate        DATE
tmp:EndDate          DATE
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:Manufacturer
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:tag                LIKE(tmp:tag)                  !List box control field - type derived from local data
tmp:tag_Icon           LONG                           !Entry's icon ID
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW9::View:Browse    VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                       PROJECT(mod:Manufacturer)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:tag2               LIKE(tmp:tag2)                 !List box control field - type derived from local data
tmp:tag2_Icon          LONG                           !Entry's icon ID
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
mod:Manufacturer       LIKE(mod:Manufacturer)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB10::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
window               WINDOW('Repair/Exchange Report Criteria'),AT(,,299,279),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,292,244),USE(?Sheet1),BELOW,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Select the Trade Account(s) you wish to include'),AT(96,32),USE(?SelectTradeAccount),FONT(,,COLOR:Navy,,CHARSET:ANSI)
                           LIST,AT(96,48,196,168),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@60L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(134,98,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(138,118,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON('&Tag'),AT(96,220,56,16),USE(?DASTAG),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(152,220,56,16),USE(?DASTAGAll),LEFT,ICON('TagAll.gif')
                           BUTTON('&Untag All'),AT(208,220,56,16),USE(?DASUNTAGALL),LEFT,ICON('uNtAG.GIF')
                         END
                         TAB('Tab 2'),USE(?Tab2)
                           PROMPT('Select the Model Numbers you wish to include'),AT(96,32),USE(?Prompt3),FONT(,,COLOR:Navy,,CHARSET:ANSI)
                           PROMPT('Manufacturer'),AT(96,44),USE(?Manufacturer),FONT(,7,,)
                           COMBO(@s30),AT(96,52,124,10),USE(tmp:Manufacturer),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           LIST,AT(96,68,192,148),USE(?List:2),IMM,MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Model Number~@s30@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(158,126,50,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(162,150,70,13),USE(?DASSHOWTAG:2),HIDE
                           BUTTON('&Tag'),AT(94,220,56,16),USE(?DASTAG:2),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(152,220,56,16),USE(?DASTAGAll:2),LEFT,ICON('TagAll.gif')
                           BUTTON('&Untag All'),AT(208,220,56,16),USE(?DASUNTAGALL:2),LEFT,ICON('UnTag.gif')
                         END
                         TAB('Tab 3'),USE(?Tab3)
                           PROMPT('Select the (Booking) Date Range for the report.'),AT(96,32),USE(?Prompt5),FONT(,,COLOR:Navy,,CHARSET:ANSI)
                           PROMPT('Start Date'),AT(100,96),USE(?tmp:StartDate:Prompt)
                           ENTRY(@d6),AT(157,96,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(224,96,10,10),USE(?LookupStartDate),SKIP,ICON('Calenda2.ico')
                           PROMPT('End Date'),AT(100,124),USE(?tmp:EndDate:Prompt)
                           ENTRY(@d6),AT(157,124,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(224,124,10,10),USE(?LookupEndDate),SKIP,ICON('Calenda2.ico')
                         END
                       END
                       PROMPT('Repair/Exchange Report Wizard'),AT(8,8),USE(?Prompt1),FONT(,16,,FONT:bold)
                       IMAGE('WIZ.GIF'),AT(8,84),USE(?Image1)
                       PANEL,AT(4,252,292,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Back'),AT(8,256,56,16),USE(?VSBackButton),LEFT,ICON(ICON:VCRrewind)
                       BUTTON('&Next'),AT(64,256,56,16),USE(?VSNextButton),LEFT,ICON(ICON:VCRfastforward)
                       BUTTON('Cancel'),AT(236,256,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                       BUTTON('&Finish'),AT(180,256,56,16),USE(?Finish),LEFT,ICON('thumbs.gif')
                     END

Wizard7         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW9                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
FDCB10               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?SelectTradeAccount{prop:FontColor} = -1
    ?SelectTradeAccount{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Tab2{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Manufacturer{prop:FontColor} = -1
    ?Manufacturer{prop:Color} = 15066597
    If ?tmp:Manufacturer{prop:ReadOnly} = True
        ?tmp:Manufacturer{prop:FontColor} = 65793
        ?tmp:Manufacturer{prop:Color} = 15066597
    Elsif ?tmp:Manufacturer{prop:Req} = True
        ?tmp:Manufacturer{prop:FontColor} = 65793
        ?tmp:Manufacturer{prop:Color} = 8454143
    Else ! If ?tmp:Manufacturer{prop:Req} = True
        ?tmp:Manufacturer{prop:FontColor} = 65793
        ?tmp:Manufacturer{prop:Color} = 16777215
    End ! If ?tmp:Manufacturer{prop:Req} = True
    ?tmp:Manufacturer{prop:Trn} = 0
    ?tmp:Manufacturer{prop:FontStyle} = font:Bold
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Tab3{prop:Color} = 15066597
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?tmp:StartDate:Prompt{prop:FontColor} = -1
    ?tmp:StartDate:Prompt{prop:Color} = 15066597
    If ?tmp:StartDate{prop:ReadOnly} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 15066597
    Elsif ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 8454143
    Else ! If ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 16777215
    End ! If ?tmp:StartDate{prop:Req} = True
    ?tmp:StartDate{prop:Trn} = 0
    ?tmp:StartDate{prop:FontStyle} = font:Bold
    ?tmp:EndDate:Prompt{prop:FontColor} = -1
    ?tmp:EndDate:Prompt{prop:Color} = 15066597
    If ?tmp:EndDate{prop:ReadOnly} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 15066597
    Elsif ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 8454143
    Else ! If ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 16777215
    End ! If ?tmp:EndDate{prop:Req} = True
    ?tmp:EndDate{prop:Trn} = 0
    ?tmp:EndDate{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::5:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW4.UpdateBuffer
   glo:Queue.Pointer = sub:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = sub:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:tag = ''
  END
    Queue:Browse.tmp:tag = tmp:tag
  IF (tmp:tag = '*')
    Queue:Browse.tmp:tag_Icon = 2
  ELSE
    Queue:Browse.tmp:tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW4.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = sub:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW4.Reset
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::5:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::5:QUEUE = glo:Queue
    ADD(DASBRW::5:QUEUE)
  END
  FREE(glo:Queue)
  BRW4.Reset
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::5:QUEUE.Pointer = sub:Account_Number
     GET(DASBRW::5:QUEUE,DASBRW::5:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = sub:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASSHOWTAG Routine
   CASE DASBRW::5:TAGDISPSTATUS
   OF 0
      DASBRW::5:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::5:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::5:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW4.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::12:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW9.UpdateBuffer
   glo:Queue2.Pointer2 = mod:Model_Number
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = mod:Model_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tmp:tag2 = '*'
  ELSE
    DELETE(glo:Queue2)
    tmp:tag2 = ''
  END
    Queue:Browse:1.tmp:tag2 = tmp:tag2
  IF (tmp:tag2 = '*')
    Queue:Browse:1.tmp:tag2_Icon = 2
  ELSE
    Queue:Browse:1.tmp:tag2_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::12:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW9.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = mod:Model_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::12:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW9.Reset
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::12:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::12:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::12:QUEUE = glo:Queue2
    ADD(DASBRW::12:QUEUE)
  END
  FREE(glo:Queue2)
  BRW9.Reset
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::12:QUEUE.Pointer2 = mod:Model_Number
     GET(DASBRW::12:QUEUE,DASBRW::12:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = mod:Model_Number
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::12:DASSHOWTAG Routine
   CASE DASBRW::12:TAGDISPSTATUS
   OF 0
      DASBRW::12:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::12:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::12:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW9.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'RepairExchangeReportCriteria',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:tag',tmp:tag,'RepairExchangeReportCriteria',1)
    SolaceViewVars('tmp:Manufacturer',tmp:Manufacturer,'RepairExchangeReportCriteria',1)
    SolaceViewVars('tmp:tag2',tmp:tag2,'RepairExchangeReportCriteria',1)
    SolaceViewVars('tmp:StartDate',tmp:StartDate,'RepairExchangeReportCriteria',1)
    SolaceViewVars('tmp:EndDate',tmp:EndDate,'RepairExchangeReportCriteria',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SelectTradeAccount;  SolaceCtrlName = '?SelectTradeAccount';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Manufacturer;  SolaceCtrlName = '?Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Manufacturer;  SolaceCtrlName = '?tmp:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:2;  SolaceCtrlName = '?DASREVTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:2;  SolaceCtrlName = '?DASSHOWTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:2;  SolaceCtrlName = '?DASTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:2;  SolaceCtrlName = '?DASTAGAll:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:2;  SolaceCtrlName = '?DASUNTAGALL:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate:Prompt;  SolaceCtrlName = '?tmp:StartDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate;  SolaceCtrlName = '?tmp:StartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupStartDate;  SolaceCtrlName = '?LookupStartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate:Prompt;  SolaceCtrlName = '?tmp:EndDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate;  SolaceCtrlName = '?tmp:EndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupEndDate;  SolaceCtrlName = '?LookupEndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1;  SolaceCtrlName = '?Image1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VSBackButton;  SolaceCtrlName = '?VSBackButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VSNextButton;  SolaceCtrlName = '?VSNextButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Finish;  SolaceCtrlName = '?Finish';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('RepairExchangeReportCriteria')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'RepairExchangeReportCriteria')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?SelectTradeAccount
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANUFACT.Open
  SELF.FilesOpened = True
  tmp:StartDate   = Deformat('1/1/1990',@d6)
  tmp:EndDate     = Today()
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:SUBTRACC,SELF)
  BRW9.Init(?List:2,Queue:Browse:1.ViewPosition,BRW9::View:Browse,Queue:Browse:1,Relate:MODELNUM,SELF)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
    Wizard7.Init(?Sheet1, |                           ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Finish, |                       ! OK button
                     ?Close, |                        ! Cancel button
                     1, |                             ! Skip hidden tabs
                     1, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  BRW4.Q &= Queue:Browse
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,sub:Account_Number_Key)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,sub:Account_Number,1,BRW4)
  BIND('tmp:tag',tmp:tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW4.AddField(tmp:tag,BRW4.Q.tmp:tag)
  BRW4.AddField(sub:Account_Number,BRW4.Q.sub:Account_Number)
  BRW4.AddField(sub:Company_Name,BRW4.Q.sub:Company_Name)
  BRW4.AddField(sub:RecordNumber,BRW4.Q.sub:RecordNumber)
  BRW9.Q &= Queue:Browse:1
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,mod:Manufacturer_Key)
  BRW9.AddRange(mod:Manufacturer,tmp:Manufacturer)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,mod:Model_Number,1,BRW9)
  BIND('tmp:tag2',tmp:tag2)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW9.AddField(tmp:tag2,BRW9.Q.tmp:tag2)
  BRW9.AddField(mod:Model_Number,BRW9.Q.mod:Model_Number)
  BRW9.AddField(mod:Manufacturer,BRW9.Q.mod:Manufacturer)
  FDCB10.Init(tmp:Manufacturer,?tmp:Manufacturer,Queue:FileDropCombo.ViewPosition,FDCB10::View:FileDropCombo,Queue:FileDropCombo,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB10.Q &= Queue:FileDropCombo
  FDCB10.AddSortOrder(man:Manufacturer_Key)
  FDCB10.AddField(man:Manufacturer,FDCB10.Q.man:Manufacturer)
  FDCB10.AddField(man:RecordNumber,FDCB10.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB10.WindowComponent)
  FDCB10.DefaultFill = 0
  BRW4.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW4.AskProcedure = 0
      CLEAR(BRW4.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:MANUFACT.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'RepairExchangeReportCriteria',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard7.Validate()
        DISABLE(Wizard7.NextControl())
     ELSE
        ENABLE(Wizard7.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:Manufacturer
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
      BRW9.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?VSBackButton
      ThisWindow.Update
         Wizard7.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
         Wizard7.TakeAccepted()
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      RepairExchangeReport(tmp:StartDate,tmp:EndDate)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'RepairExchangeReportCriteria')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::5:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::12:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      DO DASBRW::12:DASUNTAGALL
      DO DASBRW::5:DASUNTAGALL
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW4.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:tag = ''
    ELSE
      tmp:tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag = '*')
    SELF.Q.tmp:tag_Icon = 2
  ELSE
    SELF.Q.tmp:tag_Icon = 1
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW4.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW4::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW4::RecordStatus=ReturnValue
  IF BRW4::RecordStatus NOT=Record:OK THEN RETURN BRW4::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::5:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW4::RecordStatus
  RETURN ReturnValue


BRW9.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = mod:Model_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:tag2 = ''
    ELSE
      tmp:tag2 = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag2 = '*')
    SELF.Q.tmp:tag2_Icon = 2
  ELSE
    SELF.Q.tmp:tag2_Icon = 1
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW9::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW9::RecordStatus=ReturnValue
  IF BRW9::RecordStatus NOT=Record:OK THEN RETURN BRW9::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = mod:Model_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::12:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW9::RecordStatus
  RETURN ReturnValue

Wizard7.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW4.Q &= NULL) ! Has Browse Object been initialized?
       BRW4.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW9.Q &= NULL) ! Has Browse Object been initialized?
       BRW9.ResetQueue(Reset:Queue)
    END

Wizard7.TakeBackEmbed PROCEDURE
   CODE

Wizard7.TakeNextEmbed PROCEDURE
   CODE

Wizard7.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
