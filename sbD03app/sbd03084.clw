

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03084.INC'),ONCE        !Local module procedure declarations
                     END


NetworkReportCriteria PROCEDURE                       !Generated from procedure template - Window

tmp:Colour           STRING(30)
tmp:StartDate        DATE
tmp:EndDate          DATE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Colour Report Criteria'),AT(,,231,103),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,224,68),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Colour'),AT(8,20),USE(?tmp:Colour:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,20,124,10),USE(tmp:Colour),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Colour'),TIP('Colour'),UPR
                           BUTTON,AT(212,20,10,10),USE(?LookupColour),SKIP,ICON('List3.ico')
                           PROMPT('Booked From'),AT(8,36),USE(?tmp:StartDate:Prompt)
                           ENTRY(@d6),AT(84,36,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Start Date'),TIP('Start Date'),REQ,UPR
                           BUTTON,AT(151,36,10,10),USE(?LookupStartDate),ICON('Calenda2.ico')
                           PROMPT('Booked To'),AT(8,52,68,12),USE(?tmp:EndDate:Prompt),LEFT
                           ENTRY(@d6),AT(84,52,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('End Date'),TIP('End Date'),REQ,UPR
                           BUTTON,AT(152,52,10,10),USE(?LookupEndDate),SKIP,ICON('Calenda2.ico')
                         END
                       END
                       PANEL,AT(4,76,224,24),USE(?ButtonPanel),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(112,80,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(168,80,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:Colour                Like(tmp:Colour)
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?tmp:Colour:Prompt{prop:FontColor} = -1
    ?tmp:Colour:Prompt{prop:Color} = 15066597
    If ?tmp:Colour{prop:ReadOnly} = True
        ?tmp:Colour{prop:FontColor} = 65793
        ?tmp:Colour{prop:Color} = 15066597
    Elsif ?tmp:Colour{prop:Req} = True
        ?tmp:Colour{prop:FontColor} = 65793
        ?tmp:Colour{prop:Color} = 8454143
    Else ! If ?tmp:Colour{prop:Req} = True
        ?tmp:Colour{prop:FontColor} = 65793
        ?tmp:Colour{prop:Color} = 16777215
    End ! If ?tmp:Colour{prop:Req} = True
    ?tmp:Colour{prop:Trn} = 0
    ?tmp:Colour{prop:FontStyle} = font:Bold
    ?tmp:StartDate:Prompt{prop:FontColor} = -1
    ?tmp:StartDate:Prompt{prop:Color} = 15066597
    If ?tmp:StartDate{prop:ReadOnly} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 15066597
    Elsif ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 8454143
    Else ! If ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 16777215
    End ! If ?tmp:StartDate{prop:Req} = True
    ?tmp:StartDate{prop:Trn} = 0
    ?tmp:StartDate{prop:FontStyle} = font:Bold
    ?tmp:EndDate:Prompt{prop:FontColor} = -1
    ?tmp:EndDate:Prompt{prop:Color} = 15066597
    If ?tmp:EndDate{prop:ReadOnly} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 15066597
    Elsif ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 8454143
    Else ! If ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 16777215
    End ! If ?tmp:EndDate{prop:Req} = True
    ?tmp:EndDate{prop:Trn} = 0
    ?tmp:EndDate{prop:FontStyle} = font:Bold
    ?ButtonPanel{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'NetworkReportCriteria',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:Colour',tmp:Colour,'NetworkReportCriteria',1)
    SolaceViewVars('tmp:StartDate',tmp:StartDate,'NetworkReportCriteria',1)
    SolaceViewVars('tmp:EndDate',tmp:EndDate,'NetworkReportCriteria',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Colour:Prompt;  SolaceCtrlName = '?tmp:Colour:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Colour;  SolaceCtrlName = '?tmp:Colour';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupColour;  SolaceCtrlName = '?LookupColour';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate:Prompt;  SolaceCtrlName = '?tmp:StartDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate;  SolaceCtrlName = '?tmp:StartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupStartDate;  SolaceCtrlName = '?LookupStartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate:Prompt;  SolaceCtrlName = '?tmp:EndDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate;  SolaceCtrlName = '?tmp:EndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupEndDate;  SolaceCtrlName = '?LookupEndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ButtonPanel;  SolaceCtrlName = '?ButtonPanel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('NetworkReportCriteria')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'NetworkReportCriteria')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:Colour:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:COLOUR.Open
  SELF.FilesOpened = True
  !Prime Dates
  tmp:StartDate   = Deformat('1/1/1990',@d6)
  tmp:EndDate     = Today()
  OPEN(window)
  SELF.Opened=True
  !Change Colour Name?
  If GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      0{prop:Text} = Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI')) & ' Report Criteria'
      ?tmp:Colour:prompt{prop:Text} = Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI'))
  End !GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  Do RecolourWindow
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  IF ?tmp:Colour{Prop:Tip} AND ~?LookupColour{Prop:Tip}
     ?LookupColour{Prop:Tip} = 'Select ' & ?tmp:Colour{Prop:Tip}
  END
  IF ?tmp:Colour{Prop:Msg} AND ~?LookupColour{Prop:Msg}
     ?LookupColour{Prop:Msg} = 'Select ' & ?tmp:Colour{Prop:Msg}
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COLOUR.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'NetworkReportCriteria',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickDefaultColour
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:Colour
      IF tmp:Colour OR ?tmp:Colour{Prop:Req}
        col:Colour = tmp:Colour
        !Save Lookup Field Incase Of error
        look:tmp:Colour        = tmp:Colour
        IF Access:COLOUR.TryFetch(col:Colour_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:Colour = col:Colour
          ELSE
            !Restore Lookup On Error
            tmp:Colour = look:tmp:Colour
            SELECT(?tmp:Colour)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupColour
      ThisWindow.Update
      col:Colour = tmp:Colour
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:Colour = col:Colour
          Select(?+1)
      ELSE
          Select(?tmp:Colour)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Colour)
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      NetworkReport(tmp:Colour,tmp:StartDate,tmp:EndDate)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'NetworkReportCriteria')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

