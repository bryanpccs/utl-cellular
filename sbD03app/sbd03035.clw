

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03035.INC'),ONCE        !Local module procedure declarations
                     END








Parts_Order PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
Default_Invoice_Company_Name_Temp STRING(30)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
first_page_temp      BYTE(1)
pos                  STRING(255)
Part_Queue           QUEUE,PRE()
Order_Number_Temp    REAL
Part_Number_Temp     STRING(30)
Description_Temp     STRING(30)
Quantity_Temp        LONG
Purchase_cost_temp   REAL
Sale_Cost_temp       REAL
                     END
Order_Temp           REAL
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Total_Quantity_Temp  LONG
total_cost_temp      REAL
total_cost_total_temp REAL
Total_Lines_Temp     REAL
user_name_temp       STRING(22)
no_temp              STRING('NO')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(ORDERS)
                       PROJECT(ord:Date)
                       PROJECT(ord:Order_Number)
                     END
Report               REPORT('Parts Order'),AT(396,4604,7521,4125),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,479,7521,4167),USE(?unnamed)
                         STRING(@D6b),AT(5844,760),USE(ReportRunDate),TRN,LEFT,FONT(,8,,)
                         STRING('Date Printed:'),AT(5083,760),USE(?RunPrompt),TRN,FONT(,8,,)
                         STRING(@T3),AT(6521,760),USE(ReportRunTime),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(156,323),USE(def:Address_Line1),TRN
                         STRING(@s30),AT(156,104),USE(def:User_Name),TRN,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,635),USE(def:Address_Line3),TRN
                         STRING(@s15),AT(156,802),USE(def:Postcode),TRN
                         STRING('Tel: '),AT(156,958),USE(?String15),TRN
                         STRING('Fax:'),AT(156,1094),USE(?String16),TRN
                         STRING(@s15),AT(573,1094),USE(def:Fax_Number),TRN
                         STRING('Email:'),AT(156,1250,521,156),USE(?String16:2),TRN
                         STRING(@s255),AT(573,1250),USE(def:EmailAddress),TRN
                         STRING(@s30),AT(156,1667),USE(sup:Company_Name),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1667),USE(def:OrderCompanyName),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1823),USE(Address_Line1_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1823),USE(def:OrderAddressLine1),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1979),USE(Address_Line2_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1979),USE(def:OrderAddressLine2),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2135),USE(Address_Line3_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,2135),USE(def:OrderAddressLine3),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2292),USE(Address_Line4_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,2292),USE(def:OrderPostcode),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(156,2448),USE(?String26),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,2448),USE(sup:Telephone_Number),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(4083,2448),USE(?String26:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4396,2448,990,188),USE(def:OrderTelephoneNumber),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(156,2604),USE(?String28),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,2604),USE(sup:Fax_Number),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(4083,2604),USE(?String28:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4396,2604,990,188),USE(def:OrderFaxNumber),TRN,FONT(,8,,)
                         STRING(@s15),AT(156,3385),USE(sup:Account_Number),TRN,FONT(,8,,)
                         STRING(@n12),AT(1615,3385),USE(ord:Order_Number),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(3083,3385),USE(ord:Date),TRN,FONT(,8,,)
                         STRING(@s20),AT(4531,3385,1406,156),USE(tmp:PrintedBy),TRN,FONT(,8,,)
                         STRING(@s30),AT(5990,3385,1563,208),USE(def:OrderTelephoneNumber,,?def:OrderTelephoneNumber:2),TRN,FONT(,8,,)
                         STRING('OFFICIAL PARTS ORDER'),AT(4917,83,2521,323),USE(?String19),TRN,FONT(,14,,FONT:bold)
                         STRING(@s15),AT(573,958),USE(def:Telephone_Number),TRN
                         STRING(@s30),AT(156,479),USE(def:Address_Line2),TRN
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,198),USE(?DetailBand)
                           STRING(@p<<<<<<<#p),AT(52,0),USE(GLO:Q_Quantity),TRN,RIGHT,FONT(,8,,)
                           STRING(@s30),AT(844,0),USE(GLO:Q_Part_Number),TRN,FONT(,8,,)
                           STRING(@s30),AT(3083,0),USE(GLO:Q_Description),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(5052,0),USE(GLO:Q_Purchase_Cost),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(6479,0),USE(total_cost_temp),TRN,RIGHT,FONT(,8,,)
                         END
detail1                  DETAIL,PAGEAFTER(-1),AT(,,,42),USE(?detail1),ABSOLUTE
                         END
Totals                   DETAIL,AT(396,9396),USE(?Totals),ABSOLUTE
                           STRING('Total Items: '),AT(396,83),USE(?String47),TRN,FONT(,10,,FONT:bold)
                           STRING('Total Lines: '),AT(396,323),USE(?String40),TRN,FONT(,,,FONT:bold)
                           STRING(@p<<<<<<<#p),AT(1323,323),USE(Total_Lines_Temp),TRN,RIGHT,FONT(,10,,FONT:bold)
                           STRING(@n14.2),AT(6156,156),USE(total_cost_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@p<<<<<<<#p),AT(1323,83),USE(Total_Quantity_Temp),TRN,RIGHT,FONT(,10,,FONT:bold)
                           STRING('Total Order Value:'),AT(4844,156),USE(?String41),TRN,FONT(,,,FONT:bold)
                         END
Continue                 DETAIL,AT(396,9396),USE(?Continue),ABSOLUTE
                           STRING('Number of Items On Page : 20'),AT(313,104),USE(?String60),TRN,FONT(,,,FONT:bold)
                           STRING('Continued Over -'),AT(5990,104),USE(?String60:2),TRN,FONT(,,,FONT:bold)
                         END
                       END
                       FOOTER,AT(396,10156,7521,1125)
                         TEXT,AT(83,83,7365,958),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,10802)
                         IMAGE,AT(0,0,7521,11156),USE(?Image1)
                         STRING('Order Number'),AT(1594,3177),USE(?String32),TRN,FONT(,8,,FONT:bold)
                         STRING('Order Date'),AT(3073,3177),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact'),AT(4531,3177),USE(?String45),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Number'),AT(5990,3177),USE(?String58),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number'),AT(125,3177),USE(?String30),TRN,FONT(,8,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4010,1458),USE(?String42),TRN,FONT(,9,,FONT:bold)
                         STRING('Quantity'),AT(156,3823),USE(?String31),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(3083,3823),USE(?String33),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(844,3823),USE(?String34),TRN,FONT(,8,,FONT:bold)
                         STRING('Item Cost'),AT(5313,3823),USE(?String35),TRN,FONT(,8,,FONT:bold)
                         STRING('Line Cost'),AT(6750,3823),USE(?String36),TRN,FONT(,8,,FONT:bold)
                         STRING('SUPPLIER ADDRESS'),AT(104,1458),USE(?String25),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Parts_Order')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('no_temp',no_temp)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:ORDERS.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:STANTEXT.Open
  Access:SUPPLIER.UseFile
  Access:USERS.UseFile
  Access:ORDPARTS.UseFile
  
  
  RecordsToProcess = RECORDS(ORDERS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(ORDERS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(ord:Supplier_Printed_Key)
      Process:View{Prop:Filter} = |
      'UPPER(ord:Printed) = UPPER(no_temp)'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        access:supplier.clearkey(sup:company_name_key)
        sup:company_name = ord:supplier
        if access:supplier.fetch(sup:company_name_key) = Level:Benign
            address_line1_temp  = sup:address_line1
            address_line2_temp  = sup:address_line2
            If sup:address_line3 = ''
                address_line3_temp = sup:postcode
                address_line4_temp = ''
            Else
                address_line3_temp  = sup:address_line3
                address_line4_temp  = sup:postcode
            End
        end!if access:supplier.fetch(sup:company_name_key) = Level:Benign
        
        multi_print# = 1
        If glo:select1 <> ''
            If ord:order_number <> glo:select1
                multi_print# = 0
            End
        End!If glo:select1 <> ''
        
        If multi_print# = 1
        
            Sort(glo:Q_PartsOrder,glo:q_supplier,glo:q_order_number,glo:q_part_number)
        
            count# = 0
            Loop x# = 1 To Records(glo:Q_PartsOrder)
                Get(glo:Q_PartsOrder,x#)
                If glo:q_supplier <> sup:company_name Then Cycle.
                total_cost_temp = glo:q_purchase_cost * glo:q_quantity
                total_cost_total_temp += total_cost_temp
                total_quantity_temp += glo:q_quantity
                total_lines_temp += 1
                If order_temp <> ord:order_number And first_page_temp <> 1
                    Print(rpt:detail1)
                End
                tmp:RecordsCount += 1
                count# += 1
                If count# > 20
                    Print(rpt:continue)
                    Print(rpt:detail1)
                    count# = 1
                End!If count# > 25
                Print(rpt:detail)
                order_temp = ord:order_number
                first_page_temp = 0
                print# = 1
            End!Loop x# = 1 To Records(glo:Q_PartsOrder)
        
            If print# = 1
                Print(rpt:totals)
                total_cost_total_temp = 0
                total_quantity_temp = 0
                pos = Position(ord:supplier_printed_key)
                ord:printed = 'YES'
                access:orders.update()
                Reset(ord:supplier_printed_key,pos)
            End!If print# = 1
        
        End!If print# = 1
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(ORDERS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:ORDERS.Close
    Relate:STANTEXT.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'ORDERS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  !Alternative Invoice Address
  Set(Defaults)
  access:Defaults.next()
  If def:use_invoice_address = 'YES' And def:use_for_order = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
  Else!If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:User_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
  End!If def:use_invoice_address = 'YES'
  
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','PARTS ORDER')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'PARTS ORDER'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'PARTS ORDER'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'PARTS ORDER'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Parts_Order'
  END
  Report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Parts_Order',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Default_Invoice_Company_Name_Temp',Default_Invoice_Company_Name_Temp,'Parts_Order',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Parts_Order',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Parts_Order',1)
    SolaceViewVars('Default_Invoice_Address_Line1_Temp',Default_Invoice_Address_Line1_Temp,'Parts_Order',1)
    SolaceViewVars('Default_Invoice_Address_Line2_Temp',Default_Invoice_Address_Line2_Temp,'Parts_Order',1)
    SolaceViewVars('Default_Invoice_Address_Line3_Temp',Default_Invoice_Address_Line3_Temp,'Parts_Order',1)
    SolaceViewVars('Default_Invoice_Postcode_Temp',Default_Invoice_Postcode_Temp,'Parts_Order',1)
    SolaceViewVars('Default_Invoice_Telephone_Number_Temp',Default_Invoice_Telephone_Number_Temp,'Parts_Order',1)
    SolaceViewVars('Default_Invoice_Fax_Number_Temp',Default_Invoice_Fax_Number_Temp,'Parts_Order',1)
    SolaceViewVars('Default_Invoice_VAT_Number_Temp',Default_Invoice_VAT_Number_Temp,'Parts_Order',1)
    SolaceViewVars('RejectRecord',RejectRecord,'Parts_Order',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Parts_Order',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Parts_Order',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Parts_Order',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Parts_Order',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Parts_Order',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Parts_Order',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Parts_Order',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Parts_Order',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Parts_Order',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Parts_Order',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Parts_Order',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Parts_Order',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Parts_Order',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Parts_Order',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Parts_Order',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Parts_Order',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Parts_Order',1)
    SolaceViewVars('InitialPath',InitialPath,'Parts_Order',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Parts_Order',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Parts_Order',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Parts_Order',1)
    SolaceViewVars('first_page_temp',first_page_temp,'Parts_Order',1)
    SolaceViewVars('pos',pos,'Parts_Order',1)
    SolaceViewVars('Part_Queue:Order_Number_Temp',Part_Queue:Order_Number_Temp,'Parts_Order',1)
    SolaceViewVars('Part_Queue:Part_Number_Temp',Part_Queue:Part_Number_Temp,'Parts_Order',1)
    SolaceViewVars('Part_Queue:Description_Temp',Part_Queue:Description_Temp,'Parts_Order',1)
    SolaceViewVars('Part_Queue:Quantity_Temp',Part_Queue:Quantity_Temp,'Parts_Order',1)
    SolaceViewVars('Part_Queue:Purchase_cost_temp',Part_Queue:Purchase_cost_temp,'Parts_Order',1)
    SolaceViewVars('Part_Queue:Sale_Cost_temp',Part_Queue:Sale_Cost_temp,'Parts_Order',1)
    SolaceViewVars('Order_Temp',Order_Temp,'Parts_Order',1)
    SolaceViewVars('Address_Line1_Temp',Address_Line1_Temp,'Parts_Order',1)
    SolaceViewVars('Address_Line2_Temp',Address_Line2_Temp,'Parts_Order',1)
    SolaceViewVars('Address_Line3_Temp',Address_Line3_Temp,'Parts_Order',1)
    SolaceViewVars('Address_Line4_Temp',Address_Line4_Temp,'Parts_Order',1)
    SolaceViewVars('Total_Quantity_Temp',Total_Quantity_Temp,'Parts_Order',1)
    SolaceViewVars('total_cost_temp',total_cost_temp,'Parts_Order',1)
    SolaceViewVars('total_cost_total_temp',total_cost_total_temp,'Parts_Order',1)
    SolaceViewVars('Total_Lines_Temp',Total_Lines_Temp,'Parts_Order',1)
    SolaceViewVars('user_name_temp',user_name_temp,'Parts_Order',1)
    SolaceViewVars('no_temp',no_temp,'Parts_Order',1)


BuildCtrlQueue      Routine







