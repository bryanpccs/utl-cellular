

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03003.INC'),ONCE        !Local module procedure declarations
                     END








Exchange_Exceptions_Report PROCEDURE(f_type)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
title_temp           STRING(26)
update_status_temp   STRING(3)
invoice_name_temp    STRING(40)
pos                  STRING(255)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
count_temp           REAL
customer_temp        STRING(30)
days_overdue_temp    REAL
date_range_temp      STRING(30)
tmp:PrintedBy        STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:ESN)
                       PROJECT(job:Exchange_Consignment_Number)
                       PROJECT(job:Exchange_Despatched)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Telephone_Collection)
                     END
Report               REPORT('Exchange Exceptions Report'),AT(365,2760,7521,7615),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,479,7521,1677),USE(?unnamed)
                         STRING(@s30),AT(156,83,3844,240),USE(def:User_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,365,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,521,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,677,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,833,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Courier:'),AT(4948,521),USE(?String35),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(156,1094,260,198),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1094),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Printed By:'),AT(4948,1146),USE(?String42),TRN,FONT(,8,,)
                         STRING(@s60),AT(5833,1146),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(5833,833),USE(date_range_temp),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Range:'),AT(4948,833),USE(?String36),TRN,FONT(,8,,)
                         STRING('Fax: '),AT(156,1250),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1250),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1406,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1406),USE(?String19:2),TRN,FONT(,9,,)
                         STRING(@s3),AT(5833,1458),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(5833,521),USE(invoice_name_temp),TRN,FONT(,8,,FONT:bold)
                         STRING('Printed:'),AT(4948,1302),USE(?String31),TRN,FONT(,8,,)
                         STRING(@D6),AT(5833,1302),USE(ReportRunDate),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@T3),AT(6510,1302),USE(ReportRunTime),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Account No.:'),AT(4948,677),USE(?String32),TRN,FONT(,8,,)
                         STRING(@s15),AT(5833,677,1000,240),USE(cou:Account_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Page Number'),AT(4948,1458,917,198),USE(?String34),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,198),USE(?DetailBand)
                           STRING(@s15),AT(729,0),USE(job:Exchange_Consignment_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@p<<<<<<<<#p),AT(0,0),USE(job:Ref_Number),TRN,RIGHT,FONT(,8,,)
                           STRING(@s20),AT(4063,0),USE(customer_temp),TRN,LEFT,FONT(,8,,)
                           STRING(@s15),AT(5417,0),USE(job:Telephone_Collection),TRN,LEFT,FONT(,8,,)
                           STRING(@n6b),AT(7031,0),USE(days_overdue_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@s20),AT(1719,0),USE(job:Model_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@s16),AT(3021,0),USE(job:ESN),TRN,LEFT,FONT(,8,,)
                           STRING(@d6b),AT(6385,0),USE(job:Exchange_Despatched),TRN,RIGHT,FONT(,8,,)
                         END
                         FOOTER,AT(0,0,,406),USE(?unnamed:4)
                           LINE,AT(365,83,6802,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(5760,156),USE(?String30),TRN,FONT(,10,,FONT:bold)
                           STRING(@n8),AT(6750,156),USE(count_temp),TRN,LEFT,FONT(,10,,FONT:bold)
                         END
                       END
                       FOOTER,AT(365,10333,7521,1156),USE(?unnamed:2),FONT('Arial',,,)
                         LINE,AT(365,52,6802,0),USE(?Line1:2),COLOR(COLOR:Black)
                         TEXT,AT(156,104,7198,958),USE(stt:Text),TRN,FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11198),USE(?Image1)
                         STRING(@s26),AT(4375,104),USE(title_temp),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING('Job No'),AT(156,2083),USE(?String29),TRN,FONT(,8,,FONT:bold)
                         STRING('Consign. No'),AT(698,2083),USE(?String33),TRN,FONT(,8,,FONT:bold)
                         STRING('Model No'),AT(1688,2083),USE(?String39),TRN,FONT(,8,,FONT:bold)
                         STRING('E.S.N. / I.M.E.I.'),AT(2990,2083),USE(?String43),TRN,FONT(,8,,FONT:bold)
                         STRING('Customer'),AT(4031,2083),USE(?String37),TRN,FONT(,8,,FONT:bold)
                         STRING('Tel. No.'),AT(5385,2083),USE(?String38),TRN,FONT(,8,,FONT:bold)
                         STRING('Desp.'),AT(6354,2083),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Days Over'),AT(6823,2083),USE(?String40),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Exchange_Exceptions_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  CASE MESSAGE(CPCS:AskPrvwDlgText,CPCS:AskPrvwDlgTitle,ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
    OF BUTTON:Yes
      PreviewReq = True
    OF BUTTON:No
      PreviewReq = False
    OF BUTTON:Cancel
       DO ProcedureReturn
  END
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:STAHEAD.Open
  Relate:STANTEXT.Open
  Access:COURIER.UseFile
  Access:AUDIT.UseFile
  Access:JOBSTAGE.UseFile
  Access:STATUS.UseFile
  Access:USERS.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(job:Ref_Number_Key)
      Process:View{Prop:Filter} = ''
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        print# = 0
        If job:exchange_courier = glo:select1
            If job:workshop <> 'YES' And job:exchange_despatched <> ''
                Weekend_Routine(JOB:Exchange_Despatched,2,end_date")
                If Today() > end_date"
                    print# = 1
                End!If Today() < end_date"
        
                If print# = 1
                    Days_Between_Routine(end_date",Today(),days")
                    days_overdue_temp = days"
                    count_temp += 1
                    If job:surname = ''
                        customer_temp = Clip(job:company_Name)
                    Else!If job:surname = ''
                        if job:title = '' and job:initial = ''
                            customer_temp = clip(job:surname)
                        elsif job:title = '' and job:initial <> ''
                            customer_temp = clip(job:surname) & ', ' & clip(job:initial)
                        elsif job:title <> '' and job:initial = ''
                            customer_temp = clip(job:surname) & ', ' & clip(job:title)
                        elsif job:title <> '' and job:initial <> ''
                            customer_temp = clip(job:surname) & ', ' & clip(job:title) & ' ' & clip(job:initial)
                        else
                            customer_temp = ''
                        end
                    End!If job:surname = ''
                    tmp:RecordsCount += 1
                    Print(rpt:detail)
        
                    If update_status_temp = 'YES'
                        GetStatus(950,1,'JOB') !exchange claim made
        
                        access:jobs.update()
                    End!If update_status_temp = 'YES'
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        Case f_type
                            Of 'E'
                                aud:notes = 'THIS JOB WAS INCLUDED IN THE EXCHANGE EXCEPTIONS REPORT'
                            Of 'C'
                                aud:notes = 'THIS JOB WAS INCLUDED IN THE EXCHANGE CLAIMS REPORT'
                        End!Case f_type
                        aud:ref_number    = job:ref_number
                        aud:date          = Today()
                        aud:time          = Clock()
                        access:users.clearkey(use:password_key)
                        use:password =glo:password
                        access:users.fetch(use:password_key)
                        aud:user = sub(use:forename,1,1) & sub(use:surname,1,1)
                        aud:action        = 'REPORT GENERATION'
                        if access:audit.insert()
                            access:audit.cancelautoinc()
                        end
                    end!if access:audit.primerecord() = level:benign
                End!If print# = 1
            End!If job:workshop <> 'YES' And job:exchange_despatch <> ''
        End!If job:exchange_courier = glo:select1
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:STAHEAD.Close
    Relate:STANTEXT.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  If f_type = 'C'
    Case MessageEx('Do you wish to update the Status of all the jobs in this report to:<13,10><13,10>''950 EXCHANGE CLAIM MADE''','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
        Of 1 ! &Yes Button
            update_status_temp = 'YES'
        Of 2 ! &No Button
            update_status_temp = 'NO'
    End!Case MessageEx
  End!If f
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  access:courier.clearkey(cou:courier_key)
  cou:courier = glo:select1
  if access:courier.fetch(cou:courier_key) = Level:Benign
      invoice_name_temp   = cou:courier
      address_line1_temp  = cou:address_line1
      address_line2_temp  = cou:address_line2
      If sup:address_line3 = ''
          address_line3_temp = cou:postcode
          address_line4_temp = ''
      Else
          address_line3_temp  = cou:address_line3
          address_line4_temp  = cou:postcode
      End
  end
  count_temp = 0
  
  access:users.clearkey(use:password_key)
  use:password =glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = Clip(use:forename) & ' ' & Clip(use:surname)
  
  date_range_temp = Format(glo:select2,@d6) & ' - ' & Format(glo:select3,@d6)
  Case f_type
      Of 'E'
          title_temp = 'EXCHANGE EXCEPTIONS REPORT'
          access:stantext.clearkey(stt:description_key)
          stt:description = 'EXCHANGE EXCEPTIONS REPORT'
          access:stantext.fetch(stt:description_key)
      Of 'C'
          title_temp = 'EXCHANGE CLAIMS REPORT'
          access:stantext.clearkey(stt:description_key)
          stt:description = 'EXCHANGE CLAIMS REPORT'
          access:stantext.fetch(stt:description_key)
  End!Case
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Exchange_Exceptions_Report'
  END
  Report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Exchange_Exceptions_Report',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Exchange_Exceptions_Report',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Exchange_Exceptions_Report',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Exchange_Exceptions_Report',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Exchange_Exceptions_Report',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Exchange_Exceptions_Report',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Exchange_Exceptions_Report',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Exchange_Exceptions_Report',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Exchange_Exceptions_Report',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Exchange_Exceptions_Report',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Exchange_Exceptions_Report',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Exchange_Exceptions_Report',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Exchange_Exceptions_Report',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Exchange_Exceptions_Report',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Exchange_Exceptions_Report',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Exchange_Exceptions_Report',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Exchange_Exceptions_Report',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Exchange_Exceptions_Report',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Exchange_Exceptions_Report',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Exchange_Exceptions_Report',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Exchange_Exceptions_Report',1)
    SolaceViewVars('InitialPath',InitialPath,'Exchange_Exceptions_Report',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Exchange_Exceptions_Report',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Exchange_Exceptions_Report',1)
    SolaceViewVars('title_temp',title_temp,'Exchange_Exceptions_Report',1)
    SolaceViewVars('update_status_temp',update_status_temp,'Exchange_Exceptions_Report',1)
    SolaceViewVars('invoice_name_temp',invoice_name_temp,'Exchange_Exceptions_Report',1)
    SolaceViewVars('pos',pos,'Exchange_Exceptions_Report',1)
    SolaceViewVars('Address_Line1_Temp',Address_Line1_Temp,'Exchange_Exceptions_Report',1)
    SolaceViewVars('Address_Line2_Temp',Address_Line2_Temp,'Exchange_Exceptions_Report',1)
    SolaceViewVars('Address_Line3_Temp',Address_Line3_Temp,'Exchange_Exceptions_Report',1)
    SolaceViewVars('Address_Line4_Temp',Address_Line4_Temp,'Exchange_Exceptions_Report',1)
    SolaceViewVars('count_temp',count_temp,'Exchange_Exceptions_Report',1)
    SolaceViewVars('customer_temp',customer_temp,'Exchange_Exceptions_Report',1)
    SolaceViewVars('days_overdue_temp',days_overdue_temp,'Exchange_Exceptions_Report',1)
    SolaceViewVars('date_range_temp',date_range_temp,'Exchange_Exceptions_Report',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Exchange_Exceptions_Report',1)


BuildCtrlQueue      Routine







