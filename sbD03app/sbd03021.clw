

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('SBD03021.INC'),ONCE        !Local module procedure declarations
                     END


Exchange_Units_In_Channel PROCEDURE                   !Generated from procedure template - Report

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
Progress:Thermometer BYTE
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:RecordsCount     REAL
Exchange_Queue       QUEUE,PRE(excque)
model_number         STRING(30)
request_received     REAL
same_day             REAL
other_day            REAL
received_in_workshop REAL
                     END
In_Repair_Queue      QUEUE,PRE(repque)
model_number         STRING(30)
in_stock             REAL
in_workshop          REAL
awaiting_parts       REAL
RTM                  REAL
                     END
first_page_temp      BYTE(1)
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_xch_id          USHORT,AUTO
endofreport          BYTE,AUTO
tmp:printer          STRING(255)
FilesOpened          BYTE
tmp:PrintedBy        STRING(60)
account_number_temp  STRING(60)
model_number_temp    STRING(30)
requests_received_temp STRING(6)
same_day_temp        STRING(6)
other_day_temp       STRING(6)
Failed_Exchange_Temp STRING(6)
Despatched_Temp      STRING(6)
In_Workshop_Temp     STRING(6)
Awaiting_Arrival_Temp STRING(6)
In_Stock_Temp        STRING(6)
Units_In_Workshop_Temp STRING(6)
Received_In_Workshop_Temp STRING(6)
Awaiting_Parts_Temp  STRING(6)
RTM_Temp             STRING(6)
In_Repair_Total      STRING(6)
In_Channel_Temp      STRING(6)
Removed_From_Stock_Temp STRING(6)
In_Scheme_Temp       STRING(6)
Manufacturer_Temp    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(JOBS)
                       PROJECT(job:date_booked)
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

! moving bar window
rejectrecord2         long
recordstoprocess2     long,auto
recordsprocessed2     long,auto
recordspercycle2      long,auto
recordsthiscycle2     long,auto
percentprogress2      byte
recordstatus2         byte,auto

progress:thermometer2 byte
progresswindow2 WINDOW('Printing...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(Progress:Thermometer2),AT(15,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,141,10),USE(?progress:userstring2),CENTER
       STRING(''),AT(0,30,141,10),USE(?progress:pcttext2),CENTER
     END
report               REPORT('Exchange Units In Channel Report'),AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,750,7521,1771),USE(?unnamed)
                         STRING('Trade Account:'),AT(4948,208),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s60),AT(5729,208),USE(account_number_temp),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Date Range:'),AT(4948,469),USE(?String23),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5729,469),USE(GLO:Select1),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(6406,469),USE(GLO:Select2),TRN,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(4948,729),USE(?String26),TRN,FONT(,8,,)
                         STRING(@s60),AT(5729,729),USE(tmp:PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Printed:'),AT(4948,990),USE(?String27),TRN,FONT(,8,,)
                         STRING('Page Number:'),AT(4948,1250),USE(?string59),TRN,FONT(,8,,)
                         STRING(@s3),AT(5729,1250),USE(ReportPageNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('XX/XX/XXXX'),AT(5729,990),USE(?ReportDateStamp),TRN,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,7208),USE(?detailband)
                           STRING('MODEL NUMBER:'),AT(677,0),USE(?model_string),TRN,FONT(,9,,FONT:bold)
                           STRING(@s30),AT(1875,0),USE(model_number_temp),TRN,FONT(,9,,FONT:bold)
                           STRING('EXCHANGE TRANSACTION'),AT(677,365),USE(?String28),TRN,FONT(,9,,FONT:bold)
                           STRING(@s6),AT(5906,677),USE(requests_received_temp),TRN,RIGHT
                           STRING('IN TRANSIT'),AT(698,2135),USE(?String28:2),TRN,FONT(,9,,FONT:bold)
                           STRING(@s6),AT(5906,2396),USE(Despatched_Temp),TRN,RIGHT
                           STRING('IN REPAIR'),AT(698,3750),USE(?String28:3),TRN,FONT(,9,,FONT:bold)
                           STRING(@s6),AT(5906,3958),USE(In_Stock_Temp),TRN,RIGHT
                           STRING('GENERAL DETAILS'),AT(677,5729),USE(?String28:4),TRN,FONT(,9,,FONT:bold)
                           STRING('Exchange Units In Channel:'),AT(1875,5990),USE(?String41),TRN,FONT(,,,FONT:bold)
                           STRING(@s6),AT(5906,5990),USE(In_Channel_Temp),TRN,RIGHT,FONT(,,,FONT:bold)
                           LINE,AT(5938,6198,500,0),USE(?Line1:5),COLOR(COLOR:Black)
                           STRING('Exchange Units In Stock:'),AT(1875,3958),USE(?String36),TRN
                           STRING('Exchange Units In Workshop:'),AT(1875,4271),USE(?String37),TRN
                           STRING(@s6),AT(5906,4271),USE(In_Workshop_Temp),TRN,RIGHT
                           STRING('Exchange Units Awaiting Parts:'),AT(1875,4583),USE(?String38),TRN
                           STRING(@s6),AT(5906,4583),USE(Awaiting_Parts_Temp),TRN,RIGHT
                           STRING('Exchange Units R.T.M.:'),AT(1875,4896),USE(?String39),TRN
                           STRING(@s6),AT(5906,4896),USE(RTM_Temp),TRN,RIGHT
                           STRING('Total:'),AT(1875,5208),USE(?String45),TRN,FONT(,10,,FONT:bold)
                           STRING(@s6),AT(5906,5208),USE(In_Repair_Total),TRN,RIGHT,FONT(,,,FONT:bold)
                           STRING('Exchange Units Despatched:'),AT(1875,2396),USE(?String33),TRN
                           STRING('Exchange Units Received Into Workshop:'),AT(1875,2708),USE(?String34),TRN
                           STRING(@s6),AT(5906,2708),USE(Received_In_Workshop_Temp),TRN,RIGHT
                           STRING('Exchange Units Awaiting Arrival:'),AT(1875,3021),USE(?String35),TRN,FONT(,,,FONT:bold)
                           STRING(@s6),AT(5906,3021),USE(Awaiting_Arrival_Temp),TRN,RIGHT,FONT(,,,FONT:bold)
                           STRING('Exchange Requests Received:'),AT(1875,677),USE(?String29),TRN
                           STRING('Exchange Units Despatched (Same Day):'),AT(1875,990),USE(?String30),TRN
                           STRING(@s6),AT(5906,990),USE(same_day_temp),TRN,RIGHT
                           STRING('Exchange Units Despatched (Not Same Day):'),AT(1875,1354),USE(?String31),TRN
                           STRING(@s6),AT(5906,1354),USE(other_day_temp),TRN,RIGHT
                           LINE,AT(5938,1667,500,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Failed Exchange Transactions:'),AT(1875,1719),USE(?String32),TRN,FONT(,,,FONT:bold)
                           STRING(@s6),AT(5906,1719),USE(Failed_Exchange_Temp),TRN,RIGHT,FONT(,,,FONT:bold)
                           LINE,AT(5938,2969,500,0),USE(?Line1:2),COLOR(COLOR:Black)
                           LINE,AT(5938,5156,500,0),USE(?Line1:3),COLOR(COLOR:Black)
                           LINE,AT(5938,5938,500,0),USE(?Line1:4),COLOR(COLOR:Black)
                         END
new_page                 DETAIL,PAGEBEFORE(-1),AT(,,,52),USE(?new_page)
                         END
first_page_title         DETAIL,AT(,,,656),USE(?first_page_title)
                           STRING('SELECTED MODEL NUMBERS'),AT(677,104),USE(?String28:5),TRN,FONT(,9,,FONT:bold)
                         END
first_page_detail        DETAIL,AT(,,,167),USE(?first_page_detail)
                           STRING(@s20),AT(1875,0),USE(GLO:Pointer),TRN,FONT(,8,,)
                           STRING(@s30),AT(3385,0),USE(Manufacturer_Temp),TRN,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('EXCHANGE UNITS IN CHANNEL'),AT(4271,0,3177,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?Image1)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,885),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s15),AT(521,885),USE(def:Telephone_Number),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1042),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s15),AT(521,1042),USE(def:Fax_Number),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1198,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1198),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Description'),AT(698,2083),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Units In Channel'),AT(5906,2083),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 22
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Progress:UserString{prop:FontColor} = -1
    ?Progress:UserString{prop:Color} = 15066597
    ?Progress:PctText{prop:FontColor} = -1
    ?Progress:PctText{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
  recordsprocessed2 += 1
  recordsthiscycle2 += 1
  if percentprogress2 < 100
    percentprogress2 = (recordsprocessed2 / recordstoprocess2)*100
    if percentprogress2 > 100
      percentprogress2 = 100
    end
    if percentprogress2 <> progress:thermometer2 then
      progress:thermometer2 = percentprogress2
      ?progress:pcttext2{prop:text} = format(percentprogress2,@n3) & '% Completed'
      display()
    end
  end


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Exchange_Units_In_Channel',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Exchange_Units_In_Channel',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Exchange_Units_In_Channel',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Exchange_Units_In_Channel',1)
    SolaceViewVars('tmp:RecordsCount',tmp:RecordsCount,'Exchange_Units_In_Channel',1)
    SolaceViewVars('Exchange_Queue:model_number',Exchange_Queue:model_number,'Exchange_Units_In_Channel',1)
    SolaceViewVars('Exchange_Queue:request_received',Exchange_Queue:request_received,'Exchange_Units_In_Channel',1)
    SolaceViewVars('Exchange_Queue:same_day',Exchange_Queue:same_day,'Exchange_Units_In_Channel',1)
    SolaceViewVars('Exchange_Queue:other_day',Exchange_Queue:other_day,'Exchange_Units_In_Channel',1)
    SolaceViewVars('Exchange_Queue:received_in_workshop',Exchange_Queue:received_in_workshop,'Exchange_Units_In_Channel',1)
    SolaceViewVars('In_Repair_Queue:model_number',In_Repair_Queue:model_number,'Exchange_Units_In_Channel',1)
    SolaceViewVars('In_Repair_Queue:in_stock',In_Repair_Queue:in_stock,'Exchange_Units_In_Channel',1)
    SolaceViewVars('In_Repair_Queue:in_workshop',In_Repair_Queue:in_workshop,'Exchange_Units_In_Channel',1)
    SolaceViewVars('In_Repair_Queue:awaiting_parts',In_Repair_Queue:awaiting_parts,'Exchange_Units_In_Channel',1)
    SolaceViewVars('In_Repair_Queue:RTM',In_Repair_Queue:RTM,'Exchange_Units_In_Channel',1)
    SolaceViewVars('first_page_temp',first_page_temp,'Exchange_Units_In_Channel',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Exchange_Units_In_Channel',1)
    SolaceViewVars('save_par_id',save_par_id,'Exchange_Units_In_Channel',1)
    SolaceViewVars('save_xch_id',save_xch_id,'Exchange_Units_In_Channel',1)
    SolaceViewVars('endofreport',endofreport,'Exchange_Units_In_Channel',1)
    SolaceViewVars('tmp:printer',tmp:printer,'Exchange_Units_In_Channel',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Exchange_Units_In_Channel',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Exchange_Units_In_Channel',1)
    SolaceViewVars('account_number_temp',account_number_temp,'Exchange_Units_In_Channel',1)
    SolaceViewVars('model_number_temp',model_number_temp,'Exchange_Units_In_Channel',1)
    SolaceViewVars('requests_received_temp',requests_received_temp,'Exchange_Units_In_Channel',1)
    SolaceViewVars('same_day_temp',same_day_temp,'Exchange_Units_In_Channel',1)
    SolaceViewVars('other_day_temp',other_day_temp,'Exchange_Units_In_Channel',1)
    SolaceViewVars('Failed_Exchange_Temp',Failed_Exchange_Temp,'Exchange_Units_In_Channel',1)
    SolaceViewVars('Despatched_Temp',Despatched_Temp,'Exchange_Units_In_Channel',1)
    SolaceViewVars('In_Workshop_Temp',In_Workshop_Temp,'Exchange_Units_In_Channel',1)
    SolaceViewVars('Awaiting_Arrival_Temp',Awaiting_Arrival_Temp,'Exchange_Units_In_Channel',1)
    SolaceViewVars('In_Stock_Temp',In_Stock_Temp,'Exchange_Units_In_Channel',1)
    SolaceViewVars('Units_In_Workshop_Temp',Units_In_Workshop_Temp,'Exchange_Units_In_Channel',1)
    SolaceViewVars('Received_In_Workshop_Temp',Received_In_Workshop_Temp,'Exchange_Units_In_Channel',1)
    SolaceViewVars('Awaiting_Parts_Temp',Awaiting_Parts_Temp,'Exchange_Units_In_Channel',1)
    SolaceViewVars('RTM_Temp',RTM_Temp,'Exchange_Units_In_Channel',1)
    SolaceViewVars('In_Repair_Total',In_Repair_Total,'Exchange_Units_In_Channel',1)
    SolaceViewVars('In_Channel_Temp',In_Channel_Temp,'Exchange_Units_In_Channel',1)
    SolaceViewVars('Removed_From_Stock_Temp',Removed_From_Stock_Temp,'Exchange_Units_In_Channel',1)
    SolaceViewVars('In_Scheme_Temp',In_Scheme_Temp,'Exchange_Units_In_Channel',1)
    SolaceViewVars('Manufacturer_Temp',Manufacturer_Temp,'Exchange_Units_In_Channel',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Progress:Thermometer;  SolaceCtrlName = '?Progress:Thermometer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:UserString;  SolaceCtrlName = '?Progress:UserString';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:PctText;  SolaceCtrlName = '?Progress:PctText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:Cancel;  SolaceCtrlName = '?Progress:Cancel';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 34
  PARENT.Ask


ThisWindow.AskPreview PROCEDURE

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(AskPreview, ())
  recordspercycle2     = 25
  recordsprocessed2    = 0
  percentprogress2     = 0
  setcursor(cursor:wait)
  open(progresswindow2)
  Display()
  progress:thermometer2    = 0
  ?progress:pcttext2{prop:text} = '0% Completed'
  
  recordstoprocess2    = Records(glo:Queue) + Records(exchange)
  
  setcursor(cursor:wait)
  
  in_stock_temp = 0
  in_workshop_temp = 0
  Awaiting_Parts_Temp = 0
  RTM_Temp = 0
  setcursor(cursor:wait)
  
  save_xch_id = access:exchange.savefile()
  set(xch:ref_number_key)
  loop
      if access:exchange.next()
         break
      end !if
      Do GetNextRecord2
      yldcnt# += 1
      if yldcnt# > 25
         yield() ; yldcnt# = 0
      end !if
      Sort(glo:Queue,glo:pointer)
      glo:pointer = xch:model_number
      Get(glo:Queue,glo:pointer)
      If error()
          Cycle
      End
      If glo:select4 = 'YES'                                                               !If One Model Per Page. Build up the
          Clear(in_repair_queue)
          Sort(in_repair_queue,repque:model_number)                                           !In Repair Queue.
          repque:model_number = xch:model_number
          Get(in_repair_queue,repque:model_number)
          If Error()                                                                          !Check the model number of the exchange
              repque:model_number = xch:model_number                                          !unit. Add if not found, put if found.
              If xch:available = 'AVL'
                  repque:in_stock = 1
              Else
                  repque:in_stock = 0
              End
              Add(in_repair_queue)
          Else
              If xch:available = 'AVL'
                  repque:in_stock += 1
                  Put(in_repair_queue)
              End
          End!If Error()
      Else!If glo:select4 = 'YES'
          If xch:available = 'AVL'
              in_stock_temp += 1
          End!If xch:job_number <> 0
      End!If glo:select4 = 'YES'
  
  
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number = xch:job_number
      if access:jobs.fetch(job:ref_number_key) = Level:Benign
          If job:date_completed = ''
  !Model Dependent
              workshop# = 0
              If job:workshop = 'YES'
                  workshop# = 1
              End!If job:workshop = 'YES'
              setcursor(cursor:wait)
              rtm# = 0
              If job:third_party_site <> '' And job:workshop <> 'YES'
                  rtm# = 1
              Else!If job:third_party_site <> '' And job:workshop <> 'YES'
                  found# = 0
                  save_wpr_id = access:warparts.savefile()
                  access:warparts.clearkey(wpr:part_number_key)
                  wpr:ref_number  = job:ref_number
                  set(wpr:part_number_key,wpr:part_number_key)
                  loop
                      if access:warparts.next()
                         break
                      end !if
                      if wpr:ref_number  <> job:ref_number      |
                          then break.  ! end if
                      yldcnt# += 1
                      if yldcnt# > 25
                         yield() ; yldcnt# = 0
                      end !if
                      If wpr:date_ordered <> '' and wpr:date_received = ''
                          found# = 1
                      Else!If wpr:date_ordered <> and wpr:date_received = 'YES'
                          If wpr:pending_ref_number <> '' and wpr:date_received = ''
                              found# = 1
                          End!If wpr:pending_ref_number <> ''
                      End!If wpr:date_ordered <> and wpr:date_received = 'YES'
                  end !loop
                  access:warparts.restorefile(save_wpr_id)
                  setcursor()
  
                  If found# = 0
                      setcursor(cursor:wait)
                      save_par_id = access:parts.savefile()
                      access:parts.clearkey(par:part_number_key)
                      par:ref_number  = job:ref_number
                      set(par:part_number_key,par:part_number_key)
                      loop
                          if access:parts.next()
                             break
                          end !if
                          if par:ref_number  <> job:ref_number      |
                              then break.  ! end if
                          yldcnt# += 1
                          if yldcnt# > 25
                             yield() ; yldcnt# = 0
                          end !if
                          If par:date_ordered <> '' and par:date_received = ''
                              found# = 1
                          Else!If wpr:date_ordered <> and wpr:date_received = 'YES'
                              If par:pending_ref_number <> '' and par:date_received = ''
                                  found# = 1
                              End!If wpr:pending_ref_number <> ''
                          End!If wpr:date_ordered <> and wpr:date_received = 'YES'
                      end !loop
                      access:parts.restorefile(save_par_id)
                      setcursor()
                  End!If found# = 0
  
              End!If job:third_party_site <> '' And job:workshop <> 'YES'
  
              If glo:select4 = 'YES'
                  Clear(in_repair_queue)
                  Sort(in_repair_queue,repque:model_number)
                  repque:model_number = xch:model_number
                  Get(in_repair_queue,repque:model_number)
                  If workshop# = 1
                      repque:in_workshop += 1
                  End!If job:workshop = 'YES'
                  If rtm# = 1
                      repque:rtm += 1
                  End!If rtm# = 1
                  If found# = 1
                      repque:awaiting_parts += 1
                  End!If found# = 1
                  Put(in_repair_queue)
              Else!If glo:select4 = 'YES'
                  If workshop# = 1
                      in_workshop_temp += 1
                  End!If job:workshop = 'YES'
                  If rtm# = 1
                      rtm_temp += 1
                  End!If rtm# = 1
                  If found# = 1
                      awaiting_parts_temp += 1
                  End!If found# = 1
  
              End!If glo:select4 = 'YES'
  
          End!If job:date_completed = ''
      end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
  
  end !loop
  access:exchange.restorefile(save_xch_id)
  setcursor()
  
  !If One Model Per Page
  If glo:select4 = 'YES'
  
      first_page# = 1
      Sort(glo:Queue,glo:pointer)                                                       !Loop through the global list of models
      Loop x# = 1 to Records(glo:Queue)                                                    !so that even if there aren't any records
          Get(glo:Queue,x#)                                                                !in the exchange queue, a page will still
          in_stock_temp = 0
          in_workshop_temp = 0
          Awaiting_Parts_Temp = 0
          RTM_Temp = 0
          requests_received_temp  = 0
          same_day_temp           = 0
          other_day_temp          = 0
          received_in_workshop_temp = 0
          Clear(exchange_queue)
          Sort(exchange_queue,excque:model_number)                                            !be printed for the in stock bit.
          excque:model_number = glo:pointer
          Get(exchange_queue,excque:model_number)
          Do getnextrecord2
          model_number_temp       = excque:model_number
          requests_received_temp  = excque:request_received
          same_day_temp           = excque:same_day
          other_day_temp          = excque:other_day
          received_in_workshop_temp = excque:received_in_workshop
          Failed_Exchange_Temp = requests_received_temp - same_day_temp - other_day_temp
          Despatched_Temp = same_day_temp + other_day_temp
          Awaiting_Arrival_Temp = Despatched_Temp - Received_In_Workshop_Temp
          Clear(in_repair_queue)
          Sort(in_repair_queue,repque:model_number)
          Loop y# = 1 To Records(in_repair_queue)
              Get(in_repair_queue,y#)
              If repque:model_number <> excque:model_number
                  Cycle
              End
              in_stock_temp   += repque:in_stock
              in_workshop_temp    += repque:in_workshop
              awaiting_parts_temp += repque:awaiting_parts
              rtm_temp    += repque:RTM
          End!Loop x# = 1 To Records(in_repair_queue)
          In_Repair_Total = In_Stock_Temp + In_Workshop_Temp + Awaiting_Parts_Temp + RTM_Temp
          In_Channel_Temp = awaiting_arrival_temp + in_repair_total
          tmp:RecordsCount += 1
          Print(rpt:Detail)
          If x# <> Records(glo:Queue)
              Print(rpt:new_page)
          End!If first_page# = 1
      End!Loop x# = 1 To Records(exchange_queue)
  
  Else!If glo:select4 = 'YES'
      Failed_Exchange_Temp = requests_received_temp - same_day_temp - other_day_temp
      Despatched_Temp = same_day_temp + other_day_temp
      Awaiting_Arrival_Temp = Despatched_Temp - Received_In_Workshop_Temp
      In_Repair_Total = In_Stock_Temp + In_Workshop_Temp + Awaiting_Parts_Temp + RTM_Temp
      In_Channel_Temp = awaiting_arrival_temp + in_repair_total
      tmp:RecordsCount += 1
      Print(rpt:Detail)
  End!If glo:select4 = 'YES'
  
  setcursor()
  close(progresswindow2)
  
  if tmp:RecordsCount = 0
  	Case MessageEx('There are no records that match the selected criteria.','ServiceBase 2000',|
  	               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
  		Of 1 ! &OK Button
  	End!Case MessageEx
      tmp:RecordsCount = 1
  end!if tmp:RecordsCount = 0
  IF ClarioNETServer:Active()                         !---ClarioNET 46
    IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
      ENDPAGE(SELF.Report)
      LOOP TempNameFunc_Flag = 1 TO RECORDS(SELF.PreviewQueue)
        GET(SELF.PreviewQueue, TempNameFunc_Flag)
        ClarioNET:AddMetafileName(SELF.PreviewQueue.Filename)
      END
      ClarioNET:SetReportProperties(report)
      FREE(SELF.PreviewQueue)
      TempNameFunc_Flag = 0
    END
  ELSE
    PARENT.AskPreview
  END                                                 !---ClarioNET 48
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(AskPreview, ())


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Exchange_Units_In_Channel')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Exchange_Units_In_Channel')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:JOBS.Open
  Relate:TRANTYPE.Open
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:date_booked)
  ThisReport.AddSortOrder(job:Date_Booked_Key)
  ThisReport.AddRange(job:date_booked,GLO:Select1,GLO:Select2)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  Do RecolourWindow


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
    Relate:TRANTYPE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Exchange_Units_In_Channel',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !Choose Printer
  tmp:printer = printer{propprint:device}
  Choose_Printer(new_printer",preview",copies")
  printer{propprint:device} = new_printer"
  printer{propprint:copies} = copies"
  if preview" <> 'YES'
      self.skippreview = True
  End!if preview" <> 'YES'
  ReturnValue = PARENT.OpenReport()
  !printed by
  set(defaults)
  access:defaults.next()
  
  access:users.clearkey(use:password_key)
  use:password =glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF ~ReturnValue
    report$?ReportPageNumber{PROP:PageNo}=True
  END
  IF ~ReturnValue
    SELF.Report $ ?ReportDateStamp{PROP:Text}=FORMAT(TODAY(),@d6b)
  END
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF ClarioNETServer:Active()
    ClarioNET:EndReport                               !---ClarioNET 101
  END                                                 !---ClarioNET 102
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Exchange_Units_In_Channel')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeNoRecords, ())
  return
  PARENT.TakeNoRecords
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeNoRecords, ())


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 91
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
    IF ClarioNETServer:Active()                       !---ClarioNET 44
      IF TempNameFunc_Flag = 0
        TempNameFunc_Flag = 1
        report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
      END
    END
  error# = 0
  If glo:select3 <> ''
      If job:account_number <> glo:select3
          error# = 1
      End!If job:account_number <> glo:select3
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = glo:select3
      if access:subtracc.fetch(sub:account_number_key) = Level:Benign
          account_number_temp = Clip(sub:account_number) & '-' & Clip(sub:company_name)
      end
  Else!If glo:select <> ''
      account_number_temp = 'ALL'
  End!If glo:select <> ''
  
  If error# = 0
  !IF glo:select4 = YES, i.e. one model per page. Don't print the first (summary) page.
  !Otherwise, show the model on top of each page
      If glo:select4 <> 'YES'
          Settarget(report)
          Hide(?model_string)
          model_number_temp = ''
          Settarget()
          If first_page_temp = 1
              first_page_temp = 0
              Print(rpt:first_page_title)
              Sort(glo:Queue,glo:pointer)
              Loop x# = 1 To Records(glo:Queue)
                  Get(glo:Queue,x#)
                  access:modelnum.clearkey(mod:model_number_key)
                  mod:model_number = glo:pointer
                  if access:modelnum.fetch(mod:model_number_key) = Level:Benign
                      manufacturer_temp = mod:manufacturer
                  end!if access:modelnum.fetch(mod:model_number_key) = Level:Benign
                  Print(rpt:first_page_detail)
              End!Loop x# = 1 To Records(glo:Queue)
              Print(rpt:new_page)
          End!If first_page_temp = 1
      End!If glo:select4 = 'YES'
  
  !Looping through jobs in date order.
  
  !First If glo:select4 = 'YES', means one page per model.
  
  !Check the queue of models and see if the job model is in it
      Sort(glo:Queue,glo:pointer)
      glo:pointer  = job:model_number
      Get(glo:Queue,glo:pointer)                                                        
      If ~Error()
          request# = 0
          access:trantype.clearkey(trt:transit_type_key)                                      !Check the transit type to see if the
          trt:transit_type = job:transit_type                                                 !default status is '108 Exchange Unit
          if access:trantype.fetch(trt:transit_type_key) = Level:benign                       !Required'.
              If sub(trt:initial_status,1,3) = '108'
                  request# = 1                                                                !Request Received
              End!If sub(trt:initial_status,1,3) = '108'
          end!if access:trantype.fetch(trt:transit_type_key) = Level:benign
          If glo:select4 = 'YES'
              Clear(exchange_queue)                                                       !to the job. Hopefully this should tie
              Sort(exchange_queue,excque:model_number)                                    !up with the requests, but there may be
              excque:model_number = job:model_number                                      !a mismatch due to transit types being
              Get(exchange_queue,excque:model_number)                                     !changed on the job.
              If Error()                                                                  !Model Number doesn't exist
                  excque:model_number = job:model_number                                  
                  If request# = 1
                      excque:request_received = 1
                  Else!If request# = 1
                      excque:request_received = 0
                  End!If request# = 1
                  If job:exchange_unit_number <> ''
                      If job:exchange_despatched <> ''
                          If job:exchange_despatched = job:date_booked
                              excque:same_day = 1
                              excque:other_day = 0
                          Else!If job:exchange_despatched = job:date_booked
                              excque:other_day = 1
                              excque:same_day = 0
                          End!If job:exchange_despatched = job:date_booked
                      End!If job:exchange_despatched <> ''
  
                      If job:workshop = 'YES'
                          excque:received_in_workshop = 1
                      Else!If job:workshop = 'YES'
                          excque:received_in_workshop = 0
                      End!If job:workshop = 'YES'
                  End!If job:exchange_unit_number <> ''
                  Add(Exchange_Queue)
                  
              Else!If Error()                                                             !Model number does exist
                  If request# = 1                                                         
                      excque:request_received += 1
                  End
                  If job:exchange_unit_number <> ''
                      If job:exchange_despatched <> ''
                          If job:exchange_despatched = job:date_booked
                              excque:same_day += 1
                          Else!If job:exchange_despatched = job:date_booked
                              excque:other_day += 1
                          End!If job:exchange_despatched = job:date_booked
                      End!If job:exchange_despatched <> ''
  
                      If job:workshop = 'YES'
                          excque:received_in_workshop += 1
                      End!If job:workshop = 'YES'
                  End!If job:exchange_unit_number <> ''
                  Put(Exchange_Queue)
              End!If Error()
          Else!If glo:select4 = 'YES'
              If request# = 1
                  requests_received_temp += 1
              End!If request# = 1
              If job:exchange_unit_number <> ''
                  If job:exchange_despatched <> ''
                      If job:exchange_despatched = job:date_booked
                          same_day_temp += 1
                      Else!If job:exchange_despatched = job:date_booked
                          other_day_temp += 1
                      End!If job:exchange_despatched = job:date_booked
                  End!If job:exchange_despatched <> ''
                  If job:workshop = 'YES'
                      Received_In_Workshop_Temp += 1
                  End!If job:workshop = 'YES'
              End!If job:exchange_unit_number <> ''
          End!If glo:select4 = 'YES'
      End!If ~Error()
  End!If error# = 0
  ReturnValue = PARENT.TakeRecord()
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

