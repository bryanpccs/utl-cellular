

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03080.INC'),ONCE        !Local module procedure declarations
                     END


TLCExportCriteria PROCEDURE                           !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
DisplayString        STRING(255)
tmp:AccountNumber    STRING(30)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:DateRangeType    BYTE(0)
tmp:ExchangeIMEI     STRING(30)
tmp:ParcelOut        STRING(30)
tmp:JobType          STRING(30)
tmp:Accessories      STRING(255)
save_cou_id          USHORT,AUTO
save_job_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
savepath             STRING(255)
window               WINDOW('TLC Daily Export Criteria'),AT(,,231,143),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,224,108),USE(?Sheet1),SPREAD
                         TAB('TLC Daily Export Criteria'),USE(?Tab1)
                           PROMPT('Main Account Number'),AT(8,20),USE(?tmp:AccountNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,20,124,10),USE(tmp:AccountNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Main Account Number'),TIP('Main Account Number'),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),REQ,UPR
                           BUTTON,AT(212,20,10,10),USE(?LookupAccountNumber),SKIP,FONT('Tahoma',8,,,CHARSET:ANSI),ICON('List3.ico')
                           PROMPT('Start Date'),AT(8,36),USE(?tmp:StartDate:Prompt)
                           ENTRY(@d6),AT(84,36,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(152,36,10,10),USE(?LookupStartDate),SKIP,ICON('Calenda2.ico')
                           PROMPT('End Date'),AT(8,52),USE(?tmp:EndDate:Prompt)
                           ENTRY(@d6),AT(84,52,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(152,52,10,10),USE(?LookupEndDate),SKIP,ICON('calenda2.ico')
                           OPTION('Date Range Type'),AT(84,68,124,40),USE(tmp:DateRangeType),BOXED
                             RADIO('Booking Date'),AT(92,80),USE(?tmp:DateRangeType:Radio1),VALUE('0')
                             RADIO('Customer Satisfaction Date'),AT(92,92),USE(?tmp:DateRangeType:Radio2),VALUE('1')
                           END
                         END
                       END
                       PANEL,AT(4,116,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(112,120,56,16),USE(?Button3),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(168,120,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Shell Execute Variables
AssocFile    CString(255)
Operation    CString(255)
Param        CString(255) 
Dir          CString(255)
!Save Entry Fields Incase Of Lookup
look:tmp:AccountNumber                Like(tmp:AccountNumber)
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?tmp:AccountNumber:Prompt{prop:FontColor} = -1
    ?tmp:AccountNumber:Prompt{prop:Color} = 15066597
    If ?tmp:AccountNumber{prop:ReadOnly} = True
        ?tmp:AccountNumber{prop:FontColor} = 65793
        ?tmp:AccountNumber{prop:Color} = 15066597
    Elsif ?tmp:AccountNumber{prop:Req} = True
        ?tmp:AccountNumber{prop:FontColor} = 65793
        ?tmp:AccountNumber{prop:Color} = 8454143
    Else ! If ?tmp:AccountNumber{prop:Req} = True
        ?tmp:AccountNumber{prop:FontColor} = 65793
        ?tmp:AccountNumber{prop:Color} = 16777215
    End ! If ?tmp:AccountNumber{prop:Req} = True
    ?tmp:AccountNumber{prop:Trn} = 0
    ?tmp:AccountNumber{prop:FontStyle} = font:Bold
    ?tmp:StartDate:Prompt{prop:FontColor} = -1
    ?tmp:StartDate:Prompt{prop:Color} = 15066597
    If ?tmp:StartDate{prop:ReadOnly} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 15066597
    Elsif ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 8454143
    Else ! If ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 16777215
    End ! If ?tmp:StartDate{prop:Req} = True
    ?tmp:StartDate{prop:Trn} = 0
    ?tmp:StartDate{prop:FontStyle} = font:Bold
    ?tmp:EndDate:Prompt{prop:FontColor} = -1
    ?tmp:EndDate:Prompt{prop:Color} = 15066597
    If ?tmp:EndDate{prop:ReadOnly} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 15066597
    Elsif ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 8454143
    Else ! If ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 16777215
    End ! If ?tmp:EndDate{prop:Req} = True
    ?tmp:EndDate{prop:Trn} = 0
    ?tmp:EndDate{prop:FontStyle} = font:Bold
    ?tmp:DateRangeType{prop:Font,3} = -1
    ?tmp:DateRangeType{prop:Color} = 15066597
    ?tmp:DateRangeType{prop:Trn} = 0
    ?tmp:DateRangeType:Radio1{prop:Font,3} = -1
    ?tmp:DateRangeType:Radio1{prop:Color} = 15066597
    ?tmp:DateRangeType:Radio1{prop:Trn} = 0
    ?tmp:DateRangeType:Radio2{prop:Font,3} = -1
    ?tmp:DateRangeType:Radio2{prop:Color} = 15066597
    ?tmp:DateRangeType:Radio2{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
ExportBit       Routine
    tmp:ExchangeIMEI    = ''
    tmp:ParcelOut       = ''
    tmp:Accessories     = ''
    tmp:JobType         = ''
    tmp:ParcelOut       = job:Consignment_Number
    If job:Exchange_Unit_Number <> ''
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number    = job:Exchange_Unit_Number
        If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
          !Found
            tmp:ExchangeIMEI  = xch:ESN
            tmp:ParcelOut     = job:Exchange_Consignment_Number
        Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
            tmp:ExchangeIMEI  = ''
        End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign

    Else!If job:Exchange_Unit_Number <> ''
        tmp:ExchangeIMEI = ''
    End!If job:Exchange_Unit_Number <> ''


    tmp:Accessories = ''
    Save_jac_ID = Access:JOBACC.SaveFile()
    Access:JOBACC.ClearKey(jac:Ref_Number_Key)
    jac:Ref_Number = job:Ref_Number
    Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
    Loop
        If Access:JOBACC.NEXT()
           Break
        End !If
        If jac:Ref_Number <> job:Ref_Number      |
            Then Break.  ! End If
        If tmp:Accessories = ''
            tmp:Accessories = jac:Accessory
        Else!If tmp:Accessories = ''
            tmp:Accessories = Clip(tmp:Accessories) & ' ' & jac:Accessory
        End!If tmp:Accessories = ''
    End !Loop
    Access:JOBACC.RestoreFile(Save_jac_ID)

    If job:Warranty_Job = 'YES'
        tmp:JobType = job:Warranty_Charge_Type
    Else!If job:Warranty_Job = 'YES'
        tmp:JobType = ''
    End!If job:Warranty_Job = 'YES'

    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
    jbn:RefNumber   = job:Ref_Number
    If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
        !Found

    Else! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:JOBNOTES.Tryfetch() = Level:Benign
    

    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Found

    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

    Clear(gen:Record)
    gen:Line1   = Stripcomma(job:Account_Number)
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(job:Order_Number))
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(job:Incoming_Consignment_Number))
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(tmp:ParcelOut))
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(job:ESN))
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(tmp:ExchangeIMEI))
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(tmp:JobType))
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(StripReturn(Clip(jbn:Fault_Description)))
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(tmp:Accessories))
    gen:Line1   = Clip(gen:Line1) & ',' & Format(job:Date_Booked,@d17)
    gen:Line1   = Clip(gen:Line1) & ',' & Format(job:DOP,@d17)
    If job:Exchange_Despatched <> ''
        gen:Line1   = Clip(gen:Line1) & ',' & Format(job:Exchange_Despatched,@d17)
    Else !If job:Exchange_Despatched <> ''
        gen:Line1   = Clip(gen:Line1) & ',' & Format(job:Date_Despatched,@d17)
    End !If job:Exchange_Despatched <> ''
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(job:Model_Number))
    gen:Line1   = Clip(gen:Line1) & ',' & Format(jobe:InWorkshopDate,@d17)
    gen:Line1   = Clip(gen:Line1) & ',' & Format(jobe:InWorkshopTime,@d17)
    Access:EXPGEN.Insert()
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'TLCExportCriteria',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('DisplayString',DisplayString,'TLCExportCriteria',1)
    SolaceViewVars('tmp:AccountNumber',tmp:AccountNumber,'TLCExportCriteria',1)
    SolaceViewVars('tmp:StartDate',tmp:StartDate,'TLCExportCriteria',1)
    SolaceViewVars('tmp:EndDate',tmp:EndDate,'TLCExportCriteria',1)
    SolaceViewVars('tmp:DateRangeType',tmp:DateRangeType,'TLCExportCriteria',1)
    SolaceViewVars('tmp:ExchangeIMEI',tmp:ExchangeIMEI,'TLCExportCriteria',1)
    SolaceViewVars('tmp:ParcelOut',tmp:ParcelOut,'TLCExportCriteria',1)
    SolaceViewVars('tmp:JobType',tmp:JobType,'TLCExportCriteria',1)
    SolaceViewVars('tmp:Accessories',tmp:Accessories,'TLCExportCriteria',1)
    SolaceViewVars('save_cou_id',save_cou_id,'TLCExportCriteria',1)
    SolaceViewVars('save_job_id',save_job_id,'TLCExportCriteria',1)
    SolaceViewVars('save_jac_id',save_jac_id,'TLCExportCriteria',1)
    SolaceViewVars('savepath',savepath,'TLCExportCriteria',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:AccountNumber:Prompt;  SolaceCtrlName = '?tmp:AccountNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:AccountNumber;  SolaceCtrlName = '?tmp:AccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupAccountNumber;  SolaceCtrlName = '?LookupAccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate:Prompt;  SolaceCtrlName = '?tmp:StartDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate;  SolaceCtrlName = '?tmp:StartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupStartDate;  SolaceCtrlName = '?LookupStartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate:Prompt;  SolaceCtrlName = '?tmp:EndDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate;  SolaceCtrlName = '?tmp:EndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupEndDate;  SolaceCtrlName = '?LookupEndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DateRangeType;  SolaceCtrlName = '?tmp:DateRangeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DateRangeType:Radio1;  SolaceCtrlName = '?tmp:DateRangeType:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DateRangeType:Radio2;  SolaceCtrlName = '?tmp:DateRangeType:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button3;  SolaceCtrlName = '?Button3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('TLCExportCriteria')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'TLCExportCriteria')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:AccountNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:COURIER.Open
  Relate:EXCHANGE.Open
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:JOBS.UseFile
  Access:JOBNOTES.UseFile
  SELF.FilesOpened = True
  tmp:StartDate   = Today()
  tmp:EndDate     = Today()
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  IF ?tmp:AccountNumber{Prop:Tip} AND ~?LookupAccountNumber{Prop:Tip}
     ?LookupAccountNumber{Prop:Tip} = 'Select ' & ?tmp:AccountNumber{Prop:Tip}
  END
  IF ?tmp:AccountNumber{Prop:Msg} AND ~?LookupAccountNumber{Prop:Msg}
     ?LookupAccountNumber{Prop:Msg} = 'Select ' & ?tmp:AccountNumber{Prop:Msg}
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
    Relate:EXCHANGE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'TLCExportCriteria',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    BrowseMainAccount
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:AccountNumber
      IF tmp:AccountNumber OR ?tmp:AccountNumber{Prop:Req}
        tra:Account_Number = tmp:AccountNumber
        !Save Lookup Field Incase Of error
        look:tmp:AccountNumber        = tmp:AccountNumber
        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:AccountNumber = tra:Account_Number
          ELSE
            !Restore Lookup On Error
            tmp:AccountNumber = look:tmp:AccountNumber
            SELECT(?tmp:AccountNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupAccountNumber
      ThisWindow.Update
      tra:Account_Number = tmp:AccountNumber
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:AccountNumber = tra:Account_Number
          Select(?+1)
      ELSE
          Select(?tmp:AccountNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:AccountNumber)
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
      If tmp:AccountNumber = ''
          Select(?tmp:AccountNumber)
      Else!If tmp:AccountNumber = ''
          savepath = path()
          glo:file_name = 'TLCEXP.CSV'
          If not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
                      file:keepdir + file:noerror + file:longname)
              !Failed
              setpath(savepath)
          else!If not filedialog
              !Found
              setpath(savepath)
              Remove(EXPGEN)
      
              recordspercycle         = 25
              recordsprocessed        = 0
              percentprogress         = 0
              progress:thermometer    = 0
              thiswindow.reset(1)
              open(progresswindow)
      
              ?progress:userstring{prop:text} = 'Running...'
              ?progress:pcttext{prop:text} = '0% Completed'
      
      
      
              Access:EXPGEN.Open()
              Access:EXPGEN.UseFile()
      
              Clear(gen:Record)
              gen:Line1   = 'CERT NO,VPS,Parcel In,Parcel Out,Org IMEI,Exc IMEI,Job Type,Fault Description,Accessories,Date Booked,'&|
                              'DOP,Desp Date,Model Number,In Workshop Date,In Workshop Time'
              Access:EXPGEN.Insert()
      
              Case tmp:DateRangeType
                  Of 0 !Booking Date Range
                      recordstoprocess    = Records(Jobs)
      
      
                      Save_job_ID = Access:JOBS.SaveFile()
                      Access:JOBS.ClearKey(job:Date_Booked_Key)
                      job:date_booked = tmp:StartDate
                      Set(job:Date_Booked_Key,job:Date_Booked_Key)
                      Loop
                          If Access:JOBS.NEXT()
                             Break
                          End !If
                          If job:date_booked > tmp:EndDate      |
                              Then Break.  ! End If
                  !---Before Routine
      
                  !---Insert Routine
                          Do GetNextRecord2
                          cancelcheck# += 1
                          If cancelcheck# > (RecordsToProcess/100)
                              Do cancelcheck
                              If tmp:cancel = 1
                                  Break
                              End!If tmp:cancel = 1
                              cancelcheck# = 0
                          End!If cancelcheck# > 50
      
      
                          Access:SUBTRACC.ClearKey(sub:Main_Account_Key)
                          sub:Main_Account_Number = tmp:AccountNumber
                          sub:Account_Number      = job:Account_Number
                          If Access:SUBTRACC.TryFetch(sub:Main_Account_Key)
                              Cycle
                          End!If job:Account_Number <> func:AccountNumber
      
                          Do ExportBit
      
                      End !Loop
                      Access:JOBS.RestoreFile(Save_job_ID)
      
                  Of 1 !Customer Satisfaction Date
                      RecordsToProcess = Records(COURIER)
      
                      Save_cou_ID = Access:COURIER.SaveFile()
                      Set(cou:Courier_Key)
                      Loop
                          If Access:COURIER.NEXT()
                             Break
                          End !If
      
                          Do GetNextRecord2
                          cancelcheck# += 1
                          If cancelcheck# > (RecordsToProcess/100)
                              Do cancelcheck
                              If tmp:cancel = 1
                                  Break
                              End!If tmp:cancel = 1
                              cancelcheck# = 0
                          End!If cancelcheck# > 50
      
                          Save_job_ID = Access:JOBS.SaveFile()
                          Access:JOBS.ClearKey(job:DateDespatchKey)
                          job:Courier         = cou:Courier
                          job:Date_Despatched = tmp:StartDate
                          Set(job:DateDespatchKey,job:DateDespatchKey)
                          Loop
                              If Access:JOBS.NEXT()
                                 Break
                              End !If
                              If job:Courier         <> cou:Courier      |
                              Or job:Date_Despatched > tmp:EndDate      |
                                  Then Break.  ! End If
                              Access:SUBTRACC.ClearKey(sub:Main_Account_Key)
                              sub:Main_Account_Number = tmp:AccountNumber
                              sub:Account_Number      = job:Account_Number
                              If Access:SUBTRACC.TryFetch(sub:Main_Account_Key)
                                  Cycle
                              End!If job:Account_Number <> func:AccountNumber
      
                              If job:Exchange_Despatched <> ''
                                  Cycle
                              End !If job:Exchange_Unit_Number <> ''
      
                              Do ExportBit
                          End !Loop
                          Access:JOBS.RestoreFile(Save_job_ID)
      
                          Save_job_ID = Access:JOBS.SaveFile()
                          Access:JOBS.ClearKey(job:DateDespExcKey)
                          job:Exchange_Courier    = cou:Courier
                          job:Exchange_Despatched = tmp:StartDate
                          Set(job:DateDespExcKey,job:DateDespExcKey)
                          Loop
                              If Access:JOBS.NEXT()
                                 Break
                              End !If
                              If job:Exchange_Courier    <> cou:Courier      |
                              Or job:Exchange_Despatched > tmp:EndDate      |
                                  Then Break.  ! End If
                              Access:SUBTRACC.ClearKey(sub:Main_Account_Key)
                              sub:Main_Account_Number = tmp:AccountNumber
                              sub:Account_Number      = job:Account_Number
                              If Access:SUBTRACC.TryFetch(sub:Main_Account_Key)
                                  Cycle
                              End!If job:Account_Number <> func:AccountNumber
      
                              Do ExportBit
                          End !Loop
                          Access:JOBS.RestoreFile(Save_job_ID)
      
                      End !Loop
                      Access:COURIER.RestoreFile(Save_cou_ID)
              End !Case tmp:DateRangeType
              Access:EXPGEN.Close()
              Do EndPrintRun
              close(progresswindow)
              Case MessageEx('Export Completed.','ServiceBase 2000',|
                             'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              AssocFile = Clip(glo:File_Name)
              Param = ''
              Dir = ''
              Operation = 'Open'
              ShellExecute(GetDesktopWindow(), Operation, AssocFile, Param, Dir, 5)
          End!If not filedialog
      
      
      
      
      
      End!If tmp:AccountNumber = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'TLCExportCriteria')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?tmp:AccountNumber
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AccountNumber, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Account Number')
             Post(Event:Accepted,?LookupAccountNumber)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupAccountNumber)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AccountNumber, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

