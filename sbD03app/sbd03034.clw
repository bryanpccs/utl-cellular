

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03034.INC'),ONCE        !Local module procedure declarations
                     END








Third_Party_Consignment_Note PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:Retained         BYTE(0)
save_aud_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:BatchNumber      LONG
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
count_temp           LONG
tmp:accessories      STRING(100)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(TRDBATCH)
                       PROJECT(trb:AuthorisationNo)
                       PROJECT(trb:ESN)
                       PROJECT(trb:Ref_Number)
                     END
report               REPORT,AT(396,3885,7521,7417),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,771,7521,2781),USE(?unnamed)
                         STRING('3rd Party Agent:'),AT(4896,156),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,156),USE(GLO:Select1),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(5885,313),USE(GLO:Select2),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(5885,469),USE(trb:AuthorisationNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Authorisation No:'),AT(4896,469),USE(?string22:3),TRN,FONT(,8,,)
                         STRING('Batch No:'),AT(4896,313),USE(?string22:2),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(4896,625),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,625),USE(tmp:PrintedBy),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(4896,781),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5833,781),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6458,781),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(4896,938),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,938),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,938),USE(?String26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6354,938,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                         STRING('Repair If Required: '),AT(156,1490),USE(?String46),TRN,FONT(,8,,)
                         STRING(@s3),AT(1771,1490),USE(GLO:Select6),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1563),USE(trd:Company_Name),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING(@s3),AT(1771,1646),USE(GLO:Select7),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s3),AT(1771,1802),USE(GLO:Select8),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1719),USE(trd:Address_Line1),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING(@s3),AT(1771,1958),USE(GLO:Select9),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s3),AT(1771,2115),USE(GLO:Select11),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Upgrade Software:'),AT(156,2271),USE(?String46:6),TRN,FONT(,8,,)
                         STRING(@s3),AT(1771,2271),USE(GLO:Select12),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1875),USE(trd:Address_Line2),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING(@s30),AT(4063,2031),USE(trd:Address_Line3),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING(@s15),AT(4063,2188),USE(trd:Postcode),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING('Screening:'),AT(156,1646),USE(?String46:2),TRN,FONT(,8,,)
                         STRING('Refurbish If Required:'),AT(156,1802),USE(?String46:3),TRN,FONT(,8,,)
                         STRING('Upgrade If Required: '),AT(156,1958),USE(?String46:4),TRN,FONT(,8,,)
                         STRING('Refurbish All Units:'),AT(156,2115),USE(?String46:5),TRN,FONT(,8,,)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,167),USE(?detailband)
                           STRING(@s16),AT(208,0),USE(trb:ESN),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s11),AT(1094,0),USE(job:MSN),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s20),AT(1719,0),USE(job:Model_Number),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s20),AT(2813,0),USE(job:Unit_Type),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s8),AT(6906,0),USE(trb:Ref_Number),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s100),AT(3906,0,2448,156),USE(tmp:accessories),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@d6),AT(6354,0),USE(jot:DateDespatched),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           LINE,AT(260,52,6979,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Number Of Lines:'),AT(260,104),USE(?String38),TRN,FONT(,8,,FONT:bold)
                           STRING(@n-14),AT(1667,104),USE(count_temp),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTDET.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('3RD PARTY CONSIGNMENT NOTE'),AT(3958,0,3490,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING('Unit Type'),AT(2813,3177),USE(?string25:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Sent'),AT(6333,3177),USE(?string25:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Job No'),AT(6979,3177),USE(?string25:4),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Accessories Despatched'),AT(3906,3177),USE(?AccessoriesDespatched),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(156,573,2240,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,938),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,938),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1094),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1250),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1250),USE(?string19:2),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1094),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING('DELIVERY ADDRESS'),AT(4052,1458),USE(?String42),TRN,FONT(,9,,FONT:bold)
                         STRING('CONSIGNMENT DETAILS'),AT(146,1458),USE(?String42:2),TRN,FONT(,9,,FONT:bold)
                         STRING('I.M.E.I. / E.S.N.'),AT(208,3177),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(1094,3177),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Model Number'),AT(1719,3177),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CPCSDummyDetail         SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Third_Party_Consignment_Note')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  ! Before Embed Point: %ProcedureSetup) DESC(Procedure Setup) ARG()
  tmp:BatchNumber   = glo:Select2
  ! After Embed Point: %ProcedureSetup) DESC(Procedure Setup) ARG()
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('tmp:BatchNumber',tmp:BatchNumber)
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:TRDBATCH.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:STANTEXT.Open
  Access:TRDPARTY.UseFile
  Access:JOBACC.UseFile
  Access:JOBTHIRD.UseFile
  Access:JOBS.UseFile
  Access:ACCESSOR.UseFile
  Access:AUDIT.UseFile
  
  
  RecordsToProcess = RECORDS(TRDBATCH)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(TRDBATCH,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(trb:Company_Batch_ESN_Key)
      Process:View{Prop:Filter} = |
      'UPPER(trb:Company_Name) = UPPER(GLO:Select1) AND (trb:Batch_Number = t' & |
      'mp:BatchNumber)'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
?   Message('Before Printing','Debug Message',icon:exclamation)

    print# = 1

    If Upper(trb:status) <> 'OUT' AND GLO:Select10 = 'NO'
        print# = 0
    End!If Upper(trb:status) = 'OUT'

    If print# = 1
?    Message('Before Looking For A Job','Debug Message',icon:exclamation)
        access:JOBS.clearkey(job:Ref_Number_Key)
        job:Ref_Number  = trb:Ref_Number
        If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign
?    Message('Found A Job','Debug Message',icon:exclamation)

            !Found
            Access:JOBTHIRD.Clearkey(jot:ThirdPartyKey)
            jot:ThirdPartyNumber = trb:RecordNumber
            If Access:JOBTHIRD.Tryfetch(jot:ThirdPartyKey) = Level:Benign
                !Found

            Else! If Access:JOBTHIRD.Tryfetch(jot:ThirdPartyKey) = Level:Benign
                !Error
                Assert(0,'<13,10>Cannot find JOBTHIRD Entry attached to the job.<13,10>')
            End! If Access:JOBTHIRD.Tryfetch(jot:ThirdPartyKey) = Level:Benign
            
            !Match as Despatched
            If glo:Select3 = 'YES'
                job:ThirdPartyDateDesp   = glo:Select4
                Access:JOBS.Update()

                !Write the turnaround time to the record
                trb:TurnTime    = glo:Select5
                Access:TRDBATCH.Update()

                !Get the individual record in the JOBTHIRD table that relates to the TRDBATCH record
                access:JOBTHIRD.clearkey(jot:ThirdPartyKey)
                jot:ThirdPartyNumber    = trb:RecordNumber
                If access:JOBTHIRD.tryfetch(jot:ThirdPartyKey) = Level:Benign
                    !Found
                    jot:DateDespatched  = glo:Select4
                    Access:JOBTHIRD.Update()
                Else! If access:JOBTHIRD.tryfetch(jot:ThirdPartyKey) = Level:Benign
                    !For some reason, a record cannot be found. I suppose I should add a record
                    !Hopefully this will recover any errors that might occur.
                    ASSERT(0,'Could not find a Third Party Entry attached to the Job ' & Clip(job:Ref_Number) & |
                            '. |A new Third Party Entry will be created')
                    !See if a previous entry exists in the JOBTHIRD file for this job
                    !If so, then take the Original IMEI Number. So every entry has the same
                    !Original IMEI Number.
                    OriginalIMEI"   = job:ESN
                    Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
                    jot:RefNumber = job:Ref_Number
                    Set(jot:RefNumberKey,jot:RefNumberKey)
                    If Access:JOBTHIRD.NEXT() = Level:Benign
                        If jot:RefNumber = job:Ref_Number
                            error# = 0
                            OriginalIMEI"    = jot:OriginalIMEI
                        End!If jot:RefNumber = job:RefNumber
                    End!If Access:JOBTHIRD.NEXT() = Level:Benign

                    !Create a new entry in the JOBTHIRD file for this despatch
                    If Access:JOBTHIRD.Primerecord() = Level:Benign
                        jot:RefNumber       = job:Ref_Number
                        jot:OriginalIMEI    = OriginalIMEI"
                        jot:OutIMEI         = trb:ESN
                        jot:InIMEI          = ''
                        jot:DateOut         = Today()
                        jot:DateIn          = ''
                        jot:ThirdPartyNumber    = trb:RecordNumber
                        If Access:JOBTHIRD.Tryinsert()
                            Access:JOBTHIRD.Cancelautoinc()
                        End!If Access:JOBTHIRD.Tryinsert()
                    End!If Access:JOBTHIRD.Primerecord() = Level:Benign

                End! If access:JOBTHIRD.tryfetch(jot:ThirdPartyKey) = Level:Benign
            End!If glo:Select3 = 'YES'

            tmp:Retained = 0
            x# = 0
            Save_aud_ID = Access:AUDIT.SaveFile()
            Access:AUDIT.ClearKey(aud:Action_Key)
            aud:Ref_Number = job:Ref_Number
            aud:Action     = '3RD PARTY AGENT: UNIT SENT'
            aud:Date       = Today()
            Set(aud:Action_Key,aud:Action_Key)
            Loop
                If Access:AUDIT.PREVIOUS()
                   Break
                End !If
                If aud:Ref_Number <> job:Ref_Number      |
                Or aud:Action     <> '3RD PARTY AGENT: UNIT SENT'      |
                    Then Break.  ! End If
                x# = Instring('ACCESSORIES DESPATCHED WITH UNIT:',aud:Notes,1,1)
                If x# <> 0
                    tmp:Accessories = StripReturn(Clip(Sub(aud:Notes,x# + 35,Len(Aud:Notes))))
                    tmp:Retained = 0
                    Break
                End !If x# <> 0
                x# = Instring('ACCESSORIES RETAINED WITH UNIT:',aud:Notes,1,1)
                If x# <> 0
                    tmp:Accessories = StripReturn(Clip(Sub(aud:Notes,x# + 33,Len(Aud:Notes))))
                    tmp:Retained = 1
                    Break
                End !If x# <> 0

            End !Loop
            Access:AUDIT.RestoreFile(Save_aud_ID)

!            tmp:Accessories = ''
!            Save_jac_ID = Access:JOBACC.SaveFile()
!            Access:JOBACC.ClearKey(jac:Ref_Number_Key)
!            jac:Ref_Number = job:Ref_Number
!            Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
!            Loop
!                If Access:JOBACC.NEXT()
!                   Break
!                End !If
!                If jac:Ref_Number <> job:Ref_Number      |
!                    Then Break.  ! End If
!                If tmp:Accessories = ''
!                    tmp:Accessories = jac:Accessory
!                Else!If tmp:Accessories = ''
!                    tmp:Accessories = Clip(tmp:Accessories) & ', ' & jac:Accessory
!                End!If tmp:Accessories = ''
!            End !Loop
!            Access:JOBACC.RestoreFile(Save_jac_ID)
!            If tmp:Accessories <> ''
                If tmp:Retained = 1
                    SetTarget(Report)
                    ?AccessoriesDespatched{prop:Text} = 'Accessories Retained'
                    SetTarget()
!                Else!If tmp:Retained = 1
!                    tmp:Accessories = '(DES) ' & Clip(tmp:Accessories)
                End!If tmp:Retained = 1
!            End!If tmp:Accessories <> ''
            Count_Temp += 1
            Print(rpt:Detail)
        Else! If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign
            Assert(0,'<13,10>Cannot find associate JOB Entry (' & Clip(trb:Ref_Number) & ').<13,10>')        !Error
        End! If access:JOBS.tryfetch(job:Ref_Number_Key) = Level:Benign
    End!If Print# = 1
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(TRDBATCH,'QUICKSCAN=off').
  IF ~ReportWasOpened
    DO OpenReportRoutine
  END
  IF ~CPCSDummyDetail
    SETTARGET(report)
    CPCSDummyDetail = LASTFIELD()+1
    CREATE(CPCSDummyDetail,CREATE:DETAIL)
    UNHIDE(CPCSDummyDetail)
    SETPOSITION(CPCSDummyDetail,,,,10)
    CREATE((CPCSDummyDetail+1),CREATE:STRING,CPCSDummyDetail)
    UNHIDE((CPCSDummyDetail+1))
    SETPOSITION((CPCSDummyDetail+1),0,0,0,0)
    (CPCSDummyDetail+1){PROP:TEXT}='X'
    SETTARGET
    PRINT(report,CPCSDummyDetail)
    LocalResponse=RequestCompleted
  END
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:ACCESSOR.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:STANTEXT.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'TRDBATCH')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','CONSIGNMENT NOTE - 3RD PARTY')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'CONSIGNMENT NOTE - 3RD PARTY'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  If GETINI('HIDE','ThirdPartyDetails',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      SetTarget(Report)
      ?String46{prop:Hide} = 1
      ?String46:2{prop:Hide} = 1
      ?String46:3{prop:Hide} = 1
      ?String46:4{prop:Hide} = 1
      ?String46:5{prop:Hide} = 1
      ?String46:6{prop:Hide} = 1
      ?glo:Select6{prop:Hide} = 1
      ?glo:Select7{prop:Hide} = 1
      ?glo:Select8{prop:Hide} = 1
      ?glo:Select9{prop:Hide} = 1
      ?glo:Select11{prop:Hide} = 1
      ?glo:Select12{prop:Hide} = 1
      ?String42:2{prop:Hide} = 1
      SetTarget()
  End !GETINI('HIDE','ThirdPartyDetails',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'CONSIGNMENT NOTE - 3RD PARTY'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'CONSIGNMENT NOTE - 3RD PARTY'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Third_Party_Consignment_Note'
  END
  report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Third_Party_Consignment_Note',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Third_Party_Consignment_Note',1)
    SolaceViewVars('tmp:Retained',tmp:Retained,'Third_Party_Consignment_Note',1)
    SolaceViewVars('save_aud_id',save_aud_id,'Third_Party_Consignment_Note',1)
    SolaceViewVars('save_jac_id',save_jac_id,'Third_Party_Consignment_Note',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Third_Party_Consignment_Note',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Third_Party_Consignment_Note',1)
    SolaceViewVars('tmp:BatchNumber',tmp:BatchNumber,'Third_Party_Consignment_Note',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Third_Party_Consignment_Note',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Third_Party_Consignment_Note',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Third_Party_Consignment_Note',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Third_Party_Consignment_Note',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Third_Party_Consignment_Note',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Third_Party_Consignment_Note',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Third_Party_Consignment_Note',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Third_Party_Consignment_Note',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Third_Party_Consignment_Note',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Third_Party_Consignment_Note',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Third_Party_Consignment_Note',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Third_Party_Consignment_Note',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Third_Party_Consignment_Note',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Third_Party_Consignment_Note',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Third_Party_Consignment_Note',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Third_Party_Consignment_Note',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Third_Party_Consignment_Note',1)
    SolaceViewVars('InitialPath',InitialPath,'Third_Party_Consignment_Note',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Third_Party_Consignment_Note',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Third_Party_Consignment_Note',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Third_Party_Consignment_Note',1)
    SolaceViewVars('count_temp',count_temp,'Third_Party_Consignment_Note',1)
    SolaceViewVars('tmp:accessories',tmp:accessories,'Third_Party_Consignment_Note',1)


BuildCtrlQueue      Routine







