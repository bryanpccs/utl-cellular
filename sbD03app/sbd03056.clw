

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03056.INC'),ONCE        !Local module procedure declarations
                     END








Restocking_Note PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_aud_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
code_temp            BYTE
option_temp          BYTE
bar_code_string_temp CSTRING(21)
bar_code_temp        CSTRING(21)
qa_reason_temp       STRING(255)
engineer_temp        STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Charge_Type)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:MSN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:Repair_Type)
                       PROJECT(job_ali:Repair_Type_Warranty)
                       PROJECT(job_ali:Unit_Type)
                       PROJECT(job_ali:Warranty_Charge_Type)
                       PROJECT(job_ali:date_booked)
                     END
report               REPORT,AT(406,1677,7521,9146),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,9115),USE(?detailband)
                           STRING('Job Number:'),AT(313,208),USE(?String2),TRN,FONT(,20,,FONT:bold,CHARSET:ANSI)
                           STRING(@s20),AT(2917,313,1771,208),USE(bar_code_temp),LEFT,FONT('C128 High 12pt LJ3',12,,FONT:regular,CHARSET:ANSI)
                           STRING(@s8),AT(4792,313),USE(job_ali:Ref_Number,,?JOB_ALI:Ref_Number:2),TRN,LEFT,FONT(,12,,FONT:bold)
                           STRING('Unit Details:'),AT(313,1823),USE(?String2:2),TRN,FONT(,20,,FONT:bold,CHARSET:ANSI)
                           STRING('Date Booked:'),AT(2917,1927),USE(?String6),TRN,FONT(,12,,FONT:bold)
                           STRING(@d6b),AT(4792,1927),USE(job_ali:date_booked),LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Manufacturer:'),AT(2917,2240),USE(?String6:2),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,2240),USE(job_ali:Manufacturer),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Model Number:'),AT(2917,2552),USE(?String6:3),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,2552),USE(job_ali:Model_Number),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Unit Type:'),AT(2917,2865),USE(?String6:4),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,2865),USE(job_ali:Unit_Type),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('E.S.N. / I.M.E.I.'),AT(2917,3177),USE(?String6:5),TRN,FONT(,12,,FONT:bold)
                           STRING(@s16),AT(4792,3177),USE(job_ali:ESN),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('M.S.N.'),AT(2917,3490),USE(?String6:6),TRN,FONT(,12,,FONT:bold)
                           STRING(@s16),AT(4792,3490),USE(job_ali:MSN),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Chargeable Type:'),AT(2917,4688),USE(?String6:7),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,4688),USE(job_ali:Charge_Type),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING(@s30),AT(4792,4948),USE(job_ali:Repair_Type),LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Repair Type:'),AT(2917,4948),USE(?String6:8),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,5313),USE(job_ali:Warranty_Charge_Type),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Warranty Type:'),AT(2917,5313),USE(?String6:9),TRN,FONT(,12,,FONT:bold)
                           STRING('Repair Type:'),AT(2917,5573),USE(?String6:10),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,5573),USE(job_ali:Repair_Type_Warranty),LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING(@s60),AT(4792,6667),USE(tmp:PrintedBy),FONT(,12,,)
                           STRING(@s60),AT(4792,6406),USE(engineer_temp),TRN,FONT(,12,,)
                           STRING('Original Engineer:'),AT(2917,6406),USE(?String6:13),TRN,FONT(,12,,FONT:bold)
                           STRING('Inspector:'),AT(2917,6667),USE(?String6:11),TRN,FONT(,12,,FONT:bold)
                           STRING('Repair Details:'),AT(260,4635),USE(?String2:3),TRN,FONT(,20,,FONT:bold,CHARSET:ANSI)
                           STRING('QA Details:'),AT(313,6406),USE(?String2:4),TRN,FONT(,20,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       FORM,AT(396,479,7521,10552),USE(?unnamed:3)
                         STRING('RESTOCKING NOTE'),AT(1188,52,5156,417),USE(?string20),TRN,CENTER,FONT(,24,,FONT:bold)
                         BOX,AT(104,521,7292,9844),USE(?Box1),COLOR(COLOR:Black)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Restocking_Note')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS_ALIAS.Open
  Relate:DEFPRINT.Open
  Access:AUDIT.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS_ALIAS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS_ALIAS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(job_ali:Ref_Number_Key)
      Process:View{Prop:Filter} = |
      'job_ali:Ref_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(rpt:detail)
        END
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS_ALIAS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDIT.Close
    Relate:DEFPRINT.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS_ALIAS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','QA FAILED NOTE')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'QA FAILED NOTE'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  !barcode bit
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(glo:select1)
  sequence2(code_temp,option_temp,bar_code_string_temp,bar_code_temp)
  
  Settarget(report)
  If job_ali:chargeable_job <> 'YES'
      Hide(?job_ali:charge_type)
      Hide(?job_ali:repair_type)
  End!If job_ali:chargeable_job <> 'YES'
  If job_ali:warranty_job <> 'YES'
      Hide(?job_ali:warranty_charge_type)
      Hide(?job_ali:repair_type_warranty)
  End!If job_ali:warranty_job <> 'YES'
  Settarget()
  
  
  setcursor(cursor:wait)
  save_aud_id = access:audit.savefile()
  access:audit.clearkey(aud:action_key)
  aud:ref_number = glo:select1
  aud:action     = 'QA REJECTION'
  set(aud:action_key,aud:action_key)
  loop
      !omemo(audit)
      if access:audit.next()
         break
      end !if
      if aud:ref_number <> glo:select1      |
      or aud:action     <> 'QA REJECTION'      |
          then break.  ! end if
      qa_reason_temp = aud:notes
      Break
  end !loop
  access:audit.restorefile(save_aud_id)
  setcursor()
  
  access:users.clearkey(use:user_code_key)
  use:user_code = job_ali:engineer
  access:users.fetch(use:user_code_key)
  engineer_temp = Clip(use:forename) & ' ' & Clip(use:surname)
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'QA FAILED NOTE'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Restocking_Note'
  END
  report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Restocking_Note',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Restocking_Note',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Restocking_Note',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Restocking_Note',1)
    SolaceViewVars('save_aud_id',save_aud_id,'Restocking_Note',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Restocking_Note',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Restocking_Note',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Restocking_Note',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Restocking_Note',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Restocking_Note',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Restocking_Note',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Restocking_Note',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Restocking_Note',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Restocking_Note',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Restocking_Note',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Restocking_Note',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Restocking_Note',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Restocking_Note',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Restocking_Note',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Restocking_Note',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Restocking_Note',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Restocking_Note',1)
    SolaceViewVars('InitialPath',InitialPath,'Restocking_Note',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Restocking_Note',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Restocking_Note',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Restocking_Note',1)
    SolaceViewVars('code_temp',code_temp,'Restocking_Note',1)
    SolaceViewVars('option_temp',option_temp,'Restocking_Note',1)
    SolaceViewVars('bar_code_string_temp',bar_code_string_temp,'Restocking_Note',1)
    SolaceViewVars('bar_code_temp',bar_code_temp,'Restocking_Note',1)
    SolaceViewVars('qa_reason_temp',qa_reason_temp,'Restocking_Note',1)
    SolaceViewVars('engineer_temp',engineer_temp,'Restocking_Note',1)


BuildCtrlQueue      Routine







