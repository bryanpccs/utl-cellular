

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD03051.INC'),ONCE        !Local module procedure declarations
                     END








Bouncer_History PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
save_audali_id       USHORT,AUTO
save_par_ali_id      USHORT,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_job_ali_id      USHORT,AUTO
tmp:ref_number       LONG
tmp:esn              STRING(20)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
save_war_ali_id      USHORT,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:printedby        STRING(60)
tmp:parts            STRING(25),DIM(15)
TextBoxLineNdx       SHORT
TextBoxHeight        LONG
TextBoxLines         LONG
TextBoxStartLine     LONG
code_temp            BYTE
option_temp          BYTE
bar_code_string_temp CSTRING(21)
bar_code_temp        CSTRING(21)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:InitialsEngineer STRING(2)
tmp:InitialsQA       STRING(2)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Charge_Type)
                       PROJECT(job_ali:Date_Completed)
                       PROJECT(job_ali:Date_Despatched)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:MSN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Mobile_Number)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:Unit_Type)
                       PROJECT(job_ali:Warranty_Charge_Type)
                       PROJECT(job_ali:date_booked)
                       JOIN(jbn_ali:RefNumberKey,job_ali:Ref_Number)
                         PROJECT(jbn_ali:Fault_Description)
                         PROJECT(jbn_ali:Invoice_Text)
                       END
                     END
report               REPORT,AT(438,1135,7521,9740),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,469,7521,1000),USE(?unnamed)
                         STRING('?PP?'),AT(7188,208,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(7031,208),USE(?String35:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Page:'),AT(6406,208),USE(?String35),TRN,FONT(,8,,FONT:bold)
                         STRING(@s3),AT(6771,208),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,3073),USE(?detailband)
                           STRING('Job Number:'),AT(208,52),USE(?String2),TRN,FONT(,12,,FONT:bold,CHARSET:ANSI)
                           STRING(@s20),AT(1573,52,2396,208),USE(bar_code_temp),LEFT,FONT('C128 High 12pt LJ3',12,,FONT:regular,CHARSET:ANSI)
                           STRING(@s8),AT(1573,240),USE(job_ali:Ref_Number),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING('Unit Details:'),AT(208,406),USE(?String2:2),TRN,FONT(,12,,FONT:bold,CHARSET:ANSI)
                           STRING('Model Number:'),AT(1573,573),USE(?String6:3),TRN,FONT(,8,,FONT:bold)
                           STRING('Booked:'),AT(4844,73),TRN,FONT(,8,,FONT:bold)
                           STRING(@d6b),AT(5854,73),USE(job_ali:date_booked),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING('Date:'),AT(4104,52),USE(?String2:4),TRN,FONT(,12,,FONT:bold,CHARSET:ANSI)
                           STRING(@D6b),AT(5865,250),USE(job_ali:Date_Completed),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@D6b),AT(5854,417),USE(job_ali:Date_Despatched),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING('Despatched:'),AT(4844,417),USE(?String6:8),TRN,FONT(,8,,FONT:bold)
                           STRING('Completed:'),AT(4844,240),USE(?String6:7),TRN,FONT(,8,,FONT:bold)
                           STRING('Reported Fault:'),AT(208,1260),USE(?String2:5),TRN,FONT(,12,,FONT:bold,CHARSET:ANSI)
                           TEXT,AT(1573,1271,5521,417),USE(jbn_ali:Fault_Description),TRN,FONT(,8,,,CHARSET:ANSI)
                           TEXT,AT(1573,1729,5521,417),USE(jbn_ali:Invoice_Text),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s16),AT(5885,573),USE(job_ali:ESN),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING('Mobile Number:'),AT(4844,885),USE(?CChargeType:2),TRN,FONT(,8,,FONT:bold)
                           STRING('Repair Details:'),AT(208,1729),USE(?String2:6),TRN,FONT(,12,,FONT:bold,CHARSET:ANSI)
                           STRING(@s16),AT(5885,729),USE(job_ali:MSN),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING('Parts Used:'),AT(208,2083),USE(?String2:3),TRN,FONT(,12,,FONT:bold,CHARSET:ANSI)
                           STRING('Chargeable Parts'),AT(208,2292),USE(?String37),TRN,FONT(,8,,FONT:underline)
                           STRING('Warranty Parts'),AT(3594,2292),USE(?String37:2),TRN,FONT(,8,,FONT:underline)
                           STRING(@s25),AT(1927,2500),USE(tmp:parts[2]),FONT(,8,,,CHARSET:ANSI)
                           STRING(@s25),AT(208,2656),USE(tmp:parts[3]),FONT(,8,,,CHARSET:ANSI)
                           STRING(@s25),AT(1927,2656),USE(tmp:parts[4]),FONT(,8,,)
                           STRING(@s25),AT(1927,2813),USE(tmp:parts[6]),FONT(,8,,,CHARSET:ANSI)
                           STRING(@s25),AT(3594,2500),USE(tmp:parts[7]),FONT(,8,,,CHARSET:ANSI)
                           STRING(@s25),AT(5260,2500),USE(tmp:parts[8]),FONT(,8,,,CHARSET:ANSI)
                           STRING(@s25),AT(208,2813),USE(tmp:parts[5]),FONT(,8,,,CHARSET:ANSI)
                           STRING(@s25),AT(5260,2656),USE(tmp:parts[10]),FONT(,8,,,CHARSET:ANSI)
                           STRING(@s25),AT(3594,2656),USE(tmp:parts[9]),FONT(,8,,,CHARSET:ANSI)
                           STRING(@s25),AT(5260,2813),USE(tmp:parts[12]),FONT(,8,,,CHARSET:ANSI)
                           STRING(@s25),AT(3594,2813),USE(tmp:parts[11]),FONT(,8,,,CHARSET:ANSI)
                           STRING(@s25),AT(208,2500),USE(tmp:parts[1]),FONT(,8,,,CHARSET:ANSI)
                           LINE,AT(208,3010,7031,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Unit Type:'),AT(1573,729),USE(?String6:4),TRN,FONT(,8,,FONT:bold)
                           STRING(@s30),AT(2667,573),USE(job_ali:Model_Number),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING('I.M.E.I. Number:'),AT(4844,573),USE(?String6:5),TRN,FONT(,8,,FONT:bold)
                           STRING('Manufacturer:'),AT(1573,417),USE(?String6:2),TRN,FONT(,8,,FONT:bold)
                           STRING(@s30),AT(2667,417),USE(job_ali:Manufacturer),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING('M.S.N.:'),AT(4844,729),TRN,FONT(,8,,FONT:bold)
                           STRING(@s30),AT(2667,729),USE(job_ali:Unit_Type),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s16),AT(5885,885),USE(job_ali:Mobile_Number),TRN,FONT(,8,,)
                           STRING(@s30),AT(2667,885),USE(job_ali:Charge_Type),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s30),AT(2667,1042),USE(job_ali:Warranty_Charge_Type),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING('QA Engineer:'),AT(6146,1042),USE(?CChargeType:4),TRN,FONT(,8,,FONT:bold)
                           STRING(@s2),AT(6958,1042),USE(tmp:InitialsQA),TRN,FONT(,8,,)
                           STRING('Engineer:'),AT(4844,1042),USE(?CChargeType:3),TRN,FONT(,8,,FONT:bold)
                           STRING(@s2),AT(5885,1042),USE(tmp:InitialsEngineer),TRN,FONT(,8,,)
                           STRING('War Charge Type:'),AT(1573,1042),USE(?WChargeType),TRN,FONT(,8,,FONT:bold)
                           STRING('Cha Charge Type:'),AT(1573,885),USE(?CChargeType),TRN,FONT(,8,,FONT:bold)
                         END
                       END
                       FORM,AT(396,479,7521,10552),USE(?unnamed:3)
                         STRING('BOUNCER HISTORY REPORT'),AT(1260,52,5000,417),USE(?string20),TRN,CENTER,FONT(,24,,FONT:bold)
                         BOX,AT(104,521,7292,9844),USE(?Box1),COLOR(COLOR:Black)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Bouncer_History')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select11',GLO:Select11)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS_ALIAS.Open
  Relate:AUDIT_ALIAS.Open
  Relate:DEFPRINT.Open
  Relate:PARTS_ALIAS.Open
  Relate:WARPARTS_ALIAS.Open
  Access:JOBNOTES_ALIAS.UseFile
  Access:USERS.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS_ALIAS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS_ALIAS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        save_job_ali_id = access:jobs_alias.savefile()
        access:jobs_alias.clearkey(job_ali:esn_key)
        job_ali:esn = glo:select2
        set(job_ali:esn_key, job_ali:esn_key)
        loop
            if access:jobs_alias.next()
                break
            end ! if
            if job_ali:esn <> glo:select2 |
                then break   ! end if
            end ! if
            ! Barcode Bit
            code_temp            = 3
            option_temp          = 0
            Bar_Code_String_Temp = job_ali:Ref_Number
            SEQUENCE2(code_temp, option_temp, Bar_code_string_temp, Bar_Code_Temp)
        
            Access:JOBNOTES_ALIAS.Clearkey(jbn_ali:RefNumberKey)
            jbn_ali:RefNumber   = job_ali:Ref_Number
            If Access:JOBNOTES_ALIAS.Tryfetch(jbn_ali:RefNumberKey) = Level:Benign
            ! Found
        
            Else! If Access:JOBNOTES_ALIAS.Tryfetch(jbn_ali:RefNumberKey) = Level:Benign
                    ! Error
                    ! Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:JOBNOTES_ALIAS.Tryfetch(jbn_ali:RefNumberKey) = Level:Benign
        
            clear(tmp:parts)
            PartNumber#     = 0
            Save_par_ali_ID = Access:PARTS_ALIAS.SaveFile()
            Access:PARTS_ALIAS.ClearKey(par_ali:Part_Number_Key)
            par_ali:Ref_Number  = job_ali:Ref_Number
            Set(par_ali:Part_Number_Key, par_ali:Part_Number_Key)
            Loop
                If Access:PARTS_ALIAS.NEXT()
                    Break
                End ! If
                If par_ali:Ref_Number  <> job_ali:Ref_Number      |
                    Then Break   ! End If
                End ! If
                PartNumber# += 1
                If PartNumber# > 6
                    Break
                End! If PartNumber# > 6
                tmp:parts[PartNumber#] = par_ali:Description
            End ! Loop
            Access:PARTS_ALIAS.RestoreFile(Save_par_ali_ID)
        
            PartNumber#     = 6
            Save_war_ali_ID = Access:WARPARTS_ALIAS.SaveFile()
            Access:WARPARTS_ALIAS.ClearKey(war_ali:Part_Number_Key)
            war_ali:Ref_Number  = job_ali:Ref_Number
            Set(war_ali:Part_Number_Key, war_ali:Part_Number_Key)
            Loop
                If Access:WARPARTS_ALIAS.NEXT()
                    Break
                End ! If
                If war_ali:Ref_Number  <>  job_ali:Ref_Number     |
                    Then Break   ! End If
                End ! If
                PartNumber# += 1
                If PartNumber# > 12
                    Break
                End! If PartNumber# > 12
                tmp:Parts[PartNumber#] = war_ali:Description
            End ! Loop
            Access:WARPARTS_ALIAS.RestoreFile(Save_war_ali_ID)
        
            ! Start - Add Engineer, and Manual QA Initials - TrkBs: 5146 (DBH: 21-02-2005)
            tmp:InitialsEngineer = ''
            tmp:InitialsQA       = ''
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code    = job_ali:Engineer
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                ! Found
                tmp:InitialsEngineer = Sub(use:Forename, 1, 1) & Sub(use:surname, 1, 1)
            Else ! If Access:USERS.Tryfetch(use:UserCodeKey) = Level:Benign
            ! Error
            End ! If Access:USERS.Tryfetch(use:UserCodeKey) = Level:Benign
        
            Save_audali_ID = Access:AUDIT_ALIAS.SaveFile()
            Access:AUDIT_ALIAS.ClearKey(audali:Ref_Number_Key)
            audali:Ref_Number = job_ali:Ref_Number
            audali:Date       = Today()
            Set(audali:Ref_Number_Key, audali:Ref_Number_Key)
            Loop
                If Access:AUDIT_ALIAS.NEXT()
                    Break
                End ! If
                If audali:Ref_Number <> job_ali:Ref_Number |
                    Or audali:Date       > Today()         |
                    Then Break   ! End If
                End ! If
                If audali:Action <> 'RAPID QA UPDATE: MANUAL QA PASSED'
                    Cycle
                End ! If audali:Action <> 'RAPID QA UPDATE: MANUAL QA PASSED'
                Access:USERS.Clearkey(use:User_Code_Key)
                use:User_Code    = audali:User
                If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    ! Found
                    tmp:InitialsQA = Sub(use:Forename, 1, 1) & Sub(use:Surname, 1, 1)
                    Break
                Else ! If Access:USERS.Tryfetch(use:UserCodeKey) = Level:Benign
                ! Error
                End ! If Access:USERS.Tryfetch(use:UserCodeKey) = Level:Benign
        
            End ! Loop
            Access:AUDIT_ALIAS.RestoreFile(Save_audali_ID)
            ! End   - Add Engineer, and Manual QA Initials - TrkBs: 5146 (DBH: 21-02-2005)
        
            SetTarget(Report)
            If job_ali:Chargeable_Job <> 'YES'
                ?CChargeType{prop:Hide}         = 1
                ?job_ali:Charge_Type{prop:Hide} = 1
            End ! If job:Chargeable_Job <> 'YES'
            If job_ali:Warranty_Job <> 'YES'
                ?WChargeType{prop:Hide}                  = 1
                ?job_ali:Warranty_Charge_Type{prop:Hide} = 1
            End ! If job:Chargeable_Job <> 'YES'
            Print(rpt:detail)
        
        end ! loop
        access:jobs_alias.restorefile(save_job_ali_id)
        
        
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS_ALIAS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDIT_ALIAS.Close
    Relate:DEFPRINT.Close
    Relate:JOBS_ALIAS.Close
    Relate:PARTS_ALIAS.Close
    Relate:WARPARTS_ALIAS.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','BOUNCER HISTORY REPORT')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'BOUNCER HISTORY REPORT'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'BOUNCER HISTORY REPORT'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Bouncer_History'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Bouncer_History',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Bouncer_History',1)
    SolaceViewVars('save_audali_id',save_audali_id,'Bouncer_History',1)
    SolaceViewVars('save_par_ali_id',save_par_ali_id,'Bouncer_History',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Bouncer_History',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Bouncer_History',1)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'Bouncer_History',1)
    SolaceViewVars('tmp:ref_number',tmp:ref_number,'Bouncer_History',1)
    SolaceViewVars('tmp:esn',tmp:esn,'Bouncer_History',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Bouncer_History',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Bouncer_History',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Bouncer_History',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Bouncer_History',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Bouncer_History',1)
    SolaceViewVars('save_war_ali_id',save_war_ali_id,'Bouncer_History',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Bouncer_History',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Bouncer_History',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Bouncer_History',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Bouncer_History',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Bouncer_History',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Bouncer_History',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Bouncer_History',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Bouncer_History',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Bouncer_History',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Bouncer_History',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Bouncer_History',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Bouncer_History',1)
    SolaceViewVars('InitialPath',InitialPath,'Bouncer_History',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Bouncer_History',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Bouncer_History',1)
    SolaceViewVars('tmp:printedby',tmp:printedby,'Bouncer_History',1)
    
      Loop SolaceDim1# = 1 to 15
        SolaceFieldName" = 'tmp:parts' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tmp:parts[SolaceDim1#],'Bouncer_History',1)
      End
    
    
    SolaceViewVars('TextBoxLineNdx',TextBoxLineNdx,'Bouncer_History',1)
    SolaceViewVars('TextBoxHeight',TextBoxHeight,'Bouncer_History',1)
    SolaceViewVars('TextBoxLines',TextBoxLines,'Bouncer_History',1)
    SolaceViewVars('TextBoxStartLine',TextBoxStartLine,'Bouncer_History',1)
    SolaceViewVars('code_temp',code_temp,'Bouncer_History',1)
    SolaceViewVars('option_temp',option_temp,'Bouncer_History',1)
    SolaceViewVars('bar_code_string_temp',bar_code_string_temp,'Bouncer_History',1)
    SolaceViewVars('bar_code_temp',bar_code_temp,'Bouncer_History',1)
    SolaceViewVars('tmp:InitialsEngineer',tmp:InitialsEngineer,'Bouncer_History',1)
    SolaceViewVars('tmp:InitialsQA',tmp:InitialsQA,'Bouncer_History',1)


BuildCtrlQueue      Routine







