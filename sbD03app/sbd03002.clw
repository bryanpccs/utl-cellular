

   MEMBER('sbd03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBD03002.INC'),ONCE        !Local module procedure declarations
                     END


Special_Delivery_Criteria PROCEDURE (f_type)          !Generated from procedure template - Window

FilesOpened          BYTE
tmp:printer          STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:summary          BYTE(0)
tmp:new_only         BYTE
tmp:StartTime        TIME
tmp:EndTime          TIME
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select1
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB1::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
Window               WINDOW('Special Delivery Report Criteria'),AT(,,220,160),FONT('Tahoma',8,,FONT:regular),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,212,124),USE(?Sheet1),SPREAD
                         TAB('Report Criteria'),USE(?Tab1)
                           PROMPT('Courier'),AT(8,20),USE(?Prompt1)
                           COMBO(@s30),AT(84,20,124,10),USE(GLO:Select1),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Despatched From'),AT(8,36),USE(?glo:select2:Prompt)
                           ENTRY(@d6b),AT(84,36,64,10),USE(GLO:Select2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           BUTTON,AT(152,36,10,10),USE(?PopCalendar),SKIP,ICON('Calenda2.ico')
                           PROMPT('To'),AT(8,52,17,11),USE(?glo:select3:Prompt)
                           ENTRY(@d6b),AT(84,52,64,10),USE(GLO:Select3),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           BUTTON,AT(152,52,10,10),USE(?PopCalendar:2),SKIP,ICON('Calenda2.ico')
                           PROMPT('Start Time'),AT(8,68),USE(?tmp:StartTime:Prompt)
                           ENTRY(@t1),AT(84,68,64,10),USE(tmp:StartTime),RIGHT,FONT(,,,FONT:bold),MSG('Start Time'),TIP('Start Time')
                           PROMPT('(HH:MM 24hr)'),AT(152,68),USE(?Prompt6)
                           PROMPT('(HH:MM 24hr)'),AT(152,84),USE(?Prompt6:2)
                           PROMPT('End Time'),AT(8,84),USE(?tmp:EndTime:Prompt)
                           ENTRY(@t1),AT(84,84,64,10),USE(tmp:EndTime),RIGHT,FONT(,,,FONT:bold),MSG('End Time'),TIP('End Time')
                           CHECK('Group Consignment Numbers'),AT(83,100),USE(tmp:summary),HIDE,MSG('Summarize Consignment Numbers'),TIP('Summarize Consignment Numbers'),VALUE('1','0')
                           CHECK('New Despatches Only'),AT(83,116),USE(tmp:new_only),VALUE('1','0')
                         END
                       END
                       PANEL,AT(4,132,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,136,56,16),USE(?OkButton),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,136,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB1                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?GLO:Select1{prop:ReadOnly} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 15066597
    Elsif ?GLO:Select1{prop:Req} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 8454143
    Else ! If ?GLO:Select1{prop:Req} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 16777215
    End ! If ?GLO:Select1{prop:Req} = True
    ?GLO:Select1{prop:Trn} = 0
    ?GLO:Select1{prop:FontStyle} = font:Bold
    ?glo:select2:Prompt{prop:FontColor} = -1
    ?glo:select2:Prompt{prop:Color} = 15066597
    If ?GLO:Select2{prop:ReadOnly} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 15066597
    Elsif ?GLO:Select2{prop:Req} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 8454143
    Else ! If ?GLO:Select2{prop:Req} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 16777215
    End ! If ?GLO:Select2{prop:Req} = True
    ?GLO:Select2{prop:Trn} = 0
    ?GLO:Select2{prop:FontStyle} = font:Bold
    ?glo:select3:Prompt{prop:FontColor} = -1
    ?glo:select3:Prompt{prop:Color} = 15066597
    If ?GLO:Select3{prop:ReadOnly} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 15066597
    Elsif ?GLO:Select3{prop:Req} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 8454143
    Else ! If ?GLO:Select3{prop:Req} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 16777215
    End ! If ?GLO:Select3{prop:Req} = True
    ?GLO:Select3{prop:Trn} = 0
    ?GLO:Select3{prop:FontStyle} = font:Bold
    ?tmp:StartTime:Prompt{prop:FontColor} = -1
    ?tmp:StartTime:Prompt{prop:Color} = 15066597
    If ?tmp:StartTime{prop:ReadOnly} = True
        ?tmp:StartTime{prop:FontColor} = 65793
        ?tmp:StartTime{prop:Color} = 15066597
    Elsif ?tmp:StartTime{prop:Req} = True
        ?tmp:StartTime{prop:FontColor} = 65793
        ?tmp:StartTime{prop:Color} = 8454143
    Else ! If ?tmp:StartTime{prop:Req} = True
        ?tmp:StartTime{prop:FontColor} = 65793
        ?tmp:StartTime{prop:Color} = 16777215
    End ! If ?tmp:StartTime{prop:Req} = True
    ?tmp:StartTime{prop:Trn} = 0
    ?tmp:StartTime{prop:FontStyle} = font:Bold
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?Prompt6:2{prop:FontColor} = -1
    ?Prompt6:2{prop:Color} = 15066597
    ?tmp:EndTime:Prompt{prop:FontColor} = -1
    ?tmp:EndTime:Prompt{prop:Color} = 15066597
    If ?tmp:EndTime{prop:ReadOnly} = True
        ?tmp:EndTime{prop:FontColor} = 65793
        ?tmp:EndTime{prop:Color} = 15066597
    Elsif ?tmp:EndTime{prop:Req} = True
        ?tmp:EndTime{prop:FontColor} = 65793
        ?tmp:EndTime{prop:Color} = 8454143
    Else ! If ?tmp:EndTime{prop:Req} = True
        ?tmp:EndTime{prop:FontColor} = 65793
        ?tmp:EndTime{prop:Color} = 16777215
    End ! If ?tmp:EndTime{prop:Req} = True
    ?tmp:EndTime{prop:Trn} = 0
    ?tmp:EndTime{prop:FontStyle} = font:Bold
    ?tmp:summary{prop:Font,3} = -1
    ?tmp:summary{prop:Color} = 15066597
    ?tmp:summary{prop:Trn} = 0
    ?tmp:new_only{prop:Font,3} = -1
    ?tmp:new_only{prop:Color} = 15066597
    ?tmp:new_only{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Special_Delivery_Criteria',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Special_Delivery_Criteria',1)
    SolaceViewVars('tmp:printer',tmp:printer,'Special_Delivery_Criteria',1)
    SolaceViewVars('tmp:summary',tmp:summary,'Special_Delivery_Criteria',1)
    SolaceViewVars('tmp:new_only',tmp:new_only,'Special_Delivery_Criteria',1)
    SolaceViewVars('tmp:StartTime',tmp:StartTime,'Special_Delivery_Criteria',1)
    SolaceViewVars('tmp:EndTime',tmp:EndTime,'Special_Delivery_Criteria',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select1;  SolaceCtrlName = '?GLO:Select1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select2:Prompt;  SolaceCtrlName = '?glo:select2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select2;  SolaceCtrlName = '?GLO:Select2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select3:Prompt;  SolaceCtrlName = '?glo:select3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select3;  SolaceCtrlName = '?GLO:Select3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartTime:Prompt;  SolaceCtrlName = '?tmp:StartTime:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartTime;  SolaceCtrlName = '?tmp:StartTime';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6:2;  SolaceCtrlName = '?Prompt6:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndTime:Prompt;  SolaceCtrlName = '?tmp:EndTime:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndTime;  SolaceCtrlName = '?tmp:EndTime';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:summary;  SolaceCtrlName = '?tmp:summary';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:new_only;  SolaceCtrlName = '?tmp:new_only';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, Window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Special_Delivery_Criteria')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Special_Delivery_Criteria')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:COURIER.Open
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  Do RecolourWindow
  ?glo:select2{Prop:Alrt,255} = MouseLeft2
  ?glo:select3{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  FDCB1.Init(GLO:Select1,?GLO:Select1,Queue:FileDropCombo.ViewPosition,FDCB1::View:FileDropCombo,Queue:FileDropCombo,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB1.Q &= Queue:FileDropCombo
  FDCB1.AddSortOrder(cou:Courier_Key)
  FDCB1.AddField(cou:Courier,FDCB1.Q.cou:Courier)
  ThisWindow.AddItem(FDCB1.WindowComponent)
  FDCB1.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Special_Delivery_Criteria',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select2 = TINCALENDARStyle1(GLO:Select2)
          Display(?glo:select2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select3 = TINCALENDARStyle1(GLO:Select3)
          Display(?glo:select3)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      error# = 0
      If glo:select1 = ''
          Select(?glo:select1)
          error# = 1
      End
      If error# = 0 And glo:select2 = ''
          Select(?glo:select2)
          error# = 1
      End
      If error# = 0 And glo:select3 = ''
          Select(?glo:select3)
          error# = 1
      End
      If error# = 0
          Case f_type
              Of 'SPECIAL DELIVERY'
                  Special_Delivery_Report(tmp:Summary,tmp:new_only,tmp:StartTime,tmp:EndTime)
              Of 'COURIER COLLECTION'
                  Courier_Collection_Report
          End!Case f_type
      
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      glo:select1 = ''
      glo:select2 = ''
      glo:select3 = ''
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Special_Delivery_Criteria')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?GLO:Select2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?GLO:Select3
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      glo:select1 = ''
      glo:select2 = Today()
      glo:select3 = Today()
      Case f_type
          Of 'SPECIAL DELIVERY'
              0{prop:text} = 'Special Delivery Report Criteria'
              ?tmp:Summary{prop:Hide} = 0
              ?tmp:new_only{Prop:Hide} = 0
          Of 'COURIER COLLECTION'
              0{prop:text} = 'Courier Collection Report Criteria'
              ?tmp:Summary{prop:Hide} = 1
              ?tmp:new_only{Prop:Hide} = 1
      End!Case f_type
      Display()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

