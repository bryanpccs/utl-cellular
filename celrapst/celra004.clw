

   MEMBER('celrapst.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA004.INC'),ONCE        !Local module procedure declarations
                     END


UpdateJobExchangeStatus PROCEDURE                     !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:status           STRING(30)
tmp:jobnumber        LONG
JobNumberQueue       QUEUE,PRE(QUE)
JobNumber            LONG
RecordNumber         LONG
                     END
Update_Text_Temp     STRING(100)
Proceed              BYTE
Change_Charges       BYTE
tmp:Charge_Type      STRING(30)
tmp:Warranty_Type    STRING(30)
window               WINDOW('Rapid Status Update'),AT(,,355,147),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,MDI,IMM
                       SHEET,AT(4,4,220,112),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Rapid EXCHANGE Status Update'),AT(8,8),USE(?RapidExchangeStatusUpdate),FONT('Tahoma',8,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Exchange Status'),AT(8,28),USE(?tmp:status:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,28,124,10),USE(tmp:status),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Exchange Status'),TIP('Exchange Status'),UPR
                           BUTTON,AT(212,28,10,10),USE(?LookupExchangeStatus),SKIP,ICON('List3.ico')
                           STRING('Change Charge Types'),AT(8,44),USE(?String2)
                           CHECK,AT(83,44),USE(Change_Charges),VALUE('1','0')
                           PROMPT('Charge Type'),AT(9,60),USE(?tmp:Charge_Type:Prompt),TRN
                           ENTRY(@s30),AT(84,60,124,10),USE(tmp:Charge_Type),DISABLE,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(212,60,10,10),USE(?CallLookup),SKIP,DISABLE,ICON('List3.ico')
                           PROMPT('Warranty Charge Type'),AT(8,76),USE(?tmp:Warranty_Type:Prompt),TRN
                           ENTRY(@s30),AT(84,76,124,10),USE(tmp:Warranty_Type),DISABLE,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(212,76,10,10),USE(?CallLookup:2),SKIP,DISABLE,ICON('List3.ico')
                           LINE,AT(6,92,215,0),USE(?Line1),COLOR(COLOR:Black)
                           PROMPT('Job Number'),AT(8,100),USE(?tmp:jobnumber:Prompt),TRN
                           ENTRY(@s8),AT(84,100,64,10),USE(tmp:jobnumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Job Number'),TIP('Job Number'),UPR
                         END
                       END
                       SHEET,AT(228,4,124,112),USE(?Sheet2),SPREAD
                         TAB('Jobs Successfully Updated'),USE(?Tab2)
                           LIST,AT(232,20,116,92),USE(?List1),VSCROLL,FORMAT('32L(2)|M~Job Number~@s8@'),FROM(JobNumberQueue)
                         END
                       END
                       PANEL,AT(4,120,348,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Finish'),AT(292,124,56,16),USE(?Close),LEFT,ICON('thumbs.gif')
                       STRING(@s100),AT(8,124,280,12),USE(Update_Text_Temp),FONT('Tahoma',10,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:status                Like(tmp:status)
look:tmp:Charge_Type                Like(tmp:Charge_Type)
look:tmp:Warranty_Type                Like(tmp:Warranty_Type)
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?RapidExchangeStatusUpdate{prop:FontColor} = -1
    ?RapidExchangeStatusUpdate{prop:Color} = 15066597
    ?tmp:status:Prompt{prop:FontColor} = -1
    ?tmp:status:Prompt{prop:Color} = 15066597
    If ?tmp:status{prop:ReadOnly} = True
        ?tmp:status{prop:FontColor} = 65793
        ?tmp:status{prop:Color} = 15066597
    Elsif ?tmp:status{prop:Req} = True
        ?tmp:status{prop:FontColor} = 65793
        ?tmp:status{prop:Color} = 8454143
    Else ! If ?tmp:status{prop:Req} = True
        ?tmp:status{prop:FontColor} = 65793
        ?tmp:status{prop:Color} = 16777215
    End ! If ?tmp:status{prop:Req} = True
    ?tmp:status{prop:Trn} = 0
    ?tmp:status{prop:FontStyle} = font:Bold
    ?String2{prop:FontColor} = -1
    ?String2{prop:Color} = 15066597
    ?Change_Charges{prop:Font,3} = -1
    ?Change_Charges{prop:Color} = 15066597
    ?Change_Charges{prop:Trn} = 0
    ?tmp:Charge_Type:Prompt{prop:FontColor} = -1
    ?tmp:Charge_Type:Prompt{prop:Color} = 15066597
    If ?tmp:Charge_Type{prop:ReadOnly} = True
        ?tmp:Charge_Type{prop:FontColor} = 65793
        ?tmp:Charge_Type{prop:Color} = 15066597
    Elsif ?tmp:Charge_Type{prop:Req} = True
        ?tmp:Charge_Type{prop:FontColor} = 65793
        ?tmp:Charge_Type{prop:Color} = 8454143
    Else ! If ?tmp:Charge_Type{prop:Req} = True
        ?tmp:Charge_Type{prop:FontColor} = 65793
        ?tmp:Charge_Type{prop:Color} = 16777215
    End ! If ?tmp:Charge_Type{prop:Req} = True
    ?tmp:Charge_Type{prop:Trn} = 0
    ?tmp:Charge_Type{prop:FontStyle} = font:Bold
    ?tmp:Warranty_Type:Prompt{prop:FontColor} = -1
    ?tmp:Warranty_Type:Prompt{prop:Color} = 15066597
    If ?tmp:Warranty_Type{prop:ReadOnly} = True
        ?tmp:Warranty_Type{prop:FontColor} = 65793
        ?tmp:Warranty_Type{prop:Color} = 15066597
    Elsif ?tmp:Warranty_Type{prop:Req} = True
        ?tmp:Warranty_Type{prop:FontColor} = 65793
        ?tmp:Warranty_Type{prop:Color} = 8454143
    Else ! If ?tmp:Warranty_Type{prop:Req} = True
        ?tmp:Warranty_Type{prop:FontColor} = 65793
        ?tmp:Warranty_Type{prop:Color} = 16777215
    End ! If ?tmp:Warranty_Type{prop:Req} = True
    ?tmp:Warranty_Type{prop:Trn} = 0
    ?tmp:Warranty_Type{prop:FontStyle} = font:Bold
    ?tmp:jobnumber:Prompt{prop:FontColor} = -1
    ?tmp:jobnumber:Prompt{prop:Color} = 15066597
    If ?tmp:jobnumber{prop:ReadOnly} = True
        ?tmp:jobnumber{prop:FontColor} = 65793
        ?tmp:jobnumber{prop:Color} = 15066597
    Elsif ?tmp:jobnumber{prop:Req} = True
        ?tmp:jobnumber{prop:FontColor} = 65793
        ?tmp:jobnumber{prop:Color} = 8454143
    Else ! If ?tmp:jobnumber{prop:Req} = True
        ?tmp:jobnumber{prop:FontColor} = 65793
        ?tmp:jobnumber{prop:Color} = 16777215
    End ! If ?tmp:jobnumber{prop:Req} = True
    ?tmp:jobnumber{prop:Trn} = 0
    ?tmp:jobnumber{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?List1{prop:FontColor} = 65793
    ?List1{prop:Color}= 16777215
    ?List1{prop:Color,2} = 16777215
    ?List1{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597

    ?Update_Text_Temp{prop:FontColor} = -1
    ?Update_Text_Temp{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateJobExchangeStatus',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:status',tmp:status,'UpdateJobExchangeStatus',1)
    SolaceViewVars('tmp:jobnumber',tmp:jobnumber,'UpdateJobExchangeStatus',1)
    SolaceViewVars('JobNumberQueue:JobNumber',JobNumberQueue:JobNumber,'UpdateJobExchangeStatus',1)
    SolaceViewVars('JobNumberQueue:RecordNumber',JobNumberQueue:RecordNumber,'UpdateJobExchangeStatus',1)
    SolaceViewVars('Update_Text_Temp',Update_Text_Temp,'UpdateJobExchangeStatus',1)
    SolaceViewVars('Proceed',Proceed,'UpdateJobExchangeStatus',1)
    SolaceViewVars('Change_Charges',Change_Charges,'UpdateJobExchangeStatus',1)
    SolaceViewVars('tmp:Charge_Type',tmp:Charge_Type,'UpdateJobExchangeStatus',1)
    SolaceViewVars('tmp:Warranty_Type',tmp:Warranty_Type,'UpdateJobExchangeStatus',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RapidExchangeStatusUpdate;  SolaceCtrlName = '?RapidExchangeStatusUpdate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:status:Prompt;  SolaceCtrlName = '?tmp:status:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:status;  SolaceCtrlName = '?tmp:status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupExchangeStatus;  SolaceCtrlName = '?LookupExchangeStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String2;  SolaceCtrlName = '?String2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change_Charges;  SolaceCtrlName = '?Change_Charges';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Charge_Type:Prompt;  SolaceCtrlName = '?tmp:Charge_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Charge_Type;  SolaceCtrlName = '?tmp:Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CallLookup;  SolaceCtrlName = '?CallLookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Warranty_Type:Prompt;  SolaceCtrlName = '?tmp:Warranty_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Warranty_Type;  SolaceCtrlName = '?tmp:Warranty_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CallLookup:2;  SolaceCtrlName = '?CallLookup:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1;  SolaceCtrlName = '?Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:jobnumber:Prompt;  SolaceCtrlName = '?tmp:jobnumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:jobnumber;  SolaceCtrlName = '?tmp:jobnumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List1;  SolaceCtrlName = '?List1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Update_Text_Temp;  SolaceCtrlName = '?Update_Text_Temp';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateJobExchangeStatus')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateJobExchangeStatus')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?RapidExchangeStatusUpdate
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:STAHEAD.Open
  Access:STATUS.UseFile
  Access:JOBS.UseFile
  Access:CHARTYPE.UseFile
  Access:JOBSTAGE.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List1{prop:vcr} = TRUE
  IF ?tmp:status{Prop:Tip} AND ~?LookupExchangeStatus{Prop:Tip}
     ?LookupExchangeStatus{Prop:Tip} = 'Select ' & ?tmp:status{Prop:Tip}
  END
  IF ?tmp:status{Prop:Msg} AND ~?LookupExchangeStatus{Prop:Msg}
     ?LookupExchangeStatus{Prop:Msg} = 'Select ' & ?tmp:status{Prop:Msg}
  END
  IF ?tmp:Charge_Type{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?tmp:Charge_Type{Prop:Tip}
  END
  IF ?tmp:Charge_Type{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?tmp:Charge_Type{Prop:Msg}
  END
  IF ?tmp:Warranty_Type{Prop:Tip} AND ~?CallLookup:2{Prop:Tip}
     ?CallLookup:2{Prop:Tip} = 'Select ' & ?tmp:Warranty_Type{Prop:Tip}
  END
  IF ?tmp:Warranty_Type{Prop:Msg} AND ~?CallLookup:2{Prop:Msg}
     ?CallLookup:2{Prop:Msg} = 'Select ' & ?tmp:Warranty_Type{Prop:Msg}
  END
  IF ?Change_Charges{Prop:Checked} = True
    ENABLE(?tmp:Charge_Type)
    ENABLE(?tmp:Warranty_Type)
    ENABLE(?CallLookup)
    ENABLE(?CallLookup:2)
  END
  IF ?Change_Charges{Prop:Checked} = False
    DISABLE(?tmp:Charge_Type)
    DISABLE(?tmp:Warranty_Type)
    DISABLE(?CallLookup)
    DISABLE(?CallLookup:2)
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:STAHEAD.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateJobExchangeStatus',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      SelectStatus
      Browse_Warranty_Charge_Types
      Browse_Warranty_Charge_Types
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:status
      IF tmp:status OR ?tmp:status{Prop:Req}
        sts:Status = tmp:status
        sts:Exchange = 'YES'
        GLO:Select1 = 'EXC'
        !Save Lookup Field Incase Of error
        look:tmp:status        = tmp:status
        IF Access:STATUS.TryFetch(sts:ExchangeKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:status = sts:Status
          ELSE
            CLEAR(sts:Exchange)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            tmp:status = look:tmp:status
            SELECT(?tmp:status)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupExchangeStatus
      ThisWindow.Update
      sts:Status = tmp:status
      sts:Exchange = 'YES'
      GLO:Select1 = 'EXC'
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:status = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:status)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:status)
    OF ?Change_Charges
      IF ?Change_Charges{Prop:Checked} = True
        ENABLE(?tmp:Charge_Type)
        ENABLE(?tmp:Warranty_Type)
        ENABLE(?CallLookup)
        ENABLE(?CallLookup:2)
      END
      IF ?Change_Charges{Prop:Checked} = False
        DISABLE(?tmp:Charge_Type)
        DISABLE(?tmp:Warranty_Type)
        DISABLE(?CallLookup)
        DISABLE(?CallLookup:2)
      END
      ThisWindow.Reset
    OF ?tmp:Charge_Type
      IF tmp:Charge_Type OR ?tmp:Charge_Type{Prop:Req}
        cha:Charge_Type = tmp:Charge_Type
        GLO:Select1 = 'NO'
        cha:Warranty = 'NO'
        !Save Lookup Field Incase Of error
        look:tmp:Charge_Type        = tmp:Charge_Type
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:Charge_Type = cha:Charge_Type
          ELSE
            CLEAR(GLO:Select1)
            CLEAR(cha:Warranty)
            !Restore Lookup On Error
            tmp:Charge_Type = look:tmp:Charge_Type
            SELECT(?tmp:Charge_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      cha:Charge_Type = tmp:Charge_Type
      GLO:Select1 = 'NO'
      cha:Warranty = 'NO'
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:Charge_Type = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?tmp:Charge_Type)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Charge_Type)
    OF ?tmp:Warranty_Type
      IF tmp:Warranty_Type OR ?tmp:Warranty_Type{Prop:Req}
        cha:Charge_Type = tmp:Warranty_Type
        GLO:Select1 = 'YES'
        cha:Warranty = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:Warranty_Type        = tmp:Warranty_Type
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:Warranty_Type = cha:Charge_Type
          ELSE
            CLEAR(GLO:Select1)
            CLEAR(cha:Warranty)
            !Restore Lookup On Error
            tmp:Warranty_Type = look:tmp:Warranty_Type
            SELECT(?tmp:Warranty_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:2
      ThisWindow.Update
      cha:Charge_Type = tmp:Warranty_Type
      GLO:Select1 = 'YES'
      cha:Warranty = 'YES'
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:Warranty_Type = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?tmp:Warranty_Type)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Warranty_Type)
    OF ?tmp:jobnumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:jobnumber, Accepted)
      update_Text_temp = ''
      IF tmp:status = ''
          Case MessageEx('You must select a New Status.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!IF tmp:status = ''
          Proceed = TRUE
          !Added By Neil 21/08/01
          access:jobs.clearkey(job:ref_number_key)
          job:ref_number = tmp:JobNumber
          if access:jobs.fetch(job:ref_number_key) = Level:Benign
            IF job:Exchange_Unit_Number <> 0
              Case MessageEx('An exchange unit has been allocated to this job. Do you wish to continue to change the status?','ServiceBase 2000',|
                       'Styles\warn.ico','|&Yes|&No',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                Of 1 ! &Yes Button
                  Proceed = TRUE
                Of 2 ! &No Button
                  Proceed = FALSE
              End!Case MessageEx
            END
          END
          IF Proceed = TRUE
            !Added By Neil 15/08/01!
            SORT(JobNumberQueue,-QUE:JobNumber)
            JobNumberQueue.QUE:JobNumber = tmp:JobNumber
            GET(JobNumberQueue,JobNumberQueue.QUE:JobNumber)
            IF ERROR()
              access:jobs.clearkey(job:ref_number_key)
              job:ref_number = tmp:JobNumber
              if access:jobs.fetch(job:ref_number_key) = Level:Benign
                  error# = 0
      
                  If error# = 0
      
                      get(audit,0)
                      if access:audit.primerecord() = level:benign
                          aud:notes         = 'PREVIOUS STATUS: ' & Clip(job:exchange_status) & '<13,10>NEW STATUS: ' & Clip(tmp:status)
                          aud:ref_number    = job:ref_number
                          aud:date          = today()
                          aud:time          = clock()
                          access:users.clearkey(use:password_key)
                          use:password =glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:Type    = 'EXC'
                          aud:action        = 'RAPID EXCHANGE STATUS UPDATE: ' & Clip(tmp:status)
                          access:audit.insert()
                      end!�if access:audit.primerecord() = level:benign
                      GetStatus(Sub(tmp:Status,1,3),1,'EXC')
                      
                      IF Change_Charges = TRUE
                        error1# = 0
                        If tmp:Charge_Type <> job:Charge_Type
                            If tmp:Charge_Type <> ''
                                Case MessageEx('Are you sure you want to change the Chargable Charge Type?','ServiceBase 2000',|
                                               'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                    Of 1 ! &Yes Button
                                    Of 2 ! &No Button
                                        error1# = 1
                                End!Case MessageEx
                            End!If tmp:chargetype <> ''
                            If error1# = 0
                                If CheckPricing('C')
                                    error1# = 1
                                End!If CheckPricing('C')
                            End!If error# = 0
      
                            If error1# = 0
                                job:Charge_Type = tmp:Charge_Type
                            Else!If error# = 0
                                tmp:Charge_Type  = job:Charge_Type
                            End!If error# = 0
      
                        End!If tmp:chargetype   <> job:Charge_Type
                        Display()
      
                        error2# = 0
                        If tmp:Warranty_Type   <> job:warranty_Charge_Type
                            If tmp:Warranty_Type <> ''
                                Case MessageEx('Are you sure you want to change the Warranty Charge Type?','ServiceBase 2000',|
                                               'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                    Of 1 ! &Yes Button
                                    Of 2 ! &No Button
                                        error2# = 1
                                End!Case MessageEx
                            End!If tmp:chargetype <> ''
                            If error2# = 0
                                If CheckPricing('W')
                                    error2# = 1
                                End!If CheckPricing('C')
                            End!If error# = 0
      
                            If error2# = 0
                                job:Warranty_Charge_Type = tmp:Warranty_Type
                            Else!If error# = 0
                                tmp:Warranty_Type  = job:Warranty_Charge_Type
                            End!If error# = 0
      
                        End!If tmp:chargetype   <> job:Charge_Type
                      END
                      Display()
                      access:jobs.update()
                      que:jobnumber = tmp:JobNumber
                      que:recordnumber += 1
                      Add(JobNumberQueue)
                      Sort(JobNumberQueue,-que:recordnumber)
      
                      update_text_temp = 'Job Number: ' & Clip(tmp:JobNumber) & ' Updated Successfully'
                      ?update_text_temp{prop:fontcolor} = color:navy
                      tmp:JobNumber = ''
                      Display()
                      beep(beep:systemasterisk)
                    End!If error# = 0
                  Select(?tmp:JobNumber)
              Else!if access:jobs.fetch(job:ref_number_key) = Level:Benign
                  Case MessageEx('Unable To Find Selected Job.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,250) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  Select(?tmp:JobNumber)
                  update_text_temp = 'Job Number: ' & Clip(tmp:JobNumber) & ' Update Failed'
                  ?update_text_temp{prop:fontcolor} = color:red
              end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
            ELSE
              !Error, already done this one!
              Case MessageEx('This job has already been processed. An Exchange Status has already been allocated during this session.'&|
                   '<13,10>','ServiceBase 2000',|
                   'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0)
                Of 1 ! &OK Button
              End!Case MessageEx
            END
          END
      End!IF tmp:status = ''
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:jobnumber, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateJobExchangeStatus')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

