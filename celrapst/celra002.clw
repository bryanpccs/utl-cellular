

   MEMBER('celrapst.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA002.INC'),ONCE        !Local module procedure declarations
                     END


Update_Jobs_Status PROCEDURE                          !Generated from procedure template - Window

FilesOpened          BYTE
job_queue_temp       QUEUE,PRE(jobque)
Job_Number           LONG
record_number        LONG
                     END
job_number_temp      REAL
engineer_temp        STRING(3)
engineer_name_temp   STRING(30)
old_status_temp      STRING(30)
Location_temp        STRING(30)
update_text_temp     STRING(100)
tmp:status           STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:SkillLevel       LONG
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:status
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB3::View:FileDropCombo VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                     END
window               WINDOW('Rapid Status Update / Job Allocation'),AT(,,347,148),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE,MDI
                       SHEET,AT(4,4,212,112),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Defaults'),USE(?Tab1)
                           PROMPT('Rapid JOB Status Update / Job Allocation'),AT(8,8,200,8),USE(?Prompt5),FONT('Tahoma',8,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('New Status'),AT(8,20),USE(?Prompt4)
                           COMBO(@s30),AT(84,20,124,10),USE(tmp:status),IMM,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Job Skill Level'),AT(8,36),USE(?tmp:SkillLevel:Prompt),TRN
                           SPIN(@n8),AT(84,36,64,10),USE(tmp:SkillLevel),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Engineer Skill Level'),TIP('Engineer Skill Level'),UPR,RANGE(1,10),STEP(1)
                           PROMPT('Engineer'),AT(8,52),USE(?tmp:SkillLevel:Prompt:2),TRN
                           BUTTON,AT(68,52,10,10),USE(?Lookup_Engineer),LEFT,ICON('list3.ico')
                           ENTRY(@s30),AT(84,52,124,10),USE(engineer_name_temp),SKIP,FONT(,,,FONT:bold),UPR,READONLY
                           PROMPT('Location'),AT(8,68),USE(?Location_Temp:Prompt),TRN
                           BUTTON,AT(68,68,10,10),USE(?Lookup_Location),ICON('list3.ico')
                           ENTRY(@s30),AT(84,68,124,10),USE(Location_temp),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           LINE,AT(15,84,189,0),USE(?Line1),COLOR(COLOR:Black)
                           PROMPT('Select Job Number'),AT(8,92),USE(?Prompt1)
                           ENTRY(@s9),AT(84,92,64,10),USE(job_number_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON('Import Jobs'),AT(152,88,56,16),USE(?ImportJobsButton),LEFT,ICON('DISK.GIF')
                         END
                       END
                       SHEET,AT(220,4,124,112),USE(?Sheet2),SPREAD
                         TAB('Jobs Successfully Updated'),USE(?Tab2)
                           LIST,AT(224,20,116,92),USE(?List1),VSCROLL,FORMAT('32L(2)|M~Job Number~@s8@'),FROM(job_queue_temp)
                         END
                       END
                       STRING(@s100),AT(8,124,268,12),USE(update_text_temp),FONT(,10,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                       BUTTON('&Finish [ESC]'),AT(284,124,56,16),USE(?Finish),LEFT,ICON('thumbs.gif')
                       PANEL,AT(4,120,340,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB3                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FileLookup6          SelectFileClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
ImportFile      FILE,DRIVER('BASIC'),PRE(imp),NAME(filename3),CREATE,BINDABLE,THREAD
Record              Record
JobNumber      String(8)
                    End
                End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    If ?tmp:status{prop:ReadOnly} = True
        ?tmp:status{prop:FontColor} = 65793
        ?tmp:status{prop:Color} = 15066597
    Elsif ?tmp:status{prop:Req} = True
        ?tmp:status{prop:FontColor} = 65793
        ?tmp:status{prop:Color} = 8454143
    Else ! If ?tmp:status{prop:Req} = True
        ?tmp:status{prop:FontColor} = 65793
        ?tmp:status{prop:Color} = 16777215
    End ! If ?tmp:status{prop:Req} = True
    ?tmp:status{prop:Trn} = 0
    ?tmp:status{prop:FontStyle} = font:Bold
    ?tmp:SkillLevel:Prompt{prop:FontColor} = -1
    ?tmp:SkillLevel:Prompt{prop:Color} = 15066597
    If ?tmp:SkillLevel{prop:ReadOnly} = True
        ?tmp:SkillLevel{prop:FontColor} = 65793
        ?tmp:SkillLevel{prop:Color} = 15066597
    Elsif ?tmp:SkillLevel{prop:Req} = True
        ?tmp:SkillLevel{prop:FontColor} = 65793
        ?tmp:SkillLevel{prop:Color} = 8454143
    Else ! If ?tmp:SkillLevel{prop:Req} = True
        ?tmp:SkillLevel{prop:FontColor} = 65793
        ?tmp:SkillLevel{prop:Color} = 16777215
    End ! If ?tmp:SkillLevel{prop:Req} = True
    ?tmp:SkillLevel{prop:Trn} = 0
    ?tmp:SkillLevel{prop:FontStyle} = font:Bold
    ?tmp:SkillLevel:Prompt:2{prop:FontColor} = -1
    ?tmp:SkillLevel:Prompt:2{prop:Color} = 15066597
    If ?engineer_name_temp{prop:ReadOnly} = True
        ?engineer_name_temp{prop:FontColor} = 65793
        ?engineer_name_temp{prop:Color} = 15066597
    Elsif ?engineer_name_temp{prop:Req} = True
        ?engineer_name_temp{prop:FontColor} = 65793
        ?engineer_name_temp{prop:Color} = 8454143
    Else ! If ?engineer_name_temp{prop:Req} = True
        ?engineer_name_temp{prop:FontColor} = 65793
        ?engineer_name_temp{prop:Color} = 16777215
    End ! If ?engineer_name_temp{prop:Req} = True
    ?engineer_name_temp{prop:Trn} = 0
    ?engineer_name_temp{prop:FontStyle} = font:Bold
    ?Location_Temp:Prompt{prop:FontColor} = -1
    ?Location_Temp:Prompt{prop:Color} = 15066597
    If ?Location_temp{prop:ReadOnly} = True
        ?Location_temp{prop:FontColor} = 65793
        ?Location_temp{prop:Color} = 15066597
    Elsif ?Location_temp{prop:Req} = True
        ?Location_temp{prop:FontColor} = 65793
        ?Location_temp{prop:Color} = 8454143
    Else ! If ?Location_temp{prop:Req} = True
        ?Location_temp{prop:FontColor} = 65793
        ?Location_temp{prop:Color} = 16777215
    End ! If ?Location_temp{prop:Req} = True
    ?Location_temp{prop:Trn} = 0
    ?Location_temp{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?job_number_temp{prop:ReadOnly} = True
        ?job_number_temp{prop:FontColor} = 65793
        ?job_number_temp{prop:Color} = 15066597
    Elsif ?job_number_temp{prop:Req} = True
        ?job_number_temp{prop:FontColor} = 65793
        ?job_number_temp{prop:Color} = 8454143
    Else ! If ?job_number_temp{prop:Req} = True
        ?job_number_temp{prop:FontColor} = 65793
        ?job_number_temp{prop:Color} = 16777215
    End ! If ?job_number_temp{prop:Req} = True
    ?job_number_temp{prop:Trn} = 0
    ?job_number_temp{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?List1{prop:FontColor} = 65793
    ?List1{prop:Color}= 16777215
    ?List1{prop:Color,2} = 16777215
    ?List1{prop:Color,3} = 12937777
    ?update_text_temp{prop:FontColor} = -1
    ?update_text_temp{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
show_old_status     Routine
    access:jobs.clearkey(job:ref_number_key)
    job:ref_number = job_number_temp
    if access:jobs.fetch(job:ref_number_key) = Level:Benign
        old_status_temp = job:current_status
    Else
        old_status_temp = ''
    end

show_engineer       Routine
    access:users.clearkey(use:user_code_key)
    use:user_code = engineer_temp
    if access:users.fetch(use:user_code_key) = Level:Benign
        engineer_name_temp = Clip(use:forename) & ' ' & Clip(use:surname)
    Else!if access:users.fetch(use:user_code_key) = Level:Benign
        engineer_name_temp = ''
    end!!if access:users.fetch(use:user_code_key) = Level:Benign
    Display(?engineer_name_temp)


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Jobs_Status',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_Jobs_Status',1)
    SolaceViewVars('job_queue_temp:Job_Number',job_queue_temp:Job_Number,'Update_Jobs_Status',1)
    SolaceViewVars('job_queue_temp:record_number',job_queue_temp:record_number,'Update_Jobs_Status',1)
    SolaceViewVars('job_number_temp',job_number_temp,'Update_Jobs_Status',1)
    SolaceViewVars('engineer_temp',engineer_temp,'Update_Jobs_Status',1)
    SolaceViewVars('engineer_name_temp',engineer_name_temp,'Update_Jobs_Status',1)
    SolaceViewVars('old_status_temp',old_status_temp,'Update_Jobs_Status',1)
    SolaceViewVars('Location_temp',Location_temp,'Update_Jobs_Status',1)
    SolaceViewVars('update_text_temp',update_text_temp,'Update_Jobs_Status',1)
    SolaceViewVars('tmp:status',tmp:status,'Update_Jobs_Status',1)
    SolaceViewVars('tmp:SkillLevel',tmp:SkillLevel,'Update_Jobs_Status',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:status;  SolaceCtrlName = '?tmp:status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SkillLevel:Prompt;  SolaceCtrlName = '?tmp:SkillLevel:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SkillLevel;  SolaceCtrlName = '?tmp:SkillLevel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SkillLevel:Prompt:2;  SolaceCtrlName = '?tmp:SkillLevel:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Engineer;  SolaceCtrlName = '?Lookup_Engineer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?engineer_name_temp;  SolaceCtrlName = '?engineer_name_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Location_Temp:Prompt;  SolaceCtrlName = '?Location_Temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Location;  SolaceCtrlName = '?Lookup_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Location_temp;  SolaceCtrlName = '?Location_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1;  SolaceCtrlName = '?Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_number_temp;  SolaceCtrlName = '?job_number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ImportJobsButton;  SolaceCtrlName = '?ImportJobsButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List1;  SolaceCtrlName = '?List1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?update_text_temp;  SolaceCtrlName = '?update_text_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Finish;  SolaceCtrlName = '?Finish';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Update_Jobs_Status')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Jobs_Status')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt5
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Clear(job_queue_temp)
  Free(job_queue_temp)
  Relate:AUDIT.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:STAHEAD.Open
  Access:JOBS.UseFile
  Access:JOBSTAGE.UseFile
  Access:USERS.UseFile
  Access:LOCINTER.UseFile
  Access:AUDSTATS.UseFile
  Access:JOBSE.UseFile
  Access:JOBSENG.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  If def:HideLocation
      ?Location_Temp{prop:Hide} = 1
      ?Location_Temp:Prompt{prop:Hide} = 1
      ?Lookup_Location{prop:Hide} = 1
  End !def:HideLocation
  If de2:UserSkillLevel
      ?tmp:SkillLevel{prop:Req} = 1
  End !de2:UserSkillLevel
  
  If GETINI('HIDE','SkillLevel',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      ?tmp:SkillLevel{prop:Hide} = 1
      ?tmp:SkillLevel:Prompt{prop:Hide} = 1
  End !GETINI('HIDE','SkillLevel',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  
  Do RecolourWindow
  ?List1{prop:vcr} = TRUE
  FDCB3.Init(tmp:status,?tmp:status,Queue:FileDropCombo.ViewPosition,FDCB3::View:FileDropCombo,Queue:FileDropCombo,Relate:STATUS,ThisWindow,GlobalErrors,0,1,0)
  FDCB3.Q &= Queue:FileDropCombo
  FDCB3.AddSortOrder(sts:Status_Key)
  FDCB3.AddField(sts:Status,FDCB3.Q.sts:Status)
  FDCB3.AddField(sts:Ref_Number,FDCB3.Q.sts:Ref_Number)
  ThisWindow.AddItem(FDCB3.WindowComponent)
  FDCB3.DefaultFill = 0
  FileLookup6.Init
  FileLookup6.Flags=BOR(FileLookup6.Flags,FILE:LongName)
  FileLookup6.SetMask('CSV Files','*.csv')
  FileLookup6.WindowTitle='Jobs Import File'
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:STAHEAD.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Jobs_Status',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Lookup_Engineer
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Engineer, Accepted)
      GlobalRequest   = SelectRecord
      If ?tmp:SkillLevel{prop:Req}
          If tmp:SKillLevel = 0
              Case MessageEx('You must select a Skill Level before you can select an Engineer.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              Select(?tmp:SkillLevel)
          Else !If tmp:SKillLevel = 0
              PickEngineersSkillLevel(tmp:SkillLevel)
          End !If tmp:SKillLevel = 0
          
      Else !?tmp:SkillLevel{prop:Req}
          PickEngineers
      End !?tmp:SkillLevel{prop:Req}
      
      case globalresponse
          of requestcompleted
              engineer_temp = use:user_code  
              select(?+2)
          of requestcancelled
              engineer_temp = ''
              select(?-1)
      end!case globalreponse
      Do show_engineer
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Engineer, Accepted)
    OF ?Lookup_Location
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Available_Locations
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Location, Accepted)
      case globalresponse
          of requestcompleted
              location_temp = loi:location  
              select(?+2)
          of requestcancelled
      !        location_temp = ''
              select(?-1)
      end!case globalreponse
      display(?location_temp)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Location, Accepted)
    OF ?Location_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Location_temp, Accepted)
      ! Amend Location Levels
      fetch_error# = 0
      access:locinter.clearkey(loi:location_available_key)
      loi:location_available = 'YES'
      loi:location           = location_temp
      if access:locinter.fetch(loi:location_available_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_available_locations
          if globalresponse = requestcompleted
              location_temp = loi:location
          Else!if globalresponse = requestcompleted
              location_temp = ''
          End!if globalresponse = requestcompleted
      End!if access:locinter.fetch(loi:location_available_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Location_temp, Accepted)
    OF ?job_number_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job_number_temp, Accepted)
    update_Text_temp = ''
    IF tmp:status = ''
        Case MessageEx('You must select a New Status.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            Of 1 ! &OK Button
        End!Case MessageEx
    Else!IF tmp:status = ''

        access:jobs.clearkey(job:ref_number_key)
        job:ref_number = job_number_temp
        if access:jobs.fetch(job:ref_number_key) = Level:Benign
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                !Found

            Else!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                !Error
                If Access:JOBSE.PrimeRecord() = Level:Benign
                    jobe:RefNumber  = job:Ref_Number
                    If Access:JOBSE.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:JOBSE.TryInsert() = Level:Benign
                        !Insert Failed
                    End!If Access:JOBSE.TryInsert() = Level:Benign
                End !If Access:JOBSE.PrimeRecord() = Level:Benign
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
            error# = 0
            If job:workshop <> 'YES'
                If engineer_temp <> ''
                    Case MessageEx('This job is not in the Workshop. <13,10><13,10>You cannot assign an Engineer to it.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    error# = 1
                End!If engineer_temp <> ''
                If location_temp <> ''
                    Case MessageEx('This job is not in the Workshop. <13,10><13,10>You cannot assign a Location to it.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    error# = 1
                End!If location_temp <> ''

            End!If job:workshop <> 'YES'

            If error# = 0
                If job:date_completed <> ''
                    Case MessageEx('The selected job has been completed. Are you sure you want to continue?','ServiceBase 2000',|
                                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                        Of 1 ! &Yes Button
                        Of 2 ! &No Button
                            error# = 1
                    End!Case MessageEx
                End!If job:date_completed <> ''
            End!If error# = 0
            If error# = 0

                get(audit,0)
                if access:audit.primerecord() = level:benign
                    aud:notes         = 'PREVIOUS STATUS: ' & Clip(job:current_status) & '<13,10>NEW STATUS: ' & Clip(tmp:status)
                    If engineer_temp <> '' and job:workshop = 'YES'
                        Access:USERS.Clearkey(use:User_Code_Key)
                        use:User_Code   = Engineer_Temp
                        If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                            !Found

                        Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                        End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                        
                        aud:notes = Clip(aud:notes) & '<13,10>PREVIOUS ENGINEER: ' & Clip(job:engineer) & '<13,10>NEW ENGINEER: ' & Clip(engineer_temp) &|
                                                        '<13,10>SKILL LEVEL: ' & use:SkillLevel
                    End!If engineer_temp <> ''
                    If location_temp <> '' and job:workshop = 'YES'
                        aud:notes = Clip(aud:notes) & '<13,10>PREVIOUS LOCATION: ' & Clip(job:location) & '<13,10>NEW LOCATION: ' & Clip(location_temp)
                    End!If location_temp <> ''
                    aud:ref_number    = job:ref_number
                    aud:date          = today()
                    aud:time          = clock()
                    access:users.clearkey(use:password_key)
                    use:password =glo:password
                    access:users.fetch(use:password_key)
                    aud:user = use:user_code
                    aud:action        = 'RAPID STATUS UPDATE: ' & Clip(tmp:status)
                    access:audit.insert()
                end!�if access:audit.primerecord() = level:
?   Message('Before GetStatus','Debug Message', Icon:Hand)
                GetStatus(Sub(tmp:Status,1,3),1,'JOB')
?   Message('After GetStatus','Debug Message', Icon:Hand)

                If job:workshop = 'YES'

                    If engineer_temp <> ''
                        job:engineer       = engineer_temp
                    End!If engineer_temp <> ''

                    jobe:SkillLevel = tmp:SkillLevel
                    Access:JOBSE.Update()

                    If Access:JOBSENG.PrimeRecord() = Level:Benign
                        joe:JobNumber     = job:Ref_Number
                        joe:UserCode      = job:Engineer
                        joe:DateAllocated = Today()
                        joe:TimeAllocated = Clock()
                        joe:AllocatedBy   = use:User_Code
                        access:users.clearkey(use:User_Code_Key)
                        use:User_Code   = job:Engineer
                        access:users.fetch(use:User_Code_Key)

                        joe:EngSkillLevel = use:SkillLevel
                        joe:JobSkillLevel = jobe:SkillLevel

                        If Access:JOBSENG.TryInsert() = Level:Benign
                            !Insert Successful
                        Else !If Access:JOBSENG.TryInsert() = Level:Benign
                            !Insert Failed
                        End !If Access:JOBSENG.TryInsert() = Level:Benign
                    End !If Access:JOBSENG.PrimeRecord() = Level:Benign

                    If location_temp <> ''

                    !Add To Old Location
                        access:locinter.clearkey(loi:location_key)
                        loi:location = job:location
                        If access:locinter.fetch(loi:location_key) = Level:Benign
                            If loi:allocate_spaces = 'YES'
                                loi:current_spaces+= 1
                                loi:location_available = 'YES'
                                access:locinter.update()
                            End
                        end !if
                    !Take From New Location
                        job:location = location_temp

                        access:locinter.clearkey(loi:location_key)
                        loi:location = job:location
                        If access:locinter.fetch(loi:location_key) = Level:Benign
                            If loi:allocate_spaces = 'YES'
                                loi:current_spaces -= 1
                                If loi:current_spaces< 1
                                    loi:current_spaces = 0
                                    loi:location_available = 'NO'
                                End
                                access:locinter.update()
                            End
                        end !if

                    End!If location_temp <> ''
                End!If job:workshop = 'YES'
                access:jobs.update()
                jobque:job_number = job_number_temp
                jobque:record_number += 1
                Add(job_queue_temp)
                Sort(job_queue_temp,-jobque:record_number)

                update_text_temp = 'Job Number: ' & Clip(job_number_temp) & ' Updated Successfully'
                ?update_text_temp{prop:fontcolor} = color:navy
                job_number_temp = ''
                old_status_temp = ''
                Display()
                beep(beep:systemasterisk)
            End!If error# = 0
            Select(?job_number_temp)
        Else!if access:jobs.fetch(job:ref_number_key) = Level:Benign
            Case MessageEx('Unable To Find Selected Job.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,250) 
                Of 1 ! &OK Button
            End!Case MessageEx
            old_status_temp = ''
            Select(?job_number_temp)
            update_text_temp = 'Job Number: ' & Clip(job_number_temp) & ' Update Failed'
            ?update_text_temp{prop:fontcolor} = color:red
        end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
    End!IF tmp:status = ''
    Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job_number_temp, Accepted)
    OF ?ImportJobsButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ImportJobsButton, Accepted)
      IF (tmp:status = '') THEN
          MessageEx('You must select a New Status.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,|
                      '',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0)
      ELSE
      filename3 = Upper(FileLookup6.Ask(1)  )
      DISPLAY
      ! Start Change 2646 BE(11/06/03)
      IF (Filename3 <> '') THEN
          CASE MessageEx('This function will update the Job Status on jobs listed in a csv file.','ServiceBase 2000',|
                               'Styles\question.ico','&OK|Cancel',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,|
                               beep:systemquestion,msgex:samewidths,84,26,0)
              OF 1 ! OK Button
                  OPEN(ImportFile)
                  IF (Error()) THEN
                      MessageEx('Cannot open the import file.'&|
                                  '<13,10>'&'<13,10>(' & Error() & ')','ServiceBase 2000',|
                                  'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                                  beep:systemhand,msgex:samewidths,84,26,0)
                  ELSE
                      SETCURSOR(cursor:wait)
                      SET(IMPORTFILE,0)
                      LOOP
                          NEXT(IMPORTFILE)
                          IF (ERROR()) THEN
                              BREAK
                          END
      
                          access:users.clearkey(use:password_key)
                          use:password =  glo:password
                          access:users.fetch(use:password_key)
      
                          access:jobs.clearkey(job:ref_number_key)
                          job:ref_number = CLIP(LEFT(imp:JobNumber))
                          IF (access:jobs.fetch(job:ref_number_key) <> Level:Benign) THEN
                              SETCURSOR
                              CASE MessageEx('Cannot find job ' & CLIP(LEFT(imp:JobNumber)),|
                                              'ServiceBase 2000',|
                                              'Styles\question.ico','&Continue|Cancel',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                                              beep:systemhand,msgex:samewidths,84,26,0)
                                  OF 2 ! Cancel Button
                                      BREAK
                              END
                              SETCURSOR(cursor:wait)
                          ELSE
                              IF (CLIP(job:current_status) <> CLIP(tmp:status)) THEN
                                  GET(audit,0)
                                  IF (access:audit.primerecord() = level:benign) THEN
                                      aud:notes = 'PREVIOUS STATUS: ' & Clip(job:current_status) & '<13,10>NEW STATUS: ' & Clip(tmp:status)
                                      aud:ref_number = job:ref_number
                                      aud:date = today()
                                      aud:time = clock()
                                      aud:user = use:user_code
                                      aud:action = 'RAPID STATUS UPDATE: ' & Clip(tmp:status)
                                      access:audit.insert()
                                  END ! IF (access:audit.primerecord() = level:benign) THEN
                                  GetStatus(Sub(tmp:Status,1,3),1,'JOB')
                                  access:jobs.update()
      
                                  jobque:job_number = job:ref_number
                                  GET(job_queue_temp,jobque:job_number)
                                  IF (ERRORCODE() = 30) THEN
                                      jobque:job_number = job:ref_number
                                      jobque:record_number += 1
                                      ADD(job_queue_temp)
                                      SORT(job_queue_temp,-jobque:record_number)
                                      DISPLAY()
                                  END
                              END
                          END ! IF (access:jobs.fetch(job:ref_number_key) <> Level:Benign) THEN
                      END ! LOOP WHILE (NEXT(IMPORTFILE) = Level:Benign)
                      CLOSE(ImportFile)
                      SETCURSOR
                  END ! IF (Error()) THEN
          END
      END
      ! End Change 2646 BE(11/06/03)
      END !IF (tmp:status = '') THEN
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ImportJobsButton, Accepted)
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      If Records(Job_Queue_Temp)
          Case MessageEx('Do you wish to print a report of all Jobs updated in this batch?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
                  Free(glo:Queue)
                  Clear(glo:Queue)
                  Loop x# = 1 To Records(job_queue_temp)
                      Get(job_queue_temp,x#)
                      glo:pointer  = jobque:Job_Number
                      Add(glo:Queue)
                  End!Loop x# = 1 To Records(job_queue_temp)
      
                  Rapid_Status_Update_Report
      
                  Free(glo:Queue)
              Of 2 ! &No Button
          End!Case MessageEx
      
      End !Records(Job_Queue_Temp)
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Jobs_Status')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

