

   MEMBER('celrapen.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA004.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CELRA002.INC'),ONCE        !Req'd for module callout resolution
                     END


Update_Engineer_Job PROCEDURE                         !Generated from procedure template - Window

FilesOpened          BYTE
save_job_id          USHORT,AUTO
tmp:CommonRef        LONG
engineer_ref_number_temp REAL
main_store_ref_number_temp REAL
engineer_quantity_temp REAL
main_store_quantity_temp REAL
main_store_sundry_temp STRING(3)
engineer_sundry_temp STRING(3)
save_ccp_id          USHORT,AUTO
save_cwp_id          USHORT,AUTO
ref_number_temp      REAL
serial_number_temp   STRING(30)
common_fault_temp    STRING(30)
tmp:return           STRING(20)
JobsListQueue        QUEUE,PRE(jobque)
JobNumber            LONG
JobNumber_NormalFG   LONG
JobNumber_NormalBG   LONG
JobNumber_SelectedFG LONG
JobNumber_SelectedBG LONG
DateAllocated        DATE
DateAllocated_NormalFG LONG
DateAllocated_NormalBG LONG
DateAllocated_SelectedFG LONG
DateAllocated_SelectedBG LONG
ReportedFault        STRING(50)
ReportedFault_NormalFG LONG
ReportedFault_NormalBG LONG
ReportedFault_SelectedFG LONG
ReportedFault_SelectedBG LONG
JobStatus            STRING(30)
JobStatus_NormalFG   LONG
JobStatus_NormalBG   LONG
JobStatus_SelectedFG LONG
JobStatus_SelectedBG LONG
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Rapid Engineer Update'),AT(,,464,320),FONT('Tahoma',8,,),CENTER,ALRT(F5Key),ALRT(F6Key),ALRT(F7Key),ALRT(F8Key),ALRT(F9Key),ALRT(F10Key),GRAY,DOUBLE,MDI
                       SHEET,AT(4,4,456,200),USE(?Sheet2),SPREAD
                         TAB('Jobs Allocated'),USE(?Tab2),FONT(,,,FONT:bold)
                           LIST,AT(8,24,448,172),USE(?List1),VSCROLL,FORMAT('43R(2)|M*~Job Number~@s8@51R(2)|M*~Date Allocated~@d6@211L(2)|M*~Reported Fault~' &|
   '@s50@120L(2)|M*~Job Status~@s30@'),FROM(JobsListQueue)
                         END
                       END
                       SHEET,AT(4,208,456,80),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Rapid Engineer Update'),USE(?Tab1)
                           PROMPT('Rapid Engineer Update'),AT(8,212),USE(?Prompt8)
                           PROMPT('Job Number'),AT(8,224),USE(?ref_number_temp:Prompt),TRN
                           ENTRY(@s8),AT(84,224,64,10),USE(ref_number_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Serial Number'),AT(8,240),USE(?serial_number_temp:Prompt),TRN
                           ENTRY(@s30),AT(84,240,124,10),USE(serial_number_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           GROUP,AT(12,252,440,28),USE(?ButtonGroup),DISABLE
                             PROMPT('Allocate Common Fault [F7]'),AT(292,215,57,17),USE(?Prompt4)
                             PROMPT('Change Status [F8]'),AT(292,236,57,17),USE(?Prompt4:2)
                             BUTTON('&Allocate Job [F5]'),AT(12,256,88,20),USE(?AllocateJob),LEFT,ICON('stock_sm.gif')
                             BUTTON('&View Job [F6]'),AT(100,256,88,20),USE(?View_Job),LEFT,ICON('spy.gif')
                             BUTTON('Allocate &Common Fault [F7]'),AT(360,212,24,20),USE(?CommonFault:2),LEFT,ICON('Bin.gif')
                             PROMPT('Remove Parts And Invoice Text'),AT(392,212,57,17),USE(?Prompt4:4)
                             BUTTON,AT(260,212,24,20),USE(?CommonFault),LEFT,ICON('fault.gif')
                             BUTTON,AT(260,236,24,20),USE(?ChangeStatus),LEFT,ICON('1.ico')
                             BUTTON,AT(260,260,24,20),USE(?EstimatePartsButton),HIDE,LEFT,ICON('stock2.gif')
                             PROMPT('Add Est. Parts && Update Job [F9]'),AT(292,260,57,17),USE(?EstPartsText),HIDE
                           END
                         END
                       END
                       BUTTON('&Next Job [F10]'),AT(340,296,56,16),USE(?OkButton),LEFT,ICON('next.gif')
                       BUTTON('Finish [ESC]'),AT(400,296,56,16),USE(?CancelButton),LEFT,ICON('thumbs.gif'),STD(STD:Close)
                       PANEL,AT(4,292,456,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
! Start Change 2618 BE(06/08/03)
!
! include definition here rather than in CLARION Data section
! in order to exclude from Solace View which appears unable
! to cope with a GROUP ARRAY data item.
!
JobStatusColours     GROUP,PRE(JSC),DIM(8)
Colour               LONG
Status               STRING(30)
                     END
! Start Change 2618 BE(06/08/03)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?List1{prop:FontColor} = 65793
    ?List1{prop:Color}= 16777215
    ?List1{prop:Color,2} = 16777215
    ?List1{prop:Color,3} = 12937777
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    ?ref_number_temp:Prompt{prop:FontColor} = -1
    ?ref_number_temp:Prompt{prop:Color} = 15066597
    If ?ref_number_temp{prop:ReadOnly} = True
        ?ref_number_temp{prop:FontColor} = 65793
        ?ref_number_temp{prop:Color} = 15066597
    Elsif ?ref_number_temp{prop:Req} = True
        ?ref_number_temp{prop:FontColor} = 65793
        ?ref_number_temp{prop:Color} = 8454143
    Else ! If ?ref_number_temp{prop:Req} = True
        ?ref_number_temp{prop:FontColor} = 65793
        ?ref_number_temp{prop:Color} = 16777215
    End ! If ?ref_number_temp{prop:Req} = True
    ?ref_number_temp{prop:Trn} = 0
    ?ref_number_temp{prop:FontStyle} = font:Bold
    ?serial_number_temp:Prompt{prop:FontColor} = -1
    ?serial_number_temp:Prompt{prop:Color} = 15066597
    If ?serial_number_temp{prop:ReadOnly} = True
        ?serial_number_temp{prop:FontColor} = 65793
        ?serial_number_temp{prop:Color} = 15066597
    Elsif ?serial_number_temp{prop:Req} = True
        ?serial_number_temp{prop:FontColor} = 65793
        ?serial_number_temp{prop:Color} = 8454143
    Else ! If ?serial_number_temp{prop:Req} = True
        ?serial_number_temp{prop:FontColor} = 65793
        ?serial_number_temp{prop:Color} = 16777215
    End ! If ?serial_number_temp{prop:Req} = True
    ?serial_number_temp{prop:Trn} = 0
    ?serial_number_temp{prop:FontStyle} = font:Bold
    ?ButtonGroup{prop:Font,3} = -1
    ?ButtonGroup{prop:Color} = 15066597
    ?ButtonGroup{prop:Trn} = 0
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?Prompt4:2{prop:FontColor} = -1
    ?Prompt4:2{prop:Color} = 15066597
    ?Prompt4:4{prop:FontColor} = -1
    ?Prompt4:4{prop:Color} = 15066597
    ?EstPartsText{prop:FontColor} = -1
    ?EstPartsText{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
BuildJobsList       Routine
    Setcursor(Cursor:Wait)
    Free(JobsListQueue)
    Clear(JobsListQueue)
    Access:USERS.ClearKey(use:password_key)
    use:Password = glo:Password
    If Access:USERS.TryFetch(use:password_key) = Level:Benign
        !Found

    Else!If Access:USERS.TryFetch(use:password_key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:USERS.TryFetch(use:password_key) = Level:Benign
    Save_job_ID = Access:JOBS.SaveFile()
    Access:JOBS.ClearKey(job:EngDateCompKey)
    job:Engineer       = use:User_Code
    job:Date_Completed = 0
    Set(job:EngDateCompKey,job:EngDateCompKey)
    Loop
        If Access:JOBS.NEXT()
           Break
        End !If
        If job:Engineer       <> use:User_Code      |
        Or job:Date_Completed <> 0      |
            Then Break.  ! End If
        Access:STATUS.ClearKey(sts:Ref_Number_Only_Key)
        sts:Ref_Number = Sub(job:Current_Status,1,3)
        If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
            !Found
            If sts:EngineerStatus
                Clear(JobsListQueue)
                Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
                jbn:RefNumber = job:Ref_Number
                If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
                    !Found
                End!If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
                jobque:JobNumber    = job:Ref_Number
                jobque:DateAllocated    = job:Date_Booked
                jobque:ReportedFault      = jbn:Fault_Description
                jobque:JobStatus        = job:Current_Status
                ! Start Change 2618 BE(06/08/03)
                jobque:JobNumber_NormalFG = -1
                jobque:JobNumber_NormalBG =  -1
                jobque:JobNumber_SelectedFG = -1
                jobque:JobNumber_SelectedBG = -1

                jobque:DateAllocated_NormalFG = -1
                jobque:DateAllocated_NormalBG =  -1
                jobque:DateAllocated_SelectedFG = -1
                jobque:DateAllocated_SelectedBG = -1

                jobque:ReportedFault_NormalFG = -1
                jobque:ReportedFault_NormalBG =  -1
                jobque:ReportedFault_SelectedFG = -1
                jobque:ReportedFault_SelectedBG = -1

                jobque:JobStatus_NormalFG = -1
                jobque:JobStatus_NormalBG =  -1
                jobque:JobStatus_SelectedFG = -1
                jobque:JobStatus_SelectedBG = -1

                LOOP ix# = 1 TO 8
                    IF ((JSC:Status[ix#] <> '') AND (jobque:JobStatus = JSC:Status[ix#])) THEN
                        jobque:JobNumber_NormalFG = JSC:Colour[ix#]
                        jobque:JobNumber_NormalBG =  COLOR:White
                        jobque:JobNumber_SelectedFG = COLOR:White
                        jobque:JobNumber_SelectedBG = JSC:Colour[ix#]

                        jobque:DateAllocated_NormalFG = JSC:Colour[ix#]
                        jobque:DateAllocated_NormalBG =  COLOR:White
                        jobque:DateAllocated_SelectedFG = COLOR:White
                        jobque:DateAllocated_SelectedBG = JSC:Colour[ix#]

                        jobque:ReportedFault_NormalFG = JSC:Colour[ix#]
                        jobque:ReportedFault_NormalBG =  COLOR:White
                        jobque:ReportedFault_SelectedFG = COLOR:White
                        jobque:ReportedFault_SelectedBG = JSC:Colour[ix#]

                        jobque:JobStatus_NormalFG = JSC:Colour[ix#]
                        jobque:JobStatus_NormalBG =  COLOR:White
                        jobque:JobStatus_SelectedFG = COLOR:White
                        jobque:JobStatus_SelectedBG = JSC:Colour[ix#]
                        BREAK
                    END
                END
                ! Start Change 2618 BE(06/08/03)
                Add(JobsListQueue)
            End !If sts:EngineerStatus
        Else!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
    End !Loop
    Access:JOBS.RestoreFile(Save_job_ID)

    Sort(JobsListQueue,jobque:JobNumber)
    Setcursor()
Fill_Lists      Routine
Pricing         Routine
stock_history       Routine        !Do The Prime, and Set The Quantity First
    shi:ref_number      = sto:ref_number
    access:users.clearkey(use:password_key)
    use:password        =glo:password
    access:users.fetch(use:password_key)
    shi:user            = use:user_code
    shi:date            = Today()
    shi:transaction_type    = 'DEC'
    shi:despatch_note_number    = par:despatch_note_number
    shi:job_number      = job:ref_number
    shi:purchase_cost   = par:purchase_cost
    shi:sale_cost       = par:sale_cost
    shi:notes           = 'STOCK DECREMENTED'
    access:stohist.insert()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Engineer_Job',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_Engineer_Job',1)
    SolaceViewVars('save_job_id',save_job_id,'Update_Engineer_Job',1)
    SolaceViewVars('tmp:CommonRef',tmp:CommonRef,'Update_Engineer_Job',1)
    SolaceViewVars('engineer_ref_number_temp',engineer_ref_number_temp,'Update_Engineer_Job',1)
    SolaceViewVars('main_store_ref_number_temp',main_store_ref_number_temp,'Update_Engineer_Job',1)
    SolaceViewVars('engineer_quantity_temp',engineer_quantity_temp,'Update_Engineer_Job',1)
    SolaceViewVars('main_store_quantity_temp',main_store_quantity_temp,'Update_Engineer_Job',1)
    SolaceViewVars('main_store_sundry_temp',main_store_sundry_temp,'Update_Engineer_Job',1)
    SolaceViewVars('engineer_sundry_temp',engineer_sundry_temp,'Update_Engineer_Job',1)
    SolaceViewVars('save_ccp_id',save_ccp_id,'Update_Engineer_Job',1)
    SolaceViewVars('save_cwp_id',save_cwp_id,'Update_Engineer_Job',1)
    SolaceViewVars('ref_number_temp',ref_number_temp,'Update_Engineer_Job',1)
    SolaceViewVars('serial_number_temp',serial_number_temp,'Update_Engineer_Job',1)
    SolaceViewVars('common_fault_temp',common_fault_temp,'Update_Engineer_Job',1)
    SolaceViewVars('tmp:return',tmp:return,'Update_Engineer_Job',1)
    SolaceViewVars('JobsListQueue:JobNumber',JobsListQueue:JobNumber,'Update_Engineer_Job',1)
    SolaceViewVars('JobsListQueue:JobNumber_NormalFG',JobsListQueue:JobNumber_NormalFG,'Update_Engineer_Job',1)
    SolaceViewVars('JobsListQueue:JobNumber_NormalBG',JobsListQueue:JobNumber_NormalBG,'Update_Engineer_Job',1)
    SolaceViewVars('JobsListQueue:JobNumber_SelectedFG',JobsListQueue:JobNumber_SelectedFG,'Update_Engineer_Job',1)
    SolaceViewVars('JobsListQueue:JobNumber_SelectedBG',JobsListQueue:JobNumber_SelectedBG,'Update_Engineer_Job',1)
    SolaceViewVars('JobsListQueue:DateAllocated',JobsListQueue:DateAllocated,'Update_Engineer_Job',1)
    SolaceViewVars('JobsListQueue:DateAllocated_NormalFG',JobsListQueue:DateAllocated_NormalFG,'Update_Engineer_Job',1)
    SolaceViewVars('JobsListQueue:DateAllocated_NormalBG',JobsListQueue:DateAllocated_NormalBG,'Update_Engineer_Job',1)
    SolaceViewVars('JobsListQueue:DateAllocated_SelectedFG',JobsListQueue:DateAllocated_SelectedFG,'Update_Engineer_Job',1)
    SolaceViewVars('JobsListQueue:DateAllocated_SelectedBG',JobsListQueue:DateAllocated_SelectedBG,'Update_Engineer_Job',1)
    SolaceViewVars('JobsListQueue:ReportedFault',JobsListQueue:ReportedFault,'Update_Engineer_Job',1)
    SolaceViewVars('JobsListQueue:ReportedFault_NormalFG',JobsListQueue:ReportedFault_NormalFG,'Update_Engineer_Job',1)
    SolaceViewVars('JobsListQueue:ReportedFault_NormalBG',JobsListQueue:ReportedFault_NormalBG,'Update_Engineer_Job',1)
    SolaceViewVars('JobsListQueue:ReportedFault_SelectedFG',JobsListQueue:ReportedFault_SelectedFG,'Update_Engineer_Job',1)
    SolaceViewVars('JobsListQueue:ReportedFault_SelectedBG',JobsListQueue:ReportedFault_SelectedBG,'Update_Engineer_Job',1)
    SolaceViewVars('JobsListQueue:JobStatus',JobsListQueue:JobStatus,'Update_Engineer_Job',1)
    SolaceViewVars('JobsListQueue:JobStatus_NormalFG',JobsListQueue:JobStatus_NormalFG,'Update_Engineer_Job',1)
    SolaceViewVars('JobsListQueue:JobStatus_NormalBG',JobsListQueue:JobStatus_NormalBG,'Update_Engineer_Job',1)
    SolaceViewVars('JobsListQueue:JobStatus_SelectedFG',JobsListQueue:JobStatus_SelectedFG,'Update_Engineer_Job',1)
    SolaceViewVars('JobsListQueue:JobStatus_SelectedBG',JobsListQueue:JobStatus_SelectedBG,'Update_Engineer_Job',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List1;  SolaceCtrlName = '?List1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ref_number_temp:Prompt;  SolaceCtrlName = '?ref_number_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ref_number_temp;  SolaceCtrlName = '?ref_number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?serial_number_temp:Prompt;  SolaceCtrlName = '?serial_number_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?serial_number_temp;  SolaceCtrlName = '?serial_number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ButtonGroup;  SolaceCtrlName = '?ButtonGroup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4:2;  SolaceCtrlName = '?Prompt4:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AllocateJob;  SolaceCtrlName = '?AllocateJob';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?View_Job;  SolaceCtrlName = '?View_Job';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CommonFault:2;  SolaceCtrlName = '?CommonFault:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4:4;  SolaceCtrlName = '?Prompt4:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CommonFault;  SolaceCtrlName = '?CommonFault';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ChangeStatus;  SolaceCtrlName = '?ChangeStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EstimatePartsButton;  SolaceCtrlName = '?EstimatePartsButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EstPartsText;  SolaceCtrlName = '?EstPartsText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Update_Engineer_Job')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Engineer_Job')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?List1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:COMMONFA.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:PICKDET.Open
  Relate:PICKNOTE.Open
  Relate:STAHEAD.Open
  Relate:USERS_ALIAS.Open
  Access:JOBS.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:STATUS.UseFile
  Access:JOBSTAGE.UseFile
  Access:USERS.UseFile
  Access:JOBNOTES.UseFile
  Access:CHARTYPE.UseFile
  Access:ESTPARTS.UseFile
  Access:STDCHRGE.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:SUBCHRGE.UseFile
  Access:JOBSE.UseFile
  Access:JOBSENG.UseFile
  SELF.FilesOpened = True
  ! Start Change 2618 BE(05/08/03)
  JSC:Colour[1] = COLOR:Maroon
  JSC:Status[1] = GETINI('JobStatusColours','Maroon',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[2] = COLOR:Green
  JSC:Status[2] = GETINI('JobStatusColours','Green',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[3] = COLOR:Olive
  JSC:Status[3] = GETINI('JobStatusColours','Olive',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[4] = COLOR:Navy
  JSC:Status[4] = GETINI('JobStatusColours','Navy',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[5] = COLOR:Purple
  JSC:Status[5] = GETINI('JobStatusColours','Purple',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[6] = COLOR:Teal
  JSC:Status[6] = GETINI('JobStatusColours','Teal',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[7] = COLOR:Gray
  JSC:Status[7] = GETINI('JobStatusColours','Gray',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[8] = COLOR:Blue
  JSC:Status[8] = GETINI('JobStatusColours','Blue',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 2618 BE(05/08/03)
  Do BuildJobsList
  OPEN(window)
  SELF.Opened=True
  Access:USERS.ClearKey(use:password_key)
  use:Password = glo:Password
  If Access:USERS.TryFetch(use:password_key) = Level:Benign
      !Found
      ! Start Change 2618 BE(07/08/03)
      !?JobsAllocated{prop:Text} = 'Jobs Allocated To: ' & Clip(use:Forename) & ' ' & Clip(use:Surname)
      ?Tab2{prop:Text} = 'Jobs Allocated To: ' & Clip(use:Forename) & ' ' & Clip(use:Surname)
      ! End Change 2618 BE(07/08/03)
  Else!If Access:USERS.TryFetch(use:password_key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:USERS.TryFetch(use:password_key) = Level:Benign
  Do RecolourWindow
  ?List1{prop:vcr} = TRUE
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:COMMONFA.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:PICKDET.Close
    Relate:PICKNOTE.Close
    Relate:STAHEAD.Close
    Relate:USERS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Engineer_Job',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ref_number_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ref_number_temp, Accepted)
      ! Start Change 3734 BE(19/01/04)
      IF ((?ButtonGroup{PROP:ENABLED} = 1) AND (job:ref_number <> ref_number_temp)) THEN
          serial_number_temp = ''
          Disable(?ButtonGroup)
          Hide(?EstimatePartsButton)
          Hide(?EstPartsText)
          Select(?serial_number_temp)
          Display()
          Do BuildJobsList
      END
      !Select(?serial_number_temp)
      ! End Change 3734 BE(19/01/04)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ref_number_temp, Accepted)
    OF ?serial_number_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?serial_number_temp, Accepted)
      IF LEN(CLIP(serial_number_temp)) = 18
        !Ericsson IMEI!
        serial_number_temp = SUB(serial_number_temp,4,15)
        !ESN_Entry_Temp = Job:ESN
        UPDATE()
        DISPLAY()
      ELSE
        !Job:ESN = ESN_Entry_Temp
      END
    Disable(?ButtonGroup)
    error# = 0
    If ref_number_temp = ''
        error# = 1
        Select(?ref_number_temp)
    End!If ref_number_temp = ''
    If serial_number_temp = '' And error# = 0
        error# = 1
        Select(?serial_number_temp)
    End!If serial_number_temp = '' And error# = 0
    If error# = 0
        access:jobs.clearkey(job:ref_number_key)
        job:ref_number   = ref_number_temp
        If access:jobs.fetch(job:ref_number_key)
            Case MessageEx('Unable to find the selected job.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        Else!If access:jobs.fetch(job:ref_number_key)
            If job:date_completed <> ''
                Case MessageEx('Warning! This job has been completed.'&|
                  '<13,10>'&|
                  '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
                               'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                    Of 1 ! &Yes Button
                    Of 2 ! &No Button
                        error# = 1
                End!Case MessageEx
            End!If job:date_completed <> ''

            If job:workshop <> 'YES' and error# = 0
                Case MessageEx('This job has not been brought into the workshop.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
            End!If job:workshop <> 'YES' and erro# = 0

            If serial_number_temp <> job:esn
                If job:exchange_unit_number <> ''
                    access:exchange.clearkey(xch:ref_number_key)
                    xch:ref_number = job:exchange_unit_number
                    if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                        If xch:esn = serial_number_temp
                            Case MessageEx('The Serial Number you have entered is the Serial Number of the Exchange Unit attached to this job.','ServiceBase 2000',|
                                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                Of 1 ! &OK Button
                            End!Case MessageEx
                            error# = 1
                        ! Start Change 3734 BE(19/01/04)
                        ELSE
                            MessageEx('The Serial Number does not match the selected job number.','ServiceBase 2000',|
                                   'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                                   beep:systemhand,msgex:samewidths,84,26,0)
                            error# = 1
                        ! End Change 3734 BE(19/01/04)
                        End!If xch:esn = serial_number_temp
                    End!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                Else!If job:exchange_unit_number <> ''
                    Case MessageEx('The Serial Number does not match the selected job number.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    error# = 1
                End!If job:exchange_unit_number <> ''
            End!If serial_number_temp <> job:esn

            If error# = 0
                ! Start Change 3933 BE(02/03/04)
                !If job:Chargeable_Job = 'YES' And job:Estimate = 'YES' and (job:Estimate_Accepted <> 'YES' Or job:Estimate_Rejected <> 'YES')
                IF (job:Chargeable_Job = 'YES' AND job:Estimate = 'YES' AND job:Estimate_Accepted <> 'YES') THEN
                ! End Change 3933 BE(02/03/04)
                    Case MessageEx('This job requires an estimate.','ServiceBase 2000',|
                                   'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Unhide(?EstimatePartsButton)
                    Unhide(?EstPartsText)
                End!If job:Estimate = 'YES' and (job:Estimate_Required <> 'YES' And job:Estimate_Refused <> 'YES')

                ! Start Change 4407 BE(24/06/04)
                SET(defaults)
                IF ((access:defaults.next() = Level:Benign) AND |
                    (INSTRING('INTEC',UPPER(def:User_Name),1,1))) THEN
                ! End Change 4407 BE(24/06/04)

                    ! Start Change 4308 BE(21/05/2004)
                    Access:USERS.ClearKey(use:password_key)
                    use:Password = glo:Password
                    IF ((Access:USERS.Fetch(use:password_key) = Level:Benign) AND |
                       (job:Engineer <> use:User_Code))THEN
                       jobskilllevel# = 0
                       SET(DEFAULT2)
                       Access:DEFAULT2.Next()
                       IF (de2:UserSkillLevel) THEN
                            access:JOBSE.Clearkey(jobe:RefNumberKey)
                            jobe:refnumber = job:ref_number
                            IF (access:jobse.fetch(jobe:RefNumberKey) = Level:Benign) THEN
                                jobskilllevel# = jobe:skilllevel
                            END
                       END
                       IF (use:skilllevel < jobskilllevel#) THEN
                           MessageEx('This job requires Skill Level ' & jobskilllevel# & '.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,|
                                       '',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                           serial_number_temp = ''
                           ref_number_temp = ''
                           DISPLAY()
                           SELECT(?ref_number_temp)
                           CYCLE
                       ELSE
                            IF (access:audit.primerecord() = level:benign) THEN
                                aud:notes         = 'NEW ENGINEER: ' & CLIP(use:User_Code) & |
                                                    '<13,10>SKILL LEVEL: ' & use:SkillLevel & |
                                                    '<13,10,13,10>PREVIOUS ENGINEER: ' & CLIP(job:engineer)
                                aud:ref_number    = job:ref_number
                                aud:date          = today()
                                aud:time          = clock()
                                aud:type          = 'JOB'
                                aud:user = use:user_code
                                aud:action        = 'ENGINEER CHANGED TO ' & CLIP(use:User_Code)
                                access:audit.insert()
                            END

                            job:Engineer = use:User_Code
                            GetStatus(310,1,'JOB') !allocated to engineer
                            access:jobs.update()

                            IF (access:JOBSENG.PrimeRecord() = Level:Benign) THEN
                                joe:JobNumber     = job:Ref_Number
                                joe:UserCode      = job:Engineer
                                joe:DateAllocated = Today()
                                joe:TimeAllocated = Clock()
                                joe:AllocatedBy   = use:User_Code
                                joe:EngSkillLevel = use:SkillLevel
                                joe:JobSkillLevel = jobe:SkillLevel
                                access:JOBSENG.insert()
                            END
                        END
                    END
                    ! End Change 4308 BE(21/05/2004)

                ! Start Change 4407 BE(24/06/04)
                END
                ! End Change 4407 BE(24/06/04)

                Enable(?ButtonGroup)
            End!If error# = 0

        End!If access:jobs.fetch(job:ref_number_key)
    End!If error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?serial_number_temp, Accepted)
    OF ?AllocateJob
      ThisWindow.Update
      AllocateJob(ref_number_temp)
      ThisWindow.Reset
    OF ?View_Job
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?View_Job, Accepted)
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number = ref_number_temp
      If access:jobs.fetch(job:ref_number_key) = Level:Benign
          saverequest# = globalrequest
          globalrequest = changerecord
          ! Start Change 1744 BE(15/04/03)
          glo:select20 = 'UPDATE_ENGINEER_JOB'
          ! End Change 1744 BE(15/04/03)
          update_jobs_rapid
          ! Start Change 1744 BE(15/04/03)
          glo:select20 = ''
          ! End Change 1744 BE(15/04/03)
          globalrequest = saverequest#
          Select(?ref_number_temp)
      Else!If access:jobs.fetch(job:ref_number_key) = Level:Benign
          Case MessageEx('Unable To Find Selected Job.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          Select(?ref_number_temp)
      End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?View_Job, Accepted)
    OF ?CommonFault:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CommonFault:2, Accepted)
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = Ref_Number_Temp
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          RemovePartsAndInvoiceText()
          Case MessageEx('Parts and text removed.','ServiceBase 2000',|
                         'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CommonFault:2, Accepted)
    OF ?CommonFault
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CommonFault, Accepted)
      If SecurityCheck('COMMON FAULTS - APPLY TO JOBS')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If SecurityCheck('COMMON FAULTS - APPLY TO JOBS')
          access:jobs.clearkey(job:ref_number_key)
          job:ref_number   = ref_number_temp
          If access:jobs.fetch(job:ref_number_key)
              beep(beep:systemhand)  ;  yield()
              case message('Unable to find selected Job.', |
                      'ServiceBase 2000', icon:hand, |
                       button:ok, button:ok, 0)
              of button:ok
              end !case
          Else!If access:jobs.fetch(job:ref_number_key)
              glo:select1 = job:model_number
              Globalrequest  = selectrecord
              Browse_Common_faults
              case globalresponse
                  of requestcompleted
                      common_fault_temp = com:description
                      tmp:commonref     = com:ref_number
                      CommonFaults(tmp:Commonref)
                     If job:ignore_warranty_charges <> 'YES'
                          Pricing_Routine('W',labour",parts",pass",a")
                          If pass" = True
                              job:labour_cost_warranty = labour"
                              job:parts_cost_warranty  = parts"
                              job:sub_total_warranty = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
                          End!If pass" = False
                      End!If job:ignore_warranty_charges <> 'YES'
      
                     If job:ignore_Chargeable_charges <> 'YES'
                          Pricing_Routine('C',labour",parts",pass",a")
                          If pass" = True
                              job:labour_cost = labour"
                              job:parts_cost  = parts"
                              job:sub_total = job:labour_cost + job:parts_cost + job:courier_cost
                          End!If pass" = False
                      End!If job:ignore_warranty_charges <> 'YES'
                      access:jobs.update()
                  of requestcancelled
                      common_fault_temp = ''
                      tmp:commonref   = ''
              end!case globalreponse
              glo:select1 = ''
          End!If access:jobs.fetch(job:ref_number_key)
      
      End!If SecurityCheck('COMMON FAULTS - APPLY TO JOBS')
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CommonFault, Accepted)
    OF ?ChangeStatus
      ThisWindow.Update
      Change_Status(ref_number_temp)
      ThisWindow.Reset
    OF ?EstimatePartsButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EstimatePartsButton, Accepted)
       BrowseEstimateParts((Ref_Number_Temp))
      If job:Estimate_Ready <> 'YES'
          job:Estimate_Ready = 'YES'
          GetStatus(510,0,'JOB')
          Access:JOBS.Update()
      End!If job:Estimate_Ready <> 'YES'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EstimatePartsButton, Accepted)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      ref_number_temp = ''
      serial_number_temp = ''
      Disable(?ButtonGroup)
      Hide(?EstimatePartsButton)
      Hide(?EstPartsText)
      Select(?ref_number_temp)
      Display()
      Do BuildJobsList
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Engineer_Job')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F5Key
              If ?ButtonGroup{prop:Disable} = 0
                  Post(Event:Accepted,?AllocateJob)
              End !If ?ButtonGroup{prop:Disable} = 0
          Of F6Key
              If ?ButtonGroup{prop:Disable} = 0
                  Post(Event:Accepted,?View_Job)
              End !If ?ButtonGroup{prop:Disable} = 0
          Of F7Key
              If ?ButtonGroup{prop:Disable} = 0
                  Post(Event:Accepted,?CommonFault)
              End !If ?ButtonGroup{prop:Disable} = 0
          Of F8Key
              If ?ButtonGroup{prop:Disable} = 0
                  Post(Event:Accepted,?ChangeStatus)
              End !If ?ButtonGroup{prop:Disable} = 0
          Of F9Key
              If ?ButtonGroup{prop:Disable} = 0
                  Post(Event:Accepted,?EstimatePartsButton)
              End !If ?ButtonGroup{prop:Disable} = 0
          Of F10Key
              Post(Event:Accepted,?OkButton)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

