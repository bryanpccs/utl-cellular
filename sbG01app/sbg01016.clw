

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01016.INC'),ONCE        !Local module procedure declarations
                     END


Update_Retail_Payment PROCEDURE                       !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?rtp:Payment_Type
pay:Payment_Type       LIKE(pay:Payment_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB7::View:FileDropCombo VIEW(PAYTYPES)
                       PROJECT(pay:Payment_Type)
                     END
History::rtp:Record  LIKE(rtp:RECORD),STATIC
QuickWindow          WINDOW('Update the JOBPAYMT File'),AT(,,220,132),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('UpdateJOBPAYMT'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,96),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Payment Type'),AT(8,36),USE(?jpt:payment_type:prompt)
                           COMBO(@s30),AT(84,36,124,10),USE(rtp:Payment_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Date'),AT(8,20),USE(?JPT:Date:Prompt),TRN
                           ENTRY(@d6b),AT(84,20,64,10),USE(rtp:Date),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           BUTTON,AT(152,20,10,10),USE(?PopCalendar),ICON('calenda2.ico')
                           PROMPT('Payment Received'),AT(8,84),USE(?JPT:Amount:Prompt),TRN
                           GROUP,AT(4,48,208,36),USE(?Credit_Card_Group)
                             PROMPT('Credit Card Number'),AT(8,52),USE(?JPT:Credit_Card_Number:Prompt)
                             ENTRY(@s20),AT(84,52,124,10),USE(rtp:Credit_Card_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Expiry Date'),AT(8,68),USE(?JPT:Expiry_Date:Prompt)
                             ENTRY(@p##/##p),AT(84,68,28,10),USE(rtp:Expiry_Date),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Issue Number'),AT(120,68),USE(?JPT:Issue_Number:Prompt)
                             ENTRY(@s5),AT(168,68,40,10),USE(rtp:Issue_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           END
                           ENTRY(@n-14.2),AT(84,84,64,10),USE(rtp:Amount),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                         END
                       END
                       BUTTON('&OK'),AT(100,108,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,108,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,104,212,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?jpt:payment_type:prompt{prop:FontColor} = -1
    ?jpt:payment_type:prompt{prop:Color} = 15066597
    If ?rtp:Payment_Type{prop:ReadOnly} = True
        ?rtp:Payment_Type{prop:FontColor} = 65793
        ?rtp:Payment_Type{prop:Color} = 15066597
    Elsif ?rtp:Payment_Type{prop:Req} = True
        ?rtp:Payment_Type{prop:FontColor} = 65793
        ?rtp:Payment_Type{prop:Color} = 8454143
    Else ! If ?rtp:Payment_Type{prop:Req} = True
        ?rtp:Payment_Type{prop:FontColor} = 65793
        ?rtp:Payment_Type{prop:Color} = 16777215
    End ! If ?rtp:Payment_Type{prop:Req} = True
    ?rtp:Payment_Type{prop:Trn} = 0
    ?rtp:Payment_Type{prop:FontStyle} = font:Bold
    ?JPT:Date:Prompt{prop:FontColor} = -1
    ?JPT:Date:Prompt{prop:Color} = 15066597
    If ?rtp:Date{prop:ReadOnly} = True
        ?rtp:Date{prop:FontColor} = 65793
        ?rtp:Date{prop:Color} = 15066597
    Elsif ?rtp:Date{prop:Req} = True
        ?rtp:Date{prop:FontColor} = 65793
        ?rtp:Date{prop:Color} = 8454143
    Else ! If ?rtp:Date{prop:Req} = True
        ?rtp:Date{prop:FontColor} = 65793
        ?rtp:Date{prop:Color} = 16777215
    End ! If ?rtp:Date{prop:Req} = True
    ?rtp:Date{prop:Trn} = 0
    ?rtp:Date{prop:FontStyle} = font:Bold
    ?JPT:Amount:Prompt{prop:FontColor} = -1
    ?JPT:Amount:Prompt{prop:Color} = 15066597
    ?Credit_Card_Group{prop:Font,3} = -1
    ?Credit_Card_Group{prop:Color} = 15066597
    ?Credit_Card_Group{prop:Trn} = 0
    ?JPT:Credit_Card_Number:Prompt{prop:FontColor} = -1
    ?JPT:Credit_Card_Number:Prompt{prop:Color} = 15066597
    If ?rtp:Credit_Card_Number{prop:ReadOnly} = True
        ?rtp:Credit_Card_Number{prop:FontColor} = 65793
        ?rtp:Credit_Card_Number{prop:Color} = 15066597
    Elsif ?rtp:Credit_Card_Number{prop:Req} = True
        ?rtp:Credit_Card_Number{prop:FontColor} = 65793
        ?rtp:Credit_Card_Number{prop:Color} = 8454143
    Else ! If ?rtp:Credit_Card_Number{prop:Req} = True
        ?rtp:Credit_Card_Number{prop:FontColor} = 65793
        ?rtp:Credit_Card_Number{prop:Color} = 16777215
    End ! If ?rtp:Credit_Card_Number{prop:Req} = True
    ?rtp:Credit_Card_Number{prop:Trn} = 0
    ?rtp:Credit_Card_Number{prop:FontStyle} = font:Bold
    ?JPT:Expiry_Date:Prompt{prop:FontColor} = -1
    ?JPT:Expiry_Date:Prompt{prop:Color} = 15066597
    If ?rtp:Expiry_Date{prop:ReadOnly} = True
        ?rtp:Expiry_Date{prop:FontColor} = 65793
        ?rtp:Expiry_Date{prop:Color} = 15066597
    Elsif ?rtp:Expiry_Date{prop:Req} = True
        ?rtp:Expiry_Date{prop:FontColor} = 65793
        ?rtp:Expiry_Date{prop:Color} = 8454143
    Else ! If ?rtp:Expiry_Date{prop:Req} = True
        ?rtp:Expiry_Date{prop:FontColor} = 65793
        ?rtp:Expiry_Date{prop:Color} = 16777215
    End ! If ?rtp:Expiry_Date{prop:Req} = True
    ?rtp:Expiry_Date{prop:Trn} = 0
    ?rtp:Expiry_Date{prop:FontStyle} = font:Bold
    ?JPT:Issue_Number:Prompt{prop:FontColor} = -1
    ?JPT:Issue_Number:Prompt{prop:Color} = 15066597
    If ?rtp:Issue_Number{prop:ReadOnly} = True
        ?rtp:Issue_Number{prop:FontColor} = 65793
        ?rtp:Issue_Number{prop:Color} = 15066597
    Elsif ?rtp:Issue_Number{prop:Req} = True
        ?rtp:Issue_Number{prop:FontColor} = 65793
        ?rtp:Issue_Number{prop:Color} = 8454143
    Else ! If ?rtp:Issue_Number{prop:Req} = True
        ?rtp:Issue_Number{prop:FontColor} = 65793
        ?rtp:Issue_Number{prop:Color} = 16777215
    End ! If ?rtp:Issue_Number{prop:Req} = True
    ?rtp:Issue_Number{prop:Trn} = 0
    ?rtp:Issue_Number{prop:FontStyle} = font:Bold
    If ?rtp:Amount{prop:ReadOnly} = True
        ?rtp:Amount{prop:FontColor} = 65793
        ?rtp:Amount{prop:Color} = 15066597
    Elsif ?rtp:Amount{prop:Req} = True
        ?rtp:Amount{prop:FontColor} = 65793
        ?rtp:Amount{prop:Color} = 8454143
    Else ! If ?rtp:Amount{prop:Req} = True
        ?rtp:Amount{prop:FontColor} = 65793
        ?rtp:Amount{prop:Color} = 16777215
    End ! If ?rtp:Amount{prop:Req} = True
    ?rtp:Amount{prop:Trn} = 0
    ?rtp:Amount{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Credit_Card_Bit     Routine
    access:paytypes.clearkey(pay:payment_type_key)
    pay:payment_type = rtp:payment_type
    if access:paytypes.fetch(pay:payment_type_key) = Level:Benign
        If pay:credit_card = 'YES'
            Enable(?credit_card_group)
            ?rtp:credit_card_number{prop:req} = 1
            ?rtp:expiry_date{prop:req} = 1
        Else
            Disable(?credit_card_group)
            ?rtp:credit_card_number{prop:req} = 0
            ?rtp:expiry_date{prop:req} = 0
        End
    end


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Retail_Payment',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Retail_Payment',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_Retail_Payment',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Retail_Payment',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jpt:payment_type:prompt;  SolaceCtrlName = '?jpt:payment_type:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rtp:Payment_Type;  SolaceCtrlName = '?rtp:Payment_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JPT:Date:Prompt;  SolaceCtrlName = '?JPT:Date:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rtp:Date;  SolaceCtrlName = '?rtp:Date';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JPT:Amount:Prompt;  SolaceCtrlName = '?JPT:Amount:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Credit_Card_Group;  SolaceCtrlName = '?Credit_Card_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JPT:Credit_Card_Number:Prompt;  SolaceCtrlName = '?JPT:Credit_Card_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rtp:Credit_Card_Number;  SolaceCtrlName = '?rtp:Credit_Card_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JPT:Expiry_Date:Prompt;  SolaceCtrlName = '?JPT:Expiry_Date:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rtp:Expiry_Date;  SolaceCtrlName = '?rtp:Expiry_Date';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JPT:Issue_Number:Prompt;  SolaceCtrlName = '?JPT:Issue_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rtp:Issue_Number;  SolaceCtrlName = '?rtp:Issue_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rtp:Amount;  SolaceCtrlName = '?rtp:Amount';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Payment'
  OF ChangeRecord
    ActionMessage = 'Changing A Payment'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Update_Retail_Payment')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Retail_Payment')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?jpt:payment_type:prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(rtp:Record,History::rtp:Record)
  SELF.AddHistoryField(?rtp:Payment_Type,4)
  SELF.AddHistoryField(?rtp:Date,3)
  SELF.AddHistoryField(?rtp:Credit_Card_Number,5)
  SELF.AddHistoryField(?rtp:Expiry_Date,6)
  SELF.AddHistoryField(?rtp:Issue_Number,7)
  SELF.AddHistoryField(?rtp:Amount,8)
  SELF.AddUpdateFile(Access:RETPAY)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:PAYTYPES.Open
  Relate:RETPAY.Open
  Access:RETSALES.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:RETPAY
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If thiswindow.request = Insertrecord
      access:users.clearkey(use:password_key)
      use:password =glo:password
      if access:users.fetch(use:password_key) = Level:Benign
         rtp:user_code = use:user_code
      end
      rtp:amount  = glo:select1
  End!If thiswindow.request = Insertrecord
  Do RecolourWindow
  ?RTP:Date{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB7.Init(rtp:Payment_Type,?rtp:Payment_Type,Queue:FileDropCombo.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo,Relate:PAYTYPES,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo
  FDCB7.AddSortOrder(pay:Payment_Type_Key)
  FDCB7.AddField(pay:Payment_Type,FDCB7.Q.pay:Payment_Type)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PAYTYPES.Close
    Relate:RETPAY.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Retail_Payment',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?rtp:Payment_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?rtp:Payment_Type, Accepted)
      Do credit_card_bit
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?rtp:Payment_Type, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          rtp:Date = TINCALENDARStyle1()
          Display(?RTP:Date)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Retail_Payment')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?rtp:Date
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do credit_card_bit
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      FDCB7.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

