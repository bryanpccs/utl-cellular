

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01017.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Retail_Payments PROCEDURE (f_ref_number,f_account_number,f_courier_cost,func:Type) !Generated from procedure template - Window

ThisThreadActive BYTE
CurrentTab           STRING(80)
vat_rate_temp        REAL
ref_number_temp      REAL
items_total_temp     REAL
sub_total_temp       REAL
vat_temp             REAL
total_temp           REAL
amount_paid_temp     REAL
balance_due_temp     REAL
line_cost_temp       REAL
tmp:Type             STRING(3)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(RETPAY)
                       PROJECT(rtp:Date)
                       PROJECT(rtp:Payment_Type)
                       PROJECT(rtp:Amount)
                       PROJECT(rtp:Record_Number)
                       PROJECT(rtp:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
rtp:Date               LIKE(rtp:Date)                 !List box control field - type derived from field
rtp:Payment_Type       LIKE(rtp:Payment_Type)         !List box control field - type derived from field
rtp:Amount             LIKE(rtp:Amount)               !List box control field - type derived from field
rtp:Record_Number      LIKE(rtp:Record_Number)        !Primary key field - type derived from field
rtp:Ref_Number         LIKE(rtp:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(RETSTOCK)
                       PROJECT(res:Part_Number)
                       PROJECT(res:Description)
                       PROJECT(res:Quantity)
                       PROJECT(res:Item_Cost)
                       PROJECT(res:Record_Number)
                       PROJECT(res:Ref_Number)
                       PROJECT(res:Despatched)
                       PROJECT(res:Despatch_Note_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
res:Part_Number        LIKE(res:Part_Number)          !List box control field - type derived from field
res:Description        LIKE(res:Description)          !List box control field - type derived from field
res:Quantity           LIKE(res:Quantity)             !List box control field - type derived from field
res:Item_Cost          LIKE(res:Item_Cost)            !List box control field - type derived from field
line_cost_temp         LIKE(line_cost_temp)           !List box control field - type derived from local data
res:Record_Number      LIKE(res:Record_Number)        !Primary key field - type derived from field
res:Ref_Number         LIKE(res:Ref_Number)           !Browse key field - type derived from field
res:Despatched         LIKE(res:Despatched)           !Browse key field - type derived from field
res:Despatch_Note_Number LIKE(res:Despatch_Note_Number) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK11::res:Despatched      LIKE(res:Despatched)
HK11::res:Ref_Number      LIKE(res:Ref_Number)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Retail Sales Payment Details'),AT(,,403,342),FONT('Tahoma',8,,),CENTER,IMM,HLP('Browse_Retail_Payments'),TIMER(50),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,396,136),USE(?Sheet2),SPREAD
                         TAB('Items Requested'),USE(?Tab3)
                           LIST,AT(8,20,384,116),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~Part Number~@s30@120L(2)|M~Description~@s30@39R(2)|M~Quantity~@n8@46R(' &|
   '2)|M~Item Cost~@n14.2@56R(2)|M~Line Cost~@n14.2@'),FROM(Queue:Browse)
                         END
                       END
                       LIST,AT(8,164,216,124),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('49R(2)|M~Date~L(0)@d6b@80L(2)|M~Payment Type~@s30@68R(12)|M~Payment Received~R(0' &|
   ')@n-14.2@'),FROM(Queue:Browse:1)
                       BUTTON('&Insert'),AT(8,292,56,16),USE(?Insert:2),LEFT,ICON('Insert.ico')
                       BUTTON('&Change'),AT(88,292,56,16),USE(?Change:2),LEFT,ICON('edit.ico'),DEFAULT
                       BUTTON('&Delete'),AT(168,292,56,16),USE(?Delete:2),LEFT,ICON('delete.ico')
                       SHEET,AT(4,148,396,164),USE(?CurrentTab),SPREAD
                         TAB('Payments Received'),USE(?Tab:3)
                           PROMPT('Courier Cost'),AT(239,172),USE(?Prompt1),TRN,COLOR(COLOR:Silver)
                           STRING(@n14.2),AT(340,172),USE(f_courier_cost),TRN,RIGHT,COLOR(COLOR:Silver)
                           STRING(@n14.2),AT(340,188),USE(items_total_temp),TRN,RIGHT,COLOR(COLOR:Silver)
                           PROMPT('Items Total'),AT(239,188),USE(?Prompt1:2),TRN,COLOR(COLOR:Silver)
                           PROMPT('Sub Total (Excl V.A.T.)'),AT(239,204),USE(?Prompt1:3),TRN,COLOR(COLOR:Silver)
                           STRING(@n14.2),AT(340,204),USE(sub_total_temp),TRN,RIGHT,COLOR(COLOR:Silver)
                           LINE,AT(240,216,154,0),USE(?Line1),COLOR(COLOR:Black)
                           LINE,AT(240,232,154,0),USE(?Line1:2),COLOR(COLOR:Black)
                           LINE,AT(239,256,154,0),USE(?Line1:3),COLOR(COLOR:Black)
                           STRING(@n14.2),AT(340,220),USE(vat_temp),TRN,RIGHT,COLOR(COLOR:Silver)
                           STRING(@n14.2),AT(340,240),USE(total_temp),TRN,RIGHT,COLOR(COLOR:Silver)
                           STRING(@n14.2),AT(340,260),USE(amount_paid_temp),TRN,RIGHT,COLOR(COLOR:Silver)
                           PROMPT('V.A.T. '),AT(239,220),USE(?Prompt1:4),TRN,COLOR(COLOR:Silver)
                           PROMPT('Total (Inc. V.A.T.)'),AT(239,240),USE(?Prompt1:5),TRN,FONT(,,,FONT:bold),COLOR(COLOR:Silver)
                           PROMPT('Amount Paid'),AT(239,260),USE(?Prompt1:6),TRN,COLOR(COLOR:Silver)
                           STRING(@n-14.2),AT(340,276),USE(balance_due_temp),TRN,RIGHT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           PROMPT('Balance Due'),AT(239,276),USE(?Prompt1:7),TRN,FONT(,,COLOR:Navy,FONT:bold),COLOR(COLOR:Silver)
                         END
                       END
                       BUTTON('Close'),AT(340,320,56,16),USE(?Close),LEFT,ICON('cancel.ico')
                       PANEL,AT(4,316,396,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetFromView          PROCEDURE(),DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort1:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
BRW1::Sort1:StepClass StepRealClass                   !Conditional Step Manager - CHOICE(?CurrentTab) = 2
BRW6                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetFromView          PROCEDURE(),DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet2{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:3{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?f_courier_cost{prop:FontColor} = -1
    ?f_courier_cost{prop:Color} = 15066597
    ?items_total_temp{prop:FontColor} = -1
    ?items_total_temp{prop:Color} = 15066597
    ?Prompt1:2{prop:FontColor} = -1
    ?Prompt1:2{prop:Color} = 15066597
    ?Prompt1:3{prop:FontColor} = -1
    ?Prompt1:3{prop:Color} = 15066597
    ?sub_total_temp{prop:FontColor} = -1
    ?sub_total_temp{prop:Color} = 15066597
    ?vat_temp{prop:FontColor} = -1
    ?vat_temp{prop:Color} = 15066597
    ?total_temp{prop:FontColor} = -1
    ?total_temp{prop:Color} = 15066597
    ?amount_paid_temp{prop:FontColor} = -1
    ?amount_paid_temp{prop:Color} = 15066597
    ?Prompt1:4{prop:FontColor} = -1
    ?Prompt1:4{prop:Color} = 15066597
    ?Prompt1:5{prop:FontColor} = -1
    ?Prompt1:5{prop:Color} = 15066597
    ?Prompt1:6{prop:FontColor} = -1
    ?Prompt1:6{prop:Color} = 15066597
    ?balance_due_temp{prop:FontColor} = -1
    ?balance_due_temp{prop:Color} = 15066597
    ?Prompt1:7{prop:FontColor} = -1
    ?Prompt1:7{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Retail_Payments',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Retail_Payments',1)
    SolaceViewVars('vat_rate_temp',vat_rate_temp,'Browse_Retail_Payments',1)
    SolaceViewVars('ref_number_temp',ref_number_temp,'Browse_Retail_Payments',1)
    SolaceViewVars('items_total_temp',items_total_temp,'Browse_Retail_Payments',1)
    SolaceViewVars('sub_total_temp',sub_total_temp,'Browse_Retail_Payments',1)
    SolaceViewVars('vat_temp',vat_temp,'Browse_Retail_Payments',1)
    SolaceViewVars('total_temp',total_temp,'Browse_Retail_Payments',1)
    SolaceViewVars('amount_paid_temp',amount_paid_temp,'Browse_Retail_Payments',1)
    SolaceViewVars('balance_due_temp',balance_due_temp,'Browse_Retail_Payments',1)
    SolaceViewVars('line_cost_temp',line_cost_temp,'Browse_Retail_Payments',1)
    SolaceViewVars('tmp:Type',tmp:Type,'Browse_Retail_Payments',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:2;  SolaceCtrlName = '?Insert:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:2;  SolaceCtrlName = '?Change:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:2;  SolaceCtrlName = '?Delete:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:3;  SolaceCtrlName = '?Tab:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?f_courier_cost;  SolaceCtrlName = '?f_courier_cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?items_total_temp;  SolaceCtrlName = '?items_total_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:2;  SolaceCtrlName = '?Prompt1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:3;  SolaceCtrlName = '?Prompt1:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub_total_temp;  SolaceCtrlName = '?sub_total_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1;  SolaceCtrlName = '?Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1:2;  SolaceCtrlName = '?Line1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1:3;  SolaceCtrlName = '?Line1:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?vat_temp;  SolaceCtrlName = '?vat_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?total_temp;  SolaceCtrlName = '?total_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?amount_paid_temp;  SolaceCtrlName = '?amount_paid_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:4;  SolaceCtrlName = '?Prompt1:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:5;  SolaceCtrlName = '?Prompt1:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:6;  SolaceCtrlName = '?Prompt1:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?balance_due_temp;  SolaceCtrlName = '?balance_due_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:7;  SolaceCtrlName = '?Prompt1:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Retail_Payments')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Retail_Payments')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?List
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Browse_Retail_Payments'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:RETPAY.Open
  Relate:SUBTRACC.Open
  Relate:VATCODE.Open
  Access:RETSALES.UseFile
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  ref_number_temp = f_ref_number
  
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = f_account_number
  if access:subtracc.fetch(sub:account_number_key) = level:benign
      access:tradeacc.clearkey(tra:account_number_key) 
      tra:account_number = sub:main_account_number
      if access:tradeacc.fetch(tra:account_number_key) = level:benign
          if tra:invoice_sub_accounts = 'YES'
              access:vatcode.clearkey(vat:vat_code_key)
              vat:vat_code = sub:retail_vat_code
              if access:vatcode.fetch(vat:vat_code_key) = level:benign
                  vat_rate_temp = vat:vat_rate
              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
          else!if tra:use_sub_accounts = 'YES'
              access:vatcode.clearkey(vat:vat_code_key)
              vat:vat_code = tra:retail_vat_code
              if access:vatcode.fetch(vat:vat_code_key) = level:benign
                  vat_rate_temp = vat:vat_rate
              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
          end!if tra:use_sub_accounts = 'YES'
      end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
  end!if access:subtracc.fetch(sub:account_number_key) = level:benign  
  tmp:Type     = func:Type
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:RETPAY,SELF)
  BRW6.Init(?List,Queue:Browse.ViewPosition,BRW6::View:Browse,Queue:Browse,Relate:RETSTOCK,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,rtp:Date_Key)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,rtp:Ref_Number,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,rtp:Date_Key)
  BRW1.AddRange(rtp:Ref_Number,ref_number_temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,rtp:Date,1,BRW1)
  BRW1.AddField(rtp:Date,BRW1.Q.rtp:Date)
  BRW1.AddField(rtp:Payment_Type,BRW1.Q.rtp:Payment_Type)
  BRW1.AddField(rtp:Amount,BRW1.Q.rtp:Amount)
  BRW1.AddField(rtp:Record_Number,BRW1.Q.rtp:Record_Number)
  BRW1.AddField(rtp:Ref_Number,BRW1.Q.rtp:Ref_Number)
  BRW6.Q &= Queue:Browse
  BRW6.RetainRow = 0
  BRW6.AddSortOrder(,res:Despatched_Key)
  BRW6.AddRange(res:Despatched)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,res:Despatch_Note_Number,1,BRW6)
  BIND('line_cost_temp',line_cost_temp)
  BRW6.AddField(res:Part_Number,BRW6.Q.res:Part_Number)
  BRW6.AddField(res:Description,BRW6.Q.res:Description)
  BRW6.AddField(res:Quantity,BRW6.Q.res:Quantity)
  BRW6.AddField(res:Item_Cost,BRW6.Q.res:Item_Cost)
  BRW6.AddField(line_cost_temp,BRW6.Q.line_cost_temp)
  BRW6.AddField(res:Record_Number,BRW6.Q.res:Record_Number)
  BRW6.AddField(res:Ref_Number,BRW6.Q.res:Ref_Number)
  BRW6.AddField(res:Despatched,BRW6.Q.res:Despatched)
  BRW6.AddField(res:Despatch_Note_Number,BRW6.Q.res:Despatch_Note_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETPAY.Close
    Relate:SUBTRACC.Close
    Relate:VATCODE.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='Browse_Retail_Payments'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Retail_Payments',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  If balance_due_temp > 0
      glo:select1    = balance_due_temp
  Else!If balance_due_temp > 0
      glo:select1  = ''
  End!If balance_due_temp > 0
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Retail_Payment
    ReturnValue = GlobalResponse
  END
  glo:select1 = ''
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Retail_Payments')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Timer)
      sub_total_temp  = items_total_temp + f_courier_cost
      vat_temp        = Round(items_total_temp * (vat_rate_temp/100),.01) + Round(f_courier_cost * (vat_rate_temp/100),.01)
      total_temp      = sub_total_temp + vat_temp
      
      balance_due_temp    = total_temp - amount_paid_temp
      
      Display(?sub_total_temp)
      Display(?vat_temp)
      Display(?total_temp)
      Display(?balance_due_temp)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Timer)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW1.ResetFromView PROCEDURE

amount_paid_temp:Sum REAL
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:RETPAY.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    amount_paid_temp:Sum += rtp:Amount
  END
  amount_paid_temp = amount_paid_temp:Sum
  PARENT.ResetFromView
  Relate:RETPAY.SetQuickScan(0)
  SETCURSOR()


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = ref_number_temp
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = tmp:Type
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW6.ResetFromView PROCEDURE

items_total_temp:Sum REAL
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:RETSTOCK.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    items_total_temp:Sum += line_cost_temp
  END
  items_total_temp = items_total_temp:Sum
  PARENT.ResetFromView
  Relate:RETSTOCK.SetQuickScan(0)
  SETCURSOR()


BRW6.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(6, SetQueueRecord, ())
  line_cost_temp = Round(Round(res:item_cost,.01) * res:quantity,.01)
  PARENT.SetQueueRecord
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(6, SetQueueRecord, ())


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

