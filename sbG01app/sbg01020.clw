

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01020.INC'),ONCE        !Local module procedure declarations
                     END


Stock_Enquiry PROCEDURE (func:OrderNumber)            !Generated from procedure template - Window

ThisThreadActive BYTE
CurrentTab           STRING(80)
save_res_id          USHORT,AUTO
save_sto_id          USHORT
Use_Euro             BYTE
save_ret_ali_id      USHORT,AUTO
save_orp_id          USHORT,AUTO
save_ope_id          USHORT,AUTO
vat_rate_temp        REAL
vat_rate_parts_temp  REAL
Location_Temp        STRING(30)
accessory_temp       STRING('ALL')
vat_temp             REAL
total_temp           REAL
line_cost_temp       REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Courier_Euro         REAL
Item_Euro            REAL
Sub_Euro             REAL
Vat_Euro             REAL
Total_Euro           REAL
tmp:OrderNumber      LONG
tmp:ShelfLocation    STRING(30)
tmp:SecondLocation   STRING(30)
tmp:Quantity         LONG
tmp:Sundry           STRING('NO {1}')
tmp:Accessory        STRING('NO {1}')
tmp:ExchangeUnit     STRING('NO {1}')
tmp:Suspend          BYTE(0)
tmp:WebORderNumber   LONG
tmp:BackOrderTotal   REAL
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Location_Temp
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(STOCK)
                       PROJECT(sto:Part_Number)
                       PROJECT(sto:Description)
                       PROJECT(sto:Shelf_Location)
                       PROJECT(sto:Second_Location)
                       PROJECT(sto:Quantity_Stock)
                       PROJECT(sto:Ref_Number)
                       PROJECT(sto:Location)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
sto:Part_Number        LIKE(sto:Part_Number)          !List box control field - type derived from field
sto:Description        LIKE(sto:Description)          !List box control field - type derived from field
sto:Shelf_Location     LIKE(sto:Shelf_Location)       !List box control field - type derived from field
sto:Second_Location    LIKE(sto:Second_Location)      !List box control field - type derived from field
sto:Quantity_Stock     LIKE(sto:Quantity_Stock)       !List box control field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !Primary key field - type derived from field
sto:Location           LIKE(sto:Location)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(RETSTOCK)
                       PROJECT(res:Part_Number)
                       PROJECT(res:Description)
                       PROJECT(res:Item_Cost)
                       PROJECT(res:Quantity)
                       PROJECT(res:Record_Number)
                       PROJECT(res:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
res:Part_Number        LIKE(res:Part_Number)          !List box control field - type derived from field
res:Part_Number_NormalFG LONG                         !Normal forground color
res:Part_Number_NormalBG LONG                         !Normal background color
res:Part_Number_SelectedFG LONG                       !Selected forground color
res:Part_Number_SelectedBG LONG                       !Selected background color
res:Description        LIKE(res:Description)          !List box control field - type derived from field
res:Description_NormalFG LONG                         !Normal forground color
res:Description_NormalBG LONG                         !Normal background color
res:Description_SelectedFG LONG                       !Selected forground color
res:Description_SelectedBG LONG                       !Selected background color
res:Item_Cost          LIKE(res:Item_Cost)            !List box control field - type derived from field
res:Item_Cost_NormalFG LONG                           !Normal forground color
res:Item_Cost_NormalBG LONG                           !Normal background color
res:Item_Cost_SelectedFG LONG                         !Selected forground color
res:Item_Cost_SelectedBG LONG                         !Selected background color
res:Quantity           LIKE(res:Quantity)             !List box control field - type derived from field
res:Quantity_NormalFG  LONG                           !Normal forground color
res:Quantity_NormalBG  LONG                           !Normal background color
res:Quantity_SelectedFG LONG                          !Selected forground color
res:Quantity_SelectedBG LONG                          !Selected background color
line_cost_temp         LIKE(line_cost_temp)           !List box control field - type derived from local data
line_cost_temp_NormalFG LONG                          !Normal forground color
line_cost_temp_NormalBG LONG                          !Normal background color
line_cost_temp_SelectedFG LONG                        !Selected forground color
line_cost_temp_SelectedBG LONG                        !Selected background color
res:Record_Number      LIKE(res:Record_Number)        !Primary key field - type derived from field
res:Ref_Number         LIKE(res:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW7::View:Browse    VIEW(ORDITEMS)
                       PROJECT(ori:partno)
                       PROJECT(ori:partdiscription)
                       PROJECT(ori:qty)
                       PROJECT(ori:recordnumber)
                       PROJECT(ori:ordhno)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:2
ori:partno             LIKE(ori:partno)               !List box control field - type derived from field
ori:partdiscription    LIKE(ori:partdiscription)      !List box control field - type derived from field
ori:qty                LIKE(ori:qty)                  !List box control field - type derived from field
sto:Second_Location    LIKE(sto:Second_Location)      !Browse hot field - type derived from field
sto:Quantity_Stock     LIKE(sto:Quantity_Stock)       !Browse hot field - type derived from field
sto:Shelf_Location     LIKE(sto:Shelf_Location)       !Browse hot field - type derived from field
sto:Suspend            LIKE(sto:Suspend)              !Browse hot field - type derived from field
sto:ExchangeUnit       LIKE(sto:ExchangeUnit)         !Browse hot field - type derived from field
sto:Accessory          LIKE(sto:Accessory)            !Browse hot field - type derived from field
sto:Sundry_Item        LIKE(sto:Sundry_Item)          !Browse hot field - type derived from field
ori:recordnumber       LIKE(ori:recordnumber)         !Primary key field - type derived from field
ori:ordhno             LIKE(ori:ordhno)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK14::sto:Location        LIKE(sto:Location)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB5::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
QuickWindow          WINDOW('Pick Stock'),AT(,,616,346),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('Pick_Stock'),TIMER(50),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,204,608,112),USE(?Sheet2),SPREAD
                         TAB('Items Requested - Please note list of prices is NOT in Euros'),USE(?Tab3)
                           LIST,AT(8,220,416,68),USE(?List),IMM,MSG('Browsing Records'),FORMAT('120L(2)|M*~Part Number~@s30@120L(2)|M*~Description~@s30@56L(2)|M*~Retail Cost~@n' &|
   '14.2@32L(2)|M*~Quantity~@n8@40R(2)|M*~Line Cost~L@n10.2@'),FROM(Queue:Browse),DROPID('FromList')
                           PROMPT('Courier Cost'),AT(432,220),USE(?RET:Courier_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(492,220,52,12),USE(ret:Courier_Cost),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('<128>'),AT(548,220),USE(?Courier_Euro:Prompt),HIDE
                           ENTRY(@n10.2),AT(556,220,52,12),USE(Courier_Euro),SKIP,HIDE,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                           PROMPT('<128>'),AT(548,236),USE(?Item_Euro:Prompt),HIDE
                           ENTRY(@n10.2),AT(556,236,52,12),USE(Item_Euro),SKIP,HIDE,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                           PROMPT('Items Cost'),AT(432,236),USE(?RET:Parts_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(492,236,52,12),USE(ret:Parts_Cost),SKIP,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('<128>'),AT(548,256),USE(?Sub_Euro:Prompt),HIDE
                           ENTRY(@n10.2),AT(556,256,52,12),USE(Sub_Euro),SKIP,HIDE,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                           PROMPT('Sub Total'),AT(432,256),USE(?RET:Sub_Total:Prompt),TRN
                           ENTRY(@n14.2),AT(492,256,52,12),USE(ret:Sub_Total),SKIP,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('<128>'),AT(548,268),USE(?Vat_Euro:Prompt),HIDE
                           ENTRY(@n10.2),AT(556,268,52,12),USE(Vat_Euro),SKIP,HIDE,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                           PROMPT('V.A.T.'),AT(432,268),USE(?vat_temp:Prompt),TRN
                           ENTRY(@n14.2),AT(492,268,52,12),USE(vat_temp),SKIP,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('<128>'),AT(548,284),USE(?Total_Euro:Prompt),HIDE
                           ENTRY(@n10.2),AT(556,284,52,12),USE(Total_Euro),SKIP,HIDE,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                           BUTTON('&Change'),AT(8,292,56,16),USE(?Change),LEFT,ICON('edit.gif')
                           BUTTON('&Delete'),AT(68,292,56,16),USE(?Delete),LEFT,ICON('delete.gif')
                           BOX,AT(156,296,10,10),USE(?Box1),FILL(COLOR:Green)
                           PROMPT('- Awaiting Ordering'),AT(176,296),USE(?Prompt3)
                           BOX,AT(252,296,10,10),USE(?Box1:2),COLOR(COLOR:Black),FILL(COLOR:Black)
                           PROMPT('- Cancelled'),AT(272,296),USE(?Prompt3:2)
                           PROMPT('Cost Of Back Order Parts (Ex VAT)'),AT(372,300),USE(?tmp:BackOrderTotal:Prompt),TRN
                           ENTRY(@n14.2),AT(491,300,52,12),USE(tmp:BackOrderTotal),SKIP,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Cost Of Back Order Parts'),TIP('Cost Of Back Order Parts'),UPR,READONLY
                           PROMPT('Total (Inc V.A.T.)'),AT(432,284),USE(?total_temp:Prompt),TRN
                           ENTRY(@n14.2),AT(491,284,52,12),USE(total_temp),SKIP,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                         END
                       END
                       PANEL,AT(4,320,608,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Insert'),AT(8,180,56,16),USE(?Insert),LEFT,ICON('insert.gif')
                       PROMPT('Press ''Insert'', Double Click or Drag the Stock Item to add to Items Request'),AT(76,184),USE(?Prompt2),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(36,0,120,328),USE(?Sheet3),NOSHEET
                         TAB('Tab 4'),USE(?NormalStockTab)
                           PROMPT('Site Location'),AT(136,20),USE(?Prompt1)
                           LIST,AT(8,52,600,124),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),ALRT(4),ALRT(EnterKey),ALRT(InsertKey),FORMAT('120L(2)|M~Part Number~@s30@120L(2)|M~Description~@s30@120L(2)|M~Shelf Location~@' &|
   's30@120L(2)|M~Second Location~@s30@32D(2)|M~Stock Qty~L@N8@'),FROM(Queue:Browse:1),DRAGID('FromList')
                           COMBO(@s30),AT(8,20,124,10),USE(Location_Temp),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M*@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           OPTION('Stock Type'),AT(416,20,192,28),USE(accessory_temp),BOXED
                             RADIO('All'),AT(580,32),USE(?accessory_temp:Radio3),VALUE('ALL')
                             RADIO('Non-Accessories'),AT(420,32),USE(?accessory_temp:Radio1),VALUE('NO')
                             RADIO('Accessories'),AT(508,32),USE(?accessory_temp:Radio2),VALUE('YES')
                           END
                           BUTTON('Generate Stock Order'),AT(528,180,80,16),USE(?Generate_Stock_Order),LEFT,ICON('stock_in.gif')
                           SHEET,AT(4,4,608,196),USE(?CurrentTab),SPREAD
                             TAB('By Part Number'),USE(?Tab:2)
                               ENTRY(@s30),AT(8,36,124,10),USE(sto:Part_Number),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                             END
                             TAB('By Description'),USE(?Tab2)
                               ENTRY(@s30),AT(8,36,124,10),USE(sto:Description),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                             END
                           END
                         END
                         TAB('Tab 5'),USE(?WebStockTab)
                           LIST,AT(8,36,392,140),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(InsertKey),FORMAT('87L(2)|M~Part Number~@s20@173L(2)|M~Description~@s60@40R(2)|M~Quantity~L@s8@'),FROM(Queue:Browse:2),DRAGID('FromList')
                           SHEET,AT(4,4,608,196),USE(?Sheet4),SPREAD
                             TAB('By Part Number'),USE(?Tab6)
                               PROMPT('Part Details'),AT(408,36),USE(?Prompt22)
                               PROMPT('Shelf Location'),AT(408,56),USE(?sto:Shelf_Location:Prompt),TRN
                               ENTRY(@s30),AT(476,56,124,10),USE(sto:Shelf_Location),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Yellow),UPR,READONLY
                               PROMPT('Sundry Item'),AT(408,108),USE(?sto:Shelf_Location:Prompt:2),TRN
                               PROMPT('2nd Location'),AT(408,72),USE(?sto:Second_Location:Prompt),TRN
                               ENTRY(@s30),AT(476,72,124,10),USE(sto:Second_Location),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                               PROMPT('Quantity In Stock'),AT(408,88),USE(?sto:Quantity_Stock:Prompt),TRN
                               ENTRY(@N8),AT(476,88,64,10),USE(sto:Quantity_Stock),SKIP,DECIMAL(14),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY
                               CHECK,AT(476,108),USE(sto:Sundry_Item),SKIP,DISABLE,VALUE('YES','NO')
                               PROMPT('Accessory'),AT(408,124),USE(?sto:Shelf_Location:Prompt:3),TRN
                               CHECK,AT(476,124),USE(sto:Accessory),SKIP,DISABLE,VALUE('YES','NO')
                               PROMPT('Exchange Unit'),AT(408,140),USE(?sto:Shelf_Location:Prompt:4),TRN
                               CHECK,AT(476,140),USE(sto:ExchangeUnit),SKIP,DISABLE,RIGHT,VALUE('YES','NO')
                               PROMPT('Suspend Part'),AT(408,156),USE(?sto:Shelf_Location:Prompt:5),TRN
                               CHECK,AT(476,156),USE(sto:Suspend),SKIP,DISABLE,MSG('Suspend Part'),TIP('Suspend Part'),VALUE('1','0')
                             END
                           END
                         END
                       END
                       BUTTON('Finish'),AT(552,324,56,16),USE(?Close),LEFT,ICON('ok.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL' And Upper(ret:Payment_Method) <> 'EXC'
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'YES' And Upper(ret:Payment_Method) <> 'EXC'
BRW1::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'NO' And Upper(ret:Payment_Method) <> 'EXC'
BRW1::Sort4:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL' And Upper(ret:Payment_Method) <> 'EXC'
BRW1::Sort5:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'YES' And Upper(ret:Payment_Method) <> 'EXC'
BRW1::Sort6:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'NO' And Upper(ret:Payment_Method) <> 'EXC'
BRW1::Sort7:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(ret:Payment_Method) = 'EXC'
BRW1::Sort8:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(ret:Payment_Method) = 'EXC'
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetFromView          PROCEDURE(),DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW7                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:2                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet2{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?RET:Courier_Cost:Prompt{prop:FontColor} = -1
    ?RET:Courier_Cost:Prompt{prop:Color} = 15066597
    If ?ret:Courier_Cost{prop:ReadOnly} = True
        ?ret:Courier_Cost{prop:FontColor} = 65793
        ?ret:Courier_Cost{prop:Color} = 15066597
    Elsif ?ret:Courier_Cost{prop:Req} = True
        ?ret:Courier_Cost{prop:FontColor} = 65793
        ?ret:Courier_Cost{prop:Color} = 8454143
    Else ! If ?ret:Courier_Cost{prop:Req} = True
        ?ret:Courier_Cost{prop:FontColor} = 65793
        ?ret:Courier_Cost{prop:Color} = 16777215
    End ! If ?ret:Courier_Cost{prop:Req} = True
    ?ret:Courier_Cost{prop:Trn} = 0
    ?ret:Courier_Cost{prop:FontStyle} = font:Bold
    ?Courier_Euro:Prompt{prop:FontColor} = -1
    ?Courier_Euro:Prompt{prop:Color} = 15066597
    If ?Courier_Euro{prop:ReadOnly} = True
        ?Courier_Euro{prop:FontColor} = 65793
        ?Courier_Euro{prop:Color} = 15066597
    Elsif ?Courier_Euro{prop:Req} = True
        ?Courier_Euro{prop:FontColor} = 65793
        ?Courier_Euro{prop:Color} = 8454143
    Else ! If ?Courier_Euro{prop:Req} = True
        ?Courier_Euro{prop:FontColor} = 65793
        ?Courier_Euro{prop:Color} = 16777215
    End ! If ?Courier_Euro{prop:Req} = True
    ?Courier_Euro{prop:Trn} = 0
    ?Courier_Euro{prop:FontStyle} = font:Bold
    ?Item_Euro:Prompt{prop:FontColor} = -1
    ?Item_Euro:Prompt{prop:Color} = 15066597
    If ?Item_Euro{prop:ReadOnly} = True
        ?Item_Euro{prop:FontColor} = 65793
        ?Item_Euro{prop:Color} = 15066597
    Elsif ?Item_Euro{prop:Req} = True
        ?Item_Euro{prop:FontColor} = 65793
        ?Item_Euro{prop:Color} = 8454143
    Else ! If ?Item_Euro{prop:Req} = True
        ?Item_Euro{prop:FontColor} = 65793
        ?Item_Euro{prop:Color} = 16777215
    End ! If ?Item_Euro{prop:Req} = True
    ?Item_Euro{prop:Trn} = 0
    ?Item_Euro{prop:FontStyle} = font:Bold
    ?RET:Parts_Cost:Prompt{prop:FontColor} = -1
    ?RET:Parts_Cost:Prompt{prop:Color} = 15066597
    If ?ret:Parts_Cost{prop:ReadOnly} = True
        ?ret:Parts_Cost{prop:FontColor} = 65793
        ?ret:Parts_Cost{prop:Color} = 15066597
    Elsif ?ret:Parts_Cost{prop:Req} = True
        ?ret:Parts_Cost{prop:FontColor} = 65793
        ?ret:Parts_Cost{prop:Color} = 8454143
    Else ! If ?ret:Parts_Cost{prop:Req} = True
        ?ret:Parts_Cost{prop:FontColor} = 65793
        ?ret:Parts_Cost{prop:Color} = 16777215
    End ! If ?ret:Parts_Cost{prop:Req} = True
    ?ret:Parts_Cost{prop:Trn} = 0
    ?ret:Parts_Cost{prop:FontStyle} = font:Bold
    ?Sub_Euro:Prompt{prop:FontColor} = -1
    ?Sub_Euro:Prompt{prop:Color} = 15066597
    If ?Sub_Euro{prop:ReadOnly} = True
        ?Sub_Euro{prop:FontColor} = 65793
        ?Sub_Euro{prop:Color} = 15066597
    Elsif ?Sub_Euro{prop:Req} = True
        ?Sub_Euro{prop:FontColor} = 65793
        ?Sub_Euro{prop:Color} = 8454143
    Else ! If ?Sub_Euro{prop:Req} = True
        ?Sub_Euro{prop:FontColor} = 65793
        ?Sub_Euro{prop:Color} = 16777215
    End ! If ?Sub_Euro{prop:Req} = True
    ?Sub_Euro{prop:Trn} = 0
    ?Sub_Euro{prop:FontStyle} = font:Bold
    ?RET:Sub_Total:Prompt{prop:FontColor} = -1
    ?RET:Sub_Total:Prompt{prop:Color} = 15066597
    If ?ret:Sub_Total{prop:ReadOnly} = True
        ?ret:Sub_Total{prop:FontColor} = 65793
        ?ret:Sub_Total{prop:Color} = 15066597
    Elsif ?ret:Sub_Total{prop:Req} = True
        ?ret:Sub_Total{prop:FontColor} = 65793
        ?ret:Sub_Total{prop:Color} = 8454143
    Else ! If ?ret:Sub_Total{prop:Req} = True
        ?ret:Sub_Total{prop:FontColor} = 65793
        ?ret:Sub_Total{prop:Color} = 16777215
    End ! If ?ret:Sub_Total{prop:Req} = True
    ?ret:Sub_Total{prop:Trn} = 0
    ?ret:Sub_Total{prop:FontStyle} = font:Bold
    ?Vat_Euro:Prompt{prop:FontColor} = -1
    ?Vat_Euro:Prompt{prop:Color} = 15066597
    If ?Vat_Euro{prop:ReadOnly} = True
        ?Vat_Euro{prop:FontColor} = 65793
        ?Vat_Euro{prop:Color} = 15066597
    Elsif ?Vat_Euro{prop:Req} = True
        ?Vat_Euro{prop:FontColor} = 65793
        ?Vat_Euro{prop:Color} = 8454143
    Else ! If ?Vat_Euro{prop:Req} = True
        ?Vat_Euro{prop:FontColor} = 65793
        ?Vat_Euro{prop:Color} = 16777215
    End ! If ?Vat_Euro{prop:Req} = True
    ?Vat_Euro{prop:Trn} = 0
    ?Vat_Euro{prop:FontStyle} = font:Bold
    ?vat_temp:Prompt{prop:FontColor} = -1
    ?vat_temp:Prompt{prop:Color} = 15066597
    If ?vat_temp{prop:ReadOnly} = True
        ?vat_temp{prop:FontColor} = 65793
        ?vat_temp{prop:Color} = 15066597
    Elsif ?vat_temp{prop:Req} = True
        ?vat_temp{prop:FontColor} = 65793
        ?vat_temp{prop:Color} = 8454143
    Else ! If ?vat_temp{prop:Req} = True
        ?vat_temp{prop:FontColor} = 65793
        ?vat_temp{prop:Color} = 16777215
    End ! If ?vat_temp{prop:Req} = True
    ?vat_temp{prop:Trn} = 0
    ?vat_temp{prop:FontStyle} = font:Bold
    ?Total_Euro:Prompt{prop:FontColor} = -1
    ?Total_Euro:Prompt{prop:Color} = 15066597
    If ?Total_Euro{prop:ReadOnly} = True
        ?Total_Euro{prop:FontColor} = 65793
        ?Total_Euro{prop:Color} = 15066597
    Elsif ?Total_Euro{prop:Req} = True
        ?Total_Euro{prop:FontColor} = 65793
        ?Total_Euro{prop:Color} = 8454143
    Else ! If ?Total_Euro{prop:Req} = True
        ?Total_Euro{prop:FontColor} = 65793
        ?Total_Euro{prop:Color} = 16777215
    End ! If ?Total_Euro{prop:Req} = True
    ?Total_Euro{prop:Trn} = 0
    ?Total_Euro{prop:FontStyle} = font:Bold
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Prompt3:2{prop:FontColor} = -1
    ?Prompt3:2{prop:Color} = 15066597
    ?tmp:BackOrderTotal:Prompt{prop:FontColor} = -1
    ?tmp:BackOrderTotal:Prompt{prop:Color} = 15066597
    If ?tmp:BackOrderTotal{prop:ReadOnly} = True
        ?tmp:BackOrderTotal{prop:FontColor} = 65793
        ?tmp:BackOrderTotal{prop:Color} = 15066597
    Elsif ?tmp:BackOrderTotal{prop:Req} = True
        ?tmp:BackOrderTotal{prop:FontColor} = 65793
        ?tmp:BackOrderTotal{prop:Color} = 8454143
    Else ! If ?tmp:BackOrderTotal{prop:Req} = True
        ?tmp:BackOrderTotal{prop:FontColor} = 65793
        ?tmp:BackOrderTotal{prop:Color} = 16777215
    End ! If ?tmp:BackOrderTotal{prop:Req} = True
    ?tmp:BackOrderTotal{prop:Trn} = 0
    ?tmp:BackOrderTotal{prop:FontStyle} = font:Bold
    ?total_temp:Prompt{prop:FontColor} = -1
    ?total_temp:Prompt{prop:Color} = 15066597
    If ?total_temp{prop:ReadOnly} = True
        ?total_temp{prop:FontColor} = 65793
        ?total_temp{prop:Color} = 15066597
    Elsif ?total_temp{prop:Req} = True
        ?total_temp{prop:FontColor} = 65793
        ?total_temp{prop:Color} = 8454143
    Else ! If ?total_temp{prop:Req} = True
        ?total_temp{prop:FontColor} = 65793
        ?total_temp{prop:Color} = 16777215
    End ! If ?total_temp{prop:Req} = True
    ?total_temp{prop:Trn} = 0
    ?total_temp{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597

    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?Sheet3{prop:Color} = 15066597
    ?NormalStockTab{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    If ?Location_Temp{prop:ReadOnly} = True
        ?Location_Temp{prop:FontColor} = 65793
        ?Location_Temp{prop:Color} = 15066597
    Elsif ?Location_Temp{prop:Req} = True
        ?Location_Temp{prop:FontColor} = 65793
        ?Location_Temp{prop:Color} = 8454143
    Else ! If ?Location_Temp{prop:Req} = True
        ?Location_Temp{prop:FontColor} = 65793
        ?Location_Temp{prop:Color} = 16777215
    End ! If ?Location_Temp{prop:Req} = True
    ?Location_Temp{prop:Trn} = 0
    ?Location_Temp{prop:FontStyle} = font:Bold
    ?accessory_temp{prop:Font,3} = -1
    ?accessory_temp{prop:Color} = 15066597
    ?accessory_temp{prop:Trn} = 0
    ?accessory_temp:Radio3{prop:Font,3} = -1
    ?accessory_temp:Radio3{prop:Color} = 15066597
    ?accessory_temp:Radio3{prop:Trn} = 0
    ?accessory_temp:Radio1{prop:Font,3} = -1
    ?accessory_temp:Radio1{prop:Color} = 15066597
    ?accessory_temp:Radio1{prop:Trn} = 0
    ?accessory_temp:Radio2{prop:Font,3} = -1
    ?accessory_temp:Radio2{prop:Color} = 15066597
    ?accessory_temp:Radio2{prop:Trn} = 0
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?sto:Part_Number{prop:ReadOnly} = True
        ?sto:Part_Number{prop:FontColor} = 65793
        ?sto:Part_Number{prop:Color} = 15066597
    Elsif ?sto:Part_Number{prop:Req} = True
        ?sto:Part_Number{prop:FontColor} = 65793
        ?sto:Part_Number{prop:Color} = 8454143
    Else ! If ?sto:Part_Number{prop:Req} = True
        ?sto:Part_Number{prop:FontColor} = 65793
        ?sto:Part_Number{prop:Color} = 16777215
    End ! If ?sto:Part_Number{prop:Req} = True
    ?sto:Part_Number{prop:Trn} = 0
    ?sto:Part_Number{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    If ?sto:Description{prop:ReadOnly} = True
        ?sto:Description{prop:FontColor} = 65793
        ?sto:Description{prop:Color} = 15066597
    Elsif ?sto:Description{prop:Req} = True
        ?sto:Description{prop:FontColor} = 65793
        ?sto:Description{prop:Color} = 8454143
    Else ! If ?sto:Description{prop:Req} = True
        ?sto:Description{prop:FontColor} = 65793
        ?sto:Description{prop:Color} = 16777215
    End ! If ?sto:Description{prop:Req} = True
    ?sto:Description{prop:Trn} = 0
    ?sto:Description{prop:FontStyle} = font:Bold
    ?WebStockTab{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Sheet4{prop:Color} = 15066597
    ?Tab6{prop:Color} = 15066597
    ?Prompt22{prop:FontColor} = -1
    ?Prompt22{prop:Color} = 15066597
    ?sto:Shelf_Location:Prompt{prop:FontColor} = -1
    ?sto:Shelf_Location:Prompt{prop:Color} = 15066597
    If ?sto:Shelf_Location{prop:ReadOnly} = True
        ?sto:Shelf_Location{prop:FontColor} = 65793
        ?sto:Shelf_Location{prop:Color} = 15066597
    Elsif ?sto:Shelf_Location{prop:Req} = True
        ?sto:Shelf_Location{prop:FontColor} = 65793
        ?sto:Shelf_Location{prop:Color} = 8454143
    Else ! If ?sto:Shelf_Location{prop:Req} = True
        ?sto:Shelf_Location{prop:FontColor} = 65793
        ?sto:Shelf_Location{prop:Color} = 16777215
    End ! If ?sto:Shelf_Location{prop:Req} = True
    ?sto:Shelf_Location{prop:Trn} = 0
    ?sto:Shelf_Location{prop:FontStyle} = font:Bold
    ?sto:Shelf_Location:Prompt:2{prop:FontColor} = -1
    ?sto:Shelf_Location:Prompt:2{prop:Color} = 15066597
    ?sto:Second_Location:Prompt{prop:FontColor} = -1
    ?sto:Second_Location:Prompt{prop:Color} = 15066597
    If ?sto:Second_Location{prop:ReadOnly} = True
        ?sto:Second_Location{prop:FontColor} = 65793
        ?sto:Second_Location{prop:Color} = 15066597
    Elsif ?sto:Second_Location{prop:Req} = True
        ?sto:Second_Location{prop:FontColor} = 65793
        ?sto:Second_Location{prop:Color} = 8454143
    Else ! If ?sto:Second_Location{prop:Req} = True
        ?sto:Second_Location{prop:FontColor} = 65793
        ?sto:Second_Location{prop:Color} = 16777215
    End ! If ?sto:Second_Location{prop:Req} = True
    ?sto:Second_Location{prop:Trn} = 0
    ?sto:Second_Location{prop:FontStyle} = font:Bold
    ?sto:Quantity_Stock:Prompt{prop:FontColor} = -1
    ?sto:Quantity_Stock:Prompt{prop:Color} = 15066597
    If ?sto:Quantity_Stock{prop:ReadOnly} = True
        ?sto:Quantity_Stock{prop:FontColor} = 65793
        ?sto:Quantity_Stock{prop:Color} = 15066597
    Elsif ?sto:Quantity_Stock{prop:Req} = True
        ?sto:Quantity_Stock{prop:FontColor} = 65793
        ?sto:Quantity_Stock{prop:Color} = 8454143
    Else ! If ?sto:Quantity_Stock{prop:Req} = True
        ?sto:Quantity_Stock{prop:FontColor} = 65793
        ?sto:Quantity_Stock{prop:Color} = 16777215
    End ! If ?sto:Quantity_Stock{prop:Req} = True
    ?sto:Quantity_Stock{prop:Trn} = 0
    ?sto:Quantity_Stock{prop:FontStyle} = font:Bold
    ?sto:Sundry_Item{prop:Font,3} = -1
    ?sto:Sundry_Item{prop:Color} = 15066597
    ?sto:Sundry_Item{prop:Trn} = 0
    ?sto:Shelf_Location:Prompt:3{prop:FontColor} = -1
    ?sto:Shelf_Location:Prompt:3{prop:Color} = 15066597
    ?sto:Accessory{prop:Font,3} = -1
    ?sto:Accessory{prop:Color} = 15066597
    ?sto:Accessory{prop:Trn} = 0
    ?sto:Shelf_Location:Prompt:4{prop:FontColor} = -1
    ?sto:Shelf_Location:Prompt:4{prop:Color} = 15066597
    ?sto:ExchangeUnit{prop:Font,3} = -1
    ?sto:ExchangeUnit{prop:Color} = 15066597
    ?sto:ExchangeUnit{prop:Trn} = 0
    ?sto:Shelf_Location:Prompt:5{prop:FontColor} = -1
    ?sto:Shelf_Location:Prompt:5{prop:Color} = 15066597
    ?sto:Suspend{prop:Font,3} = -1
    ?sto:Suspend{prop:Color} = 15066597
    ?sto:Suspend{prop:Trn} = 0

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Show_fields     ROUTINE
  IF Use_Euro = TRUE
    ?Courier_Euro{PROP:Hide} = FALSE
    ?Courier_Euro:Prompt{PROP:Hide} = FALSE
    ?Item_Euro{PROP:Hide} = FALSE
    ?Item_Euro:Prompt{PROP:Hide} = FALSE
    ?Sub_Euro{PROP:Hide} = FALSE
    ?Sub_Euro:Prompt{PROP:Hide} = FALSE
    ?Vat_Euro{PROP:Hide} = FALSE
    ?Vat_Euro:Prompt{PROP:Hide} = FALSE
    ?Total_Euro{PROP:Hide} = FALSE
    ?Total_Euro:Prompt{PROP:Hide} = FALSE
  END




SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Stock_Enquiry',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Stock_Enquiry',1)
    SolaceViewVars('save_res_id',save_res_id,'Stock_Enquiry',1)
    SolaceViewVars('save_sto_id',save_sto_id,'Stock_Enquiry',1)
    SolaceViewVars('Use_Euro',Use_Euro,'Stock_Enquiry',1)
    SolaceViewVars('save_ret_ali_id',save_ret_ali_id,'Stock_Enquiry',1)
    SolaceViewVars('save_orp_id',save_orp_id,'Stock_Enquiry',1)
    SolaceViewVars('save_ope_id',save_ope_id,'Stock_Enquiry',1)
    SolaceViewVars('vat_rate_temp',vat_rate_temp,'Stock_Enquiry',1)
    SolaceViewVars('vat_rate_parts_temp',vat_rate_parts_temp,'Stock_Enquiry',1)
    SolaceViewVars('Location_Temp',Location_Temp,'Stock_Enquiry',1)
    SolaceViewVars('accessory_temp',accessory_temp,'Stock_Enquiry',1)
    SolaceViewVars('vat_temp',vat_temp,'Stock_Enquiry',1)
    SolaceViewVars('total_temp',total_temp,'Stock_Enquiry',1)
    SolaceViewVars('line_cost_temp',line_cost_temp,'Stock_Enquiry',1)
    SolaceViewVars('Courier_Euro',Courier_Euro,'Stock_Enquiry',1)
    SolaceViewVars('Item_Euro',Item_Euro,'Stock_Enquiry',1)
    SolaceViewVars('Sub_Euro',Sub_Euro,'Stock_Enquiry',1)
    SolaceViewVars('Vat_Euro',Vat_Euro,'Stock_Enquiry',1)
    SolaceViewVars('Total_Euro',Total_Euro,'Stock_Enquiry',1)
    SolaceViewVars('tmp:OrderNumber',tmp:OrderNumber,'Stock_Enquiry',1)
    SolaceViewVars('tmp:ShelfLocation',tmp:ShelfLocation,'Stock_Enquiry',1)
    SolaceViewVars('tmp:SecondLocation',tmp:SecondLocation,'Stock_Enquiry',1)
    SolaceViewVars('tmp:Quantity',tmp:Quantity,'Stock_Enquiry',1)
    SolaceViewVars('tmp:Sundry',tmp:Sundry,'Stock_Enquiry',1)
    SolaceViewVars('tmp:Accessory',tmp:Accessory,'Stock_Enquiry',1)
    SolaceViewVars('tmp:ExchangeUnit',tmp:ExchangeUnit,'Stock_Enquiry',1)
    SolaceViewVars('tmp:Suspend',tmp:Suspend,'Stock_Enquiry',1)
    SolaceViewVars('tmp:WebORderNumber',tmp:WebORderNumber,'Stock_Enquiry',1)
    SolaceViewVars('tmp:BackOrderTotal',tmp:BackOrderTotal,'Stock_Enquiry',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Courier_Cost:Prompt;  SolaceCtrlName = '?RET:Courier_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Courier_Cost;  SolaceCtrlName = '?ret:Courier_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Courier_Euro:Prompt;  SolaceCtrlName = '?Courier_Euro:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Courier_Euro;  SolaceCtrlName = '?Courier_Euro';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Item_Euro:Prompt;  SolaceCtrlName = '?Item_Euro:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Item_Euro;  SolaceCtrlName = '?Item_Euro';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Parts_Cost:Prompt;  SolaceCtrlName = '?RET:Parts_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Parts_Cost;  SolaceCtrlName = '?ret:Parts_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sub_Euro:Prompt;  SolaceCtrlName = '?Sub_Euro:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sub_Euro;  SolaceCtrlName = '?Sub_Euro';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Sub_Total:Prompt;  SolaceCtrlName = '?RET:Sub_Total:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Sub_Total;  SolaceCtrlName = '?ret:Sub_Total';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Vat_Euro:Prompt;  SolaceCtrlName = '?Vat_Euro:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Vat_Euro;  SolaceCtrlName = '?Vat_Euro';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?vat_temp:Prompt;  SolaceCtrlName = '?vat_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?vat_temp;  SolaceCtrlName = '?vat_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Total_Euro:Prompt;  SolaceCtrlName = '?Total_Euro:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Total_Euro;  SolaceCtrlName = '?Total_Euro';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Box1;  SolaceCtrlName = '?Box1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Box1:2;  SolaceCtrlName = '?Box1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3:2;  SolaceCtrlName = '?Prompt3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:BackOrderTotal:Prompt;  SolaceCtrlName = '?tmp:BackOrderTotal:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:BackOrderTotal;  SolaceCtrlName = '?tmp:BackOrderTotal';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?total_temp:Prompt;  SolaceCtrlName = '?total_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?total_temp;  SolaceCtrlName = '?total_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet3;  SolaceCtrlName = '?Sheet3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?NormalStockTab;  SolaceCtrlName = '?NormalStockTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Location_Temp;  SolaceCtrlName = '?Location_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?accessory_temp;  SolaceCtrlName = '?accessory_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?accessory_temp:Radio3;  SolaceCtrlName = '?accessory_temp:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?accessory_temp:Radio1;  SolaceCtrlName = '?accessory_temp:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?accessory_temp:Radio2;  SolaceCtrlName = '?accessory_temp:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Generate_Stock_Order;  SolaceCtrlName = '?Generate_Stock_Order';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Part_Number;  SolaceCtrlName = '?sto:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Description;  SolaceCtrlName = '?sto:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WebStockTab;  SolaceCtrlName = '?WebStockTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet4;  SolaceCtrlName = '?Sheet4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab6;  SolaceCtrlName = '?Tab6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt22;  SolaceCtrlName = '?Prompt22';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Shelf_Location:Prompt;  SolaceCtrlName = '?sto:Shelf_Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Shelf_Location;  SolaceCtrlName = '?sto:Shelf_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Shelf_Location:Prompt:2;  SolaceCtrlName = '?sto:Shelf_Location:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Second_Location:Prompt;  SolaceCtrlName = '?sto:Second_Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Second_Location;  SolaceCtrlName = '?sto:Second_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Quantity_Stock:Prompt;  SolaceCtrlName = '?sto:Quantity_Stock:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Quantity_Stock;  SolaceCtrlName = '?sto:Quantity_Stock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Sundry_Item;  SolaceCtrlName = '?sto:Sundry_Item';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Shelf_Location:Prompt:3;  SolaceCtrlName = '?sto:Shelf_Location:Prompt:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Accessory;  SolaceCtrlName = '?sto:Accessory';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Shelf_Location:Prompt:4;  SolaceCtrlName = '?sto:Shelf_Location:Prompt:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:ExchangeUnit;  SolaceCtrlName = '?sto:ExchangeUnit';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Shelf_Location:Prompt:5;  SolaceCtrlName = '?sto:Shelf_Location:Prompt:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Suspend;  SolaceCtrlName = '?sto:Suspend';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Stock_Enquiry')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Stock_Enquiry')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?List
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Stock_Enquiry'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  Relate:LOCATION.Open
  Relate:ORDITEMS.Open
  Relate:RETSALES.Open
  Relate:RETSTOCK_ALIAS.Open
  Relate:VATCODE.Open
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:ORDERS.UseFile
  Access:ORDPARTS.UseFile
  Access:ORDPEND.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  If ret:Payment_Method = 'EXC'
      Accessory_Temp = 'YES'
  Else !ret:Payment_Method = 'EXC'
  
  End !ret:Payment_Method = 'EXC'
  
  tmp:OrderNumber = func:OrderNumber
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOCK,SELF)
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:RETSTOCK,SELF)
  BRW7.Init(?List:2,Queue:Browse:2.ViewPosition,BRW7::View:Browse,Queue:Browse:2,Relate:ORDITEMS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  SET(Defaults)
  Access:Defaults.Next()
  Use_Euro = FALSE
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = ret:account_number
  if access:subtracc.fetch(sub:account_number_key) = level:benign
      access:tradeacc.clearkey(tra:account_number_key) 
      tra:account_number = sub:main_account_number
      if access:tradeacc.fetch(tra:account_number_key) = level:benign
          IF tra:EuroApplies = 1 OR Sub:EuroApplies = 1
            Use_Euro = TRUE
            DO Show_Fields
          END
          if tra:invoice_sub_accounts = 'YES'
              access:vatcode.clearkey(vat:vat_code_key)
              vat:vat_code = sub:retail_vat_code
              if access:vatcode.fetch(vat:vat_code_key) = level:benign
                  vat_rate_temp = vat:vat_rate
              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
              access:vatcode.clearkey(vat:vat_code_key)
              vat:vat_code = sub:parts_vat_code
              if access:vatcode.fetch(vat:vat_code_key) = level:benign
                  vat_rate_parts_temp = vat:vat_rate
              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
          else!if tra:use_sub_accounts = 'YES'
              access:vatcode.clearkey(vat:vat_code_key)
              vat:vat_code = tra:retail_vat_code
              if access:vatcode.fetch(vat:vat_code_key) = level:benign
                  vat_rate_temp = vat:vat_rate
              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
              access:vatcode.clearkey(vat:vat_code_key)
              vat:vat_code = tra:parts_vat_code
              if access:vatcode.fetch(vat:vat_code_key) = level:benign
                  vat_rate_parts_temp = vat:vat_rate
              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
          end!if tra:use_sub_accounts = 'YES'
      end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
  end!if access:subtracc.fetch(sub:account_number_key) = level:benign
  
  If ret:Payment_Method = 'EXC'
      ?Accessory_Temp{prop:Disable} = True
  Else !ret:Payment_Method = 'EXC'
  
  End !ret:Payment_Method = 'EXC'
  If func:OrderNumber <> 0
      ?WebStockTab{prop:Hide} = 0
      ?NormalStockTab{prop:Hide} = 1
  Else !func:OrderNumber <> 0
      ?WebStockTab{prop:Hide} = 1
      ?NormalStockTab{prop:Hide} = 0
  End !func:OrderNumber <> 0
  ?Sheet3{prop:wizard} = 1
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?Browse:1{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,sto:Location_Key)
  BRW1.AddRange(sto:Location)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?STO:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:Location_Key)
  BRW1.AddRange(sto:Location)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?STO:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.SetFilter('(UPPER(sto:Accessory)=''YES'')')
  BRW1.AddSortOrder(,sto:Location_Key)
  BRW1.AddRange(sto:Location)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?STO:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.SetFilter('(UPPER(sto:Accessory)<<>''YES'')')
  BRW1.AddSortOrder(,sto:Description_Key)
  BRW1.AddRange(sto:Location)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?STO:Description,sto:Description,1,BRW1)
  BRW1.AddSortOrder(,sto:Description_Key)
  BRW1.AddRange(sto:Location)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?STO:Description,sto:Description,1,BRW1)
  BRW1.SetFilter('(UPPER(sto:Accessory)=''YES'')')
  BRW1.AddSortOrder(,sto:Description_Key)
  BRW1.AddRange(sto:Location)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(?STO:Description,sto:Description,1,BRW1)
  BRW1.SetFilter('(UPPER(sto:Accessory)<<>''YES'')')
  BRW1.AddSortOrder(,sto:Location_Key)
  BRW1.AddRange(sto:Location)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(?sto:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.SetFilter('(UPPER(sto:ExchangeUnit)=''YES'')')
  BRW1.AddSortOrder(,sto:Description_Key)
  BRW1.AddRange(sto:Location)
  BRW1.AddLocator(BRW1::Sort8:Locator)
  BRW1::Sort8:Locator.Init(?sto:Description,sto:Description,1,BRW1)
  BRW1.SetFilter('(UPPER(sto:ExchangeUnit)=''YES'')')
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,sto:Location_Part_Description_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,sto:Location,1,BRW1)
  BRW1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  BRW1.AddField(sto:Description,BRW1.Q.sto:Description)
  BRW1.AddField(sto:Shelf_Location,BRW1.Q.sto:Shelf_Location)
  BRW1.AddField(sto:Second_Location,BRW1.Q.sto:Second_Location)
  BRW1.AddField(sto:Quantity_Stock,BRW1.Q.sto:Quantity_Stock)
  BRW1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  BRW1.AddField(sto:Location,BRW1.Q.sto:Location)
  BRW8.Q &= Queue:Browse
  BRW8.RetainRow = 0
  BRW8.AddSortOrder(,res:Part_Number_Key)
  BRW8.AddRange(res:Ref_Number,ret:Ref_Number)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,res:Part_Number,1,BRW8)
  BIND('line_cost_temp',line_cost_temp)
  BRW8.AddField(res:Part_Number,BRW8.Q.res:Part_Number)
  BRW8.AddField(res:Description,BRW8.Q.res:Description)
  BRW8.AddField(res:Item_Cost,BRW8.Q.res:Item_Cost)
  BRW8.AddField(res:Quantity,BRW8.Q.res:Quantity)
  BRW8.AddField(line_cost_temp,BRW8.Q.line_cost_temp)
  BRW8.AddField(res:Record_Number,BRW8.Q.res:Record_Number)
  BRW8.AddField(res:Ref_Number,BRW8.Q.res:Ref_Number)
  BRW7.Q &= Queue:Browse:2
  BRW7.RetainRow = 0
  BRW7.AddSortOrder(,ori:Keyordhno)
  BRW7.AddRange(ori:ordhno,tmp:OrderNumber)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,ori:ordhno,1,BRW7)
  BRW7.AddField(ori:partno,BRW7.Q.ori:partno)
  BRW7.AddField(ori:partdiscription,BRW7.Q.ori:partdiscription)
  BRW7.AddField(ori:qty,BRW7.Q.ori:qty)
  BRW7.AddField(sto:Second_Location,BRW7.Q.sto:Second_Location)
  BRW7.AddField(sto:Quantity_Stock,BRW7.Q.sto:Quantity_Stock)
  BRW7.AddField(sto:Shelf_Location,BRW7.Q.sto:Shelf_Location)
  BRW7.AddField(sto:Suspend,BRW7.Q.sto:Suspend)
  BRW7.AddField(sto:ExchangeUnit,BRW7.Q.sto:ExchangeUnit)
  BRW7.AddField(sto:Accessory,BRW7.Q.sto:Accessory)
  BRW7.AddField(sto:Sundry_Item,BRW7.Q.sto:Sundry_Item)
  BRW7.AddField(ori:recordnumber,BRW7.Q.ori:recordnumber)
  BRW7.AddField(ori:ordhno,BRW7.Q.ori:ordhno)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW8.AskProcedure = 1
  FDCB5.Init(Location_Temp,?Location_Temp,Queue:FileDropCombo.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo
  FDCB5.AddSortOrder(loc:Location_Key)
  FDCB5.AddField(loc:Location,FDCB5.Q.loc:Location)
  FDCB5.AddField(loc:RecordNumber,FDCB5.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
    Relate:LOCATION.Close
    Relate:ORDITEMS.Close
    Relate:RETSALES.Close
    Relate:RETSTOCK_ALIAS.Close
    Relate:VATCODE.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='Stock_Enquiry'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Stock_Enquiry',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  !Has this part already been entered
  do_update# = 1
  tmp:WebOrderNumber = 0
  
  If request  = Insertrecord
      brw1.UpdateBuffer()
      brw7.UpdateBuffer()
      ! Start Change 2326 BE(13/03/03)
      suspend# = 0
      ! End Change 2326 BE(13/03/03)
      Case Choice(?Sheet3)
          Of 1
              ! Start Change 2326 BE(13/03/03)
              save_sto_id = Access:STOCK.SaveFile()
              ! Fetch Current Stock Record by Location/Part No
              ! because sto:suspend is not included in Browse View
              !
              ! NOTE:  This will check the current location for Suspend Status
              !        unlike the Web Processing below which always checks
              !        the Main Store Location
              !
              If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                  !Found
  
              Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
              suspend# = sto:suspend
              Access:STOCK.RestoreFile(save_sto_id)
              ! End Change 2326 BE(13/03/03)
          Of 2
              tmp:WebOrderNumber = ori:RecordNumber
              Access:STOCK.ClearKey(sto:Location_Key)
              sto:Location    = MainStoreLocation()
              sto:Part_Number = ori:PartNo
              If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                  !Found
  
              Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
              ! Start Change 2326 BE(13/03/03)
              suspend# = sto:suspend
              ! End Change 2326 BE(13/03/03)
      End !Case Choice(?Sheet3)
  
      ! Start Change 2326 BE(13/03/03)
      !If sto:Suspend
      IF (suspend#) THEN
      ! End Change 2326 BE(13/03/03)
          do_update# = 0
          Access:RETSTOCK.Cancelautoinc()
          Case MessageEx('This part cannot be used. It has been suspended.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else !If sto:Suspend
          If ret:Payment_Method <> 'EXC' and sto:ExchangeUnit = 'YES'
              Case MessageEx('Warning! This part is an Exchange Accessory. '&|
                '<13,10>'&|
                '<13,10>Are you sure you want to use it?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                  Of 2 ! &No Button
                      do_update# = 0
                      Access:RETSTOCK.CancelAutoinc()
              End!Case MessageEx
          Else !If ret:PaymentMethod = 'EXC' and sto:ExchangeUnit = 'YES'
  
          End !If ret:PaymentMethod = 'EXC' and sto:ExchangeUnit = 'YES'
  
          If do_Update# = 1
              save_ret_ali_id = access:retstock_alias.savefile()
              access:retstock_alias.clearkey(ret_ali:part_number_key)
              ret_ali:ref_number  = ret:ref_number
              ret_ali:part_number = sto:part_number
              set(ret_ali:part_number_key,ret_ali:part_number_key)
              loop
                  if access:retstock_alias.next()
                     break
                  end !if
                  if ret_ali:ref_number  <> ret:ref_number      |
                  or ret_ali:part_number <> sto:part_number      |
                      then break.  ! end if
                  found# = 1
                  Break
              end !loop
              access:retstock_alias.restorefile(save_ret_ali_id)
  
              If found# = 1
                  Case MessageEx('The Selected Part has already been attached to this Sale.<13,10><13,10>Are you sure you want to add another one?','ServiceBase 2000','styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                      Of 2 ! &No Button
                          do_update# = 0
                          access:retstock.cancelautoinc()
                  End!Case MessagheEx
              End!If found# = 1
  
          End !If do_Update# = 1
      End !If sto:Suspend
  
  End!If request  = Insertrecord
  
  If request  = Deleterecord
      do_update# = 0
      do_delete# = 0
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number = res:part_ref_number
      if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
          If ret:Payment_Method = 'EXC' and sto:Accessory <> 'YES'
  
          Else !If ret:Payment_Method = 'EXC' and sto:Accessory <> 'YES'
  
          End !If ret:Payment_Method = 'EXC' and sto:Accessory <> 'YES'
          If sto:sundry_item = 'YES'
              do_Delete# = 1
          Else!If sto:sundry_item = 'YES'
              If res:Despatched = 'PEN'
  
                  !Try and find if a Pending Order exists for this part number,
                  !if so decrement the quantity, otherwise cancel the part.
  
                  Found# = 0
                  Save_ope_ID = Access:ORDPEND.SaveFile()
                  Access:ORDPEND.ClearKey(ope:Supplier_Key)
                  ope:Supplier    = res:Supplier
                  ope:Part_Number = res:Part_Number
                  Set(ope:Supplier_Key,ope:Supplier_Key)
                  Loop
                      If Access:ORDPEND.NEXT()
                         Break
                      End !If
                      If ope:Supplier    <> res:Supplier  |
                      Or ope:Part_Number <> res:Part_Number      |
                          Then Break.  ! End If
                      !Only affect Pending Orders that have enough quantity,
                      !other wise we need to cancel the part.
                      If ope:Description = res:Description and ope:Quantity >= res:Quantity
                          Found# = 1
                          Case MessageEx('This part has been requested for ordering.'&|
                            '<13,10>Do you wish to remove this quantity from the order request?','ServiceBase 2000',|
                                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                              Of 1 ! &Yes Button
                                  If ope:Quantity = res:Quantity
                                      Delete(ORDPEND)
                                  Else !If ope:Quanity = res:Quantity
                                      ope:Quantity -= res:Quantity
                                      Access:ORDPEND.Update()
                                  End !If ope:Quanity = res:Quantity
                                  Do_Delete# = 1
                              Of 2 ! &No Button
                                  Do_Delete# = 0
                          End!Case MessageEx
                          Break
                      End !If ope:Description = res:Description
                  End !Loop
                  Access:ORDPEND.RestoreFile(Save_ope_ID)
  
                  If Found# = 0
                      Case MessageEx('This part has been requested for Order, but a Pending Order cannot be found. This usually means that the Order has already been raised.'&|
                        '<13,10>'&|
                        '<13,10>If you continue this part request will be cancelled.'&|
                        '<13,10>'&|
                        '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
                                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
                              res:Despatched = 'CAN'
                              Access:RETSTOCK.Update()
                              Do_Delete# = 0
                          Of 2 ! &No Button
                              Do_Delete# = 0
                      End!Case MessageEx
                  End !If Found# = 0
              Else !If res:Despatch = 'PEN'
                  !Do not allow to delete Cancelled Parts
                  If res:Despatched = 'CAN'
                      Case MessageEx('You cannot delete a cancelled part.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      Do_Delete# = 0
                  Else !If res:Despatch = 'CAN'
                      Case MessageEx('Do you want to delete this part?'&|
                        '<13,10>'&|
                        '<13,10>The stock will be recredited.','ServiceBase 2000',|
                                     'Styles\trash.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,48,msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
                              sto:Quantity_Stock += res:Quantity
                              If access:stock.update() = Level:Benign
                                  do_delete# = 1
                                  get(stohist,0)
                                  if access:stohist.primerecord() = level:benign
                                      shi:ref_number           = sto:ref_number
                                      access:users.clearkey(use:password_key)
                                      use:password              =glo:password
                                      access:users.fetch(use:password_key)
                                      shi:user                  = use:user_code    
                                      shi:date                 = today()
                                      shi:transaction_type     = 'ADD'
                                      shi:despatch_note_number = ''
                                      shi:job_number           = ret:ref_number
                                      shi:quantity             = res:quantity
                                      shi:purchase_cost        = res:purchase_cost
                                      shi:sale_cost            = res:sale_cost
                                      shi:retail_cost           = res:retail_cost
                                      shi:information          = 'RETAIL PART REMOVED FROM SALE'
                                      shi:notes                = 'PART EXISTS ON ORDER NUMBER: ' & res:order_number
                                      access:stohist.insert()
                                  end!if access:stohist.primerecord() = level:benign
                              End!If access:stock.update() = Level:Benign
  
                          Of 2 ! &No Button
                      End!Case MessageEx
                  End !If res:Despatch = 'CAN'
              End !If res:Despatch = 'PEN'
  
          End!If sto:sundry_item = 'YES'
      end!if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
      If do_delete# = 1
          Delete(retstock)
      End!If do_delete# = 1
  
  
  End!If request  = Deleterecord
  
  IF do_update# = 1
  
      glo:select1    = sto:ref_number
      glo:select2  = ''
      glo:select3  = ''
      glo:select4  = ''
      glo:select5  = ''
      glo:select6 = ''
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Retail_Stock(tmp:WebOrderNumber,0)
    ReturnValue = GlobalResponse
  END
  End!IF do_update# = 1
  !glo:Select2 = Flag
  !glo:Select3 = Stock Ref number
  !glo:Select4 = RETSTOCK record number
  !glo:Select5 = Quantity
  !glo:Select6 = OPDPEND record number, not needed anymore
  If glo:select2   = 'NEW PENDING' or glo:Select2 = 'CANCELLED'
      UpdateRetailPartRoutine()
   End !If glo:select2   = 'NEW PENDING'
  If glo:Select7 = 'DELETE'
      Access:RETSTOCK.ClearKey(res:Record_Number_Key)
      res:Record_Number = glo:Select4
      If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
          !Found
          Delete(RETSTOCK)
      Else!If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
  End !If glo:Select7 = 'DELETE'
  glo:select1     = ''
  glo:Select2     = ''
  glo:Select3     = ''
  glo:Select5     = ''
  glo:Select4     = ''
  glo:Select7     = ''
  Select(?browse:1)
  BRW1.ResetSort(1)
  BRW7.ResetSort(1)
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Location_Temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?accessory_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?accessory_temp, Accepted)
      brw1.resetsort(1)
      thiswindow.reset
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?accessory_temp, Accepted)
    OF ?Generate_Stock_Order
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Generate_Stock_Order, Accepted)
      check_access('STOCK - ORDER STOCK',x")
      if x" = false
              Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
              End!Case MessageEx
      Else!if x" = false
          Thiswindow.reset
          error# = 0
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = brw1.q.sto:ref_number
          if access:stock.fetch(sto:ref_number_key)
              error# = 1
          Else!if access:stock.fetch(sto:ref_number_key)
              If sto:sundry_item = 'YES'
                              Case MessageEx('Cannot Order!<13,10><13,10>This item has been marked as a Sundry Item.','ServiceBase 2000',|
                                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                      Of 1 ! &OK Button
                              End!Case MessageEx
                  error# = 1
              End!If sto:sundry_item = 'YES'
          end!if access:stock.fetch(sto:ref_number_key)
      
          If error# = 0
              glo:select1 = brw1.q.sto:ref_number
              Stock_Order
              glo:select1 = ''
          End!If error# = 0
      
      End!Else!if x" = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Generate_Stock_Order, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Stock_Enquiry')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:Drag
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, Drag)
      If dragid()
          setdropid(1)
      End!If dragid()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, Drag)
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:Drop
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, Drop)
      If dropid()
          post(event:accepted,?Insert)
      End!If dropid()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, Drop)
    END
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      If keycode() = EnterKey Or keycode() = MouseLeft2 Or keycode() = Insertkey
          Post(Event:accepted,?insert)
      End!If keycode() = EnterKey Or keycode() = MouseLeft2
      If keycode() = HomeKey
          Select(?Browse:1)
          Presskey(ctrlhome)
          Select(?browse:1)
      End!If keycode() = DeleteKey
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, AlertKey)
      If keycode() = EnterKey Or keycode() = MouseLeft2 Or keycode() = Insertkey
          Post(Event:accepted,?insert)
      End!If keycode() = EnterKey Or keycode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Location_Temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      set(defstock)
      access:defstock.next()
      If DST:Use_Site_Location = 'YES'
          location_temp   = dst:site_location
          Display()
          brw1.resetsort(1)
      Else
          Select(?location_temp)
      End!If dst:site_location <> ''
      
      
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:Timer
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Timer)
      !Totals
          ret:Sub_Total   = ret:courier_cost + ret:parts_cost
          VAT_Temp        = Round((ret:courier_cost * vat_rate_temp/100),.01) + Round((ret:parts_cost * vat_rate_parts_temp/100),.01)
          Total_Temp      = ret:sub_total + Vat_Temp
          Courier_Euro    = ret:courier_cost * Def:EuroRate
          Item_Euro       = ret:parts_cost * Def:EuroRate
          Sub_Euro        = ret:Sub_Total * Def:EuroRate
          Vat_Euro        = VAT_Temp * Def:EuroRate
          Total_Euro      = Total_Temp * Def:EuroRate
          Display(?ret:sub_total)
          Display(?vat_temp)
          Display(?total_temp)
          DISPLAY()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Timer)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL' And Upper(ret:Payment_Method) <> 'EXC'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'YES' And Upper(ret:Payment_Method) <> 'EXC'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'NO' And Upper(ret:Payment_Method) <> 'EXC'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL' And Upper(ret:Payment_Method) <> 'EXC'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'YES' And Upper(ret:Payment_Method) <> 'EXC'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'NO' And Upper(ret:Payment_Method) <> 'EXC'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
  ELSIF Choice(?CurrentTab) = 1 And Upper(ret:Payment_Method) = 'EXC'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(ret:Payment_Method) = 'EXC'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
  ELSE
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL' And Upper(ret:Payment_Method) <> 'EXC'
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'YES' And Upper(ret:Payment_Method) <> 'EXC'
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'NO' And Upper(ret:Payment_Method) <> 'EXC'
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL' And Upper(ret:Payment_Method) <> 'EXC'
    RETURN SELF.SetSort(4,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'YES' And Upper(ret:Payment_Method) <> 'EXC'
    RETURN SELF.SetSort(5,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'NO' And Upper(ret:Payment_Method) <> 'EXC'
    RETURN SELF.SetSort(6,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(ret:Payment_Method) = 'EXC'
    RETURN SELF.SetSort(7,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(ret:Payment_Method) = 'EXC'
    RETURN SELF.SetSort(8,Force)
  ELSE
    RETURN SELF.SetSort(9,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW8.ResetFromView PROCEDURE

ret:Parts_Cost:Sum   REAL
tmp:BackOrderTotal:Sum REAL
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:RETSTOCK.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    IF (res:Despatched = 'PIK')
      ret:Parts_Cost:Sum += line_cost_temp
    END
    IF (res:Despatched <> 'PIK')
      tmp:BackOrderTotal:Sum += line_cost_temp
    END
  END
  ret:Parts_Cost = ret:Parts_Cost:Sum
  tmp:BackOrderTotal = tmp:BackOrderTotal:Sum
  PARENT.ResetFromView
  Relate:RETSTOCK.SetQuickScan(0)
  SETCURSOR()


BRW8.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, ResetSort, (BYTE Force),BYTE)
  ReturnValue = PARENT.ResetSort(Force)
  Select(?browse:1)
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, ResetSort, (BYTE Force),BYTE)
  RETURN ReturnValue


BRW8.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, SetQueueRecord, ())
  line_cost_temp = Round(Round(RES:Item_Cost,.01) * RES:Quantity,.01)
  PARENT.SetQueueRecord
  IF (res:despatched = 'PEN')
    SELF.Q.res:Part_Number_NormalFG = 32768
    SELF.Q.res:Part_Number_NormalBG = 16777215
    SELF.Q.res:Part_Number_SelectedFG = 16777215
    SELF.Q.res:Part_Number_SelectedBG = 32768
  ELSE
    SELF.Q.res:Part_Number_NormalFG = -1
    SELF.Q.res:Part_Number_NormalBG = -1
    SELF.Q.res:Part_Number_SelectedFG = -1
    SELF.Q.res:Part_Number_SelectedBG = -1
  END
  IF (res:despatched = 'PEN')
    SELF.Q.res:Description_NormalFG = 32768
    SELF.Q.res:Description_NormalBG = 16777215
    SELF.Q.res:Description_SelectedFG = 16777215
    SELF.Q.res:Description_SelectedBG = 32768
  ELSE
    SELF.Q.res:Description_NormalFG = -1
    SELF.Q.res:Description_NormalBG = -1
    SELF.Q.res:Description_SelectedFG = -1
    SELF.Q.res:Description_SelectedBG = -1
  END
  IF (res:despatched = 'PEN')
    SELF.Q.res:Item_Cost_NormalFG = 32768
    SELF.Q.res:Item_Cost_NormalBG = 16777215
    SELF.Q.res:Item_Cost_SelectedFG = 16777215
    SELF.Q.res:Item_Cost_SelectedBG = 32768
  ELSE
    SELF.Q.res:Item_Cost_NormalFG = -1
    SELF.Q.res:Item_Cost_NormalBG = -1
    SELF.Q.res:Item_Cost_SelectedFG = -1
    SELF.Q.res:Item_Cost_SelectedBG = -1
  END
  IF (res:despatched = 'PEN')
    SELF.Q.res:Quantity_NormalFG = 32768
    SELF.Q.res:Quantity_NormalBG = 16777215
    SELF.Q.res:Quantity_SelectedFG = 16777215
    SELF.Q.res:Quantity_SelectedBG = 32768
  ELSE
    SELF.Q.res:Quantity_NormalFG = -1
    SELF.Q.res:Quantity_NormalBG = -1
    SELF.Q.res:Quantity_SelectedFG = -1
    SELF.Q.res:Quantity_SelectedBG = -1
  END
  IF (res:despatched = 'PEN')
    SELF.Q.line_cost_temp_NormalFG = 32768
    SELF.Q.line_cost_temp_NormalBG = 16777215
    SELF.Q.line_cost_temp_SelectedFG = 16777215
    SELF.Q.line_cost_temp_SelectedBG = 32768
  ELSE
    SELF.Q.line_cost_temp_NormalFG = -1
    SELF.Q.line_cost_temp_NormalBG = -1
    SELF.Q.line_cost_temp_SelectedFG = -1
    SELF.Q.line_cost_temp_SelectedBG = -1
  END
   
   
   IF (res:Despatched = 'PEN')
     SELF.Q.res:Part_Number_NormalFG = 32768
     SELF.Q.res:Part_Number_NormalBG = 16777215
     SELF.Q.res:Part_Number_SelectedFG = 16777215
     SELF.Q.res:Part_Number_SelectedBG = 32768
   ELSIF(res:Despatched = 'CAN')
     SELF.Q.res:Part_Number_NormalFG = 16777215
     SELF.Q.res:Part_Number_NormalBG = 0
     SELF.Q.res:Part_Number_SelectedFG = 0
     SELF.Q.res:Part_Number_SelectedBG = 16777215
   ELSE
     SELF.Q.res:Part_Number_NormalFG = 0
     SELF.Q.res:Part_Number_NormalBG = 16777215
     SELF.Q.res:Part_Number_SelectedFG = 16777215
     SELF.Q.res:Part_Number_SelectedBG = 8388608
   END
   IF (res:Despatched = 'PEN')
     SELF.Q.res:Description_NormalFG = 32768
     SELF.Q.res:Description_NormalBG = 16777215
     SELF.Q.res:Description_SelectedFG = 16777215
     SELF.Q.res:Description_SelectedBG = 32768
   ELSIF(res:Despatched = 'CAN')
     SELF.Q.res:Description_NormalFG = 16777215
     SELF.Q.res:Description_NormalBG = 0
     SELF.Q.res:Description_SelectedFG = 0
     SELF.Q.res:Description_SelectedBG = 16777215
   ELSE
     SELF.Q.res:Description_NormalFG = 0
     SELF.Q.res:Description_NormalBG = 16777215
     SELF.Q.res:Description_SelectedFG = 16777215
     SELF.Q.res:Description_SelectedBG = 8388608
   END
   IF (res:Despatched = 'PEN')
     SELF.Q.res:Item_Cost_NormalFG = 32768
     SELF.Q.res:Item_Cost_NormalBG = 16777215
     SELF.Q.res:Item_Cost_SelectedFG = 16777215
     SELF.Q.res:Item_Cost_SelectedBG = 32768
   ELSIF(res:Despatched = 'CAN')
     SELF.Q.res:Item_Cost_NormalFG = 16777215
     SELF.Q.res:Item_Cost_NormalBG = 0
     SELF.Q.res:Item_Cost_SelectedFG = 0
     SELF.Q.res:Item_Cost_SelectedBG = 16777215
   ELSE
     SELF.Q.res:Item_Cost_NormalFG = 0
     SELF.Q.res:Item_Cost_NormalBG = 16777215
     SELF.Q.res:Item_Cost_SelectedFG = 16777215
     SELF.Q.res:Item_Cost_SelectedBG = 8388608
   END
   IF (res:Despatched = 'PEN')
     SELF.Q.res:Quantity_NormalFG = 32768
     SELF.Q.res:Quantity_NormalBG = 16777215
     SELF.Q.res:Quantity_SelectedFG = 16777215
     SELF.Q.res:Quantity_SelectedBG = 32768
   ELSIF(res:Despatched = 'CAN')
     SELF.Q.res:Quantity_NormalFG = 16777215
     SELF.Q.res:Quantity_NormalBG = 0
     SELF.Q.res:Quantity_SelectedFG = 0
     SELF.Q.res:Quantity_SelectedBG = 16777215
   ELSE
     SELF.Q.res:Quantity_NormalFG = 0
     SELF.Q.res:Quantity_NormalBG = 16777215
     SELF.Q.res:Quantity_SelectedFG = 16777215
     SELF.Q.res:Quantity_SelectedBG = 8388608
   END
   IF (res:Despatched = 'PEN')
     SELF.Q.line_cost_temp_NormalFG = 32768
     SELF.Q.line_cost_temp_NormalBG = 16777215
     SELF.Q.line_cost_temp_SelectedFG = 16777215
     SELF.Q.line_cost_temp_SelectedBG = 32768
   ELSIF(res:Despatched = 'CAN')
     SELF.Q.line_cost_temp_NormalFG = 16777215
     SELF.Q.line_cost_temp_NormalBG = 0
     SELF.Q.line_cost_temp_SelectedFG = 0
     SELF.Q.line_cost_temp_SelectedBG = 16777215
   ELSE
     SELF.Q.line_cost_temp_NormalFG = 0
     SELF.Q.line_cost_temp_NormalBG = 16777215
     SELF.Q.line_cost_temp_SelectedFG = 16777215
     SELF.Q.line_cost_temp_SelectedBG = 8388608
   END
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, SetQueueRecord, ())


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW7.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(7, SetQueueRecord, ())
  Access:STOCK.ClearKey(sto:Location_Key)
  sto:Location    = MainStoreLocation()
  sto:Part_Number = ori:PartNo
  If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
      !Found
      tmp:ShelfLocation   = sto:Shelf_Location
      tmp:SecondLocation  = sto:Second_Location
      tmp:Sundry          = sto:Sundry_Item
      tmp:Accessory       = sto:Accessory
      tmp:ExchangeUnit    = sto:ExchangeUnit
      tmp:Suspend         = sto:Suspend
  Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
      tmp:ShelfLocation   = ''
      tmp:SecondLocation  = ''
      tmp:Sundry          = ''
      tmp:Accessory       = ''
      tmp:ExchangeUnit    = ''
      tmp:Suspend         = ''
  End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
  PARENT.SetQueueRecord
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(7, SetQueueRecord, ())


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


FDCB5.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1

