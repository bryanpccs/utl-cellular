

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01005.INC'),ONCE        !Local module procedure declarations
                     END


Update_Retail_Stock PROCEDURE (func:RecordNumber,func:Type) !Generated from procedure template - Window

CurrentTab           STRING(80)
save_sup_id          USHORT,AUTO
EuroRate             REAL
Use_Euro             BYTE
quantity_temp        LONG
save_ope_id          USHORT,AUTO
save_orp_id          USHORT,AUTO
vat_rate_temp        REAL
ActionMessage        CSTRING(40)
cost_temp            REAL
Item_Total_Temp      REAL
Quantity_Stock_Temp  LONG
Quantity_To_Order_Temp LONG
Quantity_On_Order_Temp REAL
Quantity_Required_Temp LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Euro_Item            REAL
Euro_Total           REAL
tmp:TakeFromStock    BYTE(0)
History::res:Record  LIKE(res:RECORD),STATIC
QuickWindow          WINDOW('Update the RETSTOCK File'),AT(,,247,234),FONT('Tahoma',8,,),CENTER,IMM,HLP('Update_Retail_Stock'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,240,200),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Part Number'),AT(8,20),USE(?Part_Number),FONT(,12,,)
                           STRING(@s25),AT(84,20),USE(res:Part_Number),FONT(,12,,FONT:bold)
                           PROMPT('Description'),AT(8,36),USE(?Description)
                           STRING(@s30),AT(84,36),USE(res:Description),FONT(,10,,FONT:bold)
                           STRING(@s30),AT(84,48),USE(res:Supplier),FONT(,10,,FONT:bold)
                           PROMPT('Quantity In Stock'),AT(8,64),USE(?Description:3)
                           STRING(@N8),AT(88,64,60,8),USE(Quantity_Stock_Temp),RIGHT,FONT(,10,,FONT:bold)
                           STRING(@N8),AT(88,76,60,8),USE(Quantity_To_Order_Temp),RIGHT,FONT(,10,,FONT:bold)
                           PROMPT('Quantity To Order'),AT(8,76),USE(?Description:4)
                           PROMPT('Quantity On Order'),AT(8,88),USE(?Description:5)
                           STRING(@N8),AT(88,88,60,8),USE(Quantity_On_Order_Temp),RIGHT,FONT(,10,,FONT:bold)
                           STRING(@n8),AT(88,100,60,8),USE(sto:Minimum_Level),RIGHT,FONT(,10,,FONT:bold)
                           PROMPT('Accesory Cost'),AT(8,116),USE(?res:AccessoryCost:Prompt),TRN,HIDE
                           ENTRY(@n14.2),AT(84,116,64,10),USE(res:AccessoryCost),HIDE,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Accesory Cost'),TIP('Accesory Cost'),UPR
                           PROMPT('Minimum Stock Level'),AT(8,100),USE(?Description:6)
                           PROMPT('Item Cost'),AT(8,132),USE(?Item_Cost),TRN
                           ENTRY(@n14.2),AT(84,132,64,10),USE(res:Item_Cost),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('<128>'),AT(168,132),USE(?Euro_Item:Prompt),HIDE
                           ENTRY(@n10.2),AT(176,132,64,10),USE(Euro_Item),HIDE,DECIMAL(14),FONT(,,,FONT:bold)
                           PROMPT('<128>'),AT(168,116),USE(?Euro_Item:Prompt:2),HIDE
                           ENTRY(@n10.2),AT(176,116,64,10),USE(Euro_Item,,?Euro_Item:2),HIDE,DECIMAL(14),FONT(,,,FONT:bold)
                           PROMPT('Quantity Required'),AT(8,148),USE(?Quantity_Required_Temp:Prompt)
                           SPIN(@n8),AT(84,148,64,10),USE(Quantity_Required_Temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,RANGE(1,999999),STEP(1)
                           PROMPT('Supplier'),AT(8,48),USE(?Description:2)
                           PROMPT('Quantity To Ship'),AT(8,164),USE(?RES:Quantity:Prompt),TRN
                           SPIN(@n8),AT(84,164,64,10),USE(res:Quantity),RIGHT,FONT(,,,FONT:bold),UPR,RANGE(0,9999999),STEP(1)
                           PROMPT('<128>'),AT(168,180),USE(?Euro_Total:Prompt),HIDE
                           ENTRY(@n10.2),AT(176,180,64,10),USE(Euro_Total),HIDE,DECIMAL(14),FONT(,,,FONT:bold)
                           PROMPT('Item Total (Shipped)'),AT(8,180),USE(?Item_Total_Temp:Prompt),TRN
                           ENTRY(@n14.2),AT(84,180,64,10),USE(Item_Total_Temp),SKIP,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),READONLY
                         END
                       END
                       BUTTON('&OK'),AT(128,212,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(184,212,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,208,240,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
    Map
LocalCreatePendingPart          Procedure(Long  func:Quantity)
    End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?Part_Number{prop:FontColor} = -1
    ?Part_Number{prop:Color} = 15066597
    ?res:Part_Number{prop:FontColor} = -1
    ?res:Part_Number{prop:Color} = 15066597
    ?Description{prop:FontColor} = -1
    ?Description{prop:Color} = 15066597
    ?res:Description{prop:FontColor} = -1
    ?res:Description{prop:Color} = 15066597
    ?res:Supplier{prop:FontColor} = -1
    ?res:Supplier{prop:Color} = 15066597
    ?Description:3{prop:FontColor} = -1
    ?Description:3{prop:Color} = 15066597
    ?Quantity_Stock_Temp{prop:FontColor} = -1
    ?Quantity_Stock_Temp{prop:Color} = 15066597
    ?Quantity_To_Order_Temp{prop:FontColor} = -1
    ?Quantity_To_Order_Temp{prop:Color} = 15066597
    ?Description:4{prop:FontColor} = -1
    ?Description:4{prop:Color} = 15066597
    ?Description:5{prop:FontColor} = -1
    ?Description:5{prop:Color} = 15066597
    ?Quantity_On_Order_Temp{prop:FontColor} = -1
    ?Quantity_On_Order_Temp{prop:Color} = 15066597
    ?sto:Minimum_Level{prop:FontColor} = -1
    ?sto:Minimum_Level{prop:Color} = 15066597
    ?res:AccessoryCost:Prompt{prop:FontColor} = -1
    ?res:AccessoryCost:Prompt{prop:Color} = 15066597
    If ?res:AccessoryCost{prop:ReadOnly} = True
        ?res:AccessoryCost{prop:FontColor} = 65793
        ?res:AccessoryCost{prop:Color} = 15066597
    Elsif ?res:AccessoryCost{prop:Req} = True
        ?res:AccessoryCost{prop:FontColor} = 65793
        ?res:AccessoryCost{prop:Color} = 8454143
    Else ! If ?res:AccessoryCost{prop:Req} = True
        ?res:AccessoryCost{prop:FontColor} = 65793
        ?res:AccessoryCost{prop:Color} = 16777215
    End ! If ?res:AccessoryCost{prop:Req} = True
    ?res:AccessoryCost{prop:Trn} = 0
    ?res:AccessoryCost{prop:FontStyle} = font:Bold
    ?Description:6{prop:FontColor} = -1
    ?Description:6{prop:Color} = 15066597
    ?Item_Cost{prop:FontColor} = -1
    ?Item_Cost{prop:Color} = 15066597
    If ?res:Item_Cost{prop:ReadOnly} = True
        ?res:Item_Cost{prop:FontColor} = 65793
        ?res:Item_Cost{prop:Color} = 15066597
    Elsif ?res:Item_Cost{prop:Req} = True
        ?res:Item_Cost{prop:FontColor} = 65793
        ?res:Item_Cost{prop:Color} = 8454143
    Else ! If ?res:Item_Cost{prop:Req} = True
        ?res:Item_Cost{prop:FontColor} = 65793
        ?res:Item_Cost{prop:Color} = 16777215
    End ! If ?res:Item_Cost{prop:Req} = True
    ?res:Item_Cost{prop:Trn} = 0
    ?res:Item_Cost{prop:FontStyle} = font:Bold
    ?Euro_Item:Prompt{prop:FontColor} = -1
    ?Euro_Item:Prompt{prop:Color} = 15066597
    If ?Euro_Item{prop:ReadOnly} = True
        ?Euro_Item{prop:FontColor} = 65793
        ?Euro_Item{prop:Color} = 15066597
    Elsif ?Euro_Item{prop:Req} = True
        ?Euro_Item{prop:FontColor} = 65793
        ?Euro_Item{prop:Color} = 8454143
    Else ! If ?Euro_Item{prop:Req} = True
        ?Euro_Item{prop:FontColor} = 65793
        ?Euro_Item{prop:Color} = 16777215
    End ! If ?Euro_Item{prop:Req} = True
    ?Euro_Item{prop:Trn} = 0
    ?Euro_Item{prop:FontStyle} = font:Bold
    ?Euro_Item:Prompt:2{prop:FontColor} = -1
    ?Euro_Item:Prompt:2{prop:Color} = 15066597
    If ?Euro_Item:2{prop:ReadOnly} = True
        ?Euro_Item:2{prop:FontColor} = 65793
        ?Euro_Item:2{prop:Color} = 15066597
    Elsif ?Euro_Item:2{prop:Req} = True
        ?Euro_Item:2{prop:FontColor} = 65793
        ?Euro_Item:2{prop:Color} = 8454143
    Else ! If ?Euro_Item:2{prop:Req} = True
        ?Euro_Item:2{prop:FontColor} = 65793
        ?Euro_Item:2{prop:Color} = 16777215
    End ! If ?Euro_Item:2{prop:Req} = True
    ?Euro_Item:2{prop:Trn} = 0
    ?Euro_Item:2{prop:FontStyle} = font:Bold
    ?Quantity_Required_Temp:Prompt{prop:FontColor} = -1
    ?Quantity_Required_Temp:Prompt{prop:Color} = 15066597
    If ?Quantity_Required_Temp{prop:ReadOnly} = True
        ?Quantity_Required_Temp{prop:FontColor} = 65793
        ?Quantity_Required_Temp{prop:Color} = 15066597
    Elsif ?Quantity_Required_Temp{prop:Req} = True
        ?Quantity_Required_Temp{prop:FontColor} = 65793
        ?Quantity_Required_Temp{prop:Color} = 8454143
    Else ! If ?Quantity_Required_Temp{prop:Req} = True
        ?Quantity_Required_Temp{prop:FontColor} = 65793
        ?Quantity_Required_Temp{prop:Color} = 16777215
    End ! If ?Quantity_Required_Temp{prop:Req} = True
    ?Quantity_Required_Temp{prop:Trn} = 0
    ?Quantity_Required_Temp{prop:FontStyle} = font:Bold
    ?Description:2{prop:FontColor} = -1
    ?Description:2{prop:Color} = 15066597
    ?RES:Quantity:Prompt{prop:FontColor} = -1
    ?RES:Quantity:Prompt{prop:Color} = 15066597
    If ?res:Quantity{prop:ReadOnly} = True
        ?res:Quantity{prop:FontColor} = 65793
        ?res:Quantity{prop:Color} = 15066597
    Elsif ?res:Quantity{prop:Req} = True
        ?res:Quantity{prop:FontColor} = 65793
        ?res:Quantity{prop:Color} = 8454143
    Else ! If ?res:Quantity{prop:Req} = True
        ?res:Quantity{prop:FontColor} = 65793
        ?res:Quantity{prop:Color} = 16777215
    End ! If ?res:Quantity{prop:Req} = True
    ?res:Quantity{prop:Trn} = 0
    ?res:Quantity{prop:FontStyle} = font:Bold
    ?Euro_Total:Prompt{prop:FontColor} = -1
    ?Euro_Total:Prompt{prop:Color} = 15066597
    If ?Euro_Total{prop:ReadOnly} = True
        ?Euro_Total{prop:FontColor} = 65793
        ?Euro_Total{prop:Color} = 15066597
    Elsif ?Euro_Total{prop:Req} = True
        ?Euro_Total{prop:FontColor} = 65793
        ?Euro_Total{prop:Color} = 8454143
    Else ! If ?Euro_Total{prop:Req} = True
        ?Euro_Total{prop:FontColor} = 65793
        ?Euro_Total{prop:Color} = 16777215
    End ! If ?Euro_Total{prop:Req} = True
    ?Euro_Total{prop:Trn} = 0
    ?Euro_Total{prop:FontStyle} = font:Bold
    ?Item_Total_Temp:Prompt{prop:FontColor} = -1
    ?Item_Total_Temp:Prompt{prop:Color} = 15066597
    If ?Item_Total_Temp{prop:ReadOnly} = True
        ?Item_Total_Temp{prop:FontColor} = 65793
        ?Item_Total_Temp{prop:Color} = 15066597
    Elsif ?Item_Total_Temp{prop:Req} = True
        ?Item_Total_Temp{prop:FontColor} = 65793
        ?Item_Total_Temp{prop:Color} = 8454143
    Else ! If ?Item_Total_Temp{prop:Req} = True
        ?Item_Total_Temp{prop:FontColor} = 65793
        ?Item_Total_Temp{prop:Color} = 16777215
    End ! If ?Item_Total_Temp{prop:Req} = True
    ?Item_Total_Temp{prop:Trn} = 0
    ?Item_Total_Temp{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Item_Total      Routine
    SolaceViewVars(?res:AccessoryCost{prop:Hide},'ITEM TOTAL','',6)
    If ?res:AccessoryCost{prop:Hide} = 1
        item_total_temp = res:item_cost * res:quantity
    SolaceViewVars('Item_Total_Temp',Item_Total_Temp,'Item Cost',6)
        IF Use_Euro = TRUE
            Euro_Item = res:item_cost * EuroRate
            Euro_Total = (res:item_cost * EuroRate) * res:quantity
        END
    Else !If ?res:AccessoryCost{prop:Hide} = 1
        item_total_temp = res:AccessoryCost * res:quantity
    SolaceViewVars('Item_Total_Temp',Item_Total_Temp,'Accessory Cost',6)
        IF Use_Euro = TRUE
            Euro_Item = res:AccessoryCost * EuroRate
            Euro_Total = (res:AccessoryCost * EuroRate) * res:quantity
        END
    End !If ?res:AccessoryCost{prop:Hide} = 1
    Display(?item_total_temp)
    DISPLAY()
Check_Fields    ROUTINE
    IF Use_Euro = TRUE
        Case ?res:AccessoryCost{prop:Hide}
            Of 0
                ?Euro_Item:2{PROP:Hide} = FALSE
                ?Euro_Item:Prompt:2{PROP:Hide} = FALSE
            Of 1
                ?Euro_Item{PROP:Hide} = FALSE
                ?Euro_Item:Prompt{PROP:Hide} = FALSE
        End !Case ?res:AccessoryCost{prop:Hide}
        ?Euro_Total{PROP:Hide} = FALSE
        ?Euro_Total:Prompt{PROP:Hide} = FALSE
        
    END
    DISPLAY()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Retail_Stock',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Retail_Stock',1)
    SolaceViewVars('save_sup_id',save_sup_id,'Update_Retail_Stock',1)
    SolaceViewVars('EuroRate',EuroRate,'Update_Retail_Stock',1)
    SolaceViewVars('Use_Euro',Use_Euro,'Update_Retail_Stock',1)
    SolaceViewVars('quantity_temp',quantity_temp,'Update_Retail_Stock',1)
    SolaceViewVars('save_ope_id',save_ope_id,'Update_Retail_Stock',1)
    SolaceViewVars('save_orp_id',save_orp_id,'Update_Retail_Stock',1)
    SolaceViewVars('vat_rate_temp',vat_rate_temp,'Update_Retail_Stock',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Retail_Stock',1)
    SolaceViewVars('cost_temp',cost_temp,'Update_Retail_Stock',1)
    SolaceViewVars('Item_Total_Temp',Item_Total_Temp,'Update_Retail_Stock',1)
    SolaceViewVars('Quantity_Stock_Temp',Quantity_Stock_Temp,'Update_Retail_Stock',1)
    SolaceViewVars('Quantity_To_Order_Temp',Quantity_To_Order_Temp,'Update_Retail_Stock',1)
    SolaceViewVars('Quantity_On_Order_Temp',Quantity_On_Order_Temp,'Update_Retail_Stock',1)
    SolaceViewVars('Quantity_Required_Temp',Quantity_Required_Temp,'Update_Retail_Stock',1)
    SolaceViewVars('Euro_Item',Euro_Item,'Update_Retail_Stock',1)
    SolaceViewVars('Euro_Total',Euro_Total,'Update_Retail_Stock',1)
    SolaceViewVars('tmp:TakeFromStock',tmp:TakeFromStock,'Update_Retail_Stock',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Part_Number;  SolaceCtrlName = '?Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?res:Part_Number;  SolaceCtrlName = '?res:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Description;  SolaceCtrlName = '?Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?res:Description;  SolaceCtrlName = '?res:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?res:Supplier;  SolaceCtrlName = '?res:Supplier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Description:3;  SolaceCtrlName = '?Description:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Quantity_Stock_Temp;  SolaceCtrlName = '?Quantity_Stock_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Quantity_To_Order_Temp;  SolaceCtrlName = '?Quantity_To_Order_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Description:4;  SolaceCtrlName = '?Description:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Description:5;  SolaceCtrlName = '?Description:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Quantity_On_Order_Temp;  SolaceCtrlName = '?Quantity_On_Order_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Minimum_Level;  SolaceCtrlName = '?sto:Minimum_Level';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?res:AccessoryCost:Prompt;  SolaceCtrlName = '?res:AccessoryCost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?res:AccessoryCost;  SolaceCtrlName = '?res:AccessoryCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Description:6;  SolaceCtrlName = '?Description:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Item_Cost;  SolaceCtrlName = '?Item_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?res:Item_Cost;  SolaceCtrlName = '?res:Item_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Euro_Item:Prompt;  SolaceCtrlName = '?Euro_Item:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Euro_Item;  SolaceCtrlName = '?Euro_Item';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Euro_Item:Prompt:2;  SolaceCtrlName = '?Euro_Item:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Euro_Item:2;  SolaceCtrlName = '?Euro_Item:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Quantity_Required_Temp:Prompt;  SolaceCtrlName = '?Quantity_Required_Temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Quantity_Required_Temp;  SolaceCtrlName = '?Quantity_Required_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Description:2;  SolaceCtrlName = '?Description:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RES:Quantity:Prompt;  SolaceCtrlName = '?RES:Quantity:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?res:Quantity;  SolaceCtrlName = '?res:Quantity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Euro_Total:Prompt;  SolaceCtrlName = '?Euro_Total:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Euro_Total;  SolaceCtrlName = '?Euro_Total';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Item_Total_Temp:Prompt;  SolaceCtrlName = '?Item_Total_Temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Item_Total_Temp;  SolaceCtrlName = '?Item_Total_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'Viewing A Retail Item'
  OF InsertRecord
    ActionMessage = 'Insert A Retail Item'
  OF ChangeRecord
    ActionMessage = 'Changing A Retail Item'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Update_Retail_Stock')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Retail_Stock')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Part_Number
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(res:Record,History::res:Record)
  SELF.AddHistoryField(?res:Part_Number,3)
  SELF.AddHistoryField(?res:Description,4)
  SELF.AddHistoryField(?res:Supplier,5)
  SELF.AddHistoryField(?res:AccessoryCost,9)
  SELF.AddHistoryField(?res:Item_Cost,10)
  SELF.AddHistoryField(?res:Quantity,11)
  SELF.AddUpdateFile(Access:RETSTOCK)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:INVOICE.Open
  Relate:ORDITEMS.Open
  Relate:RETSALES.Open
  Relate:VATCODE.Open
  Access:STOCK.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:ORDPEND.UseFile
  Access:ORDPARTS.UseFile
  Access:LOCATION.UseFile
  Access:SUPPLIER.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:RETSTOCK
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  If thiswindow.request   = Insertrecord
      !If func:RecordNumber <> 0 then this is adding stock
      !that has been requested from a Web Order.
      !It uses the Record Number to get the original Web Order request
      !and fill in the part screen as necesary.
  
      If func:RecordNumber <> 0
          Access:ORDITEMS.ClearKey(ori:RecordNumberKey)
          ori:RecordNumber = func:RecordNumber
          If Access:ORDITEMS.TryFetch(ori:RecordNumberKey) = Level:Benign
              !Found
              Access:STOCK.ClearKey(sto:Location_Key)
              sto:Location    = MainStoreLocation()
              sto:Part_Number = ori:PartNo
              If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                  !Found
                  Quantity_Required_Temp  = ori:Qty
                  !Used for the check at completion
                  glo:Select1 = sto:Ref_Number
              Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
          Else!If Access:ORDITEMS.TryFetch(ori:RecordNumberKey) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:ORDITEMS.TryFetch(ori:RecordNumberKey) = Level:Benign
      Else !If func:RecordNumber <> 0
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = glo:select1
          if access:stock.fetch(sto:ref_number_key)
              Return(Level:Fatal)
          End!if access:stock.fetch(sto:ref_number_key)
  
      End !If func:RecordNumber <> 0
  
      res:Part_Number   = sto:Part_Number
      res:Description   = sto:Description
      res:Supplier      = sto:Supplier
      res:Purchase_Cost = sto:Purchase_Cost
      res:Sale_Cost     = sto:Sale_Cost
      res:Retail_Cost   = sto:Retail_Cost
      res:AccessoryCost   = sto:AccessoryCost
  
      RES:Part_Ref_Number = sto:ref_number
  Else!!If thiswindow.request   = Insertrecord
      !Amending an existing part. They cannot change the quantity required.
      !Only the quantity to ship can now be changed.
  
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number  = res:part_ref_number
      access:stock.fetch(sto:ref_number_key)
      Hide(?Quantity_Required_Temp)
      Hide(?Quantity_Required_Temp:prompt)
  End!If thiswindow.request   = Insertrecord
  
  Use_Euro = FALSE
  SET(DEFAULTS)
  Access:DEFAULTS.Next()
  
  !Check to see if the Retail Sales has been invoiced.
  !If so you the Invoice's Euro Rate, otherwise use the Default Rate
  IF ret:Invoice_Number <> ''
    Access:invoice.ClearKey(inv:Invoice_Number_Key)
    inv:Invoice_Number = ret:Invoice_Number
    IF Access:Invoice.Fetch(inv:Invoice_Number_Key)
      EuroRate = def:EuroRate
    ELSE
      EuroRate = inv:EuroExhangeRate
    END
  ELSE
    EuroRate = def:EuroRate
  END
  
  !Exchange Sales do not have a price.
  If ret:Payment_Method = 'EXC'
      ?res:Item_Cost{prop:Hide} = 1
      ?Item_Cost{prop:Hide} = 1
      ?res:AccessoryCost{prop:Hide} = 0
      ?res:AccessoryCost:Prompt{prop:Hide} = 0
      res:item_cost = 0
      Disable(?res:Item_Cost)
  Else !ret:Payment_Method = 'EXC'
  
      !Get the Trade Account from the Sale to determine the price
      !of the parts
      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
      sub:Account_Number  = ret:Account_Number
      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          !Found
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = sub:Main_Account_Number
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              If tra:RetailZeroParts = 1
                  res:Item_Cost = 0
              Else !If tra:RetailZeroParts = 1
                  !Check the Trade Account Retail Price Structure
                  !Only applies if it is a new record
                  !'RET' = Retail Price
                  !Other = Trade Price
                  If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                      Case sub:Retail_Price_Structure
                          Of 'RET'
                              If ThisWindow.Request = InsertRecord
                                  res:Item_Cost   = res:Retail_Cost
                              End !If ThisWindow.Request = InsertRecord
                              ?Item_Cost{prop:Text} = 'Retail Price'
                          Of 'PUR'
                              If ThisWindow.Request = InsertRecord
                                  res:Item_Cost   = res:Purchase_Cost
                              End !If ThisWindow.Reqest = InsertRecord
                              ?Item_Cost{prop:Text} = 'Purchase Price'
                          Else
                              If ThisWindow.Request = InsertRecord
                                  res:Item_Cost   = res:Sale_Cost
                              End !If ThisWindows.Request = InsertRecord
                              ?Item_Cost{prop:Text} = 'Trade Price'
                      End !Case sub:Retail_Price_Structure
  
                      !Check whether Sub Account uses the Euro.
                      !This will change the screen displays to reflect this
                      If sub:EuroApplies = 1
                          use_Euro    = 1
                      End !If sub:EuroApplied = 1
  
                  Else !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                      Case tra:Retail_Price_Structure
                          Of 'RET'
                              If ThisWindow.Request = InsertRecord
                                  res:Item_Cost   = res:Retail_Cost
                              End !If ThisWindow.Reqest = InsertRecord
                              ?Item_Cost{prop:Text} = 'Retail Price'
                          Of 'PUR'
                              If ThisWindow.Request = InsertRecord
                                  res:Item_Cost   = res:Purchase_Cost
                              End !If ThisWindow.Reqest = InsertRecord
                              ?Item_Cost{prop:Text} = 'Purchase Price'
                          Else
                              If ThisWindow.Request = InsertRecord
                                  res:Item_Cost   = res:Sale_Cost
                              End !If ThisWindow.Request = InsertRecord
                              ?Item_Cost{prop:Text} = 'Trade Price'
                      End !Case tra:Retail_Price_Structure
  
                      If tra:EuroApplies = 1
                          Use_Euro = 1
                      End !If tra:EuroApplies = 1
                  End !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
              End !If tra:RetailZeroParts = 1
  
          Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          
      Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
  End !If ret:Payment_Method = 'EXC'
  
  Do Check_Fields
  Do item_total
  
  !Work How Parts Pending Orders There Are For This Part Number
  
  Setcursor(Cursor:Wait)
  Save_sup_ID = Access:SUPPLIER.SaveFile()
  Set(sup:Company_Name_Key)
  Loop
      If Access:SUPPLIER.NEXT()
         Break
      End !If
      Save_ope_ID = Access:ORDPEND.SaveFile()
      Access:ORDPEND.ClearKey(ope:Supplier_Key)
      ope:Supplier    = sup:Company_Name
      ope:Part_Number = res:Part_Number
      Set(ope:Supplier_Key,ope:Supplier_Key)
      Loop
          If Access:ORDPEND.NEXT()
             Break
          End !If
          If ope:Supplier    <> sup:Company_Name      |
          Or ope:Part_Number <> res:Part_Number      |
              Then Break.  ! End If
          quantity_to_order_temp += ope:quantity
      End !Loop
      Access:ORDPEND.RestoreFile(Save_ope_ID)
  End !Loop
  Access:SUPPLIER.RestoreFile(Save_sup_ID)
  Setcursor()
  
  !Work out how many parts have been ordered but not received
  
  Setcursor(Cursor:Wait)
  Save_orp_ID = Access:ORDPARTS.SaveFile()
  Access:ORDPARTS.ClearKey(orp:Received_Part_Number_Key)
  orp:All_Received = 'NO'
  orp:Part_Number  = res:Part_Number
  Set(orp:Received_Part_Number_Key,orp:Received_Part_Number_Key)
  Loop
      If Access:ORDPARTS.NEXT()
         Break
      End !If
      If orp:All_Received <> 'NO'      |
      Or orp:Part_Number  <> res:Part_Number      |
          Then Break.  ! End If
      !Double check back to the Order File to see if it really hasn't all been received.
      Access:ORDERS.Clearkey(ord:Order_Number_Key)
      ord:Order_Number    = orp:Order_Number
      If Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign
          !Found
          If ord:All_Received <> 'YES'
              Quantity_On_Order_Temp += orp:Quantity
          End !If ord:All_Received <> 'YES'
      Else! If Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign
      
  End !Loop
  Access:ORDPARTS.RestoreFile(Save_orp_ID)
  Setcursor()
  
  IF quantity_to_order_temp < 0
      quantity_to_order_temp = 0
  End!IF sto:quantity_to_order < 0
  If quantity_on_order_temp < 0
      quantity_on_order_temp = 0
  End!If sto:quantity_on_order < 0
  
  quantity_stock_temp   = sto:quantity_stock
  !Hide/Show OK Button
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If res:Despatched = 'YES' or res:Despatched = 'CAN'
      ?OK{prop:Hide} = 1
  End !res:Despatched = 'YES'
  IF SELF.Request = ViewRecord
    ?res:Part_Number{PROP:ReadOnly} = True
    ?res:Description{PROP:ReadOnly} = True
    ?res:Supplier{PROP:ReadOnly} = True
    ?res:AccessoryCost{PROP:ReadOnly} = True
    ?res:Item_Cost{PROP:ReadOnly} = True
    ?Euro_Item{PROP:ReadOnly} = True
    ?Euro_Item:2{PROP:ReadOnly} = True
    ?res:Quantity{PROP:ReadOnly} = True
    ?Euro_Total{PROP:ReadOnly} = True
    ?Item_Total_Temp{PROP:ReadOnly} = True
    HIDE(?OK)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:ORDITEMS.Close
    Relate:RETSALES.Close
    Relate:VATCODE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Retail_Stock',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    res:Purchase_Order_Number = ret:purchase_order_number
    Quantity_Required_Temp = 1
    res:Despatched = 'PIK'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?res:AccessoryCost
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?res:AccessoryCost, Accepted)
      Do item_total
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?res:AccessoryCost, Accepted)
    OF ?res:Item_Cost
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?res:Item_Cost, Accepted)
      Do item_total
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?res:Item_Cost, Accepted)
    OF ?res:Quantity
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?res:Quantity, Accepted)
      Do item_total
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?res:Quantity, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !New Record
  glo:Select2 = ''
  glo:Select3 = ''
  glo:Select4 = ''
  glo:Select5 = ''
  glo:Select6 = ''
  glo:Select7 = ''
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  If thiswindow.request = Insertrecord
  !Reget Stock Record
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number = glo:select1
      access:stock.fetch(sto:ref_number_key)
  
      If quantity_required_temp < res:quantity
          Case MessageEx('The Quantity Required cannot be less than the Quantity To Ship.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          Cycle
      End!If quantity_required_temp < res:quantity
  
      If res:quantity = 0
          Case MessageEx('You have selected not to ship any items.'&|
            '<13,10>'&|
            '<13,10>Are you sure?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
              Of 2 ! &No Button
                  Cycle
          End!Case MessageEx
      End!If res:quantity = 0
  
      If res:Quantity > sto:Quantity_Stock
          Case MessageEx('You cannot ship more items than there are in stock!','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          Cycle
      End !If res:Quantity > sto:Quantity_Stock
  
  
  
      !Communicaid don't want to take anything from stock yet. They want to manually adjust the stock levels
      !So don't need Back Ordering
  
      !Neil 11/07/01
      !Show message if number to ship drops the level below the minimum!
  
      !Oops Neil. That is now how you do it!
  
  !    IF sto:Quantity_Stock+(sto:Quantity_To_Order+sto:Quantity_On_Order)-res:Quantity <= sto:Minimum_Level
  !      !Fallen below min level if items taken!
  !        Case MessageEx('The selling of this item will cause the stock level to fall below the minimum levels. '&|
  !        '<13,10>'&|
  !        '<13,10>This is an information only message.','ServiceBase 2000',|
  !                     'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,300) 
  !          Of 1 ! &OK Button
  !        End!Case MessageEx
  !    END
  
      !If the res:Quantity = 0 it is filled in with the Required quantity.
      !Need the below flag to stop it then going on to take that quantity
      !from stock.
  
      tmp:TakeFromStock = 1
  
      If sto:sundry_item <> 'YES'                                                                     !If sundry, don't decrement
          !Take the "Quantity To Ship" from stock, and order the difference to "Quantity Required"
  
          If Quantity_Required_Temp > res:Quantity
              !You are only shipping some of the required, the rest will be ordered.
              !Am just create parts orders, the same as with Service Summary Ordering
  
                  If res:Quantity = 0
                      tmp:TakeFromStock = 0
                      !Make this part the "pending" part.
                      res:despatched = 'PEN'
                      res:date_ordered = ''
                      res:date_received = ''
                      res:Quantity    = Quantity_Required_Temp
                      LocalCreatePendingPart(Quantity_Required_Temp)
                      res:Pending_Ref_Number  = ope:Ref_Number
  
                  Else !If res:Quantity = 0
                      !Set the globals which will create a new part line
                      !for the "pending" part.
  
                      glo:select2  = 'NEW PENDING'
                      glo:select3  = sto:ref_number
                      glo:select4  = res:record_number
                      glo:select5  = quantity_required_temp - res:quantity
                      res:date_ordered    = Today()
                      res:date_received   = Today()
  !                        ope:quantity    = quantity_required_temp - res:quantity
  !                   MakePartsRequest(sto:Supplier,sto:Part_Number,sto:Description,Quantity_Required_Temp - res:Quantity)
                      LocalCreatePendingPart(Quantity_Required_Temp - res:Quantity)
                      glo:select6  = ope:ref_number
  
                      !Set the flag for the items to appear on the picking note.
                      res:Despatched = 'PIK'
                  End !If res:Quantity = 0
          End !If Quantity_Required_Temp > res:Quantity
  
          !Take the Quantity To Ship From Stock
  
          !Only take quantity from stock if this ISN'T a pending part.
          If sto:Quantity_Stock <> 0 and tmp:TakeFromStock
              sto:Quantity_stock  -= res:Quantity
              If sto:Quantity_Stock < 0
                  sto:Quantity_Stock = 0
              End !If sto:Quantity_Stock < 0
              If Access:STOCK.Update() = Level:Benign
                  get(stohist,0)
                  if access:stohist.primerecord() = level:benign
                      shi:ref_number           = sto:ref_number
                      access:users.clearkey(use:password_key)
                      use:password              =glo:password
                      access:users.fetch(use:password_key)
                      shi:user                  = use:user_code    
                      shi:date                 = today()
                      shi:transaction_type     = 'DEC'
                      shi:despatch_note_number = ''
                      shi:job_number           = ret:ref_number
                      shi:quantity             = res:quantity
                      shi:purchase_cost        = sto:purchase_cost
                      shi:sale_cost            = sto:sale_cost
                      shi:retail_cost          = sto:retail_cost
                      shi:information          = 'COMPANY: ' & Clip(ret:company_name) & '<13,10>PURCHASE ORDER NO: ' & Clip(RET:Purchase_Order_Number) & |
                                                  '<13,10>CONTACT: ' & CLip(RET:Contact_Name)
                      shi:notes                = 'RETAIL ITEM SOLD'
                      if access:stohist.insert()
                         access:stohist.cancelautoinc()
                      end
                  end!if access:stohist.primerecord() = level:benign
  
              End !If Access:STOCK.Update() = Level:Benign
          End !If sto:Quantity_Stock <> 0
  
  
      End!If sto:sundry_item <> 'YES'
      If func:RecordNumber <> 0
          !Can't delete the parts, but there is no flag in the file to hide them
  
  !        Access:ORDITEMS.ClearKey(ori:recordnumberkey)
  !        ori:recordnumber = func:RecordNumber
  !        If Access:orditems.TryFetch(ori:recordnumberkey) = Level:Benign
  !            !Found
  !            Delete(ORDITEMS)
  !        Else!If Access:orditems.TryFetch(ori:recordnumberkey) = Level:Benign
  !            !Error
  !            !Assert(0,'<13,10>Fetch Error<13,10>')
  !        End!If Access:orditems.TryFetch(ori:recordnumberkey) = Level:Benign
      End !If func:RecordNumber <> 0
  End!If thiswindow.request = Insertrecord
  !Changing Record
  
  !Intial Stock Entry - func:Type = 0
  !Amending from Process Retail Sale - func:Type = 1
  
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If ThisWindow.Request <> InsertRecord
  !Reget Stock Record
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number  = res:Part_Ref_Number
      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          !Found
  
          If res:Quantity <> Quantity_Temp
              !Ignore Sundry Items
              If sto:Sundry_Item <> 'YES'
                  If res:Despatched = 'PEN'
                      If res:Quantity > Quantity_Temp
                          !It doesn't really matter if they want to increase the quantity
                          !of a pending order. I either add some to any pending order I find,
                          !or create a new pending order if the order has already been raised.
                          Case MessageEx('You have increased the quantity of a part awaiting order.'&|
                            '<13,10>'&|
                            '<13,10>Are you sure you want to increase the quantity you wish to order?','ServiceBase 2000',|
                                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                              Of 1 ! &Yes Button
                                  !Need to see if there is a pending order
                                  !for this supplier and part number.
                                  
                                  Access:ORDPEND.ClearKey(ope:Supplier_Key)
                                  ope:Supplier    = sto:Supplier
                                  ope:Part_Number = sto:Part_Number
                                  If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                                      !Found
                                      ope:Quantity += res:Quantity - Quantity_Temp
                                      Access:ORDPEND.TryUpdate()
                                  Else!If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                      !If an pending order doesn't actually exist. Although I'm not sure why,
                                      !then ask if they want to create a new one. I don't think
                                      !I can make it be any cleverer than that.
                                      Case MessageEx('Error! Cannot find a Pending Back Order for this Part Number.'&|
                                        '<13,10>'&|
                                        '<13,10>Do you want a create a new Back Order for this part?','ServiceBase 2000',|
                                                     'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                                          Of 1 ! &Yes Button
                                              LocalCreatePendingPart(res:Quantity)
                                          Of 2 ! &No Button
                                              res:Quantity = Quantity_Temp
                                              Select(?res:Quantity)
                                              Cycle
                                      End!Case MessageEx
                                  End!If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
  
                              Of 2 ! &No Button
                                  res:Quantity = Quantity_Temp
                                  Select(?res:Quantity)
                                  Cycle
                          End!Case MessageEx
  
                      Else !If res:Quantity > Quantity_Temp
                          !I should be able to make this more cleverer than the spec
                          !I can check to see if an pending order exists and adjust
                          !it's quantity. But if not, then I bring up the message
                          !and cancel the difference.
  
                          Case MessageEx('You have decreased the quantity of a part awaiting back order.'&|
                            '<13,10>'&|
                            '<13,10>Are you sure you want to decrease the quantity you wish to order?','ServiceBase 2000',|
                                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                              Of 1 ! &Yes Button
                                  Access:ORDPEND.Clearkey(ope:Supplier_Key)
                                  ope:Supplier    = sto:Supplier
                                  ope:Part_Number = sto:Part_Number
                                  If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                      !Found
                                      ope:Quantity -= (Quantity_Temp - res:Quantity)
                                      If ope:Quantity <= 0
                                          !If it falls less than zero, then someone has ordered too much
                                          !Remove the pending order.
                                          Delete(ORDPEND)
                                      Else
                                          Access:ORDPEND.TryUpdate()
                                      End !If ope:Quantity <= 0
                                  Else! If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
  
                                      !There is no point giving a yes/no because,
                                      !they've already agreed to this course of action above.
                                      Case MessageEx('Cannot find a Pending Back Order. The quantity may have already been ordered.'&|
                                        '<13,10>'&|
                                        '<13,10>The difference will now be cancelled from the sale.','ServiceBase 2000',|
                                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                          Of 1 ! &OK Button
                                      End!Case MessageEx
  
                                      !Set globals to create a new line for
                                      !the cancelled item
                                      glo:select2  = 'CANCELLED'
                                      glo:select3  = sto:ref_number
                                      glo:select4  = res:record_number
                                      glo:select5  = Quantity_Temp - res:Quantity
                                      glo:select6  = ''
  
                                      Quantity_Temp = res:Quantity
                                  End! If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                  
                              Of 2 ! &No Button
                          End!Case MessageEx
                      End !If res:Quantity > Quantity_Temp
                  Else !If res:Despatched = 'PEN'
                      !A normal stock part.
                      If res:Quantity > Quantity_temp
                          !Do not allow the user to increase the quantity.
                          !If it's a initial Stock Entry (func:Type = 0)
                          !then the user can delete it, or add another part
  
                          !If it's from Process (func:Type = 1), then the
                          !only way is to create another sale.
                          Case func:Type
                              Of 0
                                  Case MessageEx('You cannot increase the quantity of a stock part.'&|
                                    '<13,10>'&|
                                    '<13,10>You can either delete the part and re-insert it, or insert the same part again.','ServiceBase 2000',|
                                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                      Of 1 ! &OK Button
                                  End!Case MessageEx
                                  res:Quantity = Quantity_Temp
                                  Select(?res:Quantity)
                                  Cycle
                              Of 1
                                  Case MessageEx('You cannot increase the quantity of a stock part.'&|
                                    '<13,10>'&|
                                    '<13,10>You ship more parts for this customer you will need to create another sale.','ServiceBase 2000',|
                                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                      Of 1 ! &OK Button
                                  End!Case MessageEx
                                  res:Quantity = Quantity_Temp
                                  Select(?res:Quantity)
                                  Cycle
                          End !Case func:Type
                      Else !If res:Quantity > Quantity_temp
                          !Return the difference to stock.
                          
                          Case MessageEx('You have decreased the quantity required.'&|
                            '<13,10>The difference will be returned to stock.'&|
                            '<13,10>'&|
                            '<13,10>Do you also want to create a Back Order for this quantity?','ServiceBase 2000',|
                                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                              Of 1 ! &Yes Button
                                  LocalCreatePendingPart(Quantity_Temp - res:Quantity)
                                  glo:select2  = 'NEW PENDING'
                                  glo:select3  = sto:ref_number
                                  glo:select4  = res:record_number
                                  glo:select5  = Quantity_Temp - res:Quantity
                                  glo:select6  = ope:ref_number
  
                              Of 2 ! &No Button
                          End!Case MessageEx
  
                          sto:Quantity_Stock += Quantity_Temp - res:Quantity
                          If Access:STOCK.TryUpdate() = Level:Benign
                              if access:stohist.primerecord() = level:benign
                                  shi:ref_number           = sto:ref_number
                                  access:users.clearkey(use:password_key)
                                  use:password             = glo:password
                                  access:users.fetch(use:password_key)
                                  shi:user                 = use:user_code
                                  shi:date                 = today()
                                  shi:transaction_type     = 'REC'
                                  shi:despatch_note_number = ''
                                  shi:job_number           = ret:ref_number
                                  shi:quantity             = Quantity_Temp - res:Quantity
                                  shi:purchase_cost        = sto:purchase_cost
                                  shi:sale_cost            = sto:sale_cost
                                  shi:retail_cost          = sto:retail_cost
                                  shi:information          = 'COMPANY: ' & Clip(ret:company_name) & '<13,10>PURCHASE ORDER NO: ' & |
                                                          Clip(ret:purchase_order_number) & '<13,10>CONTACT: ' & Clip(ret:contact_name)
                                  shi:notes                = 'RETAIL ITEM RECREDITED'
                                  if access:stohist.insert()
                                     access:stohist.cancelautoinc()
                                  end
                              end!if access:stohist.primerecord() = level:benign
  
                          End !If Access:STOCK.TryUpdate() = Level:Benign
  
                      End !If res:Quantity > Quantity_temp
                  End !If res:Despatched = 'PEN'
  
              Else !If sto:Sundry_Item <> 'YES'
  
              End !If sto:Sundry_Item <> 'YES'
              If res:Quantity = 0
                  glo:Select7 = 'DELETE'
              Else
                  glo:Select7 = ''
              End !If res:Quantity = 0
          End !If res:Quantity <> Quantity_Temp
      Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
  End!If thiswindow.request <> Insertrecord
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Retail_Stock')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?res:Quantity
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?res:Quantity, NewSelection)
      Do item_total
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?res:Quantity, NewSelection)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      If res:item_cost <> 0
          Select(?quantity_required_temp)
      End!If res:item_cost <> 0
      
      
      !Save Fields
      quantity_temp   = res:quantity
      Display()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
LocalCreatePendingPart          Procedure(Long  func:Quantity)
    Code
    If Access:ORDPEND.PrimeRecord() = Level:Benign
        ope:Part_Ref_Number = sto:Ref_Number
        ope:Job_Number      = res:Ref_Number
        ! Start Change BE022 BE(12/12/03)
        !ope:Part_Type       = 'STO'
        ope:Part_Type       = 'RET'
        ! End Change BE022 BE(12/12/03)
        ope:Supplier        = sto:Supplier
        ope:Part_Number     = res:Part_Number
        ope:Description     = res:Description
        ope:Quantity        = func:Quantity
        ope:Account_Number  = ret:Account_Number

        If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Successful
        Else !If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Failed
        End !f Access:ORDPEND.TryInsert() = Level:Benign
    End !If Access:ORDPEND.PrimeRecord() = Level:Benign

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
