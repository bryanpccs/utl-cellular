

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01013.INC'),ONCE        !Local module procedure declarations
                     END


Report_Type PROCEDURE (f_ref_number)                  !Generated from procedure template - Window

report_type_temp     STRING(3)
invoice_number_temp  LONG
save_res_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Select Report Type'),AT(,,151,119),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,144,84),USE(?Sheet1),SPREAD
                         TAB('Select Report Type'),USE(?Tab1)
                           OPTION,AT(20,20,112,64),USE(report_type_temp),BOXED
                             RADIO('Picking Note'),AT(44,32),USE(?report_type_temp:Radio1),VALUE('1')
                             RADIO('Despatch Note'),AT(44,48),USE(?report_type_temp:Radio2),VALUE('2')
                             RADIO('Invoice'),AT(44,64),USE(?report_type_temp:Radio3),VALUE('3')
                           END
                         END
                       END
                       BUTTON('Close'),AT(88,96,56,16),USE(?Close),LEFT,ICON('Cancel.gif')
                       PANEL,AT(4,92,144,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Print'),AT(8,96,56,16),USE(?Print),LEFT,ICON(ICON:Print1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
windowsdir      CString(260)
systemdir       String(260)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?report_type_temp{prop:Font,3} = -1
    ?report_type_temp{prop:Color} = 15066597
    ?report_type_temp{prop:Trn} = 0
    ?report_type_temp:Radio1{prop:Font,3} = -1
    ?report_type_temp:Radio1{prop:Color} = 15066597
    ?report_type_temp:Radio1{prop:Trn} = 0
    ?report_type_temp:Radio2{prop:Font,3} = -1
    ?report_type_temp:Radio2{prop:Color} = 15066597
    ?report_type_temp:Radio2{prop:Trn} = 0
    ?report_type_temp:Radio3{prop:Font,3} = -1
    ?report_type_temp:Radio3{prop:Color} = 15066597
    ?report_type_temp:Radio3{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Create_Invoice      Routine
    SystemDir   = GetSystemDirectory(WindowsDir,Size(WindowsDir))
    Set(defaults)
    access:defaults.next()
    invoice_number_temp = 0
    Case MessageEx('Are you sure you want to print a Single Invoice?','ServiceBase 2000','Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
        Of 1 ! &Yes Button
            error# = 0
            access:retsales.clearkey(ret:ref_number_key)
            ret:ref_number = f_ref_number
            if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
                If ret:invoice_number <> '' and error# = 0
                    glo:select1  = ret:invoice_number
                    Retail_Single_Invoice
                    glo:select1  = ''
                Else!If ret:invoice_number <> ''
                    error# = 0
                    If error# = 0
                        fetch_error# = 0
                        despatch# = 0
                        UseEuro# = 0
                        access:subtracc.clearkey(sub:account_number_key)
                        sub:account_number = ret:account_number
                        if access:subtracc.fetch(sub:account_number_key)
                            Case MessageEx('Error! Cannot find Sub Account.','ServiceBase 2000',|
                                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                    Of 1 ! &OK Button
                            End!Case MessageEx
                        Else!if access:subtracc.fetch(sub:account_number_key) = level:benign
                            access:tradeacc.clearkey(tra:account_number_key) 
                            tra:account_number = sub:main_account_number
                            if access:tradeacc.fetch(tra:account_number_key)
                                Case MessageEx('Error! Cannot find Trade Account.','ServiceBase 2000',|
                                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                        Of 1 ! &OK Button
                                End!Case MessageEx
                                fetch_error# = 1
                            Else!if access:tradeacc.fetch(tra:account_number_key)
                                If tra:use_sub_accounts = 'YES'
                                    If sub:despatch_invoiced_jobs = 'YES'
                                        If sub:despatch_paid_jobs = 'YES'
    !                                        If ret:paid = 'YES'
    !                                            despatch# = 1
    !                                        Else
    !                                            despatch# = 2
    !                                        End
                                        Else
                                            despatch# = 1
                                        End!If sub:despatch_paid_jobs = 'YES' And ret:paid = 'YES'
                                        
                                    End!If sub:despatch_invoiced_jobs = 'YES'
                                End!If tra:use_sub_accounts = 'YES'
                                if tra:invoice_sub_accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                                    If sub:EuroApplies
                                        UseEuro# = 1
                                    End !If sub:EuroApplies
                                    vat_number" = sub:vat_number
                                    access:vatcode.clearkey(vat:vat_code_key)
                                    vat:vat_code = sub:retail_vat_code
                                    if access:vatcode.fetch(vat:vat_code_key)
                                        Case MessageEx('Error ! A V.A.T. rate has not been setup for this Trade Account.','ServiceBase 2000',|
                                                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                                Of 1 ! &OK Button
                                        End!Case MessageEx
                                        fetch_error# = 1
                                    Else!if access:vatcode.fetch(vat:vat_code_key)
                                       retail_rate$ = vat:vat_rate
                                    end!if access:vatcode.fetch(vat:vat_code_key)
                                else!if tra:use_sub_accounts = 'YES'
                                    If tra:EuroApplies
                                        UseEuro# = 1
                                    End !If tra:EuroApplies
                                    If tra:despatch_invoiced_jobs = 'YES'
                                        If tra:despatch_paid_jobs = 'YES'
    !                                        If ret:paid = 'YES'
    !                                            despatch# = 1
    !                                        Else
    !                                            despatch# = 2
    !                                        End!If ret:paid = 'YES'
                                        Else
                                            despatch# = 1
                                        End!If tra:despatch_paid_jobs = 'YES' and ret:paid = 'YES'
                                    End!If tra:despatch_invoiced_jobs = 'YES'
                                    vat_number" = tra:vat_number
                                    access:vatcode.clearkey(vat:vat_code_key)
                                    vat:vat_code = tra:retail_vat_code
                                    if access:vatcode.fetch(vat:vat_code_key)
                                        fetch_error# = 1
                                        Case MessageEx('Error ! A V.A.T. rate has not been setup for this Trade Account.','ServiceBase 2000',|
                                                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                                Of 1 ! &OK Button
                                        End!Case MessageEx
                                    Else!if access:vatcode.fetch(vat:vat_code_key)
                                       retail_rate$ = vat:vat_rate
                                    end
                                end!if tra:use_sub_accounts = 'YES'
                            end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                        end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
                        If fetch_error# = 0
        !SAGE BIT
                            If def:use_sage = 'YES'
    !                            file_name = Clip(WindowsDir) & '\SAGEDATA.SPD'
    !                            Remove(paramss)
    !                            access:paramss.open()
    !                            access:paramss.usefile()
    !         !LABOUR
    !                            get(paramss,0)
    !                            if access:paramss.primerecord() = Level:Benign
    !                                prm:account_ref       = Stripcomma(ret:Account_number)
    !                                prm:name              = Stripcomma(ret:company_name)
    !                                prm:address_1         = Stripcomma(ret:address_line1)
    !                                prm:address_2         = Stripcomma(ret:address_line2)
    !                                prm:address_3         = Stripcomma(ret:address_line3)
    !                                prm:address_4         = ''
    !                                prm:address_5         = Stripcomma(ret:postcode)
    !                                prm:del_address_1     = Stripcomma(ret:address_line1_delivery)
    !                                prm:del_address_2     = Stripcomma(ret:address_line2_delivery)
    !                                prm:del_address_3     = Stripcomma(ret:address_line3_delivery)
    !                                prm:del_address_4     = ''
    !                                prm:del_address_5     = Stripcomma(ret:postcode_delivery)
    !                                prm:cust_tel_number   = Stripcomma(ret:telephone_number)
    !                                prm:contact_name      = Stripcomma(ret:contact_name)
    !                                prm:notes_1           = ''
    !                                prm:notes_2           = Stripcomma(Stripreturn(ret:invoice_text))
    !                                prm:notes_3           = ''
    !                                prm:taken_by          = Stripcomma(ret:who_booked)
    !                                prm:order_number      = Stripcomma(ret:purchase_order_number)
    !                                prm:cust_order_number = 'J' & Stripcomma(ret:ref_number)
    !                                prm:payment_ref       = ''
    !                                prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
    !                                prm:global_details    = ''
    !                                prm:items_net         = Stripcomma(ret:labour_cost + ret:parts_cost + ret:courier_cost)
    !                                prm:items_tax         = Stripcomma(Round(ret:labour_cost * (labour_rate$/100),.01) + |
    !                                                            Round(ret:parts_cost * (parts_rate$/100),.01) + |
    !                                                            Round(ret:courier_cost * (labour_rate$/100),.01))
    !                                prm:stock_code        = Stripcomma(DEF:Labour_Stock_Code)
    !                                prm:description       = Stripcomma(DEF:Labour_Description)
    !                                prm:nominal_code      = Stripcomma(DEF:Labour_Code)
    !                                prm:qty_order         = '1'
    !                                prm:unit_price        = '0'
    !                                prm:net_amount        = Stripcomma(ret:labour_cost)
    !                                prm:tax_amount        = Stripcomma(Round(ret:labour_cost * (labour_rate$/100),.01))
    !                                prm:comment_1         = Stripcomma(ret:charge_type)
    !                                prm:comment_2         = Stripcomma(ret:warranty_charge_type)
    !                                prm:unit_of_sale      = '0'
    !                                prm:full_net_amount   = '0'
    !                                prm:invoice_date      = '*'
    !                                prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
    !                                prm:user_name         = Clip(DEF:User_Name_Sage)
    !                                prm:password          = Clip(DEF:Password_Sage)
    !                                prm:set_invoice_number= '*'
    !                                prm:invoice_no        = '*'
    !                                access:paramss.insert()
    !                            end!if access:paramss.primerecord() = Level:Benign
    !        !PARTS
    !                            get(paramss,0)
    !                            if access:paramss.primerecord() = Level:Benign
    !                                prm:account_ref       = Stripcomma(ret:Account_number)
    !                                prm:name              = Stripcomma(ret:company_name)
    !                                prm:address_1         = Stripcomma(ret:address_line1)
    !                                prm:address_2         = Stripcomma(ret:address_line2)
    !                                prm:address_3         = Stripcomma(ret:address_line3)
    !                                prm:address_4         = ''
    !                                prm:address_5         = Stripcomma(ret:postcode)
    !                                prm:del_address_1     = Stripcomma(ret:address_line1_delivery)
    !                                prm:del_address_2     = Stripcomma(ret:address_line2_delivery)
    !                                prm:del_address_3     = Stripcomma(ret:address_line3_delivery)
    !                                prm:del_address_4     = ''
    !                                prm:del_address_5     = Stripcomma(ret:postcode_delivery)
    !                                prm:cust_tel_number   = Stripcomma(ret:telephone_number)
    !                                If ret:surname <> ''
    !                                    if ret:title = '' and ret:initial = '' 
    !                                        prm:contact_name = Stripcomma(clip(ret:surname))
    !                                    elsif ret:title = '' and ret:initial <> ''
    !                                        prm:contact_name = Stripcomma(clip(ret:surname) & ' ' & clip(ret:initial))
    !                                    elsif ret:title <> '' and ret:initial = ''
    !                                        prm:contact_name = Stripcomma(clip(ret:surname) & ' ' & clip(ret:title))
    !                                    elsif ret:title <> '' and ret:initial <> ''
    !                                        prm:contact_name = Stripcomma(clip(ret:surname) & ' ' & clip(ret:title) & ' ' & clip(ret:initial))
    !                                    else
    !                                        prm:contact_name = ''
    !                                    end
    !                                else
    !                                    prm:contact_name = ''
    !                                end
    !                                prm:notes_1           = Stripcomma(Stripreturn(ret:fault_description))
    !                                prm:notes_2           = Stripcomma(Stripreturn(ret:invoice_text))
    !                                prm:notes_3           = ''
    !                                prm:taken_by          = Stripcomma(ret:who_booked)
    !                                prm:order_number      = Stripcomma(ret:order_number)
    !                                prm:cust_order_number = 'J' & Stripcomma(ret:ref_number)
    !                                prm:payment_ref       = ''
    !                                prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
    !                                prm:global_details    = ''
    !                                prm:items_net         = Stripcomma(ret:labour_cost + ret:parts_cost + ret:courier_cost)
    !                                prm:items_tax         = Stripcomma(Round(ret:labour_cost * (labour_rate$/100),.01) + |
    !                                                            Round(ret:parts_cost * (parts_rate$/100),.01) + |
    !                                                            Round(ret:courier_cost * (labour_rate$/100),.01))
    !                                prm:stock_code        = Stripcomma(DEF:Parts_Stock_Code)
    !                                prm:description       = Stripcomma(DEF:Parts_Description)
    !                                prm:nominal_code      = Stripcomma(DEF:Parts_Code)
    !                                prm:qty_order         = '1'
    !                                prm:unit_price        = '0'
    !                                prm:net_amount        = Stripcomma(ret:parts_cost)
    !                                prm:tax_amount        = Stripcomma(Round(ret:parts_cost * (parts_rate$/100),.01))
    !                                prm:comment_1         = Stripcomma(ret:charge_type)
    !                                prm:comment_2         = Stripcomma(ret:warranty_charge_type)
    !                                prm:unit_of_sale      = '0'
    !                                prm:full_net_amount   = '0'
    !                                prm:invoice_date      = '*'
    !                                prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
    !                                prm:user_name         = Clip(DEF:User_Name_Sage)
    !                                prm:password          = Clip(DEF:Password_Sage)
    !                                prm:set_invoice_number= '*'
    !                                prm:invoice_no        = '*'
    !                                access:paramss.insert()
    !                            end!if access:paramss.primerecord() = Level:Benign
    !        !COURIER
    !                            get(paramss,0)
    !                            if access:paramss.primerecord() = Level:Benign
    !                                prm:account_ref       = Stripcomma(ret:Account_number)
    !                                prm:name              = Stripcomma(ret:company_name)
    !                                prm:address_1         = Stripcomma(ret:address_line1)
    !                                prm:address_2         = Stripcomma(ret:address_line2)
    !                                prm:address_3         = Stripcomma(ret:address_line3)
    !                                prm:address_4         = ''
    !                                prm:address_5         = Stripcomma(ret:postcode)
    !                                prm:del_address_1     = Stripcomma(ret:address_line1_delivery)
    !                                prm:del_address_2     = Stripcomma(ret:address_line2_delivery)
    !                                prm:del_address_3     = Stripcomma(ret:address_line3_delivery)
    !                                prm:del_address_4     = ''
    !                                prm:del_address_5     = Stripcomma(ret:postcode_delivery)
    !                                prm:cust_tel_number   = Stripcomma(ret:telephone_number)
    !                                If ret:surname <> ''
    !                                    if ret:title = '' and ret:initial = '' 
    !                                        prm:contact_name = Stripcomma(clip(ret:surname))
    !                                    elsif ret:title = '' and ret:initial <> ''
    !                                        prm:contact_name = Stripcomma(clip(ret:surname) & ' ' & clip(ret:initial))
    !                                    elsif ret:title <> '' and ret:initial = ''
    !                                        prm:contact_name = Stripcomma(clip(ret:surname) & ' ' & clip(ret:title))
    !                                    elsif ret:title <> '' and ret:initial <> ''
    !                                        prm:contact_name = Stripcomma(clip(ret:surname) & ' ' & clip(ret:title) & ' ' & clip(ret:initial))
    !                                    else
    !                                        prm:contact_name = ''
    !                                    end
    !                                else
    !                                    prm:contact_name = ''
    !                                end
    !                                prm:notes_1           = Stripcomma(Stripreturn(ret:fault_description))
    !                                prm:notes_2           = Stripcomma(Stripreturn(ret:invoice_text))
    !                                prm:notes_3           = ''
    !                                prm:taken_by          = Stripcomma(ret:who_booked)
    !                                prm:order_number      = Stripcomma(ret:order_number)
    !                                prm:cust_order_number = 'J' & Stripcomma(ret:ref_number)
    !                                prm:payment_ref       = ''
    !                                prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
    !                                prm:global_details    = ''
    !                                prm:items_net         = Stripcomma(ret:labour_cost + ret:parts_cost + ret:courier_cost)
    !                                prm:items_tax         = Stripcomma(Round(ret:labour_cost * (labour_rate$/100),.01) + |
    !                                                            Round(ret:parts_cost * (parts_rate$/100),.01) + |
    !                                                            Round(ret:courier_cost * (labour_rate$/100),.01))
    !                                prm:stock_code        = Stripcomma(DEF:Courier_Stock_Code)
    !                                prm:description       = Stripcomma(DEF:Courier_Description)
    !                                prm:nominal_code      = Stripcomma(DEF:Courier_Code)
    !                                prm:qty_order         = '1'
    !                                prm:unit_price        = '0'
    !                                prm:net_amount        = Stripcomma(ret:courier_cost)
    !                                prm:tax_amount        = Stripcomma(Round(ret:courier_cost * (labour_rate$/100),.01))
    !                                prm:comment_1         = Stripcomma(ret:charge_type)
    !                                prm:comment_2         = Stripcomma(ret:warranty_charge_type)
    !                                prm:unit_of_sale      = '0'
    !                                prm:full_net_amount   = '0'
    !                                prm:invoice_date      = '*'
    !                                prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
    !                                prm:user_name         = Clip(DEF:User_Name_Sage)
    !                                prm:password          = Clip(DEF:Password_Sage)
    !                                prm:set_invoice_number= '*'
    !                                prm:invoice_no        = '*'
    !                                access:paramss.insert()
    !                            end!if access:paramss.primerecord() = Level:Benign
    !                            access:paramss.close()
    !                            Run(CLip(DEF:Path_Sage) & '\SAGEPROJ.EXE',1)
    !                            sage_error# = 0
    !                            access:paramss.open()
    !                            access:paramss.usefile()
    !                            Set(paramss,0)
    !                            If access:paramss.next()
    !                                sage_error# = 1
    !                            Else!If access:paramss.next()
    !                                If prm:invoice_no = -1 or prm:invoice_no = 0 or prm:invoice_no = ''
    !                                    sage_error# = 1
    !                                Else!If prm:invoice_no = '-1' or prm:invoice_no = '0'
    !                                    invoice_number_temp = prm:invoice_no
    !                                    If invoice_number_temp = 0
    !                                        sage_error# = 1
    !                                    End!If invoice_number_temp = 0
    !                                End!If prm:invoice_no = '-1'
    !                            End!If access:paramss.next()
    !                            access:paramss.close()
                            End!If def:use_sage = 'YES'
                            If sage_error# = 0
                                invoice_error# = 1
                                get(invoice,0)
                                if access:invoice.primerecord() = level:benign
    !                                If def:use_sage = 'YES'
    !                                    inv:invoice_number = invoice_number_temp
    !                                End!If def:use_sage = 'YES'
                                    inv:invoice_type       = 'RET'
                                    inv:job_number         = ret:ref_number
                                    inv:date_created       = Today()
                                    inv:account_number     = ret:account_number
                                    inv:total              = ret:sub_total
                                    inv:vat_rate_labour    = ''
                                    inv:vat_rate_parts     = ''
                                    inv:vat_rate_retail    = retail_rate$
                                    inv:vat_number         = def:vat_number
                                    INV:Courier_Paid       = ret:courier_cost
                                    inv:parts_paid         = ret:parts_cost
                                    inv:labour_paid        = ''
                                    inv:invoice_vat_number = vat_number"
                                    If UseEuro#
                                        inv:UseAlternativeAddress   = 1
                                        inv:EuroExhangeRate = def:EuroRate
                                    Else
                                        inv:UseAlternativeAddress   = 0
                                        inv:EuroExhangeRate = def:EuroRate
                                    End !If UseEuro#
                                    If access:invoice.insert()
                                        invoice_error# = 1
                                        access:invoice.cancelautoinc()
                                    End!If access:invoice.insert()
                                    invoice_error# = 0
                                    If access:invoice.insert()
                                        invoice_error# = 1
                                        access:invoice.cancelautoinc()
                                    End!If access:invoice.insert()
                                end!if access:invoice.primerecord() = level:benign
                                If Invoice_error# = 0
    !                                Case despatch#
    !                                    Of 1
    !                                        If ret:despatched = 'YES'
    !                                            Status_Routine(710,ret:current_status,a",a",a")
    !                                        Else!If ret:despatched = 'YES'
    !                                            Status_Routine(810,ret:current_status,a",a",a")
    !                                            ret:despatched = 'REA'
    !                                            ret:despatch_type = 'JOB'
    !                                            ret:current_courier = ret:courier
    !                                        End!If ret:despatched <> 'YES'
    !                                    Of 2
    !                                        Status_Routine(803,ret:current_status,a",a",a")
    !                                    Else
    !                                        Status_Routine(710,ret:current_status,a",a",a")
    !
    !                                End!Case despatch#

                                    ret:invoice_number        = inv:invoice_number
                                    ret:invoice_date          = Today()
                                    ret:invoice_courier_cost  = ret:courier_cost
                                    ret:invoice_parts_cost    = ret:parts_cost
                                    ret:invoice_sub_total     = ret:sub_total
                                    access:retsales.update()
                                    glo:select1 = inv:invoice_number
                                    Retail_Single_Invoice
                                    glo:select1 = ''
                                    ! Start Virtual Warehouse i/f BE(06/05/03)
                                    UpdateVirtualWarehouse()
                                    ! End Virtual Warehouse i/f BE(06/05/03)
                                End!If Invoice_error# = 0
                            End!If def:use_sage = 'YES' and sage_error# = 1
                        End!If fetch_error# = 0
                    End!If error# = 0
                End!If ret:invoice_number <> ''
            end!if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign

        Of 2 ! &No Button
    End!Case MessageEx


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Report_Type',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('report_type_temp',report_type_temp,'Report_Type',1)
    SolaceViewVars('invoice_number_temp',invoice_number_temp,'Report_Type',1)
    SolaceViewVars('save_res_id',save_res_id,'Report_Type',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp;  SolaceCtrlName = '?report_type_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio1;  SolaceCtrlName = '?report_type_temp:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio2;  SolaceCtrlName = '?report_type_temp:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio3;  SolaceCtrlName = '?report_type_temp:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Print;  SolaceCtrlName = '?Print';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Report_Type')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Report_Type')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?report_type_temp:Radio1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:INVOICE.Open
  Relate:RETDESNO.Open
  Relate:VATCODE.Open
  Access:RETSTOCK.UseFile
  Access:RETSALES.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:RETDESNO.Close
    Relate:VATCODE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Report_Type',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Print
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print, Accepted)
      Case report_type_temp
          Of 1 !Picking Note
              Set(DEFAULTS)
              Access:DEFAULTS.Next()
              continue# = 0
              setcursor(cursor:wait)
              save_res_id = access:retstock.savefile()
              access:retstock.clearkey(res:part_number_key)
              res:ref_number  = f_ref_number
              set(res:part_number_key,res:part_number_key)
              loop
                  if access:retstock.next()
                     break
                  end !if
                  if res:ref_number  <> f_ref_number      |
                      then break.  ! end if
                  yldcnt# += 1
                  if yldcnt# > 25
                     yield() ; yldcnt# = 0
                  end !if
                  If res:despatched <> 'YES'
                      continue# = 1
                      Break
                  End!If res:despatched <> 'YES'
              end !loop
              access:retstock.restorefile(save_res_id)
              setcursor()
      
              If continue# = 1
                  glo:select1  = f_ref_number
                  Retail_Picking_Note('PIK')
                  glo:select1  = ''
              Else!If continue# = 1
                  Case MessageEx('All the items have been despatched.<13,10><13,10>Do you wish to reprint the original Picking Note','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                  glo:select1  = f_ref_number
                  Retail_Picking_Note('ALL')
                  glo:select1  = ''
                          Of 2 ! &No Button
                  End!Case MessageEx
              End!If continue# = 1
          Of 2 !Despatch Note
              access:retsales.clearkey(ret:ref_number_key)
              ret:ref_number = f_ref_number
              if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
                  If ret:despatch_number <> ''
                      glo:select1  = ret:despatch_number
                      Retail_Despatch_Note
                      glo:select1  = ''
                  Else!If ret:despatch_number <> ''
                      get(retdesno,0)
                      if access:retdesno.primerecord() = Level:Benign
                          RDN:Consignment_Number  = ''
                          RDN:Courier             = ret:courier
                          rdn:sale_number         = ret:ref_number
                          access:retdesno.insert()
      
                          save_res_id = access:retstock.savefile()
                          access:retstock.clearkey(res:part_number_key)
                          res:ref_number  = ret:ref_number
                          set(res:part_number_key,res:part_number_key)
                          loop
                              if access:retstock.next()
                                 break
                              end !if
                              if res:ref_number  <> ret:ref_number      |
                                  then break.  ! end if
                              If res:pending_ref_number <> ''
                                  res:despatched  = 'PEN'
                              Else!If res:pending_ref_number <> ''
                                  res:despatched = 'YES'
                              End!If res:pending_ref_number <> ''
      
                              access:retstock.update
                          end !loop
                          access:retstock.restorefile(save_res_id)
      
                          ret:despatched = 'PRO'
                          ret:despatch_number = rdn:despatch_number
                          access:retsales.update()
      
                          glo:select1  = rdn:despatch_number
                          Retail_Despatch_Note
                          glo:select1  = ''
                          thiswindow.reset(1)
                      End!if access:retdesno.primerecord() = Level:Benign
                  End!If ret:despatch_number <> ''
      
              End!if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
      
      
          Of 3 !Invoice
              Do Create_Invoice
      
      End!Case report_type_temp
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Report_Type')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

