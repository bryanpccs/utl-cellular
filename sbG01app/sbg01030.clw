

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01030.INC'),ONCE        !Local module procedure declarations
                     END


UpdateWebORders PROCEDURE                             !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
BRW9::View:Browse    VIEW(ORDITEMS)
                       PROJECT(ori:partno)
                       PROJECT(ori:qty)
                       PROJECT(ori:recordnumber)
                       PROJECT(ori:ordhno)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
ori:partno             LIKE(ori:partno)               !List box control field - type derived from field
ori:qty                LIKE(ori:qty)                  !List box control field - type derived from field
ori:recordnumber       LIKE(ori:recordnumber)         !Primary key field - type derived from field
ori:ordhno             LIKE(ori:ordhno)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::orh:Record  LIKE(orh:RECORD),STATIC
QuickWindow          WINDOW('Update the ordhead File'),AT(,,280,182),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateWebORders'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,272,156),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('&Order no:'),AT(8,20),USE(?orh:Order_no:Prompt)
                           STRING(@n12),AT(108,20,52,10),USE(orh:Order_no),RIGHT(1)
                           PROMPT('account no:'),AT(8,34),USE(?orh:account_no:Prompt)
                           ENTRY(@s40),AT(108,34,164,10),USE(orh:account_no)
                           PROMPT('&Cust Name:'),AT(8,48),USE(?orh:CustName:Prompt)
                           ENTRY(@s30),AT(108,48,124,10),USE(orh:CustName)
                           PROMPT('&Cust Add 1:'),AT(8,62),USE(?orh:CustAdd1:Prompt)
                           ENTRY(@s30),AT(108,62,124,10),USE(orh:CustAdd1)
                           PROMPT('&Cust Add 2:'),AT(8,76),USE(?orh:CustAdd2:Prompt)
                           ENTRY(@s30),AT(108,76,124,10),USE(orh:CustAdd2)
                           PROMPT('&Cust Add 3:'),AT(8,90),USE(?orh:CustAdd3:Prompt)
                           ENTRY(@s30),AT(108,90,124,10),USE(orh:CustAdd3)
                           PROMPT('&Cust Post Code:'),AT(8,104),USE(?orh:CustPostCode:Prompt)
                           ENTRY(@s10),AT(108,104,44,10),USE(orh:CustPostCode)
                           PROMPT('&Cust Tel:'),AT(8,118),USE(?orh:CustTel:Prompt)
                           ENTRY(@s20),AT(108,118,84,10),USE(orh:CustTel)
                           PROMPT('&Cust Fax:'),AT(8,132),USE(?orh:CustFax:Prompt)
                           ENTRY(@s20),AT(108,132,84,10),USE(orh:CustFax)
                           PROMPT('&d Name:'),AT(8,146),USE(?orh:dName:Prompt)
                           ENTRY(@s30),AT(108,146,124,10),USE(orh:dName)
                         END
                         TAB('General (cont.)'),USE(?Tab:2)
                           PROMPT('&d Add 1:'),AT(8,20),USE(?orh:dAdd1:Prompt)
                           ENTRY(@s30),AT(108,20,124,10),USE(orh:dAdd1)
                           PROMPT('&d Add 2:'),AT(8,34),USE(?orh:dAdd2:Prompt)
                           ENTRY(@s30),AT(108,34,124,10),USE(orh:dAdd2)
                           PROMPT('&d Add 3:'),AT(8,48),USE(?orh:dAdd3:Prompt)
                           ENTRY(@s30),AT(108,48,124,10),USE(orh:dAdd3)
                           PROMPT('&d Post Code:'),AT(8,62),USE(?orh:dPostCode:Prompt)
                           ENTRY(@s10),AT(108,62,44,10),USE(orh:dPostCode)
                           PROMPT('&d Tel:'),AT(8,76),USE(?orh:dTel:Prompt)
                           ENTRY(@s20),AT(108,76,84,10),USE(orh:dTel)
                           PROMPT('&d Fax:'),AT(8,90),USE(?orh:dFax:Prompt)
                           ENTRY(@s20),AT(108,90,84,10),USE(orh:dFax)
                           PROMPT('&date:'),AT(8,104),USE(?orh:thedate:Prompt)
                           ENTRY(@d8),AT(108,104,104,10),USE(orh:thedate),RIGHT(1)
                           PROMPT('&time:'),AT(8,118),USE(?orh:thetime:Prompt)
                           ENTRY(@t1),AT(108,118,104,10),USE(orh:thetime),RIGHT(1)
                           PROMPT('procesed:'),AT(8,132),USE(?orh:procesed:Prompt)
                           ENTRY(@n3),AT(108,132,40,10),USE(orh:procesed)
                           PROMPT('pro date:'),AT(8,146),USE(?orh:pro_date:Prompt)
                           ENTRY(@d17),AT(108,146,104,10),USE(orh:pro_date)
                         END
                         TAB('General (cont. 2)'),USE(?Tab:3)
                           PROMPT('Who Booked'),AT(8,20),USE(?orh:WhoBooked:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(108,20,124,10),USE(orh:WhoBooked),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Who Booked'),TIP('Who Booked'),UPR
                           PROMPT('Department'),AT(8,34),USE(?orh:Department:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(108,34,124,10),USE(orh:Department),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Department'),TIP('Department'),UPR
                           PROMPT('Customer Order Number'),AT(8,48),USE(?orh:CustOrderNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(108,48,124,10),USE(orh:CustOrderNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Customer Order Number'),TIP('Customer Order Number'),UPR
                           PROMPT('Date Despatched'),AT(8,62),USE(?orh:DateDespatched:Prompt)
                           ENTRY(@d6),AT(108,62,104,10),USE(orh:DateDespatched),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Date Despatched'),TIP('Date Despatched'),UPR
                           PROMPT('Sales Transaction Number'),AT(8,76),USE(?orh:SalesNumber:Prompt),TRN
                           ENTRY(@s8),AT(108,76,40,10),USE(orh:SalesNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Sales Transaction Number'),TIP('Sales Transaction Number'),UPR
                         END
                         TAB('Tab 4'),USE(?Tab4)
                           LIST,AT(12,20,150,100),USE(?List),IMM,MSG('Browsing Records'),FORMAT('80L(2)|M~partno~@s20@40R(2)|M~qty~L@n10@'),FROM(Queue:Browse)
                           BUTTON('&Insert'),AT(34,129,42,12),USE(?Insert)
                           BUTTON('&Change'),AT(78,129,42,12),USE(?Change)
                           BUTTON('&Delete'),AT(122,129,42,12),USE(?Delete)
                         END
                       END
                       BUTTON('OK'),AT(182,164,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(231,164,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(231,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW9                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?orh:Order_no:Prompt{prop:FontColor} = -1
    ?orh:Order_no:Prompt{prop:Color} = 15066597
    ?orh:Order_no{prop:FontColor} = -1
    ?orh:Order_no{prop:Color} = 15066597
    ?orh:account_no:Prompt{prop:FontColor} = -1
    ?orh:account_no:Prompt{prop:Color} = 15066597
    If ?orh:account_no{prop:ReadOnly} = True
        ?orh:account_no{prop:FontColor} = 65793
        ?orh:account_no{prop:Color} = 15066597
    Elsif ?orh:account_no{prop:Req} = True
        ?orh:account_no{prop:FontColor} = 65793
        ?orh:account_no{prop:Color} = 8454143
    Else ! If ?orh:account_no{prop:Req} = True
        ?orh:account_no{prop:FontColor} = 65793
        ?orh:account_no{prop:Color} = 16777215
    End ! If ?orh:account_no{prop:Req} = True
    ?orh:account_no{prop:Trn} = 0
    ?orh:account_no{prop:FontStyle} = font:Bold
    ?orh:CustName:Prompt{prop:FontColor} = -1
    ?orh:CustName:Prompt{prop:Color} = 15066597
    If ?orh:CustName{prop:ReadOnly} = True
        ?orh:CustName{prop:FontColor} = 65793
        ?orh:CustName{prop:Color} = 15066597
    Elsif ?orh:CustName{prop:Req} = True
        ?orh:CustName{prop:FontColor} = 65793
        ?orh:CustName{prop:Color} = 8454143
    Else ! If ?orh:CustName{prop:Req} = True
        ?orh:CustName{prop:FontColor} = 65793
        ?orh:CustName{prop:Color} = 16777215
    End ! If ?orh:CustName{prop:Req} = True
    ?orh:CustName{prop:Trn} = 0
    ?orh:CustName{prop:FontStyle} = font:Bold
    ?orh:CustAdd1:Prompt{prop:FontColor} = -1
    ?orh:CustAdd1:Prompt{prop:Color} = 15066597
    If ?orh:CustAdd1{prop:ReadOnly} = True
        ?orh:CustAdd1{prop:FontColor} = 65793
        ?orh:CustAdd1{prop:Color} = 15066597
    Elsif ?orh:CustAdd1{prop:Req} = True
        ?orh:CustAdd1{prop:FontColor} = 65793
        ?orh:CustAdd1{prop:Color} = 8454143
    Else ! If ?orh:CustAdd1{prop:Req} = True
        ?orh:CustAdd1{prop:FontColor} = 65793
        ?orh:CustAdd1{prop:Color} = 16777215
    End ! If ?orh:CustAdd1{prop:Req} = True
    ?orh:CustAdd1{prop:Trn} = 0
    ?orh:CustAdd1{prop:FontStyle} = font:Bold
    ?orh:CustAdd2:Prompt{prop:FontColor} = -1
    ?orh:CustAdd2:Prompt{prop:Color} = 15066597
    If ?orh:CustAdd2{prop:ReadOnly} = True
        ?orh:CustAdd2{prop:FontColor} = 65793
        ?orh:CustAdd2{prop:Color} = 15066597
    Elsif ?orh:CustAdd2{prop:Req} = True
        ?orh:CustAdd2{prop:FontColor} = 65793
        ?orh:CustAdd2{prop:Color} = 8454143
    Else ! If ?orh:CustAdd2{prop:Req} = True
        ?orh:CustAdd2{prop:FontColor} = 65793
        ?orh:CustAdd2{prop:Color} = 16777215
    End ! If ?orh:CustAdd2{prop:Req} = True
    ?orh:CustAdd2{prop:Trn} = 0
    ?orh:CustAdd2{prop:FontStyle} = font:Bold
    ?orh:CustAdd3:Prompt{prop:FontColor} = -1
    ?orh:CustAdd3:Prompt{prop:Color} = 15066597
    If ?orh:CustAdd3{prop:ReadOnly} = True
        ?orh:CustAdd3{prop:FontColor} = 65793
        ?orh:CustAdd3{prop:Color} = 15066597
    Elsif ?orh:CustAdd3{prop:Req} = True
        ?orh:CustAdd3{prop:FontColor} = 65793
        ?orh:CustAdd3{prop:Color} = 8454143
    Else ! If ?orh:CustAdd3{prop:Req} = True
        ?orh:CustAdd3{prop:FontColor} = 65793
        ?orh:CustAdd3{prop:Color} = 16777215
    End ! If ?orh:CustAdd3{prop:Req} = True
    ?orh:CustAdd3{prop:Trn} = 0
    ?orh:CustAdd3{prop:FontStyle} = font:Bold
    ?orh:CustPostCode:Prompt{prop:FontColor} = -1
    ?orh:CustPostCode:Prompt{prop:Color} = 15066597
    If ?orh:CustPostCode{prop:ReadOnly} = True
        ?orh:CustPostCode{prop:FontColor} = 65793
        ?orh:CustPostCode{prop:Color} = 15066597
    Elsif ?orh:CustPostCode{prop:Req} = True
        ?orh:CustPostCode{prop:FontColor} = 65793
        ?orh:CustPostCode{prop:Color} = 8454143
    Else ! If ?orh:CustPostCode{prop:Req} = True
        ?orh:CustPostCode{prop:FontColor} = 65793
        ?orh:CustPostCode{prop:Color} = 16777215
    End ! If ?orh:CustPostCode{prop:Req} = True
    ?orh:CustPostCode{prop:Trn} = 0
    ?orh:CustPostCode{prop:FontStyle} = font:Bold
    ?orh:CustTel:Prompt{prop:FontColor} = -1
    ?orh:CustTel:Prompt{prop:Color} = 15066597
    If ?orh:CustTel{prop:ReadOnly} = True
        ?orh:CustTel{prop:FontColor} = 65793
        ?orh:CustTel{prop:Color} = 15066597
    Elsif ?orh:CustTel{prop:Req} = True
        ?orh:CustTel{prop:FontColor} = 65793
        ?orh:CustTel{prop:Color} = 8454143
    Else ! If ?orh:CustTel{prop:Req} = True
        ?orh:CustTel{prop:FontColor} = 65793
        ?orh:CustTel{prop:Color} = 16777215
    End ! If ?orh:CustTel{prop:Req} = True
    ?orh:CustTel{prop:Trn} = 0
    ?orh:CustTel{prop:FontStyle} = font:Bold
    ?orh:CustFax:Prompt{prop:FontColor} = -1
    ?orh:CustFax:Prompt{prop:Color} = 15066597
    If ?orh:CustFax{prop:ReadOnly} = True
        ?orh:CustFax{prop:FontColor} = 65793
        ?orh:CustFax{prop:Color} = 15066597
    Elsif ?orh:CustFax{prop:Req} = True
        ?orh:CustFax{prop:FontColor} = 65793
        ?orh:CustFax{prop:Color} = 8454143
    Else ! If ?orh:CustFax{prop:Req} = True
        ?orh:CustFax{prop:FontColor} = 65793
        ?orh:CustFax{prop:Color} = 16777215
    End ! If ?orh:CustFax{prop:Req} = True
    ?orh:CustFax{prop:Trn} = 0
    ?orh:CustFax{prop:FontStyle} = font:Bold
    ?orh:dName:Prompt{prop:FontColor} = -1
    ?orh:dName:Prompt{prop:Color} = 15066597
    If ?orh:dName{prop:ReadOnly} = True
        ?orh:dName{prop:FontColor} = 65793
        ?orh:dName{prop:Color} = 15066597
    Elsif ?orh:dName{prop:Req} = True
        ?orh:dName{prop:FontColor} = 65793
        ?orh:dName{prop:Color} = 8454143
    Else ! If ?orh:dName{prop:Req} = True
        ?orh:dName{prop:FontColor} = 65793
        ?orh:dName{prop:Color} = 16777215
    End ! If ?orh:dName{prop:Req} = True
    ?orh:dName{prop:Trn} = 0
    ?orh:dName{prop:FontStyle} = font:Bold
    ?Tab:2{prop:Color} = 15066597
    ?orh:dAdd1:Prompt{prop:FontColor} = -1
    ?orh:dAdd1:Prompt{prop:Color} = 15066597
    If ?orh:dAdd1{prop:ReadOnly} = True
        ?orh:dAdd1{prop:FontColor} = 65793
        ?orh:dAdd1{prop:Color} = 15066597
    Elsif ?orh:dAdd1{prop:Req} = True
        ?orh:dAdd1{prop:FontColor} = 65793
        ?orh:dAdd1{prop:Color} = 8454143
    Else ! If ?orh:dAdd1{prop:Req} = True
        ?orh:dAdd1{prop:FontColor} = 65793
        ?orh:dAdd1{prop:Color} = 16777215
    End ! If ?orh:dAdd1{prop:Req} = True
    ?orh:dAdd1{prop:Trn} = 0
    ?orh:dAdd1{prop:FontStyle} = font:Bold
    ?orh:dAdd2:Prompt{prop:FontColor} = -1
    ?orh:dAdd2:Prompt{prop:Color} = 15066597
    If ?orh:dAdd2{prop:ReadOnly} = True
        ?orh:dAdd2{prop:FontColor} = 65793
        ?orh:dAdd2{prop:Color} = 15066597
    Elsif ?orh:dAdd2{prop:Req} = True
        ?orh:dAdd2{prop:FontColor} = 65793
        ?orh:dAdd2{prop:Color} = 8454143
    Else ! If ?orh:dAdd2{prop:Req} = True
        ?orh:dAdd2{prop:FontColor} = 65793
        ?orh:dAdd2{prop:Color} = 16777215
    End ! If ?orh:dAdd2{prop:Req} = True
    ?orh:dAdd2{prop:Trn} = 0
    ?orh:dAdd2{prop:FontStyle} = font:Bold
    ?orh:dAdd3:Prompt{prop:FontColor} = -1
    ?orh:dAdd3:Prompt{prop:Color} = 15066597
    If ?orh:dAdd3{prop:ReadOnly} = True
        ?orh:dAdd3{prop:FontColor} = 65793
        ?orh:dAdd3{prop:Color} = 15066597
    Elsif ?orh:dAdd3{prop:Req} = True
        ?orh:dAdd3{prop:FontColor} = 65793
        ?orh:dAdd3{prop:Color} = 8454143
    Else ! If ?orh:dAdd3{prop:Req} = True
        ?orh:dAdd3{prop:FontColor} = 65793
        ?orh:dAdd3{prop:Color} = 16777215
    End ! If ?orh:dAdd3{prop:Req} = True
    ?orh:dAdd3{prop:Trn} = 0
    ?orh:dAdd3{prop:FontStyle} = font:Bold
    ?orh:dPostCode:Prompt{prop:FontColor} = -1
    ?orh:dPostCode:Prompt{prop:Color} = 15066597
    If ?orh:dPostCode{prop:ReadOnly} = True
        ?orh:dPostCode{prop:FontColor} = 65793
        ?orh:dPostCode{prop:Color} = 15066597
    Elsif ?orh:dPostCode{prop:Req} = True
        ?orh:dPostCode{prop:FontColor} = 65793
        ?orh:dPostCode{prop:Color} = 8454143
    Else ! If ?orh:dPostCode{prop:Req} = True
        ?orh:dPostCode{prop:FontColor} = 65793
        ?orh:dPostCode{prop:Color} = 16777215
    End ! If ?orh:dPostCode{prop:Req} = True
    ?orh:dPostCode{prop:Trn} = 0
    ?orh:dPostCode{prop:FontStyle} = font:Bold
    ?orh:dTel:Prompt{prop:FontColor} = -1
    ?orh:dTel:Prompt{prop:Color} = 15066597
    If ?orh:dTel{prop:ReadOnly} = True
        ?orh:dTel{prop:FontColor} = 65793
        ?orh:dTel{prop:Color} = 15066597
    Elsif ?orh:dTel{prop:Req} = True
        ?orh:dTel{prop:FontColor} = 65793
        ?orh:dTel{prop:Color} = 8454143
    Else ! If ?orh:dTel{prop:Req} = True
        ?orh:dTel{prop:FontColor} = 65793
        ?orh:dTel{prop:Color} = 16777215
    End ! If ?orh:dTel{prop:Req} = True
    ?orh:dTel{prop:Trn} = 0
    ?orh:dTel{prop:FontStyle} = font:Bold
    ?orh:dFax:Prompt{prop:FontColor} = -1
    ?orh:dFax:Prompt{prop:Color} = 15066597
    If ?orh:dFax{prop:ReadOnly} = True
        ?orh:dFax{prop:FontColor} = 65793
        ?orh:dFax{prop:Color} = 15066597
    Elsif ?orh:dFax{prop:Req} = True
        ?orh:dFax{prop:FontColor} = 65793
        ?orh:dFax{prop:Color} = 8454143
    Else ! If ?orh:dFax{prop:Req} = True
        ?orh:dFax{prop:FontColor} = 65793
        ?orh:dFax{prop:Color} = 16777215
    End ! If ?orh:dFax{prop:Req} = True
    ?orh:dFax{prop:Trn} = 0
    ?orh:dFax{prop:FontStyle} = font:Bold
    ?orh:thedate:Prompt{prop:FontColor} = -1
    ?orh:thedate:Prompt{prop:Color} = 15066597
    If ?orh:thedate{prop:ReadOnly} = True
        ?orh:thedate{prop:FontColor} = 65793
        ?orh:thedate{prop:Color} = 15066597
    Elsif ?orh:thedate{prop:Req} = True
        ?orh:thedate{prop:FontColor} = 65793
        ?orh:thedate{prop:Color} = 8454143
    Else ! If ?orh:thedate{prop:Req} = True
        ?orh:thedate{prop:FontColor} = 65793
        ?orh:thedate{prop:Color} = 16777215
    End ! If ?orh:thedate{prop:Req} = True
    ?orh:thedate{prop:Trn} = 0
    ?orh:thedate{prop:FontStyle} = font:Bold
    ?orh:thetime:Prompt{prop:FontColor} = -1
    ?orh:thetime:Prompt{prop:Color} = 15066597
    If ?orh:thetime{prop:ReadOnly} = True
        ?orh:thetime{prop:FontColor} = 65793
        ?orh:thetime{prop:Color} = 15066597
    Elsif ?orh:thetime{prop:Req} = True
        ?orh:thetime{prop:FontColor} = 65793
        ?orh:thetime{prop:Color} = 8454143
    Else ! If ?orh:thetime{prop:Req} = True
        ?orh:thetime{prop:FontColor} = 65793
        ?orh:thetime{prop:Color} = 16777215
    End ! If ?orh:thetime{prop:Req} = True
    ?orh:thetime{prop:Trn} = 0
    ?orh:thetime{prop:FontStyle} = font:Bold
    ?orh:procesed:Prompt{prop:FontColor} = -1
    ?orh:procesed:Prompt{prop:Color} = 15066597
    If ?orh:procesed{prop:ReadOnly} = True
        ?orh:procesed{prop:FontColor} = 65793
        ?orh:procesed{prop:Color} = 15066597
    Elsif ?orh:procesed{prop:Req} = True
        ?orh:procesed{prop:FontColor} = 65793
        ?orh:procesed{prop:Color} = 8454143
    Else ! If ?orh:procesed{prop:Req} = True
        ?orh:procesed{prop:FontColor} = 65793
        ?orh:procesed{prop:Color} = 16777215
    End ! If ?orh:procesed{prop:Req} = True
    ?orh:procesed{prop:Trn} = 0
    ?orh:procesed{prop:FontStyle} = font:Bold
    ?orh:pro_date:Prompt{prop:FontColor} = -1
    ?orh:pro_date:Prompt{prop:Color} = 15066597
    If ?orh:pro_date{prop:ReadOnly} = True
        ?orh:pro_date{prop:FontColor} = 65793
        ?orh:pro_date{prop:Color} = 15066597
    Elsif ?orh:pro_date{prop:Req} = True
        ?orh:pro_date{prop:FontColor} = 65793
        ?orh:pro_date{prop:Color} = 8454143
    Else ! If ?orh:pro_date{prop:Req} = True
        ?orh:pro_date{prop:FontColor} = 65793
        ?orh:pro_date{prop:Color} = 16777215
    End ! If ?orh:pro_date{prop:Req} = True
    ?orh:pro_date{prop:Trn} = 0
    ?orh:pro_date{prop:FontStyle} = font:Bold
    ?Tab:3{prop:Color} = 15066597
    ?orh:WhoBooked:Prompt{prop:FontColor} = -1
    ?orh:WhoBooked:Prompt{prop:Color} = 15066597
    If ?orh:WhoBooked{prop:ReadOnly} = True
        ?orh:WhoBooked{prop:FontColor} = 65793
        ?orh:WhoBooked{prop:Color} = 15066597
    Elsif ?orh:WhoBooked{prop:Req} = True
        ?orh:WhoBooked{prop:FontColor} = 65793
        ?orh:WhoBooked{prop:Color} = 8454143
    Else ! If ?orh:WhoBooked{prop:Req} = True
        ?orh:WhoBooked{prop:FontColor} = 65793
        ?orh:WhoBooked{prop:Color} = 16777215
    End ! If ?orh:WhoBooked{prop:Req} = True
    ?orh:WhoBooked{prop:Trn} = 0
    ?orh:WhoBooked{prop:FontStyle} = font:Bold
    ?orh:Department:Prompt{prop:FontColor} = -1
    ?orh:Department:Prompt{prop:Color} = 15066597
    If ?orh:Department{prop:ReadOnly} = True
        ?orh:Department{prop:FontColor} = 65793
        ?orh:Department{prop:Color} = 15066597
    Elsif ?orh:Department{prop:Req} = True
        ?orh:Department{prop:FontColor} = 65793
        ?orh:Department{prop:Color} = 8454143
    Else ! If ?orh:Department{prop:Req} = True
        ?orh:Department{prop:FontColor} = 65793
        ?orh:Department{prop:Color} = 16777215
    End ! If ?orh:Department{prop:Req} = True
    ?orh:Department{prop:Trn} = 0
    ?orh:Department{prop:FontStyle} = font:Bold
    ?orh:CustOrderNumber:Prompt{prop:FontColor} = -1
    ?orh:CustOrderNumber:Prompt{prop:Color} = 15066597
    If ?orh:CustOrderNumber{prop:ReadOnly} = True
        ?orh:CustOrderNumber{prop:FontColor} = 65793
        ?orh:CustOrderNumber{prop:Color} = 15066597
    Elsif ?orh:CustOrderNumber{prop:Req} = True
        ?orh:CustOrderNumber{prop:FontColor} = 65793
        ?orh:CustOrderNumber{prop:Color} = 8454143
    Else ! If ?orh:CustOrderNumber{prop:Req} = True
        ?orh:CustOrderNumber{prop:FontColor} = 65793
        ?orh:CustOrderNumber{prop:Color} = 16777215
    End ! If ?orh:CustOrderNumber{prop:Req} = True
    ?orh:CustOrderNumber{prop:Trn} = 0
    ?orh:CustOrderNumber{prop:FontStyle} = font:Bold
    ?orh:DateDespatched:Prompt{prop:FontColor} = -1
    ?orh:DateDespatched:Prompt{prop:Color} = 15066597
    If ?orh:DateDespatched{prop:ReadOnly} = True
        ?orh:DateDespatched{prop:FontColor} = 65793
        ?orh:DateDespatched{prop:Color} = 15066597
    Elsif ?orh:DateDespatched{prop:Req} = True
        ?orh:DateDespatched{prop:FontColor} = 65793
        ?orh:DateDespatched{prop:Color} = 8454143
    Else ! If ?orh:DateDespatched{prop:Req} = True
        ?orh:DateDespatched{prop:FontColor} = 65793
        ?orh:DateDespatched{prop:Color} = 16777215
    End ! If ?orh:DateDespatched{prop:Req} = True
    ?orh:DateDespatched{prop:Trn} = 0
    ?orh:DateDespatched{prop:FontStyle} = font:Bold
    ?orh:SalesNumber:Prompt{prop:FontColor} = -1
    ?orh:SalesNumber:Prompt{prop:Color} = 15066597
    If ?orh:SalesNumber{prop:ReadOnly} = True
        ?orh:SalesNumber{prop:FontColor} = 65793
        ?orh:SalesNumber{prop:Color} = 15066597
    Elsif ?orh:SalesNumber{prop:Req} = True
        ?orh:SalesNumber{prop:FontColor} = 65793
        ?orh:SalesNumber{prop:Color} = 8454143
    Else ! If ?orh:SalesNumber{prop:Req} = True
        ?orh:SalesNumber{prop:FontColor} = 65793
        ?orh:SalesNumber{prop:Color} = 16777215
    End ! If ?orh:SalesNumber{prop:Req} = True
    ?orh:SalesNumber{prop:Trn} = 0
    ?orh:SalesNumber{prop:FontStyle} = font:Bold
    ?Tab4{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateWebORders',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateWebORders',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateWebORders',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:Order_no:Prompt;  SolaceCtrlName = '?orh:Order_no:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:Order_no;  SolaceCtrlName = '?orh:Order_no';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:account_no:Prompt;  SolaceCtrlName = '?orh:account_no:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:account_no;  SolaceCtrlName = '?orh:account_no';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:CustName:Prompt;  SolaceCtrlName = '?orh:CustName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:CustName;  SolaceCtrlName = '?orh:CustName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:CustAdd1:Prompt;  SolaceCtrlName = '?orh:CustAdd1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:CustAdd1;  SolaceCtrlName = '?orh:CustAdd1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:CustAdd2:Prompt;  SolaceCtrlName = '?orh:CustAdd2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:CustAdd2;  SolaceCtrlName = '?orh:CustAdd2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:CustAdd3:Prompt;  SolaceCtrlName = '?orh:CustAdd3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:CustAdd3;  SolaceCtrlName = '?orh:CustAdd3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:CustPostCode:Prompt;  SolaceCtrlName = '?orh:CustPostCode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:CustPostCode;  SolaceCtrlName = '?orh:CustPostCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:CustTel:Prompt;  SolaceCtrlName = '?orh:CustTel:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:CustTel;  SolaceCtrlName = '?orh:CustTel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:CustFax:Prompt;  SolaceCtrlName = '?orh:CustFax:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:CustFax;  SolaceCtrlName = '?orh:CustFax';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:dName:Prompt;  SolaceCtrlName = '?orh:dName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:dName;  SolaceCtrlName = '?orh:dName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:dAdd1:Prompt;  SolaceCtrlName = '?orh:dAdd1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:dAdd1;  SolaceCtrlName = '?orh:dAdd1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:dAdd2:Prompt;  SolaceCtrlName = '?orh:dAdd2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:dAdd2;  SolaceCtrlName = '?orh:dAdd2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:dAdd3:Prompt;  SolaceCtrlName = '?orh:dAdd3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:dAdd3;  SolaceCtrlName = '?orh:dAdd3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:dPostCode:Prompt;  SolaceCtrlName = '?orh:dPostCode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:dPostCode;  SolaceCtrlName = '?orh:dPostCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:dTel:Prompt;  SolaceCtrlName = '?orh:dTel:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:dTel;  SolaceCtrlName = '?orh:dTel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:dFax:Prompt;  SolaceCtrlName = '?orh:dFax:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:dFax;  SolaceCtrlName = '?orh:dFax';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:thedate:Prompt;  SolaceCtrlName = '?orh:thedate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:thedate;  SolaceCtrlName = '?orh:thedate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:thetime:Prompt;  SolaceCtrlName = '?orh:thetime:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:thetime;  SolaceCtrlName = '?orh:thetime';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:procesed:Prompt;  SolaceCtrlName = '?orh:procesed:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:procesed;  SolaceCtrlName = '?orh:procesed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:pro_date:Prompt;  SolaceCtrlName = '?orh:pro_date:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:pro_date;  SolaceCtrlName = '?orh:pro_date';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:3;  SolaceCtrlName = '?Tab:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:WhoBooked:Prompt;  SolaceCtrlName = '?orh:WhoBooked:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:WhoBooked;  SolaceCtrlName = '?orh:WhoBooked';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:Department:Prompt;  SolaceCtrlName = '?orh:Department:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:Department;  SolaceCtrlName = '?orh:Department';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:CustOrderNumber:Prompt;  SolaceCtrlName = '?orh:CustOrderNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:CustOrderNumber;  SolaceCtrlName = '?orh:CustOrderNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:DateDespatched:Prompt;  SolaceCtrlName = '?orh:DateDespatched:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:DateDespatched;  SolaceCtrlName = '?orh:DateDespatched';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:SalesNumber:Prompt;  SolaceCtrlName = '?orh:SalesNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orh:SalesNumber;  SolaceCtrlName = '?orh:SalesNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateWebORders')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateWebORders')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?orh:Order_no:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(orh:Record,History::orh:Record)
  SELF.AddHistoryField(?orh:Order_no,1)
  SELF.AddHistoryField(?orh:account_no,2)
  SELF.AddHistoryField(?orh:CustName,3)
  SELF.AddHistoryField(?orh:CustAdd1,4)
  SELF.AddHistoryField(?orh:CustAdd2,5)
  SELF.AddHistoryField(?orh:CustAdd3,6)
  SELF.AddHistoryField(?orh:CustPostCode,7)
  SELF.AddHistoryField(?orh:CustTel,8)
  SELF.AddHistoryField(?orh:CustFax,9)
  SELF.AddHistoryField(?orh:dName,10)
  SELF.AddHistoryField(?orh:dAdd1,11)
  SELF.AddHistoryField(?orh:dAdd2,12)
  SELF.AddHistoryField(?orh:dAdd3,13)
  SELF.AddHistoryField(?orh:dPostCode,14)
  SELF.AddHistoryField(?orh:dTel,15)
  SELF.AddHistoryField(?orh:dFax,16)
  SELF.AddHistoryField(?orh:thedate,17)
  SELF.AddHistoryField(?orh:thetime,18)
  SELF.AddHistoryField(?orh:procesed,19)
  SELF.AddHistoryField(?orh:pro_date,20)
  SELF.AddHistoryField(?orh:WhoBooked,21)
  SELF.AddHistoryField(?orh:Department,22)
  SELF.AddHistoryField(?orh:CustOrderNumber,23)
  SELF.AddHistoryField(?orh:DateDespatched,24)
  SELF.AddHistoryField(?orh:SalesNumber,25)
  SELF.AddUpdateFile(Access:ORDHEAD)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ORDHEAD.Open
  Relate:ORDITEMS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ORDHEAD
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:ORDITEMS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW9.Q &= Queue:Browse
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,ori:Keyordhno)
  BRW9.AddRange(ori:ordhno,orh:Order_no)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,ori:ordhno,1,BRW9)
  BRW9.AddField(ori:partno,BRW9.Q.ori:partno)
  BRW9.AddField(ori:qty,BRW9.Q.ori:qty)
  BRW9.AddField(ori:recordnumber,BRW9.Q.ori:recordnumber)
  BRW9.AddField(ori:ordhno,BRW9.Q.ori:ordhno)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  BRW9.AskProcedure = 1
  BRW9.AddToolbarTarget(Toolbar)
  BRW9.ToolbarItem.HelpButton = ?Help
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDHEAD.Close
    Relate:ORDITEMS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateWebORders',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateOrdItems
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateWebORders')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW9.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

