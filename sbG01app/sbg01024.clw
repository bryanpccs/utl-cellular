

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01024.INC'),ONCE        !Local module procedure declarations
                     END


TurnaroundPerformanceCriteria PROCEDURE               !Generated from procedure template - Window

tmp:IncludeNonAccessories BYTE(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:IncludeAccessories BYTE(0)
ExportQueue          QUEUE,PRE(tmpque)
Day                  STRING(30)
HitRate              LONG
QtyOrders            LONG
Value                REAL
                     END
savepath             STRING(255)
save_ret_id          USHORT,AUTO
tmp:BackOrders       BYTE(0)
save_res_id          USHORT,AUTO
window               WINDOW('Turnaround Performance Criteria'),AT(,,231,115),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,224,80),USE(?Sheet1),SPREAD
                         TAB('Turnaround Performance Criteria'),USE(?Tab1)
                           PROMPT('Start Date'),AT(8,20),USE(?tmp:StartDate:Prompt)
                           ENTRY(@d6),AT(84,20,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(152,20,10,10),USE(?LookupStartDate),SKIP,ICON('Calenda2.ico')
                           PROMPT('End Date'),AT(8,36),USE(?tmp:EndDate:Prompt)
                           ENTRY(@d6),AT(84,36,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(152,36,10,10),USE(?LookupEndDate),SKIP,ICON('Calenda2.ico')
                           CHECK('Include Exchange Accessory Sales'),AT(84,52),USE(tmp:IncludeAccessories),MSG('Include Accessories'),TIP('Include Accessories'),VALUE('1','0')
                           CHECK('Include Non-Exchange Accessory Sales'),AT(84,68),USE(tmp:IncludeNonAccessories),MSG('Include Non-Accessories'),TIP('Include Non-Accessories'),VALUE('1','0')
                         END
                       END
                       PANEL,AT(4,88,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Cancel'),AT(168,92,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                       BUTTON('&OK'),AT(112,92,56,16),USE(?Button4),LEFT,ICON('ok.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?tmp:StartDate:Prompt{prop:FontColor} = -1
    ?tmp:StartDate:Prompt{prop:Color} = 15066597
    If ?tmp:StartDate{prop:ReadOnly} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 15066597
    Elsif ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 8454143
    Else ! If ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 16777215
    End ! If ?tmp:StartDate{prop:Req} = True
    ?tmp:StartDate{prop:Trn} = 0
    ?tmp:StartDate{prop:FontStyle} = font:Bold
    ?tmp:EndDate:Prompt{prop:FontColor} = -1
    ?tmp:EndDate:Prompt{prop:Color} = 15066597
    If ?tmp:EndDate{prop:ReadOnly} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 15066597
    Elsif ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 8454143
    Else ! If ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 16777215
    End ! If ?tmp:EndDate{prop:Req} = True
    ?tmp:EndDate{prop:Trn} = 0
    ?tmp:EndDate{prop:FontStyle} = font:Bold
    ?tmp:IncludeAccessories{prop:Font,3} = -1
    ?tmp:IncludeAccessories{prop:Color} = 15066597
    ?tmp:IncludeAccessories{prop:Trn} = 0
    ?tmp:IncludeNonAccessories{prop:Font,3} = -1
    ?tmp:IncludeNonAccessories{prop:Color} = 15066597
    ?tmp:IncludeNonAccessories{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'TurnaroundPerformanceCriteria',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:IncludeNonAccessories',tmp:IncludeNonAccessories,'TurnaroundPerformanceCriteria',1)
    SolaceViewVars('tmp:StartDate',tmp:StartDate,'TurnaroundPerformanceCriteria',1)
    SolaceViewVars('tmp:EndDate',tmp:EndDate,'TurnaroundPerformanceCriteria',1)
    SolaceViewVars('tmp:IncludeAccessories',tmp:IncludeAccessories,'TurnaroundPerformanceCriteria',1)
    SolaceViewVars('ExportQueue:Day',ExportQueue:Day,'TurnaroundPerformanceCriteria',1)
    SolaceViewVars('ExportQueue:HitRate',ExportQueue:HitRate,'TurnaroundPerformanceCriteria',1)
    SolaceViewVars('ExportQueue:QtyOrders',ExportQueue:QtyOrders,'TurnaroundPerformanceCriteria',1)
    SolaceViewVars('ExportQueue:Value',ExportQueue:Value,'TurnaroundPerformanceCriteria',1)
    SolaceViewVars('savepath',savepath,'TurnaroundPerformanceCriteria',1)
    SolaceViewVars('save_ret_id',save_ret_id,'TurnaroundPerformanceCriteria',1)
    SolaceViewVars('tmp:BackOrders',tmp:BackOrders,'TurnaroundPerformanceCriteria',1)
    SolaceViewVars('save_res_id',save_res_id,'TurnaroundPerformanceCriteria',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate:Prompt;  SolaceCtrlName = '?tmp:StartDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate;  SolaceCtrlName = '?tmp:StartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupStartDate;  SolaceCtrlName = '?LookupStartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate:Prompt;  SolaceCtrlName = '?tmp:EndDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate;  SolaceCtrlName = '?tmp:EndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupEndDate;  SolaceCtrlName = '?LookupEndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:IncludeAccessories;  SolaceCtrlName = '?tmp:IncludeAccessories';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:IncludeNonAccessories;  SolaceCtrlName = '?tmp:IncludeNonAccessories';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4;  SolaceCtrlName = '?Button4';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('TurnaroundPerformanceCriteria')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'TurnaroundPerformanceCriteria')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:StartDate:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:RETSALES.Open
  Access:RETSTOCK.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETSALES.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'TurnaroundPerformanceCriteria',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
      savepath = path()
      glo:file_name = 'TURNPERM.CSV'
      If not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
                  file:keepdir + file:noerror + file:longname)
          !Failed
          setpath(savepath)
      else!If not filedialog
          !Found
          setpath(savepath)
      
          Setcursor(Cursor:Wait)
      
      !Build up the temp queue first
          Clear(ExportQueue)
          Free(ExportQueue)
          Loop x# = 0 To 15
              If x# <> 15
                  tmpque:Day = x#
              Else !If x# <> 15
                  tmpque:Day = 'Over 14'
              End !If x# <> 15
              tmpque:HitRate      = 0
              tmpque:QtyOrders    = 0
              tmpque:Value        = 0
              Add(ExportQueue)
          End !Loop x# = 1 To 16
      
          Save_ret_ID = Access:RETSALES.SaveFile()
          Access:RETSALES.ClearKey(ret:Date_Booked_Key)
          ret:date_booked = tmp:StartDate
          Set(ret:Date_Booked_Key,ret:Date_Booked_Key)
          Loop
              If Access:RETSALES.NEXT()
                 Break
              End !If
              If ret:date_booked > tmp:EndDate      |
                  Then Break.  ! End If
      
      !Check Critiria
              If tmp:IncludeNonAccessories And ret:Payment_Method = 'EXC'
                  Cycle
              End !If tmp:IncludeNonAccessories And ret:Payment_Method = 'EXC'
      
              If tmp:IncludeAccessories And ret:Payment_Method = 'EXC'
                  Cycle
              End !If tmp:InvoiceAccessories And ret:Payment_Method = 'EXC'
      
      !Need to exclude Back Orders. Lets try and find some
              tmp:BackOrders = 0
              Save_res_ID = Access:RETSTOCK.SaveFile()
              Access:RETSTOCK.ClearKey(res:Part_Number_Key)
              res:Ref_Number  = ret:Ref_Number
              Set(res:Part_Number_Key,res:Part_Number_Key)
              Loop
                  If Access:RETSTOCK.NEXT()
                     Break
                  End !If
                  If res:Ref_Number  <> ret:Ref_Number      |
                      Then Break.  ! End If
                  If res:Order_Number <> ''
                      tmp:BackORders = 1
                      Break
                  End !If res:Order_Number <> ''
              End !Loop
              Access:RETSTOCK.RestoreFile(Save_res_ID)
      
              If tmp:BackOrders
                  Cycle
              End !If tmp:BackOrders
      
      !Lets do some exporting
      
              If ret:Date_Despatched = ret:Date_Booked
                  tmpque:Day  = '0'
                  Get(ExportQueue,tmpque:Day)
                  If ~Error()
      
                  End !If ~Error()
      
              End !If ret:Date_Despatched = ret:Date_Booked
      
      
          End !Loop
          Access:RETSALES.RestoreFile(Save_ret_ID)
          Setcursor()
      End!If not filedialog
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'TurnaroundPerformanceCriteria')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

