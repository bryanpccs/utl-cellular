

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBG01034.INC'),ONCE        !Local module procedure declarations
                     END


RetailPaid           PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_rtp_id          USHORT,AUTO
tmp:VATRate          REAL
tmp:PaymentTaken     REAL
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'RetailPaid')      !Add Procedure to Log
  end


!Quick fix routine. Place this in a01 later.
    If ret:Payment_Method = 'CAS'

        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number  = ret:Account_Number
        If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Found
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = sub:Main_Account_Number
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Found
                If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'        
                    access:vatcode.clearkey(vat:vat_code_key)
                    vat:vat_code = sub:retail_vat_code
                    if access:vatcode.fetch(vat:vat_code_key) = level:benign
                        tmp:VatRate = vat:vat_rate
                    end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                    
                Else !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'        
                    access:vatcode.clearkey(vat:vat_code_key)
                    vat:vat_code = tra:retail_vat_code
                    if access:vatcode.fetch(vat:vat_code_key) = level:benign
                        tmp:VatRate = vat:vat_rate
                    end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                    
                End !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'        
            Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            
        Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

        tmp:PaymentTaken = 0
        setcursor(cursor:wait)                                                                  !Has the stock been paid for
        save_rtp_id = access:retpay.savefile()
        access:retpay.clearkey(rtp:date_key)
        rtp:ref_number   = ret:ref_number
        set(rtp:date_key,rtp:date_key)
        loop
            if access:retpay.next()
               break
            end !if
            if rtp:ref_number   <> ret:ref_number      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            tmp:PaymentTaken += rtp:amount
        end !loop
        access:retpay.restorefile(save_rtp_id)
        setcursor()

        If Round((ret:Sub_Total + (ret:Sub_Total * tmp:VatRate/100)),.01) - Round(tmp:PaymentTaken,.01) > .01
            Return Level:Fatal
        End !If Round((ret:Sub_Total + (ret:Sub_Total * tmp:VatRate/100)),.01) - Round(tmp:PaymentTaken,.01) > .01
    End !If ret:Payment_Method = 'CAS'
    Return Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'RetailPaid',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_rtp_id',save_rtp_id,'RetailPaid',1)
    SolaceViewVars('tmp:VATRate',tmp:VATRate,'RetailPaid',1)
    SolaceViewVars('tmp:PaymentTaken',tmp:PaymentTaken,'RetailPaid',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
