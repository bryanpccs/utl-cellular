

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBG01032.INC'),ONCE        !Local module procedure declarations
                     END


UpdateRetailPartRoutine PROCEDURE                     ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_res_id          USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'UpdateRetailPartRoutine')      !Add Procedure to Log
  end


    access:stock.clearkey(sto:ref_number_key)
    sto:ref_number  = glo:select3
    If access:stock.fetch(sto:ref_number_key)
        Case MessageEx('An Error has occured retreiving the details of this Stock Part.','ServiceBase 2000',icon:hand,'|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,0+2+0+0,84,26,0) 
            Of 1 ! &OK Button
        End!Case MessagheEx
    Else!If access:stock.fetch(sto:ref_number_key)
        Access:RETSTOCK_ALIAS.Clearkey(ret_ali:Record_Number_Key)
        ret_ali:Record_Number = glo:Select4
        If Access:RETSTOCK_ALIAS.Tryfetch(ret_ali:Record_Number_Key) = Level:Benign
            !Found
            If Access:RETSTOCK.PrimeRecord() = Level:Benign
                RecordNumber$           = res:Record_Number
                res:Record              :=: ret_ali:Record
                res:Record_Number       = RecordNumber$
                res:Quantity            = glo:Select5
                res:Pending_Ref_Number  = glo:Select6
                If glo:Select2 = 'NEW PENDING'
                    res:Despatched = 'PEN'
                End !If glo:Select2 = 'NEW PENDING'
                If glo:Select2= 'CANCELLED'
                    res:Despatched = 'CAN'
                End !If glo:Select2= 'CANCELLED'
                res:Date_Ordered        = ''
                res:Date_Received       = ''
                If Access:RETSTOCK.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:RETSTOCK.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:RETSTOCK.TryInsert() = Level:Benign
            End !If Access:RETSTOCK.PrimeRecord() = Level:Benign
        Else! If Access:RETSTOCK_ALIAS.Tryfetch(ret_ali:Record_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:RETSTOCK_ALIAS.Tryfetch(ret_ali:Record_Number_Key) = Level:Benign
    End !If access:stock.fetch(sto:ref_number_key)


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateRetailPartRoutine',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_res_id',save_res_id,'UpdateRetailPartRoutine',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
