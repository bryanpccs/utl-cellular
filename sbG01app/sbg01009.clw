

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01009.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Undelivered_Parts PROCEDURE                    !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisThreadActive BYTE
CurrentTab           STRING(80)
sales_queue_temp     QUEUE,PRE(SALTMP)
ref_number           LONG
                     END
stop_on_errors_temp  BYTE
save_res_id          USHORT,AUTO
save_orp_id          USHORT,AUTO
pos                  STRING(255)
vat_rate_temp        REAL
trade_queue_temp     QUEUE,PRE(TMPTRA)
Account_Number       STRING(15)
                     END
no_temp              STRING('NO')
Account_Number_temp  STRING(15)
ret_temp             STRING('RET')
yes_temp             STRING('YES')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Account_Number_temp
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(ORDPARTS)
                       PROJECT(orp:Part_Number)
                       PROJECT(orp:Description)
                       PROJECT(orp:Account_Number)
                       PROJECT(orp:Quantity)
                       PROJECT(orp:Job_Number)
                       PROJECT(orp:Record_Number)
                       PROJECT(orp:Part_Type)
                       PROJECT(orp:All_Received)
                       PROJECT(orp:Allocated_To_Sale)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
orp:Part_Number        LIKE(orp:Part_Number)          !List box control field - type derived from field
orp:Description        LIKE(orp:Description)          !List box control field - type derived from field
orp:Account_Number     LIKE(orp:Account_Number)       !List box control field - type derived from field
orp:Quantity           LIKE(orp:Quantity)             !List box control field - type derived from field
orp:Job_Number         LIKE(orp:Job_Number)           !List box control field - type derived from field
orp:Record_Number      LIKE(orp:Record_Number)        !Primary key field - type derived from field
orp:Part_Type          LIKE(orp:Part_Type)            !Browse key field - type derived from field
orp:All_Received       LIKE(orp:All_Received)         !Browse key field - type derived from field
orp:Allocated_To_Sale  LIKE(orp:Allocated_To_Sale)    !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK6::orp:Account_Number   LIKE(orp:Account_Number)
HK6::orp:All_Received     LIKE(orp:All_Received)
HK6::orp:Allocated_To_Sale LIKE(orp:Allocated_To_Sale)
HK6::orp:Part_Type        LIKE(orp:Part_Type)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB5::View:FileDropCombo VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
QuickWindow          WINDOW('Browse Unallocated Parts'),AT(,,515,235),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),HLP('Browse_Undelivered_Parts'),SYSTEM,GRAY,DOUBLE,IMM
                       ENTRY(@s30),AT(8,24,124,10),USE(orp:Part_Number),UPR
                       LIST,AT(8,40,416,188),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~Part Number~@s30@120L(2)|M~Description~@s30@60L(2)|M~Account Number~@s' &|
   '15@32D(2)|M~Quantity~L@p<<<<<<<<<<<<<<#p@36D(2)|M~Sale Number~L@p<<<<<<<<<<<<<<<<#p@'),FROM(Queue:Browse:1)
                       BUTTON('Clear Entry'),AT(436,44,76,20),USE(?ClearEntry),LEFT,ICON('DELETE.ICO')
                       BUTTON('Clear List'),AT(436,68,76,20),USE(?Button4),LEFT,ICON(ICON:Hand)
                       SHEET,AT(4,4,424,228),USE(?CurrentTab),SPREAD
                         TAB('By Part Number'),USE(?Tab:2)
                           BUTTON('Create Sales From All Parts'),AT(436,8,76,20),USE(?All_Parts),LEFT,ICON('Clipbrd.gif')
                         END
                         TAB('Account Number'),USE(?Tab2)
                           BUTTON('Create Sales For Account'),AT(436,8,76,20),USE(?Trade_Account),LEFT,ICON('Clipbrd.gif')
                           COMBO(@s15),AT(136,24,124,10),USE(Account_Number_temp),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('67L(2)|M@s15@120L(2)|M@s30@'),DROP(10,200),FROM(Queue:FileDropCombo)
                         END
                       END
                       BUTTON('Close'),AT(436,212,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort1:Locator  StepLocatorClass                 !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    If ?orp:Part_Number{prop:ReadOnly} = True
        ?orp:Part_Number{prop:FontColor} = 65793
        ?orp:Part_Number{prop:Color} = 15066597
    Elsif ?orp:Part_Number{prop:Req} = True
        ?orp:Part_Number{prop:FontColor} = 65793
        ?orp:Part_Number{prop:Color} = 8454143
    Else ! If ?orp:Part_Number{prop:Req} = True
        ?orp:Part_Number{prop:FontColor} = 65793
        ?orp:Part_Number{prop:Color} = 16777215
    End ! If ?orp:Part_Number{prop:Req} = True
    ?orp:Part_Number{prop:Trn} = 0
    ?orp:Part_Number{prop:FontStyle} = font:Bold
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    If ?Account_Number_temp{prop:ReadOnly} = True
        ?Account_Number_temp{prop:FontColor} = 65793
        ?Account_Number_temp{prop:Color} = 15066597
    Elsif ?Account_Number_temp{prop:Req} = True
        ?Account_Number_temp{prop:FontColor} = 65793
        ?Account_Number_temp{prop:Color} = 8454143
    Else ! If ?Account_Number_temp{prop:Req} = True
        ?Account_Number_temp{prop:FontColor} = 65793
        ?Account_Number_temp{prop:Color} = 16777215
    End ! If ?Account_Number_temp{prop:Req} = True
    ?Account_Number_temp{prop:Trn} = 0
    ?Account_Number_temp{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Undelivered_Parts',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Undelivered_Parts',1)
    SolaceViewVars('sales_queue_temp:ref_number',sales_queue_temp:ref_number,'Browse_Undelivered_Parts',1)
    SolaceViewVars('stop_on_errors_temp',stop_on_errors_temp,'Browse_Undelivered_Parts',1)
    SolaceViewVars('save_res_id',save_res_id,'Browse_Undelivered_Parts',1)
    SolaceViewVars('save_orp_id',save_orp_id,'Browse_Undelivered_Parts',1)
    SolaceViewVars('pos',pos,'Browse_Undelivered_Parts',1)
    SolaceViewVars('vat_rate_temp',vat_rate_temp,'Browse_Undelivered_Parts',1)
    SolaceViewVars('trade_queue_temp:Account_Number',trade_queue_temp:Account_Number,'Browse_Undelivered_Parts',1)
    SolaceViewVars('no_temp',no_temp,'Browse_Undelivered_Parts',1)
    SolaceViewVars('Account_Number_temp',Account_Number_temp,'Browse_Undelivered_Parts',1)
    SolaceViewVars('ret_temp',ret_temp,'Browse_Undelivered_Parts',1)
    SolaceViewVars('yes_temp',yes_temp,'Browse_Undelivered_Parts',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?orp:Part_Number;  SolaceCtrlName = '?orp:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ClearEntry;  SolaceCtrlName = '?ClearEntry';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4;  SolaceCtrlName = '?Button4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?All_Parts;  SolaceCtrlName = '?All_Parts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Trade_Account;  SolaceCtrlName = '?Trade_Account';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Account_Number_temp;  SolaceCtrlName = '?Account_Number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Undelivered_Parts')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Undelivered_Parts')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?orp:Part_Number
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Browse_Undelivered_Parts'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:INVOICE.Open
  Relate:RETDESNO.Open
  Relate:RETSALES_ALIAS.Open
  Relate:RETSTOCK_ALIAS.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Access:TRADEACC.UseFile
  Access:RETSALES.UseFile
  Access:RETSTOCK.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ORDPARTS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,orp:Allocated_Account_Key)
  BRW1.AddRange(orp:Account_Number,Account_Number_temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,orp:Part_Number,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,orp:Allocated_Key)
  BRW1.AddRange(orp:Allocated_To_Sale,no_temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,orp:Part_Number,1,BRW1)
  BRW1.AddField(orp:Part_Number,BRW1.Q.orp:Part_Number)
  BRW1.AddField(orp:Description,BRW1.Q.orp:Description)
  BRW1.AddField(orp:Account_Number,BRW1.Q.orp:Account_Number)
  BRW1.AddField(orp:Quantity,BRW1.Q.orp:Quantity)
  BRW1.AddField(orp:Job_Number,BRW1.Q.orp:Job_Number)
  BRW1.AddField(orp:Record_Number,BRW1.Q.orp:Record_Number)
  BRW1.AddField(orp:Part_Type,BRW1.Q.orp:Part_Type)
  BRW1.AddField(orp:All_Received,BRW1.Q.orp:All_Received)
  BRW1.AddField(orp:Allocated_To_Sale,BRW1.Q.orp:Allocated_To_Sale)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB5.Init(Account_Number_temp,?Account_Number_temp,Queue:FileDropCombo.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo,Relate:SUBTRACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo
  FDCB5.AddSortOrder(sub:Account_Number_Key)
  FDCB5.AddField(sub:Account_Number,FDCB5.Q.sub:Account_Number)
  FDCB5.AddField(sub:Company_Name,FDCB5.Q.sub:Company_Name)
  FDCB5.AddField(sub:RecordNumber,FDCB5.Q.sub:RecordNumber)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:INVOICE.Close
    Relate:RETDESNO.Close
    Relate:RETSALES_ALIAS.Close
    Relate:RETSTOCK_ALIAS.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='Browse_Undelivered_Parts'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Undelivered_Parts',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ClearEntry
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearEntry, Accepted)
      If SecurityCheck('RETAIL BACK ORDERS - DELETE')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else !SecurityCheck('RETAIL BACK ORDERS - CLEAR')
          Case MessageEx('Are you sure you want to clear the Back Order entry?','ServiceBase 2000',|
                         'Styles\trash.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
              Of 1 ! &Yes Button
      
                  Brw1.UpdateBuffer()
                  orp:Allocated_To_Sale = 'YES'
                  Access:ORDPARTS.Update()
      
              Of 2 ! &No Button
          End!Case MessageEx
      End !SecurityCheck('RETAIL BACK ORDERS - CLEAR')
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearEntry, Accepted)
    OF ?Button4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
      If SecurityCheck('RETAIL BACK ORDERS - CLEAR')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else !SecurityCheck('RETAIL BACK ORDERS - CLEAR')
          Case MessageEx('Are you sure you want to clear the Back Order list?','ServiceBase 2000',|
                         'Styles\trash.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
              Of 1 ! &Yes Button
                  Case MessageEx('Warning! You will not be able to reverse this action.'&|
                    '<13,10>'&|
                    '<13,10>Are you sure?','ServiceBase 2000',|
                                 'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                          setcursor(cursor:wait)
                          save_orp_id = access:ordparts.savefile()
                          access:ordparts.clearkey(orp:allocated_key)
                          orp:part_type         = 'RET'
                          orp:all_received      = 'YES'
                          orp:allocated_to_sale = 'NO'
                          set(orp:allocated_key,orp:allocated_key)
                          loop
                              if access:ordparts.next()
                                 break
                              end !if
                              if orp:part_type         <> 'RET'      |
                              or orp:all_received      <> 'YES'      |
                              or orp:allocated_to_sale <> 'NO'      |
                                  then break.  ! end if
      
                              pos = Position(orp:allocated_key)
                              orp:allocated_to_sale   = 'YES'
                              access:ordparts.update()
                              Reset(orp:allocated_key,pos)
                          end !loop
                          access:ordparts.restorefile(save_orp_id)
                          setcursor()
                      Of 2 ! &No Button
                  End!Case MessageEx
      
              Of 2 ! &No Button
          End!Case MessageEx
      End !SecurityCheck('RETAIL BACK ORDERS - CLEAR')
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
    OF ?All_Parts
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?All_Parts, Accepted)
      Case MessageEx('Are you sure you want to create Sales for ALL the Unallocated parts?','ServiceBase 2000','Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Clear(trade_queue_temp)
              Free(trade_queue_temp)
              Clear(sales_queue_temp)
              Free(sales_queue_temp)
              setcursor(cursor:wait)
              save_orp_id = access:ordparts.savefile()
              access:ordparts.clearkey(orp:allocated_key)
              orp:part_type         = 'RET'
              orp:all_received      = 'YES'
              orp:allocated_to_sale = 'NO'
              set(orp:allocated_key,orp:allocated_key)
              loop
                  if access:ordparts.next()
                     break
                  end !if
                  if orp:part_type         <> 'RET'      |
                  or orp:all_received      <> 'YES'   |
                  or orp:allocated_to_sale <> 'NO'      |
                      then break.  ! end if
                  yldcnt# += 1
                  if yldcnt# > 25
                     yield() ; yldcnt# = 0
                  end !if
                  
                  Sort(trade_queue_temp,tratmp:account_number)
                  tratmp:account_number   = orp:account_number
                  Get(trade_queue_temp,tratmp:account_number)
                  If Error()
                      Add(trade_queue_temp)
                  End
              end !loop
              access:ordparts.restorefile(save_orp_id)
              setcursor()
              count# = 0
      
              Sort(trade_queue_temp,tratmp:account_number)
              Loop x# = 1 To Records(trade_queue_temp)
                  Get(trade_queue_temp,x#)
                  error# = 0
                  get(retsales,0)
                  if access:retsales.primerecord() = Level:Benign
                      access:users.clearkey(use:password_key)
                      use:password    =glo:password
                      access:users.fetch(use:password_key)
                      ret:who_booked  = use:user_code
      
                      access:subtracc.clearkey(sub:account_number_key)
                      sub:account_number = tratmp:account_number
                      if access:subtracc.tryfetch(sub:account_number_key)
                          error# = 1
                      Else!if access:subtracc.tryfetch(sub:account_number_key)
      
                          access:tradeacc.clearkey(tra:account_number_key)
                          tra:account_number = sub:main_account_number
                          if access:tradeacc.fetch(tra:account_number_key)
                              error# = 1
                          Else!if access:tradeacc.fetch(tra:account_number_key)
      
                              if tra:invoice_sub_accounts = 'YES'
                                  access:vatcode.clearkey(vat:vat_code_key)
                                  vat:vat_code = sub:retail_vat_code
                                  if access:vatcode.fetch(vat:vat_code_key) = level:benign
                                      vat_rate_temp = vat:vat_rate
                                  end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                                  access:vatcode.clearkey(vat:vat_code_key)
                                  vat:vat_code = sub:retail_vat_code
                                  if access:vatcode.fetch(vat:vat_code_key) = level:benign
                                      vat_rate_temp = vat:vat_rate
                                  end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                              else!if tra:use_sub_accounts = 'YES'
                                  access:vatcode.clearkey(vat:vat_code_key)
                                  vat:vat_code = tra:retail_vat_code
                                  if access:vatcode.fetch(vat:vat_code_key) = level:benign
                                      vat_rate_temp = vat:vat_rate
                                  end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                                  access:vatcode.clearkey(vat:vat_code_key)
                                  vat:vat_code = tra:retail_vat_code
                                  if access:vatcode.fetch(vat:vat_code_key) = level:benign
                                      vat_rate_temp = vat:vat_rate
                                  end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                              end!if tra:use_sub_accounts = 'YES'
      
                              If tra:use_sub_accounts <> 'YES'
                                  ret:courier_cost    = TRA:Courier_Cost
                                  ret:courier         = tra:courier_outgoing
                                  use_invoice# = 1
                                  If tra:use_delivery_address = 'YES'
                                      use_delivery# = 1
                                  End!If tra:use_delivery_address = 'YES'
                                  If TRA:Retail_Payment_Type = 'ACC'
                                      ret:payment_method  = 'TRA'
                                  End!If SUB:Retail_Payment_Type = 'ACC'
                                  IF TRA:retail_payment_type  = 'CAS'
                                      ret:payment_method = 'CAS'
                                  End!IF sub:retail_payment_type  = 'CAS'
      
                              Else!If tra:use_sub_accounts <> 'YES'
                                  ret:courier_cost    = sub:courier_cost
                                  ret:courier         = sub:courier_outgoing
                                  If tra:invoice_sub_accounts = 'YES'
                                      use_invoice# = 2
                                      If sub:use_delivery_address = 'YES'
                                          use_delivery# = 2
                                      End!If sub:use_delivery_address = 'YES'
                                      If SUB:Retail_Payment_Type = 'ACC'
                                          ret:payment_method  = 'TRA'
                                      End!If SUB:Retail_Payment_Type = 'ACC'
                                      IF sub:retail_payment_type  = 'CAS'
                                          ret:payment_method = 'CAS'
                                      End!IF sub:retail_payment_type  = 'CAS'
                                  Else!If tra:invoice_sub_accounts = 'YES'
                                      use_invoice# = 1
                                      If TRA:Retail_Payment_Type = 'ACC'
                                          ret:payment_method  = 'TRA'
                                      End!If SUB:Retail_Payment_Type = 'ACC'
                                      IF TRA:retail_payment_type  = 'CAS'
                                          ret:payment_method = 'CAS'
                                      End!IF sub:retail_payment_type  = 'CAS'
                                  End!If tra:invoice_sub_accounts = 'YES'
      
                              End!If tra:use_sub_accounts <> 'YES'
                              Case use_invoice#
                                  OF 1
                                      RET:Postcode        = tra:postcode
                                      RET:Company_Name    = tra:company_name
                                      RET:Building_Name   = ''
                                      RET:Address_Line1   = tra:address_line1
                                      RET:Address_Line2   = tra:address_line2
                                      RET:Address_Line3   = tra:address_line3
                                      RET:Telephone_Number= tra:telephone_number
                                      RET:Fax_Number      = tra:fax_number
                                  Of 2
                                      RET:Postcode        = sub:postcode
                                      RET:Company_Name    = sub:company_name
                                      RET:Building_Name   = ''
                                      RET:Address_Line1   = sub:address_line1
                                      RET:Address_Line2   = sub:address_line2
                                      RET:Address_Line3   = sub:address_line3
                                      RET:Telephone_Number= sub:telephone_number
                                      RET:Fax_Number      = sub:fax_number
                              End!If use_invoice# = 1
                              Case use_delivery#
                                  Of 1
                                      RET:Postcode_Delivery       = tra:postcode
                                      RET:Company_Name_Delivery   = tra:company_name
                                      RET:Building_Name_Delivery  = ''
                                      RET:Address_Line1_Delivery  = tra:address_line1
                                      RET:Address_Line2_Delivery  = tra:address_line2
                                      RET:Address_Line3_Delivery  = tra:address_line3
                                      RET:Telephone_Delivery      = tra:telephone_number
                                      RET:Fax_Number_Delivery     = tra:fax_number
                                  Of 2
                                      RET:Postcode_Delivery       = sub:postcode
                                      RET:Company_Name_Delivery   = sub:company_name
                                      RET:Building_Name_Delivery  = ''
                                      RET:Address_Line1_Delivery  = sub:address_line1
                                      RET:Address_Line2_Delivery  = sub:address_line2
                                      RET:Address_Line3_Delivery  = sub:address_line3
                                      RET:Telephone_Delivery      = sub:telephone_number
                                      RET:Fax_Number_Delivery     = sub:fax_number
                              End!Case use_delivery#
                              ret:account_number  = tratmp:account_number
                              ret:purchase_order_number   = 'VARIOUS'
                              if access:retsales.insert()
                                  error# = 1
                              Else!if access:retsales.insert()
                                  count# += 1
                                  save_orp_id = access:ordparts.savefile()
                                  access:ordparts.clearkey(orp:allocated_account_key)
                                  orp:part_type         = 'RET'
                                  orp:all_received      = 'YES'
                                  orp:allocated_to_sale = 'NO'
                                  orp:account_number    = tratmp:account_number
                                  set(orp:allocated_account_key,orp:allocated_account_key)
                                  loop
                                      if access:ordparts.next()
                                         break
                                      end !if
                                      if orp:part_type         <> 'RET'      |
                                      or orp:all_received      <> 'YES'      |
                                      or orp:allocated_to_sale <> 'NO'      |
                                      or orp:account_number    <> tratmp:account_number      |
                                          then break.  ! end if
      
      
                                      access:retstock_alias.clearkey(ret_ali:order_part_key)
                                      ret_ali:ref_number        = orp:job_number
                                      ret_ali:order_part_number = orp:record_number
                                      if access:retstock_alias.tryfetch(ret_ali:order_part_key) = Level:Benign
                                          get(retstock,0)
                                          if access:retstock.primerecord() = Level:Benign
                                              record_number$      = res:record_number
                                              res:record  :=: ret_ali:record
                                              res:record_number   = record_number$
                                              res:ref_number           = ret:ref_number
                                              res:quantity             = orp:quantity
                                              res:despatched           = 'YES'
                                              res:order_number         = orp:order_number
                                              res:date_ordered         = Today()
                                              res:date_received        = orp:date_received
                                              res:despatch_note_number = ''
                                              res:despatch_date        = ''
                                              res:part_ref_number      = orp:part_ref_number
                                              res:pending_ref_number   = ''
                                              res:order_part_number    = orp:record_number
                                              res:purchase_order_number = ret_ali:purchase_order_number
                                              res:previous_sale_number    = ret_ali:ref_number
                                              if access:retstock.insert()
                                                 access:retstock.cancelautoinc()
                                              end
                                          End!if access:retstock.primerecord() = Level:Benign
                                      end!if access:retstock_alias.tryfetch(ret_ali:order_part_key) = Level:Benign
      
                                      pos = Position(orp:allocated_account_key)
                                      orp:allocated_to_sale   = 'YES'
                                      orp:job_number          = ret:ref_number
                                      access:ordparts.update()
                                      Reset(orp:allocated_account_key,pos)
      
                                  end !loop
                                  access:ordparts.restorefile(save_orp_id)
      
                                  saltmp:ref_number   = ret:ref_number
                                  Add(sales_queue_temp)
      
                              End!if access:retsales.insert()
                          End!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                      end!if access:subtracc.tryfetch(sub:account_number_key) = Level:Benign
                  end!if access:retsales.primerecord() = Level:Benign
                  If error# = 1
                      access:retsales.cancelautoinc()
                  End!If error# = 1
              End!Loop x# = 1 To Records(trade_queue_temp)
      
              Case MessageEx(Clip(count#) & ' New Sales have been created.<13,10><13,10>Picking Notes, Despatch Notes and Invoices will now be produced.<13,10><13,10>(This message will close automatically)','ServiceBase 2000','Styles\idea.ico','|&Close',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,500)
                  Of 1 ! &Close Button
              End!Case MessageEx
      
              Clear(sales_queue_temp)
              Sort(sales_queue_temp,saltmp:ref_number)
              Loop x# = 1 To Records(sales_queue_Temp)
                  Get(sales_queue_temp,x#)
      
                  glo:select1  = saltmp:ref_number
                  Retail_Picking_Note('ALL')
                  glo:select1  = ''
      
                  access:retsales.clearkey(ret:ref_number_key)
                  ret:ref_number = saltmp:ref_number
                  if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
      
                      get(retdesno,0)
                      if access:retdesno.primerecord() = Level:Benign
                          RDN:Consignment_Number  = ''
                          RDN:Courier             = ret:courier
                          rdn:sale_number         = ret:ref_number
                          access:retdesno.insert()
      
                          save_res_id = access:retstock.savefile()
                          access:retstock.clearkey(res:part_number_key)
                          res:ref_number  = ret:ref_number
                          set(res:part_number_key,res:part_number_key)
                          loop
                              if access:retstock.next()
                                 break
                              end !if
                              if res:ref_number  <> ret:ref_number      |
                                  then break.  ! end if
                              If res:pending_ref_number <> ''
                                  res:despatched  = 'PEN'
                              Else!If res:pending_ref_number <> ''
                                  res:despatched = 'YES'
                              End!If res:pending_ref_number <> ''
                              access:retstock.update
                          end !loop
                          access:retstock.restorefile(save_res_id)
      
                          ret:despatched = 'PRO'
                          ret:despatch_number = rdn:despatch_number
                          access:retsales.update()
      
                          glo:select1  = rdn:despatch_number
                          Retail_Despatch_Note
                          glo:select1  = ''
                          thiswindow.reset(1)
                      End!if access:retdesno.primerecord() = Level:Benign
                  end!if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
      
                  access:retsales.clearkey(ret:ref_number_key)
                  ret:ref_number = saltmp:ref_number
                  if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
                      Include('ret_inv.inc')
                  end
      
              
              End!Loop x# = 1 To Records(sales_queue_Temp)
          Of 2 ! &No Button
      End!Case MessageEx
      
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?All_Parts, Accepted)
    OF ?Trade_Account
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Trade_Account, Accepted)
      Case MessageEx('Are you sure you want to create Sales for ALL the Unallocated parts <13,10> for this Account?','ServiceBase 2000','Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
          Of 1 ! &Yes Button
              count# = 0
              get(retsales,0)
              if access:retsales.primerecord() = Level:Benign
              
                  access:subtracc.clearkey(sub:account_number_key)
                  sub:account_number = account_number_temp
                  if access:subtracc.tryfetch(sub:account_number_key)
                      error# = 1
                  Else!if access:subtracc.tryfetch(sub:account_number_key)
      
                      access:tradeacc.clearkey(tra:account_number_key)
                      tra:account_number = sub:main_account_number
                      if access:tradeacc.fetch(tra:account_number_key)
                          error# = 1
                      Else!if access:tradeacc.fetch(tra:account_number_key)
      
                          if tra:invoice_sub_accounts = 'YES'
                              access:vatcode.clearkey(vat:vat_code_key)
                              vat:vat_code = sub:retail_vat_code
                              if access:vatcode.fetch(vat:vat_code_key) = level:benign
                                  vat_rate_temp = vat:vat_rate
                              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                              access:vatcode.clearkey(vat:vat_code_key)
                              vat:vat_code = sub:retail_vat_code
                              if access:vatcode.fetch(vat:vat_code_key) = level:benign
                                  vat_rate_temp = vat:vat_rate
                              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                          else!if tra:use_sub_accounts = 'YES'
                              access:vatcode.clearkey(vat:vat_code_key)
                              vat:vat_code = tra:retail_vat_code
                              if access:vatcode.fetch(vat:vat_code_key) = level:benign
                                  vat_rate_temp = vat:vat_rate
                              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                              access:vatcode.clearkey(vat:vat_code_key)
                              vat:vat_code = tra:retail_vat_code
                              if access:vatcode.fetch(vat:vat_code_key) = level:benign
                                  vat_rate_temp = vat:vat_rate
                              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                          end!if tra:use_sub_accounts = 'YES'
      
                          If tra:use_sub_accounts <> 'YES'
                              ret:courier_cost    = TRA:Courier_Cost
                              ret:courier         = tra:courier_outgoing
                              use_invoice# = 1
                              If tra:use_delivery_address = 'YES'
                                  use_delivery# = 1
                              End!If tra:use_delivery_address = 'YES'
                              If TRA:Retail_Payment_Type = 'ACC'
                                  ret:payment_method  = 'TRA'
                              End!If SUB:Retail_Payment_Type = 'ACC'
                              IF TRA:retail_payment_type  = 'CAS'
                                  ret:payment_method = 'CAS'
                              End!IF sub:retail_payment_type  = 'CAS'
      
                          Else!If tra:use_sub_accounts <> 'YES'
                              ret:courier_cost    = sub:courier_cost
                              ret:courier         = sub:courier_outgoing
                              If tra:invoice_sub_accounts = 'YES'
                                  use_invoice# = 2
                                  If sub:use_delivery_address = 'YES'
                                      use_delivery# = 2
                                  End!If sub:use_delivery_address = 'YES'
                                  If SUB:Retail_Payment_Type = 'ACC'
                                      ret:payment_method  = 'TRA'
                                  End!If SUB:Retail_Payment_Type = 'ACC'
                                  IF sub:retail_payment_type  = 'CAS'
                                      ret:payment_method = 'CAS'
                                  End!IF sub:retail_payment_type  = 'CAS'
                              Else!If tra:invoice_sub_accounts = 'YES'
                                  use_invoice# = 1
                                  If TRA:Retail_Payment_Type = 'ACC'
                                      ret:payment_method  = 'TRA'
                                  End!If SUB:Retail_Payment_Type = 'ACC'
                                  IF TRA:retail_payment_type  = 'CAS'
                                      ret:payment_method = 'CAS'
                                  End!IF sub:retail_payment_type  = 'CAS'
                              End!If tra:invoice_sub_accounts = 'YES'
      
                          End!If tra:use_sub_accounts <> 'YES'
                          Case use_invoice#
                              OF 1
                                  RET:Postcode        = tra:postcode
                                  RET:Company_Name    = tra:company_name
                                  RET:Building_Name   = ''
                                  RET:Address_Line1   = tra:address_line1
                                  RET:Address_Line2   = tra:address_line2
                                  RET:Address_Line3   = tra:address_line3
                                  RET:Telephone_Number= tra:telephone_number
                                  RET:Fax_Number      = tra:fax_number
                              Of 2
                                  RET:Postcode        = sub:postcode
                                  RET:Company_Name    = sub:company_name
                                  RET:Building_Name   = ''
                                  RET:Address_Line1   = sub:address_line1
                                  RET:Address_Line2   = sub:address_line2
                                  RET:Address_Line3   = sub:address_line3
                                  RET:Telephone_Number= sub:telephone_number
                                  RET:Fax_Number      = sub:fax_number
                          End!If use_invoice# = 1
                          Case use_delivery#
                              Of 1
                                  RET:Postcode_Delivery       = tra:postcode
                                  RET:Company_Name_Delivery   = tra:company_name
                                  RET:Building_Name_Delivery  = ''
                                  RET:Address_Line1_Delivery  = tra:address_line1
                                  RET:Address_Line2_Delivery  = tra:address_line2
                                  RET:Address_Line3_Delivery  = tra:address_line3
                                  RET:Telephone_Delivery      = tra:telephone_number
                                  RET:Fax_Number_Delivery     = tra:fax_number
                              Of 2
                                  RET:Postcode_Delivery       = sub:postcode
                                  RET:Company_Name_Delivery   = sub:company_name
                                  RET:Building_Name_Delivery  = ''
                                  RET:Address_Line1_Delivery  = sub:address_line1
                                  RET:Address_Line2_Delivery  = sub:address_line2
                                  RET:Address_Line3_Delivery  = sub:address_line3
                                  RET:Telephone_Delivery      = sub:telephone_number
                                  RET:Fax_Number_Delivery     = sub:fax_number
                          End!Case use_delivery#
                          ret:account_number  = account_number_temp
                          ret:purchase_order_number   = 'VARIOUS'
                          if access:retsales.insert()
                              error# = 1
                          Else!if access:retsales.insert()
                              count# += 1
                              save_orp_id = access:ordparts.savefile()
                              access:ordparts.clearkey(orp:allocated_account_key)
                              orp:part_type         = 'RET'
                              orp:all_received      = 'YES'
                              orp:allocated_to_sale = 'NO'
                              orp:account_number    = account_number_temp
                              set(orp:allocated_account_key,orp:allocated_account_key)
                              loop
                                  if access:ordparts.next()
                                     break
                                  end !if
                                  if orp:part_type         <> 'RET'      |
                                  or orp:all_received      <> 'YES'      |
                                  or orp:allocated_to_sale <> 'NO'      |
                                  or orp:account_number    <> account_number_temp      |
                                      then break.  ! end if
      
                                  access:retstock_alias.clearkey(ret_ali:order_part_key)
                                  ret_ali:ref_number        = orp:job_number
                                  ret_ali:order_part_number = orp:record_number
                                  if access:retstock_alias.tryfetch(ret_ali:order_part_key) = Level:Benign
                                      get(retstock,0)
                                      if access:retstock.primerecord() = Level:Benign
                                          record_number$      = res:record_number
                                          res:record  :=: ret_ali:record
                                          res:record_number   = record_number$
                                          res:ref_number           = ret:ref_number
                                          res:quantity             = orp:quantity
                                          res:despatched           = 'YES'
                                          res:order_number         = orp:order_number
                                          res:date_ordered         = Today()
                                          res:date_received        = orp:date_received
                                          res:despatch_note_number = ''
                                          res:despatch_date        = ''
                                          res:part_ref_number      = orp:part_ref_number
                                          res:pending_ref_number   = ''
                                          res:order_part_number    = orp:record_number
                                          res:purchase_order_number = ret_ali:purchase_order_number
                                          res:previous_sale_number    = ret_ali:ref_number
                                          if access:retstock.insert()
                                             access:retstock.cancelautoinc()
                                          end
                                      End!if access:retstock.primerecord() = Level:Benign
                                  end!if access:retstock_alias.tryfetch(ret_ali:order_part_key) = Level:Benign
      
                                  pos = Position(orp:allocated_account_key)
                                  orp:allocated_to_sale   = 'YES'
                                  orp:job_number          = ret:ref_number
                                  access:ordparts.update()
                                  Reset(orp:allocated_account_key,pos)
      
                              end !loop
                              access:ordparts.restorefile(save_orp_id)
      
                              saltmp:ref_number   = ret:ref_number
                              Add(sales_queue_temp)
      
                          End!if access:retsales.insert()
                      End!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                  end!if access:subtracc.tryfetch(sub:account_number_key) = Level:Benign
              end!if access:retsales.primerecord() = Level:Benign
              If error# = 1
                  access:retsales.cancelautoinc()
              End!If error# = 1
      
              Case MessageEx(Clip(count#) & ' New Sales have been created.<13,10><13,10>Picking Notes, Despatch Notes and Invoices will now be produced.<13,10><13,10>(This message will close automatically)','ServiceBase 2000','Styles\idea.ico','|&Close',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,500)
                  Of 1 ! &Close Button
              End!Case MessageEx
      
              Loop x# = 1 To Records(sales_queue_Temp)
                  Get(sales_queue_temp,x#)
      
                  glo:select1  = saltmp:ref_number
                  Retail_Picking_Note('ALL')
                  glo:select1  = ''
      
                  access:retsales.clearkey(ret:ref_number_key)
                  ret:ref_number = saltmp:ref_number
                  if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
      
                      get(retdesno,0)
                      if access:retdesno.primerecord() = Level:Benign
                          RDN:Consignment_Number  = ''
                          RDN:Courier             = ret:courier
                          rdn:sale_number         = ret:ref_number
                          access:retdesno.insert()
      
                          save_res_id = access:retstock.savefile()
                          access:retstock.clearkey(res:part_number_key)
                          res:ref_number  = ret:ref_number
                          set(res:part_number_key,res:part_number_key)
                          loop
                              if access:retstock.next()
                                 break
                              end !if
                              if res:ref_number  <> ret:ref_number      |
                                  then break.  ! end if
                              If res:pending_ref_number <> ''
                                  res:despatched  = 'PEN'
                              Else!If res:pending_ref_number <> ''
                                  res:despatched = 'YES'
                              End!If res:pending_ref_number <> ''
                              access:retstock.update
                          end !loop
                          access:retstock.restorefile(save_res_id)
      
                          ret:despatched = 'PRO'
                          ret:despatch_number = rdn:despatch_number
                          access:retsales.update()
      
                          glo:select1  = rdn:despatch_number
                          Retail_Despatch_Note
                          glo:select1  = ''
                          thiswindow.reset(1)
                      End!if access:retdesno.primerecord() = Level:Benign
                  end!if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
      
                  access:retsales.clearkey(ret:ref_number_key)
                  ret:ref_number = saltmp:ref_number
                  if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
                      Include('ret_inv.inc')
                  end
      
              
              End!Loop x# = 1 To Records(sales_queue_Temp)
          Of 2 ! &No Button
      End!Case MessageEx
      
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Trade_Account, Accepted)
    OF ?Account_Number_temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      !Free(BRW1.ListQueue)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Undelivered_Parts')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = ret_temp
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = yes_temp
     end
     GET(SELF.Order.RangeList.List,3)
     if not error()
         Self.Order.RangeList.List.Right = no_temp
     end
     GET(SELF.Order.RangeList.List,4)
     if not error()
         Self.Order.RangeList.List.Right = Account_Number_temp
     end
  ELSE
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = ret_temp
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = yes_temp
     end
     GET(SELF.Order.RangeList.List,3)
     if not error()
         Self.Order.RangeList.List.Right = no_temp
     end
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

