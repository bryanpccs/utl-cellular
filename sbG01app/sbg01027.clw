

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01027.INC'),ONCE        !Local module procedure declarations
                     END


UpdateRETACCOUNTSLIST PROCEDURE                       !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::retacc:Record LIKE(retacc:RECORD),STATIC
QuickWindow          WINDOW('Update Order'),AT(,,220,98),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('UpdateRETACCOUNTSLIST'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,64),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Account Number'),AT(8,20),USE(?RETACC:AccountNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,20,124,10),USE(retacc:AccountNumber),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Account Number'),TIP('Account Number'),UPR,READONLY
                           PROMPT('Quantity Ordered'),AT(8,36),USE(?RETACC:QuantityOrdered:Prompt),TRN
                           ENTRY(@s8),AT(84,36,64,10),USE(retacc:QuantityOrdered),SKIP,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Quantity Ordered'),TIP('Quantity Ordered'),UPR,READONLY
                           PROMPT('Quantity To Ship'),AT(8,52),USE(?RETACC:QuantityToShip:Prompt),TRN
                           ENTRY(@s8),AT(84,52,64,10),USE(retacc:QuantityToShip),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Quantity To Ship'),TIP('Quantity To Ship'),UPR
                         END
                       END
                       PANEL,AT(4,72,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,76,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(156,76,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?RETACC:AccountNumber:Prompt{prop:FontColor} = -1
    ?RETACC:AccountNumber:Prompt{prop:Color} = 15066597
    If ?retacc:AccountNumber{prop:ReadOnly} = True
        ?retacc:AccountNumber{prop:FontColor} = 65793
        ?retacc:AccountNumber{prop:Color} = 15066597
    Elsif ?retacc:AccountNumber{prop:Req} = True
        ?retacc:AccountNumber{prop:FontColor} = 65793
        ?retacc:AccountNumber{prop:Color} = 8454143
    Else ! If ?retacc:AccountNumber{prop:Req} = True
        ?retacc:AccountNumber{prop:FontColor} = 65793
        ?retacc:AccountNumber{prop:Color} = 16777215
    End ! If ?retacc:AccountNumber{prop:Req} = True
    ?retacc:AccountNumber{prop:Trn} = 0
    ?retacc:AccountNumber{prop:FontStyle} = font:Bold
    ?RETACC:QuantityOrdered:Prompt{prop:FontColor} = -1
    ?RETACC:QuantityOrdered:Prompt{prop:Color} = 15066597
    If ?retacc:QuantityOrdered{prop:ReadOnly} = True
        ?retacc:QuantityOrdered{prop:FontColor} = 65793
        ?retacc:QuantityOrdered{prop:Color} = 15066597
    Elsif ?retacc:QuantityOrdered{prop:Req} = True
        ?retacc:QuantityOrdered{prop:FontColor} = 65793
        ?retacc:QuantityOrdered{prop:Color} = 8454143
    Else ! If ?retacc:QuantityOrdered{prop:Req} = True
        ?retacc:QuantityOrdered{prop:FontColor} = 65793
        ?retacc:QuantityOrdered{prop:Color} = 16777215
    End ! If ?retacc:QuantityOrdered{prop:Req} = True
    ?retacc:QuantityOrdered{prop:Trn} = 0
    ?retacc:QuantityOrdered{prop:FontStyle} = font:Bold
    ?RETACC:QuantityToShip:Prompt{prop:FontColor} = -1
    ?RETACC:QuantityToShip:Prompt{prop:Color} = 15066597
    If ?retacc:QuantityToShip{prop:ReadOnly} = True
        ?retacc:QuantityToShip{prop:FontColor} = 65793
        ?retacc:QuantityToShip{prop:Color} = 15066597
    Elsif ?retacc:QuantityToShip{prop:Req} = True
        ?retacc:QuantityToShip{prop:FontColor} = 65793
        ?retacc:QuantityToShip{prop:Color} = 8454143
    Else ! If ?retacc:QuantityToShip{prop:Req} = True
        ?retacc:QuantityToShip{prop:FontColor} = 65793
        ?retacc:QuantityToShip{prop:Color} = 16777215
    End ! If ?retacc:QuantityToShip{prop:Req} = True
    ?retacc:QuantityToShip{prop:Trn} = 0
    ?retacc:QuantityToShip{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateRETACCOUNTSLIST',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateRETACCOUNTSLIST',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateRETACCOUNTSLIST',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RETACC:AccountNumber:Prompt;  SolaceCtrlName = '?RETACC:AccountNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?retacc:AccountNumber;  SolaceCtrlName = '?retacc:AccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RETACC:QuantityOrdered:Prompt;  SolaceCtrlName = '?RETACC:QuantityOrdered:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?retacc:QuantityOrdered;  SolaceCtrlName = '?retacc:QuantityOrdered';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RETACC:QuantityToShip:Prompt;  SolaceCtrlName = '?RETACC:QuantityToShip:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?retacc:QuantityToShip;  SolaceCtrlName = '?retacc:QuantityToShip';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateRETACCOUNTSLIST')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateRETACCOUNTSLIST')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?RETACC:AccountNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(retacc:Record,History::retacc:Record)
  SELF.AddHistoryField(?retacc:AccountNumber,3)
  SELF.AddHistoryField(?retacc:QuantityOrdered,6)
  SELF.AddHistoryField(?retacc:QuantityToShip,7)
  SELF.AddUpdateFile(Access:RETACCOUNTSLIST)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:RETACCOUNTSLIST.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:RETACCOUNTSLIST
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETACCOUNTSLIST.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateRETACCOUNTSLIST',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateRETACCOUNTSLIST')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

