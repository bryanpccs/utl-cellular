

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01025.INC'),ONCE        !Local module procedure declarations
                     END


DailySparesAnalysisCriteria PROCEDURE                 !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:SiteLocation     STRING(30)
tmp:Supplier         STRING(30)
savepath             STRING(255)
save_ret_id          USHORT,AUTO
tmp:SalesTotal       REAL
tmp:BackOrderTotal   REAL
tmp:OldestDate       DATE
tmp:StockValue       REAL
tmp:OldestOrder      DATE
save_res_id          USHORT,AUTO
save_sto_id          USHORT,AUTO
save_ord_id          USHORT,AUTO
window               WINDOW('Daily Spares Analysis Criteria'),AT(,,228,115),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,220,80),USE(?Sheet1),SPREAD
                         TAB('Daily Spares Analysis Criteria'),USE(?Tab1)
                           PROMPT('Start Date'),AT(8,20),USE(?tmp:StartDate:Prompt)
                           ENTRY(@d6),AT(84,20,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(152,20,10,10),USE(?LookupStartDate),SKIP,FONT('Tahoma',8,,,CHARSET:ANSI),ICON('Calenda2.ico')
                           PROMPT('End Date'),AT(8,36),USE(?tmp:EndDate:Prompt)
                           ENTRY(@d6),AT(84,36,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(152,36,10,10),USE(?LookupEndDate),SKIP,ICON('Calenda2.ico')
                           PROMPT('Site Location'),AT(8,52),USE(?tmp:SiteLocation:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,52,124,10),USE(tmp:SiteLocation),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Site Location'),TIP('Site Location'),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(212,52,10,10),USE(?LookupLocation),SKIP,ICON('List3.ico')
                           PROMPT('Supplier'),AT(8,68),USE(?tmp:Supplier:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,68,124,10),USE(tmp:Supplier),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Supplier'),TIP('Supplier'),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),UPR
                           BUTTON,AT(212,68,10,10),USE(?LookupSupplier),SKIP,ICON('List3.ico')
                         END
                       END
                       PANEL,AT(4,88,220,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Cancel'),AT(164,92,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                       BUTTON('&OK'),AT(108,92,56,16),USE(?Button6),LEFT,ICON('ok.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
look:tmp:SiteLocation                Like(tmp:SiteLocation)
look:tmp:Supplier                Like(tmp:Supplier)
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?tmp:StartDate:Prompt{prop:FontColor} = -1
    ?tmp:StartDate:Prompt{prop:Color} = 15066597
    If ?tmp:StartDate{prop:ReadOnly} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 15066597
    Elsif ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 8454143
    Else ! If ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 16777215
    End ! If ?tmp:StartDate{prop:Req} = True
    ?tmp:StartDate{prop:Trn} = 0
    ?tmp:StartDate{prop:FontStyle} = font:Bold
    ?tmp:EndDate:Prompt{prop:FontColor} = -1
    ?tmp:EndDate:Prompt{prop:Color} = 15066597
    If ?tmp:EndDate{prop:ReadOnly} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 15066597
    Elsif ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 8454143
    Else ! If ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 16777215
    End ! If ?tmp:EndDate{prop:Req} = True
    ?tmp:EndDate{prop:Trn} = 0
    ?tmp:EndDate{prop:FontStyle} = font:Bold
    ?tmp:SiteLocation:Prompt{prop:FontColor} = -1
    ?tmp:SiteLocation:Prompt{prop:Color} = 15066597
    If ?tmp:SiteLocation{prop:ReadOnly} = True
        ?tmp:SiteLocation{prop:FontColor} = 65793
        ?tmp:SiteLocation{prop:Color} = 15066597
    Elsif ?tmp:SiteLocation{prop:Req} = True
        ?tmp:SiteLocation{prop:FontColor} = 65793
        ?tmp:SiteLocation{prop:Color} = 8454143
    Else ! If ?tmp:SiteLocation{prop:Req} = True
        ?tmp:SiteLocation{prop:FontColor} = 65793
        ?tmp:SiteLocation{prop:Color} = 16777215
    End ! If ?tmp:SiteLocation{prop:Req} = True
    ?tmp:SiteLocation{prop:Trn} = 0
    ?tmp:SiteLocation{prop:FontStyle} = font:Bold
    ?tmp:Supplier:Prompt{prop:FontColor} = -1
    ?tmp:Supplier:Prompt{prop:Color} = 15066597
    If ?tmp:Supplier{prop:ReadOnly} = True
        ?tmp:Supplier{prop:FontColor} = 65793
        ?tmp:Supplier{prop:Color} = 15066597
    Elsif ?tmp:Supplier{prop:Req} = True
        ?tmp:Supplier{prop:FontColor} = 65793
        ?tmp:Supplier{prop:Color} = 8454143
    Else ! If ?tmp:Supplier{prop:Req} = True
        ?tmp:Supplier{prop:FontColor} = 65793
        ?tmp:Supplier{prop:Color} = 16777215
    End ! If ?tmp:Supplier{prop:Req} = True
    ?tmp:Supplier{prop:Trn} = 0
    ?tmp:Supplier{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'DailySparesAnalysisCriteria',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:StartDate',tmp:StartDate,'DailySparesAnalysisCriteria',1)
    SolaceViewVars('tmp:EndDate',tmp:EndDate,'DailySparesAnalysisCriteria',1)
    SolaceViewVars('tmp:SiteLocation',tmp:SiteLocation,'DailySparesAnalysisCriteria',1)
    SolaceViewVars('tmp:Supplier',tmp:Supplier,'DailySparesAnalysisCriteria',1)
    SolaceViewVars('savepath',savepath,'DailySparesAnalysisCriteria',1)
    SolaceViewVars('save_ret_id',save_ret_id,'DailySparesAnalysisCriteria',1)
    SolaceViewVars('tmp:SalesTotal',tmp:SalesTotal,'DailySparesAnalysisCriteria',1)
    SolaceViewVars('tmp:BackOrderTotal',tmp:BackOrderTotal,'DailySparesAnalysisCriteria',1)
    SolaceViewVars('tmp:OldestDate',tmp:OldestDate,'DailySparesAnalysisCriteria',1)
    SolaceViewVars('tmp:StockValue',tmp:StockValue,'DailySparesAnalysisCriteria',1)
    SolaceViewVars('tmp:OldestOrder',tmp:OldestOrder,'DailySparesAnalysisCriteria',1)
    SolaceViewVars('save_res_id',save_res_id,'DailySparesAnalysisCriteria',1)
    SolaceViewVars('save_sto_id',save_sto_id,'DailySparesAnalysisCriteria',1)
    SolaceViewVars('save_ord_id',save_ord_id,'DailySparesAnalysisCriteria',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate:Prompt;  SolaceCtrlName = '?tmp:StartDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate;  SolaceCtrlName = '?tmp:StartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupStartDate;  SolaceCtrlName = '?LookupStartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate:Prompt;  SolaceCtrlName = '?tmp:EndDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate;  SolaceCtrlName = '?tmp:EndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupEndDate;  SolaceCtrlName = '?LookupEndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SiteLocation:Prompt;  SolaceCtrlName = '?tmp:SiteLocation:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SiteLocation;  SolaceCtrlName = '?tmp:SiteLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupLocation;  SolaceCtrlName = '?LookupLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Supplier:Prompt;  SolaceCtrlName = '?tmp:Supplier:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Supplier;  SolaceCtrlName = '?tmp:Supplier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupSupplier;  SolaceCtrlName = '?LookupSupplier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button6;  SolaceCtrlName = '?Button6';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('DailySparesAnalysisCriteria')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'DailySparesAnalysisCriteria')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:StartDate:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOCATION.Open
  Relate:RETSALES.Open
  Access:SUPPLIER.UseFile
  Access:RETSTOCK.UseFile
  Access:STOCK.UseFile
  Access:ORDERS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  IF ?tmp:SiteLocation{Prop:Tip} AND ~?LookupLocation{Prop:Tip}
     ?LookupLocation{Prop:Tip} = 'Select ' & ?tmp:SiteLocation{Prop:Tip}
  END
  IF ?tmp:SiteLocation{Prop:Msg} AND ~?LookupLocation{Prop:Msg}
     ?LookupLocation{Prop:Msg} = 'Select ' & ?tmp:SiteLocation{Prop:Msg}
  END
  IF ?tmp:Supplier{Prop:Tip} AND ~?LookupSupplier{Prop:Tip}
     ?LookupSupplier{Prop:Tip} = 'Select ' & ?tmp:Supplier{Prop:Tip}
  END
  IF ?tmp:Supplier{Prop:Msg} AND ~?LookupSupplier{Prop:Msg}
     ?LookupSupplier{Prop:Msg} = 'Select ' & ?tmp:Supplier{Prop:Msg}
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
    Relate:RETSALES.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'DailySparesAnalysisCriteria',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Locations
      PickSuppliers
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tmp:SiteLocation
      IF tmp:SiteLocation OR ?tmp:SiteLocation{Prop:Req}
        loc:Location = tmp:SiteLocation
        !Save Lookup Field Incase Of error
        look:tmp:SiteLocation        = tmp:SiteLocation
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:SiteLocation = loc:Location
          ELSE
            !Restore Lookup On Error
            tmp:SiteLocation = look:tmp:SiteLocation
            SELECT(?tmp:SiteLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupLocation
      ThisWindow.Update
      loc:Location = tmp:SiteLocation
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:SiteLocation = loc:Location
          Select(?+1)
      ELSE
          Select(?tmp:SiteLocation)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:SiteLocation)
    OF ?tmp:Supplier
      IF tmp:Supplier OR ?tmp:Supplier{Prop:Req}
        sup:Company_Name = tmp:Supplier
        !Save Lookup Field Incase Of error
        look:tmp:Supplier        = tmp:Supplier
        IF Access:SUPPLIER.TryFetch(sup:Company_Name_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:Supplier = sup:Company_Name
          ELSE
            !Restore Lookup On Error
            tmp:Supplier = look:tmp:Supplier
            SELECT(?tmp:Supplier)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupSupplier
      ThisWindow.Update
      sup:Company_Name = tmp:Supplier
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:Supplier = sup:Company_Name
          Select(?+1)
      ELSE
          Select(?tmp:Supplier)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Supplier)
    OF ?Button6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
      savepath = path()
      glo:file_name = 'EXPJOBS.CSV'
      If not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
                  file:keepdir + file:noerror + file:longname)
          !Failed
          setpath(savepath)
      else!If not filedialog
          !Found
          setpath(savepath)
      
          Loop x# = tmp:StartDate To tmp:EndDate
              Clear(gen:Record)
              gen:Line1   = Format(x#,@d6)
              Access:EXPGEN.Insert()
      
      !Find the invoiced sales
              Save_ret_ID = Access:RETSALES.SaveFile()
              Access:RETSALES.ClearKey(ret:Date_Booked_Key)
              ret:date_booked = x#
              Set(ret:Date_Booked_Key,ret:Date_Booked_Key)
              Loop
                  If Access:RETSALES.NEXT()
                     Break
                  End !If
                  If ret:date_booked <> x#      |
                      Then Break.  ! End If
      
                  If ret:Invoice_Number = ''
                      Cycle
                  End !If ret:Invoice_Number = ''
      
                  tmp:SalesTotal      = 0
                  tmp:BackOrderTotal  = 0
                  tmp:OldestDate      = 0
      
                  Save_res_ID = Access:RETSTOCK.SaveFile()
                  Access:RETSTOCK.ClearKey(res:Part_Number_Key)
                  res:Ref_Number  = ret:Ref_Number
                  Set(res:Part_Number_Key,res:Part_Number_Key)
                  Loop
                      If Access:RETSTOCK.NEXT()
                         Break
                      End !If
                      If res:Ref_Number  <> ret:Ref_Number      |
                          Then Break.  ! End If
                      If res:Despatched = 'YES'
                          !Non-BackOrder
                          tmp:SalesTotal += res:Item_Cost * res:Quantity
                      Else !If res:Despathced = 'YES'
                          tmp:BackOrderTotal += res:Item_cost * res:Quantity
                          If res:Date_Ordered <> ''
                              If tmp:OldestDate = 0
                                  tmp:OldestDate  = res:Date_Ordered
                              End !If tmp:OldestDate = 0
                              If res:Date_Ordered < tmp:OldestDate
                                  tmp:OldestDate  = res:Date_Ordered
                              End !If res:Date_Ordered < tmp:OldestDate
                          End !If res:Date_Ordered <> ''
                      End !If res:Despathced = 'YES'
                  End !Loop
                  Access:RETSTOCK.RestoreFile(Save_res_ID)
      
                  gen:Line1   = Clip(gen:Line1) & ',' & tmp:SalesTotal
                  gen:Line1   = Clip(gen:Line1) & ',' & tmp:BackOrderTotal
                  gen:Line1   = Clip(gen:Line1) & ',' & tmp:SalesTotal + tmp:BackOrderTotal
      
                  tmp:StockValue = 0
      
                  Save_sto_ID = Access:STOCK.SaveFile()
                  If tmp:SiteLocation <> ''
                      Access:STOCK.ClearKey(sto:Location_Key)
                      sto:Location    = tmp:SiteLocation
                      Set(sto:Location_Key,sto:Location_Key)
                  Else !If tmp:Location <> ''
                      Set(sto:Ref_Number_Key)
                  End !If tmp:Location <> ''
                  Loop
                      If Access:STOCK.NEXT()
                         Break
                      End !If
                      If tmp:SiteLocation <> ''
                          If sto:Location    <> tmp:SiteLocation      |
                              Then Break.  ! End If
                      End !If tmp:SiteLocation <> ''
                      tmp:StockValue += sto:Retail_Cost * sto:Quantity_stock
                  End !Loop
                  Access:STOCK.RestoreFile(Save_sto_ID)
      
                  gen:Line1   = Clip(gen:Line1) & ',' & tmp:StockValue
                  gen:Line1   = Clip(gen:Line1) & ',' & Format(tmp:OldestDate,@d6)
      
      ! Oldest Order From Supplier?!?!
                  tmp:OldestOrder = 0
                  Save_ord_ID = Access:ORDERS.SaveFile()
      
                  If tmp:Supplier <> ''
      
                      Access:ORDERS.ClearKey(ord:Supplier_Key)
                      ord:Supplier     = tmp:Supplier
                      Set(ord:Supplier_Key,ord:Supplier_Key)
                  Else!If tmp:Supplier <> ''
                      Set(ord:Order_Number_Key)
                  End!If tmp:Supplier <> ''
                  Loop
                      If Access:ORDERS.NEXT()
                         Break
                      End !If
                      If tmp:Supplier <> ''
                          If ord:Supplier     <> tmp:Supplier      |
                              Then Break.  ! End If
                      End !If tmp:Supplier <> ''
                      If ord:All_Received <> 'YES'
                          tmp:OldestOrder = ord:Date
                          Break
                      End !If ord:All_Received <> 'YES'
                  End !Loop
                  Access:ORDERS.RestoreFile(Save_ord_ID)
      
                  gen:Line1   = Clip(gen:Line1) & ',' & tmp:OldestOrder
                  gen:Line1   = Clip(gen:Line1) & ',,'
                  Access:EXPGEN.Insert()
      
              End !Loop
              Access:RETSALES.RestoreFile(Save_ret_ID)
          End !Loop x# = tmp:StartDate To tmp:EndDate
      
      End!If not filedialog
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'DailySparesAnalysisCriteria')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?tmp:SiteLocation
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SiteLocation, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Location')
             Post(Event:Accepted,?LookupLocation)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupLocation)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SiteLocation, AlertKey)
    END
  OF ?tmp:Supplier
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Supplier, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Supplier')
             Post(Event:Accepted,?LookupSupplier)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupSupplier)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Supplier, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

