

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01031.INC'),ONCE        !Local module procedure declarations
                     END


UpdateOrdItems PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::ori:Record  LIKE(ori:RECORD),STATIC
QuickWindow          WINDOW('Update the orditems File'),AT(,,332,154),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateOrdItems'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,324,128),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?ori:recordnumber:Prompt),TRN
                           ENTRY(@s8),AT(80,20,40,10),USE(ori:recordnumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Record Number'),TIP('Record Number'),UPR
                           PROMPT('&ordhno:'),AT(8,34),USE(?ori:ordhno:Prompt)
                           ENTRY(@n12),AT(80,34,52,10),USE(ori:ordhno),RIGHT(1)
                           PROMPT('manufact:'),AT(8,48),USE(?ori:manufact:Prompt)
                           ENTRY(@s40),AT(80,48,164,10),USE(ori:manufact)
                           PROMPT('&partno:'),AT(8,62),USE(?ori:partno:Prompt)
                           ENTRY(@s20),AT(80,62,84,10),USE(ori:partno)
                           PROMPT('&partdiscription:'),AT(8,76),USE(?ori:partdiscription:Prompt)
                           ENTRY(@s60),AT(80,76,244,10),USE(ori:partdiscription)
                           PROMPT('&qty:'),AT(8,90),USE(?ori:qty:Prompt)
                           ENTRY(@n10),AT(80,90,44,10),USE(ori:qty),RIGHT(1)
                           PROMPT('&itemcost:'),AT(8,104),USE(?ori:itemcost:Prompt)
                           ENTRY(@n10.2),AT(80,104,44,10),USE(ori:itemcost),DECIMAL(14)
                           PROMPT('&totalcost:'),AT(8,118),USE(?ori:totalcost:Prompt)
                           ENTRY(@n10.2),AT(80,118,44,10),USE(ori:totalcost),DECIMAL(14)
                         END
                       END
                       BUTTON('OK'),AT(234,136,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(283,136,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(283,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?ori:recordnumber:Prompt{prop:FontColor} = -1
    ?ori:recordnumber:Prompt{prop:Color} = 15066597
    If ?ori:recordnumber{prop:ReadOnly} = True
        ?ori:recordnumber{prop:FontColor} = 65793
        ?ori:recordnumber{prop:Color} = 15066597
    Elsif ?ori:recordnumber{prop:Req} = True
        ?ori:recordnumber{prop:FontColor} = 65793
        ?ori:recordnumber{prop:Color} = 8454143
    Else ! If ?ori:recordnumber{prop:Req} = True
        ?ori:recordnumber{prop:FontColor} = 65793
        ?ori:recordnumber{prop:Color} = 16777215
    End ! If ?ori:recordnumber{prop:Req} = True
    ?ori:recordnumber{prop:Trn} = 0
    ?ori:recordnumber{prop:FontStyle} = font:Bold
    ?ori:ordhno:Prompt{prop:FontColor} = -1
    ?ori:ordhno:Prompt{prop:Color} = 15066597
    If ?ori:ordhno{prop:ReadOnly} = True
        ?ori:ordhno{prop:FontColor} = 65793
        ?ori:ordhno{prop:Color} = 15066597
    Elsif ?ori:ordhno{prop:Req} = True
        ?ori:ordhno{prop:FontColor} = 65793
        ?ori:ordhno{prop:Color} = 8454143
    Else ! If ?ori:ordhno{prop:Req} = True
        ?ori:ordhno{prop:FontColor} = 65793
        ?ori:ordhno{prop:Color} = 16777215
    End ! If ?ori:ordhno{prop:Req} = True
    ?ori:ordhno{prop:Trn} = 0
    ?ori:ordhno{prop:FontStyle} = font:Bold
    ?ori:manufact:Prompt{prop:FontColor} = -1
    ?ori:manufact:Prompt{prop:Color} = 15066597
    If ?ori:manufact{prop:ReadOnly} = True
        ?ori:manufact{prop:FontColor} = 65793
        ?ori:manufact{prop:Color} = 15066597
    Elsif ?ori:manufact{prop:Req} = True
        ?ori:manufact{prop:FontColor} = 65793
        ?ori:manufact{prop:Color} = 8454143
    Else ! If ?ori:manufact{prop:Req} = True
        ?ori:manufact{prop:FontColor} = 65793
        ?ori:manufact{prop:Color} = 16777215
    End ! If ?ori:manufact{prop:Req} = True
    ?ori:manufact{prop:Trn} = 0
    ?ori:manufact{prop:FontStyle} = font:Bold
    ?ori:partno:Prompt{prop:FontColor} = -1
    ?ori:partno:Prompt{prop:Color} = 15066597
    If ?ori:partno{prop:ReadOnly} = True
        ?ori:partno{prop:FontColor} = 65793
        ?ori:partno{prop:Color} = 15066597
    Elsif ?ori:partno{prop:Req} = True
        ?ori:partno{prop:FontColor} = 65793
        ?ori:partno{prop:Color} = 8454143
    Else ! If ?ori:partno{prop:Req} = True
        ?ori:partno{prop:FontColor} = 65793
        ?ori:partno{prop:Color} = 16777215
    End ! If ?ori:partno{prop:Req} = True
    ?ori:partno{prop:Trn} = 0
    ?ori:partno{prop:FontStyle} = font:Bold
    ?ori:partdiscription:Prompt{prop:FontColor} = -1
    ?ori:partdiscription:Prompt{prop:Color} = 15066597
    If ?ori:partdiscription{prop:ReadOnly} = True
        ?ori:partdiscription{prop:FontColor} = 65793
        ?ori:partdiscription{prop:Color} = 15066597
    Elsif ?ori:partdiscription{prop:Req} = True
        ?ori:partdiscription{prop:FontColor} = 65793
        ?ori:partdiscription{prop:Color} = 8454143
    Else ! If ?ori:partdiscription{prop:Req} = True
        ?ori:partdiscription{prop:FontColor} = 65793
        ?ori:partdiscription{prop:Color} = 16777215
    End ! If ?ori:partdiscription{prop:Req} = True
    ?ori:partdiscription{prop:Trn} = 0
    ?ori:partdiscription{prop:FontStyle} = font:Bold
    ?ori:qty:Prompt{prop:FontColor} = -1
    ?ori:qty:Prompt{prop:Color} = 15066597
    If ?ori:qty{prop:ReadOnly} = True
        ?ori:qty{prop:FontColor} = 65793
        ?ori:qty{prop:Color} = 15066597
    Elsif ?ori:qty{prop:Req} = True
        ?ori:qty{prop:FontColor} = 65793
        ?ori:qty{prop:Color} = 8454143
    Else ! If ?ori:qty{prop:Req} = True
        ?ori:qty{prop:FontColor} = 65793
        ?ori:qty{prop:Color} = 16777215
    End ! If ?ori:qty{prop:Req} = True
    ?ori:qty{prop:Trn} = 0
    ?ori:qty{prop:FontStyle} = font:Bold
    ?ori:itemcost:Prompt{prop:FontColor} = -1
    ?ori:itemcost:Prompt{prop:Color} = 15066597
    If ?ori:itemcost{prop:ReadOnly} = True
        ?ori:itemcost{prop:FontColor} = 65793
        ?ori:itemcost{prop:Color} = 15066597
    Elsif ?ori:itemcost{prop:Req} = True
        ?ori:itemcost{prop:FontColor} = 65793
        ?ori:itemcost{prop:Color} = 8454143
    Else ! If ?ori:itemcost{prop:Req} = True
        ?ori:itemcost{prop:FontColor} = 65793
        ?ori:itemcost{prop:Color} = 16777215
    End ! If ?ori:itemcost{prop:Req} = True
    ?ori:itemcost{prop:Trn} = 0
    ?ori:itemcost{prop:FontStyle} = font:Bold
    ?ori:totalcost:Prompt{prop:FontColor} = -1
    ?ori:totalcost:Prompt{prop:Color} = 15066597
    If ?ori:totalcost{prop:ReadOnly} = True
        ?ori:totalcost{prop:FontColor} = 65793
        ?ori:totalcost{prop:Color} = 15066597
    Elsif ?ori:totalcost{prop:Req} = True
        ?ori:totalcost{prop:FontColor} = 65793
        ?ori:totalcost{prop:Color} = 8454143
    Else ! If ?ori:totalcost{prop:Req} = True
        ?ori:totalcost{prop:FontColor} = 65793
        ?ori:totalcost{prop:Color} = 16777215
    End ! If ?ori:totalcost{prop:Req} = True
    ?ori:totalcost{prop:Trn} = 0
    ?ori:totalcost{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateOrdItems',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateOrdItems',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateOrdItems',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ori:recordnumber:Prompt;  SolaceCtrlName = '?ori:recordnumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ori:recordnumber;  SolaceCtrlName = '?ori:recordnumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ori:ordhno:Prompt;  SolaceCtrlName = '?ori:ordhno:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ori:ordhno;  SolaceCtrlName = '?ori:ordhno';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ori:manufact:Prompt;  SolaceCtrlName = '?ori:manufact:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ori:manufact;  SolaceCtrlName = '?ori:manufact';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ori:partno:Prompt;  SolaceCtrlName = '?ori:partno:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ori:partno;  SolaceCtrlName = '?ori:partno';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ori:partdiscription:Prompt;  SolaceCtrlName = '?ori:partdiscription:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ori:partdiscription;  SolaceCtrlName = '?ori:partdiscription';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ori:qty:Prompt;  SolaceCtrlName = '?ori:qty:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ori:qty;  SolaceCtrlName = '?ori:qty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ori:itemcost:Prompt;  SolaceCtrlName = '?ori:itemcost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ori:itemcost;  SolaceCtrlName = '?ori:itemcost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ori:totalcost:Prompt;  SolaceCtrlName = '?ori:totalcost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ori:totalcost;  SolaceCtrlName = '?ori:totalcost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateOrdItems')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateOrdItems')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ori:recordnumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(ori:Record,History::ori:Record)
  SELF.AddHistoryField(?ori:recordnumber,1)
  SELF.AddHistoryField(?ori:ordhno,2)
  SELF.AddHistoryField(?ori:manufact,3)
  SELF.AddHistoryField(?ori:partno,4)
  SELF.AddHistoryField(?ori:partdiscription,5)
  SELF.AddHistoryField(?ori:qty,6)
  SELF.AddHistoryField(?ori:itemcost,7)
  SELF.AddHistoryField(?ori:totalcost,8)
  SELF.AddUpdateFile(Access:ORDITEMS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ORDITEMS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ORDITEMS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDITEMS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateOrdItems',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateOrdItems')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

