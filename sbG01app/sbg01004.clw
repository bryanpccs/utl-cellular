

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01004.INC'),ONCE        !Local module procedure declarations
                     END


Process_Retail_Stock PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
Quantity_temp        REAL
ActionMessage        CSTRING(40)
quantity_required_temp REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::res:Record  LIKE(res:RECORD),STATIC
QuickWindow          WINDOW('Update the RETSTOCK File'),AT(,,220,132),FONT('Tahoma',8,,),CENTER,IMM,HLP('Process_Retail_Stock'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,96),USE(?Sheet1),SPREAD
                         TAB('Item Details'),USE(?Tab1)
                           PROMPT('Part Number'),AT(8,20),USE(?RES:Part_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(res:Part_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Description'),AT(8,36),USE(?RES:Description:Prompt),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(res:Description),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Quantity In Stock'),AT(8,52),USE(?STO:Quantity_Stock:Prompt),TRN
                           ENTRY(@s8),AT(84,52,64,10),USE(sto:Quantity_Stock),SKIP,RIGHT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Quantity Required'),AT(8,68),USE(?Prompt4)
                           ENTRY(@s8),AT(84,68,64,10),USE(quantity_required_temp),SKIP,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Quantity To Sell'),AT(8,84),USE(?RES:Quantity:Prompt),TRN
                           SPIN(@n8),AT(84,84,64,10),USE(res:Quantity),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,RANGE(1,999999),STEP(1)
                         END
                       END
                       BUTTON('&OK'),AT(100,108,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,108,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,104,212,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?RES:Part_Number:Prompt{prop:FontColor} = -1
    ?RES:Part_Number:Prompt{prop:Color} = 15066597
    If ?res:Part_Number{prop:ReadOnly} = True
        ?res:Part_Number{prop:FontColor} = 65793
        ?res:Part_Number{prop:Color} = 15066597
    Elsif ?res:Part_Number{prop:Req} = True
        ?res:Part_Number{prop:FontColor} = 65793
        ?res:Part_Number{prop:Color} = 8454143
    Else ! If ?res:Part_Number{prop:Req} = True
        ?res:Part_Number{prop:FontColor} = 65793
        ?res:Part_Number{prop:Color} = 16777215
    End ! If ?res:Part_Number{prop:Req} = True
    ?res:Part_Number{prop:Trn} = 0
    ?res:Part_Number{prop:FontStyle} = font:Bold
    ?RES:Description:Prompt{prop:FontColor} = -1
    ?RES:Description:Prompt{prop:Color} = 15066597
    If ?res:Description{prop:ReadOnly} = True
        ?res:Description{prop:FontColor} = 65793
        ?res:Description{prop:Color} = 15066597
    Elsif ?res:Description{prop:Req} = True
        ?res:Description{prop:FontColor} = 65793
        ?res:Description{prop:Color} = 8454143
    Else ! If ?res:Description{prop:Req} = True
        ?res:Description{prop:FontColor} = 65793
        ?res:Description{prop:Color} = 16777215
    End ! If ?res:Description{prop:Req} = True
    ?res:Description{prop:Trn} = 0
    ?res:Description{prop:FontStyle} = font:Bold
    ?STO:Quantity_Stock:Prompt{prop:FontColor} = -1
    ?STO:Quantity_Stock:Prompt{prop:Color} = 15066597
    If ?sto:Quantity_Stock{prop:ReadOnly} = True
        ?sto:Quantity_Stock{prop:FontColor} = 65793
        ?sto:Quantity_Stock{prop:Color} = 15066597
    Elsif ?sto:Quantity_Stock{prop:Req} = True
        ?sto:Quantity_Stock{prop:FontColor} = 65793
        ?sto:Quantity_Stock{prop:Color} = 8454143
    Else ! If ?sto:Quantity_Stock{prop:Req} = True
        ?sto:Quantity_Stock{prop:FontColor} = 65793
        ?sto:Quantity_Stock{prop:Color} = 16777215
    End ! If ?sto:Quantity_Stock{prop:Req} = True
    ?sto:Quantity_Stock{prop:Trn} = 0
    ?sto:Quantity_Stock{prop:FontStyle} = font:Bold
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    If ?quantity_required_temp{prop:ReadOnly} = True
        ?quantity_required_temp{prop:FontColor} = 65793
        ?quantity_required_temp{prop:Color} = 15066597
    Elsif ?quantity_required_temp{prop:Req} = True
        ?quantity_required_temp{prop:FontColor} = 65793
        ?quantity_required_temp{prop:Color} = 8454143
    Else ! If ?quantity_required_temp{prop:Req} = True
        ?quantity_required_temp{prop:FontColor} = 65793
        ?quantity_required_temp{prop:Color} = 16777215
    End ! If ?quantity_required_temp{prop:Req} = True
    ?quantity_required_temp{prop:Trn} = 0
    ?quantity_required_temp{prop:FontStyle} = font:Bold
    ?RES:Quantity:Prompt{prop:FontColor} = -1
    ?RES:Quantity:Prompt{prop:Color} = 15066597
    If ?res:Quantity{prop:ReadOnly} = True
        ?res:Quantity{prop:FontColor} = 65793
        ?res:Quantity{prop:Color} = 15066597
    Elsif ?res:Quantity{prop:Req} = True
        ?res:Quantity{prop:FontColor} = 65793
        ?res:Quantity{prop:Color} = 8454143
    Else ! If ?res:Quantity{prop:Req} = True
        ?res:Quantity{prop:FontColor} = 65793
        ?res:Quantity{prop:Color} = 16777215
    End ! If ?res:Quantity{prop:Req} = True
    ?res:Quantity{prop:Trn} = 0
    ?res:Quantity{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Process_Retail_Stock',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Process_Retail_Stock',1)
    SolaceViewVars('Quantity_temp',Quantity_temp,'Process_Retail_Stock',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Process_Retail_Stock',1)
    SolaceViewVars('quantity_required_temp',quantity_required_temp,'Process_Retail_Stock',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RES:Part_Number:Prompt;  SolaceCtrlName = '?RES:Part_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?res:Part_Number;  SolaceCtrlName = '?res:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RES:Description:Prompt;  SolaceCtrlName = '?RES:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?res:Description;  SolaceCtrlName = '?res:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Quantity_Stock:Prompt;  SolaceCtrlName = '?STO:Quantity_Stock:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Quantity_Stock;  SolaceCtrlName = '?sto:Quantity_Stock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?quantity_required_temp;  SolaceCtrlName = '?quantity_required_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RES:Quantity:Prompt;  SolaceCtrlName = '?RES:Quantity:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?res:Quantity;  SolaceCtrlName = '?res:Quantity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Process_Retail_Stock')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Process_Retail_Stock')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?RES:Part_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(res:Record,History::res:Record)
  SELF.AddHistoryField(?res:Part_Number,3)
  SELF.AddHistoryField(?res:Description,4)
  SELF.AddHistoryField(?res:Quantity,11)
  SELF.AddUpdateFile(Access:RETSTOCK)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:RETSTOCK.Open
  Relate:STOCK.Open
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:RETSTOCK
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  access:stock.clearkey(sto:ref_number_key)
  sto:ref_number = res:part_ref_number
  if access:stock.tryfetch(sto:ref_number_key)
      Return(Level:Fatal)
  end!if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
  quantity_required_temp  = res:quantity
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETSTOCK.Close
    Relate:STOCK.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Process_Retail_Stock',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  error# = 0
  If quantity_required_temp <> res:quantity
          Case MessageEx('Are you sure you want to change the quantity?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
  
                  Of 2 ! &No Button
                  error# = 1
              res:quantity    = quantity_required_temp
          End!Case MessageEx
  
  End!If quantity_required_temp <> res:quantity
  If error# = 0
      If res:quantity <= sto:quantity_stock
          res:date_ordered    = Today()
          res:date_received   = Today()
          res:despatched = 'YES'
  
          sto:quantity_stock -= res:quantity
          access:stock.update()
          get(stohist,0)
          if access:stohist.primerecord() = level:benign
              shi:ref_number           = sto:ref_number
              access:users.clearkey(use:password_key)
              use:password              =glo:password
              access:users.fetch(use:password_key)
              shi:user                  = use:user_code    
              shi:date                 = today()
              shi:transaction_type     = 'DEC'
              shi:despatch_note_number = ''
              shi:job_number           = ''
              shi:sales_number         = ret:ref_number
              shi:quantity             = res:quantity
              shi:purchase_cost        = res:purchase_cost
              shi:sale_cost            = res:sale_cost
              shi:retail_cost          = res:retail_cost
              shi:information          = ''
              shi:notes                = 'RETAIL ITEM SOLD'
              if access:stohist.insert()
                 access:stohist.cancelautoinc()
              end
          end!if access:stohist.primerecord() = level:benign
      End!If res:quantity < sto:quantity_stock
  End!If error# = 0
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Process_Retail_Stock')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

