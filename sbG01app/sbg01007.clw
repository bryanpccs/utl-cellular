

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01007.INC'),ONCE        !Local module procedure declarations
                     END


Insert_Consignment_Note_Number PROCEDURE (f_ref_number,f_number,f_consignment_note_number) !Generated from procedure template - Window

FilesOpened          BYTE
consignment_note_number_temp STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Despatch Routine'),AT(,,220,72),FONT('Tahoma',8,,FONT:regular),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,212,36),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Consignment Note'),AT(8,20),USE(?consignment_note_number_temp:Prompt)
                           ENTRY(@s30),AT(84,20,124,10),USE(consignment_note_number_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       PANEL,AT(4,44,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Address Label'),AT(8,48,56,16),USE(?Address_Label),LEFT,ICON('Print.gif')
                       BUTTON('&OK'),AT(100,48,56,16),USE(?OkButton),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(156,48,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?consignment_note_number_temp:Prompt{prop:FontColor} = -1
    ?consignment_note_number_temp:Prompt{prop:Color} = 15066597
    If ?consignment_note_number_temp{prop:ReadOnly} = True
        ?consignment_note_number_temp{prop:FontColor} = 65793
        ?consignment_note_number_temp{prop:Color} = 15066597
    Elsif ?consignment_note_number_temp{prop:Req} = True
        ?consignment_note_number_temp{prop:FontColor} = 65793
        ?consignment_note_number_temp{prop:Color} = 8454143
    Else ! If ?consignment_note_number_temp{prop:Req} = True
        ?consignment_note_number_temp{prop:FontColor} = 65793
        ?consignment_note_number_temp{prop:Color} = 16777215
    End ! If ?consignment_note_number_temp{prop:Req} = True
    ?consignment_note_number_temp{prop:Trn} = 0
    ?consignment_note_number_temp{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Insert_Consignment_Note_Number',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Insert_Consignment_Note_Number',1)
    SolaceViewVars('consignment_note_number_temp',consignment_note_number_temp,'Insert_Consignment_Note_Number',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?consignment_note_number_temp:Prompt;  SolaceCtrlName = '?consignment_note_number_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?consignment_note_number_temp;  SolaceCtrlName = '?consignment_note_number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Address_Label;  SolaceCtrlName = '?Address_Label';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Insert_Consignment_Note_Number')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Insert_Consignment_Note_Number')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?consignment_note_number_temp:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Insert_Consignment_Note_Number',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Address_Label
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Address_Label, Accepted)
      Set(defaults)
      access:defaults.next()
      glo:select1 = f_ref_number
      Loop x# = 1 to f_number
          Case def:label_printer_type
              Of 'TEC B-440 / B-442'
                  Address_Label_Retail
              Of 'TEC B-452'
                  Address_Label_Retail_B452
          End!Case def:themal_printer_type
      End!Loop x# = 1 to f_number
      glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Address_Label, Accepted)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      If consignment_note_number_temp <> ''
          f_consignment_note_number   = consignment_note_number_temp
          Post(event:closewindow)
      Else
          Select(?consignment_note_number_temp)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      Case MessageEx('Warning! You have chosen to CANCEL the Despatch Procedure.<13,10><13,10>Only continue if you are NOT going to despatch this unit at this time.','ServiceBase 2000',|
                     'Styles\warn.ico','|&Continue|&Cancel',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
              Of 1 ! &Continue Button
                  f_consignment_note_number = ''
              Post(event:closewindow)
              Of 2 ! &Cancel Button
      End!Case MessageEx
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Insert_Consignment_Note_Number')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

