

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01006.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SBG01007.INC'),ONCE        !Req'd for module callout resolution
                     END


Process_Retail_Sale PROCEDURE                         !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::9:TAGFLAG          BYTE(0)
DASBRW::9:TAGMOUSE         BYTE(0)
DASBRW::9:TAGDISPSTATUS    BYTE(0)
DASBRW::9:QUEUE           QUEUE
Pointer                       LIKE(GLO:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
tmp:BrowseType       BYTE(0)
tmp:PrintDespatchNote BYTE(0)
tmp:PrintInvoice     BYTE(0)
tmp:tag              STRING(20)
stop_on_errors_temp  BYTE(0)
save_ret_id          USHORT,AUTO
save_ret_ali_id      USHORT,AUTO
save_rtp_id          USHORT,AUTO
transfer_temp        BYTE
ref_number_temp      LONG
save_res_ali_id      USHORT,AUTO
save_res_id          USHORT,AUTO
ActionMessage        CSTRING(40)
address_line1_temp   STRING(60)
raised_by_temp       STRING(50)
Search_Field_Temp    STRING(30)
despatched_tick      STRING(1)
payment_status_temp  STRING(20)
Despatch_Number_temp STRING(20)
tmp:VatRate          REAL
tmp:PaymentTaken     REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW3::View:Browse    VIEW(RETSTOCK)
                       PROJECT(res:Part_Number)
                       PROJECT(res:Description)
                       PROJECT(res:Quantity)
                       PROJECT(res:Purchase_Order_Number)
                       PROJECT(res:Previous_Sale_Number)
                       PROJECT(res:Record_Number)
                       PROJECT(res:Ref_Number)
                       PROJECT(res:Despatched)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:tag                LIKE(tmp:tag)                  !List box control field - type derived from local data
tmp:tag_NormalFG       LONG                           !Normal forground color
tmp:tag_NormalBG       LONG                           !Normal background color
tmp:tag_SelectedFG     LONG                           !Selected forground color
tmp:tag_SelectedBG     LONG                           !Selected background color
tmp:tag_Icon           LONG                           !Entry's icon ID
res:Part_Number        LIKE(res:Part_Number)          !List box control field - type derived from field
res:Part_Number_NormalFG LONG                         !Normal forground color
res:Part_Number_NormalBG LONG                         !Normal background color
res:Part_Number_SelectedFG LONG                       !Selected forground color
res:Part_Number_SelectedBG LONG                       !Selected background color
res:Description        LIKE(res:Description)          !List box control field - type derived from field
res:Description_NormalFG LONG                         !Normal forground color
res:Description_NormalBG LONG                         !Normal background color
res:Description_SelectedFG LONG                       !Selected forground color
res:Description_SelectedBG LONG                       !Selected background color
res:Quantity           LIKE(res:Quantity)             !List box control field - type derived from field
res:Quantity_NormalFG  LONG                           !Normal forground color
res:Quantity_NormalBG  LONG                           !Normal background color
res:Quantity_SelectedFG LONG                          !Selected forground color
res:Quantity_SelectedBG LONG                          !Selected background color
res:Purchase_Order_Number LIKE(res:Purchase_Order_Number) !List box control field - type derived from field
res:Purchase_Order_Number_NormalFG LONG               !Normal forground color
res:Purchase_Order_Number_NormalBG LONG               !Normal background color
res:Purchase_Order_Number_SelectedFG LONG             !Selected forground color
res:Purchase_Order_Number_SelectedBG LONG             !Selected background color
res:Previous_Sale_Number LIKE(res:Previous_Sale_Number) !List box control field - type derived from field
res:Previous_Sale_Number_NormalFG LONG                !Normal forground color
res:Previous_Sale_Number_NormalBG LONG                !Normal background color
res:Previous_Sale_Number_SelectedFG LONG              !Selected forground color
res:Previous_Sale_Number_SelectedBG LONG              !Selected background color
tmp:BrowseType         LIKE(tmp:BrowseType)           !List box control field - type derived from local data
tmp:BrowseType_NormalFG LONG                          !Normal forground color
tmp:BrowseType_NormalBG LONG                          !Normal background color
tmp:BrowseType_SelectedFG LONG                        !Selected forground color
tmp:BrowseType_SelectedBG LONG                        !Selected background color
res:Record_Number      LIKE(res:Record_Number)        !Primary key field - type derived from field
res:Ref_Number         LIKE(res:Ref_Number)           !Browse key field - type derived from field
res:Despatched         LIKE(res:Despatched)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK15::res:Despatched      LIKE(res:Despatched)
HK15::res:Ref_Number      LIKE(res:Ref_Number)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Process Retail Sale'),AT(,,531,316),FONT('Tahoma',8,,),CENTER,IMM,HLP('Process_Retail_Sales'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,524,124),USE(?Sheet3),WIZARD,SPREAD
                         TAB('Tab 2'),USE(?Tab2)
                           PROMPT(''),AT(8,8),USE(?Prompt1),FONT(,10,COLOR:Navy,FONT:bold),COLOR(COLOR:Silver)
                           PROMPT('Order Details'),AT(304,8),USE(?Prompt7),TRN,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           LINE,AT(263,12,0,109),USE(?Line1),COLOR(COLOR:Black)
                           PROMPT('Account No:'),AT(8,24),USE(?Prompt2)
                           STRING(@s15),AT(88,24),USE(ret:Account_Number),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           PROMPT('Date Raised:'),AT(304,24),USE(?Prompt8),TRN,COLOR(COLOR:Silver)
                           STRING(@d6b),AT(380,24),USE(ret:date_booked),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           STRING(@t1),AT(420,24),USE(ret:time_booked),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           PROMPT('Contact Name:'),AT(8,36),USE(?Prompt3)
                           PROMPT('Delivery Address'),AT(8,52),USE(?Prompt4),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           STRING(@s30),AT(88,36),USE(ret:Contact_Name),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           STRING('Rasied By:'),AT(304,32),USE(?String8),TRN,COLOR(COLOR:Silver)
                           STRING('Payment Status:'),AT(304,44),USE(?String8:2),TRN,COLOR(COLOR:Silver)
                           PROMPT('Company Name:'),AT(8,68),USE(?Prompt5)
                           STRING(@s30),AT(88,68),USE(ret:Company_Name_Delivery),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           STRING(@s50),AT(380,32),USE(raised_by_temp),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           STRING(@s20),AT(380,44),USE(payment_status_temp),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           PROMPT('Invoice Number:'),AT(304,60),USE(?Prompt9),TRN,COLOR(COLOR:Silver)
                           PROMPT('Despatch Number'),AT(304,52),USE(?Prompt9:2),TRN,COLOR(COLOR:Silver)
                           STRING(@s8b),AT(380,60),USE(ret:Invoice_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                           STRING(@s8b),AT(380,52),USE(ret:Despatch_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                           PROMPT('Address:'),AT(8,80),USE(?Prompt6)
                           STRING(@s60),AT(88,80),USE(address_line1_temp),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           PROMPT('Delivery Text'),AT(304,72),USE(?ret:delivery_text:prompt)
                           TEXT,AT(380,72,124,24),USE(ret:Delivery_Text),VSCROLL,FONT(,,,FONT:bold),UPR
                           STRING(@s30),AT(88,92),USE(ret:Address_Line2_Delivery),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           PROMPT('OUTSTANDING PAYMENT'),AT(184,264),USE(?OutstandingPayment),HIDE,CENTER,FONT(,14,COLOR:Red,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(88,104),USE(ret:Address_Line3_Delivery),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           STRING(@s10),AT(88,116),USE(ret:Postcode_Delivery),TRN,LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           PROMPT('Date Despatched'),AT(304,100),USE(?RET:Date_Despatched:Prompt),TRN
                           ENTRY(@d6b),AT(380,100,64,10),USE(ret:Date_Despatched),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Consignment Number'),AT(304,112),USE(?RET:Consignment_Number:Prompt),TRN
                           ENTRY(@s30),AT(380,112,124,10),USE(ret:Consignment_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON('&Adjust Quantity'),AT(440,264,80,16),USE(?Change),LEFT,ICON('edit.gif')
                       SHEET,AT(4,132,524,152),USE(?Sheet4),SPREAD
                         TAB('Un-Picked Items'),USE(?Tab4)
                           BUTTON('&Rev tags'),AT(376,148,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('&Tag'),AT(8,264,56,16),USE(?DASTAG),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(64,264,56,16),USE(?DASTAGAll),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(120,264,56,16),USE(?DASUNTAGALL),LEFT,ICON('UnTag.gif')
                           BUTTON('Pick Tagged Items'),AT(356,264,80,16),USE(?ProcessTagged),LEFT,ICON('process.gif')
                           BUTTON('sho&W tags'),AT(424,148,70,13),USE(?DASSHOWTAG),HIDE
                           ENTRY(@s30),AT(8,148,124,10),USE(res:Part_Number),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('Picked Items'),USE(?Tab3)
                           ENTRY(@s30),AT(8,148,124,10),USE(res:Part_Number,,?res:Part_Number:2),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('Back Orders'),USE(?Tab5)
                           BOX,AT(432,148,11,10),USE(?Box1),COLOR(COLOR:Black),FILL(COLOR:Black)
                           BOX,AT(332,148,11,10),USE(?Box1:3),COLOR(COLOR:Black),FILL(COLOR:Silver)
                           PROMPT('- Cancelled Back Order'),AT(448,148),USE(?Prompt14)
                           PROMPT('- Placed On New Sale'),AT(348,148),USE(?Prompt14:2)
                           BOX,AT(393,170,11,10),USE(?Box1:2),COLOR(COLOR:Black),FILL(COLOR:Black)
                         END
                       END
                       LIST,AT(8,164,516,96),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)*J@n1@125L(2)|M*~Part Number~@s30@126L(2)|M*~Description~@s30@32L(2)|M*~Qu' &|
   'antity~@n8@120L(2)|M*~Purchase Order Number~@s30@97R(2)|M*~Sales Number~L@s8b@4R' &|
   '(2)|M*~Browse Type For Colouring~L@n1@'),FROM(Queue:Browse:1),DROPID('FromList')
                       BUTTON('Close'),AT(468,292,56,16),USE(?Close),LEFT,ICON('Cancel.gif')
                       BUTTON('Payments'),AT(8,292,64,16),USE(?Payments),LEFT,ICON('money2.gif')
                       BUTTON('Print Invoice'),AT(292,292,75,17),USE(?PrintInvoice),LEFT,ICON('Print.gif')
                       PANEL,AT(4,288,524,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Return Tagged Items To Stock'),AT(128,292,84,16),USE(?ReturnTagged),HIDE,LEFT,ICON('stockvsm.gif')
                       BUTTON('Print Despatch Note'),AT(216,292,75,17),USE(?PrintDespatchNote),LEFT,ICON('Print.gif')
                       BUTTON('&OK'),AT(412,292,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT,REQ
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW3                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW3::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW3::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet4) = 1
BRW3::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet4) = 2
BRW3::Sort3:Locator  StepLocatorClass                 !Conditional Locator - Choice(?Sheet4) = 3
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet3{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?ret:Account_Number{prop:FontColor} = -1
    ?ret:Account_Number{prop:Color} = 15066597
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    ?ret:date_booked{prop:FontColor} = -1
    ?ret:date_booked{prop:Color} = 15066597
    ?ret:time_booked{prop:FontColor} = -1
    ?ret:time_booked{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?ret:Contact_Name{prop:FontColor} = -1
    ?ret:Contact_Name{prop:Color} = 15066597
    ?String8{prop:FontColor} = -1
    ?String8{prop:Color} = 15066597
    ?String8:2{prop:FontColor} = -1
    ?String8:2{prop:Color} = 15066597
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?ret:Company_Name_Delivery{prop:FontColor} = -1
    ?ret:Company_Name_Delivery{prop:Color} = 15066597
    ?raised_by_temp{prop:FontColor} = -1
    ?raised_by_temp{prop:Color} = 15066597
    ?payment_status_temp{prop:FontColor} = -1
    ?payment_status_temp{prop:Color} = 15066597
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    ?Prompt9:2{prop:FontColor} = -1
    ?Prompt9:2{prop:Color} = 15066597
    ?ret:Invoice_Number{prop:FontColor} = -1
    ?ret:Invoice_Number{prop:Color} = 15066597
    ?ret:Despatch_Number{prop:FontColor} = -1
    ?ret:Despatch_Number{prop:Color} = 15066597
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?address_line1_temp{prop:FontColor} = -1
    ?address_line1_temp{prop:Color} = 15066597
    ?ret:delivery_text:prompt{prop:FontColor} = -1
    ?ret:delivery_text:prompt{prop:Color} = 15066597
    If ?ret:Delivery_Text{prop:ReadOnly} = True
        ?ret:Delivery_Text{prop:FontColor} = 65793
        ?ret:Delivery_Text{prop:Color} = 15066597
    Elsif ?ret:Delivery_Text{prop:Req} = True
        ?ret:Delivery_Text{prop:FontColor} = 65793
        ?ret:Delivery_Text{prop:Color} = 8454143
    Else ! If ?ret:Delivery_Text{prop:Req} = True
        ?ret:Delivery_Text{prop:FontColor} = 65793
        ?ret:Delivery_Text{prop:Color} = 16777215
    End ! If ?ret:Delivery_Text{prop:Req} = True
    ?ret:Delivery_Text{prop:Trn} = 0
    ?ret:Delivery_Text{prop:FontStyle} = font:Bold
    ?ret:Address_Line2_Delivery{prop:FontColor} = -1
    ?ret:Address_Line2_Delivery{prop:Color} = 15066597
    ?OutstandingPayment{prop:FontColor} = -1
    ?OutstandingPayment{prop:Color} = 15066597
    ?ret:Address_Line3_Delivery{prop:FontColor} = -1
    ?ret:Address_Line3_Delivery{prop:Color} = 15066597
    ?ret:Postcode_Delivery{prop:FontColor} = -1
    ?ret:Postcode_Delivery{prop:Color} = 15066597
    ?RET:Date_Despatched:Prompt{prop:FontColor} = -1
    ?RET:Date_Despatched:Prompt{prop:Color} = 15066597
    If ?ret:Date_Despatched{prop:ReadOnly} = True
        ?ret:Date_Despatched{prop:FontColor} = 65793
        ?ret:Date_Despatched{prop:Color} = 15066597
    Elsif ?ret:Date_Despatched{prop:Req} = True
        ?ret:Date_Despatched{prop:FontColor} = 65793
        ?ret:Date_Despatched{prop:Color} = 8454143
    Else ! If ?ret:Date_Despatched{prop:Req} = True
        ?ret:Date_Despatched{prop:FontColor} = 65793
        ?ret:Date_Despatched{prop:Color} = 16777215
    End ! If ?ret:Date_Despatched{prop:Req} = True
    ?ret:Date_Despatched{prop:Trn} = 0
    ?ret:Date_Despatched{prop:FontStyle} = font:Bold
    ?RET:Consignment_Number:Prompt{prop:FontColor} = -1
    ?RET:Consignment_Number:Prompt{prop:Color} = 15066597
    If ?ret:Consignment_Number{prop:ReadOnly} = True
        ?ret:Consignment_Number{prop:FontColor} = 65793
        ?ret:Consignment_Number{prop:Color} = 15066597
    Elsif ?ret:Consignment_Number{prop:Req} = True
        ?ret:Consignment_Number{prop:FontColor} = 65793
        ?ret:Consignment_Number{prop:Color} = 8454143
    Else ! If ?ret:Consignment_Number{prop:Req} = True
        ?ret:Consignment_Number{prop:FontColor} = 65793
        ?ret:Consignment_Number{prop:Color} = 16777215
    End ! If ?ret:Consignment_Number{prop:Req} = True
    ?ret:Consignment_Number{prop:Trn} = 0
    ?ret:Consignment_Number{prop:FontStyle} = font:Bold
    ?Sheet4{prop:Color} = 15066597
    ?Tab4{prop:Color} = 15066597
    If ?res:Part_Number{prop:ReadOnly} = True
        ?res:Part_Number{prop:FontColor} = 65793
        ?res:Part_Number{prop:Color} = 15066597
    Elsif ?res:Part_Number{prop:Req} = True
        ?res:Part_Number{prop:FontColor} = 65793
        ?res:Part_Number{prop:Color} = 8454143
    Else ! If ?res:Part_Number{prop:Req} = True
        ?res:Part_Number{prop:FontColor} = 65793
        ?res:Part_Number{prop:Color} = 16777215
    End ! If ?res:Part_Number{prop:Req} = True
    ?res:Part_Number{prop:Trn} = 0
    ?res:Part_Number{prop:FontStyle} = font:Bold
    ?Tab3{prop:Color} = 15066597
    If ?res:Part_Number:2{prop:ReadOnly} = True
        ?res:Part_Number:2{prop:FontColor} = 65793
        ?res:Part_Number:2{prop:Color} = 15066597
    Elsif ?res:Part_Number:2{prop:Req} = True
        ?res:Part_Number:2{prop:FontColor} = 65793
        ?res:Part_Number:2{prop:Color} = 8454143
    Else ! If ?res:Part_Number:2{prop:Req} = True
        ?res:Part_Number:2{prop:FontColor} = 65793
        ?res:Part_Number:2{prop:Color} = 16777215
    End ! If ?res:Part_Number:2{prop:Req} = True
    ?res:Part_Number:2{prop:Trn} = 0
    ?res:Part_Number:2{prop:FontStyle} = font:Bold
    ?Tab5{prop:Color} = 15066597
    ?Prompt14{prop:FontColor} = -1
    ?Prompt14{prop:Color} = 15066597
    ?Prompt14:2{prop:FontColor} = -1
    ?Prompt14:2{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::9:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW3.UpdateBuffer
   glo:Queue.Pointer = res:Record_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = res:Record_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:tag = ''
  END
    Queue:Browse:1.tmp:tag = tmp:tag
  Queue:Browse:1.tmp:tag_NormalFG = -1
  Queue:Browse:1.tmp:tag_NormalBG = -1
  Queue:Browse:1.tmp:tag_SelectedFG = -1
  Queue:Browse:1.tmp:tag_SelectedBG = -1
  IF (tmp:tag = '*')
    Queue:Browse:1.tmp:tag_Icon = 2
  ELSE
    Queue:Browse:1.tmp:tag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::9:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW3.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = res:Record_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::9:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW3.Reset
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::9:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::9:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::9:QUEUE = glo:Queue
    ADD(DASBRW::9:QUEUE)
  END
  FREE(glo:Queue)
  BRW3.Reset
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::9:QUEUE.Pointer = res:Record_Number
     GET(DASBRW::9:QUEUE,DASBRW::9:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = res:Record_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::9:DASSHOWTAG Routine
   CASE DASBRW::9:TAGDISPSTATUS
   OF 0
      DASBRW::9:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::9:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::9:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW3.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
ShowHideUnPicking       Routine
    Access:RETSTOCK.ClearKey(res:Despatched_Only_Key)
    res:Ref_Number = ret:Ref_Number
    res:Despatched = 'PIK'
    Set(res:Despatched_Only_Key,res:Despatched_Only_Key)
    If Access:RETSTOCK.NEXT()
        ?Tab4{prop:Hide} = 1
    Else !If Access:RETSTOCK.NEXT()
        If res:Ref_Number <> ret:Ref_Number Or |
            res:Despatched <> 'PIK'
            ?tab4{prop:Hide} = 1
        Else !If res:Ref_Number <> ref:Ref_Number Or |
            ?Tab4{prop:Hide} = 0
        End !If res:Ref_Number <> ref:Ref_Number Or |
    End !Access:RETSTOCK.NEXT()
    Brw3.ResetSort(1)
CheckPayment        Routine
    If RetailPaid() = Level:Benign
        ?OutstandingPayment{prop:Hide} = 1
    Else !If SalePaid() = Level:Benign
        ?OutstandingPayment{prop:Hide} = 0
    End !If SalePaid() = Level:Benign
    Display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Process_Retail_Sale',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Process_Retail_Sale',1)
    SolaceViewVars('tmp:BrowseType',tmp:BrowseType,'Process_Retail_Sale',1)
    SolaceViewVars('tmp:PrintDespatchNote',tmp:PrintDespatchNote,'Process_Retail_Sale',1)
    SolaceViewVars('tmp:PrintInvoice',tmp:PrintInvoice,'Process_Retail_Sale',1)
    SolaceViewVars('tmp:tag',tmp:tag,'Process_Retail_Sale',1)
    SolaceViewVars('stop_on_errors_temp',stop_on_errors_temp,'Process_Retail_Sale',1)
    SolaceViewVars('save_ret_id',save_ret_id,'Process_Retail_Sale',1)
    SolaceViewVars('save_ret_ali_id',save_ret_ali_id,'Process_Retail_Sale',1)
    SolaceViewVars('save_rtp_id',save_rtp_id,'Process_Retail_Sale',1)
    SolaceViewVars('transfer_temp',transfer_temp,'Process_Retail_Sale',1)
    SolaceViewVars('ref_number_temp',ref_number_temp,'Process_Retail_Sale',1)
    SolaceViewVars('save_res_ali_id',save_res_ali_id,'Process_Retail_Sale',1)
    SolaceViewVars('save_res_id',save_res_id,'Process_Retail_Sale',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Process_Retail_Sale',1)
    SolaceViewVars('address_line1_temp',address_line1_temp,'Process_Retail_Sale',1)
    SolaceViewVars('raised_by_temp',raised_by_temp,'Process_Retail_Sale',1)
    SolaceViewVars('Search_Field_Temp',Search_Field_Temp,'Process_Retail_Sale',1)
    SolaceViewVars('despatched_tick',despatched_tick,'Process_Retail_Sale',1)
    SolaceViewVars('payment_status_temp',payment_status_temp,'Process_Retail_Sale',1)
    SolaceViewVars('Despatch_Number_temp',Despatch_Number_temp,'Process_Retail_Sale',1)
    SolaceViewVars('tmp:VatRate',tmp:VatRate,'Process_Retail_Sale',1)
    SolaceViewVars('tmp:PaymentTaken',tmp:PaymentTaken,'Process_Retail_Sale',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet3;  SolaceCtrlName = '?Sheet3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1;  SolaceCtrlName = '?Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Account_Number;  SolaceCtrlName = '?ret:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:date_booked;  SolaceCtrlName = '?ret:date_booked';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:time_booked;  SolaceCtrlName = '?ret:time_booked';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Contact_Name;  SolaceCtrlName = '?ret:Contact_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String8;  SolaceCtrlName = '?String8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String8:2;  SolaceCtrlName = '?String8:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Company_Name_Delivery;  SolaceCtrlName = '?ret:Company_Name_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?raised_by_temp;  SolaceCtrlName = '?raised_by_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?payment_status_temp;  SolaceCtrlName = '?payment_status_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt9;  SolaceCtrlName = '?Prompt9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt9:2;  SolaceCtrlName = '?Prompt9:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Invoice_Number;  SolaceCtrlName = '?ret:Invoice_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Despatch_Number;  SolaceCtrlName = '?ret:Despatch_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?address_line1_temp;  SolaceCtrlName = '?address_line1_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:delivery_text:prompt;  SolaceCtrlName = '?ret:delivery_text:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Delivery_Text;  SolaceCtrlName = '?ret:Delivery_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Address_Line2_Delivery;  SolaceCtrlName = '?ret:Address_Line2_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OutstandingPayment;  SolaceCtrlName = '?OutstandingPayment';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Address_Line3_Delivery;  SolaceCtrlName = '?ret:Address_Line3_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Postcode_Delivery;  SolaceCtrlName = '?ret:Postcode_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Date_Despatched:Prompt;  SolaceCtrlName = '?RET:Date_Despatched:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Date_Despatched;  SolaceCtrlName = '?ret:Date_Despatched';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Consignment_Number:Prompt;  SolaceCtrlName = '?RET:Consignment_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Consignment_Number;  SolaceCtrlName = '?ret:Consignment_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet4;  SolaceCtrlName = '?Sheet4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ProcessTagged;  SolaceCtrlName = '?ProcessTagged';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?res:Part_Number;  SolaceCtrlName = '?res:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?res:Part_Number:2;  SolaceCtrlName = '?res:Part_Number:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Box1;  SolaceCtrlName = '?Box1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Box1:3;  SolaceCtrlName = '?Box1:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt14;  SolaceCtrlName = '?Prompt14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt14:2;  SolaceCtrlName = '?Prompt14:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Box1:2;  SolaceCtrlName = '?Box1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Payments;  SolaceCtrlName = '?Payments';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PrintInvoice;  SolaceCtrlName = '?PrintInvoice';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReturnTagged;  SolaceCtrlName = '?ReturnTagged';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PrintDespatchNote;  SolaceCtrlName = '?PrintDespatchNote';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Process_Retail_Sale')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Process_Retail_Sale')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  SELF.AddUpdateFile(Access:RETSALES)
  Relate:COURIER.Open
  Relate:DEFAULTS.Open
  Relate:RETDESNO.Open
  Relate:RETSALES_ALIAS.Open
  Relate:VATCODE.Open
  Access:RETSALES.UseFile
  Access:USERS.UseFile
  Access:INVOICE.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:RETSTOCK.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:RETSALES
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW3.Init(?List:2,Queue:Browse:1.ViewPosition,BRW3::View:Browse,Queue:Browse:1,Relate:RETSTOCK,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do CheckPayment
  Do RecolourWindow
  ?List:2{prop:vcr} = TRUE
  ! support for CPCS
  BRW3.Q &= Queue:Browse:1
  BRW3.RetainRow = 0
  BRW3.AddSortOrder(,res:DespatchedPartKey)
  BRW3.AddRange(res:Despatched)
  BRW3.AddLocator(BRW3::Sort1:Locator)
  BRW3::Sort1:Locator.Init(?res:Part_Number,res:Part_Number,1,BRW3)
  BRW3.AddSortOrder(,res:DespatchedPartKey)
  BRW3.AddRange(res:Despatched)
  BRW3.AddLocator(BRW3::Sort2:Locator)
  BRW3::Sort2:Locator.Init(?res:Part_Number:2,res:Part_Number,1,BRW3)
  BRW3.AddSortOrder(,res:Part_Number_Key)
  BRW3.AddRange(res:Ref_Number)
  BRW3.AddLocator(BRW3::Sort3:Locator)
  BRW3::Sort3:Locator.Init(,res:Part_Number,1,BRW3)
  BRW3.SetFilter('(Upper(res:Despatched) = ''PEN'' Or Upper(res:Despatched) = ''CAN'' or Upper(res:Despatched) = ''OLD'')')
  BRW3.AddSortOrder(,res:Part_Number_Key)
  BRW3.AddRange(res:Ref_Number,ret:Ref_Number)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(,res:Part_Number,1,BRW3)
  BIND('tmp:tag',tmp:tag)
  BIND('tmp:BrowseType',tmp:BrowseType)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW3.AddField(tmp:tag,BRW3.Q.tmp:tag)
  BRW3.AddField(res:Part_Number,BRW3.Q.res:Part_Number)
  BRW3.AddField(res:Description,BRW3.Q.res:Description)
  BRW3.AddField(res:Quantity,BRW3.Q.res:Quantity)
  BRW3.AddField(res:Purchase_Order_Number,BRW3.Q.res:Purchase_Order_Number)
  BRW3.AddField(res:Previous_Sale_Number,BRW3.Q.res:Previous_Sale_Number)
  BRW3.AddField(tmp:BrowseType,BRW3.Q.tmp:BrowseType)
  BRW3.AddField(res:Record_Number,BRW3.Q.res:Record_Number)
  BRW3.AddField(res:Ref_Number,BRW3.Q.res:Ref_Number)
  BRW3.AddField(res:Despatched,BRW3.Q.res:Despatched)
  BRW3.AskProcedure = 1
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab4{PROP:TEXT} = 'Un-Picked Items'
    ?Tab3{PROP:TEXT} = 'Picked Items'
    ?Tab5{PROP:TEXT} = 'Back Orders'
    ?List:2{PROP:FORMAT} ='11L(2)*J@n1@#1#125L(2)|M*~Part Number~@s30@#7#126L(2)|M*~Description~@s30@#12#32L(2)|M*~Quantity~@n8@#17#120L(2)|M*~Purchase Order Number~@s30@#22#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  Ref_Number_Temp = ret:Ref_Number
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:COURIER.Close
    Relate:DEFAULTS.Close
    Relate:RETDESNO.Close
    Relate:RETSALES_ALIAS.Close
    Relate:VATCODE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Process_Retail_Sale',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
      If transfer_temp = 1
          access:retstock.open()
          access:retstock.usefile()
          access:retsales.open()
          access:retsales.usefile()
          access:retstock_alias.open()
          access:retstock_alias.usefile()
          access:retsales_alias.open()
          access:retsales_alias.usefile()
  
          access:retsales_alias.clearkey(res_ali:ref_number_key)
          res_ali:ref_number = ref_number_temp
          if access:retsales_alias.tryfetch(res_ali:ref_number_key) = Level:Benign
              Get(retsales,0)
              If access:retsales.primerecord() = Level:Benign
                  ref_number$ = ret:ref_number
                  ret:record  :=: res_ali:record
                  ret:ref_number  = ref_number$
                  ret:despatched = 'RET'
                  access:retsales.insert()
  
                  setcursor(cursor:wait)
                  save_ret_ali_id = access:retstock_alias.savefile()
                  access:retstock_alias.clearkey(ret_ali:despatched_only_key)
                  ret_ali:ref_number = ref_number_temp
                  ret_ali:despatched = 'NO'
                  set(ret_ali:despatched_only_key,ret_ali:despatched_only_key)
                  loop
                      if access:retstock_alias.next()
                         break
                      end !if
                      if ret_ali:ref_number <> ref_number_temp      |
                      or ret_ali:despatched <> 'NO'      |
                          then break.  ! end if
                      Get(retstock,0)
                      If access:retstock.primerecord() = Level:Benign
                          record_number$  = res:record_number
                          res:record  :=: ret_ali:record
                          res:record_number   = record_number$
                          res:ref_number  = ret:ref_number
                          access:retstock.insert()
                      End!If access:retstock.primerecord() = Level:Benign
                  end !loop
                  access:retstock_alias.restorefile(save_ret_ali_id)
                  setcursor()
              End!If access:retsales.primerecord() = Level:Benign
  
              save_res_id = access:retstock.savefile()
              access:retstock.clearkey(res:despatched_only_key)
              res:ref_number = ref_number_temp
              res:despatched = 'NO'
              set(res:despatched_only_key,res:despatched_only_key)
              loop
                  if access:retstock.next()
                     break
                  end !if
                  if res:ref_number <> ref_number_temp      |
                  or res:despatched <> 'NO'      |
                      then break.  ! end if
                  Delete(retstock)
              end !loop
              access:retstock.restorefile(save_res_id)
          end!if access:retsales_alias.tryfetch(res_ali:ref_number_key) = Level:Benign
          access:retstock_alias.close()
          access:retsales.close()
          access:retstock.close()
          access:retsales_alias.close()
  
      End!If transfer_temp = 1
  If thiswindow.response <> requestcancelled
      Access:DEFAULTS.Open()
      Access:DEFAULTS.UseFile()
  
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
  
      glo:select1  = Ref_number_temp
      relate:retsales.open()
      access:retdesno.open()
      access:retdesno.usefile()
      relate:tradeacc.open()
      relate:invoice.open()
      access:vatcode.open()
      access:vatcode.usefile()
  
      CheckPayment# = 0
  
      Access:RETSALES.Clearkey(ret:Ref_Number_Key)
      ret:Ref_Number  = Ref_Number_Temp
      If Access:RETSALES.Tryfetch(ret:Ref_Number_Key) = Level:Benign
          !Found
  
      
          If tmp:PrintDespatchNote = 1
  
  
              !Create a New Despatch Batch Number, and assign this to the job
  
              If ret:Despatch_Number = 0
                  if access:retdesno.primerecord() = Level:Benign
                      RDN:Consignment_Number  = ''
                      RDN:Courier             = ret:courier
                      rdn:sale_number         = ret:ref_number
                      access:retdesno.insert()
  
                      ret:despatched = 'PRO'
  
                      CheckPayment# = 1
  
                      ret:despatch_number = rdn:despatch_number
                      access:retsales.update()
                  End!if access:retdesno.primerecord() = Level:Benign
              End !If ret:Despatch_Number = 0
              glo:select1  = ret:despatch_number
              Retail_Despatch_Note
              glo:select1  = ''
              thiswindow.reset(1)
  
          End!If tmp:PrintDespatchNote = 1
  
          If tmp:PrintInvoice = 1
              Include('ret_inv.inc')
          End!If tmp:createinvoice = 1
  
          If CheckPayment#
              If ret:Payment_Method = 'CAS'
                  If RetailPaid() = Level:Fatal
                      ret:Despatched = 'PAY'
                      Access:RETSALES.Update()
                  End !If RetailPaid() = Level:Benign
              End !If ret:Payment_Method = 'CAS'
          End !If CheckPayment#
  
      Else! If Access:RETSALES.Tryfetch(ret:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:RETSALES.Tryfetch(ret:Ref_Number_Key) = Level:Benign
  
  
      access:vatcode.close()
      relate:retsales.close()
      access:retdesno.close()
      relate:tradeacc.close()
      relate:invoice.close()
      
      glo:select1  = ''
  
      Access:DEFAULTS.Close()
  End!IF thiswindow.request <> requestcancelled
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  Case request
      Of insertrecord
          do_update# = False
          access:RETSTOCK.cancelautoinc()
      Of changerecord
  !        If res:despatched <> 'YES'
  !            request = Viewrecord
  !        End!If res:despatched <> 'YES'
      Of deleterecord
          do_update# = False
  End!Case request
  
  If do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Retail_Stock(0,1)
    ReturnValue = GlobalResponse
  END
  End!If do_update# = true
      !glo:Select2 = Flag
      !glo:Select3 = Stock Ref number
      !glo:Select4 = RETSTOCK record number
      !glo:Select5 = Quantity
      !glo:Select6 = OPDPEND record number, not needed anymore
      If glo:select2   = 'NEW PENDING' or glo:Select2 = 'CANCELLED'
          UpdateRetailPartRoutine()
      End !If glo:select2   = 'NEW PENDING'
      If glo:Select7 = 'DELETE'
          Access:RETSTOCK.ClearKey(res:Record_Number_Key)
          res:Record_Number = glo:Select4
          If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
              !Found
              Delete(RETSTOCK)
          Else!If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
      End !If glo:Select7 = 'DELETE'
      glo:select1     = ''
      glo:Select2     = ''
      glo:Select3     = ''
      glo:Select5     = ''
      glo:Select4     = ''
      glo:Select7     = ''
  
  BRW3.ResetSort(1)
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ProcessTagged
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessTagged, Accepted)
      Case MessageEx('Are you sure you want to marked the tagged items as picked?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Loop x# = 1 To Records(glo:Queue)
                  Get(glo:Queue,x#)
                  Access:RETSTOCK.ClearKey(res:Record_Number_Key)
                  res:Record_Number = glo:Pointer
                  If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
                      If res:Despatched = 'PIK'
                          res:Despatched = 'YES'
                          Access:RETSTOCK.Update()
                      End!If res:Despatched = 'PIK'
                  End!If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
              End!Loop x# = 1 To Records(glo:Queue)
          Of 2 ! &No Button
      End!Case MessageEx
      Do ShowHideUnPicking
      Do DASBRW::9:DASUNTAGALL
      BRW3.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessTagged, Accepted)
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Payments
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Payments, Accepted)
      Browse_Retail_payments (ret:ref_number,ret:account_number,ret:courier_cost,'YES')
      Do CheckPayment
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Payments, Accepted)
    OF ?PrintInvoice
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PrintInvoice, Accepted)
      tmp:PrintInvoice = 0
      If ret:Invoice_Number <> ''
          Case MessageEx('This sale has already been invoiced.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,300) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If ret:Invoice_Number <> ''
          Case MessageEx('Are you sure you want to create an Invoice for this sale?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,300) 
              Of 1 ! &Yes Button
                  Case MessageEx('An Invoice will be created when you complete this screen.'&|
                    '<13,10>'&|
                    '<13,10>(This message will close automatically)','ServiceBase 2000',|
                                 'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,300) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
      
                  tmp:PrintInvoice = 1
              Of 2 ! &No Button
          End!Case MessageEx
      End!If ret:Invoice_Number <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PrintInvoice, Accepted)
    OF ?ReturnTagged
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReturnTagged, Accepted)
       Case MessageEx('Are you sure you want to return the tagged (non-picked) items to stock?','',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Loop x# = 1 To Records(glo:Queue)
                  Get(glo:Queue,x#)
                  Access:RETSTOCK.ClearKey(res:Record_Number_Key)
                  res:Record_Number = glo:Pointer
                  If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
                      If res:Despatched = 'PIK'
                          Access:STOCK.ClearKey(sto:Ref_Number_Key)
                          sto:Ref_Number = res:Part_Ref_Number
                          If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                              sto:Quantity_Stock += res:Quantity
                              Access:STOCK.Update()
                              If Access:STOHIST.Primerecord() = Level:Benign
                                  shi:Ref_Number              = sto:Ref_Number
                                  shi:Transaction_Type        = 'ADD'
                                  shi:Despatch_Note_Number    = ''
                                  shi:Quantity                = res:Quantity
                                  shi:Date                    = Today()
                                  shi:Purchase_Cost           = res:Purchase_Cost
                                  shi:Sale_Cost               = res:Sale_Cost
                                  shi:Job_Number              = res:Ref_Number
                                  Access:USERS.Clearkey(use:Password_Key)
                                  use:Password                = glo:Password
                                  Access:USERS.Tryfetch(use:Password_Key)
                                  shi:User                    = use:User_Code
                                  shi:Notes                   = 'RETAIL ITEM RESTOCKED'
                                  shi:Information             = ''
                                  If Access:STOHIST.Tryinsert()
                                      Access:STOHIST.Cancelautoinc()
                                  End!If Access:STOHIST.Tryinsert()
                              End!If Access:STOHIST.Primerecord() = Level:Benign
                              res:Despatched = 'RES'
                              Access:RETSTOCK.Update()
                          End!If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                      End!If res:Despatched = 'PIK'
                  End!If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
              End!Loop x# = 1 To Records(glo:Queue)
          Of 2 ! &No Button
      End!Case MessageEx
      BRW3.ResetSort(1)
      Do DASBRW::9:DASUNTAGALL
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReturnTagged, Accepted)
    OF ?PrintDespatchNote
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PrintDespatchNote, Accepted)
      tmp:PrintDespatchNote = 0
      If ret:Consignment_Number <> ''
          Case MessageEx('This sale has already been despatched.'&|
            '<13,10>'&|
            '<13,10>Do you want to reprint the Despatch Note.','ServiceBase 2000',|
                         'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
                  tmp:PrintDespatchNote = 1
              Of 2 ! &No Button
          End!Case MessageEx
      Else!If ret:Despatch_Number <> ''
          Case MessageEx('Are you sure you want to Despatch this sale?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
                   tmp:PrintDespatchNote = 1
              Of 2 ! &No Button
          End!Case MessageEx
      End!If ret:Despatch_Number <> ''
      If tmp:PrintDespatchNote = 1
          Case MessageEx('A Despatch Note will be printed when you complete this screen.'&|
            '<13,10>'&|
            '<13,10>(This message will close automatically)','ServiceBase 2000',|
                         'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,300) 
              Of 1 ! &OK Button
          End!Case MessageEx
      End !If tmp:PrintDespatchNote = 1
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PrintDespatchNote, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Check Invoice And Change It
  access:invoice.clearkey(inv:invoice_number_key)
  inv:invoice_number = ret:invoice_number
  if access:invoice.tryfetch(inv:invoice_number_key) = Level:Benign
      cost$ = 0
      setcursor(cursor:wait)
      save_res_id = access:retstock.savefile()
      access:retstock.clearkey(res:despatched_only_key)
      res:ref_number = ret:ref_number
      res:despatched = 'YES'
      set(res:despatched_only_key,res:despatched_only_key)
      loop
          if access:retstock.next()
             break
          end !if
          if res:ref_number <> ret:ref_number      |
          or res:despatched <> 'YES'      |
              then break.  ! end if
          cost$  += Round(res:item_cost * res:quantity,.01)
      end !loop
      access:retstock.restorefile(save_res_id)
      setcursor()
      
      If inv:parts_paid <> cost$
          ret:parts_cost = Round(cost$,.01)
          ret:sub_total   = Round(ret:parts_cost,.01) + ret:courier_cost
          inv:parts_paid = ret:parts_cost
          inv:total      = ret:sub_total
          access:invoice.update()
          access:retsales.update()
      End!If inv:parts_paid <> ret:parts_cost
  end!if access:invoice.tryfetch(inv:invoice_number_key) = Level:Benign
  !Picking Finished? Autoprint Despatch Note?
      Picked# = 0
      Save_res_ID = Access:RETSTOCK.SaveFile()
      Access:RETSTOCK.ClearKey(res:Despatched_Only_Key)
      res:Ref_Number = ret:Ref_Number
      res:Despatched = 'YES'
      Set(res:Despatched_Only_Key,res:Despatched_Only_Key)
      Loop
          If Access:RETSTOCK.NEXT()
             Break
          End !If
          If res:Ref_Number <> ret:Ref_Number      |
          Or res:Despatched <> 'YES'      |
              Then Break.  ! End If
          !Is there anything in the picked section?
          Picked# = 1
          Break
      End !Loop
      Access:RETSTOCK.RestoreFile(Save_res_ID)
  
      If Picked#
          Finished# = 0
          Access:RETSTOCK.ClearKey(res:Despatched_Only_Key)
          res:Ref_Number = ret:Ref_Number
          res:Despatched = 'PIK'
          Set(res:Despatched_Only_Key,res:Despatched_Only_Key)
          If Access:RETSTOCK.NEXT()
              Finished# = 1
          Else !If Access:RETSTOCK.NEXT()
              If res:Ref_Number <> ret:Ref_Number Or |
                  res:Despatched <> 'PIK'
                  Finished# = 1
              Else !If res:Ref_Number <> ref:Ref_Number Or |
                  Finished# = 0
              End !If res:Ref_Number <> ref:Ref_Number Or |
          End !Access:RETSTOCK.NEXT()
          If Finished# = 1
              If ret:Despatch_Number = 0
                  tmp:PrintDespatchNote = 1
              End !If ret:Despatch_Number = 0
              If ret:Invoice_Number = 0
                  tmp:PrintInvoice      = 1
              End !If ret:Invoice_Number = 0
          End !If Finished# = 1
      Else !If Picked#
          !This is nothing picked so don't print an invoice/despatch note
          !If there is nothing awaiting to be picked
          Found# = 0
          Save_res_ID = Access:RETSTOCK.SaveFile()
          Access:RETSTOCK.ClearKey(res:Despatched_Only_Key)
          res:Ref_Number = ret:Ref_Number
          res:Despatched = 'PIK'
          Set(res:Despatched_Only_Key,res:Despatched_Only_Key)
          Loop
              If Access:RETSTOCK.NEXT()
                 Break
              End !If
              If res:Ref_Number <> ret:Ref_Number      |
              Or res:Despatched <> 'PIK'      |
                  Then Break.  ! End If
              Found# = 1
              Break
          End !Loop
          Access:RETSTOCK.RestoreFile(Save_res_ID)
          If Found# = 0
              tmp:PrintDespatchNote   = 0
              tmp:PrintInvoice        = 0
              ret:Despatched = 'XXX'
          End !If Found# = 0
          
      End !If Picked#
  !Sale Paid?
      If RetailPaid() = Level:Benign
          If ret:Despatched = 'PAY'
              ret:Despatched = 'PRO'
          End !If ret:Despatched = 'PAY'
      Else!If RetailPaid() = Level:Benign
          ret:Despatched = 'PAY'
      End !If RetailPaid() = Level:Benign
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Process_Retail_Sale')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet4
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?Sheet4)
        OF 1
          ?List:2{PROP:FORMAT} ='11L(2)*J@n1@#1#125L(2)|M*~Part Number~@s30@#7#126L(2)|M*~Description~@s30@#12#32L(2)|M*~Quantity~@n8@#17#120L(2)|M*~Purchase Order Number~@s30@#22#'
          ?Tab4{PROP:TEXT} = 'Un-Picked Items'
        OF 2
          ?List:2{PROP:FORMAT} ='125L(2)|M*~Part Number~@s30@#7#126L(2)|M*~Description~@s30@#12#32L(2)|M*~Quantity~@n8@#17#120L(2)|M*~Purchase Order Number~@s30@#22#'
          ?Tab3{PROP:TEXT} = 'Picked Items'
        OF 3
          ?List:2{PROP:FORMAT} ='125L(2)|M*~Part Number~@s30@#7#126L(2)|M*~Description~@s30@#12#32L(2)|M*~Quantity~@n8@#17#120L(2)|M*~Purchase Order Number~@s30@#22#97R(2)|M*~Sales Number~L@s8b@#27#'
          ?Tab5{PROP:TEXT} = 'Back Orders'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::9:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      ?prompt1{prop:text} = 'Customer Details.  Sale No: ' & ret:ref_number
      
      If RET:Building_Name_Delivery   = ''
          address_line1_temp  = Clip(ret:address_line1_delivery)
      Else!If RET:Building_Name_Delivery   = ''
          address_line1_temp  = Clip(ret:building_name_delivery) & ' ' & Clip(ret:address_line1_delivery)
      End!If RET:Building_Name_Delivery   = ''
      access:users.clearkey(use:user_code_key)
      use:user_code = ret:who_booked
      if access:users.tryfetch(use:user_code_key) = Level:Benign
          raised_by_temp  = Clip(use:forename) & ' ' & Clip(use:surname)
      end!if access:users.tryfetch(use:user_code_key) = Level:Benign
      
      Case ret:payment_method
          OF 'TRA'
              payment_status_temp = 'Trade'
          Of 'CAS'
              payment_status_temp = 'Cash'
          Of 'EXC'
              Payment_Status_Temp = 'Exchange Accessory'
      End!Case ret:payment_method
      
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      
      Do DASBRW::9:DASUNTAGALL
      Do ShowHideUnPicking
      BRW3.ResetSort(1)
      Post(Event:NewSelection,?Sheet4)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW3.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?Sheet4) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = ret:Ref_Number
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 'PIK'
  ELSIF Choice(?Sheet4) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = ret:Ref_Number
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 'YES'
  ELSIF Choice(?Sheet4) = 3
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = ret:Ref_Number
  ELSE
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW3.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
  END


BRW3.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet4) = 1
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?Sheet4) = 2
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?Sheet4) = 3
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW3.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(3, SetQueueRecord, ())
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = res:Record_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:tag = ''
    ELSE
      tmp:tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  SELF.Q.tmp:tag_NormalFG = -1
  SELF.Q.tmp:tag_NormalBG = -1
  SELF.Q.tmp:tag_SelectedFG = -1
  SELF.Q.tmp:tag_SelectedBG = -1
  IF (tmp:tag = '*')
    SELF.Q.tmp:tag_Icon = 2
  ELSE
    SELF.Q.tmp:tag_Icon = 1
  END
  SELF.Q.res:Part_Number_NormalFG = -1
  SELF.Q.res:Part_Number_NormalBG = -1
  SELF.Q.res:Part_Number_SelectedFG = -1
  SELF.Q.res:Part_Number_SelectedBG = -1
  SELF.Q.res:Description_NormalFG = -1
  SELF.Q.res:Description_NormalBG = -1
  SELF.Q.res:Description_SelectedFG = -1
  SELF.Q.res:Description_SelectedBG = -1
  SELF.Q.res:Quantity_NormalFG = -1
  SELF.Q.res:Quantity_NormalBG = -1
  SELF.Q.res:Quantity_SelectedFG = -1
  SELF.Q.res:Quantity_SelectedBG = -1
  SELF.Q.res:Purchase_Order_Number_NormalFG = -1
  SELF.Q.res:Purchase_Order_Number_NormalBG = -1
  SELF.Q.res:Purchase_Order_Number_SelectedFG = -1
  SELF.Q.res:Purchase_Order_Number_SelectedBG = -1
  SELF.Q.res:Previous_Sale_Number_NormalFG = -1
  SELF.Q.res:Previous_Sale_Number_NormalBG = -1
  SELF.Q.res:Previous_Sale_Number_SelectedFG = -1
  SELF.Q.res:Previous_Sale_Number_SelectedBG = -1
  SELF.Q.tmp:BrowseType_NormalFG = -1
  SELF.Q.tmp:BrowseType_NormalBG = -1
  SELF.Q.tmp:BrowseType_SelectedFG = -1
  SELF.Q.tmp:BrowseType_SelectedBG = -1
  tmp:BrowseType = 0
  If res:Despatched = 'PEN'
      tmp:BrowseType = 1 !Green
  End!If res:Despatched = 'PEN'
  If res:Despatched = 'ORD'
      tmp:BrowseType = 2 !Red
  End!If res:Despatched = 'ORD'
  If res:Despatched = 'OLD'
      tmp:BrowseType = 3 !Gray
  End!If res:Despatched = 'OLD'
  If res:Despatched = 'CAN'
      tmp:BrowseType = 4 !Black, black!
  End !If res:Despatched = 'CAN'
  If res:Despatched = 'PIK'
      tmp:BrowseType = 5 !Blue
  End !If res:Despatched = 'PIK'
  
   
   
   IF (tmp:BrowseType = 0)
     SELF.Q.tmp:tag_NormalFG = 0
     SELF.Q.tmp:tag_NormalBG = 16777215
     SELF.Q.tmp:tag_SelectedFG = 16777215
     SELF.Q.tmp:tag_SelectedBG = 8388608
   ELSIF(tmp:BrowseType = 1)
     SELF.Q.tmp:tag_NormalFG = 32768
     SELF.Q.tmp:tag_NormalBG = 16777215
     SELF.Q.tmp:tag_SelectedFG = 16777215
     SELF.Q.tmp:tag_SelectedBG = 32768
   ELSIF(tmp:BrowseType = 2)
     SELF.Q.tmp:tag_NormalFG = 255
     SELF.Q.tmp:tag_NormalBG = 16777215
     SELF.Q.tmp:tag_SelectedFG = 16777215
     SELF.Q.tmp:tag_SelectedBG = 255
   ELSIF(tmp:BrowseType = 3)
     SELF.Q.tmp:tag_NormalFG = 8421504
     SELF.Q.tmp:tag_NormalBG = 16777215
     SELF.Q.tmp:tag_SelectedFG = 16777215
     SELF.Q.tmp:tag_SelectedBG = 8421504
   ELSIF(tmp:BrowseType = 4)
     SELF.Q.tmp:tag_NormalFG = 16777215
     SELF.Q.tmp:tag_NormalBG = 0
     SELF.Q.tmp:tag_SelectedFG = 16777215
     SELF.Q.tmp:tag_SelectedBG = 0
   ELSIF(tmp:BrowseType = 5)
     SELF.Q.tmp:tag_NormalFG = 8388736
     SELF.Q.tmp:tag_NormalBG = 16777215
     SELF.Q.tmp:tag_SelectedFG = 16777215
     SELF.Q.tmp:tag_SelectedBG = 8388736
   ELSE
     SELF.Q.tmp:tag_NormalFG = 0
     SELF.Q.tmp:tag_NormalBG = 16777215
     SELF.Q.tmp:tag_SelectedFG = 16777215
     SELF.Q.tmp:tag_SelectedBG = 8388608
   END
   IF (tmp:BrowseType = 0)
     SELF.Q.res:Part_Number_NormalFG = 0
     SELF.Q.res:Part_Number_NormalBG = 16777215
     SELF.Q.res:Part_Number_SelectedFG = 16777215
     SELF.Q.res:Part_Number_SelectedBG = 8388608
   ELSIF(tmp:BrowseType = 1)
     SELF.Q.res:Part_Number_NormalFG = 32768
     SELF.Q.res:Part_Number_NormalBG = 16777215
     SELF.Q.res:Part_Number_SelectedFG = 16777215
     SELF.Q.res:Part_Number_SelectedBG = 32768
   ELSIF(tmp:BrowseType = 2)
     SELF.Q.res:Part_Number_NormalFG = 255
     SELF.Q.res:Part_Number_NormalBG = 16777215
     SELF.Q.res:Part_Number_SelectedFG = 16777215
     SELF.Q.res:Part_Number_SelectedBG = 255
   ELSIF(tmp:BrowseType = 3)
     SELF.Q.res:Part_Number_NormalFG = 8421504
     SELF.Q.res:Part_Number_NormalBG = 16777215
     SELF.Q.res:Part_Number_SelectedFG = 16777215
     SELF.Q.res:Part_Number_SelectedBG = 8421504
   ELSIF(tmp:BrowseType = 4)
     SELF.Q.res:Part_Number_NormalFG = 16777215
     SELF.Q.res:Part_Number_NormalBG = 0
     SELF.Q.res:Part_Number_SelectedFG = 16777215
     SELF.Q.res:Part_Number_SelectedBG = 0
   ELSIF(tmp:BrowseType = 5)
     SELF.Q.res:Part_Number_NormalFG = 8388736
     SELF.Q.res:Part_Number_NormalBG = 16777215
     SELF.Q.res:Part_Number_SelectedFG = 16777215
     SELF.Q.res:Part_Number_SelectedBG = 8388736
   ELSE
     SELF.Q.res:Part_Number_NormalFG = 0
     SELF.Q.res:Part_Number_NormalBG = 16777215
     SELF.Q.res:Part_Number_SelectedFG = 16777215
     SELF.Q.res:Part_Number_SelectedBG = 8388608
   END
   IF (tmp:BrowseType = 0)
     SELF.Q.res:Description_NormalFG = 0
     SELF.Q.res:Description_NormalBG = 16777215
     SELF.Q.res:Description_SelectedFG = 16777215
     SELF.Q.res:Description_SelectedBG = 8388608
   ELSIF(tmp:BrowseType = 1)
     SELF.Q.res:Description_NormalFG = 32768
     SELF.Q.res:Description_NormalBG = 16777215
     SELF.Q.res:Description_SelectedFG = 16777215
     SELF.Q.res:Description_SelectedBG = 32768
   ELSIF(tmp:BrowseType = 2)
     SELF.Q.res:Description_NormalFG = 255
     SELF.Q.res:Description_NormalBG = 16777215
     SELF.Q.res:Description_SelectedFG = 16777215
     SELF.Q.res:Description_SelectedBG = 255
   ELSIF(tmp:BrowseType = 3)
     SELF.Q.res:Description_NormalFG = 8421504
     SELF.Q.res:Description_NormalBG = 16777215
     SELF.Q.res:Description_SelectedFG = 16777215
     SELF.Q.res:Description_SelectedBG = 8421504
   ELSIF(tmp:BrowseType = 4)
     SELF.Q.res:Description_NormalFG = 16777215
     SELF.Q.res:Description_NormalBG = 0
     SELF.Q.res:Description_SelectedFG = 16777215
     SELF.Q.res:Description_SelectedBG = 0
   ELSIF(tmp:BrowseType = 5)
     SELF.Q.res:Description_NormalFG = 8388736
     SELF.Q.res:Description_NormalBG = 16777215
     SELF.Q.res:Description_SelectedFG = 16777215
     SELF.Q.res:Description_SelectedBG = 8388736
   ELSE
     SELF.Q.res:Description_NormalFG = 0
     SELF.Q.res:Description_NormalBG = 16777215
     SELF.Q.res:Description_SelectedFG = 16777215
     SELF.Q.res:Description_SelectedBG = 8388608
   END
   IF (tmp:BrowseType = 0)
     SELF.Q.res:Quantity_NormalFG = 0
     SELF.Q.res:Quantity_NormalBG = 16777215
     SELF.Q.res:Quantity_SelectedFG = 16777215
     SELF.Q.res:Quantity_SelectedBG = 8388608
   ELSIF(tmp:BrowseType = 1)
     SELF.Q.res:Quantity_NormalFG = 32768
     SELF.Q.res:Quantity_NormalBG = 16777215
     SELF.Q.res:Quantity_SelectedFG = 16777215
     SELF.Q.res:Quantity_SelectedBG = 32768
   ELSIF(tmp:BrowseType = 2)
     SELF.Q.res:Quantity_NormalFG = 255
     SELF.Q.res:Quantity_NormalBG = 16777215
     SELF.Q.res:Quantity_SelectedFG = 16777215
     SELF.Q.res:Quantity_SelectedBG = 255
   ELSIF(tmp:BrowseType = 3)
     SELF.Q.res:Quantity_NormalFG = 8421504
     SELF.Q.res:Quantity_NormalBG = 16777215
     SELF.Q.res:Quantity_SelectedFG = 16777215
     SELF.Q.res:Quantity_SelectedBG = 8421504
   ELSIF(tmp:BrowseType = 4)
     SELF.Q.res:Quantity_NormalFG = 16777215
     SELF.Q.res:Quantity_NormalBG = 0
     SELF.Q.res:Quantity_SelectedFG = 16777215
     SELF.Q.res:Quantity_SelectedBG = 0
   ELSIF(tmp:BrowseType = 5)
     SELF.Q.res:Quantity_NormalFG = 8388736
     SELF.Q.res:Quantity_NormalBG = 16777215
     SELF.Q.res:Quantity_SelectedFG = 16777215
     SELF.Q.res:Quantity_SelectedBG = 8388736
   ELSE
     SELF.Q.res:Quantity_NormalFG = 0
     SELF.Q.res:Quantity_NormalBG = 16777215
     SELF.Q.res:Quantity_SelectedFG = 16777215
     SELF.Q.res:Quantity_SelectedBG = 8388608
   END
   IF (tmp:BrowseType = 0)
     SELF.Q.res:Purchase_Order_Number_NormalFG = 0
     SELF.Q.res:Purchase_Order_Number_NormalBG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedFG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedBG = 8388608
   ELSIF(tmp:BrowseType = 1)
     SELF.Q.res:Purchase_Order_Number_NormalFG = 32768
     SELF.Q.res:Purchase_Order_Number_NormalBG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedFG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedBG = 32768
   ELSIF(tmp:BrowseType = 2)
     SELF.Q.res:Purchase_Order_Number_NormalFG = 255
     SELF.Q.res:Purchase_Order_Number_NormalBG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedFG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedBG = 255
   ELSIF(tmp:BrowseType = 3)
     SELF.Q.res:Purchase_Order_Number_NormalFG = 8421504
     SELF.Q.res:Purchase_Order_Number_NormalBG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedFG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedBG = 8421504
   ELSIF(tmp:BrowseType = 4)
     SELF.Q.res:Purchase_Order_Number_NormalFG = 16777215
     SELF.Q.res:Purchase_Order_Number_NormalBG = 0
     SELF.Q.res:Purchase_Order_Number_SelectedFG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedBG = 0
   ELSIF(tmp:BrowseType = 5)
     SELF.Q.res:Purchase_Order_Number_NormalFG = 8388736
     SELF.Q.res:Purchase_Order_Number_NormalBG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedFG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedBG = 8388736
   ELSE
     SELF.Q.res:Purchase_Order_Number_NormalFG = 0
     SELF.Q.res:Purchase_Order_Number_NormalBG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedFG = 16777215
     SELF.Q.res:Purchase_Order_Number_SelectedBG = 8388608
   END
   IF (tmp:BrowseType = 0)
     SELF.Q.res:Previous_Sale_Number_NormalFG = 0
     SELF.Q.res:Previous_Sale_Number_NormalBG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedFG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedBG = 8388608
   ELSIF(tmp:BrowseType = 1)
     SELF.Q.res:Previous_Sale_Number_NormalFG = 32768
     SELF.Q.res:Previous_Sale_Number_NormalBG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedFG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedBG = 32768
   ELSIF(tmp:BrowseType = 2)
     SELF.Q.res:Previous_Sale_Number_NormalFG = 255
     SELF.Q.res:Previous_Sale_Number_NormalBG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedFG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedBG = 255
   ELSIF(tmp:BrowseType = 3)
     SELF.Q.res:Previous_Sale_Number_NormalFG = 8421504
     SELF.Q.res:Previous_Sale_Number_NormalBG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedFG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedBG = 8421504
   ELSIF(tmp:BrowseType = 4)
     SELF.Q.res:Previous_Sale_Number_NormalFG = 16777215
     SELF.Q.res:Previous_Sale_Number_NormalBG = 0
     SELF.Q.res:Previous_Sale_Number_SelectedFG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedBG = 0
   ELSIF(tmp:BrowseType = 5)
     SELF.Q.res:Previous_Sale_Number_NormalFG = 8388736
     SELF.Q.res:Previous_Sale_Number_NormalBG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedFG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedBG = 8388736
   ELSE
     SELF.Q.res:Previous_Sale_Number_NormalFG = 0
     SELF.Q.res:Previous_Sale_Number_NormalBG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedFG = 16777215
     SELF.Q.res:Previous_Sale_Number_SelectedBG = 8388608
   END
   IF (tmp:BrowseType = 0)
     SELF.Q.tmp:BrowseType_NormalFG = 0
     SELF.Q.tmp:BrowseType_NormalBG = 16777215
     SELF.Q.tmp:BrowseType_SelectedFG = 16777215
     SELF.Q.tmp:BrowseType_SelectedBG = 8388608
   ELSIF(tmp:BrowseType = 1)
     SELF.Q.tmp:BrowseType_NormalFG = 32768
     SELF.Q.tmp:BrowseType_NormalBG = 16777215
     SELF.Q.tmp:BrowseType_SelectedFG = 16777215
     SELF.Q.tmp:BrowseType_SelectedBG = 32768
   ELSIF(tmp:BrowseType = 2)
     SELF.Q.tmp:BrowseType_NormalFG = 255
     SELF.Q.tmp:BrowseType_NormalBG = 16777215
     SELF.Q.tmp:BrowseType_SelectedFG = 16777215
     SELF.Q.tmp:BrowseType_SelectedBG = 255
   ELSIF(tmp:BrowseType = 3)
     SELF.Q.tmp:BrowseType_NormalFG = 8421504
     SELF.Q.tmp:BrowseType_NormalBG = 16777215
     SELF.Q.tmp:BrowseType_SelectedFG = 16777215
     SELF.Q.tmp:BrowseType_SelectedBG = 8421504
   ELSIF(tmp:BrowseType = 4)
     SELF.Q.tmp:BrowseType_NormalFG = 16777215
     SELF.Q.tmp:BrowseType_NormalBG = 0
     SELF.Q.tmp:BrowseType_SelectedFG = 16777215
     SELF.Q.tmp:BrowseType_SelectedBG = 0
   ELSIF(tmp:BrowseType = 5)
     SELF.Q.tmp:BrowseType_NormalFG = 8388736
     SELF.Q.tmp:BrowseType_NormalBG = 16777215
     SELF.Q.tmp:BrowseType_SelectedFG = 16777215
     SELF.Q.tmp:BrowseType_SelectedBG = 8388736
   ELSE
     SELF.Q.tmp:BrowseType_NormalFG = 0
     SELF.Q.tmp:BrowseType_NormalBG = 16777215
     SELF.Q.tmp:BrowseType_SelectedFG = 16777215
     SELF.Q.tmp:BrowseType_SelectedBG = 8388608
   END
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(3, SetQueueRecord, ())


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW3.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW3::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW3::RecordStatus=ReturnValue
  IF BRW3::RecordStatus NOT=Record:OK THEN RETURN BRW3::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = res:Record_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::9:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW3::RecordStatus
  RETURN ReturnValue

