

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01019.INC'),ONCE        !Local module procedure declarations
                     END


UpdateRETSALES PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::ret:Record  LIKE(ret:RECORD),STATIC
QuickWindow          WINDOW('Update the RETSALES File'),AT(,,358,182),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateRETSALES'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,350,156),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Sales Number'),AT(8,20),USE(?RET:Ref_Number:Prompt),TRN
                           ENTRY(@s8),AT(100,20,40,10),USE(ret:Ref_Number),RIGHT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('who booked'),AT(8,34),USE(?RET:who_booked:Prompt)
                           ENTRY(@s3),AT(100,34,40,10),USE(ret:who_booked),COLOR(COLOR:Yellow),UPR
                           PROMPT('date booked:'),AT(8,48),USE(?RET:date_booked:Prompt)
                           ENTRY(@d6b),AT(100,48,104,10),USE(ret:date_booked),COLOR(COLOR:Yellow)
                           PROMPT('time booked:'),AT(8,62),USE(?RET:time_booked:Prompt)
                           ENTRY(@t1),AT(100,62,104,10),USE(ret:time_booked)
                           PROMPT('Account Number'),AT(8,76),USE(?RET:Account_Number:Prompt),TRN
                           ENTRY(@s15),AT(100,76,64,10),USE(ret:Account_Number),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Yellow,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Contact Name'),AT(8,90),USE(?RET:Contact_Name:Prompt),TRN
                           ENTRY(@s30),AT(100,90,124,10),USE(ret:Contact_Name),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Purchase Order Number'),AT(8,104),USE(?RET:Purchase_Order_Number:Prompt),TRN
                           ENTRY(@s30),AT(100,104,124,10),USE(ret:Purchase_Order_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Delivery Or Collection'),AT(8,118),USE(?RET:Delivery_Collection:Prompt)
                           ENTRY(@s3),AT(100,118,40,10),USE(ret:Delivery_Collection)
                           PROMPT('Payment Method'),AT(8,132),USE(?RET:Payment_Method:Prompt)
                           ENTRY(@s3),AT(100,132,40,10),USE(ret:Payment_Method)
                           PROMPT('Courier Cost'),AT(8,146),USE(?RET:Courier_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(100,146,60,10),USE(ret:Courier_Cost),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('General (cont.)'),USE(?Tab:2)
                           PROMPT('Courier Cost'),AT(8,20),USE(?RET:Parts_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(100,20,60,10),USE(ret:Parts_Cost),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Courier Cost'),AT(8,34),USE(?RET:Sub_Total:Prompt),TRN
                           ENTRY(@n14.2),AT(100,34,60,10),USE(ret:Sub_Total),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Courier'),AT(8,48),USE(?RET:Courier:Prompt),TRN
                           ENTRY(@s30),AT(100,48,124,10),USE(ret:Courier),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Consignment Number'),AT(8,62),USE(?RET:Consignment_Number:Prompt),TRN
                           ENTRY(@s30),AT(100,62,124,10),USE(ret:Consignment_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Date Despatched'),AT(8,76),USE(?RET:Date_Despatched:Prompt),TRN
                           ENTRY(@d6b),AT(100,76,104,10),USE(ret:Date_Despatched),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Despatched'),AT(8,90),USE(?RET:Despatched:Prompt)
                           ENTRY(@s3),AT(100,90,40,10),USE(ret:Despatched),UPR
                           PROMPT('Despatch_Number'),AT(8,104),USE(?RET:Despatch_Number:Prompt)
                           ENTRY(@s8),AT(100,104,40,10),USE(ret:Despatch_Number),RIGHT(1),UPR
                           PROMPT('Invoice Number'),AT(8,118),USE(?RET:Invoice_Number:Prompt),TRN
                           ENTRY(@s8),AT(100,118,40,10),USE(ret:Invoice_Number),RIGHT(1),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Invoice Date'),AT(8,132),USE(?RET:Invoice_Date:Prompt),TRN
                           ENTRY(@d6b),AT(100,132,104,10),USE(ret:Invoice_Date),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Courier Cost'),AT(8,146),USE(?RET:Invoice_Courier_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(100,146,60,10),USE(ret:Invoice_Courier_Cost),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('General (cont. 2)'),USE(?Tab:3)
                           PROMPT('Parts Cost'),AT(8,20),USE(?RET:Invoice_Parts_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(100,20,60,10),USE(ret:Invoice_Parts_Cost),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Sub Total'),AT(8,34),USE(?RET:Invoice_Sub_Total:Prompt),TRN
                           ENTRY(@n14.2),AT(100,34,60,10),USE(ret:Invoice_Sub_Total),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Postcode'),AT(8,48),USE(?RET:Postcode:Prompt),TRN
                           ENTRY(@s10),AT(100,48,44,10),USE(ret:Postcode),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Company Name'),AT(8,62),USE(?RET:Company_Name:Prompt),TRN
                           ENTRY(@s30),AT(100,62,124,10),USE(ret:Company_Name),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Building Name/No'),AT(8,76),USE(?RET:Building_Name:Prompt),TRN
                           ENTRY(@s30),AT(100,76,124,10),USE(ret:Building_Name),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Address'),AT(8,90),USE(?RET:Address_Line1:Prompt),TRN
                           ENTRY(@s30),AT(100,90,124,10),USE(ret:Address_Line1),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('DELETE THIS'),AT(8,104),USE(?RET:Address_Line2:Prompt)
                           ENTRY(@s30),AT(100,104,124,10),USE(ret:Address_Line2),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('DELETE THIS'),AT(8,118),USE(?RET:Address_Line3:Prompt)
                           ENTRY(@s30),AT(100,118,124,10),USE(ret:Address_Line3),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Telephone Number'),AT(8,132),USE(?RET:Telephone_Number:Prompt),TRN
                           ENTRY(@s15),AT(100,132,64,10),USE(ret:Telephone_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Fax Number'),AT(8,146),USE(?RET:Fax_Number:Prompt),TRN
                           ENTRY(@s15),AT(100,146,64,10),USE(ret:Fax_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('General (cont. 3)'),USE(?Tab:4)
                           PROMPT('Postcode'),AT(8,20),USE(?RET:Postcode_Delivery:Prompt),TRN,LEFT
                           ENTRY(@s10),AT(100,20,44,10),USE(ret:Postcode_Delivery),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Company Name'),AT(8,34),USE(?RET:Company_Name_Delivery:Prompt),TRN
                           ENTRY(@s30),AT(100,34,124,10),USE(ret:Company_Name_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Building Name/No'),AT(8,48),USE(?RET:Building_Name_Delivery:Prompt),TRN
                           ENTRY(@s30),AT(100,48,124,10),USE(ret:Building_Name_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Address'),AT(8,62),USE(?RET:Address_Line1_Delivery:Prompt)
                           ENTRY(@s30),AT(100,62,124,10),USE(ret:Address_Line1_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('DELETE THIS'),AT(8,76),USE(?RET:Address_Line2_Delivery:Prompt)
                           ENTRY(@s30),AT(100,76,124,10),USE(ret:Address_Line2_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('DELETE THIS'),AT(8,90),USE(?RET:Address_Line3_Delivery:Prompt)
                           ENTRY(@s30),AT(100,90,124,10),USE(ret:Address_Line3_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Telephone Number'),AT(8,104),USE(?RET:Telephone_Delivery:Prompt),TRN
                           ENTRY(@s15),AT(100,104,64,10),USE(ret:Telephone_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Fax Number'),AT(8,118),USE(?RET:Fax_Number_Delivery:Prompt),TRN
                           ENTRY(@s15),AT(100,118,64,10),USE(ret:Fax_Number_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Delivery Text'),AT(8,132),USE(?RET:Delivery_Text:Prompt)
                           ENTRY(@s255),AT(100,132,250,10),USE(ret:Delivery_Text),UPR
                           PROMPT('Invoice Text:'),AT(8,146),USE(?RET:Invoice_Text:Prompt)
                           ENTRY(@s255),AT(100,146,250,10),USE(ret:Invoice_Text)
                         END
                       END
                       BUTTON('OK'),AT(260,164,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(309,164,45,14),USE(?Cancel)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?RET:Ref_Number:Prompt{prop:FontColor} = -1
    ?RET:Ref_Number:Prompt{prop:Color} = 15066597
    If ?ret:Ref_Number{prop:ReadOnly} = True
        ?ret:Ref_Number{prop:FontColor} = 65793
        ?ret:Ref_Number{prop:Color} = 15066597
    Elsif ?ret:Ref_Number{prop:Req} = True
        ?ret:Ref_Number{prop:FontColor} = 65793
        ?ret:Ref_Number{prop:Color} = 8454143
    Else ! If ?ret:Ref_Number{prop:Req} = True
        ?ret:Ref_Number{prop:FontColor} = 65793
        ?ret:Ref_Number{prop:Color} = 16777215
    End ! If ?ret:Ref_Number{prop:Req} = True
    ?ret:Ref_Number{prop:Trn} = 0
    ?ret:Ref_Number{prop:FontStyle} = font:Bold
    ?RET:who_booked:Prompt{prop:FontColor} = -1
    ?RET:who_booked:Prompt{prop:Color} = 15066597
    If ?ret:who_booked{prop:ReadOnly} = True
        ?ret:who_booked{prop:FontColor} = 65793
        ?ret:who_booked{prop:Color} = 15066597
    Elsif ?ret:who_booked{prop:Req} = True
        ?ret:who_booked{prop:FontColor} = 65793
        ?ret:who_booked{prop:Color} = 8454143
    Else ! If ?ret:who_booked{prop:Req} = True
        ?ret:who_booked{prop:FontColor} = 65793
        ?ret:who_booked{prop:Color} = 16777215
    End ! If ?ret:who_booked{prop:Req} = True
    ?ret:who_booked{prop:Trn} = 0
    ?ret:who_booked{prop:FontStyle} = font:Bold
    ?RET:date_booked:Prompt{prop:FontColor} = -1
    ?RET:date_booked:Prompt{prop:Color} = 15066597
    If ?ret:date_booked{prop:ReadOnly} = True
        ?ret:date_booked{prop:FontColor} = 65793
        ?ret:date_booked{prop:Color} = 15066597
    Elsif ?ret:date_booked{prop:Req} = True
        ?ret:date_booked{prop:FontColor} = 65793
        ?ret:date_booked{prop:Color} = 8454143
    Else ! If ?ret:date_booked{prop:Req} = True
        ?ret:date_booked{prop:FontColor} = 65793
        ?ret:date_booked{prop:Color} = 16777215
    End ! If ?ret:date_booked{prop:Req} = True
    ?ret:date_booked{prop:Trn} = 0
    ?ret:date_booked{prop:FontStyle} = font:Bold
    ?RET:time_booked:Prompt{prop:FontColor} = -1
    ?RET:time_booked:Prompt{prop:Color} = 15066597
    If ?ret:time_booked{prop:ReadOnly} = True
        ?ret:time_booked{prop:FontColor} = 65793
        ?ret:time_booked{prop:Color} = 15066597
    Elsif ?ret:time_booked{prop:Req} = True
        ?ret:time_booked{prop:FontColor} = 65793
        ?ret:time_booked{prop:Color} = 8454143
    Else ! If ?ret:time_booked{prop:Req} = True
        ?ret:time_booked{prop:FontColor} = 65793
        ?ret:time_booked{prop:Color} = 16777215
    End ! If ?ret:time_booked{prop:Req} = True
    ?ret:time_booked{prop:Trn} = 0
    ?ret:time_booked{prop:FontStyle} = font:Bold
    ?RET:Account_Number:Prompt{prop:FontColor} = -1
    ?RET:Account_Number:Prompt{prop:Color} = 15066597
    If ?ret:Account_Number{prop:ReadOnly} = True
        ?ret:Account_Number{prop:FontColor} = 65793
        ?ret:Account_Number{prop:Color} = 15066597
    Elsif ?ret:Account_Number{prop:Req} = True
        ?ret:Account_Number{prop:FontColor} = 65793
        ?ret:Account_Number{prop:Color} = 8454143
    Else ! If ?ret:Account_Number{prop:Req} = True
        ?ret:Account_Number{prop:FontColor} = 65793
        ?ret:Account_Number{prop:Color} = 16777215
    End ! If ?ret:Account_Number{prop:Req} = True
    ?ret:Account_Number{prop:Trn} = 0
    ?ret:Account_Number{prop:FontStyle} = font:Bold
    ?RET:Contact_Name:Prompt{prop:FontColor} = -1
    ?RET:Contact_Name:Prompt{prop:Color} = 15066597
    If ?ret:Contact_Name{prop:ReadOnly} = True
        ?ret:Contact_Name{prop:FontColor} = 65793
        ?ret:Contact_Name{prop:Color} = 15066597
    Elsif ?ret:Contact_Name{prop:Req} = True
        ?ret:Contact_Name{prop:FontColor} = 65793
        ?ret:Contact_Name{prop:Color} = 8454143
    Else ! If ?ret:Contact_Name{prop:Req} = True
        ?ret:Contact_Name{prop:FontColor} = 65793
        ?ret:Contact_Name{prop:Color} = 16777215
    End ! If ?ret:Contact_Name{prop:Req} = True
    ?ret:Contact_Name{prop:Trn} = 0
    ?ret:Contact_Name{prop:FontStyle} = font:Bold
    ?RET:Purchase_Order_Number:Prompt{prop:FontColor} = -1
    ?RET:Purchase_Order_Number:Prompt{prop:Color} = 15066597
    If ?ret:Purchase_Order_Number{prop:ReadOnly} = True
        ?ret:Purchase_Order_Number{prop:FontColor} = 65793
        ?ret:Purchase_Order_Number{prop:Color} = 15066597
    Elsif ?ret:Purchase_Order_Number{prop:Req} = True
        ?ret:Purchase_Order_Number{prop:FontColor} = 65793
        ?ret:Purchase_Order_Number{prop:Color} = 8454143
    Else ! If ?ret:Purchase_Order_Number{prop:Req} = True
        ?ret:Purchase_Order_Number{prop:FontColor} = 65793
        ?ret:Purchase_Order_Number{prop:Color} = 16777215
    End ! If ?ret:Purchase_Order_Number{prop:Req} = True
    ?ret:Purchase_Order_Number{prop:Trn} = 0
    ?ret:Purchase_Order_Number{prop:FontStyle} = font:Bold
    ?RET:Delivery_Collection:Prompt{prop:FontColor} = -1
    ?RET:Delivery_Collection:Prompt{prop:Color} = 15066597
    If ?ret:Delivery_Collection{prop:ReadOnly} = True
        ?ret:Delivery_Collection{prop:FontColor} = 65793
        ?ret:Delivery_Collection{prop:Color} = 15066597
    Elsif ?ret:Delivery_Collection{prop:Req} = True
        ?ret:Delivery_Collection{prop:FontColor} = 65793
        ?ret:Delivery_Collection{prop:Color} = 8454143
    Else ! If ?ret:Delivery_Collection{prop:Req} = True
        ?ret:Delivery_Collection{prop:FontColor} = 65793
        ?ret:Delivery_Collection{prop:Color} = 16777215
    End ! If ?ret:Delivery_Collection{prop:Req} = True
    ?ret:Delivery_Collection{prop:Trn} = 0
    ?ret:Delivery_Collection{prop:FontStyle} = font:Bold
    ?RET:Payment_Method:Prompt{prop:FontColor} = -1
    ?RET:Payment_Method:Prompt{prop:Color} = 15066597
    If ?ret:Payment_Method{prop:ReadOnly} = True
        ?ret:Payment_Method{prop:FontColor} = 65793
        ?ret:Payment_Method{prop:Color} = 15066597
    Elsif ?ret:Payment_Method{prop:Req} = True
        ?ret:Payment_Method{prop:FontColor} = 65793
        ?ret:Payment_Method{prop:Color} = 8454143
    Else ! If ?ret:Payment_Method{prop:Req} = True
        ?ret:Payment_Method{prop:FontColor} = 65793
        ?ret:Payment_Method{prop:Color} = 16777215
    End ! If ?ret:Payment_Method{prop:Req} = True
    ?ret:Payment_Method{prop:Trn} = 0
    ?ret:Payment_Method{prop:FontStyle} = font:Bold
    ?RET:Courier_Cost:Prompt{prop:FontColor} = -1
    ?RET:Courier_Cost:Prompt{prop:Color} = 15066597
    If ?ret:Courier_Cost{prop:ReadOnly} = True
        ?ret:Courier_Cost{prop:FontColor} = 65793
        ?ret:Courier_Cost{prop:Color} = 15066597
    Elsif ?ret:Courier_Cost{prop:Req} = True
        ?ret:Courier_Cost{prop:FontColor} = 65793
        ?ret:Courier_Cost{prop:Color} = 8454143
    Else ! If ?ret:Courier_Cost{prop:Req} = True
        ?ret:Courier_Cost{prop:FontColor} = 65793
        ?ret:Courier_Cost{prop:Color} = 16777215
    End ! If ?ret:Courier_Cost{prop:Req} = True
    ?ret:Courier_Cost{prop:Trn} = 0
    ?ret:Courier_Cost{prop:FontStyle} = font:Bold
    ?Tab:2{prop:Color} = 15066597
    ?RET:Parts_Cost:Prompt{prop:FontColor} = -1
    ?RET:Parts_Cost:Prompt{prop:Color} = 15066597
    If ?ret:Parts_Cost{prop:ReadOnly} = True
        ?ret:Parts_Cost{prop:FontColor} = 65793
        ?ret:Parts_Cost{prop:Color} = 15066597
    Elsif ?ret:Parts_Cost{prop:Req} = True
        ?ret:Parts_Cost{prop:FontColor} = 65793
        ?ret:Parts_Cost{prop:Color} = 8454143
    Else ! If ?ret:Parts_Cost{prop:Req} = True
        ?ret:Parts_Cost{prop:FontColor} = 65793
        ?ret:Parts_Cost{prop:Color} = 16777215
    End ! If ?ret:Parts_Cost{prop:Req} = True
    ?ret:Parts_Cost{prop:Trn} = 0
    ?ret:Parts_Cost{prop:FontStyle} = font:Bold
    ?RET:Sub_Total:Prompt{prop:FontColor} = -1
    ?RET:Sub_Total:Prompt{prop:Color} = 15066597
    If ?ret:Sub_Total{prop:ReadOnly} = True
        ?ret:Sub_Total{prop:FontColor} = 65793
        ?ret:Sub_Total{prop:Color} = 15066597
    Elsif ?ret:Sub_Total{prop:Req} = True
        ?ret:Sub_Total{prop:FontColor} = 65793
        ?ret:Sub_Total{prop:Color} = 8454143
    Else ! If ?ret:Sub_Total{prop:Req} = True
        ?ret:Sub_Total{prop:FontColor} = 65793
        ?ret:Sub_Total{prop:Color} = 16777215
    End ! If ?ret:Sub_Total{prop:Req} = True
    ?ret:Sub_Total{prop:Trn} = 0
    ?ret:Sub_Total{prop:FontStyle} = font:Bold
    ?RET:Courier:Prompt{prop:FontColor} = -1
    ?RET:Courier:Prompt{prop:Color} = 15066597
    If ?ret:Courier{prop:ReadOnly} = True
        ?ret:Courier{prop:FontColor} = 65793
        ?ret:Courier{prop:Color} = 15066597
    Elsif ?ret:Courier{prop:Req} = True
        ?ret:Courier{prop:FontColor} = 65793
        ?ret:Courier{prop:Color} = 8454143
    Else ! If ?ret:Courier{prop:Req} = True
        ?ret:Courier{prop:FontColor} = 65793
        ?ret:Courier{prop:Color} = 16777215
    End ! If ?ret:Courier{prop:Req} = True
    ?ret:Courier{prop:Trn} = 0
    ?ret:Courier{prop:FontStyle} = font:Bold
    ?RET:Consignment_Number:Prompt{prop:FontColor} = -1
    ?RET:Consignment_Number:Prompt{prop:Color} = 15066597
    If ?ret:Consignment_Number{prop:ReadOnly} = True
        ?ret:Consignment_Number{prop:FontColor} = 65793
        ?ret:Consignment_Number{prop:Color} = 15066597
    Elsif ?ret:Consignment_Number{prop:Req} = True
        ?ret:Consignment_Number{prop:FontColor} = 65793
        ?ret:Consignment_Number{prop:Color} = 8454143
    Else ! If ?ret:Consignment_Number{prop:Req} = True
        ?ret:Consignment_Number{prop:FontColor} = 65793
        ?ret:Consignment_Number{prop:Color} = 16777215
    End ! If ?ret:Consignment_Number{prop:Req} = True
    ?ret:Consignment_Number{prop:Trn} = 0
    ?ret:Consignment_Number{prop:FontStyle} = font:Bold
    ?RET:Date_Despatched:Prompt{prop:FontColor} = -1
    ?RET:Date_Despatched:Prompt{prop:Color} = 15066597
    If ?ret:Date_Despatched{prop:ReadOnly} = True
        ?ret:Date_Despatched{prop:FontColor} = 65793
        ?ret:Date_Despatched{prop:Color} = 15066597
    Elsif ?ret:Date_Despatched{prop:Req} = True
        ?ret:Date_Despatched{prop:FontColor} = 65793
        ?ret:Date_Despatched{prop:Color} = 8454143
    Else ! If ?ret:Date_Despatched{prop:Req} = True
        ?ret:Date_Despatched{prop:FontColor} = 65793
        ?ret:Date_Despatched{prop:Color} = 16777215
    End ! If ?ret:Date_Despatched{prop:Req} = True
    ?ret:Date_Despatched{prop:Trn} = 0
    ?ret:Date_Despatched{prop:FontStyle} = font:Bold
    ?RET:Despatched:Prompt{prop:FontColor} = -1
    ?RET:Despatched:Prompt{prop:Color} = 15066597
    If ?ret:Despatched{prop:ReadOnly} = True
        ?ret:Despatched{prop:FontColor} = 65793
        ?ret:Despatched{prop:Color} = 15066597
    Elsif ?ret:Despatched{prop:Req} = True
        ?ret:Despatched{prop:FontColor} = 65793
        ?ret:Despatched{prop:Color} = 8454143
    Else ! If ?ret:Despatched{prop:Req} = True
        ?ret:Despatched{prop:FontColor} = 65793
        ?ret:Despatched{prop:Color} = 16777215
    End ! If ?ret:Despatched{prop:Req} = True
    ?ret:Despatched{prop:Trn} = 0
    ?ret:Despatched{prop:FontStyle} = font:Bold
    ?RET:Despatch_Number:Prompt{prop:FontColor} = -1
    ?RET:Despatch_Number:Prompt{prop:Color} = 15066597
    If ?ret:Despatch_Number{prop:ReadOnly} = True
        ?ret:Despatch_Number{prop:FontColor} = 65793
        ?ret:Despatch_Number{prop:Color} = 15066597
    Elsif ?ret:Despatch_Number{prop:Req} = True
        ?ret:Despatch_Number{prop:FontColor} = 65793
        ?ret:Despatch_Number{prop:Color} = 8454143
    Else ! If ?ret:Despatch_Number{prop:Req} = True
        ?ret:Despatch_Number{prop:FontColor} = 65793
        ?ret:Despatch_Number{prop:Color} = 16777215
    End ! If ?ret:Despatch_Number{prop:Req} = True
    ?ret:Despatch_Number{prop:Trn} = 0
    ?ret:Despatch_Number{prop:FontStyle} = font:Bold
    ?RET:Invoice_Number:Prompt{prop:FontColor} = -1
    ?RET:Invoice_Number:Prompt{prop:Color} = 15066597
    If ?ret:Invoice_Number{prop:ReadOnly} = True
        ?ret:Invoice_Number{prop:FontColor} = 65793
        ?ret:Invoice_Number{prop:Color} = 15066597
    Elsif ?ret:Invoice_Number{prop:Req} = True
        ?ret:Invoice_Number{prop:FontColor} = 65793
        ?ret:Invoice_Number{prop:Color} = 8454143
    Else ! If ?ret:Invoice_Number{prop:Req} = True
        ?ret:Invoice_Number{prop:FontColor} = 65793
        ?ret:Invoice_Number{prop:Color} = 16777215
    End ! If ?ret:Invoice_Number{prop:Req} = True
    ?ret:Invoice_Number{prop:Trn} = 0
    ?ret:Invoice_Number{prop:FontStyle} = font:Bold
    ?RET:Invoice_Date:Prompt{prop:FontColor} = -1
    ?RET:Invoice_Date:Prompt{prop:Color} = 15066597
    If ?ret:Invoice_Date{prop:ReadOnly} = True
        ?ret:Invoice_Date{prop:FontColor} = 65793
        ?ret:Invoice_Date{prop:Color} = 15066597
    Elsif ?ret:Invoice_Date{prop:Req} = True
        ?ret:Invoice_Date{prop:FontColor} = 65793
        ?ret:Invoice_Date{prop:Color} = 8454143
    Else ! If ?ret:Invoice_Date{prop:Req} = True
        ?ret:Invoice_Date{prop:FontColor} = 65793
        ?ret:Invoice_Date{prop:Color} = 16777215
    End ! If ?ret:Invoice_Date{prop:Req} = True
    ?ret:Invoice_Date{prop:Trn} = 0
    ?ret:Invoice_Date{prop:FontStyle} = font:Bold
    ?RET:Invoice_Courier_Cost:Prompt{prop:FontColor} = -1
    ?RET:Invoice_Courier_Cost:Prompt{prop:Color} = 15066597
    If ?ret:Invoice_Courier_Cost{prop:ReadOnly} = True
        ?ret:Invoice_Courier_Cost{prop:FontColor} = 65793
        ?ret:Invoice_Courier_Cost{prop:Color} = 15066597
    Elsif ?ret:Invoice_Courier_Cost{prop:Req} = True
        ?ret:Invoice_Courier_Cost{prop:FontColor} = 65793
        ?ret:Invoice_Courier_Cost{prop:Color} = 8454143
    Else ! If ?ret:Invoice_Courier_Cost{prop:Req} = True
        ?ret:Invoice_Courier_Cost{prop:FontColor} = 65793
        ?ret:Invoice_Courier_Cost{prop:Color} = 16777215
    End ! If ?ret:Invoice_Courier_Cost{prop:Req} = True
    ?ret:Invoice_Courier_Cost{prop:Trn} = 0
    ?ret:Invoice_Courier_Cost{prop:FontStyle} = font:Bold
    ?Tab:3{prop:Color} = 15066597
    ?RET:Invoice_Parts_Cost:Prompt{prop:FontColor} = -1
    ?RET:Invoice_Parts_Cost:Prompt{prop:Color} = 15066597
    If ?ret:Invoice_Parts_Cost{prop:ReadOnly} = True
        ?ret:Invoice_Parts_Cost{prop:FontColor} = 65793
        ?ret:Invoice_Parts_Cost{prop:Color} = 15066597
    Elsif ?ret:Invoice_Parts_Cost{prop:Req} = True
        ?ret:Invoice_Parts_Cost{prop:FontColor} = 65793
        ?ret:Invoice_Parts_Cost{prop:Color} = 8454143
    Else ! If ?ret:Invoice_Parts_Cost{prop:Req} = True
        ?ret:Invoice_Parts_Cost{prop:FontColor} = 65793
        ?ret:Invoice_Parts_Cost{prop:Color} = 16777215
    End ! If ?ret:Invoice_Parts_Cost{prop:Req} = True
    ?ret:Invoice_Parts_Cost{prop:Trn} = 0
    ?ret:Invoice_Parts_Cost{prop:FontStyle} = font:Bold
    ?RET:Invoice_Sub_Total:Prompt{prop:FontColor} = -1
    ?RET:Invoice_Sub_Total:Prompt{prop:Color} = 15066597
    If ?ret:Invoice_Sub_Total{prop:ReadOnly} = True
        ?ret:Invoice_Sub_Total{prop:FontColor} = 65793
        ?ret:Invoice_Sub_Total{prop:Color} = 15066597
    Elsif ?ret:Invoice_Sub_Total{prop:Req} = True
        ?ret:Invoice_Sub_Total{prop:FontColor} = 65793
        ?ret:Invoice_Sub_Total{prop:Color} = 8454143
    Else ! If ?ret:Invoice_Sub_Total{prop:Req} = True
        ?ret:Invoice_Sub_Total{prop:FontColor} = 65793
        ?ret:Invoice_Sub_Total{prop:Color} = 16777215
    End ! If ?ret:Invoice_Sub_Total{prop:Req} = True
    ?ret:Invoice_Sub_Total{prop:Trn} = 0
    ?ret:Invoice_Sub_Total{prop:FontStyle} = font:Bold
    ?RET:Postcode:Prompt{prop:FontColor} = -1
    ?RET:Postcode:Prompt{prop:Color} = 15066597
    If ?ret:Postcode{prop:ReadOnly} = True
        ?ret:Postcode{prop:FontColor} = 65793
        ?ret:Postcode{prop:Color} = 15066597
    Elsif ?ret:Postcode{prop:Req} = True
        ?ret:Postcode{prop:FontColor} = 65793
        ?ret:Postcode{prop:Color} = 8454143
    Else ! If ?ret:Postcode{prop:Req} = True
        ?ret:Postcode{prop:FontColor} = 65793
        ?ret:Postcode{prop:Color} = 16777215
    End ! If ?ret:Postcode{prop:Req} = True
    ?ret:Postcode{prop:Trn} = 0
    ?ret:Postcode{prop:FontStyle} = font:Bold
    ?RET:Company_Name:Prompt{prop:FontColor} = -1
    ?RET:Company_Name:Prompt{prop:Color} = 15066597
    If ?ret:Company_Name{prop:ReadOnly} = True
        ?ret:Company_Name{prop:FontColor} = 65793
        ?ret:Company_Name{prop:Color} = 15066597
    Elsif ?ret:Company_Name{prop:Req} = True
        ?ret:Company_Name{prop:FontColor} = 65793
        ?ret:Company_Name{prop:Color} = 8454143
    Else ! If ?ret:Company_Name{prop:Req} = True
        ?ret:Company_Name{prop:FontColor} = 65793
        ?ret:Company_Name{prop:Color} = 16777215
    End ! If ?ret:Company_Name{prop:Req} = True
    ?ret:Company_Name{prop:Trn} = 0
    ?ret:Company_Name{prop:FontStyle} = font:Bold
    ?RET:Building_Name:Prompt{prop:FontColor} = -1
    ?RET:Building_Name:Prompt{prop:Color} = 15066597
    If ?ret:Building_Name{prop:ReadOnly} = True
        ?ret:Building_Name{prop:FontColor} = 65793
        ?ret:Building_Name{prop:Color} = 15066597
    Elsif ?ret:Building_Name{prop:Req} = True
        ?ret:Building_Name{prop:FontColor} = 65793
        ?ret:Building_Name{prop:Color} = 8454143
    Else ! If ?ret:Building_Name{prop:Req} = True
        ?ret:Building_Name{prop:FontColor} = 65793
        ?ret:Building_Name{prop:Color} = 16777215
    End ! If ?ret:Building_Name{prop:Req} = True
    ?ret:Building_Name{prop:Trn} = 0
    ?ret:Building_Name{prop:FontStyle} = font:Bold
    ?RET:Address_Line1:Prompt{prop:FontColor} = -1
    ?RET:Address_Line1:Prompt{prop:Color} = 15066597
    If ?ret:Address_Line1{prop:ReadOnly} = True
        ?ret:Address_Line1{prop:FontColor} = 65793
        ?ret:Address_Line1{prop:Color} = 15066597
    Elsif ?ret:Address_Line1{prop:Req} = True
        ?ret:Address_Line1{prop:FontColor} = 65793
        ?ret:Address_Line1{prop:Color} = 8454143
    Else ! If ?ret:Address_Line1{prop:Req} = True
        ?ret:Address_Line1{prop:FontColor} = 65793
        ?ret:Address_Line1{prop:Color} = 16777215
    End ! If ?ret:Address_Line1{prop:Req} = True
    ?ret:Address_Line1{prop:Trn} = 0
    ?ret:Address_Line1{prop:FontStyle} = font:Bold
    ?RET:Address_Line2:Prompt{prop:FontColor} = -1
    ?RET:Address_Line2:Prompt{prop:Color} = 15066597
    If ?ret:Address_Line2{prop:ReadOnly} = True
        ?ret:Address_Line2{prop:FontColor} = 65793
        ?ret:Address_Line2{prop:Color} = 15066597
    Elsif ?ret:Address_Line2{prop:Req} = True
        ?ret:Address_Line2{prop:FontColor} = 65793
        ?ret:Address_Line2{prop:Color} = 8454143
    Else ! If ?ret:Address_Line2{prop:Req} = True
        ?ret:Address_Line2{prop:FontColor} = 65793
        ?ret:Address_Line2{prop:Color} = 16777215
    End ! If ?ret:Address_Line2{prop:Req} = True
    ?ret:Address_Line2{prop:Trn} = 0
    ?ret:Address_Line2{prop:FontStyle} = font:Bold
    ?RET:Address_Line3:Prompt{prop:FontColor} = -1
    ?RET:Address_Line3:Prompt{prop:Color} = 15066597
    If ?ret:Address_Line3{prop:ReadOnly} = True
        ?ret:Address_Line3{prop:FontColor} = 65793
        ?ret:Address_Line3{prop:Color} = 15066597
    Elsif ?ret:Address_Line3{prop:Req} = True
        ?ret:Address_Line3{prop:FontColor} = 65793
        ?ret:Address_Line3{prop:Color} = 8454143
    Else ! If ?ret:Address_Line3{prop:Req} = True
        ?ret:Address_Line3{prop:FontColor} = 65793
        ?ret:Address_Line3{prop:Color} = 16777215
    End ! If ?ret:Address_Line3{prop:Req} = True
    ?ret:Address_Line3{prop:Trn} = 0
    ?ret:Address_Line3{prop:FontStyle} = font:Bold
    ?RET:Telephone_Number:Prompt{prop:FontColor} = -1
    ?RET:Telephone_Number:Prompt{prop:Color} = 15066597
    If ?ret:Telephone_Number{prop:ReadOnly} = True
        ?ret:Telephone_Number{prop:FontColor} = 65793
        ?ret:Telephone_Number{prop:Color} = 15066597
    Elsif ?ret:Telephone_Number{prop:Req} = True
        ?ret:Telephone_Number{prop:FontColor} = 65793
        ?ret:Telephone_Number{prop:Color} = 8454143
    Else ! If ?ret:Telephone_Number{prop:Req} = True
        ?ret:Telephone_Number{prop:FontColor} = 65793
        ?ret:Telephone_Number{prop:Color} = 16777215
    End ! If ?ret:Telephone_Number{prop:Req} = True
    ?ret:Telephone_Number{prop:Trn} = 0
    ?ret:Telephone_Number{prop:FontStyle} = font:Bold
    ?RET:Fax_Number:Prompt{prop:FontColor} = -1
    ?RET:Fax_Number:Prompt{prop:Color} = 15066597
    If ?ret:Fax_Number{prop:ReadOnly} = True
        ?ret:Fax_Number{prop:FontColor} = 65793
        ?ret:Fax_Number{prop:Color} = 15066597
    Elsif ?ret:Fax_Number{prop:Req} = True
        ?ret:Fax_Number{prop:FontColor} = 65793
        ?ret:Fax_Number{prop:Color} = 8454143
    Else ! If ?ret:Fax_Number{prop:Req} = True
        ?ret:Fax_Number{prop:FontColor} = 65793
        ?ret:Fax_Number{prop:Color} = 16777215
    End ! If ?ret:Fax_Number{prop:Req} = True
    ?ret:Fax_Number{prop:Trn} = 0
    ?ret:Fax_Number{prop:FontStyle} = font:Bold
    ?Tab:4{prop:Color} = 15066597
    ?RET:Postcode_Delivery:Prompt{prop:FontColor} = -1
    ?RET:Postcode_Delivery:Prompt{prop:Color} = 15066597
    If ?ret:Postcode_Delivery{prop:ReadOnly} = True
        ?ret:Postcode_Delivery{prop:FontColor} = 65793
        ?ret:Postcode_Delivery{prop:Color} = 15066597
    Elsif ?ret:Postcode_Delivery{prop:Req} = True
        ?ret:Postcode_Delivery{prop:FontColor} = 65793
        ?ret:Postcode_Delivery{prop:Color} = 8454143
    Else ! If ?ret:Postcode_Delivery{prop:Req} = True
        ?ret:Postcode_Delivery{prop:FontColor} = 65793
        ?ret:Postcode_Delivery{prop:Color} = 16777215
    End ! If ?ret:Postcode_Delivery{prop:Req} = True
    ?ret:Postcode_Delivery{prop:Trn} = 0
    ?ret:Postcode_Delivery{prop:FontStyle} = font:Bold
    ?RET:Company_Name_Delivery:Prompt{prop:FontColor} = -1
    ?RET:Company_Name_Delivery:Prompt{prop:Color} = 15066597
    If ?ret:Company_Name_Delivery{prop:ReadOnly} = True
        ?ret:Company_Name_Delivery{prop:FontColor} = 65793
        ?ret:Company_Name_Delivery{prop:Color} = 15066597
    Elsif ?ret:Company_Name_Delivery{prop:Req} = True
        ?ret:Company_Name_Delivery{prop:FontColor} = 65793
        ?ret:Company_Name_Delivery{prop:Color} = 8454143
    Else ! If ?ret:Company_Name_Delivery{prop:Req} = True
        ?ret:Company_Name_Delivery{prop:FontColor} = 65793
        ?ret:Company_Name_Delivery{prop:Color} = 16777215
    End ! If ?ret:Company_Name_Delivery{prop:Req} = True
    ?ret:Company_Name_Delivery{prop:Trn} = 0
    ?ret:Company_Name_Delivery{prop:FontStyle} = font:Bold
    ?RET:Building_Name_Delivery:Prompt{prop:FontColor} = -1
    ?RET:Building_Name_Delivery:Prompt{prop:Color} = 15066597
    If ?ret:Building_Name_Delivery{prop:ReadOnly} = True
        ?ret:Building_Name_Delivery{prop:FontColor} = 65793
        ?ret:Building_Name_Delivery{prop:Color} = 15066597
    Elsif ?ret:Building_Name_Delivery{prop:Req} = True
        ?ret:Building_Name_Delivery{prop:FontColor} = 65793
        ?ret:Building_Name_Delivery{prop:Color} = 8454143
    Else ! If ?ret:Building_Name_Delivery{prop:Req} = True
        ?ret:Building_Name_Delivery{prop:FontColor} = 65793
        ?ret:Building_Name_Delivery{prop:Color} = 16777215
    End ! If ?ret:Building_Name_Delivery{prop:Req} = True
    ?ret:Building_Name_Delivery{prop:Trn} = 0
    ?ret:Building_Name_Delivery{prop:FontStyle} = font:Bold
    ?RET:Address_Line1_Delivery:Prompt{prop:FontColor} = -1
    ?RET:Address_Line1_Delivery:Prompt{prop:Color} = 15066597
    If ?ret:Address_Line1_Delivery{prop:ReadOnly} = True
        ?ret:Address_Line1_Delivery{prop:FontColor} = 65793
        ?ret:Address_Line1_Delivery{prop:Color} = 15066597
    Elsif ?ret:Address_Line1_Delivery{prop:Req} = True
        ?ret:Address_Line1_Delivery{prop:FontColor} = 65793
        ?ret:Address_Line1_Delivery{prop:Color} = 8454143
    Else ! If ?ret:Address_Line1_Delivery{prop:Req} = True
        ?ret:Address_Line1_Delivery{prop:FontColor} = 65793
        ?ret:Address_Line1_Delivery{prop:Color} = 16777215
    End ! If ?ret:Address_Line1_Delivery{prop:Req} = True
    ?ret:Address_Line1_Delivery{prop:Trn} = 0
    ?ret:Address_Line1_Delivery{prop:FontStyle} = font:Bold
    ?RET:Address_Line2_Delivery:Prompt{prop:FontColor} = -1
    ?RET:Address_Line2_Delivery:Prompt{prop:Color} = 15066597
    If ?ret:Address_Line2_Delivery{prop:ReadOnly} = True
        ?ret:Address_Line2_Delivery{prop:FontColor} = 65793
        ?ret:Address_Line2_Delivery{prop:Color} = 15066597
    Elsif ?ret:Address_Line2_Delivery{prop:Req} = True
        ?ret:Address_Line2_Delivery{prop:FontColor} = 65793
        ?ret:Address_Line2_Delivery{prop:Color} = 8454143
    Else ! If ?ret:Address_Line2_Delivery{prop:Req} = True
        ?ret:Address_Line2_Delivery{prop:FontColor} = 65793
        ?ret:Address_Line2_Delivery{prop:Color} = 16777215
    End ! If ?ret:Address_Line2_Delivery{prop:Req} = True
    ?ret:Address_Line2_Delivery{prop:Trn} = 0
    ?ret:Address_Line2_Delivery{prop:FontStyle} = font:Bold
    ?RET:Address_Line3_Delivery:Prompt{prop:FontColor} = -1
    ?RET:Address_Line3_Delivery:Prompt{prop:Color} = 15066597
    If ?ret:Address_Line3_Delivery{prop:ReadOnly} = True
        ?ret:Address_Line3_Delivery{prop:FontColor} = 65793
        ?ret:Address_Line3_Delivery{prop:Color} = 15066597
    Elsif ?ret:Address_Line3_Delivery{prop:Req} = True
        ?ret:Address_Line3_Delivery{prop:FontColor} = 65793
        ?ret:Address_Line3_Delivery{prop:Color} = 8454143
    Else ! If ?ret:Address_Line3_Delivery{prop:Req} = True
        ?ret:Address_Line3_Delivery{prop:FontColor} = 65793
        ?ret:Address_Line3_Delivery{prop:Color} = 16777215
    End ! If ?ret:Address_Line3_Delivery{prop:Req} = True
    ?ret:Address_Line3_Delivery{prop:Trn} = 0
    ?ret:Address_Line3_Delivery{prop:FontStyle} = font:Bold
    ?RET:Telephone_Delivery:Prompt{prop:FontColor} = -1
    ?RET:Telephone_Delivery:Prompt{prop:Color} = 15066597
    If ?ret:Telephone_Delivery{prop:ReadOnly} = True
        ?ret:Telephone_Delivery{prop:FontColor} = 65793
        ?ret:Telephone_Delivery{prop:Color} = 15066597
    Elsif ?ret:Telephone_Delivery{prop:Req} = True
        ?ret:Telephone_Delivery{prop:FontColor} = 65793
        ?ret:Telephone_Delivery{prop:Color} = 8454143
    Else ! If ?ret:Telephone_Delivery{prop:Req} = True
        ?ret:Telephone_Delivery{prop:FontColor} = 65793
        ?ret:Telephone_Delivery{prop:Color} = 16777215
    End ! If ?ret:Telephone_Delivery{prop:Req} = True
    ?ret:Telephone_Delivery{prop:Trn} = 0
    ?ret:Telephone_Delivery{prop:FontStyle} = font:Bold
    ?RET:Fax_Number_Delivery:Prompt{prop:FontColor} = -1
    ?RET:Fax_Number_Delivery:Prompt{prop:Color} = 15066597
    If ?ret:Fax_Number_Delivery{prop:ReadOnly} = True
        ?ret:Fax_Number_Delivery{prop:FontColor} = 65793
        ?ret:Fax_Number_Delivery{prop:Color} = 15066597
    Elsif ?ret:Fax_Number_Delivery{prop:Req} = True
        ?ret:Fax_Number_Delivery{prop:FontColor} = 65793
        ?ret:Fax_Number_Delivery{prop:Color} = 8454143
    Else ! If ?ret:Fax_Number_Delivery{prop:Req} = True
        ?ret:Fax_Number_Delivery{prop:FontColor} = 65793
        ?ret:Fax_Number_Delivery{prop:Color} = 16777215
    End ! If ?ret:Fax_Number_Delivery{prop:Req} = True
    ?ret:Fax_Number_Delivery{prop:Trn} = 0
    ?ret:Fax_Number_Delivery{prop:FontStyle} = font:Bold
    ?RET:Delivery_Text:Prompt{prop:FontColor} = -1
    ?RET:Delivery_Text:Prompt{prop:Color} = 15066597
    If ?ret:Delivery_Text{prop:ReadOnly} = True
        ?ret:Delivery_Text{prop:FontColor} = 65793
        ?ret:Delivery_Text{prop:Color} = 15066597
    Elsif ?ret:Delivery_Text{prop:Req} = True
        ?ret:Delivery_Text{prop:FontColor} = 65793
        ?ret:Delivery_Text{prop:Color} = 8454143
    Else ! If ?ret:Delivery_Text{prop:Req} = True
        ?ret:Delivery_Text{prop:FontColor} = 65793
        ?ret:Delivery_Text{prop:Color} = 16777215
    End ! If ?ret:Delivery_Text{prop:Req} = True
    ?ret:Delivery_Text{prop:Trn} = 0
    ?ret:Delivery_Text{prop:FontStyle} = font:Bold
    ?RET:Invoice_Text:Prompt{prop:FontColor} = -1
    ?RET:Invoice_Text:Prompt{prop:Color} = 15066597
    If ?ret:Invoice_Text{prop:ReadOnly} = True
        ?ret:Invoice_Text{prop:FontColor} = 65793
        ?ret:Invoice_Text{prop:Color} = 15066597
    Elsif ?ret:Invoice_Text{prop:Req} = True
        ?ret:Invoice_Text{prop:FontColor} = 65793
        ?ret:Invoice_Text{prop:Color} = 8454143
    Else ! If ?ret:Invoice_Text{prop:Req} = True
        ?ret:Invoice_Text{prop:FontColor} = 65793
        ?ret:Invoice_Text{prop:Color} = 16777215
    End ! If ?ret:Invoice_Text{prop:Req} = True
    ?ret:Invoice_Text{prop:Trn} = 0
    ?ret:Invoice_Text{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateRETSALES',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateRETSALES',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateRETSALES',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Ref_Number:Prompt;  SolaceCtrlName = '?RET:Ref_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Ref_Number;  SolaceCtrlName = '?ret:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:who_booked:Prompt;  SolaceCtrlName = '?RET:who_booked:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:who_booked;  SolaceCtrlName = '?ret:who_booked';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:date_booked:Prompt;  SolaceCtrlName = '?RET:date_booked:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:date_booked;  SolaceCtrlName = '?ret:date_booked';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:time_booked:Prompt;  SolaceCtrlName = '?RET:time_booked:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:time_booked;  SolaceCtrlName = '?ret:time_booked';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Account_Number:Prompt;  SolaceCtrlName = '?RET:Account_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Account_Number;  SolaceCtrlName = '?ret:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Contact_Name:Prompt;  SolaceCtrlName = '?RET:Contact_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Contact_Name;  SolaceCtrlName = '?ret:Contact_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Purchase_Order_Number:Prompt;  SolaceCtrlName = '?RET:Purchase_Order_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Purchase_Order_Number;  SolaceCtrlName = '?ret:Purchase_Order_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Delivery_Collection:Prompt;  SolaceCtrlName = '?RET:Delivery_Collection:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Delivery_Collection;  SolaceCtrlName = '?ret:Delivery_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Payment_Method:Prompt;  SolaceCtrlName = '?RET:Payment_Method:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Payment_Method;  SolaceCtrlName = '?ret:Payment_Method';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Courier_Cost:Prompt;  SolaceCtrlName = '?RET:Courier_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Courier_Cost;  SolaceCtrlName = '?ret:Courier_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Parts_Cost:Prompt;  SolaceCtrlName = '?RET:Parts_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Parts_Cost;  SolaceCtrlName = '?ret:Parts_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Sub_Total:Prompt;  SolaceCtrlName = '?RET:Sub_Total:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Sub_Total;  SolaceCtrlName = '?ret:Sub_Total';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Courier:Prompt;  SolaceCtrlName = '?RET:Courier:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Courier;  SolaceCtrlName = '?ret:Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Consignment_Number:Prompt;  SolaceCtrlName = '?RET:Consignment_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Consignment_Number;  SolaceCtrlName = '?ret:Consignment_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Date_Despatched:Prompt;  SolaceCtrlName = '?RET:Date_Despatched:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Date_Despatched;  SolaceCtrlName = '?ret:Date_Despatched';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Despatched:Prompt;  SolaceCtrlName = '?RET:Despatched:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Despatched;  SolaceCtrlName = '?ret:Despatched';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Despatch_Number:Prompt;  SolaceCtrlName = '?RET:Despatch_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Despatch_Number;  SolaceCtrlName = '?ret:Despatch_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Invoice_Number:Prompt;  SolaceCtrlName = '?RET:Invoice_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Invoice_Number;  SolaceCtrlName = '?ret:Invoice_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Invoice_Date:Prompt;  SolaceCtrlName = '?RET:Invoice_Date:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Invoice_Date;  SolaceCtrlName = '?ret:Invoice_Date';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Invoice_Courier_Cost:Prompt;  SolaceCtrlName = '?RET:Invoice_Courier_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Invoice_Courier_Cost;  SolaceCtrlName = '?ret:Invoice_Courier_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:3;  SolaceCtrlName = '?Tab:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Invoice_Parts_Cost:Prompt;  SolaceCtrlName = '?RET:Invoice_Parts_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Invoice_Parts_Cost;  SolaceCtrlName = '?ret:Invoice_Parts_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Invoice_Sub_Total:Prompt;  SolaceCtrlName = '?RET:Invoice_Sub_Total:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Invoice_Sub_Total;  SolaceCtrlName = '?ret:Invoice_Sub_Total';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Postcode:Prompt;  SolaceCtrlName = '?RET:Postcode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Postcode;  SolaceCtrlName = '?ret:Postcode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Company_Name:Prompt;  SolaceCtrlName = '?RET:Company_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Company_Name;  SolaceCtrlName = '?ret:Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Building_Name:Prompt;  SolaceCtrlName = '?RET:Building_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Building_Name;  SolaceCtrlName = '?ret:Building_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Address_Line1:Prompt;  SolaceCtrlName = '?RET:Address_Line1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Address_Line1;  SolaceCtrlName = '?ret:Address_Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Address_Line2:Prompt;  SolaceCtrlName = '?RET:Address_Line2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Address_Line2;  SolaceCtrlName = '?ret:Address_Line2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Address_Line3:Prompt;  SolaceCtrlName = '?RET:Address_Line3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Address_Line3;  SolaceCtrlName = '?ret:Address_Line3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Telephone_Number:Prompt;  SolaceCtrlName = '?RET:Telephone_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Telephone_Number;  SolaceCtrlName = '?ret:Telephone_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Fax_Number:Prompt;  SolaceCtrlName = '?RET:Fax_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Fax_Number;  SolaceCtrlName = '?ret:Fax_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:4;  SolaceCtrlName = '?Tab:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Postcode_Delivery:Prompt;  SolaceCtrlName = '?RET:Postcode_Delivery:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Postcode_Delivery;  SolaceCtrlName = '?ret:Postcode_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Company_Name_Delivery:Prompt;  SolaceCtrlName = '?RET:Company_Name_Delivery:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Company_Name_Delivery;  SolaceCtrlName = '?ret:Company_Name_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Building_Name_Delivery:Prompt;  SolaceCtrlName = '?RET:Building_Name_Delivery:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Building_Name_Delivery;  SolaceCtrlName = '?ret:Building_Name_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Address_Line1_Delivery:Prompt;  SolaceCtrlName = '?RET:Address_Line1_Delivery:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Address_Line1_Delivery;  SolaceCtrlName = '?ret:Address_Line1_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Address_Line2_Delivery:Prompt;  SolaceCtrlName = '?RET:Address_Line2_Delivery:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Address_Line2_Delivery;  SolaceCtrlName = '?ret:Address_Line2_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Address_Line3_Delivery:Prompt;  SolaceCtrlName = '?RET:Address_Line3_Delivery:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Address_Line3_Delivery;  SolaceCtrlName = '?ret:Address_Line3_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Telephone_Delivery:Prompt;  SolaceCtrlName = '?RET:Telephone_Delivery:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Telephone_Delivery;  SolaceCtrlName = '?ret:Telephone_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Fax_Number_Delivery:Prompt;  SolaceCtrlName = '?RET:Fax_Number_Delivery:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Fax_Number_Delivery;  SolaceCtrlName = '?ret:Fax_Number_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Delivery_Text:Prompt;  SolaceCtrlName = '?RET:Delivery_Text:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Delivery_Text;  SolaceCtrlName = '?ret:Delivery_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Invoice_Text:Prompt;  SolaceCtrlName = '?RET:Invoice_Text:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Invoice_Text;  SolaceCtrlName = '?ret:Invoice_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateRETSALES')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateRETSALES')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?RET:Ref_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(ret:Record,History::ret:Record)
  SELF.AddHistoryField(?ret:Ref_Number,1)
  SELF.AddHistoryField(?ret:who_booked,2)
  SELF.AddHistoryField(?ret:date_booked,3)
  SELF.AddHistoryField(?ret:time_booked,4)
  SELF.AddHistoryField(?ret:Account_Number,5)
  SELF.AddHistoryField(?ret:Contact_Name,6)
  SELF.AddHistoryField(?ret:Purchase_Order_Number,7)
  SELF.AddHistoryField(?ret:Delivery_Collection,8)
  SELF.AddHistoryField(?ret:Payment_Method,9)
  SELF.AddHistoryField(?ret:Courier_Cost,10)
  SELF.AddHistoryField(?ret:Parts_Cost,11)
  SELF.AddHistoryField(?ret:Sub_Total,12)
  SELF.AddHistoryField(?ret:Courier,13)
  SELF.AddHistoryField(?ret:Consignment_Number,14)
  SELF.AddHistoryField(?ret:Date_Despatched,15)
  SELF.AddHistoryField(?ret:Despatched,16)
  SELF.AddHistoryField(?ret:Despatch_Number,17)
  SELF.AddHistoryField(?ret:Invoice_Number,18)
  SELF.AddHistoryField(?ret:Invoice_Date,19)
  SELF.AddHistoryField(?ret:Invoice_Courier_Cost,20)
  SELF.AddHistoryField(?ret:Invoice_Parts_Cost,21)
  SELF.AddHistoryField(?ret:Invoice_Sub_Total,22)
  SELF.AddHistoryField(?ret:Postcode,27)
  SELF.AddHistoryField(?ret:Company_Name,28)
  SELF.AddHistoryField(?ret:Building_Name,29)
  SELF.AddHistoryField(?ret:Address_Line1,30)
  SELF.AddHistoryField(?ret:Address_Line2,31)
  SELF.AddHistoryField(?ret:Address_Line3,32)
  SELF.AddHistoryField(?ret:Telephone_Number,33)
  SELF.AddHistoryField(?ret:Fax_Number,34)
  SELF.AddHistoryField(?ret:Postcode_Delivery,35)
  SELF.AddHistoryField(?ret:Company_Name_Delivery,36)
  SELF.AddHistoryField(?ret:Building_Name_Delivery,37)
  SELF.AddHistoryField(?ret:Address_Line1_Delivery,38)
  SELF.AddHistoryField(?ret:Address_Line2_Delivery,39)
  SELF.AddHistoryField(?ret:Address_Line3_Delivery,40)
  SELF.AddHistoryField(?ret:Telephone_Delivery,41)
  SELF.AddHistoryField(?ret:Fax_Number_Delivery,42)
  SELF.AddHistoryField(?ret:Delivery_Text,43)
  SELF.AddHistoryField(?ret:Invoice_Text,44)
  SELF.AddUpdateFile(Access:RETSALES)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:RETSALES.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:RETSALES
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETSALES.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateRETSALES',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateRETSALES')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

