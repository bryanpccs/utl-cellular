

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01028.INC'),ONCE        !Local module procedure declarations
                     END


UpdateRETPARTSLIST PROCEDURE                          !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::retpar:Record LIKE(retpar:RECORD),STATIC
QuickWindow          WINDOW('Update the RETPARTSLIST File'),AT(,,248,166),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateRETPARTSLIST'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,240,140),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?RETPAR:RecordNumber:Prompt),TRN
                           ENTRY(@s8),AT(116,20,40,10),USE(retpar:RecordNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Record Number'),TIP('Record Number'),UPR
                           PROMPT('Reference To Accounts List'),AT(8,34),USE(?RETPAR:RefNumber:Prompt),TRN
                           PROMPT('Reference To Stock'),AT(8,48),USE(?RETPAR:PartRefNumber:Prompt),TRN
                           ENTRY(@s8),AT(116,48,40,10),USE(retpar:PartRefNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Reference To Stock'),TIP('Reference To Stock'),UPR
                           PROMPT('Part Number'),AT(8,62),USE(?RETPAR:PartNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(116,62,124,10),USE(retpar:PartNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Part Number'),TIP('Part Number'),UPR
                           PROMPT('Description'),AT(8,76),USE(?RETPAR:Description:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(116,76,124,10),USE(retpar:Description),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('desc'),TIP('desc'),UPR
                           PROMPT('Quantity In Stock'),AT(8,90),USE(?RETPAR:QuantityStock:Prompt),TRN
                           ENTRY(@s8),AT(116,90,40,10),USE(retpar:QuantityStock),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Quantity In Stock'),TIP('Quantity In Stock'),UPR
                           PROMPT('Quantity On Back Order'),AT(8,104),USE(?RETPAR:QuantityBackOrder:Prompt),TRN
                           ENTRY(@s8),AT(116,104,40,10),USE(retpar:QuantityBackOrder),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Quantity In Back Order'),TIP('Quantity In Back Order'),UPR
                           PROMPT('Quantity On Order'),AT(8,118),USE(?RETPAR:QuantityOnOrder:Prompt),TRN
                           ENTRY(@s8),AT(116,118,40,10),USE(retpar:QuantityOnOrder),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Quantity On Order'),TIP('Quantity On Order'),UPR
                           CHECK('Processed'),AT(116,132,70,8),USE(retpar:Processed),MSG('Processed'),TIP('Processed'),VALUE('1','0')
                         END
                       END
                       BUTTON('OK'),AT(150,148,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(199,148,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(199,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?RETPAR:RecordNumber:Prompt{prop:FontColor} = -1
    ?RETPAR:RecordNumber:Prompt{prop:Color} = 15066597
    If ?retpar:RecordNumber{prop:ReadOnly} = True
        ?retpar:RecordNumber{prop:FontColor} = 65793
        ?retpar:RecordNumber{prop:Color} = 15066597
    Elsif ?retpar:RecordNumber{prop:Req} = True
        ?retpar:RecordNumber{prop:FontColor} = 65793
        ?retpar:RecordNumber{prop:Color} = 8454143
    Else ! If ?retpar:RecordNumber{prop:Req} = True
        ?retpar:RecordNumber{prop:FontColor} = 65793
        ?retpar:RecordNumber{prop:Color} = 16777215
    End ! If ?retpar:RecordNumber{prop:Req} = True
    ?retpar:RecordNumber{prop:Trn} = 0
    ?retpar:RecordNumber{prop:FontStyle} = font:Bold
    ?RETPAR:RefNumber:Prompt{prop:FontColor} = -1
    ?RETPAR:RefNumber:Prompt{prop:Color} = 15066597
    ?RETPAR:PartRefNumber:Prompt{prop:FontColor} = -1
    ?RETPAR:PartRefNumber:Prompt{prop:Color} = 15066597
    If ?retpar:PartRefNumber{prop:ReadOnly} = True
        ?retpar:PartRefNumber{prop:FontColor} = 65793
        ?retpar:PartRefNumber{prop:Color} = 15066597
    Elsif ?retpar:PartRefNumber{prop:Req} = True
        ?retpar:PartRefNumber{prop:FontColor} = 65793
        ?retpar:PartRefNumber{prop:Color} = 8454143
    Else ! If ?retpar:PartRefNumber{prop:Req} = True
        ?retpar:PartRefNumber{prop:FontColor} = 65793
        ?retpar:PartRefNumber{prop:Color} = 16777215
    End ! If ?retpar:PartRefNumber{prop:Req} = True
    ?retpar:PartRefNumber{prop:Trn} = 0
    ?retpar:PartRefNumber{prop:FontStyle} = font:Bold
    ?RETPAR:PartNumber:Prompt{prop:FontColor} = -1
    ?RETPAR:PartNumber:Prompt{prop:Color} = 15066597
    If ?retpar:PartNumber{prop:ReadOnly} = True
        ?retpar:PartNumber{prop:FontColor} = 65793
        ?retpar:PartNumber{prop:Color} = 15066597
    Elsif ?retpar:PartNumber{prop:Req} = True
        ?retpar:PartNumber{prop:FontColor} = 65793
        ?retpar:PartNumber{prop:Color} = 8454143
    Else ! If ?retpar:PartNumber{prop:Req} = True
        ?retpar:PartNumber{prop:FontColor} = 65793
        ?retpar:PartNumber{prop:Color} = 16777215
    End ! If ?retpar:PartNumber{prop:Req} = True
    ?retpar:PartNumber{prop:Trn} = 0
    ?retpar:PartNumber{prop:FontStyle} = font:Bold
    ?RETPAR:Description:Prompt{prop:FontColor} = -1
    ?RETPAR:Description:Prompt{prop:Color} = 15066597
    If ?retpar:Description{prop:ReadOnly} = True
        ?retpar:Description{prop:FontColor} = 65793
        ?retpar:Description{prop:Color} = 15066597
    Elsif ?retpar:Description{prop:Req} = True
        ?retpar:Description{prop:FontColor} = 65793
        ?retpar:Description{prop:Color} = 8454143
    Else ! If ?retpar:Description{prop:Req} = True
        ?retpar:Description{prop:FontColor} = 65793
        ?retpar:Description{prop:Color} = 16777215
    End ! If ?retpar:Description{prop:Req} = True
    ?retpar:Description{prop:Trn} = 0
    ?retpar:Description{prop:FontStyle} = font:Bold
    ?RETPAR:QuantityStock:Prompt{prop:FontColor} = -1
    ?RETPAR:QuantityStock:Prompt{prop:Color} = 15066597
    If ?retpar:QuantityStock{prop:ReadOnly} = True
        ?retpar:QuantityStock{prop:FontColor} = 65793
        ?retpar:QuantityStock{prop:Color} = 15066597
    Elsif ?retpar:QuantityStock{prop:Req} = True
        ?retpar:QuantityStock{prop:FontColor} = 65793
        ?retpar:QuantityStock{prop:Color} = 8454143
    Else ! If ?retpar:QuantityStock{prop:Req} = True
        ?retpar:QuantityStock{prop:FontColor} = 65793
        ?retpar:QuantityStock{prop:Color} = 16777215
    End ! If ?retpar:QuantityStock{prop:Req} = True
    ?retpar:QuantityStock{prop:Trn} = 0
    ?retpar:QuantityStock{prop:FontStyle} = font:Bold
    ?RETPAR:QuantityBackOrder:Prompt{prop:FontColor} = -1
    ?RETPAR:QuantityBackOrder:Prompt{prop:Color} = 15066597
    If ?retpar:QuantityBackOrder{prop:ReadOnly} = True
        ?retpar:QuantityBackOrder{prop:FontColor} = 65793
        ?retpar:QuantityBackOrder{prop:Color} = 15066597
    Elsif ?retpar:QuantityBackOrder{prop:Req} = True
        ?retpar:QuantityBackOrder{prop:FontColor} = 65793
        ?retpar:QuantityBackOrder{prop:Color} = 8454143
    Else ! If ?retpar:QuantityBackOrder{prop:Req} = True
        ?retpar:QuantityBackOrder{prop:FontColor} = 65793
        ?retpar:QuantityBackOrder{prop:Color} = 16777215
    End ! If ?retpar:QuantityBackOrder{prop:Req} = True
    ?retpar:QuantityBackOrder{prop:Trn} = 0
    ?retpar:QuantityBackOrder{prop:FontStyle} = font:Bold
    ?RETPAR:QuantityOnOrder:Prompt{prop:FontColor} = -1
    ?RETPAR:QuantityOnOrder:Prompt{prop:Color} = 15066597
    If ?retpar:QuantityOnOrder{prop:ReadOnly} = True
        ?retpar:QuantityOnOrder{prop:FontColor} = 65793
        ?retpar:QuantityOnOrder{prop:Color} = 15066597
    Elsif ?retpar:QuantityOnOrder{prop:Req} = True
        ?retpar:QuantityOnOrder{prop:FontColor} = 65793
        ?retpar:QuantityOnOrder{prop:Color} = 8454143
    Else ! If ?retpar:QuantityOnOrder{prop:Req} = True
        ?retpar:QuantityOnOrder{prop:FontColor} = 65793
        ?retpar:QuantityOnOrder{prop:Color} = 16777215
    End ! If ?retpar:QuantityOnOrder{prop:Req} = True
    ?retpar:QuantityOnOrder{prop:Trn} = 0
    ?retpar:QuantityOnOrder{prop:FontStyle} = font:Bold
    ?retpar:Processed{prop:Font,3} = -1
    ?retpar:Processed{prop:Color} = 15066597
    ?retpar:Processed{prop:Trn} = 0

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateRETPARTSLIST',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateRETPARTSLIST',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateRETPARTSLIST',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RETPAR:RecordNumber:Prompt;  SolaceCtrlName = '?RETPAR:RecordNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?retpar:RecordNumber;  SolaceCtrlName = '?retpar:RecordNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RETPAR:RefNumber:Prompt;  SolaceCtrlName = '?RETPAR:RefNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RETPAR:PartRefNumber:Prompt;  SolaceCtrlName = '?RETPAR:PartRefNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?retpar:PartRefNumber;  SolaceCtrlName = '?retpar:PartRefNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RETPAR:PartNumber:Prompt;  SolaceCtrlName = '?RETPAR:PartNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?retpar:PartNumber;  SolaceCtrlName = '?retpar:PartNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RETPAR:Description:Prompt;  SolaceCtrlName = '?RETPAR:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?retpar:Description;  SolaceCtrlName = '?retpar:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RETPAR:QuantityStock:Prompt;  SolaceCtrlName = '?RETPAR:QuantityStock:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?retpar:QuantityStock;  SolaceCtrlName = '?retpar:QuantityStock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RETPAR:QuantityBackOrder:Prompt;  SolaceCtrlName = '?RETPAR:QuantityBackOrder:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?retpar:QuantityBackOrder;  SolaceCtrlName = '?retpar:QuantityBackOrder';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RETPAR:QuantityOnOrder:Prompt;  SolaceCtrlName = '?RETPAR:QuantityOnOrder:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?retpar:QuantityOnOrder;  SolaceCtrlName = '?retpar:QuantityOnOrder';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?retpar:Processed;  SolaceCtrlName = '?retpar:Processed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a RETPARTSLIST Record'
  OF ChangeRecord
    ActionMessage = 'Changing a RETPARTSLIST Record'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateRETPARTSLIST')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateRETPARTSLIST')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?RETPAR:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(retpar:Record,History::retpar:Record)
  SELF.AddHistoryField(?retpar:RecordNumber,1)
  SELF.AddHistoryField(?retpar:PartRefNumber,2)
  SELF.AddHistoryField(?retpar:PartNumber,3)
  SELF.AddHistoryField(?retpar:Description,4)
  SELF.AddHistoryField(?retpar:QuantityStock,6)
  SELF.AddHistoryField(?retpar:QuantityBackOrder,7)
  SELF.AddHistoryField(?retpar:QuantityOnOrder,8)
  SELF.AddHistoryField(?retpar:Processed,9)
  SELF.AddUpdateFile(Access:RETPARTSLIST)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:RETPARTSLIST.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:RETPARTSLIST
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETPARTSLIST.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateRETPARTSLIST',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateRETPARTSLIST')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

