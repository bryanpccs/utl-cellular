

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBG01036.INC'),ONCE        !Local module procedure declarations
                     END


UpdateVirtualWarehouse PROCEDURE                      ! Declare Procedure
save_res_id          USHORT
save_sto_id          USHORT
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
ATLSTOCK             FILE,DRIVER('Btrieve'),OEM,PRE(ATLS),CREATE,BINDABLE,THREAD
Service_centre_key       KEY(ATLS:Service_Centre_Code,ATLS:Ref_Number),DUP,NOCASE,OPT
Reference_number_key     KEY(ATLS:Ref_Number),DUP,NOCASE,OPT
Service_centre_part_number KEY(ATLS:Service_Centre_Code,ATLS:Part_Number),DUP,NOCASE,OPT
Record                   RECORD,PRE()
Ref_Number                  LONG
Service_Centre_Code         STRING(10)
Part_Number                 STRING(30)
Description                 STRING(30)
Qty_in_Stock                REAL
Minimum_Stock_Level         REAL
Item_cost                   REAL
                         END
                     END

CENTRE_CODES         FILE,DRIVER('Btrieve'),PRE(CEN),CREATE,BINDABLE,THREAD
service_centre_code_key  KEY(CEN:SERVICE_CENTRE_CODE),DUP,NOCASE
name_key                 KEY(CEN:Name),DUP,NOCASE
trade_account_key        KEY(CEN:Trade_Account_Number),DUP,NOCASE
Record                   RECORD,PRE()
SERVICE_CENTRE_CODE         STRING(10)
Batch_No_counter            SHORT
Contact_Name                STRING(60)
Telephone                   STRING(20)
Fax_Number                  STRING(20)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Post_Code                   STRING(20)
email_address               STRING(50)
Name                        STRING(80)
PASSWORD                    STRING(10)
Include_Fault_Logger        STRING(3)
Country_Code                STRING(10)
Include_Analysis            STRING(3)
Ignore_Warranty_Period      STRING(3)
Export_Flag                 STRING(1)
Use_Euro                    STRING(1)
Service_Level               STRING(20)
VAT_labour                  REAL
VAT_parts                   REAL
trade_account_number        STRING(15)
WarrantyPartsValuation      BYTE
Stock_Replicated            STRING(3)
ASCNumber                   STRING(10)
NewCountryCode              STRING(2)
RegionCode                  STRING(2)
SenderCountry               STRING(2)
Refurbisher                 STRING(1)
BER_Allowed                 STRING(1)
Virtual_Warehouse           STRING(1)
! Start Amendment BE(12/05/03)
Virtual_warehouse_activation_date  DATE
! Start Amendment BE(12/05/03)
! Start Amendment BE(16/05/03)
Parts_Distributor           STRING(1)
! Start Amendment BE(16/05/03)
                         END
                   END

ATL_Stock_history    FILE,DRIVER('Btrieve'),OEM,PRE(ASH),CREATE,BINDABLE,THREAD
Service_Centre_Date      KEY(ASH:Service_Centre_Number,ASH:Date),DUP,NOCASE
Service_Centre_Part_Number KEY(ASH:Service_Centre_Number,ASH:Part_Number),DUP,NOCASE
Service_Centre_Invoice_number KEY(ASH:Service_Centre_Number,ASH:Invoice_number),DUP,NOCASE
Sevice_Centre_Job_ref    KEY(ASH:Service_Centre_Number,ASH:Job_Ref_number),DUP,NOCASE
Record                   RECORD,PRE()
Service_Centre_Number       STRING(20)
Date                        DATE
Part_Number                 STRING(20)
Description                 STRING(255)
Quantity                    STRING(20)
Invoice_number              STRING(20)
Job_Ref_number              STRING(20)
Part_Value                  STRING(20)
Comment                     STRING(255)
                         END
                     END

! Start Amendment BE(16/06/03)
BULK_PURCHASE       FILE,DRIVER('Btrieve'),OEM,PRE(BUL),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(BUL:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Part_Number                 STRING(30)
Bulk_qty                    REAL
Bulk_Cost                   REAL
Item_Cost                   REAL
                         END
                     END  
! End Amendment BE(16/06/03)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'UpdateVirtualWarehouse')      !Add Procedure to Log
  end


  InstallPath" = GETINI('Directories', 'Install', '', CLIP(PATH()) & '\VirtualWarehouse.ini')
  IF (CLIP(InstallPath") = '') THEN
    ! Return if Virtual Warehouse not Installed
    RETURN
  END

  CENTRE_CODES{PROP:NAME} = CLIP(InstallPath") & '\CENTRE_C.DAT'
  ATLSTOCK{PROP:NAME} = CLIP(InstallPath") & '\ATLSTOCK.DAT'
  ATL_STOCK_HISTORY{PROP:NAME} = CLIP(InstallPath") & '\ATL_STOC.DAT'
  ! Start Amendment BE(16/06/03)
  BULK_PURCHASE{PROP:NAME} = CLIP(InstallPath") & '\BULK_PUR.DAT'
  ! End Amendment BE(16/06/03)

  !  IMPORTANT *****************************************************************************
  !
  !  The 3 Virtual Warehouse Files are declared manually within this program.
  !  It is imperative that ANY change in the Virtual Warehouse Dictionary for
  !  these files is reflected in the declarations here.
  !
  !  A symptom of the dictionary definition being out of step is a "File Not Found Error"
  !  this is initially misleading but I recommend that the first thing to check is the data
  !  definition.
  !
  !  A second problem with the error message is that the FILE{PROP:Name} pathname AFTER the error
  !  indicates the ServiceBase install directory (i.e. NOT the VirtualWarehouse.ini pathname as
  !  you might expect from th above code).  This is also very misleading.
  !
  ! ****************************************************************************************

  ! Check if Account is defined in Virtual Warehouse and if it has the "Uses VW Flag" set
  OPEN(CENTRE_CODES, ReadOnly)
  IF (ERRORCODE()) THEN
    MESSAGE('Error Opening Virtual Warehouse File (' & CENTRE_CODES{PROP:NAME}  & ') : ' & ERROR())
  ELSE
    CEN:Trade_Account_Number = RET:Account_Number
    GET(CENTRE_CODES, CEN:trade_account_key)
    !IF (ERRORCODE()) THEN
    !    MESSAGE('Error Accessing Virtual Warehouse File (' & RET:Account_Number & ') : ' & ERROR())
    !ELSE
    IF (ERRORCODE() = 0) THEN
        IF ((UPPER(CEN:Virtual_Warehouse) = 'Y') OR |
            (UPPER(CEN:Virtual_Warehouse) = 'T') OR |
            (CEN:Virtual_Warehouse = '1')) THEN
            ! Do not update ATLSTOCK file if Virtual Warehouse not switched on for this Service Centre
            continue# = 1
            OPEN(ATLSTOCK, ReadWrite)
            IF (ERRORCODE()) THEN
                MESSAGE('Error Opening Virtual Warehouse File (' & ATLSTOCK{PROP:NAME}  & ') : ' & ERROR())
                continue# = 0
            END
            OPEN(ATL_STOCK_HISTORY, ReadWrite)
            IF (ERRORCODE()) THEN
                CREATE(ATL_STOCK_HISTORY)
                IF (ERRORCODE()) THEN
                    MESSAGE('Error Creating Virtual Warehouse File (' & ATL_STOCK_HISTORY{PROP:NAME}  & ') : ' & ERROR())
                    continue# = 0
                ELSE
                    CLOSE(ATL_STOCK_HISTORY)
                    OPEN(ATL_STOCK_HISTORY, ReadWrite)
                    IF (ERRORCODE()) THEN
                        MESSAGE('Error Opening Virtual Warehouse File (' & ATL_STOCK_HISTORY{PROP:NAME}  & ') : ' & ERROR())
                        continue# = 0
                    END
                END
            END
            ! Start Amendment BE(16/06/03)
            OPEN(BULK_PURCHASE, ReadOnly)
            IF (ERRORCODE()) THEN
                MESSAGE('Error Opening Virtual Warehouse File (' & BULK_PURCHASE{PROP:NAME}  & ') : ' & ERROR())
                continue# = 0
            END
            ! End Amendment BE(16/06/03)
            IF (continue# = 1) THEN
                ! Process Retail Parts Only (Not Accessories)
                save_res_id = access:RETSTOCK.savefile()
                save_sto_id = access:STOCK.savefile()
                access:RETSTOCK.clearkey(res:despatched_only_key)
                res:ref_number = ret:ref_number
                set(res:despatched_only_key, res:despatched_only_key)
                loop
                    IF ((access:RETSTOCK.next()) OR (res:ref_number <> ret:ref_number)) THEN
                        BREAK
                    END

                    ! Start Change BE(16/05/03)
                    IF (res:despatched = 'PEN') THEN
                        CYCLE
                    END
                    ! End Change BE(16/05/03)

                    ! Check if Part or Accessory
                    access:STOCK.clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number = res:Part_Ref_Number
                    IF (access:STOCK.tryfetch(sto:Ref_Number_Key) = Level:benign) THEN
                        IF (sto:Accessory <> 'YES') THEN
                            ! Start Amendment BE(12/06/03)
                            IsBulkItem# = 1
                            BUL:Part_Number = res:Part_Number
                            GET(BULK_PURCHASE, BUL:Part_Number_Key)
                            IF (ERRORCODE()) THEN
                                IsBulkItem# = 0
                            END
                            ! End Amendment BE(12/06/03)
                            ! Update Virtual warehouse ATLSTOCK File
                            ! N.B. Cannot use Stock Number as access key because the same part reference
                            !      can be held for many different service centres.
                            ATLS:Service_Centre_Code = CEN:Service_Centre_Code
                            ATLS:Part_Number = res:Part_Number
                            GET(ATLSTOCK, ATLS:service_centre_part_number)
                            IF (ERRORCODE()) THEN
                                ATLS:Ref_Number = sto:Ref_Number
                                ATLS:Service_Centre_Code = CEN:Service_Centre_Code
                                ATLS:Part_Number = res:Part_Number
                                ATLS:Description = res:Description
                                ATLS:Minimum_Stock_Level = 0
                                ! Start Amendment BE(16/06/03)
                                !ATLS:Qty_In_Stock = res:Quantity
                                !ATLS:Item_Cost = sto:Sale_Cost
                                IF (IsBulkItem# = 1) THEN
                                    ATLS:Qty_In_Stock = (res:Quantity * BUL:Bulk_qty)
                                    ATLS:Item_Cost = BUL:Item_Cost
                                ELSE
                                    ATLS:Qty_In_Stock = res:Quantity
                                    ATLS:Item_Cost = sto:Sale_Cost
                                END
                                ! End Amendment BE(16/06/03)
                                ADD(ATLSTOCK)
                                !IF (ERRORCODE()) THEN
                                !    MESSAGE('Error Creating ATLSTOCK: ' & ERROR())
                                !END
                            ELSE
                                ! Start Amendment BE(16/06/03)
                                !ATLS:Qty_In_Stock += res:Quantity
                                IF (IsBulkItem# = 1) THEN
                                    ATLS:Qty_In_Stock += (res:Quantity * BUL:Bulk_qty)
                                ELSE
                                    ATLS:Qty_In_Stock += res:Quantity
                                END
                                ! End Amendment BE(16/06/03)
                                PUT(ATLSTOCK)
                                !IF (ERRORCODE()) THEN
                                !    MESSAGE('Error Updating ATLSTOCK: ' & ERROR())
                                !END
                            END

                            ASH:Service_Centre_Number  = CEN:Service_Centre_Code
                            ASH:Date                   = TODAY()
                            ASH:Part_Number            = res:Part_Number
                            ASH:Description            = res:Description
                            ! Start Amendment BE(12/05/03)
                            !ASH:Quantity               = CLIP(LEFT(FORMAT(res:Quantity, @n8)))
                            !ASH:Invoice_number         = CLIP(LEFT(FORMAT(ret:Invoice_Number, @n8)))
                            !ASH:Job_Ref_number         = CLIP(LEFT(FORMAT(ret:ref_number, @n8)))
                            !ASH:Part_Value             = CLIP(LEFT(FORMAT(sto:Sale_Cost, @n12.02)))
                            !ASH:Comment                = ''
                            ! Start Amendment BE(16/06/03)
                            !ASH:Quantity               = CLIP(LEFT(FORMAT(res:Quantity, @n_8)))
                            !ASH:Part_Value             = CLIP(LEFT(FORMAT(sto:Sale_Cost, @n_12.02)))
                            IF (IsBulkItem# = 1) THEN
                                ASH:Quantity               = CLIP(LEFT(FORMAT((res:Quantity * BUL:Bulk_qty), @n_8)))
                                ASH:Part_Value             = CLIP(LEFT(FORMAT(BUL:Item_Cost, @n_12.02)))
                            ELSE
                                ASH:Quantity               = CLIP(LEFT(FORMAT(res:Quantity, @n_8)))
                                ASH:Part_Value             = CLIP(LEFT(FORMAT(sto:Sale_Cost, @n_12.02)))
                            END
                            ! End Amendment BE(16/06/03)
                            ASH:Invoice_number         = CLIP(LEFT(FORMAT(ret:Invoice_Number, @n_8)))
                            !ASH:Job_Ref_number         = CLIP(LEFT(FORMAT(ret:ref_number, @n8)))
                            !ASH:Part_Value             = CLIP(LEFT(FORMAT(sto:Sale_Cost, @n_12.02)))
                            ASH:Comment                = 'RETAIL SALE'
                            ! End Amendment BE(12/05/03)
                            ADD(ATL_STOCK_HISTORY)
                            !IF (ERRORCODE()) THEN
                            !    MESSAGE('Error Creating ATL_STOCK_HISTORY: ' & ERROR())
                            !END
                        END
                    !ELSE
                    !    MESSAGE('STOCK Fetch Error: ' & ERROR())
                    END

                END
                access:STOCK.restorefile(save_sto_id)
                access:RETSTOCK.restorefile(save_res_id)
                CLOSE(ATLSTOCK)
                CLOSE(ATL_STOCK_HISTORY)
                ! Start Amendment BE(16/06/03)
                CLOSE(BULK_PURCHASE)
                ! End Amendment BE(16/06/03)
            END
        END
    END
    CLOSE(CENTRE_CODES)
  END




SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateVirtualWarehouse',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_res_id',save_res_id,'UpdateVirtualWarehouse',1)
    SolaceViewVars('save_sto_id',save_sto_id,'UpdateVirtualWarehouse',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
