

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01002.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SBG01001.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('SBG01003.INC'),ONCE        !Req'd for module callout resolution
                     END


Update_Retail_Sales PROCEDURE (func:OrderNumber)      !Generated from procedure template - Window

CurrentTab           STRING(80)
tmp:CreateInvoice    BYTE(0)
stop_on_errors_temp  BYTE
sav:ref_number       LONG
Parts_Queue_Temp     QUEUE,PRE(PARTMP)
Part_Ref_Number      REAL
Pending_Ref_Number   REAL
Quantity             REAL
                     END
ref_number_temp      LONG
save_res_id          USHORT,AUTO
save_rtp_id          USHORT,AUTO
vat_rate_temp        REAL
Payment_Method_Temp  STRING(3)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::ret:Record  LIKE(ret:RECORD),STATIC
hMenu                LONG
menuItemCount        LONG
hWnd                 LONG
pos                  LONG
mInfo                LIKE(MENUITEMINFO)
MenuType             CSTRING(260)
QuickWindow          WINDOW('Update the RETSALES File'),AT(,,443,260),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Update_Retail_Sales'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,436,88),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Prompt 1'),AT(8,8,256,12),USE(?Prompt1),FONT('Tahoma',8,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Prompt 2'),AT(284,8,152,12),USE(?Prompt2),RIGHT,FONT('Tahoma',8,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Account Number'),AT(8,24),USE(?RET:Account_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,24,124,10),USE(ret:Account_Number),LEFT,FONT('Tahoma',8,,FONT:bold),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),REQ,UPR
                           BUTTON,AT(212,24,10,10),USE(?LookupAccountNumber),SKIP,FONT('Tahoma',8,,,CHARSET:ANSI),ICON('List3.ico')
                           PROMPT('Purchase Order No'),AT(8,48),USE(?RET:Purchase_Order_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,48,124,10),USE(ret:Purchase_Order_Number),LEFT,FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Contact Name'),AT(8,72),USE(?RET:Contact_Name:Prompt),TRN
                           ENTRY(@s30),AT(84,72,124,10),USE(ret:Contact_Name),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           OPTION('Delivery Or Collection'),AT(228,24,124,28),USE(ret:Delivery_Collection),BOXED,COLOR(COLOR:Silver)
                             RADIO('Delivery'),AT(240,36),USE(?RET:Delivery_Collection:Radio1),VALUE('DEL')
                             RADIO('Collection'),AT(300,36),USE(?RET:Delivery_Collection:Radio2),VALUE('COL')
                           END
                           PROMPT('Courier Cost'),AT(356,28),USE(?RET:Courier_Cost:Prompt),TRN,HIDE,FONT(,7,,)
                           ENTRY(@n14.2),AT(356,40,64,10),USE(ret:Courier_Cost),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           OPTION('Payment Method'),AT(228,56,192,28),USE(ret:Payment_Method),BOXED,COLOR(COLOR:Silver)
                             RADIO('Trade'),AT(240,68),USE(?RET:Payment_Method:Radio1),SKIP,VALUE('TRA')
                             RADIO('Cash'),AT(288,68),USE(?RET:Payment_Method:Radio2),SKIP,VALUE('CAS')
                             RADIO('Exchange Accessory'),AT(332,68),USE(?ret:Payment_Method:Radio3),VALUE('EXC')
                           END
                         END
                       END
                       SHEET,AT(4,96,220,132),USE(?Sheet2),SPREAD
                         TAB('Invoice Address'),USE(?Tab2)
                           PROMPT('Company Name'),AT(8,116),USE(?RET:Company_Name:Prompt),TRN
                           ENTRY(@s30),AT(84,116,124,10),USE(ret:Company_Name),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Postcode'),AT(8,128),USE(?RET:Postcode:Prompt),TRN
                           ENTRY(@s10),AT(84,128,64,10),USE(ret:Postcode),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Building Name/No'),AT(8,140),USE(?RET:Building_Name:Prompt),TRN
                           ENTRY(@s30),AT(84,140,124,10),USE(ret:Building_Name),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Address'),AT(8,156),USE(?RET:Address_Line1:Prompt),TRN
                           ENTRY(@s30),AT(84,156,124,10),USE(ret:Address_Line1),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(84,168,124,10),USE(ret:Address_Line2),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(84,180,124,10),USE(ret:Address_Line3),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Telephone Number'),AT(8,196),USE(?RET:Telephone_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,196,124,10),USE(ret:Telephone_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fax Number'),AT(8,212),USE(?RET:Fax_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,212,124,10),USE(ret:Fax_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       SHEET,AT(228,96,212,132),USE(?Delivery_Sheet),WIZARD,SPREAD
                         TAB('Delivery Address'),USE(?Tab3)
                           BUTTON('&Replicate Invoice Address'),AT(308,100,124,12),USE(?Replicate),LEFT,ICON('arrow.ico')
                           PROMPT('Company Name'),AT(232,116),USE(?RET:Company_Name_Delivery:Prompt),TRN
                           ENTRY(@s30),AT(308,116,124,10),USE(ret:Company_Name_Delivery),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Postcode'),AT(232,128),USE(?RET:Postcode_Delivery:Prompt),TRN,LEFT
                           ENTRY(@s10),AT(308,128,64,10),USE(ret:Postcode_Delivery),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Building Name/No'),AT(232,140),USE(?RET:Building_Name_Delivery:Prompt),TRN
                           ENTRY(@s30),AT(308,140,124,10),USE(ret:Building_Name_Delivery),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Address'),AT(232,156),USE(?RET:Address_Line1_Delivery:Prompt)
                           ENTRY(@s30),AT(308,156,124,10),USE(ret:Address_Line1_Delivery),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(308,168,124,10),USE(ret:Address_Line2_Delivery),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Delivery Address'),AT(232,100),USE(?Prompt19)
                           ENTRY(@s30),AT(308,180,124,10),USE(ret:Address_Line3_Delivery),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Telephone Number'),AT(232,196),USE(?RET:Telephone_Delivery:Prompt),TRN
                           ENTRY(@s15),AT(308,196,124,10),USE(ret:Telephone_Delivery),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fax Number'),AT(232,212),USE(?RET:Fax_Number_Delivery:Prompt),TRN
                           ENTRY(@s15),AT(308,212,124,10),USE(ret:Fax_Number_Delivery),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       BUTTON('&Stock Enquiry'),AT(8,236,64,16),USE(?StockEnquiry),LEFT,ICON('stock_sm.gif')
                       BUTTON('&Payment'),AT(76,236,56,16),USE(?Payment),HIDE,LEFT,ICON('money2.gif')
                       BUTTON('De&livery Text'),AT(136,236,56,16),USE(?Delivery_Text),LEFT,ICON('History.gif')
                       BUTTON('&Invoice Text'),AT(196,236,56,16),USE(?Invoice_Text),LEFT,ICON('History.gif')
                       BUTTON('&OK'),AT(324,236,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(380,236,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,232,436,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
look:ret:Account_Number                Like(ret:Account_Number)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?RET:Account_Number:Prompt{prop:FontColor} = -1
    ?RET:Account_Number:Prompt{prop:Color} = 15066597
    If ?ret:Account_Number{prop:ReadOnly} = True
        ?ret:Account_Number{prop:FontColor} = 65793
        ?ret:Account_Number{prop:Color} = 15066597
    Elsif ?ret:Account_Number{prop:Req} = True
        ?ret:Account_Number{prop:FontColor} = 65793
        ?ret:Account_Number{prop:Color} = 8454143
    Else ! If ?ret:Account_Number{prop:Req} = True
        ?ret:Account_Number{prop:FontColor} = 65793
        ?ret:Account_Number{prop:Color} = 16777215
    End ! If ?ret:Account_Number{prop:Req} = True
    ?ret:Account_Number{prop:Trn} = 0
    ?ret:Account_Number{prop:FontStyle} = font:Bold
    ?RET:Purchase_Order_Number:Prompt{prop:FontColor} = -1
    ?RET:Purchase_Order_Number:Prompt{prop:Color} = 15066597
    If ?ret:Purchase_Order_Number{prop:ReadOnly} = True
        ?ret:Purchase_Order_Number{prop:FontColor} = 65793
        ?ret:Purchase_Order_Number{prop:Color} = 15066597
    Elsif ?ret:Purchase_Order_Number{prop:Req} = True
        ?ret:Purchase_Order_Number{prop:FontColor} = 65793
        ?ret:Purchase_Order_Number{prop:Color} = 8454143
    Else ! If ?ret:Purchase_Order_Number{prop:Req} = True
        ?ret:Purchase_Order_Number{prop:FontColor} = 65793
        ?ret:Purchase_Order_Number{prop:Color} = 16777215
    End ! If ?ret:Purchase_Order_Number{prop:Req} = True
    ?ret:Purchase_Order_Number{prop:Trn} = 0
    ?ret:Purchase_Order_Number{prop:FontStyle} = font:Bold
    ?RET:Contact_Name:Prompt{prop:FontColor} = -1
    ?RET:Contact_Name:Prompt{prop:Color} = 15066597
    If ?ret:Contact_Name{prop:ReadOnly} = True
        ?ret:Contact_Name{prop:FontColor} = 65793
        ?ret:Contact_Name{prop:Color} = 15066597
    Elsif ?ret:Contact_Name{prop:Req} = True
        ?ret:Contact_Name{prop:FontColor} = 65793
        ?ret:Contact_Name{prop:Color} = 8454143
    Else ! If ?ret:Contact_Name{prop:Req} = True
        ?ret:Contact_Name{prop:FontColor} = 65793
        ?ret:Contact_Name{prop:Color} = 16777215
    End ! If ?ret:Contact_Name{prop:Req} = True
    ?ret:Contact_Name{prop:Trn} = 0
    ?ret:Contact_Name{prop:FontStyle} = font:Bold
    ?ret:Delivery_Collection{prop:Font,3} = -1
    ?ret:Delivery_Collection{prop:Color} = 15066597
    ?ret:Delivery_Collection{prop:Trn} = 0
    ?RET:Delivery_Collection:Radio1{prop:Font,3} = -1
    ?RET:Delivery_Collection:Radio1{prop:Color} = 15066597
    ?RET:Delivery_Collection:Radio1{prop:Trn} = 0
    ?RET:Delivery_Collection:Radio2{prop:Font,3} = -1
    ?RET:Delivery_Collection:Radio2{prop:Color} = 15066597
    ?RET:Delivery_Collection:Radio2{prop:Trn} = 0
    ?RET:Courier_Cost:Prompt{prop:FontColor} = -1
    ?RET:Courier_Cost:Prompt{prop:Color} = 15066597
    If ?ret:Courier_Cost{prop:ReadOnly} = True
        ?ret:Courier_Cost{prop:FontColor} = 65793
        ?ret:Courier_Cost{prop:Color} = 15066597
    Elsif ?ret:Courier_Cost{prop:Req} = True
        ?ret:Courier_Cost{prop:FontColor} = 65793
        ?ret:Courier_Cost{prop:Color} = 8454143
    Else ! If ?ret:Courier_Cost{prop:Req} = True
        ?ret:Courier_Cost{prop:FontColor} = 65793
        ?ret:Courier_Cost{prop:Color} = 16777215
    End ! If ?ret:Courier_Cost{prop:Req} = True
    ?ret:Courier_Cost{prop:Trn} = 0
    ?ret:Courier_Cost{prop:FontStyle} = font:Bold
    ?ret:Payment_Method{prop:Font,3} = -1
    ?ret:Payment_Method{prop:Color} = 15066597
    ?ret:Payment_Method{prop:Trn} = 0
    ?RET:Payment_Method:Radio1{prop:Font,3} = -1
    ?RET:Payment_Method:Radio1{prop:Color} = 15066597
    ?RET:Payment_Method:Radio1{prop:Trn} = 0
    ?RET:Payment_Method:Radio2{prop:Font,3} = -1
    ?RET:Payment_Method:Radio2{prop:Color} = 15066597
    ?RET:Payment_Method:Radio2{prop:Trn} = 0
    ?ret:Payment_Method:Radio3{prop:Font,3} = -1
    ?ret:Payment_Method:Radio3{prop:Color} = 15066597
    ?ret:Payment_Method:Radio3{prop:Trn} = 0
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?RET:Company_Name:Prompt{prop:FontColor} = -1
    ?RET:Company_Name:Prompt{prop:Color} = 15066597
    If ?ret:Company_Name{prop:ReadOnly} = True
        ?ret:Company_Name{prop:FontColor} = 65793
        ?ret:Company_Name{prop:Color} = 15066597
    Elsif ?ret:Company_Name{prop:Req} = True
        ?ret:Company_Name{prop:FontColor} = 65793
        ?ret:Company_Name{prop:Color} = 8454143
    Else ! If ?ret:Company_Name{prop:Req} = True
        ?ret:Company_Name{prop:FontColor} = 65793
        ?ret:Company_Name{prop:Color} = 16777215
    End ! If ?ret:Company_Name{prop:Req} = True
    ?ret:Company_Name{prop:Trn} = 0
    ?ret:Company_Name{prop:FontStyle} = font:Bold
    ?RET:Postcode:Prompt{prop:FontColor} = -1
    ?RET:Postcode:Prompt{prop:Color} = 15066597
    If ?ret:Postcode{prop:ReadOnly} = True
        ?ret:Postcode{prop:FontColor} = 65793
        ?ret:Postcode{prop:Color} = 15066597
    Elsif ?ret:Postcode{prop:Req} = True
        ?ret:Postcode{prop:FontColor} = 65793
        ?ret:Postcode{prop:Color} = 8454143
    Else ! If ?ret:Postcode{prop:Req} = True
        ?ret:Postcode{prop:FontColor} = 65793
        ?ret:Postcode{prop:Color} = 16777215
    End ! If ?ret:Postcode{prop:Req} = True
    ?ret:Postcode{prop:Trn} = 0
    ?ret:Postcode{prop:FontStyle} = font:Bold
    ?RET:Building_Name:Prompt{prop:FontColor} = -1
    ?RET:Building_Name:Prompt{prop:Color} = 15066597
    If ?ret:Building_Name{prop:ReadOnly} = True
        ?ret:Building_Name{prop:FontColor} = 65793
        ?ret:Building_Name{prop:Color} = 15066597
    Elsif ?ret:Building_Name{prop:Req} = True
        ?ret:Building_Name{prop:FontColor} = 65793
        ?ret:Building_Name{prop:Color} = 8454143
    Else ! If ?ret:Building_Name{prop:Req} = True
        ?ret:Building_Name{prop:FontColor} = 65793
        ?ret:Building_Name{prop:Color} = 16777215
    End ! If ?ret:Building_Name{prop:Req} = True
    ?ret:Building_Name{prop:Trn} = 0
    ?ret:Building_Name{prop:FontStyle} = font:Bold
    ?RET:Address_Line1:Prompt{prop:FontColor} = -1
    ?RET:Address_Line1:Prompt{prop:Color} = 15066597
    If ?ret:Address_Line1{prop:ReadOnly} = True
        ?ret:Address_Line1{prop:FontColor} = 65793
        ?ret:Address_Line1{prop:Color} = 15066597
    Elsif ?ret:Address_Line1{prop:Req} = True
        ?ret:Address_Line1{prop:FontColor} = 65793
        ?ret:Address_Line1{prop:Color} = 8454143
    Else ! If ?ret:Address_Line1{prop:Req} = True
        ?ret:Address_Line1{prop:FontColor} = 65793
        ?ret:Address_Line1{prop:Color} = 16777215
    End ! If ?ret:Address_Line1{prop:Req} = True
    ?ret:Address_Line1{prop:Trn} = 0
    ?ret:Address_Line1{prop:FontStyle} = font:Bold
    If ?ret:Address_Line2{prop:ReadOnly} = True
        ?ret:Address_Line2{prop:FontColor} = 65793
        ?ret:Address_Line2{prop:Color} = 15066597
    Elsif ?ret:Address_Line2{prop:Req} = True
        ?ret:Address_Line2{prop:FontColor} = 65793
        ?ret:Address_Line2{prop:Color} = 8454143
    Else ! If ?ret:Address_Line2{prop:Req} = True
        ?ret:Address_Line2{prop:FontColor} = 65793
        ?ret:Address_Line2{prop:Color} = 16777215
    End ! If ?ret:Address_Line2{prop:Req} = True
    ?ret:Address_Line2{prop:Trn} = 0
    ?ret:Address_Line2{prop:FontStyle} = font:Bold
    If ?ret:Address_Line3{prop:ReadOnly} = True
        ?ret:Address_Line3{prop:FontColor} = 65793
        ?ret:Address_Line3{prop:Color} = 15066597
    Elsif ?ret:Address_Line3{prop:Req} = True
        ?ret:Address_Line3{prop:FontColor} = 65793
        ?ret:Address_Line3{prop:Color} = 8454143
    Else ! If ?ret:Address_Line3{prop:Req} = True
        ?ret:Address_Line3{prop:FontColor} = 65793
        ?ret:Address_Line3{prop:Color} = 16777215
    End ! If ?ret:Address_Line3{prop:Req} = True
    ?ret:Address_Line3{prop:Trn} = 0
    ?ret:Address_Line3{prop:FontStyle} = font:Bold
    ?RET:Telephone_Number:Prompt{prop:FontColor} = -1
    ?RET:Telephone_Number:Prompt{prop:Color} = 15066597
    If ?ret:Telephone_Number{prop:ReadOnly} = True
        ?ret:Telephone_Number{prop:FontColor} = 65793
        ?ret:Telephone_Number{prop:Color} = 15066597
    Elsif ?ret:Telephone_Number{prop:Req} = True
        ?ret:Telephone_Number{prop:FontColor} = 65793
        ?ret:Telephone_Number{prop:Color} = 8454143
    Else ! If ?ret:Telephone_Number{prop:Req} = True
        ?ret:Telephone_Number{prop:FontColor} = 65793
        ?ret:Telephone_Number{prop:Color} = 16777215
    End ! If ?ret:Telephone_Number{prop:Req} = True
    ?ret:Telephone_Number{prop:Trn} = 0
    ?ret:Telephone_Number{prop:FontStyle} = font:Bold
    ?RET:Fax_Number:Prompt{prop:FontColor} = -1
    ?RET:Fax_Number:Prompt{prop:Color} = 15066597
    If ?ret:Fax_Number{prop:ReadOnly} = True
        ?ret:Fax_Number{prop:FontColor} = 65793
        ?ret:Fax_Number{prop:Color} = 15066597
    Elsif ?ret:Fax_Number{prop:Req} = True
        ?ret:Fax_Number{prop:FontColor} = 65793
        ?ret:Fax_Number{prop:Color} = 8454143
    Else ! If ?ret:Fax_Number{prop:Req} = True
        ?ret:Fax_Number{prop:FontColor} = 65793
        ?ret:Fax_Number{prop:Color} = 16777215
    End ! If ?ret:Fax_Number{prop:Req} = True
    ?ret:Fax_Number{prop:Trn} = 0
    ?ret:Fax_Number{prop:FontStyle} = font:Bold
    ?Delivery_Sheet{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?RET:Company_Name_Delivery:Prompt{prop:FontColor} = -1
    ?RET:Company_Name_Delivery:Prompt{prop:Color} = 15066597
    If ?ret:Company_Name_Delivery{prop:ReadOnly} = True
        ?ret:Company_Name_Delivery{prop:FontColor} = 65793
        ?ret:Company_Name_Delivery{prop:Color} = 15066597
    Elsif ?ret:Company_Name_Delivery{prop:Req} = True
        ?ret:Company_Name_Delivery{prop:FontColor} = 65793
        ?ret:Company_Name_Delivery{prop:Color} = 8454143
    Else ! If ?ret:Company_Name_Delivery{prop:Req} = True
        ?ret:Company_Name_Delivery{prop:FontColor} = 65793
        ?ret:Company_Name_Delivery{prop:Color} = 16777215
    End ! If ?ret:Company_Name_Delivery{prop:Req} = True
    ?ret:Company_Name_Delivery{prop:Trn} = 0
    ?ret:Company_Name_Delivery{prop:FontStyle} = font:Bold
    ?RET:Postcode_Delivery:Prompt{prop:FontColor} = -1
    ?RET:Postcode_Delivery:Prompt{prop:Color} = 15066597
    If ?ret:Postcode_Delivery{prop:ReadOnly} = True
        ?ret:Postcode_Delivery{prop:FontColor} = 65793
        ?ret:Postcode_Delivery{prop:Color} = 15066597
    Elsif ?ret:Postcode_Delivery{prop:Req} = True
        ?ret:Postcode_Delivery{prop:FontColor} = 65793
        ?ret:Postcode_Delivery{prop:Color} = 8454143
    Else ! If ?ret:Postcode_Delivery{prop:Req} = True
        ?ret:Postcode_Delivery{prop:FontColor} = 65793
        ?ret:Postcode_Delivery{prop:Color} = 16777215
    End ! If ?ret:Postcode_Delivery{prop:Req} = True
    ?ret:Postcode_Delivery{prop:Trn} = 0
    ?ret:Postcode_Delivery{prop:FontStyle} = font:Bold
    ?RET:Building_Name_Delivery:Prompt{prop:FontColor} = -1
    ?RET:Building_Name_Delivery:Prompt{prop:Color} = 15066597
    If ?ret:Building_Name_Delivery{prop:ReadOnly} = True
        ?ret:Building_Name_Delivery{prop:FontColor} = 65793
        ?ret:Building_Name_Delivery{prop:Color} = 15066597
    Elsif ?ret:Building_Name_Delivery{prop:Req} = True
        ?ret:Building_Name_Delivery{prop:FontColor} = 65793
        ?ret:Building_Name_Delivery{prop:Color} = 8454143
    Else ! If ?ret:Building_Name_Delivery{prop:Req} = True
        ?ret:Building_Name_Delivery{prop:FontColor} = 65793
        ?ret:Building_Name_Delivery{prop:Color} = 16777215
    End ! If ?ret:Building_Name_Delivery{prop:Req} = True
    ?ret:Building_Name_Delivery{prop:Trn} = 0
    ?ret:Building_Name_Delivery{prop:FontStyle} = font:Bold
    ?RET:Address_Line1_Delivery:Prompt{prop:FontColor} = -1
    ?RET:Address_Line1_Delivery:Prompt{prop:Color} = 15066597
    If ?ret:Address_Line1_Delivery{prop:ReadOnly} = True
        ?ret:Address_Line1_Delivery{prop:FontColor} = 65793
        ?ret:Address_Line1_Delivery{prop:Color} = 15066597
    Elsif ?ret:Address_Line1_Delivery{prop:Req} = True
        ?ret:Address_Line1_Delivery{prop:FontColor} = 65793
        ?ret:Address_Line1_Delivery{prop:Color} = 8454143
    Else ! If ?ret:Address_Line1_Delivery{prop:Req} = True
        ?ret:Address_Line1_Delivery{prop:FontColor} = 65793
        ?ret:Address_Line1_Delivery{prop:Color} = 16777215
    End ! If ?ret:Address_Line1_Delivery{prop:Req} = True
    ?ret:Address_Line1_Delivery{prop:Trn} = 0
    ?ret:Address_Line1_Delivery{prop:FontStyle} = font:Bold
    If ?ret:Address_Line2_Delivery{prop:ReadOnly} = True
        ?ret:Address_Line2_Delivery{prop:FontColor} = 65793
        ?ret:Address_Line2_Delivery{prop:Color} = 15066597
    Elsif ?ret:Address_Line2_Delivery{prop:Req} = True
        ?ret:Address_Line2_Delivery{prop:FontColor} = 65793
        ?ret:Address_Line2_Delivery{prop:Color} = 8454143
    Else ! If ?ret:Address_Line2_Delivery{prop:Req} = True
        ?ret:Address_Line2_Delivery{prop:FontColor} = 65793
        ?ret:Address_Line2_Delivery{prop:Color} = 16777215
    End ! If ?ret:Address_Line2_Delivery{prop:Req} = True
    ?ret:Address_Line2_Delivery{prop:Trn} = 0
    ?ret:Address_Line2_Delivery{prop:FontStyle} = font:Bold
    ?Prompt19{prop:FontColor} = -1
    ?Prompt19{prop:Color} = 15066597
    If ?ret:Address_Line3_Delivery{prop:ReadOnly} = True
        ?ret:Address_Line3_Delivery{prop:FontColor} = 65793
        ?ret:Address_Line3_Delivery{prop:Color} = 15066597
    Elsif ?ret:Address_Line3_Delivery{prop:Req} = True
        ?ret:Address_Line3_Delivery{prop:FontColor} = 65793
        ?ret:Address_Line3_Delivery{prop:Color} = 8454143
    Else ! If ?ret:Address_Line3_Delivery{prop:Req} = True
        ?ret:Address_Line3_Delivery{prop:FontColor} = 65793
        ?ret:Address_Line3_Delivery{prop:Color} = 16777215
    End ! If ?ret:Address_Line3_Delivery{prop:Req} = True
    ?ret:Address_Line3_Delivery{prop:Trn} = 0
    ?ret:Address_Line3_Delivery{prop:FontStyle} = font:Bold
    ?RET:Telephone_Delivery:Prompt{prop:FontColor} = -1
    ?RET:Telephone_Delivery:Prompt{prop:Color} = 15066597
    If ?ret:Telephone_Delivery{prop:ReadOnly} = True
        ?ret:Telephone_Delivery{prop:FontColor} = 65793
        ?ret:Telephone_Delivery{prop:Color} = 15066597
    Elsif ?ret:Telephone_Delivery{prop:Req} = True
        ?ret:Telephone_Delivery{prop:FontColor} = 65793
        ?ret:Telephone_Delivery{prop:Color} = 8454143
    Else ! If ?ret:Telephone_Delivery{prop:Req} = True
        ?ret:Telephone_Delivery{prop:FontColor} = 65793
        ?ret:Telephone_Delivery{prop:Color} = 16777215
    End ! If ?ret:Telephone_Delivery{prop:Req} = True
    ?ret:Telephone_Delivery{prop:Trn} = 0
    ?ret:Telephone_Delivery{prop:FontStyle} = font:Bold
    ?RET:Fax_Number_Delivery:Prompt{prop:FontColor} = -1
    ?RET:Fax_Number_Delivery:Prompt{prop:Color} = 15066597
    If ?ret:Fax_Number_Delivery{prop:ReadOnly} = True
        ?ret:Fax_Number_Delivery{prop:FontColor} = 65793
        ?ret:Fax_Number_Delivery{prop:Color} = 15066597
    Elsif ?ret:Fax_Number_Delivery{prop:Req} = True
        ?ret:Fax_Number_Delivery{prop:FontColor} = 65793
        ?ret:Fax_Number_Delivery{prop:Color} = 8454143
    Else ! If ?ret:Fax_Number_Delivery{prop:Req} = True
        ?ret:Fax_Number_Delivery{prop:FontColor} = 65793
        ?ret:Fax_Number_Delivery{prop:Color} = 16777215
    End ! If ?ret:Fax_Number_Delivery{prop:Req} = True
    ?ret:Fax_Number_Delivery{prop:Trn} = 0
    ?ret:Fax_Number_Delivery{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Retail_Sales',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Retail_Sales',1)
    SolaceViewVars('tmp:CreateInvoice',tmp:CreateInvoice,'Update_Retail_Sales',1)
    SolaceViewVars('stop_on_errors_temp',stop_on_errors_temp,'Update_Retail_Sales',1)
    SolaceViewVars('sav:ref_number',sav:ref_number,'Update_Retail_Sales',1)
    SolaceViewVars('Parts_Queue_Temp:Part_Ref_Number',Parts_Queue_Temp:Part_Ref_Number,'Update_Retail_Sales',1)
    SolaceViewVars('Parts_Queue_Temp:Pending_Ref_Number',Parts_Queue_Temp:Pending_Ref_Number,'Update_Retail_Sales',1)
    SolaceViewVars('Parts_Queue_Temp:Quantity',Parts_Queue_Temp:Quantity,'Update_Retail_Sales',1)
    SolaceViewVars('ref_number_temp',ref_number_temp,'Update_Retail_Sales',1)
    SolaceViewVars('save_res_id',save_res_id,'Update_Retail_Sales',1)
    SolaceViewVars('save_rtp_id',save_rtp_id,'Update_Retail_Sales',1)
    SolaceViewVars('vat_rate_temp',vat_rate_temp,'Update_Retail_Sales',1)
    SolaceViewVars('Payment_Method_Temp',Payment_Method_Temp,'Update_Retail_Sales',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Retail_Sales',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Account_Number:Prompt;  SolaceCtrlName = '?RET:Account_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Account_Number;  SolaceCtrlName = '?ret:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupAccountNumber;  SolaceCtrlName = '?LookupAccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Purchase_Order_Number:Prompt;  SolaceCtrlName = '?RET:Purchase_Order_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Purchase_Order_Number;  SolaceCtrlName = '?ret:Purchase_Order_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Contact_Name:Prompt;  SolaceCtrlName = '?RET:Contact_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Contact_Name;  SolaceCtrlName = '?ret:Contact_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Delivery_Collection;  SolaceCtrlName = '?ret:Delivery_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Delivery_Collection:Radio1;  SolaceCtrlName = '?RET:Delivery_Collection:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Delivery_Collection:Radio2;  SolaceCtrlName = '?RET:Delivery_Collection:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Courier_Cost:Prompt;  SolaceCtrlName = '?RET:Courier_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Courier_Cost;  SolaceCtrlName = '?ret:Courier_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Payment_Method;  SolaceCtrlName = '?ret:Payment_Method';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Payment_Method:Radio1;  SolaceCtrlName = '?RET:Payment_Method:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Payment_Method:Radio2;  SolaceCtrlName = '?RET:Payment_Method:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Payment_Method:Radio3;  SolaceCtrlName = '?ret:Payment_Method:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Company_Name:Prompt;  SolaceCtrlName = '?RET:Company_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Company_Name;  SolaceCtrlName = '?ret:Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Postcode:Prompt;  SolaceCtrlName = '?RET:Postcode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Postcode;  SolaceCtrlName = '?ret:Postcode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Building_Name:Prompt;  SolaceCtrlName = '?RET:Building_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Building_Name;  SolaceCtrlName = '?ret:Building_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Address_Line1:Prompt;  SolaceCtrlName = '?RET:Address_Line1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Address_Line1;  SolaceCtrlName = '?ret:Address_Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Address_Line2;  SolaceCtrlName = '?ret:Address_Line2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Address_Line3;  SolaceCtrlName = '?ret:Address_Line3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Telephone_Number:Prompt;  SolaceCtrlName = '?RET:Telephone_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Telephone_Number;  SolaceCtrlName = '?ret:Telephone_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Fax_Number:Prompt;  SolaceCtrlName = '?RET:Fax_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Fax_Number;  SolaceCtrlName = '?ret:Fax_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delivery_Sheet;  SolaceCtrlName = '?Delivery_Sheet';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Replicate;  SolaceCtrlName = '?Replicate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Company_Name_Delivery:Prompt;  SolaceCtrlName = '?RET:Company_Name_Delivery:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Company_Name_Delivery;  SolaceCtrlName = '?ret:Company_Name_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Postcode_Delivery:Prompt;  SolaceCtrlName = '?RET:Postcode_Delivery:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Postcode_Delivery;  SolaceCtrlName = '?ret:Postcode_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Building_Name_Delivery:Prompt;  SolaceCtrlName = '?RET:Building_Name_Delivery:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Building_Name_Delivery;  SolaceCtrlName = '?ret:Building_Name_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Address_Line1_Delivery:Prompt;  SolaceCtrlName = '?RET:Address_Line1_Delivery:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Address_Line1_Delivery;  SolaceCtrlName = '?ret:Address_Line1_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Address_Line2_Delivery;  SolaceCtrlName = '?ret:Address_Line2_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt19;  SolaceCtrlName = '?Prompt19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Address_Line3_Delivery;  SolaceCtrlName = '?ret:Address_Line3_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Telephone_Delivery:Prompt;  SolaceCtrlName = '?RET:Telephone_Delivery:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Telephone_Delivery;  SolaceCtrlName = '?ret:Telephone_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Fax_Number_Delivery:Prompt;  SolaceCtrlName = '?RET:Fax_Number_Delivery:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Fax_Number_Delivery;  SolaceCtrlName = '?ret:Fax_Number_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StockEnquiry;  SolaceCtrlName = '?StockEnquiry';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Payment;  SolaceCtrlName = '?Payment';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delivery_Text;  SolaceCtrlName = '?Delivery_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Invoice_Text;  SolaceCtrlName = '?Invoice_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Retail Sale'
  OF ChangeRecord
    GlobalErrors.Throw(Msg:UpdateIllegal)
    RETURN
  OF DeleteRecord
    GlobalErrors.Throw(Msg:DeleteIllegal)
    RETURN
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  Globalrequest   = Insertrecord
  
  GlobalErrors.SetProcedureName('Update_Retail_Sales')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Retail_Sales')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(ret:Record,History::ret:Record)
  SELF.AddHistoryField(?ret:Account_Number,5)
  SELF.AddHistoryField(?ret:Purchase_Order_Number,7)
  SELF.AddHistoryField(?ret:Contact_Name,6)
  SELF.AddHistoryField(?ret:Delivery_Collection,8)
  SELF.AddHistoryField(?ret:Courier_Cost,10)
  SELF.AddHistoryField(?ret:Payment_Method,9)
  SELF.AddHistoryField(?ret:Company_Name,28)
  SELF.AddHistoryField(?ret:Postcode,27)
  SELF.AddHistoryField(?ret:Building_Name,29)
  SELF.AddHistoryField(?ret:Address_Line1,30)
  SELF.AddHistoryField(?ret:Address_Line2,31)
  SELF.AddHistoryField(?ret:Address_Line3,32)
  SELF.AddHistoryField(?ret:Telephone_Number,33)
  SELF.AddHistoryField(?ret:Fax_Number,34)
  SELF.AddHistoryField(?ret:Company_Name_Delivery,36)
  SELF.AddHistoryField(?ret:Postcode_Delivery,35)
  SELF.AddHistoryField(?ret:Building_Name_Delivery,37)
  SELF.AddHistoryField(?ret:Address_Line1_Delivery,38)
  SELF.AddHistoryField(?ret:Address_Line2_Delivery,39)
  SELF.AddHistoryField(?ret:Address_Line3_Delivery,40)
  SELF.AddHistoryField(?ret:Telephone_Delivery,41)
  SELF.AddHistoryField(?ret:Fax_Number_Delivery,42)
  SELF.AddUpdateFile(Access:RETSALES)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:ORDHEAD.Open
  Relate:ORDPEND.Open
  Relate:RETPAY.Open
  Relate:RETSALES_ALIAS.Open
  Relate:VATCODE.Open
  Access:USERS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:RETSTOCK.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:RETSALES
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  If func:OrderNumber <> 0
      Access:ORDHEAD.ClearKey(orh:KeyOrder_no)
      orh:Order_no = func:OrderNumber
      If Access:ordhead.TryFetch(orh:KeyOrder_no) = Level:Benign
          !Found
          ret:Account_Number         = orh:Account_No
          ret:Contact_Name           = orh:Department
          ret:Purchase_Order_Number  = orh:CustOrderNumber
          ret:WebOrderNumber         = orh:Order_No
          ret:WebDateCreated         = orh:TheDate
          ret:WebTimeCreated         = orh:TheTime
          ret:WebCreatedUser         = orh:WhoBooked
          ret:Postcode               = orh:CustPostCode
          ret:Company_Name           = orh:CustName
          ret:Building_Name          = ''
          ret:Address_Line1          = orh:CustAdd1
          ret:Address_Line2          = orh:CustAdd2
          ret:Address_Line3          = orh:CustAdd3
          ret:Telephone_Number       = orh:CustTel
          ret:Fax_Number             = orh:CustFax
          ret:Postcode_Delivery      = orh:dPostCode
          ret:Company_Name_Delivery  = orh:dName
          ret:Building_Name_Delivery = ''
          ret:Address_Line1_Delivery = orh:dAdd1
          ret:Address_Line2_Delivery = orh:dAdd2
          ret:Address_Line3_Delivery = orh:dAdd3
          ret:Telephone_Delivery     = orh:dTel
          ret:Fax_Number_Delivery    = orh:dFax
          ret:Delivery_Text          = ''
          ret:Invoice_Text           = ''
  
      Else!If Access:ordhead.TryFetch(orh:KeyOrder_no) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:ordhead.TryFetch(orh:KeyOrder_no) = Level:Benign
  End !func:OrderNumber <> 0
  OPEN(QuickWindow)
  SELF.Opened=True
  IF thiswindow.request   = Insertrecord
      access:users.clearkey(use:password_key)
      use:password =glo:password
      access:users.fetch(use:password_key)
      ret:who_booked  = use:user_code
  Else!IF thiswindow.request   = Insertrecord
      access:users.clearkey(use:user_code_key)
      use:user_code = ret:who_booked
      access:users.fetch(use:user_code_key)
  End!IF thiswindow.request   = Insertrecord
  ?prompt1{prop:text} = 'Sales No: ' & CLip(ret:ref_number) & '   User: ' & Clip(use:forename) & ' ' & CLip(use:surname)
  ?prompt2{prop:text} = 'Date Raised: ' & Format(ret:date_booked,@d6) & '  ' & Format(ret:time_booked,@t1)
  
  If func:OrderNumber <> 0
      Post(Event:Accepted,?ret:Account_Number)
      ?StockEnquiry{prop:Text} = 'Web Stock Requests'
  End !func:OrderNumber <> 0
  
  
  
  
           
  Do RecolourWindow
     hWnd = 0{PROP:Handle}
     !get the system menu handle
     hMenu = GetSystemMenu(hwnd, 0)
  
     !loop backwards through the menu
     LOOP I# = GetMenuItemCount(hMenu) To 0 BY -1
        minfo.cbSize     = Size(mInfo)
        minfo.fMask      = BOR(MIIM_TYPE,MIIM_ID)
        minfo.fType      = MFT_STRING
        MenuType         = ALL(' ',255)
        minfo.dwTypeData = Address(MenuType)
        mInfo.cch        = Size(MenuType)
        !Retrieve the current MENUITEMINFO.
        !Specifying fByPosition=True indicates
        !uItem points to an item by position.
        !Returns 1 if successful
        If GetMenuItemInfo(hMenu, I#, True, mInfo) = 1 Then
          !The close command has an ID of 61536. Once
          !that has been deleted, in a MDI Child window
          !two separators would remain (in a SDI one
          !separator would remain).
          !
          !To assure that this code will cover both
          !MDI and SDI system menus, 1 is subtracted
          !from the item number (c) to remove either
          !the top separator (of the two that will
          !remain in a MDIChild), or the single SDI
          !one (that would remain if this code was
          !applied against a SDI form.)
           If (mInfo.wID = 61536) Then
              a# = RemoveMenu(hMenu, I#,BOR(MF_REMOVE,MF_BYPOSITION))
              b# = RemoveMenu(hMenu, I#-1,BOR(MF_REMOVE,MF_BYPOSITION))
           End
        End
     END !LOOP
  
     !force a redraw of the non-client
     !area of the form to cause the
     !disabled X to paint correctly
     c# = DrawMenuBar(hwnd)
     Display
  ! support for CPCS
  IF ?ret:Account_Number{Prop:Tip} AND ~?LookupAccountNumber{Prop:Tip}
     ?LookupAccountNumber{Prop:Tip} = 'Select ' & ?ret:Account_Number{Prop:Tip}
  END
  IF ?ret:Account_Number{Prop:Msg} AND ~?LookupAccountNumber{Prop:Msg}
     ?LookupAccountNumber{Prop:Msg} = 'Select ' & ?ret:Account_Number{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  ref_number_temp = ret:ref_number
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:ORDHEAD.Close
    Relate:ORDPEND.Close
    Relate:RETPAY.Close
    Relate:RETSALES_ALIAS.Close
    Relate:VATCODE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Retail_Sales',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  If thiswindow.response  = 2
      If thiswindow.request   = Insertrecord
          access:stock.open()
          access:stock.usefile()
          access:stohist.open()
          access:stohist.usefile()
          access:ordpend.open()
          access:ordpend.usefile()
  
          Clear(parts_queue_temp)
          Loop x# = 1 To Records(parts_queue_temp)
              Get(parts_queue_temp,x#)
              If partmp:pending_ref_number <> ''
                  access:ordpend.clearkey(ope:ref_number_key)
                  ope:ref_number  = partmp:pending_Ref_number
                  If access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                      Delete(ordpend)
                  End!If access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
              Else!If partmp:pending_ref_number <> ''
                  If partmp:part_ref_number <> ''
                      access:stock.clearkey(sto:ref_number_key)
                      sto:ref_number  = partmp:part_ref_number
                      If access:stock.fetch(sto:ref_number_key) = Level:Benign
                          sto:quantity_stock  += partmp:quantity
                          access:stock.update()
                          get(stohist,0)
                          if access:stohist.primerecord() = level:benign
                              shi:ref_number           = sto:ref_number
                              access:users.clearkey(use:password_key)
                              use:password             = glo:password
                              access:users.fetch(use:password_key)
                              shi:user                  = use:user_code    
                              shi:date                 = today()
                              shi:transaction_type     = 'REC'
                              shi:despatch_note_number = ''
                              shi:job_number           = ret:ref_number
                              shi:quantity             = partmp:quantity
                              shi:purchase_cost        = sto:purchase_cost
                              shi:sale_cost            = sto:sale_cost
                              shi:retail_cost          = sto:retail_cost
                              shi:information          = 'SALE CANCELLED'
                              shi:notes                = 'STOCK RECREDITED'
                              if access:stohist.insert()
                                 access:stohist.cancelautoinc()
                              end
                          end!if access:stohist.primerecord() = level:benign
                      End!If access:stock.fetch(sto:ref_number_key) = Level:Benign
                  End!If access:part_ref_number <> ''
              End!If partmp:pending_ref_number <> ''
          End!Loop x# = 1 To Records(parts_queue_temp)
          access:stock.close()
          access:stohist.close()
          access:ordpend.close()
      End!If thiswindow.request   = Insertrecord
  End!If thiswindow.response  = 2
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(PrimeFields, ())
    ret:date_booked = Today()
    ret:time_booked = Clock()
  PARENT.PrimeFields
  access:retsales.primerecord()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(PrimeFields, ())


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickSubAccounts
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?Cancel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      !Save Fields In Case Of Cancel
      sav:ref_number  = ret:ref_number
      
      If thiswindow.request   = Insertrecord
          Clear(parts_queue_temp)
          Free(parts_queue_temp)
          save_res_id = access:retstock.savefile()
          access:retstock.clearkey(res:part_number_key)
          res:ref_number  = ret:ref_number
          set(res:part_number_key,res:part_number_key)
          loop
              if access:retstock.next()
                 break
              end !if
              if res:ref_number  <> ret:ref_number      |
                  then break.  ! end if
              partmp:part_ref_number  = res:part_ref_number
              partmp:quantity        = res:quantity
              partmp:pending_ref_number   = res:pending_ref_number
              Add(Parts_queue_temp)
          end !loop
          access:retstock.restorefile(save_res_id)
      End!If thiswindow.request   = Insertrecord
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ret:Account_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Account_Number, Accepted)
      IF ret:Account_Number OR ?ret:Account_Number{Prop:Req}
        sub:Account_Number = ret:Account_Number
        !Save Lookup Field Incase Of error
        look:ret:Account_Number        = ret:Account_Number
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            ret:Account_Number = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            ret:Account_Number = look:ret:Account_Number
            SELECT(?ret:Account_Number)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      If ~0{prop:acceptall}
          If ret:account_number <> ''
              stop# = 0
              access:tradeacc.clearkey(tra:account_number_key)
              tra:account_number = sub:main_account_number
              if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                  If tra:stop_account = 'YES'
                      stop# = 1
                  End!If tra:stop_account = 'YES'
              End!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
      
              If stop# = 1
                  Case MessageEx('The selected Trade Account is on STOP.<13,10><13,10>A new transaction cannot be created for this customer. Please select another.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                  End!Case MessageEx
                  ret:account_number  = ''
              Else!If stop# = 1
                  ! Start Change 4393 BE(21/06/2004)
                  RET:Postcode        = ''
                  RET:Company_Name    = ''
                  RET:Building_Name   = ''
                  RET:Address_Line1   = ''
                  RET:Address_Line2   = ''
                  RET:Address_Line3   = ''
                  RET:Telephone_Number= ''
                  RET:Fax_Number      = ''
      
                  RET:Postcode_delivery        = ''
                  RET:Company_Name_delivery    = ''
                  RET:Building_Name_delivery   = ''
                  RET:Address_Line1_delivery   = ''
                  RET:Address_Line2_delivery   = ''
                  RET:Address_Line3_delivery   = ''
                  RET:Telephone_delivery       = ''
                  RET:Fax_Number_delivery      = ''
                  ! End Change 4393 BE(21/06/2004)
      
                  if tra:invoice_sub_accounts = 'YES'
                      If SUB:Retail_Payment_Type = 'ACC'
                          ret:payment_method  = 'TRA'
                      End!If SUB:Retail_Payment_Type = 'ACC'
                      IF sub:retail_payment_type  = 'CAS'
                          ret:payment_method = 'CAS'
                      End!IF sub:retail_payment_type  = 'CAS'
      
                      If sub:use_customer_address <> 'YES'
                          RET:Postcode        = sub:postcode
                          RET:Company_Name    = sub:company_name
                          RET:Building_Name   = ''
                          RET:Address_Line1   = sub:address_line1
                          RET:Address_Line2   = sub:address_line2
                          RET:Address_Line3   = sub:address_line3
                          RET:Telephone_Number= sub:telephone_number
                          RET:Fax_Number      = sub:fax_number
                      End!If sub:use_customer_address <> 'YES'
                      RET:Courier_Cost = sub:Courier_Cost
      
                      access:vatcode.clearkey(vat:vat_code_key)
                      vat:vat_code = sub:retail_vat_code
                      if access:vatcode.fetch(vat:vat_code_key) = level:benign
                          vat_rate_temp = vat:vat_rate
                      end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                      access:vatcode.clearkey(vat:vat_code_key)
                      vat:vat_code = sub:retail_vat_code
                      if access:vatcode.fetch(vat:vat_code_key) = level:benign
                          vat_rate_temp = vat:vat_rate
                      end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                  else!if tra:use_sub_accounts = 'YES'
                      If TRA:Retail_Payment_Type = 'ACC'
                          ret:payment_method  = 'TRA'
                      End!If SUB:Retail_Payment_Type = 'ACC'
                      IF TRA:retail_payment_type  = 'CAS'
                          ret:payment_method = 'CAS'
                      End!IF sub:retail_payment_type  = 'CAS'
      
                      RET:Courier_Cost = TRA:Courier_Cost
                      If tra:use_customer_address <> 'YES'
                          RET:Postcode        = tra:postcode
                          RET:Company_Name    = tra:company_name
                          RET:Building_Name   = ''
                          RET:Address_Line1   = tra:address_line1
                          RET:Address_Line2   = tra:address_line2
                          RET:Address_Line3   = tra:address_line3
                          RET:Telephone_Number= tra:telephone_number
                          RET:Fax_Number      = tra:fax_number
                      End!If tra:use_customer_address <> 'YES'
      
                      access:vatcode.clearkey(vat:vat_code_key)
                      vat:vat_code = tra:retail_vat_code
                      if access:vatcode.fetch(vat:vat_code_key) = level:benign
                          vat_rate_temp = vat:vat_rate
                      end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                      access:vatcode.clearkey(vat:vat_code_key)
                      vat:vat_code = tra:retail_vat_code
                      if access:vatcode.fetch(vat:vat_code_key) = level:benign
                          vat_rate_temp = vat:vat_rate
                      end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                  end!if tra:use_sub_accounts = 'YES'
      
                  If tra:use_sub_accounts <> 'YES'
                      If tra:use_delivery_address = 'YES'
                          RET:Postcode_Delivery       = tra:postcode
                          RET:Company_Name_Delivery   = tra:company_name
                          RET:Building_Name_Delivery  = ''
                          RET:Address_Line1_Delivery  = tra:address_line1
                          RET:Address_Line2_Delivery  = tra:address_line2
                          RET:Address_Line3_Delivery  = tra:address_line3
                          RET:Telephone_Delivery      = tra:telephone_number
                          RET:Fax_Number_Delivery     = tra:fax_number
                      End !If tra:use_delivery_address = 'YES'
                      ret:courier = tra:courier_outgoing
                      
                  Else!If tra:use_sub_accounts <> 'YES'
      
                      If sub:use_delivery_address = 'YES'
                          RET:Postcode_delivery        = sub:postcode
                          RET:Company_Name_delivery    = sub:company_name
                          RET:Building_Name_delivery   = ''
                          RET:Address_Line1_delivery   = sub:address_line1
                          RET:Address_Line2_delivery   = sub:address_line2
                          RET:Address_Line3_delivery   = sub:address_line3
                          RET:Telephone_delivery= sub:telephone_number
                          RET:Fax_Number_delivery      = sub:fax_number
                      End
                      ret:courier = sub:courier_outgoing
                      
                  End!If tra:use_sub_accounts <> 'YES'
      
                  payment_method_temp = ret:payment_method
              End!If stop# = 1
              post(event:accepted,?ret:payment_method)
          End!If ret:account_number <> ''
      
          Display()
      End!If ~0{prop:acceptall}
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Account_Number, Accepted)
    OF ?LookupAccountNumber
      ThisWindow.Update
      sub:Account_Number = ret:Account_Number
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          ret:Account_Number = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?ret:Account_Number)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?ret:Account_Number)
    OF ?ret:Purchase_Order_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Purchase_Order_Number, Accepted)
      !Neil 10/7/2001!
      !Routine to check if PO used before by this customer!
      
      !IF ret:Purchase_Order_Number <> ''    !make sure not blank!!!
      !  Access:RETSALES_ALIAS.ClearKey(res_ali:Despatched_Account_Key)
      !  res_ali:Despatched = 'YES'
      !  res_ali:Account_Number = ret:Account_Number
      !  res_ali:Purchase_Order_Number = ret:Purchase_Order_Number
      !  IF Access:RETSALES_ALIAS.Fetch(res_ali:Despatched_Account_Key)
      !    !Error - No match - Allow PO!
      !  ELSE
      !    Case MessageEx('This purchase order number has been used previously by this company, would you like to continue booking this retail sale?','ServiceBase 2000',|
      !                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
      !      Of 1 ! &Yes Button
      !
      !      Of 2 ! &No Button
      !        ret:Purchase_Order_Number = ''
      !        SELECT(?ret:Purchase_Order_Number)
      !        CYCLE
      !    End!Case MessageEx
      !  END
      !END
      
      !Don't understand the above code, so here's mine
      If ~0{prop:AcceptAll}
          Access:RETSALES_ALIAS.ClearKey(res_ali:Purchase_Number_Key)
          res_ali:Purchase_Order_Number = ret:Purchase_Order_Number
          Set(res_ali:Purchase_Number_Key,res_ali:Purchase_Number_Key)
          Access:RETSALES_ALIAS.Next()
          If res_ali:Purchase_Order_Number = ret:Purchase_Order_Number And ret:Purchase_Order_Number <> ''
              DuplicatePurchaseOrderList(ret:Purchase_Order_Number)
          End !res_ali:Purchase_Order_Number = ret:Purchase_Order_Number
      End !0{prop:AcceptAll}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Purchase_Order_Number, Accepted)
    OF ?ret:Delivery_Collection
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Delivery_Collection, Accepted)
      If ret:delivery_collection <> ''
          Unhide(?ret:courier_cost)
          Unhide(?ret:courier_cost:prompt)
          Case ret:delivery_collection
              Of 'COL'
                  Disable(?Delivery_Sheet)
              Of 'DEL'
                  Enable(?Delivery_Sheet)
          End!Case ret:delivery_collection
      Else!If ret:delivery_collection <> ''
              Hide(?ret:courier_Cost)
              Hide(?ret:courier_cost:prompt)
      End!Case ret:delivery_collection
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Delivery_Collection, Accepted)
    OF ?ret:Payment_Method
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Payment_Method, Accepted)
      If  ~0{prop:acceptall}
          If ret:payment_method   <> payment_method_temp
              If payment_method_temp <> ''
                  If ret:Payment_Method = 'EXC'
                      Case MessageEx('Exchange Accessory.'&|
                        '<13,10>'&|
                        '<13,10>All parts on this sales transaction will be zero valued.'&|
                        '<13,10>'&|
                        '<13,10>Are you sure?','ServiceBase 2000',|
                                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
      
                          Of 2 ! &No Button
                              ret:Payment_Method  = Payment_Method_Temp
                      End!Case MessageEx
                  Else !If ret:Payment_Method = 'EXC'
                      account# = 0
                      cash# = 0
                      access:subtracc.clearkey(sub:account_number_key)                                !Check the account to see if
                      sub:account_number = ret:account_number                                         !it should be cash, or account
                      if access:subtracc.fetch(sub:account_number_key) = level:benign
                          access:tradeacc.clearkey(tra:account_number_key) 
                          tra:account_number = sub:main_account_number
                          if access:tradeacc.fetch(tra:account_number_key) = level:benign
                              if tra:invoice_sub_accounts = 'YES'
                                  IF sub:retail_payment_type = 'ACC'
                                      account#    = 1
                                  End!IF sub:payment_type = 'ACC'
                                  IF sub:retail_payment_Type = 'CAS'
                                      cash#   = 1
                                  End!IF sub:payment_Type = 'CAS'
                              else!if tra:use_sub_accounts = 'YES'
                                  If tra:retail_payment_type = 'ACC'
                                      account#    = 1
                                  End!If tra:retail_payment_type = 'ACC'
                                  If tra:retail_payment_type  = 'CAS'
                                      cash# = 1
                                  End!If tra:retail_payment_type  = 'CAS'
      
                              end!if tra:use_sub_accounts = 'YES'
                          end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                      end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
      
                      Case ret:payment_method                                                         !Check what the payment method has
                          Of 'TRA'                                                                    !been changed to.
                              If cash# = 1
                                  Case MessageEx('The selected Trade Account can only be used for Cash Sales.','ServiceBase 2000',|
                                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                          Of 1 ! &OK Button
                                  End!Case MessageEx
                                  ret:payment_method  = payment_method_temp
                              Else
                                  Case MessageEx('Are you sure you want to change the Payment Method to Account?','ServiceBase 2000',|
                                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                          Of 1 ! &Yes Button
                                              payment_method_temp = ret:payment_method
                                          Of 2 ! &No Button
                                              ret:payment_method  = payment_method_temp
                                  End!Case MessageEx
                              End!If cash# = 1
                          Of 'CAS'
                              If account# = 1
                                  Case MessageEx('The Selected Trade Account has not been setup as a Cash Account, are you sure you want to change the Payment Method to Cash?','ServiceBase 2000',|
                                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                          Of 1 ! &Yes Button
                                              payment_method_temp = ret:payment_method
                                          Of 2 ! &No Button
                                              ret:payment_method  = payment_method_temp
                                  End!Case MessageEx
                              Else!If account# = 1
                                  payment_method_temp = ret:payment_method
                              End!If account# = 1
                      End!Case ret:payment_method
                  End !If ret:Payment_Method = 'EXC'
      
              Else!If payment_method_temp <> ''
                  If ret:Payment_Method = 'EXC'
                      Case MessageEx('Exchange Accessory.'&|
                        '<13,10>'&|
                        '<13,10>All parts on this sales transatcion will be zero valued.'&|
                        '<13,10>'&|
                        '<13,10>Are you sure?','ServiceBase 2000',|
                                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
                              payment_method_temp = ret:payment_method
                          Of 2 ! &No Button
                              ret:Payment_Method  = Payment_Method_Temp
                      End!Case MessageEx
                  End!If ret:Payment_Method = 'EXC'
              End!If payment_method_temp <> ''
      
          End!If ret:payment_method   <> payment_method_temp
          If ret:payment_method = 'CAS'
              Unhide(?Payment)
          Else!If ret:payment_method = 'CAS'
              Hide(?Payment)
          End!If ret:payment_method = 'CAS'
      End!If ~0{prop:acceptall}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Payment_Method, Accepted)
    OF ?Replicate
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Replicate, Accepted)
      RET:Postcode_Delivery=RET:Postcode
      RET:Company_Name_Delivery=RET:Company_Name
      RET:Building_Name_Delivery=RET:Building_Name
      RET:Address_Line1_Delivery=RET:Address_Line1
      RET:Address_Line2_Delivery=RET:Address_Line2
      RET:Address_Line3_Delivery=RET:Address_Line3
      RET:Telephone_Delivery=RET:Telephone_Number
      RET:Fax_Number_Delivery=RET:Fax_Number
      DIsplay()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Replicate, Accepted)
    OF ?StockEnquiry
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StockEnquiry, Accepted)
      error# = 0
      IF ret:account_number   = ''
              Case MessageEx('You must select a Trade Account.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
              End!Case MessageEx
          error# = 1
      End!IF ret:account_number   = ''
      If error# = 0 And ret:payment_method = ''
              Case MessageEx('You must select a Payment Method.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
              End!Case MessageEx
          error# = 1
      End!If error# = 0 And ret:payment_method = ''
      If error# = 0 And ret:delivery_collection = ''
              Case MessageEx('You must select Delivery or Collection.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
              End!Case MessageEx
          error# = 1
      End!If error# = 0 And ret:payment_method = ''
      If error# = 0
          Stock_Enquiry(func:OrderNumber)
      End!If error# = 0
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StockEnquiry, Accepted)
    OF ?Payment
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Payment, Accepted)
      error# = 0
      If error# = 0
          Browse_Retail_payments (ret:ref_number,ret:account_number,ret:courier_cost,'PIK')
      End!If error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Payment, Accepted)
    OF ?Delivery_Text
      ThisWindow.Update
      Delivery_Text
      ThisWindow.Reset
    OF ?Invoice_Text
      ThisWindow.Update
      Invoice_Text
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Error Checking
  IF ret:Delivery_Collection = ''
          Case MessageEx('You must select Delivery or Collection.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
          End!Case MessageEx
      Select(?ret:Delivery_Collection)
      Cycle
  End!IF ret:Delivery_Collection = ''
  
  If ret:delivery_collection  = 'DEL'
      If ret:Address_Line1 = ''
                  Case MessageEx('This sale has been marked as a Delivery.<13,10><13,10>You must enter a Delivery Address.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                  End!Case MessageEx
          Cycle
      End!If RET:Address_Delivery_Group = ''
  End!If ret:delivery_collection  = 'DEL'
  
  count# = 0
  stock_amount$   = 0
  setcursor(cursor:wait)                                                                      !Has any stock been picked?
  save_res_id = access:retstock.savefile()
  access:retstock.clearkey(res:part_number_key)
  res:ref_number  = ret:ref_number
  set(res:part_number_key,res:part_number_key)
  loop
      if access:retstock.next()
         break
      end !if
      if res:ref_number  <> ret:ref_number      |
          then break.  ! end if
      yldcnt# += 1
      if yldcnt# > 25
         yield() ; yldcnt# = 0
      end !if
      count# += 1
      If res:Despatched = 'PIK'
          stock_amount$ += Round(res:quantity * res:item_cost,.01)
      End !If res:Despatch = 'PIK'
      
  end !loop
  access:retstock.restorefile(save_res_id)
  setcursor()
  
  If count# = 0
          Case MessageEx('You must pick Stock before you can complete a Transcation.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
          End!Case MessageEx
      Cycle
  Else!If count# = 0
      If ret:payment_method = 'CAS'
  
          access:subtracc.clearkey(sub:account_number_key)
          sub:account_number = ret:account_number
          if access:subtracc.fetch(sub:account_number_key) = Level:benign
              stop# = 0
              access:tradeacc.clearkey(tra:account_number_key)
              tra:account_number = sub:main_account_number
              if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                  if tra:invoice_sub_accounts = 'YES'
                      access:vatcode.clearkey(vat:vat_code_key)
                      vat:vat_code = sub:retail_vat_code
                      if access:vatcode.fetch(vat:vat_code_key) = level:benign
                          vat_rate_temp = vat:vat_rate
                      end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                  else!if tra:use_sub_accounts = 'YES'
                      access:vatcode.clearkey(vat:vat_code_key)
                      vat:vat_code = tra:retail_vat_code
                      if access:vatcode.fetch(vat:vat_code_key) = level:benign
                          vat_rate_temp = vat:vat_rate
                      end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                  end!if tra:use_sub_accounts = 'YES'
              End!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
          End!if access:subtracc.fetch(sub:account_number_key)
  
  
          payment_amount$ = 0
          setcursor(cursor:wait)                                                                  !Has the stock been paid for
          save_rtp_id = access:retpay.savefile()
          access:retpay.clearkey(rtp:date_key)
          rtp:ref_number   = ret:ref_number
          set(rtp:date_key,rtp:date_key)
          loop
              if access:retpay.next()
                 break
              end !if
              if rtp:ref_number   <> ret:ref_number      |
                  then break.  ! end if
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              payment_amount$ += rtp:amount
          end !loop
          access:retpay.restorefile(save_rtp_id)
          setcursor()
  
          ret:sub_total   = Round(stock_amount$ + ret:courier_cost,.01)
          vat$            = Round(stock_amount$ * vat_rate_temp/100,.01) + Round(ret:courier_cost * vat_rate_temp/100,.01)
          total$          = Round(ret:sub_total + vat$,.01)
  
          If total$ - payment_amount$ > .01
                          Case MessageEx('A Balance of '&Format(Round(total$ - payment_amount$,.01),@n14.2)&' is still outstanding for this Sale.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                          End!Case MessageEx
              Cycle
          End!If stock_amount$ + ret:courier_cost <> payment_amount$
          
      End!If ret:payment_method = 'CAS'
  
  End!If count# = 0
  If func:OrderNumber <> 0
      Access:ordhead.ClearKey(orh:KeyOrder_no)
      orh:Order_no = func:OrderNumber
      If Access:ordhead.TryFetch(orh:KeyOrder_no) = Level:Benign
          !Found
          orh:SalesNumber = ret:Ref_Number
          orh:procesed = 1
          orh:pro_date = Today()
          Access:ORDHEAD.TryUpdate()
      Else!If Access:ordhead.TryFetch(orh:KeyOrder_no) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:ordhead.TryFetch(orh:KeyOrder_no) = Level:Benign
  End !func:OrderNumber <> 0
  ReturnValue = PARENT.TakeCompleted()
  glo:Select1 = ret:Ref_Number
  Retail_Picking_Note('PIK')
  glo:Select1 = ''
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Retail_Sales')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?ret:Account_Number
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Account_Number, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Account Number')
             Post(Event:Accepted,?LookupAccountNumber)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupAccountNumber)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Account_Number, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      !Save Fields
      payment_method_temp = ret:payment_method
      If thiswindow.request   = Insertrecord
          ret:delivery_collection = 'DEL'
      End!If thiswindow.request   = Insertrecord
      Post(Event:accepted,?ret:delivery_collection)
      Post(Event:accepted,?ret:payment_method)
      
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

