

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01008.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Retail_Sales PROCEDURE                         !Generated from procedure template - Window

ThisThreadActive BYTE
CurrentTab           STRING(80)
Account_Number       STRING(15)
RET_temp             STRING('RET')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Account_Number
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(RETSALES)
                       PROJECT(ret:Ref_Number)
                       PROJECT(ret:Purchase_Order_Number)
                       PROJECT(ret:Account_Number)
                       PROJECT(ret:date_booked)
                       PROJECT(ret:Courier)
                       PROJECT(ret:Despatched)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
ret:Ref_Number         LIKE(ret:Ref_Number)           !List box control field - type derived from field
ret:Purchase_Order_Number LIKE(ret:Purchase_Order_Number) !List box control field - type derived from field
ret:Account_Number     LIKE(ret:Account_Number)       !List box control field - type derived from field
ret:date_booked        LIKE(ret:date_booked)          !List box control field - type derived from field
ret:Courier            LIKE(ret:Courier)              !List box control field - type derived from field
ret:Despatched         LIKE(ret:Despatched)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB7::View:FileDropCombo VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
QuickWindow          WINDOW('Sales Order Processing'),AT(,,454,264),FONT('Tahoma',8,,),CENTER,IMM,HLP('Browse_Retail_Sales'),SYSTEM,GRAY,RESIZE
                       LIST,AT(8,36,352,220),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),ALRT(4),FORMAT('50R(2)|M~Sales Number~L@s8@92L(2)|M~Purchase Order Number~@s30@69L(2)|M~Account ' &|
   'Number~@s15@47R(2)|M~Date Raised~@d6b@120L(2)|M~Courier~@s30@'),FROM(Queue:Browse:1)
                       BUTTON('Process Sale'),AT(372,108,76,20),USE(?process_sale),LEFT,ICON('process.gif')
                       SHEET,AT(4,4,360,256),USE(?CurrentTab),SPREAD
                         TAB('By Sales Number'),USE(?Tab3)
                           ENTRY(@s8),AT(8,20,64,10),USE(ret:Ref_Number),RIGHT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Purchase Order No'),USE(?Tab:2)
                           ENTRY(@s30),AT(8,20,124,10),USE(ret:Purchase_Order_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('By Account Number'),USE(?Tab2)
                           ENTRY(@s30),AT(8,20,124,10),USE(ret:Purchase_Order_Number,,?RET:Purchase_Order_Number:2),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           COMBO(@s15),AT(136,20,124,10),USE(Account_Number),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('60L(2)|M@s15@120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                         END
                       END
                       BUTTON('Close'),AT(372,240,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
BRW1::Sort1:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2
BRW1::Sort2:StepClass CLASS(StepStringClass)          !Conditional Step Manager - Choice(?CurrentTab) = 3
Init                   PROCEDURE(BYTE Controls,BYTE Mode)
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    If ?ret:Ref_Number{prop:ReadOnly} = True
        ?ret:Ref_Number{prop:FontColor} = 65793
        ?ret:Ref_Number{prop:Color} = 15066597
    Elsif ?ret:Ref_Number{prop:Req} = True
        ?ret:Ref_Number{prop:FontColor} = 65793
        ?ret:Ref_Number{prop:Color} = 8454143
    Else ! If ?ret:Ref_Number{prop:Req} = True
        ?ret:Ref_Number{prop:FontColor} = 65793
        ?ret:Ref_Number{prop:Color} = 16777215
    End ! If ?ret:Ref_Number{prop:Req} = True
    ?ret:Ref_Number{prop:Trn} = 0
    ?ret:Ref_Number{prop:FontStyle} = font:Bold
    ?Tab:2{prop:Color} = 15066597
    If ?ret:Purchase_Order_Number{prop:ReadOnly} = True
        ?ret:Purchase_Order_Number{prop:FontColor} = 65793
        ?ret:Purchase_Order_Number{prop:Color} = 15066597
    Elsif ?ret:Purchase_Order_Number{prop:Req} = True
        ?ret:Purchase_Order_Number{prop:FontColor} = 65793
        ?ret:Purchase_Order_Number{prop:Color} = 8454143
    Else ! If ?ret:Purchase_Order_Number{prop:Req} = True
        ?ret:Purchase_Order_Number{prop:FontColor} = 65793
        ?ret:Purchase_Order_Number{prop:Color} = 16777215
    End ! If ?ret:Purchase_Order_Number{prop:Req} = True
    ?ret:Purchase_Order_Number{prop:Trn} = 0
    ?ret:Purchase_Order_Number{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    If ?RET:Purchase_Order_Number:2{prop:ReadOnly} = True
        ?RET:Purchase_Order_Number:2{prop:FontColor} = 65793
        ?RET:Purchase_Order_Number:2{prop:Color} = 15066597
    Elsif ?RET:Purchase_Order_Number:2{prop:Req} = True
        ?RET:Purchase_Order_Number:2{prop:FontColor} = 65793
        ?RET:Purchase_Order_Number:2{prop:Color} = 8454143
    Else ! If ?RET:Purchase_Order_Number:2{prop:Req} = True
        ?RET:Purchase_Order_Number:2{prop:FontColor} = 65793
        ?RET:Purchase_Order_Number:2{prop:Color} = 16777215
    End ! If ?RET:Purchase_Order_Number:2{prop:Req} = True
    ?RET:Purchase_Order_Number:2{prop:Trn} = 0
    ?RET:Purchase_Order_Number:2{prop:FontStyle} = font:Bold
    If ?Account_Number{prop:ReadOnly} = True
        ?Account_Number{prop:FontColor} = 65793
        ?Account_Number{prop:Color} = 15066597
    Elsif ?Account_Number{prop:Req} = True
        ?Account_Number{prop:FontColor} = 65793
        ?Account_Number{prop:Color} = 8454143
    Else ! If ?Account_Number{prop:Req} = True
        ?Account_Number{prop:FontColor} = 65793
        ?Account_Number{prop:Color} = 16777215
    End ! If ?Account_Number{prop:Req} = True
    ?Account_Number{prop:Trn} = 0
    ?Account_Number{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Retail_Sales',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Retail_Sales',1)
    SolaceViewVars('Account_Number',Account_Number,'Browse_Retail_Sales',1)
    SolaceViewVars('RET_temp',RET_temp,'Browse_Retail_Sales',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?process_sale;  SolaceCtrlName = '?process_sale';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Ref_Number;  SolaceCtrlName = '?ret:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Purchase_Order_Number;  SolaceCtrlName = '?ret:Purchase_Order_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RET:Purchase_Order_Number:2;  SolaceCtrlName = '?RET:Purchase_Order_Number:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Account_Number;  SolaceCtrlName = '?Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Retail_Sales')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Retail_Sales')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Browse_Retail_Sales'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:RETSALES.Open
  Relate:SUBTRACC.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:RETSALES,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,ret:Despatched_Ref_Key)
  BRW1.AddRange(ret:Despatched,RET_temp)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?ret:Ref_Number,ret:Ref_Number,1,BRW1)
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,ret:Despatched_Purchase_Key)
  BRW1.AddRange(ret:Despatched,RET_temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?RET:Purchase_Order_Number,ret:Purchase_Order_Number,1,BRW1)
  BRW1::Sort2:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort2:StepClass,ret:Despatched_Account_Key)
  BRW1.AddRange(ret:Account_Number,Account_Number)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?RET:Purchase_Order_Number:2,ret:Purchase_Order_Number,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,ret:Ref_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?RET:Purchase_Order_Number,ret:Ref_Number,1,BRW1)
  BRW1.AddField(ret:Ref_Number,BRW1.Q.ret:Ref_Number)
  BRW1.AddField(ret:Purchase_Order_Number,BRW1.Q.ret:Purchase_Order_Number)
  BRW1.AddField(ret:Account_Number,BRW1.Q.ret:Account_Number)
  BRW1.AddField(ret:date_booked,BRW1.Q.ret:date_booked)
  BRW1.AddField(ret:Courier,BRW1.Q.ret:Courier)
  BRW1.AddField(ret:Despatched,BRW1.Q.ret:Despatched)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB7.Init(Account_Number,?Account_Number,Queue:FileDropCombo.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo,Relate:SUBTRACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo
  FDCB7.AddSortOrder(sub:Account_Number_Key)
  FDCB7.AddField(sub:Account_Number,FDCB7.Q.sub:Account_Number)
  FDCB7.AddField(sub:Company_Name,FDCB7.Q.sub:Company_Name)
  FDCB7.AddField(sub:RecordNumber,FDCB7.Q.sub:RecordNumber)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab3{PROP:TEXT} = 'By Sales Number'
    ?Tab:2{PROP:TEXT} = 'By Purchase Order No'
    ?Tab2{PROP:TEXT} = 'By Account Number'
    ?Browse:1{PROP:FORMAT} ='50R(2)|M~Sales Number~L@s8@#1#92L(2)|M~Purchase Order Number~@s30@#2#69L(2)|M~Account Number~@s15@#3#47R(2)|M~Date Raised~@d6b@#4#120L(2)|M~Courier~@s30@#5#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETSALES.Close
    Relate:SUBTRACC.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='Browse_Retail_Sales'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Retail_Sales',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?process_sale
      ThisWindow.Update
      Process_Retail_Sale
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?process_sale, Accepted)
      ThisWindow.Reset(1)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?process_sale, Accepted)
    OF ?Account_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Account_Number, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Account_Number, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Retail_Sales')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      IF keycode() = mouseleft2
          Post(event:accepted,?process_sale)
      End!IF keycode() = mouseleft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='50R(2)|M~Sales Number~L@s8@#1#92L(2)|M~Purchase Order Number~@s30@#2#69L(2)|M~Account Number~@s15@#3#47R(2)|M~Date Raised~@d6b@#4#120L(2)|M~Courier~@s30@#5#'
          ?Tab3{PROP:TEXT} = 'By Sales Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='92L(2)|M~Purchase Order Number~@s30@#2#50R(2)|M~Sales Number~L@s8@#1#69L(2)|M~Account Number~@s15@#3#47R(2)|M~Date Raised~@d6b@#4#120L(2)|M~Courier~@s30@#5#'
          ?Tab:2{PROP:TEXT} = 'By Purchase Order No'
        OF 3
          ?Browse:1{PROP:FORMAT} ='92L(2)|M~Purchase Order Number~@s30@#2#50R(2)|M~Sales Number~L@s8@#1#69L(2)|M~Account Number~@s15@#3#47R(2)|M~Date Raised~@d6b@#4#120L(2)|M~Courier~@s30@#5#'
          ?Tab2{PROP:TEXT} = 'By Account Number'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?ret:Ref_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Ref_Number, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Ref_Number, Selected)
    OF ?ret:Purchase_Order_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Purchase_Order_Number, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Purchase_Order_Number, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      FDCB7.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 3
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1::Sort2:StepClass.Init PROCEDURE(BYTE Controls,BYTE Mode)

  CODE
  ! Before Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 2, Init, (BYTE Controls,BYTE Mode))
  ret:despatched  = 'RET'
  PARENT.Init(Controls,Mode)
  ! After Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 2, Init, (BYTE Controls,BYTE Mode))


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

