

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABEIP.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('SBG01026.INC'),ONCE        !Local module procedure declarations
                     END








BackOrderProcessing PROCEDURE                         !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisThreadActive BYTE
save_orp_id          USHORT,AUTO
save_tratmp_id       USHORT,AUTO
save_ret_ali_id      USHORT,AUTO
tmp:StockBelow       LONG(1)
save_res_id          USHORT,AUTO
save_retpar_id       USHORT,AUTO
save_retacc_id       USHORT,AUTO
tmp:CurrentStock     LONG
tmp:TotalOrdered     LONG
tmp:TotalToShip      LONG
tmp:CloseWindow      BYTE(0)
tmp:CurrentStockAfter LONG
tmp:QuantityAvailable LONG
tmp:QuantityOnOrder  LONG
tmp:Location         STRING(30)
tmp:VatRate          REAL
tmp:UseDeliveryAddress BYTE(0)
tmp:UseInvoice       BYTE(0)
tmp:PaymentMethod    STRING('0 {2}')
AccountQueue         QUEUE,PRE(acctmp)
AccountNumber        STRING(30),NAME('acctmp:AccountNumber')
SaleNumber           LONG,NAME('acctmp:SaleNumber')
PaymentMethod        STRING('0 {2}'),NAME('acctmp:PaymentMethod')
PostcodeDelivery     STRING(10),NAME('acctmp:PostcodeDelivery')
CompanyNameDelivery  STRING(30),NAME('acctmp:CompanyNameDelivery')
BuildingNameDelivery STRING(30),NAME('acctmp:BuildingNameDelivery')
AddressLine1Delivery STRING(30),NAME('acctmp:AddressLine1Delivery')
AddressLine2Delivery STRING(30),NAME('acctmp:AddressLine2Delivery')
AddressLine3Delivery STRING(30),NAME('acctmp:AddressLine3Delivery')
TelephoneDelivery    STRING(15),NAME('acctmp:TelephoneDelivery')
FaxNumberDelivery    STRING(15),NAME('acctmp:FaxNumberDelivery')
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:Location
tratmp:Company_Name    LIKE(tratmp:Company_Name)      !List box control field - type derived from field
tratmp:RefNumber       LIKE(tratmp:RefNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(RETPARTSLIST)
                       PROJECT(retpar:PartNumber)
                       PROJECT(retpar:Description)
                       PROJECT(retpar:QuantityBackOrder)
                       PROJECT(retpar:QuantityOnOrder)
                       PROJECT(retpar:QuantityStock)
                       PROJECT(retpar:RecordNumber)
                       PROJECT(retpar:Location)
                       PROJECT(retpar:Processed)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
retpar:PartNumber      LIKE(retpar:PartNumber)        !List box control field - type derived from field
retpar:Description     LIKE(retpar:Description)       !List box control field - type derived from field
retpar:QuantityBackOrder LIKE(retpar:QuantityBackOrder) !List box control field - type derived from field
retpar:QuantityOnOrder LIKE(retpar:QuantityOnOrder)   !List box control field - type derived from field
retpar:QuantityStock   LIKE(retpar:QuantityStock)     !List box control field - type derived from field
retpar:RecordNumber    LIKE(retpar:RecordNumber)      !Primary key field - type derived from field
retpar:Location        LIKE(retpar:Location)          !Browse key field - type derived from field
retpar:Processed       LIKE(retpar:Processed)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW5::View:Browse    VIEW(RETACCOUNTSLIST)
                       PROJECT(retacc:CompanyName)
                       PROJECT(retacc:AccountNumber)
                       PROJECT(retacc:SaleNumber)
                       PROJECT(retacc:SaleType)
                       PROJECT(retacc:DateOrdered)
                       PROJECT(retacc:QuantityOrdered)
                       PROJECT(retacc:QuantityToShip)
                       PROJECT(retacc:RecordNumber)
                       PROJECT(retacc:RefNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
retacc:CompanyName     LIKE(retacc:CompanyName)       !List box control field - type derived from field
retacc:AccountNumber   LIKE(retacc:AccountNumber)     !List box control field - type derived from field
retacc:SaleNumber      LIKE(retacc:SaleNumber)        !List box control field - type derived from field
retacc:SaleType        LIKE(retacc:SaleType)          !List box control field - type derived from field
retacc:DateOrdered     LIKE(retacc:DateOrdered)       !List box control field - type derived from field
retacc:QuantityOrdered LIKE(retacc:QuantityOrdered)   !List box control field - type derived from field
retacc:QuantityToShip  LIKE(retacc:QuantityToShip)    !List box control field - type derived from field
retacc:RecordNumber    LIKE(retacc:RecordNumber)      !Primary key field - type derived from field
retacc:RefNumber       LIKE(retacc:RefNumber)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK13::retacc:RefNumber    LIKE(retacc:RefNumber)
! ---------------------------------------- Higher Keys --------------------------------------- !
! ---------------------------------------- Higher Keys --------------------------------------- !
HK18::retpar:Location     LIKE(retpar:Location)
HK18::retpar:Processed    LIKE(retpar:Processed)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB17::View:FileDropCombo VIEW(TRADETMP)
                       PROJECT(tratmp:Company_Name)
                       PROJECT(tratmp:RefNumber)
                     END
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask4          DECIMAL(10,0,0)                  !Xplore
XploreMask14          DECIMAL(10,0,0)                 !Xplore
XploreTitle4         STRING(' ')                      !Xplore
xpInitialTab4        SHORT                            !Xplore
XploreMask5          DECIMAL(10,0,0)                  !Xplore
XploreMask15          DECIMAL(10,0,0)                 !Xplore
XploreTitle5         STRING(' ')                      !Xplore
xpInitialTab5        SHORT                            !Xplore
window               WINDOW('Back Order Processing'),AT(,,668,348),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),ALRT(EscKey),ALRT(F6Key),ALRT(F7Key),ALRT(F8Key),ALRT(F10Key),TIMER(50),GRAY,DOUBLE
                       PANEL,AT(4,4,660,20),USE(?Panel1:2),FILL(COLOR:Silver)
                       PROMPT('Hide if "In Stock" is below'),AT(8,8),USE(?tmp:StockBelow:Prompt),TRN
                       ENTRY(@s8),AT(96,8,64,10),USE(tmp:StockBelow),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Hide if "In Stock" is below'),TIP('Hide if "In Stock" is below'),UPR
                       SHEET,AT(4,28,324,288),USE(?Sheet1),SPREAD
                         TAB('Un-Processed Orders'),USE(?Tab1)
                           BUTTON('View Stock Item'),AT(8,324,76,16),USE(?ViewStockItem),LEFT,ICON('edit.ico')
                           BUTTON('Total Value Of Parts'),AT(248,60,76,16),USE(?TotalValue),LEFT,ICON('MONEY_SM.GIF')
                           BUTTON('Cancel Selected Part'),AT(252,324,76,16),USE(?CancelPart),LEFT,ICON('ViewFail.gif')
                         END
                         TAB('Processed Orders'),USE(?Tab2)
                           BUTTON('Generate Pick Notes / Quit [F10]'),AT(8,324,84,16),USE(?GeneratePickNotes),LEFT,ICON('Pick.gif')
                         END
                       END
                       LIST,AT(8,80,316,232),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('100L(2)|M~Part Number~@s30@82L(2)|M~Description~@s30@[42L(2)|M~Back Order~@s8@40' &|
   'L(2)|M~On Order~@s8@45L(2)|M~In Stock~@s8@]|M~Quantity~'),FROM(Queue:Browse)
                       LIST,AT(336,60,324,220),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('90L(2)|M~Company Name~@s30@64L(2)|M~Account Number~@s30@35R(2)|M~Sale No~@s8@21L' &|
   '(2)|M~Type~@s4@39R(2)|M~Requested~@d6@27R(2)|M~Req~@s5@32R(2)|M~To Ship~@s5@'),FROM(Queue:Browse:1)
                       PANEL,AT(4,320,660,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Process Items [F7]'),AT(336,324,76,16),USE(?ProcessItems),LEFT,ICON('process.gif')
                       BUTTON('Auto Allocate [F8]'),AT(584,324,76,16),USE(?AutoAllocate),LEFT,ICON('transfer.gif')
                       BUTTON('Calculate Back Orders [F6]'),AT(164,8,108,12),USE(?Recalculate),LEFT,ICON('calc.gif')
                       COMBO(@s20),AT(8,48,124,10),USE(tmp:Location),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                       ENTRY(@s30),AT(8,64,124,10),USE(retpar:PartNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Part Number'),TIP('Part Number'),UPR
                       SHEET,AT(332,28,332,288),USE(?Sheet2),WIZARD,SPREAD
                         TAB('By Trade Account'),USE(?Tab3)
                           GROUP('Type Key'),AT(500,32,160,24),USE(?Group1),BOXED
                             PROMPT('TRA = Trade    CAS = Cash    EXC = Exchange'),AT(508,40),USE(?Prompt5)
                           END
                           PROMPT('By Account'),AT(336,32),USE(?Prompt6)
                           BUTTON('&Insert'),AT(536,280,42,12),USE(?Insert),HIDE
                           BUTTON('&Change'),AT(576,280,42,12),USE(?Change),HIDE,DEFAULT
                           BUTTON('&Delete'),AT(616,280,42,12),USE(?Delete),HIDE
                           PROMPT('Actual Stock Value'),AT(336,288),USE(?RETPAR:QuantityStock:Prompt),TRN
                           ENTRY(@s8),AT(416,288,40,10),USE(retpar:QuantityStock),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Quantity In Stock'),TIP('Quantity In Stock'),UPR,READONLY
                           ENTRY(@s8),AT(620,300,40,10),USE(tmp:TotalToShip),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Total To Ship'),TIP('Total To Ship'),UPR,READONLY
                           PROMPT('Stock After Processing'),AT(336,300),USE(?tmp:CurrentStockAfter:Prompt),TRN
                           ENTRY(@s8),AT(416,300,40,10),USE(tmp:CurrentStockAfter),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Current Stock After'),TIP('Current Stock After'),UPR,READONLY
                           PROMPT('Totals'),AT(536,300),USE(?Prompt4)
                           ENTRY(@s8),AT(564,300,40,10),USE(tmp:TotalOrdered),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Total Ordered'),TIP('Total Ordered'),UPR,READONLY
                         END
                       END
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW4::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW4::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet1) = 2
BRW5                 CLASS(BrowseClass)               !Browse using ?List:2
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetFromView          PROCEDURE(),DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW5::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW5::EIPManager     BrowseEIPManager                 !Browse EIP Manager for Browse using ?List:2
EditInPlace::RETACC:QuantityToShip CLASS(EditEntryClass) !Edit-in-place class for field retacc:QuantityToShip
TakeEvent              PROCEDURE(UNSIGNED Event),BYTE,DERIVED
                     END

FDCB17               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
! (BE 20/02/03) CHANGE 161
    MAP
IsStopAccount   PROCEDURE(STRING AccountNumber),SHORT
    END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore4              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore4Step1         StepStringClass !STRING          !Xplore: Column displaying retpar:PartNumber
Xplore4Locator1      StepLocatorClass                 !Xplore: Column displaying retpar:PartNumber
Xplore4Step2         StepStringClass !STRING          !Xplore: Column displaying retpar:Description
Xplore4Locator2      StepLocatorClass                 !Xplore: Column displaying retpar:Description
Xplore4Step3         StepLongClass   !LONG            !Xplore: Column displaying retpar:QuantityBackOrder
Xplore4Locator3      StepLocatorClass                 !Xplore: Column displaying retpar:QuantityBackOrder
Xplore4Step4         StepLongClass   !LONG            !Xplore: Column displaying retpar:QuantityOnOrder
Xplore4Locator4      StepLocatorClass                 !Xplore: Column displaying retpar:QuantityOnOrder
Xplore4Step5         StepLongClass   !LONG            !Xplore: Column displaying retpar:QuantityStock
Xplore4Locator5      StepLocatorClass                 !Xplore: Column displaying retpar:QuantityStock
Xplore4Step6         StepLongClass   !LONG            !Xplore: Column displaying retpar:RecordNumber
Xplore4Locator6      StepLocatorClass                 !Xplore: Column displaying retpar:RecordNumber
Xplore4Step7         StepStringClass !STRING          !Xplore: Column displaying retpar:Location
Xplore4Locator7      StepLocatorClass                 !Xplore: Column displaying retpar:Location
Xplore4Step8         StepCustomClass !BYTE            !Xplore: Column displaying retpar:Processed
Xplore4Locator8      StepLocatorClass                 !Xplore: Column displaying retpar:Processed
Xplore5              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore5Step1         StepStringClass !STRING          !Xplore: Column displaying retacc:CompanyName
Xplore5Locator1      StepLocatorClass                 !Xplore: Column displaying retacc:CompanyName
Xplore5Step2         StepStringClass !STRING          !Xplore: Column displaying retacc:AccountNumber
Xplore5Locator2      StepLocatorClass                 !Xplore: Column displaying retacc:AccountNumber
Xplore5Step3         StepLongClass   !LONG            !Xplore: Column displaying retacc:SaleNumber
Xplore5Locator3      StepLocatorClass                 !Xplore: Column displaying retacc:SaleNumber
Xplore5Step4         StepStringClass !STRING          !Xplore: Column displaying retacc:SaleType
Xplore5Locator4      StepLocatorClass                 !Xplore: Column displaying retacc:SaleType
Xplore5Step5         StepCustomClass !DATE            !Xplore: Column displaying retacc:DateOrdered
Xplore5Locator5      StepLocatorClass                 !Xplore: Column displaying retacc:DateOrdered
Xplore5Step6         StepLongClass   !LONG            !Xplore: Column displaying retacc:QuantityOrdered
Xplore5Locator6      StepLocatorClass                 !Xplore: Column displaying retacc:QuantityOrdered
Xplore5Step7         StepLongClass   !LONG            !Xplore: Column displaying retacc:QuantityToShip
Xplore5Locator7      StepLocatorClass                 !Xplore: Column displaying retacc:QuantityToShip
Xplore5Step8         StepLongClass   !LONG            !Xplore: Column displaying retacc:RecordNumber
Xplore5Locator8      StepLocatorClass                 !Xplore: Column displaying retacc:RecordNumber
Xplore5Step9         StepLongClass   !LONG            !Xplore: Column displaying retacc:RefNumber
Xplore5Locator9      StepLocatorClass                 !Xplore: Column displaying retacc:RefNumber
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Panel1:2{prop:Fill} = 15066597

    ?tmp:StockBelow:Prompt{prop:FontColor} = -1
    ?tmp:StockBelow:Prompt{prop:Color} = 15066597
    If ?tmp:StockBelow{prop:ReadOnly} = True
        ?tmp:StockBelow{prop:FontColor} = 65793
        ?tmp:StockBelow{prop:Color} = 15066597
    Elsif ?tmp:StockBelow{prop:Req} = True
        ?tmp:StockBelow{prop:FontColor} = 65793
        ?tmp:StockBelow{prop:Color} = 8454143
    Else ! If ?tmp:StockBelow{prop:Req} = True
        ?tmp:StockBelow{prop:FontColor} = 65793
        ?tmp:StockBelow{prop:Color} = 16777215
    End ! If ?tmp:StockBelow{prop:Req} = True
    ?tmp:StockBelow{prop:Trn} = 0
    ?tmp:StockBelow{prop:FontStyle} = font:Bold
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597

    If ?tmp:Location{prop:ReadOnly} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 15066597
    Elsif ?tmp:Location{prop:Req} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 8454143
    Else ! If ?tmp:Location{prop:Req} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 16777215
    End ! If ?tmp:Location{prop:Req} = True
    ?tmp:Location{prop:Trn} = 0
    ?tmp:Location{prop:FontStyle} = font:Bold
    If ?retpar:PartNumber{prop:ReadOnly} = True
        ?retpar:PartNumber{prop:FontColor} = 65793
        ?retpar:PartNumber{prop:Color} = 15066597
    Elsif ?retpar:PartNumber{prop:Req} = True
        ?retpar:PartNumber{prop:FontColor} = 65793
        ?retpar:PartNumber{prop:Color} = 8454143
    Else ! If ?retpar:PartNumber{prop:Req} = True
        ?retpar:PartNumber{prop:FontColor} = 65793
        ?retpar:PartNumber{prop:Color} = 16777215
    End ! If ?retpar:PartNumber{prop:Req} = True
    ?retpar:PartNumber{prop:Trn} = 0
    ?retpar:PartNumber{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?RETPAR:QuantityStock:Prompt{prop:FontColor} = -1
    ?RETPAR:QuantityStock:Prompt{prop:Color} = 15066597
    If ?retpar:QuantityStock{prop:ReadOnly} = True
        ?retpar:QuantityStock{prop:FontColor} = 65793
        ?retpar:QuantityStock{prop:Color} = 15066597
    Elsif ?retpar:QuantityStock{prop:Req} = True
        ?retpar:QuantityStock{prop:FontColor} = 65793
        ?retpar:QuantityStock{prop:Color} = 8454143
    Else ! If ?retpar:QuantityStock{prop:Req} = True
        ?retpar:QuantityStock{prop:FontColor} = 65793
        ?retpar:QuantityStock{prop:Color} = 16777215
    End ! If ?retpar:QuantityStock{prop:Req} = True
    ?retpar:QuantityStock{prop:Trn} = 0
    ?retpar:QuantityStock{prop:FontStyle} = font:Bold
    If ?tmp:TotalToShip{prop:ReadOnly} = True
        ?tmp:TotalToShip{prop:FontColor} = 65793
        ?tmp:TotalToShip{prop:Color} = 15066597
    Elsif ?tmp:TotalToShip{prop:Req} = True
        ?tmp:TotalToShip{prop:FontColor} = 65793
        ?tmp:TotalToShip{prop:Color} = 8454143
    Else ! If ?tmp:TotalToShip{prop:Req} = True
        ?tmp:TotalToShip{prop:FontColor} = 65793
        ?tmp:TotalToShip{prop:Color} = 16777215
    End ! If ?tmp:TotalToShip{prop:Req} = True
    ?tmp:TotalToShip{prop:Trn} = 0
    ?tmp:TotalToShip{prop:FontStyle} = font:Bold
    ?tmp:CurrentStockAfter:Prompt{prop:FontColor} = -1
    ?tmp:CurrentStockAfter:Prompt{prop:Color} = 15066597
    If ?tmp:CurrentStockAfter{prop:ReadOnly} = True
        ?tmp:CurrentStockAfter{prop:FontColor} = 65793
        ?tmp:CurrentStockAfter{prop:Color} = 15066597
    Elsif ?tmp:CurrentStockAfter{prop:Req} = True
        ?tmp:CurrentStockAfter{prop:FontColor} = 65793
        ?tmp:CurrentStockAfter{prop:Color} = 8454143
    Else ! If ?tmp:CurrentStockAfter{prop:Req} = True
        ?tmp:CurrentStockAfter{prop:FontColor} = 65793
        ?tmp:CurrentStockAfter{prop:Color} = 16777215
    End ! If ?tmp:CurrentStockAfter{prop:Req} = True
    ?tmp:CurrentStockAfter{prop:Trn} = 0
    ?tmp:CurrentStockAfter{prop:FontStyle} = font:Bold
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    If ?tmp:TotalOrdered{prop:ReadOnly} = True
        ?tmp:TotalOrdered{prop:FontColor} = 65793
        ?tmp:TotalOrdered{prop:Color} = 15066597
    Elsif ?tmp:TotalOrdered{prop:Req} = True
        ?tmp:TotalOrdered{prop:FontColor} = 65793
        ?tmp:TotalOrdered{prop:Color} = 8454143
    Else ! If ?tmp:TotalOrdered{prop:Req} = True
        ?tmp:TotalOrdered{prop:FontColor} = 65793
        ?tmp:TotalOrdered{prop:Color} = 16777215
    End ! If ?tmp:TotalOrdered{prop:Req} = True
    ?tmp:TotalOrdered{prop:Trn} = 0
    ?tmp:TotalOrdered{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
GetItemCost     Routine
    !Get the Trade Account from the Sale to determine the price
    !of the parts
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = ret:Account_Number
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:RetailZeroParts = 1
                res:Item_Cost = 0
            Else !If tra:RetailZeroParts = 1
                !Check the Trade Account Retail Price Structure
                !Only applies if it is a new record
                !'RET' = Retail Price
                !Other = Trade Price
                If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                    Case sub:Retail_Price_Structure
                        Of 'RET'
                            res:Item_Cost   = res:Retail_Cost
                        Of 'PUR'
                            res:Item_Cost   = res:Purchase_Cost
                        Else
                            res:Item_Cost   = res:Sale_Cost
                    End !Case sub:Retail_Price_Structure
                Else !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                    Case tra:Retail_Price_Structure
                        Of 'RET'
                            res:Item_Cost   = res:Retail_Cost
                        Of 'PUR'
                            res:Item_Cost   = res:Purchase_Cost
                        Else
                            res:Item_Cost   = res:Sale_Cost
                    End !Case tra:Retail_Price_Structure
                End !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
            End !If tra:RetailZeroParts = 1
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
AddRetailPart       Routine
    !Now take part from stock and attach to sale
    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number  = retpar:PartRefNumber
    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        !Found
        !If the ship quantity is still less that the quantity in stock,
        !or the quantity required, then create a "Pending Part" for this amount.
        Do GetItemCost

        !Create the normal part
        If Access:RETSTOCK.PrimeRecord() = Level:Benign
            res:Ref_Number      = ret:Ref_Number
            res:Part_Number     = retpar:PartNumber
            res:Description     = retpar:Description
            res:Supplier        = sto:Supplier
            res:Purchase_Cost   = sto:Purchase_Cost
            res:Sale_Cost       = sto:Sale_Cost
            res:Retail_Cost     = sto:Retail_Cost
            res:AccessoryCost   = sto:AccessoryCost

            Do GetItemCost

            res:Quantity        = retacc:QuantityToShip
            res:Despatched      = 'PIK'
            res:Part_Ref_Number = sto:Ref_Number
            res:Date_Received   = Today()
            res:Previous_Sale_Number = retacc:SaleNumber
            Access:RETSALES_ALIAS.ClearKey(res_ali:Ref_Number_Key)
            res_ali:Ref_Number = retacc:SaleNumber
            If Access:RETSALES_ALIAS.TryFetch(res_ali:Ref_Number_Key) = Level:Benign
                !Found
                !If this is back order, from a back order try and find the original part
                !on the original sale, and look for the purchase order number
                If res_ali:Purchase_Order_Number <> 'VARIOUS'
                    res:Purchase_Order_Number   = res_ali:Purchase_Order_Number
                Else !If res_ali:Purchase_Order_Number <> 'VARIOUS'
                    Access:RETSTOCK_ALIAS.ClearKey(ret_ali:Record_Number_Key)
                    ret_ali:Record_Number = retacc:OrigRecordNumber
                    If Access:RETSTOCK_ALIAS.TryFetch(ret_ali:Record_Number_Key) = Level:Benign
                        !Found
                        res:Purchase_Order_Number   = ret_ali:Purchase_Order_Number
                    Else!If Access:RETSTOCK_ALIAS.TryFetch(ret_ali:Record_Number_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End!If Access:RETSTOCK_ALIAS.TryFetch(ret_ali:Record_Number_Key) = Level:Benign
                End !If res_ali:Purchase_Order_Number <> 'VARIOUS'
                
            Else!If Access:RETSALES_ALIAS.TryFetch(res_ali:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:RETSALES_ALIAS.TryFetch(res_ali:Ref_Number_Key) = Level:Benign
            
            If Access:RETSTOCK.TryInsert() = Level:Benign
                !Insert Successful
                !Create a "Pending Part" if the original SaLe request has not been fulfilled

                If retacc:QuantityToShip < retacc:QuantityOrdered
                    If Access:RETSTOCK.PrimeRecord() = Level:Benign
                        res:Ref_Number      = ret:Ref_Number
                        res:Part_Number     = retpar:PartNumber
                        res:Description     = retpar:Description
                        res:Supplier        = sto:Supplier
                        res:Purchase_Cost   = sto:Purchase_Cost
                        res:Sale_Cost       = sto:Sale_Cost
                        res:Retail_Cost     = sto:Retail_Cost
                        res:AccessoryCost   = sto:AccessoryCost

                        Do GetItemCost

                        res:Quantity        = retacc:QuantityOrdered - retacc:QuantityToShip
                        res:Despatched      = 'PEN'
                        res:Part_Ref_Number = sto:Ref_Number
                        res:Date_Received   = Today()
                        res:Previous_Sale_Number    = retacc:SaleNumber
                        Access:RETSALES_ALIAS.ClearKey(res_ali:Ref_Number_Key)
                        res_ali:Ref_Number = retacc:SaleNumber
                        If Access:RETSALES_ALIAS.TryFetch(res_ali:Ref_Number_Key) = Level:Benign
                            !Found
                            !If this is back order, from a back order try and find the original part
                            !on the original sale, and look for the purchase order number
                            If res_ali:Purchase_Order_Number <> 'VARIOUS'
                                res:Purchase_Order_Number   = res_ali:Purchase_Order_Number
                            Else !If res_ali:Purchase_Order_Number <> 'VARIOUS'
                                Access:RETSTOCK_ALIAS.ClearKey(ret_ali:Record_Number_Key)
                                ret_ali:Record_Number = retacc:OrigRecordNumber
                                If Access:RETSTOCK_ALIAS.TryFetch(ret_ali:Record_Number_Key) = Level:Benign
                                    !Found
                                    res:Purchase_Order_Number   = ret_ali:Purchase_Order_Number
                                Else!If Access:RETSTOCK_ALIAS.TryFetch(ret_ali:Record_Number_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:RETSTOCK_ALIAS.TryFetch(ret_ali:Record_Number_Key) = Level:Benign
                            End !If res_ali:Purchase_Order_Number <> 'VARIOUS'
                        Else!If Access:RETSALES_ALIAS.TryFetch(res_ali:Ref_Number_Key) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                        End!If Access:RETSALES_ALIAS.TryFetch(res_ali:Ref_Number_Key) = Level:Benign
                        If Access:RETSTOCK.TryInsert() = Level:Benign
                            !Insert Successful
                        Else !If Access:RETSTOCK.TryInsert() = Level:Benign
                            !Insert Failed
                        End !If Access:RETSTOCK.TryInsert() = Level:Benign
                    End !If Access:RETSTOCK.PrimeRecord() = Level:Benign
                End !If retacc:QuantityToShip < retacc:QuantityOrdered

                Save_ret_ali_ID = Access:RETSTOCK_ALIAS.SaveFile()
                Access:RETSTOCK_ALIAS.ClearKey(ret_ali:Despatched_Only_Key)
                ret_ali:Ref_Number = retacc:SaleNumber
                ret_ali:Despatched = 'PEN'
                Set(ret_ali:Despatched_Only_Key,ret_ali:Despatched_Only_Key)
                Loop
                    If Access:RETSTOCK_ALIAS.NEXT()
                       Break
                    End !If
                    If ret_ali:Ref_Number <> retacc:SaleNumber      |
                    Or ret_ali:Despatched <> 'PEN'      |
                        Then Break.  ! End If
                    !I've changed a key element, but I'm breaking out straight
                    !away, so it's shouldn't matter that I'm not saving the
                    !position.
                    If ret_ali:Part_Number = retpar:PartNumber And ret_ali:Quantity = retacc:QuantityOrdered
                        Access:RETSTOCK.ClearKey(res:Record_Number_Key)
                        res:Record_Number = ret_ali:Record_Number
                        If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
                            !Found
                            res:Despatched = 'OLD'
                            res:Previous_Sale_Number = ret:Ref_Number
                            Access:RETSTOCK.TryUpdate()
                            Break

                        Else!If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                        End!If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
                    End !If res:Part_Number = retpar:PartNumber
                End !Loop
                !Access:RETSTOCK_ALIAS.RestoreFile(Save_res_ID)
                Access:RETSTOCK_ALIAS.RestoreFile(Save_ret_ali_ID)   ! Fixed BE 21/02/03

                !Take Part From Stock
                sto:Quantity_Stock -= retacc:QuantityToShip
                If sto:Quantity_Stock < 0
                    sto:Quantity_Stock = 0
                End !If sto:Quantity_Stock < 0
                If Access:STOCK.TryUpdate() = Level:Benign
                    if access:stohist.primerecord() = level:benign
                        shi:ref_number           = sto:ref_number
                        access:users.clearkey(use:password_key)
                        use:password              =glo:password
                        access:users.fetch(use:password_key)
                        shi:user                  = use:user_code    
                        shi:date                 = today()
                        shi:transaction_type     = 'DEC'
                        shi:despatch_note_number = ''
                        shi:job_number           = ret:ref_number
                        shi:quantity             = retacc:QuantityToShip
                        shi:purchase_cost        = sto:purchase_cost
                        shi:sale_cost            = sto:sale_cost
                        shi:retail_cost          = sto:retail_cost
                        shi:information          = 'COMPANY: ' & Clip(ret:company_name) & '<13,10>PURCHASE ORDER NO: ' & Clip(RET:Purchase_Order_Number) & |
                                                    '<13,10>CONTACT: ' & CLip(RET:Contact_Name)
                        shi:notes                = 'RETAIL ITEM SOLD'
                        if access:stohist.insert()
                           access:stohist.cancelautoinc()
                        end
                    end!if access:stohist.primerecord() = level:benign

                End !If Acccess:STOCK.TryUpdate() = Level:Benign
            Else !If Access:RETSTOCK.TryInsert() = Level:Benign
                !Insert Failed
            End !If Access:RETSTOCK.TryInsert() = Level:Benign
        End !If Access:RETSTOCK.PrimeRecord() = Level:Benign

    Else! If Access:.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'BackOrderProcessing',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_orp_id',save_orp_id,'BackOrderProcessing',1)
    SolaceViewVars('save_tratmp_id',save_tratmp_id,'BackOrderProcessing',1)
    SolaceViewVars('save_ret_ali_id',save_ret_ali_id,'BackOrderProcessing',1)
    SolaceViewVars('tmp:StockBelow',tmp:StockBelow,'BackOrderProcessing',1)
    SolaceViewVars('save_res_id',save_res_id,'BackOrderProcessing',1)
    SolaceViewVars('save_retpar_id',save_retpar_id,'BackOrderProcessing',1)
    SolaceViewVars('save_retacc_id',save_retacc_id,'BackOrderProcessing',1)
    SolaceViewVars('tmp:CurrentStock',tmp:CurrentStock,'BackOrderProcessing',1)
    SolaceViewVars('tmp:TotalOrdered',tmp:TotalOrdered,'BackOrderProcessing',1)
    SolaceViewVars('tmp:TotalToShip',tmp:TotalToShip,'BackOrderProcessing',1)
    SolaceViewVars('tmp:CloseWindow',tmp:CloseWindow,'BackOrderProcessing',1)
    SolaceViewVars('tmp:CurrentStockAfter',tmp:CurrentStockAfter,'BackOrderProcessing',1)
    SolaceViewVars('tmp:QuantityAvailable',tmp:QuantityAvailable,'BackOrderProcessing',1)
    SolaceViewVars('tmp:QuantityOnOrder',tmp:QuantityOnOrder,'BackOrderProcessing',1)
    SolaceViewVars('tmp:Location',tmp:Location,'BackOrderProcessing',1)
    SolaceViewVars('tmp:VatRate',tmp:VatRate,'BackOrderProcessing',1)
    SolaceViewVars('tmp:UseDeliveryAddress',tmp:UseDeliveryAddress,'BackOrderProcessing',1)
    SolaceViewVars('tmp:UseInvoice',tmp:UseInvoice,'BackOrderProcessing',1)
    SolaceViewVars('tmp:PaymentMethod',tmp:PaymentMethod,'BackOrderProcessing',1)
    SolaceViewVars('AccountQueue:AccountNumber',AccountQueue:AccountNumber,'BackOrderProcessing',1)
    SolaceViewVars('AccountQueue:SaleNumber',AccountQueue:SaleNumber,'BackOrderProcessing',1)
    SolaceViewVars('AccountQueue:PaymentMethod',AccountQueue:PaymentMethod,'BackOrderProcessing',1)
    SolaceViewVars('AccountQueue:PostcodeDelivery',AccountQueue:PostcodeDelivery,'BackOrderProcessing',1)
    SolaceViewVars('AccountQueue:CompanyNameDelivery',AccountQueue:CompanyNameDelivery,'BackOrderProcessing',1)
    SolaceViewVars('AccountQueue:BuildingNameDelivery',AccountQueue:BuildingNameDelivery,'BackOrderProcessing',1)
    SolaceViewVars('AccountQueue:AddressLine1Delivery',AccountQueue:AddressLine1Delivery,'BackOrderProcessing',1)
    SolaceViewVars('AccountQueue:AddressLine2Delivery',AccountQueue:AddressLine2Delivery,'BackOrderProcessing',1)
    SolaceViewVars('AccountQueue:AddressLine3Delivery',AccountQueue:AddressLine3Delivery,'BackOrderProcessing',1)
    SolaceViewVars('AccountQueue:TelephoneDelivery',AccountQueue:TelephoneDelivery,'BackOrderProcessing',1)
    SolaceViewVars('AccountQueue:FaxNumberDelivery',AccountQueue:FaxNumberDelivery,'BackOrderProcessing',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Panel1:2;  SolaceCtrlName = '?Panel1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StockBelow:Prompt;  SolaceCtrlName = '?tmp:StockBelow:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StockBelow;  SolaceCtrlName = '?tmp:StockBelow';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ViewStockItem;  SolaceCtrlName = '?ViewStockItem';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TotalValue;  SolaceCtrlName = '?TotalValue';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelPart;  SolaceCtrlName = '?CancelPart';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GeneratePickNotes;  SolaceCtrlName = '?GeneratePickNotes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ProcessItems;  SolaceCtrlName = '?ProcessItems';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AutoAllocate;  SolaceCtrlName = '?AutoAllocate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Recalculate;  SolaceCtrlName = '?Recalculate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Location;  SolaceCtrlName = '?tmp:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?retpar:PartNumber;  SolaceCtrlName = '?retpar:PartNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group1;  SolaceCtrlName = '?Group1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RETPAR:QuantityStock:Prompt;  SolaceCtrlName = '?RETPAR:QuantityStock:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?retpar:QuantityStock;  SolaceCtrlName = '?retpar:QuantityStock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:TotalToShip;  SolaceCtrlName = '?tmp:TotalToShip';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CurrentStockAfter:Prompt;  SolaceCtrlName = '?tmp:CurrentStockAfter:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CurrentStockAfter;  SolaceCtrlName = '?tmp:CurrentStockAfter';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:TotalOrdered;  SolaceCtrlName = '?tmp:TotalOrdered';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore4.GetBbSize('BackOrderProcessing','?List')    !Xplore
  BRW4.SequenceNbr = 0                                !Xplore
  Xplore5.GetBbSize('BackOrderProcessing','?List:2')  !Xplore
  BRW5.SequenceNbr = 0                                !Xplore
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BackOrderProcessing')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'BackOrderProcessing')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1:2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='BackOrderProcessing'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:INVOICE.Open
  Relate:RETACCOUNTSLIST.Open
  Relate:RETDESNO.Open
  Relate:RETSALES_ALIAS.Open
  Relate:RETSTOCK_ALIAS.Open
  Relate:TRADETMP.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Access:RETSTOCK.UseFile
  Access:RETSALES.UseFile
  Access:STOCK.UseFile
  Access:ORDPARTS.UseFile
  Access:ORDERS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:RETPARTSLIST,SELF)
  BRW5.Init(?List:2,Queue:Browse:1.ViewPosition,BRW5::View:Browse,Queue:Browse:1,Relate:RETACCOUNTSLIST,SELF)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore4.Init(ThisWindow,BRW4,Queue:Browse,window,?List,'sbg01app.INI','>Header',0,BRW4.ViewOrder,Xplore4.RestoreHeader,BRW4.SequenceNbr,XploreMask4,XploreMask14,XploreTitle4,BRW4.FileSeqOn)
  Xplore5.Init(ThisWindow,BRW5,Queue:Browse:1,window,?List:2,'sbg01app.INI','>Header',0,BRW5.ViewOrder,Xplore5.RestoreHeader,BRW5.SequenceNbr,XploreMask5,XploreMask15,XploreTitle5,BRW5.FileSeqOn)
  ! support for CPCS
  BRW4.Q &= Queue:Browse
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,retpar:LocPartNumberKey)
  BRW4.AddRange(retpar:Processed)
  BRW4.AddLocator(BRW4::Sort1:Locator)
  BRW4::Sort1:Locator.Init(?retpar:PartNumber,retpar:PartNumber,1,BRW4)
  BRW4.AddSortOrder(,retpar:LocPartNumberKey)
  BRW4.AddRange(retpar:Processed)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(?retpar:PartNumber,retpar:PartNumber,1,BRW4)
  BRW4.AddField(retpar:PartNumber,BRW4.Q.retpar:PartNumber)
  BRW4.AddField(retpar:Description,BRW4.Q.retpar:Description)
  BRW4.AddField(retpar:QuantityBackOrder,BRW4.Q.retpar:QuantityBackOrder)
  BRW4.AddField(retpar:QuantityOnOrder,BRW4.Q.retpar:QuantityOnOrder)
  BRW4.AddField(retpar:QuantityStock,BRW4.Q.retpar:QuantityStock)
  BRW4.AddField(retpar:RecordNumber,BRW4.Q.retpar:RecordNumber)
  BRW4.AddField(retpar:Location,BRW4.Q.retpar:Location)
  BRW4.AddField(retpar:Processed,BRW4.Q.retpar:Processed)
  BRW5.Q &= Queue:Browse:1
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,retacc:AccountNumberKey)
  BRW5.AddRange(retacc:RefNumber)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,retacc:AccountNumber,1,BRW5)
  BRW5.AddField(retacc:CompanyName,BRW5.Q.retacc:CompanyName)
  BRW5.AddField(retacc:AccountNumber,BRW5.Q.retacc:AccountNumber)
  BRW5.AddField(retacc:SaleNumber,BRW5.Q.retacc:SaleNumber)
  BRW5.AddField(retacc:SaleType,BRW5.Q.retacc:SaleType)
  BRW5.AddField(retacc:DateOrdered,BRW5.Q.retacc:DateOrdered)
  BRW5.AddField(retacc:QuantityOrdered,BRW5.Q.retacc:QuantityOrdered)
  BRW5.AddField(retacc:QuantityToShip,BRW5.Q.retacc:QuantityToShip)
  BRW5.AddField(retacc:RecordNumber,BRW5.Q.retacc:RecordNumber)
  BRW5.AddField(retacc:RefNumber,BRW5.Q.retacc:RefNumber)
  FDCB17.Init(tmp:Location,?tmp:Location,Queue:FileDropCombo.ViewPosition,FDCB17::View:FileDropCombo,Queue:FileDropCombo,Relate:TRADETMP,ThisWindow,GlobalErrors,0,1,0)
  FDCB17.Q &= Queue:FileDropCombo
  FDCB17.AddSortOrder(tratmp:CompanyNameKey)
  FDCB17.AddField(tratmp:Company_Name,FDCB17.Q.tratmp:Company_Name)
  FDCB17.AddField(tratmp:RefNumber,FDCB17.Q.tratmp:RefNumber)
  ThisWindow.AddItem(FDCB17.WindowComponent)
  FDCB17.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:RETACCOUNTSLIST.Close
    Relate:RETDESNO.Close
    Relate:RETSALES_ALIAS.Close
    Relate:RETSTOCK_ALIAS.Close
    Relate:TRADETMP.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore4.EraseVisual()                             !Xplore
    Xplore5.EraseVisual()                             !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore4.sq.Col = Xplore4.CurrentCol
    GET(Xplore4.sq,Xplore4.sq.Col)
    IF Xplore4.ListType = 1 AND BRW4.ViewOrder = False !Xplore
      BRW4.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore4.PutInix('BackOrderProcessing','?List',BRW4.SequenceNbr,Xplore4.sq.AscDesc) !Xplore
  END                                                 !Xplore
  IF ThisWindow.Opened                                !Xplore
    Xplore5.sq.Col = Xplore5.CurrentCol
    GET(Xplore5.sq,Xplore5.sq.Col)
    IF Xplore5.ListType = 1 AND BRW5.ViewOrder = False !Xplore
      BRW5.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore5.PutInix('BackOrderProcessing','?List:2',BRW5.SequenceNbr,Xplore5.sq.AscDesc) !Xplore
  END                                                 !Xplore
  IF ThisThreadActive
    ThreadQ.Proc='BackOrderProcessing'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore4.Kill()                                      !Xplore
  Xplore5.Kill()                                      !Xplore
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'BackOrderProcessing',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  Remove(RETACCOUNTSLIST)
  Remove(RETPARTSLIST)
  Remove(TRADETMP)
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateRETACCOUNTSLIST
    ReturnValue = GlobalResponse
  END
  brw4.updateviewrecord()
  retpar:QuantityStock -= tmp:TotalToShip
  Access:RETPARTSLIST.TryUpdate()
  Brw4.ResetSort(1)
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore4.Upper = True                                !Xplore
  Xplore5.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore4.AddField(retpar:PartNumber,BRW4.Q.retpar:PartNumber)
  Xplore4.AddField(retpar:Description,BRW4.Q.retpar:Description)
  Xplore4.AddField(retpar:QuantityBackOrder,BRW4.Q.retpar:QuantityBackOrder)
  Xplore4.AddField(retpar:QuantityOnOrder,BRW4.Q.retpar:QuantityOnOrder)
  Xplore4.AddField(retpar:QuantityStock,BRW4.Q.retpar:QuantityStock)
  Xplore4.AddField(retpar:RecordNumber,BRW4.Q.retpar:RecordNumber)
  Xplore4.AddField(retpar:Location,BRW4.Q.retpar:Location)
  Xplore4.AddField(retpar:Processed,BRW4.Q.retpar:Processed)
  BRW4.FileOrderNbr = BRW4.AddSortOrder(,retpar:LocPartNumberKey) !Xplore Sort Order for File Sequence
  BRW4.AddRange(retpar:Processed)                     !Xplore
  BRW4.SetOrder('')                                   !Xplore
  BRW4.ViewOrder = True                               !Xplore
  Xplore4.AddAllColumnSortOrders(1)                   !Xplore
  BRW4.ViewOrder = False                              !Xplore
  Xplore5.AddField(retacc:CompanyName,BRW5.Q.retacc:CompanyName)
  Xplore5.AddField(retacc:AccountNumber,BRW5.Q.retacc:AccountNumber)
  Xplore5.AddField(retacc:SaleNumber,BRW5.Q.retacc:SaleNumber)
  Xplore5.AddField(retacc:SaleType,BRW5.Q.retacc:SaleType)
  Xplore5.AddField(retacc:DateOrdered,BRW5.Q.retacc:DateOrdered)
  Xplore5.AddField(retacc:QuantityOrdered,BRW5.Q.retacc:QuantityOrdered)
  Xplore5.AddField(retacc:QuantityToShip,BRW5.Q.retacc:QuantityToShip)
  Xplore5.AddField(retacc:RecordNumber,BRW5.Q.retacc:RecordNumber)
  Xplore5.AddField(retacc:RefNumber,BRW5.Q.retacc:RefNumber)
  BRW5.FileOrderNbr = BRW5.AddSortOrder(,retacc:AccountNumberKey) !Xplore Sort Order for File Sequence
  BRW5.AddRange(retacc:RefNumber)                     !Xplore
  BRW5.SetOrder('')                                   !Xplore
  BRW5.ViewOrder = True                               !Xplore
  Xplore5.AddAllColumnSortOrders(1)                   !Xplore
  BRW5.ViewOrder = False                              !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ViewStockItem
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ViewStockItem, Accepted)
      If SecurityCheck('STOCK - CHANGE')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else !SecurityCheck('STOCK - CHANGE')
          Access:RETPARTSLIST.Clearkey(retpar:RecordNumberKey)
          retpar:RecordNumber = brw4.q.retpar:RecordNumber
          If Access:RETPARTSLIST.Tryfetch(retpar:RecordNumberKey) = Level:Benign
              Access:STOCK.Clearkey(sto:Ref_Number_Key)
              sto:Ref_Number  = retpar:PartRefNumber
              If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                  !Found
                  Globalrequest = ChangeRecord
                  UpdateStock
              Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                  !Error
              End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          End !If Access:RETPARTSLIST.Tryfetch(retpar:RecordNumberKey) = Level:Benign
      
      End !SecurityCheck('STOCK - CHANGE')
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ViewStockItem, Accepted)
    OF ?TotalValue
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TotalValue, Accepted)
      !Total Value of all the parts required
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      TotalCost$ = 0
      TotalCostEuro$ = 0
      ! Start Change 2379 BE(01/04/03)
      TotalSaleCost$ = 0
      TotalSaleCostEuro$ = 0
      TotalExchangeCost$ = 0
      TotalExchangeCostEuro$ = 0
      ItemCost$ = 0
      ! End Change 2379 BE(01/04/03)
      Save_retpar_ID = Access:RETPARTSLIST.SaveFile()
      Access:RETPARTSLIST.ClearKey(retpar:LocPartNumberKey)
      retpar:Location   = tmp:Location
      retpar:Processed  = 0
      Set(retpar:LocPartNumberKey,retpar:LocPartNumberKey)
      Loop
          If Access:RETPARTSLIST.NEXT()
             Break
          End !If
          If retpar:Location   <> tmp:Location      |
          Or retpar:Processed  <> 0      |
              Then Break.  ! End If
          Save_retacc_ID = Access:RETACCOUNTSLIST.SaveFile()
          Access:RETACCOUNTSLIST.ClearKey(retacc:AccountNumberKey)
          retacc:RefNumber     = retpar:RecordNumber
          Set(retacc:AccountNumberKey,retacc:AccountNumberKey)
          Loop
              If Access:RETACCOUNTSLIST.NEXT()
                 Break
              End !If
              If retacc:RefNumber     <> retpar:RecordNumber      |
                  Then Break.  ! End If
      
              UseEuro# = 0
              Access:RETSTOCK.ClearKey(res:DespatchedPartKey)
              res:Ref_Number  = retacc:SaleNumber
              res:Despatched  = 'PEN'
              res:Part_Number = retpar:PartNumber
              If Access:RETSTOCK.TryFetch(res:DespatchedPartKey) = Level:Benign
                  !Found
      
                  ! Start Change 2379 BE(01/04/03)
                  ItemCost$ = res:Item_Cost
                  ! End Change 2379 BE(01/04/03)
      
                  Access:RETSALES.Clearkey(ret:Ref_Number_Key)
                  ret:Ref_Number  = res:Ref_Number
                  If Access:RETSALES.Tryfetch(ret:Ref_Number_Key) = Level:Benign
                      !Found
                      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                      sub:Account_Number  = ret:Account_Number
                      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                          !Found
                          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                          tra:Account_Number  = sub:Main_Account_Number
                          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                              !Found
                              If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                                  If sub:EuroApplies
                                      UseEuro# = 1
                                  End !If sub:EuroApplies.
                              Else !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                                  If tra:EuroApplies
                                      UseEuro# = 1
                                  End !If tra:EuroApplies
                              End !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
      
                              ! Start Change 2379 BE(01/04/03)
                              !
                              ! Exchange Parts are Zero Rated in res:Item_Cost
                              ! So re-calculate here for purposes of split totalling
                              !
                              IF (retacc:SaleType = 'EXC') THEN
                                  IF (tra:RetailZeroParts = 1) THEN
                                      ItemCost$ = 0
                                  ELSE
                                      IF (tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES') THEN
                                          CASE sub:Retail_Price_Structure
                                              OF 'RET'
                                                  ItemCost$   = res:Retail_Cost
                                              OF 'PUR'
                                                  ItemCost$   = res:Purchase_Cost
                                              ELSE
                                                  ItemCost$   = res:Sale_Cost
                                          END
                                      ELSE
                                          CASE tra:Retail_Price_Structure
                                              OF 'RET'
                                                  ItemCost$   = res:Retail_Cost
                                              OF 'PUR'
                                                  ItemCost$   = res:Purchase_Cost
                                              ELSE
                                                  ItemCost$   = res:Sale_Cost
                                          END
                                      END
                                  END
                              END
                              ! End Change 2379 BE(01/04/03)
      
                          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                              !Error
                          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                      Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                          !Error
                      End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      
                      If ret:Invoice_Number <> ''
                          Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
                          inv:Invoice_Number  = ret:Invoice_Number
                          If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                              !Found
                              If inv:UseAlternativeAddress
                                  UseEuro# = 2
                              End !If inv:UseAlternativeAddress
                          Else! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                          
                      End !If ret:Invoice_Number <> ''
      
                  Else ! If Access:RETSALES.Tryfetch(ret:Ref_Number_Key) = Level:Benign
                      !Error
                  End !If Access:RETSALES.Tryfetch(ret:Ref_Number_Key) = Level:Benign
      
                  Case UseEuro#
                      Of 0
                          ! Start Change 2379 BE(01/04/03)
                          IF (retacc:SaleType = 'EXC') THEN
                              TotalExchangeCost$  += (ItemCost$ * res:Quantity)
                          ELSE
                              TotalSaleCost$  += (ItemCost$ * res:Quantity)
                          END
                          ! End Change 2379 BE(01/04/03)
                          TotalCost$  += (res:Item_Cost * res:Quantity)
                      Of 1
                          ! Start Change 2379 BE(01/04/03)
                          IF (retacc:SaleType = 'EXC') THEN
                              TotalExchangeCostEuro$  += (ItemCost$ * res:Quantity) * def:EuroRate
                          ELSE
                              TotalSaleCostEuro$  += (ItemCost$ * res:Quantity) * def:EuroRate
                          END
                          ! End Change 2379 BE(01/04/03)
                          TotalCostEuro$ += (res:Item_Cost * res:Quantity) * def:EuroRate
                      Of 2
                          ! Start Change 2379 BE(01/04/03)
                          IF (retacc:SaleType = 'EXC') THEN
                              TotalExchangeCostEuro$  += (res:Quantity * ItemCost$) * inv:EuroExhangeRate
                          ELSE
                              TotalSaleCostEuro$  += (res:Quantity * ItemCost$) * inv:EuroExhangeRate
                          END
                          ! End Change 2379 BE(01/04/03)
                          TotalCostEuro$ += res:Quantity * res:Item_Cost * inv:EuroExhangeRate
                  End !Case UseEuro#
      
                  
              Else!If Access:RETSTOCK.TryFetch(res:DespatchedPartKey) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:RETSTOCK.TryFetch(res:DespatchedPartKey) = Level:Benign
          End !Loop
          Access:RETACCOUNTSLIST.RestoreFile(Save_retacc_ID)
      End !Loop
      Access:RETPARTSLIST.RestoreFile(Save_retpar_ID)
      
      ! Start Change 2379 BE(01/04/03)
      !If TotalCostEuro$ <> 0
      !    Case MessageEx('The total value of unprocessed parts in this location is:'&|
      !      '<13,10>'&|
      !      '<13,10>Sterling: '& Format(TotalCost$,@n14.2) &|
      !      '<13,10>'&|
      !      '<13,10>Euros: '& Format(TotalCostEuro$,@n14.2)|
      !      ,'ServiceBase 2000',|
      !                   'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
      !        Of 1 ! &OK Button
      !    End!Case MessageEx
      !Else !TotalCostEuro$ <> 0
      !    Case MessageEx('The total value of unprocessed parts in this location is:'&|
      !      '<13,10>' & Format(TotalCost$,@n14.2),'ServiceBase 2000',|
      !                   'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
      !        Of 1 ! &OK Button
      !    End!Case MessageEx
      !
      !End !TotalCostEuro$ <> 0
      
      IF ((TotalExchangeCostEuro$ <> 0) OR (TotalSaleCostEuro$ <> 0)) THEN
          MessageEx('The total value of unprocessed Exchange parts in this location is:' & |
            '<13,10>' & |
            '<13,10>Sterling: '& Format(TotalExchangeCost$,@n14.2) & |
            '<13,10>' & |
            '<13,10>Euros: '& Format(TotalExchangeCostEuro$,@n14.2) & |
            '<13,10>' & |
            '<13,10>The total value of unprocessed Sale parts in this location is:'&|
            '<13,10>' & |
            '<13,10>Sterling: '& Format(TotalSaleCost$,@n14.2) & |
            '<13,10>' & |
            '<13,10>Euros: '& Format(TotalSaleCostEuro$,@n14.2) |
                         ,'ServiceBase 2000', |
                         'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
      ELSE
          MessageEx('The total value of unprocessed Exchange parts in this location is:' & |
            '<13,10>' & Format(TotalExchangeCost$,@n14.2) & |
            '<13,10>' & |
            '<13,10>The total value of unprocessed Sale parts in this location is:' & |
            '<13,10>' & Format(TotalSaleCost$,@n14.2) |
                         ,'ServiceBase 2000',|
                         'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
      END
      ! End Change 2379 BE(01/04/03)
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TotalValue, Accepted)
    OF ?CancelPart
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelPart, Accepted)
      Case MessageEx('Are you sure you want to cancel the selected Back Order request?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Access:RETPARTSLIST.Clearkey(retpar:RecordNumberKey)
              retpar:RecordNumber = brw4.q.retpar:RecordNumber
              If Access:RETPARTSLIST.Tryfetch(retpar:RecordNumberKey) = Level:Benign
                  !Found
                  Access:RETSTOCK.ClearKey(res:DespatchedPartKey)
                  res:Ref_Number  = brw5.q.retacc:SaleNumber
                  res:Despatched  = 'PEN'
                  res:Part_Number = retpar:PartNumber
                  If Access:RETSTOCK.TryFetch(res:DespatchedPartKey) = Level:Benign
                      !Found
                      res:Despatched = 'CAN'
                      If Access:RETSTOCK.Update() = Level:Benign
                          Delete(RETPARTSLIST)
                      End !If Access:RESTOCK.Update() = Level:Benign
      
                  Else!If Access:RETSTOCK.TryFetch(res:DespatchedPartKey) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                      Case MessageEx('Unable to find the selected part on Sale Number ' & brw5.q.retacc:saleNumber & '.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  End!If Access:RETSTOCK.TryFetch(res:DespatchedPartKey) = Level:Benign
                  
              Else ! If Access:RETPARTSLIST.Tryfetch(retpar:RecordNumberKey) = Level:Benign
                  !Error
              End !If Access:RETPARTSLIST.Tryfetch(retpar:RecordNumberKey) = Level:Benign
      
          Of 2 ! &No Button!If Access:RETPARTSLIST.Tryfetch(retpart:RecordNumberKey) = Level:Benign
      End!Case MessageEx
      brw4.resetsort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelPart, Accepted)
    OF ?GeneratePickNotes
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GeneratePickNotes, Accepted)
      Case MessageEx('Do you wish to Generate Sales and Pick Notes for the Process Parts, or do you wish to Quit the complete process?','ServiceBase 2000',|
                     'Styles\question.ico','|&Process Parts|&Quit Process|&Cancel',3,3,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Process Parts Button
              Clear(AccountQueue)
              Free(AccountQueue)
              Save_retacc_ID = Access:RETACCOUNTSLIST.SaveFile()
              Set(retacc:RecordNumberKey)
              Loop
                  If Access:RETACCOUNTSLIST.NEXT()
                     Break
                  End !If
      
                  If retacc:QuantityToShip = 0
                      Cycle
                  End !If retacc:QuantityToShip <> 0
      
                  Access:RETPARTSLIST.ClearKey(retpar:RecordNumberKey)
                  retpar:RecordNumber = retacc:RefNumber
                  If Access:RETPARTSLIST.TryFetch(retpar:RecordNumberKey) = Level:Benign
                      !Found
                      If retpar:Processed <> 1
                          Cycle
                      End !If retpar:Processed <> 1
                      !Is there still enough in Stock
                      Access:STOCK.Clearkey(sto:Ref_Number_Key)
                      sto:Ref_Number  = retpar:PartRefNumber
                      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          !Found
                          If sto:Quantity_Stock < retacc:QuantityToShip
                              Cycle
                          End !If sto:Quantity_Stock < retacc:QuantityToShip
                      Else! If Access:.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                  Else!If Access:RETPARTSLIST.TryFetch(retpar:RecordNumberKey) = Level:Benign
                      !Error
                      Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:RETPARTSLIST.TryFetch(retpar:RecordNumberKey) = Level:Benign
      
                  !Get the original sale from the part to see
                  !if it's an Exchange Accesory Sale
                  Access:RETSALES.Clearkey(ret:Ref_Number_Key)
                  ret:Ref_Number  = retacc:SaleNumber
                  If Access:RETSALES.Tryfetch(ret:Ref_Number_Key) = Level:Benign
                      !Found
                  Else! If Access:RETSALES.Tryfetch(ret:Ref_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:RETSALES.Tryfetch(ret:Ref_Number_Key) = Level:Benign
                  
                  Sort(AccountQueue,acctmp:AccountNumber)
                  acctmp:AccountNumber        = retacc:AccountNumber
                  acctmp:PaymentMethod        = ret:Payment_Method
                  acctmp:PostcodeDelivery     = ret:Postcode_Delivery
                  acctmp:CompanyNameDelivery  = ret:Company_Name_Delivery
                  acctmp:BuildingNameDelivery = ret:Building_Name_Delivery
                  acctmp:AddressLine1Delivery = ret:Address_Line1_Delivery
                  acctmp:AddressLine2Delivery = ret:Address_Line2_Delivery
                  acctmp:AddressLine3Delivery = ret:Address_Line3_Delivery
                  acctmp:TelephoneDelivery    = ret:Telephone_Delivery
                  acctmp:FaxNumberDelivery    = ret:Fax_Number_Delivery
                  tmp:PaymentMethod           = ret:Payment_Method
                  
                  Get(AccountQueue,'acctmp:AccountNumber,acctmp:PaymentMethod,acctmp:PostcodeDelivery,acctmp:CompanyNameDelivery,acctmp:BuildingNameDelivery,acctmp:AddressLine1Delivery,acctmp:AddressLine2Delivery,acctmp:AddressLine3Delivery,acctmp:TelephoneDelivery,acctmp:FaxNumberDelivery')
                  If Error()
                      !Create New Sale For This Account
                      If Access:RETSALES.PrimeRecord() = Level:Benign
                          Access:USERS.ClearKey(use:password_key)
                          use:Password = glo:Password
                          If Access:USERS.TryFetch(use:password_key) = Level:Benign
                              !Found
                              ret:Who_Booked  = use:User_Code
                          End!If Access:USERS.TryFetch(use:password_key) = Level:Benign
      
                          Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                          sub:Account_Number  = retacc:AccountNumber
                          If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                              !Found
                              Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                              tra:Account_Number  = sub:Main_Account_Number
                              If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                  !Found
                                  !Get the Retail Vat Rate
                                  If tra:Invoice_Sub_Accounts = 'YES'
                                      Access:VATCODE.Clearkey(vat:Vat_Code_Key)
                                      vat:Vat_Code    = sub:Retail_Vat_Code
                                      If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
                                          !Found
                                          tmp:VatRate = vat:Vat_Rate
                                      Else! If Access:.Tryfetch(vat:Vat_Code_Key) = Level:Benign
                                          !Error
                                          Assert(0,'<13,10>Fetch Error<13,10>')
                                      End! If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
                                      
                                  Else !If tra:Invoice_Sub_Accounts = 'YES'
                                      Access:VATCODE.Clearkey(vat:Vat_Code_Key)
                                      vat:Vat_Code    = tra:Retail_Vat_Code
                                      If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
                                          !Found
                                          tmp:VatRate = vat:Vat_Rate
                                      Else! If Access:.Tryfetch(vat:Vat_Code_Key) = Level:Benign
                                          !Error
                                          Assert(0,'<13,10>Fetch Error<13,10>')
                                      End! If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
                                      
                                  End !If tra:Invoice_Sub_Accounts = 'YES'
      
                                  !Work out which Invoice and Delivery addresses to use
                                  !Also find out the Retail Payment Method
                                  If tra:Use_Sub_Accounts <> 'YES'
                                      ret:Courier_Cost    = tra:Courier_Cost
                                      ret:Courier         = tra:Courier_Outgoing
                                      If tra:Use_Delivery_Address = 'YES'
                                          tmp:UseDeliveryAddress = 1
                                      End !If tra:Use_Delivery_Address = 'YES'
                                      tmp:UseInvoice = 1
                                  Else !If tra:Use_Sub_Accounts = 'YES'
                                      ret:Courier_Cost    = sub:Courier_Cost
                                      ret:Courier         = sub:Courier_Outgoing
      
                                      If tra:Invoice_Sub_Accounts = 'YES'
                                          tmp:UseInvoice = 2
                                          If sub:Use_Delivery_Address = 'YES'
                                              tmp:UseDeliveryAddress = 2
                                          End !If sub:Use_Delivery_Address = 'YES'
                                      Else !If tra:Invoice_Sub_Accounts = 'YES'
                                          tmp:UseInvoice = 1
      
                                      End !If tra:Invoice_Sub_Accounts = 'YES'
                                  End !If tra:Use_Sub_Accounts = 'YES'
      
                                  !Make the new sale the same as the original sale
                                  ret:Payment_Method = tmp:PaymentMethod
      
                                  Case tmp:UseInvoice
                                      Of 1 !Use Trade Address
                                          ret:Postcode            = tra:Postcode
                                          ret:Company_Name        = tra:Company_Name
                                          ret:Building_Name       = ''
                                          ret:Address_Line1       = tra:Address_Line1
                                          ret:Address_Line2       = tra:Address_Line2
                                          ret:Address_Line3       = tra:Address_Line3
                                          ret:Telephone_Number    = tra:Telephone_Number
                                          ret:Fax_Number          = tra:Fax_Number
                                      Of 2 !Use Sub Account Address
                                          ret:Postcode            = sub:Postcode
                                          ret:Company_Name        = sub:Company_Name
                                          ret:Building_Name       = ''
                                          ret:Address_Line1       = sub:Address_Line1
                                          ret:Address_Line2       = sub:Address_Line2
                                          ret:Address_Line3       = sub:Address_Line3
                                          ret:Telephone_Number    = sub:Telephone_Number
                                          ret:Fax_Number          = sub:Fax_Number
                                  End !Case tmp:UseInvoice
      !                            Case tmp:UseDeliveryAddress
      !                                Of 1 !Use Trade Address for Delivery
      !                                    ret:Postcode_Delivery       = tra:Postcode
      !                                    ret:Company_Name_Delivery   = tra:Company_Name
      !                                    ret:Building_Name_Delivery  = ''
      !                                    ret:Address_Line1_Delivery  = tra:Address_Line1
      !                                    ret:Address_Line2_Delivery  = tra:Address_Line2
      !                                    ret:Address_Line3_Delivery  = tra:Address_Line3
      !                                    ret:Telephone_Delivery      = tra:Telephone_Number
      !                                    ret:Fax_Number_Delivery     = tra:Fax_Number
      !                                Of 2 !Use Sub Account Address For Delivery
      !                                    ret:Postcode_Delivery       = sub:Postcode
      !                                    ret:Company_Name_Delivery   = sub:Company_Name
      !                                    ret:Building_Name_Delivery  = ''
      !                                    ret:Address_Line1_Delivery  = sub:Address_Line1
      !                                    ret:Address_Line2_Delivery  = sub:Address_Line2
      !                                    ret:Address_Line3_Delivery  = sub:Address_Line3
      !                                    ret:Telephone_Delivery      = sub:Telephone_Number
      !                                    ret:Fax_Number_Delivery     = sub:Fax_Number
      !                            End !Case tmp:UseDelivery
                                          ret:Postcode_Delivery       = acctmp:PostcodeDelivery
                                          ret:Company_Name_Delivery   = acctmp:CompanyNameDelivery
                                          ret:Building_Name_Delivery  = acctmp:BuildingNameDelivery
                                          ret:Address_Line1_Delivery  = acctmp:AddressLine1Delivery
                                          ret:Address_Line2_Delivery  = acctmp:AddressLine2Delivery
                                          ret:Address_Line3_Delivery  = acctmp:AddressLine3Delivery
                                          ret:Telephone_Delivery      = acctmp:TelephoneDelivery
                                          ret:Fax_Number_Delivery     = acctmp:FaxNumberDelivery
      
                                  ret:Account_Number  = retacc:AccountNumber
                                  ! Start Change 1764 BE(20/03/03)
                                  !ret:Purchase_Order_Number   = 'VARIOUS'
                                  Access:RETSTOCK_ALIAS.ClearKey(ret_ali:Record_Number_Key)
                                  ret_ali:Record_Number = retacc:OrigRecordNumber
                                  IF (Access:RETSTOCK_ALIAS.TryFetch(ret_ali:Record_Number_Key) = Level:Benign) THEN
                                      ret:Purchase_Order_Number = ret_ali:Purchase_Order_Number
                                  ELSE
                                      ! Safety Net in case Retstock Fetch fails
                                      ret:Purchase_Order_Number = 'VARIOUS'
                                  END
                                  ! End Change 1764 BE(20/03/03)
                              Else! If Access:.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                  !Error
                                  Assert(0,'<13,10>Fetch Error<13,10>')
                              End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                              
                          Else! If Access:.Tryfetch(sub:Account_Number_Key) = Level:Benign
                              !Error
                              Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                          
                          If Access:RETSALES.TryInsert() = Level:Benign
                              !Insert Successful
                              !Add Account to the local queue, so that you can add to this sale later
                              acctmp:AccountNumber        = retacc:AccountNumber
                              acctmp:SaleNumber           = ret:Ref_Number
                              acctmp:PaymentMethod        = tmp:PaymentMethod
                              acctmp:PostcodeDelivery     = ret:Postcode_Delivery
                              acctmp:CompanyNameDelivery  = ret:Company_Name_Delivery
                              acctmp:BuildingNameDelivery = ret:Building_Name_Delivery
                              acctmp:AddressLine1Delivery = ret:Address_Line1_Delivery
                              acctmp:AddressLine2Delivery = ret:Address_Line2_Delivery
                              acctmp:AddressLine3Delivery = ret:Address_Line3_Delivery
                              acctmp:TelephoneDelivery    = ret:Telephone_Delivery
                              acctmp:FaxNumberDelivery    = ret:Fax_Number_Delivery
                              Add(AccountQueue)
      
                              Do AddRetailPart
      
                          Else !If Access:RETSALES.TryInsert() = Level:Benign
                              !Insert Failed
                          End !If Access:RETSALES.TryInsert() = Level:Benign
                      End !If Access:RETSALES.PrimeRecord() = Level:Benign
      
                  Else !If Error()
                      !Sale Already Created
                      Access:RETSALES.ClearKey(ret:Ref_Number_Key)
                      ret:Ref_Number = acctmp:SaleNumber
                      If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
                          !Found
                          ! Start Change 1764 BE(20/03/03)
                          Access:RETSTOCK_ALIAS.ClearKey(ret_ali:Record_Number_Key)
                          ret_ali:Record_Number = retacc:OrigRecordNumber
                          IF (Access:RETSTOCK_ALIAS.TryFetch(ret_ali:Record_Number_Key) = Level:Benign) THEN
                              IF ((ret:Purchase_Order_Number <> 'VARIOUS') AND  |
                                  (ret:Purchase_Order_Number <> ret_ali:Purchase_Order_Number)) THEN
                                  ret:Purchase_Order_Number = 'VARIOUS'
                                  access:retsales.tryupdate()
                              END
                          ELSE
                              ! Safety Net in case Retstock Fetch fails
                              ret:Purchase_Order_Number = 'VARIOUS'
                              access:retsales.tryupdate()
                          END
                          ! End Change 1764 BE(20/03/03)
                          Do AddRetailPart
                      Else!If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
                          !Error
                          Assert(0,'<13,10>Fetch Error<13,10>')
                      End!If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
                  End !If Error()
      
              End !Loop
              Access:RETACCOUNTSLIST.RestoreFile(Save_retacc_ID)
              !Set(DEFAULTS)
              !Access:DEFAULTS.Next()
              Clear(AccountQueue)
              Sort(AccountQueue,acctmp:SaleNumber)
              Loop x# = 1 to Records(AccountQueue)
                  Get(AccountQueue,x#)
                  !MESSAGE(x#)
                  glo:select1  = AccountQueue.acctmp:SaleNumber
                  !Case def:RetBackOrders
                  !Of 1
                     Retail_Picking_Note('PIK')
      
              END
              tmp:CloseWindow = True
          Of 2 ! &Quit Process Button
                  tmp:CloseWindow = True
          Of 3 ! &Cancel Button
      End!Case MessageEx
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GeneratePickNotes, Accepted)
    OF ?List
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      BRW5.ApplyRange
      BRW5.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?ProcessItems
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessItems, Accepted)
      Case MessageEx('Are you sure you want to process this part?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Brw4.UpdateViewRecord()
      
              If retpar:Processed = 1
                  Case MessageEx('This part has already been Processed.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
              Else !If retpar:Process = 0
                  If tmp:CurrentStockAfter < 0
                      Case MessageEx('There are insufficient item in stock to Process this item.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  Else !If tmp:CurrentStockAfter < 0
                      retpar:Processed = 1
                      Access:RETPARTSLIST.Update()
                      Brw4.ResetSort(1)
                      Brw4.TakeScroll(Event:ScrollTop)
                  End !If tmp:CurrentStockAfter < 0
              End !If retpar:Process = 0
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessItems, Accepted)
    OF ?AutoAllocate
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AutoAllocate, Accepted)
      brw4.UpdateViewRecord()
      
      If retpar:Processed = 1
          Case MessageEx('To cannot Auto Allocate. This part has already been processed.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else !retpar:Processed = 1
      
          tmp:QuantityAvailable   = retpar:QuantityStock
      
          Setcursor(Cursor:Wait)
          Save_retacc_ID = Access:RETACCOUNTSLIST.SaveFile()
          Access:RETACCOUNTSLIST.ClearKey(retacc:AccountNumberKey)
          retacc:RefNumber     = retpar:RecordNumber
          Set(retacc:AccountNumberKey,retacc:AccountNumberKey)
          Loop
              If Access:RETACCOUNTSLIST.NEXT()
                 Break
              End !If
              If retacc:RefNumber     <> retpar:RecordNumber      |
                  Then Break.  ! End If
      
              If retacc:QuantityToShip <> 0
                  Cycle
              End !If retacc:QuantityToShip <> 0
      
              IF (IsStopAccount(retacc:AccountNumber)) THEN  ! (BE 20/02/03) CHANGE 161
                  SETCURSOR()
                  MessageEx('Warning: Account ' & CLIP(retacc:AccountNumber)  & ' has been stopped', |
                          'ServiceBase 2000', |
                          'Styles\idea.ico','|&OK',1,1,'',,'Tahoma', |
                          8,0,400,CHARSET:ANSI,12632256,'',0, |
                          beep:systemasterisk,msgex:samewidths,84,26,0)
                  SETCURSOR(CURSOR:wait)
                  CYCLE
              END
      
              If retacc:QuantityOrdered <> 0
                  If tmp:CurrentStockAfter => retacc:QuantityOrdered
                      retacc:QuantityToShip   = retacc:QuantityOrdered
                      tmp:CurrentStockAfter   -= retacc:QuantityToShip
                      Access:RETACCOUNTSLIST.TryUpdate()
                  Else !If tmp:CurrentStockAfter => retacc:QuantityOrdered
                      If tmp:CurrentStockAfter > 0
                          retacc:QuantityToShip = tmp:CurrentStockAfter
                          tmp:CurrentStockAfter = 0
                          Access:RETACCOUNTSLIST.TryUpdate()
                      End !If tmp:CurrentStockAfter > 0
                  End !If tmp:CurrentStockAfter => retacc:QuantityOrdered
      
              End !If retacc:QuantityToShip <> 0
          End !Loop
          Access:RETACCOUNTSLIST.RestoreFile(Save_retacc_ID)
          Setcursor()
      End !retpar:Processed = 1
      BRW5.ResetSort(1)
      BRW4.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AutoAllocate, Accepted)
    OF ?Recalculate
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Recalculate, Accepted)
    !Are there records?
    error# = 0
    Set(RETPARTSLIST)
    If Access:RETPARTSLIST.Next() = Level:Benign
        Case MessageEx('Warning! This will recalculate all Back Order information.'&|
          '<13,10>'&|
          '<13,10>Any changes you may of made will be lost.'&|
          '<13,10>'&|
          '<13,10>Are you sure?','ServiceBase 2000',|
                       'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
            Of 2 ! &No Button
                error# = 1
        End!Case MessageEx

    End !If Access:RETPARTSLIST.Next() = Level:Benign

    If error# = 0

        !Loop through all Back Order parts

        !Using Tradetmp file for the list of locations.
        !Delete list so I can build a new one
        Save_tratmp_ID = Access:TRADETMP.SaveFile()
        Set(TRADETMP,0)
        Loop
            If Access:TRADETMP.NEXT()
               Break
            End !If
            relate:TRADETMP.Delete(0)
        End !Loop
        Access:TRADETMP.RestoreFile(Save_tratmp_ID)

        Save_retpar_ID = Access:RETPARTSLIST.SaveFile()
        Set(RETPARTSLIST,0)
        Loop
            If Access:RETPARTSLIST.Next()
               Break
            End !If
            relate:RETPARTSLIST.Delete(0)
        End !Loop
        Access:RETPARTSLIST.RestoreFile(Save_retpar_ID)


        !Search through all parts with 'PEN', i.e. a request was made
        !I used to use 'ORD', but as I'm now using Summary Ordering For
        !Retail Stock this is no longer needed
        Setcursor(Cursor:Wait)
        !Record Count
        RecordCount# = 0
        Save_res_ID = Access:RETSTOCK.SaveFile()
        Access:RETSTOCK.ClearKey(res:DespatchedKey)
        res:Despatched           = 'PEN'
        Set(res:DespatchedKey,res:DespatchedKey)
        Loop
            If Access:RETSTOCK.NEXT()
               Break
            End !If
            If res:Despatched           <> 'PEN'      |
                Then Break.  ! End If
            RecordCount# += 1
        End !Loop
        Access:RETSTOCK.RestoreFile(Save_res_ID)
        Setcursor()

        recordspercycle         = 25
        recordsprocessed        = 0
        percentprogress         = 0
        progress:thermometer    = 0
        thiswindow.reset(1)
        open(progresswindow)

        ?progress:userstring{prop:text} = 'Running...'
        ?progress:pcttext{prop:text} = '0% Completed'

        recordstoprocess    = RecordCount#

        Save_res_ID = Access:RETSTOCK.SaveFile()
        Access:RETSTOCK.ClearKey(res:DespatchedKey)
        res:Despatched = 'PEN'
        Set(res:DespatchedKey,res:DespatchedKey)
        Loop
            If Access:RETSTOCK.NEXT()
               Break
            End !If
            If res:Despatched <> 'PEN'      |
                Then Break.  ! End If
            Do GetNextRecord2
            cancelcheck# += 1
            If cancelcheck# > (RecordsToProcess/100)
                Do cancelcheck
                If tmp:cancel = 1
                    Break
                End!If tmp:cancel = 1
                cancelcheck# = 0
            End!If cancelcheck# > 50

            !If back order part found, then add to the list

            Access:RETSALES.Clearkey(ret:Ref_Number_Key)
            ret:Ref_Number  = res:Ref_Number
            If Access:RETSALES.Tryfetch(ret:Ref_Number_Key) = Level:Benign
                !Found
                !Work Out Parts On Order. Just look for all parts numbers that match.
                tmp:QuantityOnOrder = 0
                Save_orp_ID = Access:ORDPARTS.SaveFile()
                Access:ORDPARTS.ClearKey(orp:Received_Part_Number_Key)
                orp:All_Received = 'NO'
                orp:Part_Number  = res:Part_Number
                Set(orp:Received_Part_Number_Key,orp:Received_Part_Number_Key)
                Loop
                    If Access:ORDPARTS.NEXT()
                       Break
                    End !If
                    If orp:All_Received <> 'NO'      |
                    Or orp:Part_Number  <> res:Part_Number      |
                        Then Break.  ! End If
                    !Only count parts that have come from the same stock location
                    If orp:Part_Ref_Number <> res:Part_Ref_Number
                        Cycle
                    End !If orp:Part_Ref_Number <> res:Part_Ref_Number

                    If orp:order_number <> ''
                        access:orders.clearkey(ord:order_number_key)
                        ord:order_number = orp:order_number
                        if access:orders.fetch(ord:order_number_key) = Level:Benign
                            If ord:all_received = 'NO'
                                If orp:all_received = 'NO'
                                    tmp:QuantityOnOrder += orp:quantity
                                End!If orp:all_received <> 'YES'
                            End!If ord:all_received <> 'YES'
                        end!if access:orders.fetch(ord:order_number_key) = Level:Benign
                    End!If orp:order_number <> ''

                End !Loop
                Access:ORDPARTS.RestoreFile(Save_orp_ID)

                Access:STOCK.ClearKey(sto:Ref_Number_Key)
                sto:Ref_Number = res:Part_Ref_Number
                If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                    !Found
                    !Build the Parts List
                    Access:RETPARTSLIST.ClearKey(retpar:LocPartNumberKey)
                    retpar:Location   = sto:Location
                    retpar:Processed  = 0
                    retpar:PartNumber = res:Part_Number
                    If Access:RETPARTSLIST.TryFetch(retpar:LocPartNumberKey) = Level:Benign
                        !Found
                        Retpar:QuantityBackOrder += res:Quantity
                        Access:RETPARTSLIST.Update()
                    Else!If Access:RETPARTSLIST.TryFetch(retpar:LocPartNumberKey) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                        If Access:RETPARTSLIST.Primerecord() = Level:Benign
                            retpar:PartRefNumber    = sto:Ref_Number
                            retpar:PartNumber       = sto:Part_Number
                            retpar:Description      = sto:Description
                            retpar:QuantityStock    = sto:Quantity_Stock
                            retpar:Location         = sto:Location
                            retpar:QuantityOnOrder  = tmp:QuantityOnOrder
                            retpar:QuantityBackOrder = res:Quantity
                            If Access:RETPARTSLIST.Tryinsert()
                                Access:RETPARTSLIST.Cancelautoinc()
                            End!If Access:RETPARTSLIST.Tryinsert()l:Benign
                        End!If Access:RETPARTSLIST.Primerecord() = Level:Benign

                        !Build the Location list
                        Access:TRADETMP.ClearKey(tratmp:CompanyNameKey)
                        tratmp:Company_Name = sto:Location
                        If Access:TRADETMP.TryFetch(tratmp:CompanyNameKey) = Level:Benign
                            !Found

                        Else!If Access:TRADETMP.TryFetch(tratmp:CompanyNameKey) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                            If Access:TRADETMP.Primerecord() = Level:Benign
                                tratmp:Company_Name  = sto:Location
                                If Access:TRADETMP.Tryinsert()
                                    Access:TRADETMP.Cancelautoinc()
                                End!If Access:TRADETMP.Tryinsert()nign
                            End!If Access:TRADETMP.Primerecord() = Level:Benign
                        End!If Access:TRADETMP.TryFetch(tratmp:CompanyNameKey) = Level:Benign
                     End!If Access:RETPARTSLIST.TryFetch(retpar:LocPartNumberKey) = Level:Benign

    !                Access:RETACCOUNTSLIST.ClearKey(retacc:DateOrderedKey)
    !                retacc:RefNumber     = retpar:RecordNumber
    !                retacc:DateOrdered   = res:Date_Ordered
    !                retacc:AccountNumber = ret:Account_Number
    !                If Access:RETACCOUNTSLIST.TryFetch(retacc:DateOrderedKey)
                        If Access:RETACCOUNTSLIST.Primerecord() = Level:Benign
                            retacc:AccountNumber   = ret:Account_Number
                            Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                            sub:Account_Number  = ret:Account_Number
                            If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                                !Found
                                retacc:CompanyName     = sub:Company_Name
                            Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                            retacc:DateOrdered     = ret:date_booked
                            retacc:QuantityOrdered = res:Quantity
                            retacc:QuantityToShip  = 0
                            retacc:RefNumber       = retpar:RecordNumber
                            retacc:SaleNumber       = ret:Ref_Number
                            retacc:OrigRecordNumber = res:Record_Number
                            retacc:SaleType        = ret:Payment_Method
                            If Access:RETACCOUNTSLIST.Tryinsert()
                                Access:RETACCOUNTSLIST.Cancelautoinc()
                            End!If Access:RETACCOUNTSLIST.Tryinsert()evel:Benign
                        End!If Access:RETACCOUNTSLIST.Primerecord() = Level:Benign
    !                Else !If Access:RETACCOUNTSLIST.TryFetch(retacc:DateOrderedKey) = Level:Benign
    !                    retacc:QuantityOrdered += res:Quantity
    !                    Access:RETACCOUNTSLIST.Update()
    !                End !If Access:RETACCOUNTSLIST.TryFetch(retacc:DateOrderedKey) = Level:Benign
                Else!If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign

            End! If Access:RETSALES.Tryfetch(ret:Ref_Number_Key) = Level:Beni = Level:Benign
        End !Loop
        Access:RETSTOCK.RestoreFile(Save_res_ID)
        Do EndPrintRun
        !Remove any entries below the "Is Below" figure

        Save_RETPAR_ID = Access:RETPARTSLIST.SaveFile()
        Set(RETPARTSLIST,0)
        Loop
            If Access:RETPARTSLIST.NEXT()
               Break
            End !If
            If retpar:QuantityStock < tmp:StockBelow
                relate:RETPARTSLIST.Delete(0)
            End !If retpar:Quantity_STock < tmp:StockBelow
        End !Loop
        Access:RETPARTSLIST.RestoreFile(Save_RETPAR_ID)

        Setcursor()
        close(progresswindow)

    End !If error# = 0
      BRW4.ResetSort(1)
      BRW5.ResetSort(1)
      FDCB17.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Recalculate, Accepted)
    OF ?tmp:Location
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW4.ApplyRange
      BRW4.ResetSort(1)
      Select(?List)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'BackOrderProcessing')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore4.IgnoreEvent = True                       !Xplore
     Xplore4.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore5.IgnoreEvent = True                       !Xplore
     Xplore5.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?ViewStockItem)
      End !KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      BRW5.ApplyRange
      BRW5.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?tmp:Location
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW4.ApplyRange
      BRW4.ResetSort(1)
      Select(?List)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
      If ~tmp:CloseWindow
          Cycle
      End !tmp:CloseWindow
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
    OF EVENT:GainFocus
      IF window{PROP:Iconize}=TRUE
        window{PROP:Iconize}=FALSE
        IF window{PROP:Active}<>TRUE
           window{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F6Key
              Post(Event:Accepted,?Recalculate)
          Of F7Key
              Post(Event:Accepted,?ProcessItems)
          Of F8Key
              Post(Event:Accepted,?AutoAllocate)
          Of F10Key
              If Choice(?Sheet1) = 2
                  Post(Event:Accepted,?GeneratePickNotes)
              End !If Choice(?Tab2) = 2
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Select(?tmp:StockBelow)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 4)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
      POST(xpEVENT:Xplore + 5)                        !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore4.GetColumnInfo()                         !Xplore
      Xplore5.GetColumnInfo()                         !Xplore
    OF EVENT:Timer
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Timer)
      tmp:CurrentStockAfter = brw4.q.retpar:QuantityStock - tmp:TotalToShip
      If tmp:CurrentStockAfter < 0
          ?tmp:CurrentStockAfter{prop:fontcolor} = color:Red
      Else !tmp:CurrentStockAfter < 0
          ?tmp:CurrentStockAfter{prop:fontcolor} = color:none
      End !tmp:CurrentStockAfter < 0
      Display(?tmp:CurrentStockAfter)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Timer)
    OF EVENT:Sized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Sized',PRIORITY(8000)
      Xplore4.GetColumnInfo()                         !Xplore
      Xplore5.GetColumnInfo()                         !Xplore
    OF EVENT:Iconized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Iconized',PRIORITY(9000)
      OF xpEVENT:Xplore + 4                           !Xplore
        Xplore4.GetColumnInfo()                       !Xplore
      OF xpEVENT:Xplore + 5                           !Xplore
        Xplore5.GetColumnInfo()                       !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
! (BE 20/02/03) CHANGE 161
IsStopAccount   PROCEDURE(STRING AccountNumber)
Save_Sub_Id     USHORT
Save_Tra_Id     USHORT
Result          SHORT
                CODE
                Result = 0  ! Default False
                Save_Sub_Id = Access:SUBTRACC.SaveFile()
                Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                sub:Account_Number = AccountNumber
                If (Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign) THEN
                    Save_Tra_Id = Access:TRADEACC.SaveFile()
                    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                    tra:Account_Number  = sub:Main_Account_Number
                    IF (Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign) THEN
                        IF (tra:Use_Sub_Accounts = 'YES') THEN
                            IF (sub:Stop_Account = 'YES') THEN
                               Result = 1 ! True
                            END
                        ELSE
                            IF (tra:Stop_Account = 'YES') THEN
                               Result = 1 ! True
                            END
                        END
                    END
                    Access:TRADEACC.RestoreFile(Save_Tra_Id)
                END
                Access:SUBTRACC.RestoreFile(Save_Sub_Id)
                RETURN(Result)

BRW4.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?Sheet1) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 1
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 0
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW4.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,4,'GetFreeElementName'
  IF BRW4.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW4.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW4.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,4,'GetFreeElementPosition'
  IF BRW4.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW4.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,4
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore4.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore4.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore4.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW4.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore4.sq)                    !Xplore
    Xplore4.SetupOrder(BRW4.SortOrderNbr)             !Xplore
    GET(Xplore4.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW4.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW4.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW4.FileOrderNbr,Force)      !Xplore
  END                                                 !Xplore
  IF Choice(?Sheet1) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW4.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,4,'TakeEvent'
  Xplore4.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?List                                  !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore4.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore4.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore4.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore4.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore4.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW4.FileSeqOn = False                         !Xplore
       Xplore4.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW4.SavePosition()                           !Xplore
       Xplore4.HandleMyEvents()                       !Xplore
       !BRW4.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?List                             !Xplore
  PARENT.TakeEvent


BRW4.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore4.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = retpar:RecordNumber
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW5.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,5,'GetFreeElementName'
  IF BRW5.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW5.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW5.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,5,'GetFreeElementPosition'
  IF BRW5.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW5.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.EIP &= BRW5::EIPManager
  SELF.AddEditControl(EditInPlace::RETACC:QuantityToShip,7)
  SELF.AddEditControl(,5)
  SELF.AddEditControl(,6)
  SELF.AddEditControl(,1)
  SELF.AddEditControl(,2)
  SELF.ArrowAction = EIPAction:Default+EIPAction:Remain+EIPAction:RetainColumn
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW5.ResetFromView PROCEDURE

tmp:TotalOrdered:Sum REAL
tmp:TotalToShip:Sum  REAL
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:RETACCOUNTSLIST.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    tmp:TotalOrdered:Sum += retacc:QuantityOrdered
    tmp:TotalToShip:Sum += retacc:QuantityToShip
  END
  tmp:TotalOrdered = tmp:TotalOrdered:Sum
  tmp:TotalToShip = tmp:TotalToShip:Sum
  PARENT.ResetFromView
  Relate:RETACCOUNTSLIST.SetQuickScan(0)
  SETCURSOR()


BRW5.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,5
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore5.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore5.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore5.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW5.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore5.sq)                    !Xplore
    Xplore5.SetupOrder(BRW5.SortOrderNbr)             !Xplore
    GET(Xplore5.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW5.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW5.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW5.FileOrderNbr,Force)      !Xplore
  ELSE
    RETURN SELF.SetSort(1,FORCE)
  END                                                 !Xplore
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW5.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,5,'TakeEvent'
  Xplore5.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?List:2                                !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore5.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore5.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore5.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore5.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore5.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW5.FileSeqOn = False                         !Xplore
       Xplore5.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW5.SavePosition()                           !Xplore
       Xplore5.HandleMyEvents()                       !Xplore
       !BRW5.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?List:2                           !Xplore
  PARENT.TakeEvent


BRW5.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore5.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


EditInPlace::RETACC:QuantityToShip.TakeEvent PROCEDURE(UNSIGNED Event)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %EditInPlaceManagerMethodCodeSection) DESC(Edit-In-Place Manager Executable Code Section) ARG(12, 1, TakeEvent, (UNSIGNED Event),BYTE)
  ReturnValue = PARENT.TakeEvent(Event)
  ! (BE 20/02/03) CHANGE 161
  IF (EVENT() = Event:Selected) THEN
      IF (IsStopAccount(retacc:AccountNumber)) THEN
          MessageEx('Warning: This Account has been stopped', |
                      'ServiceBase 2000', |
                      'Styles\idea.ico','|&OK',1,1,'',,'Tahoma', |
                      8,0,400,CHARSET:ANSI,12632256,'',0, |
                      beep:systemasterisk,msgex:samewidths,84,26,0)
          PRESSKEY(TabKey)
      END
  END
  ! After Embed Point: %EditInPlaceManagerMethodCodeSection) DESC(Edit-In-Place Manager Executable Code Section) ARG(12, 1, TakeEvent, (UNSIGNED Event),BYTE)
  RETURN ReturnValue

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW4.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW4.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW4.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW4.ResetPairsQ PROCEDURE()
  CODE
Xplore4.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue BYTE
PV          &PrintPreviewClass
  CODE
  !Standard Previewer
  PV           &= NEW(PrintPreviewClass)
  PV.Init(PQ)
  PV.Maximize   = True
  ReturnValue   = PV.DISPLAY(PageWidth,1,1,1)
  PV.Kill()
  DISPOSE(PV)
  RETURN ReturnValue
!================================================================================
!Xplore4.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore4.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW4.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !retpar:PartNumber
  OF 2 !retpar:Description
  OF 3 !retpar:QuantityBackOrder
  OF 4 !retpar:QuantityOnOrder
  OF 5 !retpar:QuantityStock
  OF 6 !retpar:RecordNumber
  OF 7 !retpar:Location
  OF 8 !retpar:Processed
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore4.SetNewOrderFields PROCEDURE()
  CODE
  BRW4.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore4.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore4Step8,retpar:LocPartNumberKey)
  SELF.FQ.SortField = SELF.BC.AddSortOrder(,retpar:LocPartNumberKey)
  EXECUTE SortQRecord
    BEGIN
      Xplore4Step1.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore4Locator1)
      Xplore4Locator1.Init(?retpar:PartNumber,retpar:PartNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore4Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore4Locator2)
      Xplore4Locator2.Init(,retpar:Description,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore4Step3.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore4Locator3)
      Xplore4Locator3.Init(,retpar:QuantityBackOrder,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore4Step4.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore4Locator4)
      Xplore4Locator4.Init(,retpar:QuantityOnOrder,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore4Step5.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore4Locator5)
      Xplore4Locator5.Init(?retpar:QuantityStock,retpar:QuantityStock,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore4Step6.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore4Locator6)
      Xplore4Locator6.Init(,retpar:RecordNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore4Step7.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore4Locator7)
      Xplore4Locator7.Init(,retpar:Location,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore4Step8.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore4Locator8)
      Xplore4Locator8.Init(,retpar:Processed,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  SELF.BC.AddRange(retpar:Processed)
  RETURN
!================================================================================
Xplore4.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore4.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW4.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(RETPARTSLIST)
  END
  RETURN TotalRecords
!================================================================================
Xplore4.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('retpar:PartNumber')          !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Part Number')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Part Number')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retpar:Description')         !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Description')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Description')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retpar:QuantityBackOrder')   !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Back Order')                 !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Quantity On Back Order')     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retpar:QuantityOnOrder')     !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('On Order')                   !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Quantity On Order')          !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retpar:QuantityStock')       !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('In Stock')                   !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Quantity In Stock')          !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retpar:RecordNumber')        !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Record Number')              !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Record Number')              !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retpar:Location')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Location')                   !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Location')                   !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retpar:Processed')           !Field Name
                SHORT(4)                              !Default Column Width
                PSTRING('Processed')                  !Header
                PSTRING('@n1')                        !Picture
                PSTRING('Processed')                  !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(8)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)
BRW5.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW5.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW5.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW5.ResetPairsQ PROCEDURE()
  CODE
Xplore5.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue BYTE
PV          &PrintPreviewClass
  CODE
  !Standard Previewer
  PV           &= NEW(PrintPreviewClass)
  PV.Init(PQ)
  PV.Maximize   = True
  ReturnValue   = PV.DISPLAY(PageWidth,1,1,1)
  PV.Kill()
  DISPOSE(PV)
  RETURN ReturnValue
!================================================================================
!Xplore5.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore5.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW5.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !retacc:CompanyName
  OF 2 !retacc:AccountNumber
  OF 3 !retacc:SaleNumber
  OF 4 !retacc:SaleType
  OF 5 !retacc:DateOrdered
  OF 6 !retacc:QuantityOrdered
  OF 7 !retacc:QuantityToShip
  OF 8 !retacc:RecordNumber
  OF 9 !retacc:RefNumber
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore5.SetNewOrderFields PROCEDURE()
  CODE
  BRW5.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore5.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore5Step9,retacc:AccountNumberKey)
  SELF.FQ.SortField = SELF.BC.AddSortOrder(,retacc:AccountNumberKey)
  EXECUTE SortQRecord
    BEGIN
      Xplore5Step1.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore5Locator1)
      Xplore5Locator1.Init(,retacc:CompanyName,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore5Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore5Locator2)
      Xplore5Locator2.Init(,retacc:AccountNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore5Step3.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore5Locator3)
      Xplore5Locator3.Init(,retacc:SaleNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore5Step4.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore5Locator4)
      Xplore5Locator4.Init(,retacc:SaleType,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore5Step5.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore5Locator5)
      Xplore5Locator5.Init(,retacc:DateOrdered,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore5Step6.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore5Locator6)
      Xplore5Locator6.Init(,retacc:QuantityOrdered,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore5Step7.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore5Locator7)
      Xplore5Locator7.Init(,retacc:QuantityToShip,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore5Step8.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore5Locator8)
      Xplore5Locator8.Init(,retacc:RecordNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore5Step9.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore5Locator9)
      Xplore5Locator9.Init(,retacc:RefNumber,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  SELF.BC.AddRange(retacc:RefNumber)
  RETURN
!================================================================================
Xplore5.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore5.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW5.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(RETACCOUNTSLIST)
  END
  RETURN TotalRecords
!================================================================================
Xplore5.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('retacc:CompanyName')         !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Company Name')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Company Name')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retacc:AccountNumber')       !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Account Number')             !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Account Number')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retacc:SaleNumber')          !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Sale No')                    !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Sale Number')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retacc:SaleType')            !Field Name
                SHORT(16)                             !Default Column Width
                PSTRING('Type')                       !Header
                PSTRING('@s4')                        !Picture
                PSTRING('Type')                       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retacc:DateOrdered')         !Field Name
                SHORT(40)                             !Default Column Width
                PSTRING('Requested')                  !Header
                PSTRING('@d6')                        !Picture
                PSTRING('Date Ordered')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retacc:QuantityOrdered')     !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Req')                        !Header
                PSTRING('@s5')                        !Picture
                PSTRING('Quantity Ordered')           !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retacc:QuantityToShip')      !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('To Ship')                    !Header
                PSTRING('@s5')                        !Picture
                PSTRING('Quantity To Ship')           !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retacc:RecordNumber')        !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Record Number')              !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Record Number')              !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('retacc:RefNumber')           !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Reference To Part Entry')    !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Reference To Part Entry')    !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(9)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
