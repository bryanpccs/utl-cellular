

   MEMBER('sbg01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBG01033.INC'),ONCE        !Local module procedure declarations
                     END


BrowsePickingReconciliation PROCEDURE                 !Generated from procedure template - Window

ThisThreadActive BYTE
CurrentTab           STRING(80)
Save_ret_ID          USHORT
Save_res2_ID         USHORT
save_res_id          USHORT,AUTO
save_ret_ali_id      USHORT,AUTO
Account_Number       STRING(15)
RET_temp             STRING('RET')
no_value_temp        REAL
tag_temp             STRING(1)
stop_on_errors_temp  BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Zero             BYTE(0)
BRW1::View:Browse    VIEW(RETSALES)
                       PROJECT(ret:Ref_Number)
                       PROJECT(ret:Purchase_Order_Number)
                       PROJECT(ret:Account_Number)
                       PROJECT(ret:date_booked)
                       PROJECT(ret:who_booked)
                       PROJECT(ret:Courier)
                       PROJECT(ret:Invoice_Number)
                       PROJECT(ret:Despatch_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
ret:Ref_Number         LIKE(ret:Ref_Number)           !List box control field - type derived from field
ret:Purchase_Order_Number LIKE(ret:Purchase_Order_Number) !List box control field - type derived from field
ret:Account_Number     LIKE(ret:Account_Number)       !List box control field - type derived from field
ret:date_booked        LIKE(ret:date_booked)          !List box control field - type derived from field
ret:who_booked         LIKE(ret:who_booked)           !List box control field - type derived from field
ret:Courier            LIKE(ret:Courier)              !List box control field - type derived from field
ret:Invoice_Number     LIKE(ret:Invoice_Number)       !List box control field - type derived from field
ret:Despatch_Number    LIKE(ret:Despatch_Number)      !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Sales Awaiting Picking'),AT(,,529,264),FONT('Tahoma',8,,),CENTER,IMM,HLP('Browse_Retail_Sales'),SYSTEM,GRAY,RESIZE
                       LIST,AT(8,36,432,220),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('39R(2)|M~Sales No~L@s8@92L(2)|M~Purchase Order Number~@s30@69L(2)|M~Account Numb' &|
   'er~@s15@47R(2)|M~Date Raised~@d6b@22L(2)|M~User~@s3@108L(2)|M~Courier~@s30@32L(2' &|
   ')|M~Invoice No~@s8@'),FROM(Queue:Browse:1)
                       BUTTON('&Insert'),AT(376,100,42,12),USE(?Insert),HIDE
                       BUTTON('Valuate &Orders'),AT(452,156,76,20),USE(?Valuate),LEFT,ICON('MONEY_SM.GIF')
                       BUTTON('&View Sale'),AT(452,184,76,20),USE(?Change),LEFT,ICON('edit.ico')
                       BUTTON('Print Routines'),AT(452,20,76,20),USE(?Print),LEFT,ICON(ICON:Print1)
                       SHEET,AT(4,4,440,256),USE(?CurrentTab),SPREAD
                         TAB('Browse Sales'),USE(?Tab:2)
                           ENTRY(@s8),AT(8,20,64,10),USE(ret:Ref_Number),RIGHT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON('Close'),AT(452,240,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
windowsdir      CString(260)
systemdir       String(260)
    MAP                             ! BE 24/02/03
CheckPickList   PROCEDURE(),LONG
    END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?ret:Ref_Number{prop:ReadOnly} = True
        ?ret:Ref_Number{prop:FontColor} = 65793
        ?ret:Ref_Number{prop:Color} = 15066597
    Elsif ?ret:Ref_Number{prop:Req} = True
        ?ret:Ref_Number{prop:FontColor} = 65793
        ?ret:Ref_Number{prop:Color} = 8454143
    Else ! If ?ret:Ref_Number{prop:Req} = True
        ?ret:Ref_Number{prop:FontColor} = 65793
        ?ret:Ref_Number{prop:Color} = 16777215
    End ! If ?ret:Ref_Number{prop:Req} = True
    ?ret:Ref_Number{prop:Trn} = 0
    ?ret:Ref_Number{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Create_Invoice      Routine
    SystemDir   = GetSystemDirectory(WindowsDir,Size(WindowsDir))
    Set(defaults)
    access:defaults.next()
  ! invoice_number_temp = 0
    error# = 0
    access:retsales.clearkey(ret:ref_number_key)
    ret:ref_number = glo:pointer
    if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
        If ret:invoice_number <> '' and error# = 0
        Else!If ret:invoice_number <> ''
            error# = 0
            If error# = 0
                fetch_error# = 0
                despatch# = 0
                access:subtracc.clearkey(sub:account_number_key)
                sub:account_number = ret:account_number
                if access:subtracc.fetch(sub:account_number_key)
                Else!if access:subtracc.fetch(sub:account_number_key) = level:benign
                    access:tradeacc.clearkey(tra:account_number_key) 
                    tra:account_number = sub:main_account_number
                    if access:tradeacc.fetch(tra:account_number_key)
                        fetch_error# = 1
                    Else!if access:tradeacc.fetch(tra:account_number_key)
                        If tra:use_sub_accounts = 'YES'
                            If sub:despatch_invoiced_jobs = 'YES'
                                If sub:despatch_paid_jobs = 'YES'
!                                        If ret:paid = 'YES'
!                                            despatch# = 1
!                                        Else
!                                            despatch# = 2
!                                        End
                                Else
                                    despatch# = 1
                                End!If sub:despatch_paid_jobs = 'YES' And ret:paid = 'YES'
                                
                            End!If sub:despatch_invoiced_jobs = 'YES'
                        End!If tra:use_sub_accounts = 'YES'
                        if tra:invoice_sub_accounts = 'YES'
                            vat_number" = sub:vat_number
                            access:vatcode.clearkey(vat:vat_code_key)
                            vat:vat_code = sub:retail_vat_code
                            if access:vatcode.fetch(vat:vat_code_key)
                                fetch_error# = 1
                            Else!if access:vatcode.fetch(vat:vat_code_key)
                               retail_rate$ = vat:vat_rate
                            end!if access:vatcode.fetch(vat:vat_code_key)
                        else!if tra:use_sub_accounts = 'YES'
                            If tra:despatch_invoiced_jobs = 'YES'
                                If tra:despatch_paid_jobs = 'YES'
!                                        If ret:paid = 'YES'
!                                            despatch# = 1
!                                        Else
!                                            despatch# = 2
!                                        End!If ret:paid = 'YES'
                                Else
                                    despatch# = 1
                                End!If tra:despatch_paid_jobs = 'YES' and ret:paid = 'YES'
                            End!If tra:despatch_invoiced_jobs = 'YES'
                            vat_number" = tra:vat_number
                            access:vatcode.clearkey(vat:vat_code_key)
                            vat:vat_code = tra:retail_vat_code
                            if access:vatcode.fetch(vat:vat_code_key)
                                fetch_error# = 1
                            Else!if access:vatcode.fetch(vat:vat_code_key)
                               retail_rate$ = vat:vat_rate
                            end
                        end!if tra:use_sub_accounts = 'YES'
                    end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
                If fetch_error# = 0
!SAGE BIT
                    If def:use_sage = 'YES'
!                            file_name = Clip(WindowsDir) & '\SAGEDATA.SPD'
!                            Remove(paramss)
!                            access:paramss.open()
!                            access:paramss.usefile()
!         !LABOUR
!                            get(paramss,0)
!                            if access:paramss.primerecord() = Level:Benign
!                                prm:account_ref       = Stripcomma(ret:Account_number)
!                                prm:name              = Stripcomma(ret:company_name)
!                                prm:address_1         = Stripcomma(ret:address_line1)
!                                prm:address_2         = Stripcomma(ret:address_line2)
!                                prm:address_3         = Stripcomma(ret:address_line3)
!                                prm:address_4         = ''
!                                prm:address_5         = Stripcomma(ret:postcode)
!                                prm:del_address_1     = Stripcomma(ret:address_line1_delivery)
!                                prm:del_address_2     = Stripcomma(ret:address_line2_delivery)
!                                prm:del_address_3     = Stripcomma(ret:address_line3_delivery)
!                                prm:del_address_4     = ''
!                                prm:del_address_5     = Stripcomma(ret:postcode_delivery)
!                                prm:cust_tel_number   = Stripcomma(ret:telephone_number)
!                                prm:contact_name      = Stripcomma(ret:contact_name)
!                                prm:notes_1           = ''
!                                prm:notes_2           = Stripcomma(Stripreturn(ret:invoice_text))
!                                prm:notes_3           = ''
!                                prm:taken_by          = Stripcomma(ret:who_booked)
!                                prm:order_number      = Stripcomma(ret:purchase_order_number)
!                                prm:cust_order_number = 'J' & Stripcomma(ret:ref_number)
!                                prm:payment_ref       = ''
!                                prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
!                                prm:global_details    = ''
!                                prm:items_net         = Stripcomma(ret:labour_cost + ret:parts_cost + ret:courier_cost)
!                                prm:items_tax         = Stripcomma(Round(ret:labour_cost * (labour_rate$/100),.01) + |
!                                                            Round(ret:parts_cost * (parts_rate$/100),.01) + |
!                                                            Round(ret:courier_cost * (labour_rate$/100),.01))
!                                prm:stock_code        = Stripcomma(DEF:Labour_Stock_Code)
!                                prm:description       = Stripcomma(DEF:Labour_Description)
!                                prm:nominal_code      = Stripcomma(DEF:Labour_Code)
!                                prm:qty_order         = '1'
!                                prm:unit_price        = '0'
!                                prm:net_amount        = Stripcomma(ret:labour_cost)
!                                prm:tax_amount        = Stripcomma(Round(ret:labour_cost * (labour_rate$/100),.01))
!                                prm:comment_1         = Stripcomma(ret:charge_type)
!                                prm:comment_2         = Stripcomma(ret:warranty_charge_type)
!                                prm:unit_of_sale      = '0'
!                                prm:full_net_amount   = '0'
!                                prm:invoice_date      = '*'
!                                prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
!                                prm:user_name         = Clip(DEF:User_Name_Sage)
!                                prm:password          = Clip(DEF:Password_Sage)
!                                prm:set_invoice_number= '*'
!                                prm:invoice_no        = '*'
!                                access:paramss.insert()
!                            end!if access:paramss.primerecord() = Level:Benign
!        !PARTS
!                            get(paramss,0)
!                            if access:paramss.primerecord() = Level:Benign
!                                prm:account_ref       = Stripcomma(ret:Account_number)
!                                prm:name              = Stripcomma(ret:company_name)
!                                prm:address_1         = Stripcomma(ret:address_line1)
!                                prm:address_2         = Stripcomma(ret:address_line2)
!                                prm:address_3         = Stripcomma(ret:address_line3)
!                                prm:address_4         = ''
!                                prm:address_5         = Stripcomma(ret:postcode)
!                                prm:del_address_1     = Stripcomma(ret:address_line1_delivery)
!                                prm:del_address_2     = Stripcomma(ret:address_line2_delivery)
!                                prm:del_address_3     = Stripcomma(ret:address_line3_delivery)
!                                prm:del_address_4     = ''
!                                prm:del_address_5     = Stripcomma(ret:postcode_delivery)
!                                prm:cust_tel_number   = Stripcomma(ret:telephone_number)
!                                If ret:surname <> ''
!                                    if ret:title = '' and ret:initial = '' 
!                                        prm:contact_name = Stripcomma(clip(ret:surname))
!                                    elsif ret:title = '' and ret:initial <> ''
!                                        prm:contact_name = Stripcomma(clip(ret:surname) & ' ' & clip(ret:initial))
!                                    elsif ret:title <> '' and ret:initial = ''
!                                        prm:contact_name = Stripcomma(clip(ret:surname) & ' ' & clip(ret:title))
!                                    elsif ret:title <> '' and ret:initial <> ''
!                                        prm:contact_name = Stripcomma(clip(ret:surname) & ' ' & clip(ret:title) & ' ' & clip(ret:initial))
!                                    else
!                                        prm:contact_name = ''
!                                    end
!                                else
!                                    prm:contact_name = ''
!                                end
!                                prm:notes_1           = Stripcomma(Stripreturn(ret:fault_description))
!                                prm:notes_2           = Stripcomma(Stripreturn(ret:invoice_text))
!                                prm:notes_3           = ''
!                                prm:taken_by          = Stripcomma(ret:who_booked)
!                                prm:order_number      = Stripcomma(ret:order_number)
!                                prm:cust_order_number = 'J' & Stripcomma(ret:ref_number)
!                                prm:payment_ref       = ''
!                                prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
!                                prm:global_details    = ''
!                                prm:items_net         = Stripcomma(ret:labour_cost + ret:parts_cost + ret:courier_cost)
!                                prm:items_tax         = Stripcomma(Round(ret:labour_cost * (labour_rate$/100),.01) + |
!                                                            Round(ret:parts_cost * (parts_rate$/100),.01) + |
!                                                            Round(ret:courier_cost * (labour_rate$/100),.01))
!                                prm:stock_code        = Stripcomma(DEF:Parts_Stock_Code)
!                                prm:description       = Stripcomma(DEF:Parts_Description)
!                                prm:nominal_code      = Stripcomma(DEF:Parts_Code)
!                                prm:qty_order         = '1'
!                                prm:unit_price        = '0'
!                                prm:net_amount        = Stripcomma(ret:parts_cost)
!                                prm:tax_amount        = Stripcomma(Round(ret:parts_cost * (parts_rate$/100),.01))
!                                prm:comment_1         = Stripcomma(ret:charge_type)
!                                prm:comment_2         = Stripcomma(ret:warranty_charge_type)
!                                prm:unit_of_sale      = '0'
!                                prm:full_net_amount   = '0'
!                                prm:invoice_date      = '*'
!                                prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
!                                prm:user_name         = Clip(DEF:User_Name_Sage)
!                                prm:password          = Clip(DEF:Password_Sage)
!                                prm:set_invoice_number= '*'
!                                prm:invoice_no        = '*'
!                                access:paramss.insert()
!                            end!if access:paramss.primerecord() = Level:Benign
!        !COURIER
!                            get(paramss,0)
!                            if access:paramss.primerecord() = Level:Benign
!                                prm:account_ref       = Stripcomma(ret:Account_number)
!                                prm:name              = Stripcomma(ret:company_name)
!                                prm:address_1         = Stripcomma(ret:address_line1)
!                                prm:address_2         = Stripcomma(ret:address_line2)
!                                prm:address_3         = Stripcomma(ret:address_line3)
!                                prm:address_4         = ''
!                                prm:address_5         = Stripcomma(ret:postcode)
!                                prm:del_address_1     = Stripcomma(ret:address_line1_delivery)
!                                prm:del_address_2     = Stripcomma(ret:address_line2_delivery)
!                                prm:del_address_3     = Stripcomma(ret:address_line3_delivery)
!                                prm:del_address_4     = ''
!                                prm:del_address_5     = Stripcomma(ret:postcode_delivery)
!                                prm:cust_tel_number   = Stripcomma(ret:telephone_number)
!                                If ret:surname <> ''
!                                    if ret:title = '' and ret:initial = '' 
!                                        prm:contact_name = Stripcomma(clip(ret:surname))
!                                    elsif ret:title = '' and ret:initial <> ''
!                                        prm:contact_name = Stripcomma(clip(ret:surname) & ' ' & clip(ret:initial))
!                                    elsif ret:title <> '' and ret:initial = ''
!                                        prm:contact_name = Stripcomma(clip(ret:surname) & ' ' & clip(ret:title))
!                                    elsif ret:title <> '' and ret:initial <> ''
!                                        prm:contact_name = Stripcomma(clip(ret:surname) & ' ' & clip(ret:title) & ' ' & clip(ret:initial))
!                                    else
!                                        prm:contact_name = ''
!                                    end
!                                else
!                                    prm:contact_name = ''
!                                end
!                                prm:notes_1           = Stripcomma(Stripreturn(ret:fault_description))
!                                prm:notes_2           = Stripcomma(Stripreturn(ret:invoice_text))
!                                prm:notes_3           = ''
!                                prm:taken_by          = Stripcomma(ret:who_booked)
!                                prm:order_number      = Stripcomma(ret:order_number)
!                                prm:cust_order_number = 'J' & Stripcomma(ret:ref_number)
!                                prm:payment_ref       = ''
!                                prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
!                                prm:global_details    = ''
!                                prm:items_net         = Stripcomma(ret:labour_cost + ret:parts_cost + ret:courier_cost)
!                                prm:items_tax         = Stripcomma(Round(ret:labour_cost * (labour_rate$/100),.01) + |
!                                                            Round(ret:parts_cost * (parts_rate$/100),.01) + |
!                                                            Round(ret:courier_cost * (labour_rate$/100),.01))
!                                prm:stock_code        = Stripcomma(DEF:Courier_Stock_Code)
!                                prm:description       = Stripcomma(DEF:Courier_Description)
!                                prm:nominal_code      = Stripcomma(DEF:Courier_Code)
!                                prm:qty_order         = '1'
!                                prm:unit_price        = '0'
!                                prm:net_amount        = Stripcomma(ret:courier_cost)
!                                prm:tax_amount        = Stripcomma(Round(ret:courier_cost * (labour_rate$/100),.01))
!                                prm:comment_1         = Stripcomma(ret:charge_type)
!                                prm:comment_2         = Stripcomma(ret:warranty_charge_type)
!                                prm:unit_of_sale      = '0'
!                                prm:full_net_amount   = '0'
!                                prm:invoice_date      = '*'
!                                prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
!                                prm:user_name         = Clip(DEF:User_Name_Sage)
!                                prm:password          = Clip(DEF:Password_Sage)
!                                prm:set_invoice_number= '*'
!                                prm:invoice_no        = '*'
!                                access:paramss.insert()
!                            end!if access:paramss.primerecord() = Level:Benign
!                            access:paramss.close()
!                            Run(CLip(DEF:Path_Sage) & '\SAGEPROJ.EXE',1)
!                            sage_error# = 0
!                            access:paramss.open()
!                            access:paramss.usefile()
!                            Set(paramss,0)
!                            If access:paramss.next()
!                                sage_error# = 1
!                            Else!If access:paramss.next()
!                                If prm:invoice_no = -1 or prm:invoice_no = 0 or prm:invoice_no = ''
!                                    sage_error# = 1
!                                Else!If prm:invoice_no = '-1' or prm:invoice_no = '0'
!                                    invoice_number_temp = prm:invoice_no
!                                    If invoice_number_temp = 0
!                                        sage_error# = 1
!                                    End!If invoice_number_temp = 0
!                                End!If prm:invoice_no = '-1'
!                            End!If access:paramss.next()
!                            access:paramss.close()
                    End!If def:use_sage = 'YES'
                    If sage_error# = 0
                        invoice_error# = 1
                        get(invoice,0)
                        if access:invoice.primerecord() = level:benign
!                                If def:use_sage = 'YES'
!                                    inv:invoice_number = invoice_number_temp
!                                End!If def:use_sage = 'YES'
                            inv:invoice_type       = 'RET'
                            inv:job_number         = ret:ref_number
                            inv:date_created       = Today()
                            inv:account_number     = ret:account_number
                            inv:total              = ret:sub_total
                            inv:vat_rate_labour    = ''
                            inv:vat_rate_parts     = ''
                            inv:vat_rate_retail    = retail_rate$
                            inv:vat_number         = def:vat_number
                            INV:Courier_Paid       = ret:courier_cost
                            inv:parts_paid         = ret:parts_cost
                            inv:labour_paid        = ''
                            inv:invoice_vat_number = vat_number"
                            invoice_error# = 0
                            If access:invoice.insert()
                                invoice_error# = 1
                                access:invoice.cancelautoinc()
                            End!If access:invoice.insert()
                        end!if access:invoice.primerecord() = level:benign
                        If Invoice_error# = 0
!                                Case despatch#
!                                    Of 1
!                                        If ret:despatched = 'YES'
!                                            Status_Routine(710,ret:current_status,a",a",a")
!                                        Else!If ret:despatched = 'YES'
!                                            Status_Routine(810,ret:current_status,a",a",a")
!                                            ret:despatched = 'REA'
!                                            ret:despatch_type = 'JOB'
!                                            ret:current_courier = ret:courier
!                                        End!If ret:despatched <> 'YES'
!                                    Of 2
!                                        Status_Routine(803,ret:current_status,a",a",a")
!                                    Else
!                                        Status_Routine(710,ret:current_status,a",a",a")
!
!                                End!Case despatch#

                            ret:invoice_number        = inv:invoice_number
                            ret:invoice_date          = Today()
                            ret:invoice_courier_cost  = ret:courier_cost
                            ret:invoice_parts_cost    = ret:parts_cost
                            ret:invoice_sub_total     = ret:sub_total
                            access:retsales.update()
                            glo:select1 = inv:invoice_number
                            Retail_Single_Invoice
                            glo:select1 = ''
                            ! Start Virtual Warehouse i/f BE(06/05/03)
                            UpdateVirtualWarehouse()
                            ! End Virtual Warehouse i/f BE(06/05/03)
                        End!If Invoice_error# = 0
                    End!If def:use_sage = 'YES' and sage_error# = 1
                End!If fetch_error# = 0
            End!If error# = 0
        End!If ret:invoice_number <> ''
    end!if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'BrowsePickingReconciliation',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'BrowsePickingReconciliation',1)
    SolaceViewVars('Save_ret_ID',Save_ret_ID,'BrowsePickingReconciliation',1)
    SolaceViewVars('Save_res2_ID',Save_res2_ID,'BrowsePickingReconciliation',1)
    SolaceViewVars('save_res_id',save_res_id,'BrowsePickingReconciliation',1)
    SolaceViewVars('save_ret_ali_id',save_ret_ali_id,'BrowsePickingReconciliation',1)
    SolaceViewVars('Account_Number',Account_Number,'BrowsePickingReconciliation',1)
    SolaceViewVars('RET_temp',RET_temp,'BrowsePickingReconciliation',1)
    SolaceViewVars('no_value_temp',no_value_temp,'BrowsePickingReconciliation',1)
    SolaceViewVars('tag_temp',tag_temp,'BrowsePickingReconciliation',1)
    SolaceViewVars('stop_on_errors_temp',stop_on_errors_temp,'BrowsePickingReconciliation',1)
    SolaceViewVars('tmp:Zero',tmp:Zero,'BrowsePickingReconciliation',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Valuate;  SolaceCtrlName = '?Valuate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Print;  SolaceCtrlName = '?Print';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ret:Ref_Number;  SolaceCtrlName = '?ret:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowsePickingReconciliation')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'BrowsePickingReconciliation')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='BrowsePickingReconciliation'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:INVOICE.Open
  Relate:RETSALES.Open
  Relate:RETSALES_ALIAS.Open
  Relate:RETSTOCK_ALIAS.Open
  Relate:USELEVEL.Open
  Relate:VATCODE.Open
  Access:RETSTOCK.UseFile
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:RETSALES,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,ret:DespatchNoRefNoKey)
  BRW1.AddRange(ret:Despatch_Number,tmp:Zero)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?ret:Ref_Number,ret:Ref_Number,1,BRW1)
  BRW1.AddField(ret:Ref_Number,BRW1.Q.ret:Ref_Number)
  BRW1.AddField(ret:Purchase_Order_Number,BRW1.Q.ret:Purchase_Order_Number)
  BRW1.AddField(ret:Account_Number,BRW1.Q.ret:Account_Number)
  BRW1.AddField(ret:date_booked,BRW1.Q.ret:date_booked)
  BRW1.AddField(ret:who_booked,BRW1.Q.ret:who_booked)
  BRW1.AddField(ret:Courier,BRW1.Q.ret:Courier)
  BRW1.AddField(ret:Invoice_Number,BRW1.Q.ret:Invoice_Number)
  BRW1.AddField(ret:Despatch_Number,BRW1.Q.ret:Despatch_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  brw1.InsertControl = 0
  brw1.DeleteControl = 0
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:RETSALES.Close
    Relate:RETSALES_ALIAS.Close
    Relate:RETSTOCK_ALIAS.Close
    Relate:USELEVEL.Close
    Relate:VATCODE.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='BrowsePickingReconciliation'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'BrowsePickingReconciliation',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  
  case request
      of deleterecord
          check_access('RETAIL SALES - DELETE',x")
          if x" = false
                          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                          End!Case MessageEx
              do_update# = false
          end
      of insertrecord
          do_update# = false
          Case MessageEx('You cannot insert from here.','ServiceBase 2000',icon:hand,'|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,0+2+0+0,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessagheEx
  end !case request
  
  if do_update# = true
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Process_Retail_Sale
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Valuate
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Valuate, Accepted)
      ! Total Value of all Sales Awaiting Picking (BE 18/02/03)
      ! Problem No 144
      !
      SET(DEFAULTS)
      Access:DEFAULTS.Next()
      TotalCost$ = 0
      TotalCostEuro$ = 0
      Save_ret_ID = Access:RETSALES.SaveFile()
      Access:RETSALES.ClearKey(ret:Despatch_Number_key)
      ret:Despatch_Number = 0
      SET(ret:Despatch_Number_key, ret:Despatch_Number_key)
      SETCURSOR(CURSOR:Wait)
      LOOP
          IF (Access:RETSALES.NEXT()) THEN
             BREAK
          END
          IF (ret:Despatch_Number <> 0) THEN
              BREAK
          END
          IF (ret:Despatched = 'XXX') THEN  ! Don't know what this despatch code is, but it is
              CYCLE                         ! excluded from the Browse List box so exclude it here also.
          END
          IF (CheckPickList() = 0) THEN
              CYCLE
          END
      
          UseEuro# = 0
          Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
          sub:Account_Number  = ret:Account_Number
          IF (Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign) THEN
              Access:TRADEACC.Clearkey(tra:Account_Number_Key)
              tra:Account_Number  = sub:Main_Account_Number
              IF (Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign) THEN
                      IF (tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES') THEN
                          If (sub:EuroApplies) THEN
                                  UseEuro# = 1
                          End !If sub:EuroApplies.
                      ELSE !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                          If (tra:EuroApplies) THEN
                                  UseEuro# = 1
                          End !If tra:EuroApplies
                      END !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
              ELSE ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Error
                  ASSERT(0)
              END !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          ELSE ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
             !Error
             ASSERT(0)
          END !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      
          IF (ret:Invoice_Number <> '') THEN
              Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
              inv:Invoice_Number  = ret:Invoice_Number
              IF (Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign) THEN
                  IF (inv:UseAlternativeAddress) THEN
                      UseEuro# = 2
                  END !If inv:UseAlternativeAddress
              ELSE! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                 !Error
                 ASSERT(0)
              END! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
          END !If ret:Invoice_Number <> ''
      
          Save_res2_ID = Access:RETSTOCK.SaveFile()
          Access:RETSTOCK.ClearKey(res:Despatched_Only_Key)
          res:Ref_Number = ret:Ref_Number
          SET(res:Despatched_Only_Key,res:Despatched_Only_Key)
          LOOP
              IF (Access:RETSTOCK.NEXT()) THEN
                  BREAK
              END
              IF (res:Ref_Number <> ret:Ref_Number) THEN
                  BREAK
              END
      
              !MESSAGE('Ref_Number: ' & FORMAT(res:Ref_Number,@n14) & |
              !        '<13,10>Despatched: ' & res:Despatched & |
              !        '<13,10>Item_Cost: ' & FORMAT(res:Item_Cost,@n14.4) & |
              !        '<13,10>Quantity: ' & FORMAT(res:Quantity,@n14.4) & |
              !        '<13,10>Eurorate: ' & FORMAT(def:EuroRate,@n14.4) & |
              !        '<13,10>UseEuro: ' & FORMAT(UseEuro#,@n14))  ! DEBUG
      
              ! Following IF statement excludes Back Orders from calculation
              ! Commentd out because it is not neceassry
              !IF (res:Despatched <> 'PEN' AND res:Despatched <> 'CAN' AND res:Despatched <> 'OLD') THEN
              IF (res:Despatched NOT= 'OLD') THEN
                  CASE UseEuro#
                      OF 0
                          TotalCost$  += res:Item_Cost * res:Quantity
                      OF 1
                          TotalCostEuro$ += (res:Item_Cost * res:Quantity) * def:EuroRate
                      OF 2
                          TotalCostEuro$ += (res:Item_Cost * res:Quantity) * inv:EuroExhangeRate
                  END !CASE
             END !IF
          END !LOOP
      
          Access:RETSTOCK.RestoreFile(Save_res2_ID)
      
      END !Loop
      
      Access:RETSALES.RestoreFile(Save_ret_ID)
      
      SETCURSOR()
      
      IF (TotalCostEuro$ <> 0) THEN
          MessageEx('The total value of parts for sales awaiting picking is:'&|
            '<13,10>'&|
            '<13,10>Sterling: '& Format(TotalCost$,@n14.2) &|
            '<13,10>'&|
            '<13,10>Euros: '& Format(TotalCostEuro$,@n14.2)|
            ,'ServiceBase 2000',|
                         'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
      ELSE !TotalCostEuro$ <> 0
          MessageEx('The total value of parts for sales awaiting picking is:'&|
            '<13,10>Sterling: ' & Format(TotalCost$,@n14.2),'ServiceBase 2000',|
                         'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
      END !TotalCostEuro$ <> 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Valuate, Accepted)
    OF ?Print
      ThisWindow.Update
      Report_Type(ret:ref_number)
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'BrowsePickingReconciliation')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?ret:Ref_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Ref_Number, Selected)
      Select(?browse:1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ret:Ref_Number, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
CheckPickList   PROCEDURE()    ! BE 24/02/03
Result          LONG
SaveRes         USHORT
                CODE
                Result = 0
                SaveRes = Access:RETSTOCK.SaveFile()
                Access:RETSTOCK.ClearKey(res:Despatched_Only_Key)
                res:Ref_Number = ret:Ref_Number
                SET(res:Despatched_Only_Key, res:Despatched_Only_Key)
                LOOP
                    IF (Access:RETSTOCK.Next()) THEN
                        BREAK
                    END
                    IF (res:Ref_Number <> ret:Ref_Number) THEN
                        BREAK
                    END
                    IF (res:Despatched = 'PIK') THEN
                        Result = 1
                        BREAK
                    END
                END ! LOOP
                Access:RETSTOCK.RestoreFile(SaveRes)
                RETURN(Result)

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  IF (CheckPickList() = 0) THEN  ! Change 144 BE 25/02/03
      RETURN Record:Filtered
  END
  If ret:Despatched = 'XXX'
      Return Record:Filtered
  End !ret:Despatched = 'XXX'
  BRW1::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
