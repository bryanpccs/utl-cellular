

   MEMBER('sbrdayan.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBRDA002.INC'),ONCE        !Local module procedure declarations
                     END


DailySparesAnalysisCriteria PROCEDURE                 !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
tmp:cumulative       REAL
tmp:BackOrderValue   REAL
tmp:RetailCostTotal  REAL
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:SiteLocation     STRING(30)
tmp:Supplier         STRING(30)
savepath             STRING(255)
save_ret_id          USHORT,AUTO
tmp:SalesTotal       REAL
tmp:BackOrderTotal   REAL
tmp:OldestDate       DATE
tmp:StockValue       REAL
tmp:OldestOrder      DATE
tmp:SalesToDate      REAL
tmp:ExchangesAtCost  REAL
tmp:BackOrderExchanges REAL
save_res_id          USHORT,AUTO
save_sto_id          USHORT,AUTO
save_ord_id          USHORT,AUTO
tmp:Type             STRING('A {19}')
window               WINDOW('Daily Analysis Criteria'),AT(,,228,147),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,220,112),USE(?Sheet1),SPREAD
                         TAB('Daily Spares Analysis Criteria'),USE(?Tab1)
                           PROMPT('Start Date'),AT(8,20),USE(?tmp:StartDate:Prompt)
                           ENTRY(@d6),AT(84,20,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(152,20,10,10),USE(?LookupStartDate),SKIP,FONT('Arial',8,,,CHARSET:ANSI),ICON('Calenda2.ico')
                           PROMPT('End Date'),AT(8,36),USE(?tmp:EndDate:Prompt)
                           ENTRY(@d6),AT(84,36,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(152,36,10,10),USE(?LookupEndDate),SKIP,ICON('Calenda2.ico')
                           PROMPT('Site Location'),AT(8,52),USE(?tmp:SiteLocation:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,52,124,10),USE(tmp:SiteLocation),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Site Location'),TIP('Site Location'),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),ALRT(DownKey),ALRT(TabKey),UPR
                           BUTTON,AT(212,52,10,10),USE(?LookupLocation),SKIP,ICON('List3.ico')
                           PROMPT('Supplier'),AT(8,68),USE(?tmp:Supplier:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,68,124,10),USE(tmp:Supplier),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Supplier'),TIP('Supplier'),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(DownKey),UPR
                           BUTTON,AT(212,68,10,10),USE(?LookupSupplier),SKIP,ICON('List3.ico')
                           OPTION('Analysis Type'),AT(84,84,124,28),USE(tmp:Type),BOXED
                             RADIO('Accessory'),AT(92,96),USE(?Option1:Radio1),VALUE('A')
                             RADIO('Spares'),AT(164,96),USE(?Option1:Radio2),VALUE('S')
                           END
                         END
                       END
                       PANEL,AT(4,120,220,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(108,124,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(164,124,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Shell Execute Variables
AssocFile    CString(255)
Operation    CString(255)
Param        CString(255) 
Dir          CString(255)
!Save Entry Fields Incase Of Lookup
look:tmp:SiteLocation                Like(tmp:SiteLocation)
look:tmp:Supplier                Like(tmp:Supplier)
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?tmp:StartDate:Prompt{prop:FontColor} = -1
    ?tmp:StartDate:Prompt{prop:Color} = 15066597
    If ?tmp:StartDate{prop:ReadOnly} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 15066597
    Elsif ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 8454143
    Else ! If ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 16777215
    End ! If ?tmp:StartDate{prop:Req} = True
    ?tmp:StartDate{prop:Trn} = 0
    ?tmp:StartDate{prop:FontStyle} = font:Bold
    ?tmp:EndDate:Prompt{prop:FontColor} = -1
    ?tmp:EndDate:Prompt{prop:Color} = 15066597
    If ?tmp:EndDate{prop:ReadOnly} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 15066597
    Elsif ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 8454143
    Else ! If ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 16777215
    End ! If ?tmp:EndDate{prop:Req} = True
    ?tmp:EndDate{prop:Trn} = 0
    ?tmp:EndDate{prop:FontStyle} = font:Bold
    ?tmp:SiteLocation:Prompt{prop:FontColor} = -1
    ?tmp:SiteLocation:Prompt{prop:Color} = 15066597
    If ?tmp:SiteLocation{prop:ReadOnly} = True
        ?tmp:SiteLocation{prop:FontColor} = 65793
        ?tmp:SiteLocation{prop:Color} = 15066597
    Elsif ?tmp:SiteLocation{prop:Req} = True
        ?tmp:SiteLocation{prop:FontColor} = 65793
        ?tmp:SiteLocation{prop:Color} = 8454143
    Else ! If ?tmp:SiteLocation{prop:Req} = True
        ?tmp:SiteLocation{prop:FontColor} = 65793
        ?tmp:SiteLocation{prop:Color} = 16777215
    End ! If ?tmp:SiteLocation{prop:Req} = True
    ?tmp:SiteLocation{prop:Trn} = 0
    ?tmp:SiteLocation{prop:FontStyle} = font:Bold
    ?tmp:Supplier:Prompt{prop:FontColor} = -1
    ?tmp:Supplier:Prompt{prop:Color} = 15066597
    If ?tmp:Supplier{prop:ReadOnly} = True
        ?tmp:Supplier{prop:FontColor} = 65793
        ?tmp:Supplier{prop:Color} = 15066597
    Elsif ?tmp:Supplier{prop:Req} = True
        ?tmp:Supplier{prop:FontColor} = 65793
        ?tmp:Supplier{prop:Color} = 8454143
    Else ! If ?tmp:Supplier{prop:Req} = True
        ?tmp:Supplier{prop:FontColor} = 65793
        ?tmp:Supplier{prop:Color} = 16777215
    End ! If ?tmp:Supplier{prop:Req} = True
    ?tmp:Supplier{prop:Trn} = 0
    ?tmp:Supplier{prop:FontStyle} = font:Bold
    ?tmp:Type{prop:Font,3} = -1
    ?tmp:Type{prop:Color} = 15066597
    ?tmp:Type{prop:Trn} = 0
    ?Option1:Radio1{prop:Font,3} = -1
    ?Option1:Radio1{prop:Color} = 15066597
    ?Option1:Radio1{prop:Trn} = 0
    ?Option1:Radio2{prop:Font,3} = -1
    ?Option1:Radio2{prop:Color} = 15066597
    ?Option1:Radio2{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('DailySparesAnalysisCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:StartDate:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOCATION.Open
  Relate:RETSALES.Open
  Access:SUPPLIER.UseFile
  Access:RETSTOCK.UseFile
  Access:STOCK.UseFile
  Access:ORDERS.UseFile
  Access:USERS.UseFile
  Access:SUPVALA.UseFile
  Access:SUPVALB.UseFile
  Access:LOCVALUE.UseFile
  SELF.FilesOpened = True
  error# = 1
  Loop x# = 1 To Len(Clip(Command()))
      IF Sub(Command(),x#,1) = '%'
          glo:Password    = Clip(Sub(Command(),x# + 1,30))
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
              error# = 0
          Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          
      End !IF Sub(Command(),x#,1) = '%'
  End !x# = 1 To Len(Clip(Command())
  If error# = 1
      Halt()
  End !error# = 1
  
  tmp:StartDate = Deformat('1/1/1990',@d6)
  tmp:EndDate     = Today()
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  IF ?tmp:SiteLocation{Prop:Tip} AND ~?LookupLocation{Prop:Tip}
     ?LookupLocation{Prop:Tip} = 'Select ' & ?tmp:SiteLocation{Prop:Tip}
  END
  IF ?tmp:SiteLocation{Prop:Msg} AND ~?LookupLocation{Prop:Msg}
     ?LookupLocation{Prop:Msg} = 'Select ' & ?tmp:SiteLocation{Prop:Msg}
  END
  IF ?tmp:Supplier{Prop:Tip} AND ~?LookupSupplier{Prop:Tip}
     ?LookupSupplier{Prop:Tip} = 'Select ' & ?tmp:Supplier{Prop:Tip}
  END
  IF ?tmp:Supplier{Prop:Msg} AND ~?LookupSupplier{Prop:Msg}
     ?LookupSupplier{Prop:Msg} = 'Select ' & ?tmp:Supplier{Prop:Msg}
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
    Relate:RETSALES.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickSiteLocations
      PickSuppliers
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tmp:SiteLocation
      IF tmp:SiteLocation OR ?tmp:SiteLocation{Prop:Req}
        loc:Location = tmp:SiteLocation
        !Save Lookup Field Incase Of error
        look:tmp:SiteLocation        = tmp:SiteLocation
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:SiteLocation = loc:Location
          ELSE
            !Restore Lookup On Error
            tmp:SiteLocation = look:tmp:SiteLocation
            SELECT(?tmp:SiteLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupLocation
      ThisWindow.Update
      loc:Location = tmp:SiteLocation
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:SiteLocation = loc:Location
          Select(?+1)
      ELSE
          Select(?tmp:SiteLocation)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:SiteLocation)
    OF ?tmp:Supplier
      IF tmp:Supplier OR ?tmp:Supplier{Prop:Req}
        sup:Company_Name = tmp:Supplier
        !Save Lookup Field Incase Of error
        look:tmp:Supplier        = tmp:Supplier
        IF Access:SUPPLIER.TryFetch(sup:Company_Name_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:Supplier = sup:Company_Name
          ELSE
            !Restore Lookup On Error
            tmp:Supplier = look:tmp:Supplier
            SELECT(?tmp:Supplier)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupSupplier
      ThisWindow.Update
      sup:Company_Name = tmp:Supplier
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:Supplier = sup:Company_Name
          Select(?+1)
      ELSE
          Select(?tmp:Supplier)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Supplier)
    OF ?OK
      ThisWindow.Update
      savepath = path()
      glo:file_name = 'DAILYAN.CSV'
      If not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
                  file:keepdir + file:noerror + file:longname)
          !Failed
          setpath(savepath)
      else!If not filedialog
          !Found
          setpath(savepath)
          Remove(EXPGEN)
      
          Access:EXPGEN.Open()
          Access:EXPGEN.Usefile()
      
          recordspercycle         = 25
          recordsprocessed        = 0
          percentprogress         = 0
          progress:thermometer    = 0
          thiswindow.reset(1)
          open(progresswindow)
      
          ?progress:userstring{prop:text} = 'Running...'
          ?progress:pcttext{prop:text} = '0% Completed'
      
          recordstoprocess    = tmp:EndDate - tmp:StartDate
      
      ! Oldest Order From Supplier?!?!
          Setcursor(cursor:wait)
          tmp:OldestOrder = 0
          Save_ord_ID = Access:ORDERS.SaveFile()
      
          If tmp:Supplier <> ''
      
              Access:ORDERS.ClearKey(ord:Supplier_Key)
              ord:Supplier     = tmp:Supplier
              Set(ord:Supplier_Key,ord:Supplier_Key)
          Else!If tmp:Supplier <> ''
              Set(ord:Order_Number_Key)
          End!If tmp:Supplier <> ''
          Loop
              If Access:ORDERS.NEXT()
                 Break
              End !If
              If tmp:Supplier <> ''
                  If ord:Supplier     <> tmp:Supplier      |
                      Then Break.  ! End If
              End !If tmp:Supplier <> ''
              If ord:All_Received <> 'YES'
                  tmp:OldestOrder = ord:Date
                  Break
              End !If ord:All_Received <> 'YES'
          End !Loop
          Access:ORDERS.RestoreFile(Save_ord_ID)
      
      !    tmp:StockValue = 0
      !
      !    Save_sto_ID = Access:STOCK.SaveFile()
      !    If tmp:SiteLocation <> ''
      !        Access:STOCK.ClearKey(sto:Location_Key)
      !        sto:Location    = tmp:SiteLocation
      !        Set(sto:Location_Key,sto:Location_Key)
      !    Else !If tmp:SiteLocation <> ''
      !        Set(sto:Ref_Number_Key)
      !    End !If tmp:SiteLocation <> ''
      !    Loop
      !        If Access:STOCK.NEXT()
      !           Break
      !        End !If
      !        If tmp:SiteLocation <> ''
      !            If sto:Location    <> tmp:SiteLocation      |
      !                Then Break.  ! End If
      !        End !If tmp:SiteLocation <> ''
      !        tmp:StockValue += sto:Retail_Cost * sto:Quantity_stock
      !    End !Loop
      !    Access:STOCK.RestoreFile(Save_sto_ID)
      
          Setcursor()
          Clear(gen:Record)
          Case tmp:Type
              Of 'A'
                  gen:Line1   = 'Date,Retail Sales To Date,Exchanges At Cost To Date,Stock Value,' & |
                                  'Back Order Sales,Back Order Exchanges,Sales Forecast,Exchage Forecast,' & |
                                  'Headcount,Back Order Value At Nokia'
              Of 'S'
                  gen:Line1   = 'Date,Sales,Back Order Value,Turnover To Date,Stock Value,Oldest Back Order,' & |
                                  'Oldest Back Order At Nokia,Headcount,Forecast'
          End !Case tmp:Type
          
          Access:EXPGEN.Insert()
          tmp:cumulative      = 0
          Loop x# = tmp:StartDate To tmp:EndDate
              Do GetNextRecord2
              cancelcheck# += 1
              If cancelcheck# > (RecordsToProcess/100)
                  Do cancelcheck
                  If tmp:cancel = 1
                      Break
                  End!If tmp:cancel = 1
                  cancelcheck# = 0
              End!If cancelcheck# > 50
      
      
              Clear(gen:Record)
              gen:Line1   = Format(x#,@d6)
      
              tmp:SalesToDate = 0
              tmp:SalesTotal      = 0
              tmp:SalesToDate     = 0
              tmp:BackOrderTotal  = 0
              tmp:OldestDate      = 0
              
              tmp:ExchangesAtCost  = 0
      
              Save_ret_ID = Access:RETSALES.SaveFile()
              Access:RETSALES.ClearKey(ret:Date_Booked_Key)
              ret:date_booked = x#
              Set(ret:Date_Booked_Key,ret:Date_Booked_Key)
              Loop
                  If Access:RETSALES.NEXT()
                     Break
                  End !If
                  If ret:date_booked <> x#      |
                      Then Break.  ! End If
      
                  If ret:Invoice_Number = ''
                      Cycle
                  End !If ret:Invoice_Number = ''
      
      
                  tmp:SalesTotal      = 0
                  tmp:SalesToDate     = 0
                  tmp:BackOrderTotal  = 0
                  tmp:OldestDate      = 0
                  !tmp:cumulative      = 0
                  tmp:ExchangesAtCost  = 0
      
                  Save_res_ID = Access:RETSTOCK.SaveFile()
                  Access:RETSTOCK.ClearKey(res:Part_Number_Key)
                  res:Ref_Number  = ret:Ref_Number
                  Set(res:Part_Number_Key,res:Part_Number_Key)
                  Loop
                      If Access:RETSTOCK.NEXT()
                         Break
                      End !If
                      If res:Ref_Number  <> ret:Ref_Number      |
                          Then Break.  ! End If
      
      
      
                      Access:STOCK.Clearkey(sto:Ref_Number_Key)
                      sto:Ref_Number  = res:Part_Ref_Number
                      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          If ret:Payment_Method = 'EXC'
                              If res:Despatched = 'YES'
                                  tmp:ExchangesAtCost += res:Quantity * sto:Purchase_cost
                              Else !If res:Despatched = 'YES'
                                  tmp:BackOrderExchanges  = res:Quantity * sto:Purchase_Cost
                              End !If res:Despatched = 'YES'
                          Else!If ret:Payment_Method = 'EXC'
                          
                              !Found
                              If tmp:Type = 'A' and sto:Accessory <> 'YES'
                                  Cycle
                              End !If tmp:Type = 'A' and sto:Accessory <> 'YES'
                              IF tmp:Type = 'S' and sto:Accessory = 'YES'
                                  Cycle
                              End !IF tmp:Type = 'S' and sto:Accessory = 'YES'
      
                              If res:Despatched = 'YES'
                                  !Non-BackOrder
                                  tmp:SalesTotal += res:Item_Cost * res:Quantity
                              Else !If res:Despathced = 'YES'
                                  tmp:BackOrderTotal += res:Item_cost * res:Quantity
                                  If res:Date_Ordered <> ''
                                      If tmp:OldestDate = 0
                                          tmp:OldestDate  = res:Date_Ordered
                                      End !If tmp:OldestDate = 0
                                      If res:Date_Ordered < tmp:OldestDate
                                          tmp:OldestDate  = res:Date_Ordered
                                      End !If res:Date_Ordered < tmp:OldestDate
                                  End !If res:Date_Ordered <> ''
                              End !If res:Despathced = 'YES'
                          End !If ret:Payment_Method
                      Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      If ret:Payment_Method <> 'EXC'
                          tmp:SalesToDate += ret:Sub_Total
                      End !If ret:Payment_Method = 'EXC'
      
                      
                  End !Loop
                  Access:RETSTOCK.RestoreFile(Save_res_ID)
      
      
              End !Loop
              Access:RETSALES.RestoreFile(Save_ret_ID)
      
              Case tmp:Type
                  Of 'A'
                      gen:Line1   = Clip(gen:Line1) & ',' & Round(tmp:SalesToDate,.01)
                      gen:Line1   = Clip(gen:Line1) & ',' & Round(tmp:ExchangesAtCost,.01)
                      IF tmp:SiteLocation <> ''
                        Access:LOCVALUE.ClearKey(lov:DateKey)
                        lov:Location = tmp:SiteLocation
                        lov:TheDate  = x#
                        If Access:LOCVALUE.TryFetch(lov:DateKey) = Level:Benign
                            !Found
                            gen:Line1   = Clip(gen:Line1) & ',' & Round(lov:RetailCostTotal,.01)
                        Else!If Access:LOCVALUE.TryFetch(lov:DateKey) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                            gen:Line1   = Clip(gen:Line1) & ','
                        End!If Access:LOCVALUE.TryFetch(lov:DateKey) = Level:Benign
                      ELSE
                        tmp:RetailCostTotal = 0
                        Access:LOCVALUE.ClearKey(lov:DateOnly)
                        lov:TheDate  = x#
                        SET(lov:DateOnly,lov:DateOnly)
                        LOOP
                          IF Access:LocValue.Next()
                            BREAK
                          END
                          IF lov:TheDate <> x#
                            BREAK
                          END
                          !Found
                          tmp:RetailCostTotal += lov:RetailCostTotal
                        End!If Access:LOCVALUE.TryFetch(lov:DateKey) = Level:Benign
                        If  tmp:RetailCostTotal = 0
                            !Found
                            gen:Line1   = Clip(gen:Line1) & ','
                        Else!If Access:LOCVALUE.TryFetch(lov:DateKey) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                            gen:Line1   = Clip(gen:Line1) & ',' & Round(tmp:RetailCostTotal,.01)
                        End!If Access:LOCVALUE.TryFetch(lov:DateKey) = Level:Benign
                      END
      
                      gen:Line1   = Clip(gen:Line1) & ',' & Round(tmp:BackOrderTotal,.01)
                      gen:Line1   = Clip(gen:Line1) & ',' & Round(tmp:BackOrderExchanges,.01)
                      gen:Line1   = Clip(gen:Line1) & ','
                      gen:Line1   = Clip(gen:Line1) & ','
                      gen:Line1   = Clip(gen:Line1) & ','
      
                      IF tmp:Supplier <> '' AND tmp:SiteLocation <> ''
                        Access:SUPVALB.ClearKey(suvb:LocationKey)
                        suvb:Supplier = tmp:Supplier
                        suvb:RunDate  = x#
                        suvb:Location = tmp:SiteLocation
                        If Access:SUPVALB.TryFetch(suvb:LocationKey) = Level:Benign
                            !Found
                            gen:Line1   = Clip(gen:Line1) & ',' & Round(suvb:BackOrderValue,.01)
                        Else!If Access:SUPVALB.TryFetch(suvb:LocationKey) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                            gen:Line1   = Clip(gen:Line1) & ','
                        End!If Access:SUPVALB.TryFetch(suvb:LocationKey) = Level:Benign
                      ELSE
                        tmp:BackOrderValue = 0
                        Access:SUPVALB.ClearKey(suvb:DateOnly)
                        suvb:RunDate  = x#
                        SET(suvb:DateOnly,suvb:DateOnly)
                        LOOP
                          IF Access:SupValb.Next()
                            BREAK
                          END
                          IF suvb:RunDate <> x#
                            BREAK
                          END
                          IF tmp:SiteLocation <> ''
                            IF suvb:Location <> tmp:SiteLocation
                              CYCLE
                            END
                          END
                          tmp:BackOrderValue += suvb:BackOrderValue
                        END
                        If tmp:BackOrderValue = 0
                            !Found
                            gen:Line1   = Clip(gen:Line1) & ','
                        Else!If Access:SUPVALB.TryFetch(suvb:LocationKey) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                            gen:Line1   = Clip(gen:Line1) & ',' & Round(tmp:BackOrderValue,.01)
                        End!If Access:SUPVALB.TryFetch(suvb:LocationKey) = Level:Benign
                      END
      
                  Of 'S'
                      tmp:cumulative += tmp:SalesTotal + tmp:BackOrderTotal
                      gen:Line1   = Clip(gen:Line1) & ',' & Round(tmp:SalesTotal,.01)
                      gen:Line1   = Clip(gen:Line1) & ',' & Round(tmp:BackOrderTotal,.01)
                      gen:Line1   = Clip(gen:Line1) & ',' & Round(tmp:cumulative,.01)
      
                      IF tmp:SiteLocation <> ''
                        Access:LOCVALUE.ClearKey(lov:DateKey)
                        lov:Location = tmp:SiteLocation
                        lov:TheDate  = x#
                        If Access:LOCVALUE.TryFetch(lov:DateKey) = Level:Benign
                            !Found
                            gen:Line1   = Clip(gen:Line1) & ',' & Round(lov:RetailCostTotal,.01)
                        Else!If Access:LOCVALUE.TryFetch(lov:DateKey) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                            gen:Line1   = Clip(gen:Line1) & ','
                        End!If Access:LOCVALUE.TryFetch(lov:DateKey) = Level:Benign
                      ELSE
                        tmp:RetailCostTotal = 0
                        Access:LOCVALUE.ClearKey(lov:DateOnly)
                        lov:TheDate  = x#
                        SET(lov:DateOnly,lov:DateOnly)
                        LOOP
                          IF Access:LocValue.Next()
                            BREAK
                          END
                          IF lov:TheDate <> x#
                            BREAK
                          END
                          !Found
                          tmp:RetailCostTotal += lov:RetailCostTotal
                        End!If Access:LOCVALUE.TryFetch(lov:DateKey) = Level:Benign
                        If  tmp:RetailCostTotal = 0
                            !Found
                            gen:Line1   = Clip(gen:Line1) & ','
                        Else!If Access:LOCVALUE.TryFetch(lov:DateKey) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                            gen:Line1   = Clip(gen:Line1) & ',' & Round(tmp:RetailCostTotal,.01)
                        End!If Access:LOCVALUE.TryFetch(lov:DateKey) = Level:Benign
                      END
      
                      IF tmp:Supplier <> ''
                        Access:SUPVALA.ClearKey(suva:RunDateKey)
                        suva:Supplier = tmp:Supplier
                        suva:RunDate  = x#
                        If Access:SUPVALA.TryFetch(suva:RunDateKey) = Level:Benign
                            !Found
                            gen:Line1   = Clip(gen:Line1) & ',' & Format(suva:OldBackOrder,@d6)
                            gen:Line1   = Clip(gen:Line1) & ',' & Format(suva:OldOutOrder,@d6)
                        Else!If Access:SUPVALA.TryFetch(suva:RunDateKey) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                            gen:Line1   = Clip(gen:Line1) & ','
                            gen:Line1   = Clip(gen:Line1) & ','
                        End!If Access:SUPVALA.TryFetch(suva:RunDateKey) = Level:Benign
                      ELSE
                        Access:SUPVALA.ClearKey(suva:DateOnly)
                        suva:RunDate  = x#
                        If Access:SUPVALA.TryFetch(suva:DateOnly) = Level:Benign
                            !Found
                            gen:Line1   = Clip(gen:Line1) & ',' & Format(suva:OldBackOrder,@d6)
                            gen:Line1   = Clip(gen:Line1) & ',NO SUPPLIER SPECIFIED'
                        ELSE
                            gen:Line1   = Clip(gen:Line1) & ',' & Format(tmp:OldestOrder,@d6)
                            gen:Line1   = Clip(gen:Line1) & ',NO SUPPLIER SPECIFIED'
                        END
                      END
                      gen:Line1   = Clip(gen:Line1) & ',,'
      
              End !Case tmp:Type
              Access:EXPGEN.Insert()
      
          End !Loop x# = tmp:StartDate To tmp:EndDate
      
          Do EndPrintRun
          close(progresswindow)
          Access:EXPGEN.Close()
          Case MessageEx('Export Completed.','ServiceBase 2000',|
                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          AssocFile = Clip(glo:File_Name)
          Param = ''
          Dir = ''
          Operation = 'Open'
          ShellExecute(GetDesktopWindow(), Operation, AssocFile, Param, Dir, 5)
      End!If not filedialog
      
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?tmp:SiteLocation
    CASE EVENT()
    OF EVENT:AlertKey
      If Keycode() = MouseRight
         Execute Popup('Lookup Location')
             Post(Event:Accepted,?LookupLocation)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupLocation)
      End!If Keycode() = MouseRight
    END
  OF ?tmp:Supplier
    CASE EVENT()
    OF EVENT:AlertKey
      If Keycode() = MouseRight
         Execute Popup('Lookup Supplier')
             Post(Event:Accepted,?LookupSupplier)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupSupplier)
      End!If Keycode() = MouseRight
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

