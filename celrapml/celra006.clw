

   MEMBER('celrapml.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_fwiz.inc'),ONCE

                     MAP
                       INCLUDE('CELRA006.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CELRA005.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('CELRA007.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('CELRA008.INC'),ONCE        !Req'd for module callout resolution
                     END


Multiple_Job_Wizard PROCEDURE                         !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
FilesOpened          BYTE
ValidateMobileNumber BYTE(0)
temp_255string       STRING(255)
tmp:do_next          BYTE(0)
force_fault_codes_temp STRING('NO {1}')
error_temp           BYTE(0)
number_jobs_temp     REAL
Account_Number_temp  STRING(15)
Transit_Type_temp    STRING(30)
Address_Collection_Group GROUP,PRE()
save_par_id          USHORT,AUTO
date_error_temp      BYTE(0)
save_job_id          USHORT,AUTO
check_for_bouncers_temp BYTE(0)
saved_ref_number_temp REAL
saved_esn_temp       STRING(16)
saved_msn_temp       STRING(16)
time_completed_temp  TIME
Postcode_Collection  STRING(10)
save_wpr_id          USHORT,AUTO
Company_Name_Collection STRING(30)
save_maf_id          USHORT,AUTO
Address_Line1_Collection STRING(30)
Address_Line2_Collection STRING(30)
Address_Line3_Collection STRING(30)
Telephone_Collection STRING(15)
                     END
Address_Group        GROUP,PRE()
Postcode             STRING(10)
Company_Name         STRING(30)
Address_Line1        STRING(30)
Address_Line2        STRING(30)
Address_Line3        STRING(30)
Telephone_Number     STRING(15)
Fax_Number           STRING(15)
                     END
Address_Delivery_Group GROUP,PRE()
Postcode_Delivery    STRING(10)
Company_Name_Delivery STRING(30)
Address_Line1_Delivery STRING(30)
Address_Line2_Delivery STRING(30)
Address_Line3_Delivery STRING(30)
Telephone_Delivery   STRING(15)
                     END
TabNumber            BYTE(1)
MaxTabs              BYTE(6)
Order_Number_Tick_Temp STRING(1)
Order_Number_Temp    STRING(30)
Internal_Location_Tick_Temp STRING(1)
Internal_Location_Temp STRING(30)
Mobile_Number_temp   STRING(15)
Mobile_Number_Tick_Temp STRING(1)
Model_Number_temp    STRING(30)
model_number_tick_temp STRING(1)
Unit_Type_temp       STRING(30)
unit_type_tick_temp  STRING(1)
ESN_temp             STRING(30)
esn_tick_temp        STRING(1)
MSN_temp             STRING(30)
msn_tick_temp        STRING(1)
Fault_Description_temp STRING(10000)
fault_description_tick_temp STRING(1)
Intermittent_Fault_temp STRING(3)
intermittent_fault_tick_temp STRING(1)
Charge_Type_temp     STRING(30)
charge_type_tick_temp STRING(1)
Repair_Type_temp     STRING(30)
repair_type_tick_temp STRING(1)
Physical_Damage_temp STRING(3)
physical_damage_tick_temp STRING(1)
manufacturer_temp    STRING(30)
number_temp          REAL(1)
use_batch_temp       STRING('3')
batch_number_temp    REAL
auto_complete_temp   STRING(1)
date_completed_temp  DATE
auto_despatch_temp   STRING(1)
courier_temp         STRING(30)
Title_temp           STRING(4)
initial_temp         STRING(1)
surname_temp         STRING(30)
Print_Job_Receipt_Temp STRING('N')
Print_Job_Card_Temp  STRING('N')
Print_Despatch_Notes_Temp STRING('N')
warranty_job_tick_temp STRING(3)
warranty_charge_type_temp STRING(30)
print_job_label_temp STRING('YES')
repair_type_warranty_tick_temp STRING(1)
repair_type_warranty_temp STRING(30)
accessories_temp     STRING(30)
Colour_Temp          STRING(30)
colour_tick_temp     STRING('NO {1}')
tmp:DOPTick          BYTE(0)
tmp:dop              DATE
save_jac_id          USHORT,AUTO
tmp:JobCreated       BYTE(0)
tmp:ProductCode      STRING(30)
print_bookin_label_temp BYTE(0)
Consignment_Number_Tick_Temp BYTE
Consignment_Date_Tick_Temp BYTE
Consignment_Number_Temp STRING(30)
Consignment_Date_Temp DATE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Multiple Job Booking Wizard'),AT(,,455,358),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),SYSTEM,GRAY,MAX,DOUBLE,MDI
                       SHEET,AT(4,4,448,324),USE(?Sheet1),BELOW,SPREAD
                         TAB('1'),USE(?Tab1)
                           PROMPT('Common Fields - Unit Details'),AT(88,84),USE(?Prompt2),FONT('Tahoma',12,,FONT:bold)
                           PROMPT('Common Fields - Apply Default'),AT(88,28),USE(?Prompt2:2),FONT('Tahoma',12,,FONT:bold)
                           PROMPT('Select and fill-in in the fields that you required to be common to all the jobs ' &|
   'you wish to book in'),AT(88,100,240,20),FONT(,8,,)
                           PROMPT('Select "Apply Default" to assign common fields'),AT(88,40,240,8),USE(?Prompt47),FONT(,8,,)
                           CHECK('Order Number'),AT(92,124),USE(Order_Number_Tick_Temp),VALUE('Y','N')
                           ENTRY(@s30),AT(204,124,124,10),USE(Order_Number_Temp),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           CHECK('Internal Location'),AT(92,140),USE(Internal_Location_Tick_Temp),VALUE('Y','N')
                           ENTRY(@s30),AT(204,140,124,10),USE(Internal_Location_Temp),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(MouseLeft2),ALRT(EnterKey),UPR
                           BUTTON,AT(332,140,10,10),USE(?Lookup_Location),SKIP,ICON('list3.ico')
                           CHECK('Model Number'),AT(92,156),USE(model_number_tick_temp),VALUE('Y','N')
                           ENTRY(@s30),AT(204,156,124,10),USE(Model_Number_temp),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(332,156,10,10),USE(?lookup_Model_Number:3),SKIP,ICON('list3.ico')
                           CHECK('Colour'),AT(92,172),USE(colour_tick_temp),HIDE,VALUE('Y','N')
                           ENTRY(@s30),AT(204,172,124,10),USE(Colour_Temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(332,172,10,10),USE(?Lookup_Colour),SKIP,ICON('list3.ico')
                           CHECK('Unit Type'),AT(92,188),USE(unit_type_tick_temp),VALUE('Y','N')
                           ENTRY(@s30),AT(204,188,124,10),USE(Unit_Type_temp),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(332,188,10,10),USE(?Lookup_Unit_Type),SKIP,ICON('list3.ico')
                           CHECK('I.M.E.I. Number'),AT(92,204),USE(esn_tick_temp),VALUE('Y','N')
                           ENTRY(@s16),AT(204,204,124,10),USE(ESN_temp),HIDE,FONT(,8,,FONT:bold),REQ,UPR
                           CHECK('Fault Description'),AT(92,220),USE(fault_description_tick_temp),VALUE('Y','N')
                           BUTTON,AT(332,220,10,10),USE(?Fault_Description_Text),HIDE,ICON('list3.ico')
                           TEXT,AT(204,220,124,20),USE(Fault_Description_temp),HIDE,VSCROLL,FONT(,8,,FONT:bold),ALRT(MouseLeft2),ALRT(EnterKey),REQ,UPR
                           CHECK('D.O.P'),AT(92,248),USE(tmp:DOPTick),RIGHT,VALUE('1','0')
                           ENTRY(@d6),AT(204,248,64,10),USE(tmp:dop),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           CHECK('Incoming Consignment Number'),AT(92,264),USE(Consignment_Number_Tick_Temp),VALUE('1','0')
                           ENTRY(@s30),AT(204,264,124,10),USE(Consignment_Number_Temp),FONT(,8,,FONT:bold)
                           CHECK('Incoming Consignment Date'),AT(92,280),USE(Consignment_Date_Tick_Temp),VALUE('1','0')
                           BUTTON('...'),AT(332,276,10,10),USE(?PopConsignmentDate),LEFT,ICON('calenda2.ico')
                           ENTRY(@d6b),AT(204,276,124,10),USE(Consignment_Date_Temp),FONT(,8,,FONT:bold)
                           BUTTON('Apply Default'),AT(88,52,76,20),USE(?ApplyDefault),LEFT,ICON('process.gif')
                         END
                         TAB('2'),USE(?Tab6)
                           PROMPT('Common Fields - Charge Details'),AT(92,32),USE(?Prompt40),FONT('Tahoma',12,,FONT:bold)
                           PROMPT('Select and fill-in in the fields that you required to be common to all the jobs ' &|
   'you wish to book in'),AT(92,48,240,36),USE(?Prompt41),FONT(,8,,)
                           CHECK('Chargeable Charge Type'),AT(92,100),USE(charge_type_tick_temp),VALUE('Y','N')
                           BUTTON,AT(188,100,10,10),USE(?Lookup_Charge_Type),HIDE,ICON('list3.ico')
                           ENTRY(@s30),AT(204,100,124,10),USE(Charge_Type_temp),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           CHECK('Chargeable Repair Type'),AT(92,116,88,12),USE(repair_type_tick_temp),VALUE('Y','N')
                           BUTTON,AT(188,116,10,10),USE(?Lookup_Repair_Type),HIDE,ICON('list3.ico')
                           ENTRY(@s30),AT(204,116,124,10),USE(Repair_Type_temp),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           CHECK('Warranty Charge Type'),AT(92,132),USE(warranty_job_tick_temp),VALUE('Y','N')
                           BUTTON,AT(188,132,10,10),USE(?lookup_warranty_charge_type),HIDE,ICON('list3.ico')
                           ENTRY(@s30),AT(204,132,124,10),USE(warranty_charge_type_temp),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           CHECK('Warranty Repair Type'),AT(92,148),USE(repair_type_warranty_tick_temp),HIDE,VALUE('Y','N')
                           BUTTON,AT(188,148,10,10),USE(?Lookup_Repair_Type_Warranty),HIDE,ICON('list3.ico')
                           ENTRY(@s30),AT(204,148,124,10),USE(repair_type_warranty_temp),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           CHECK('Auto Complete'),AT(92,164),USE(auto_complete_temp),VALUE('Y','N')
                           ENTRY(@D6b),AT(204,164,64,10),USE(date_completed_temp),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold),ALRT(AltD),REQ,UPR
                           BUTTON,AT(268,164,10,10),USE(?PopCalendar),SKIP,HIDE,ICON('Calenda2.ico')
                           CHECK('Auto Despatch'),AT(92,180),USE(auto_despatch_temp),HIDE,VALUE('Y','N')
                           BUTTON,AT(188,180,10,10),USE(?Lookup_Courier),HIDE,ICON('list3.ico')
                           ENTRY(@s30),AT(204,180,124,10),USE(courier_temp),HIDE,FONT('Tahoma',,,FONT:bold,CHARSET:ANSI),UPR
                         END
                         TAB('3'),USE(?Tab2)
                           PROMPT('Common Fields - Account Details'),AT(88,28),USE(?Prompt4),FONT('Tahoma',12,,FONT:bold)
                           PROMPT('Trade Account No'),AT(92,48),USE(?Prompt23)
                           ENTRY(@s15),AT(168,48,64,10),USE(Account_Number_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(MouseLeft2),ALRT(EnterKey),REQ,UPR
                           BUTTON,AT(236,48,10,10),USE(?Lookup_Account),SKIP,ICON('list3.ico')
                           PROMPT('Transit Type'),AT(92,64),USE(?Prompt24)
                           ENTRY(@s30),AT(168,64,124,10),USE(Transit_Type_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(MouseLeft2),ALRT(EnterKey),REQ,UPR
                           BUTTON,AT(296,64,10,10),USE(?Lookup_Transit_Type),SKIP,ICON('list3.ico')
                           GROUP,AT(84,72,220,28),USE(?name_group),HIDE
                             PROMPT('Customer Name'),AT(92,84,72,8),USE(?Title_temp:Prompt),TRN
                             PROMPT('Title'),AT(168,76),USE(?Title_temp:Prompt:2),TRN,FONT(,7,,)
                             PROMPT('Initial'),AT(200,76),USE(?Title_temp:Prompt:3),TRN,FONT(,7,,)
                             PROMPT('Surname'),AT(216,76),USE(?Title_temp:Prompt:4),TRN,FONT(,7,,)
                             ENTRY(@s4),AT(168,84,28,10),USE(Title_temp),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                             ENTRY(@s1),AT(200,84,12,10),USE(initial_temp),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                             ENTRY(@s30),AT(216,84,76,10),USE(surname_temp),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           END
                           PROMPT('Customer Address'),AT(92,104),USE(?Prompt20),FONT('Tahoma',10,,FONT:bold)
                           PROMPT('Company Name'),AT(92,116),USE(?Company_Name:Prompt),TRN
                           ENTRY(@s30),AT(168,116,124,10),USE(Company_Name),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Postcode'),AT(92,132),USE(?Postcode:Prompt),TRN
                           ENTRY(@s10),AT(168,132,64,10),USE(Postcode),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Address'),AT(92,144),USE(?Address_Line1:Prompt),TRN
                           ENTRY(@s30),AT(168,144,124,10),USE(Address_Line1),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(168,156,124,10),USE(Address_Line2),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(168,168,124,10),USE(Address_Line3),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Telephone Number'),AT(92,184),USE(?Telephone_Number:Prompt),TRN
                           ENTRY(@s15),AT(168,184,124,10),USE(Telephone_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fax Number'),AT(92,196),USE(?Fax_Number:Prompt),TRN
                           ENTRY(@s15),AT(168,196,124,10),USE(Fax_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           GROUP,AT(88,216,212,96),USE(?Delivery_address_Group),HIDE
                             PROMPT('Delivery Address'),AT(168,216),USE(?Prompt22),FONT('Tahoma',10,,FONT:bold)
                             PROMPT('Company Name'),AT(92,232),USE(?Company_Name:Prompt:2),TRN
                             ENTRY(@s30),AT(168,232,124,10),USE(Company_Name_Delivery),FONT('Tahoma',8,,FONT:bold),UPR
                             PROMPT('Postcode'),AT(92,248),USE(?Postcode:Prompt:2),TRN
                             ENTRY(@s10),AT(168,248,64,10),USE(Postcode_Delivery),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                             BUTTON('Clear/Copy'),AT(236,248,56,10),USE(?Delivery_Button),LEFT,ICON(ICON:Cut)
                             PROMPT('Address'),AT(92,260),USE(?Address_Line1:Prompt:2),TRN
                             ENTRY(@s30),AT(168,260,124,10),USE(Address_Line1_Delivery),FONT('Tahoma',8,,FONT:bold),UPR
                             ENTRY(@s30),AT(168,272,124,10),USE(Address_Line2_Delivery),FONT('Tahoma',8,,FONT:bold),UPR
                             ENTRY(@s30),AT(168,284,124,10),USE(Address_Line3_Delivery),FONT('Tahoma',8,,FONT:bold),UPR
                             PROMPT('Telephone Number'),AT(92,300),USE(?Telephone_Number:Prompt:2),TRN
                             ENTRY(@s15),AT(168,300,124,10),USE(Telephone_Delivery),FONT('Tahoma',8,,FONT:bold),UPR
                           END
                           GROUP,AT(300,96,148,104),USE(?collection_address_group),HIDE
                             PROMPT('Collection Address'),AT(320,104),USE(?Prompt21),FONT('Tahoma',10,,FONT:bold)
                             ENTRY(@s30),AT(320,116,124,10),USE(Company_Name_Collection),FONT('Tahoma',8,,FONT:bold),UPR
                             ENTRY(@s10),AT(320,132,64,10),USE(Postcode_Collection),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                             BUTTON('Clear/Copy'),AT(388,132,56,10),USE(?Collection_Clear),LEFT,ICON(ICON:Cut)
                             ENTRY(@s30),AT(320,144,124,10),USE(Address_Line1_Collection),FONT('Tahoma',8,,FONT:bold),UPR
                             ENTRY(@s30),AT(320,156,124,10),USE(Address_Line2_Collection),FONT('Tahoma',8,,FONT:bold),UPR
                             ENTRY(@s30),AT(320,168,124,10),USE(Address_Line3_Collection),FONT('Tahoma',8,,FONT:bold),UPR
                             ENTRY(@s15),AT(320,184,124,10),USE(Telephone_Collection),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           END
                         END
                         TAB('4'),USE(?Tab5)
                           PROMPT('Printing Options'),AT(88,28),USE(?Prompt37),FONT(,12,,FONT:bold)
                           PROMPT('Select which paperwork you wish to print for each job'),AT(88,44),USE(?Prompt38),FONT(,8,,)
                           CHECK('Print Booking In Label'),AT(212,100),USE(print_bookin_label_temp),VALUE('1','0')
                           CHECK('Print Job Label'),AT(212,115),USE(print_job_label_temp),VALUE('YES','NO')
                           CHECK('Print Job Card'),AT(212,130),USE(Print_Job_Card_Temp),VALUE('Y','N')
                           CHECK('Print Job Receipt'),AT(212,145),USE(Print_Job_Receipt_Temp),VALUE('Y','N')
                           CHECK('Print Despatch Note'),AT(212,160),USE(Print_Despatch_Notes_Temp),VALUE('Y','N')
                         END
                         TAB('5'),USE(?Tab4)
                           PROMPT('Creating A Batch'),AT(84,28),USE(?Prompt33),FONT('Tahoma',12,,FONT:bold)
                           PROMPT('The Batch Procedure should only be used if the unit''s in the batch require the s' &|
   'ame information during updates.'),AT(84,44,372,24),USE(?Prompt34),FONT(,10,,)
                           PROMPT('i.e. if they are all despatched on the same day or require the same status chang' &|
   'e.'),AT(84,72,372,16),FONT(,10,,)
                           OPTION('Do you wish to: '),AT(188,104,133,88),USE(use_batch_temp),BOXED
                             RADIO('Create A New Batch'),AT(212,124),USE(?Option5:Radio1),VALUE('1')
                             RADIO('Add To Existing Batch'),AT(212,144),USE(?Option5:Radio2),VALUE('2')
                             RADIO('Do Not Create Batch'),AT(212,164),USE(?Option5:Radio3),VALUE('3')
                           END
                           PROMPT('Batch Message'),AT(128,196,244,12),USE(?Batch_Message),HIDE,CENTER,FONT(,10,,)
                           ENTRY(@p<<<<<<#pb),AT(192,212,124,10),USE(batch_number_temp),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           STRING('Click ''Next'' when you are ready to begin creating jobs:'),AT(140,236),USE(?String2),FONT(,10,,FONT:bold)
                         END
                         TAB('6'),USE(?Tab6:2),HIDE
                           PROMPT('Finishing - Create Jobs'),AT(8,28),USE(?Prompt42),FONT('Tahoma',12,,FONT:bold)
                           GROUP,AT(92,40,276,280),USE(?Fields_Group)
                             PROMPT('I.M.E.I. Number'),AT(124,48),USE(?ESN_temp:Prompt),TRN
                             ENTRY(@s18),AT(236,48,124,10),USE(ESN_temp,,?ESN_temp:2),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                             PROMPT(''),AT(364,48,75,11),USE(?BouncerText),FONT(,10,COLOR:Red,FONT:bold)
                             PROMPT('M.S.N.'),AT(124,64),USE(?msn:prompt),TRN
                             ENTRY(@s16),AT(236,64,124,10),USE(MSN_temp),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                             PROMPT('Product Code'),AT(123,80),USE(?tmp:ProductCode:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             BUTTON,AT(220,80,10,10),USE(?LookupProductCode),SKIP,ICON('List3.ico')
                             ENTRY(@s30),AT(236,80,124,10),USE(tmp:ProductCode),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Product Code'),TIP('Product Code'),UPR
                             PROMPT('Model Number'),AT(124,96),USE(?Prompt26)
                             BUTTON,AT(220,96,10,10),USE(?Lookup_Model_Number:2),ICON('list3.ico')
                             ENTRY(@s30),AT(236,96,124,10),USE(Model_Number_temp,,?Model_Number_temp:2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Unit Type'),AT(124,112),USE(?Prompt27)
                             BUTTON,AT(220,112,10,10),USE(?Lookup_unit_Type:2),ICON('list3.ico')
                             ENTRY(@s30),AT(236,112,124,10),USE(Unit_Type_temp,,?Unit_Type_temp:2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Accessories'),AT(124,128),USE(?Prompt43)
                             BUTTON,AT(220,128,10,10),USE(?Lookup_Accessories),ICON('list3.ico')
                             ENTRY(@s30),AT(236,128,124,10),USE(accessories_temp),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                             PROMPT('Order Number'),AT(124,144),USE(?Order_Number_Temp:Prompt),TRN
                             ENTRY(@s30),AT(236,144,124,10),USE(Order_Number_Temp,,?Order_Number_Temp:2),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                             PROMPT('Internal Location'),AT(124,160),USE(?Prompt36)
                             BUTTON,AT(220,160,10,10),USE(?Lookup_Internal_Location:2),ICON('list3.ico')
                             ENTRY(@s30),AT(236,160,124,10),USE(Internal_Location_Temp,,?Internal_Location_Temp:2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Fault Description'),AT(124,176),USE(?Prompt25)
                             BUTTON,AT(220,176,10,10),USE(?fault_description_text_2),ICON('list3.ico')
                             TEXT,AT(236,176,124,20),USE(Fault_Description_temp,,?Fault_Description_temp:2),VSCROLL,FONT('Tahoma',8,,FONT:bold),UPR
                             PROMPT('Mobile Number'),AT(124,200),USE(?Mobile_Number:Prompt),TRN
                             ENTRY(@s15),AT(236,200,124,10),USE(Mobile_Number_temp,,?Mobile_Number_temp:2),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                             PROMPT('D.O.P.'),AT(124,216),USE(?tmp:dop:Prompt)
                             ENTRY(@d6),AT(236,216,64,10),USE(tmp:dop,,?tmp:dop:2),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Chargeable Charge Type'),AT(124,232,80,12),USE(?Prompt28)
                             BUTTON,AT(220,232,10,10),USE(?Lookup_Charge_Type:2),ICON('list3.ico')
                             ENTRY(@s30),AT(236,232,124,10),USE(Charge_Type_temp,,?Charge_Type_temp:2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Chargeable Repair Type'),AT(124,248),USE(?Prompt31)
                             BUTTON,AT(220,248,10,10),USE(?Lookup_Repair_Type:2),ICON('list3.ico')
                             ENTRY(@s30),AT(236,248,124,10),USE(Repair_Type_temp,,?Repair_Type_temp:2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Warranty Charge Type'),AT(124,264),USE(?Prompt35)
                             BUTTON,AT(220,264,10,10),USE(?Lookup_Warranty_Charge_Type:2),LEFT,ICON('list3.ico')
                             ENTRY(@s30),AT(236,264,124,10),USE(warranty_charge_type_temp,,?warranty_charge_type_temp:2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Warranty Repair Type'),AT(124,280),USE(?repair_type_warranty_temp:Prompt),TRN
                             BUTTON,AT(220,280,10,10),USE(?Lookup_Warranty_Repair_Type),LEFT,ICON('list3.ico')
                             ENTRY(@s30),AT(236,280,124,10),USE(repair_type_warranty_temp,,?repair_type_warranty_temp:2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             BUTTON('Fault Codes'),AT(176,336,56,16),USE(?Fault_Codes),LEFT,ICON('history.gif')
                             BUTTON('&Save Job And Insert Another'),AT(92,300,88,20),USE(?save_Button),LEFT,ICON('next.gif')
                             BUTTON('Insert Multi&ple Jobs'),AT(184,300,88,20),USE(?Insert_Multiple),LEFT,ICON('insert.gif')
                             BUTTON('Save Job And  &Finish'),AT(276,300,88,20),USE(?Save_Finish_Button),LEFT,ICON('cancel.gif')
                           END
                         END
                       END
                       PROMPT('Multiple Job Booking Wizard'),AT(8,8),USE(?Prompt1),FONT(,14,COLOR:Navy,FONT:bold)
                       IMAGE('wizard.gif'),AT(8,116),USE(?Image1)
                       PANEL,AT(4,332,448,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Back'),AT(8,336,56,16),USE(?VSBackButton),LEFT,ICON(ICON:VCRrewind)
                       BUTTON('&Next'),AT(64,336,56,16),USE(?VSNextButton),LEFT,ICON(ICON:VCRfastforward)
                       BUTTON('Finish'),AT(120,336,56,16),USE(?Finish),LEFT,ICON('thumbs.gif')
                       BUTTON('Close'),AT(392,336,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
Wizard4         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_jactmp_id   ushort,auto
!Save Entry Fields Incase Of Lookup
look:Internal_Location_Temp                Like(Internal_Location_Temp)
look:Model_Number_temp                Like(Model_Number_temp)
look:Colour_Temp                Like(Colour_Temp)
look:Unit_Type_temp                Like(Unit_Type_temp)
look:Account_Number_temp                Like(Account_Number_temp)
look:Transit_Type_temp                Like(Transit_Type_temp)
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?Prompt2:2{prop:FontColor} = -1
    ?Prompt2:2{prop:Color} = 15066597
    ?Prompt47{prop:FontColor} = -1
    ?Prompt47{prop:Color} = 15066597
    ?Order_Number_Tick_Temp{prop:Font,3} = -1
    ?Order_Number_Tick_Temp{prop:Color} = 15066597
    ?Order_Number_Tick_Temp{prop:Trn} = 0
    If ?Order_Number_Temp{prop:ReadOnly} = True
        ?Order_Number_Temp{prop:FontColor} = 65793
        ?Order_Number_Temp{prop:Color} = 15066597
    Elsif ?Order_Number_Temp{prop:Req} = True
        ?Order_Number_Temp{prop:FontColor} = 65793
        ?Order_Number_Temp{prop:Color} = 8454143
    Else ! If ?Order_Number_Temp{prop:Req} = True
        ?Order_Number_Temp{prop:FontColor} = 65793
        ?Order_Number_Temp{prop:Color} = 16777215
    End ! If ?Order_Number_Temp{prop:Req} = True
    ?Order_Number_Temp{prop:Trn} = 0
    ?Order_Number_Temp{prop:FontStyle} = font:Bold
    ?Internal_Location_Tick_Temp{prop:Font,3} = -1
    ?Internal_Location_Tick_Temp{prop:Color} = 15066597
    ?Internal_Location_Tick_Temp{prop:Trn} = 0
    If ?Internal_Location_Temp{prop:ReadOnly} = True
        ?Internal_Location_Temp{prop:FontColor} = 65793
        ?Internal_Location_Temp{prop:Color} = 15066597
    Elsif ?Internal_Location_Temp{prop:Req} = True
        ?Internal_Location_Temp{prop:FontColor} = 65793
        ?Internal_Location_Temp{prop:Color} = 8454143
    Else ! If ?Internal_Location_Temp{prop:Req} = True
        ?Internal_Location_Temp{prop:FontColor} = 65793
        ?Internal_Location_Temp{prop:Color} = 16777215
    End ! If ?Internal_Location_Temp{prop:Req} = True
    ?Internal_Location_Temp{prop:Trn} = 0
    ?Internal_Location_Temp{prop:FontStyle} = font:Bold
    ?model_number_tick_temp{prop:Font,3} = -1
    ?model_number_tick_temp{prop:Color} = 15066597
    ?model_number_tick_temp{prop:Trn} = 0
    If ?Model_Number_temp{prop:ReadOnly} = True
        ?Model_Number_temp{prop:FontColor} = 65793
        ?Model_Number_temp{prop:Color} = 15066597
    Elsif ?Model_Number_temp{prop:Req} = True
        ?Model_Number_temp{prop:FontColor} = 65793
        ?Model_Number_temp{prop:Color} = 8454143
    Else ! If ?Model_Number_temp{prop:Req} = True
        ?Model_Number_temp{prop:FontColor} = 65793
        ?Model_Number_temp{prop:Color} = 16777215
    End ! If ?Model_Number_temp{prop:Req} = True
    ?Model_Number_temp{prop:Trn} = 0
    ?Model_Number_temp{prop:FontStyle} = font:Bold
    ?colour_tick_temp{prop:Font,3} = -1
    ?colour_tick_temp{prop:Color} = 15066597
    ?colour_tick_temp{prop:Trn} = 0
    If ?Colour_Temp{prop:ReadOnly} = True
        ?Colour_Temp{prop:FontColor} = 65793
        ?Colour_Temp{prop:Color} = 15066597
    Elsif ?Colour_Temp{prop:Req} = True
        ?Colour_Temp{prop:FontColor} = 65793
        ?Colour_Temp{prop:Color} = 8454143
    Else ! If ?Colour_Temp{prop:Req} = True
        ?Colour_Temp{prop:FontColor} = 65793
        ?Colour_Temp{prop:Color} = 16777215
    End ! If ?Colour_Temp{prop:Req} = True
    ?Colour_Temp{prop:Trn} = 0
    ?Colour_Temp{prop:FontStyle} = font:Bold
    ?unit_type_tick_temp{prop:Font,3} = -1
    ?unit_type_tick_temp{prop:Color} = 15066597
    ?unit_type_tick_temp{prop:Trn} = 0
    If ?Unit_Type_temp{prop:ReadOnly} = True
        ?Unit_Type_temp{prop:FontColor} = 65793
        ?Unit_Type_temp{prop:Color} = 15066597
    Elsif ?Unit_Type_temp{prop:Req} = True
        ?Unit_Type_temp{prop:FontColor} = 65793
        ?Unit_Type_temp{prop:Color} = 8454143
    Else ! If ?Unit_Type_temp{prop:Req} = True
        ?Unit_Type_temp{prop:FontColor} = 65793
        ?Unit_Type_temp{prop:Color} = 16777215
    End ! If ?Unit_Type_temp{prop:Req} = True
    ?Unit_Type_temp{prop:Trn} = 0
    ?Unit_Type_temp{prop:FontStyle} = font:Bold
    ?esn_tick_temp{prop:Font,3} = -1
    ?esn_tick_temp{prop:Color} = 15066597
    ?esn_tick_temp{prop:Trn} = 0
    If ?ESN_temp{prop:ReadOnly} = True
        ?ESN_temp{prop:FontColor} = 65793
        ?ESN_temp{prop:Color} = 15066597
    Elsif ?ESN_temp{prop:Req} = True
        ?ESN_temp{prop:FontColor} = 65793
        ?ESN_temp{prop:Color} = 8454143
    Else ! If ?ESN_temp{prop:Req} = True
        ?ESN_temp{prop:FontColor} = 65793
        ?ESN_temp{prop:Color} = 16777215
    End ! If ?ESN_temp{prop:Req} = True
    ?ESN_temp{prop:Trn} = 0
    ?ESN_temp{prop:FontStyle} = font:Bold
    ?fault_description_tick_temp{prop:Font,3} = -1
    ?fault_description_tick_temp{prop:Color} = 15066597
    ?fault_description_tick_temp{prop:Trn} = 0
    If ?Fault_Description_temp{prop:ReadOnly} = True
        ?Fault_Description_temp{prop:FontColor} = 65793
        ?Fault_Description_temp{prop:Color} = 15066597
    Elsif ?Fault_Description_temp{prop:Req} = True
        ?Fault_Description_temp{prop:FontColor} = 65793
        ?Fault_Description_temp{prop:Color} = 8454143
    Else ! If ?Fault_Description_temp{prop:Req} = True
        ?Fault_Description_temp{prop:FontColor} = 65793
        ?Fault_Description_temp{prop:Color} = 16777215
    End ! If ?Fault_Description_temp{prop:Req} = True
    ?Fault_Description_temp{prop:Trn} = 0
    ?Fault_Description_temp{prop:FontStyle} = font:Bold
    ?tmp:DOPTick{prop:Font,3} = -1
    ?tmp:DOPTick{prop:Color} = 15066597
    ?tmp:DOPTick{prop:Trn} = 0
    If ?tmp:dop{prop:ReadOnly} = True
        ?tmp:dop{prop:FontColor} = 65793
        ?tmp:dop{prop:Color} = 15066597
    Elsif ?tmp:dop{prop:Req} = True
        ?tmp:dop{prop:FontColor} = 65793
        ?tmp:dop{prop:Color} = 8454143
    Else ! If ?tmp:dop{prop:Req} = True
        ?tmp:dop{prop:FontColor} = 65793
        ?tmp:dop{prop:Color} = 16777215
    End ! If ?tmp:dop{prop:Req} = True
    ?tmp:dop{prop:Trn} = 0
    ?tmp:dop{prop:FontStyle} = font:Bold
    ?Consignment_Number_Tick_Temp{prop:Font,3} = -1
    ?Consignment_Number_Tick_Temp{prop:Color} = 15066597
    ?Consignment_Number_Tick_Temp{prop:Trn} = 0
    If ?Consignment_Number_Temp{prop:ReadOnly} = True
        ?Consignment_Number_Temp{prop:FontColor} = 65793
        ?Consignment_Number_Temp{prop:Color} = 15066597
    Elsif ?Consignment_Number_Temp{prop:Req} = True
        ?Consignment_Number_Temp{prop:FontColor} = 65793
        ?Consignment_Number_Temp{prop:Color} = 8454143
    Else ! If ?Consignment_Number_Temp{prop:Req} = True
        ?Consignment_Number_Temp{prop:FontColor} = 65793
        ?Consignment_Number_Temp{prop:Color} = 16777215
    End ! If ?Consignment_Number_Temp{prop:Req} = True
    ?Consignment_Number_Temp{prop:Trn} = 0
    ?Consignment_Number_Temp{prop:FontStyle} = font:Bold
    ?Consignment_Date_Tick_Temp{prop:Font,3} = -1
    ?Consignment_Date_Tick_Temp{prop:Color} = 15066597
    ?Consignment_Date_Tick_Temp{prop:Trn} = 0
    If ?Consignment_Date_Temp{prop:ReadOnly} = True
        ?Consignment_Date_Temp{prop:FontColor} = 65793
        ?Consignment_Date_Temp{prop:Color} = 15066597
    Elsif ?Consignment_Date_Temp{prop:Req} = True
        ?Consignment_Date_Temp{prop:FontColor} = 65793
        ?Consignment_Date_Temp{prop:Color} = 8454143
    Else ! If ?Consignment_Date_Temp{prop:Req} = True
        ?Consignment_Date_Temp{prop:FontColor} = 65793
        ?Consignment_Date_Temp{prop:Color} = 16777215
    End ! If ?Consignment_Date_Temp{prop:Req} = True
    ?Consignment_Date_Temp{prop:Trn} = 0
    ?Consignment_Date_Temp{prop:FontStyle} = font:Bold
    ?Tab6{prop:Color} = 15066597
    ?Prompt40{prop:FontColor} = -1
    ?Prompt40{prop:Color} = 15066597
    ?Prompt41{prop:FontColor} = -1
    ?Prompt41{prop:Color} = 15066597
    ?charge_type_tick_temp{prop:Font,3} = -1
    ?charge_type_tick_temp{prop:Color} = 15066597
    ?charge_type_tick_temp{prop:Trn} = 0
    If ?Charge_Type_temp{prop:ReadOnly} = True
        ?Charge_Type_temp{prop:FontColor} = 65793
        ?Charge_Type_temp{prop:Color} = 15066597
    Elsif ?Charge_Type_temp{prop:Req} = True
        ?Charge_Type_temp{prop:FontColor} = 65793
        ?Charge_Type_temp{prop:Color} = 8454143
    Else ! If ?Charge_Type_temp{prop:Req} = True
        ?Charge_Type_temp{prop:FontColor} = 65793
        ?Charge_Type_temp{prop:Color} = 16777215
    End ! If ?Charge_Type_temp{prop:Req} = True
    ?Charge_Type_temp{prop:Trn} = 0
    ?Charge_Type_temp{prop:FontStyle} = font:Bold
    ?repair_type_tick_temp{prop:Font,3} = -1
    ?repair_type_tick_temp{prop:Color} = 15066597
    ?repair_type_tick_temp{prop:Trn} = 0
    If ?Repair_Type_temp{prop:ReadOnly} = True
        ?Repair_Type_temp{prop:FontColor} = 65793
        ?Repair_Type_temp{prop:Color} = 15066597
    Elsif ?Repair_Type_temp{prop:Req} = True
        ?Repair_Type_temp{prop:FontColor} = 65793
        ?Repair_Type_temp{prop:Color} = 8454143
    Else ! If ?Repair_Type_temp{prop:Req} = True
        ?Repair_Type_temp{prop:FontColor} = 65793
        ?Repair_Type_temp{prop:Color} = 16777215
    End ! If ?Repair_Type_temp{prop:Req} = True
    ?Repair_Type_temp{prop:Trn} = 0
    ?Repair_Type_temp{prop:FontStyle} = font:Bold
    ?warranty_job_tick_temp{prop:Font,3} = -1
    ?warranty_job_tick_temp{prop:Color} = 15066597
    ?warranty_job_tick_temp{prop:Trn} = 0
    If ?warranty_charge_type_temp{prop:ReadOnly} = True
        ?warranty_charge_type_temp{prop:FontColor} = 65793
        ?warranty_charge_type_temp{prop:Color} = 15066597
    Elsif ?warranty_charge_type_temp{prop:Req} = True
        ?warranty_charge_type_temp{prop:FontColor} = 65793
        ?warranty_charge_type_temp{prop:Color} = 8454143
    Else ! If ?warranty_charge_type_temp{prop:Req} = True
        ?warranty_charge_type_temp{prop:FontColor} = 65793
        ?warranty_charge_type_temp{prop:Color} = 16777215
    End ! If ?warranty_charge_type_temp{prop:Req} = True
    ?warranty_charge_type_temp{prop:Trn} = 0
    ?warranty_charge_type_temp{prop:FontStyle} = font:Bold
    ?repair_type_warranty_tick_temp{prop:Font,3} = -1
    ?repair_type_warranty_tick_temp{prop:Color} = 15066597
    ?repair_type_warranty_tick_temp{prop:Trn} = 0
    If ?repair_type_warranty_temp{prop:ReadOnly} = True
        ?repair_type_warranty_temp{prop:FontColor} = 65793
        ?repair_type_warranty_temp{prop:Color} = 15066597
    Elsif ?repair_type_warranty_temp{prop:Req} = True
        ?repair_type_warranty_temp{prop:FontColor} = 65793
        ?repair_type_warranty_temp{prop:Color} = 8454143
    Else ! If ?repair_type_warranty_temp{prop:Req} = True
        ?repair_type_warranty_temp{prop:FontColor} = 65793
        ?repair_type_warranty_temp{prop:Color} = 16777215
    End ! If ?repair_type_warranty_temp{prop:Req} = True
    ?repair_type_warranty_temp{prop:Trn} = 0
    ?repair_type_warranty_temp{prop:FontStyle} = font:Bold
    ?auto_complete_temp{prop:Font,3} = -1
    ?auto_complete_temp{prop:Color} = 15066597
    ?auto_complete_temp{prop:Trn} = 0
    If ?date_completed_temp{prop:ReadOnly} = True
        ?date_completed_temp{prop:FontColor} = 65793
        ?date_completed_temp{prop:Color} = 15066597
    Elsif ?date_completed_temp{prop:Req} = True
        ?date_completed_temp{prop:FontColor} = 65793
        ?date_completed_temp{prop:Color} = 8454143
    Else ! If ?date_completed_temp{prop:Req} = True
        ?date_completed_temp{prop:FontColor} = 65793
        ?date_completed_temp{prop:Color} = 16777215
    End ! If ?date_completed_temp{prop:Req} = True
    ?date_completed_temp{prop:Trn} = 0
    ?date_completed_temp{prop:FontStyle} = font:Bold
    ?auto_despatch_temp{prop:Font,3} = -1
    ?auto_despatch_temp{prop:Color} = 15066597
    ?auto_despatch_temp{prop:Trn} = 0
    If ?courier_temp{prop:ReadOnly} = True
        ?courier_temp{prop:FontColor} = 65793
        ?courier_temp{prop:Color} = 15066597
    Elsif ?courier_temp{prop:Req} = True
        ?courier_temp{prop:FontColor} = 65793
        ?courier_temp{prop:Color} = 8454143
    Else ! If ?courier_temp{prop:Req} = True
        ?courier_temp{prop:FontColor} = 65793
        ?courier_temp{prop:Color} = 16777215
    End ! If ?courier_temp{prop:Req} = True
    ?courier_temp{prop:Trn} = 0
    ?courier_temp{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?Prompt23{prop:FontColor} = -1
    ?Prompt23{prop:Color} = 15066597
    If ?Account_Number_temp{prop:ReadOnly} = True
        ?Account_Number_temp{prop:FontColor} = 65793
        ?Account_Number_temp{prop:Color} = 15066597
    Elsif ?Account_Number_temp{prop:Req} = True
        ?Account_Number_temp{prop:FontColor} = 65793
        ?Account_Number_temp{prop:Color} = 8454143
    Else ! If ?Account_Number_temp{prop:Req} = True
        ?Account_Number_temp{prop:FontColor} = 65793
        ?Account_Number_temp{prop:Color} = 16777215
    End ! If ?Account_Number_temp{prop:Req} = True
    ?Account_Number_temp{prop:Trn} = 0
    ?Account_Number_temp{prop:FontStyle} = font:Bold
    ?Prompt24{prop:FontColor} = -1
    ?Prompt24{prop:Color} = 15066597
    If ?Transit_Type_temp{prop:ReadOnly} = True
        ?Transit_Type_temp{prop:FontColor} = 65793
        ?Transit_Type_temp{prop:Color} = 15066597
    Elsif ?Transit_Type_temp{prop:Req} = True
        ?Transit_Type_temp{prop:FontColor} = 65793
        ?Transit_Type_temp{prop:Color} = 8454143
    Else ! If ?Transit_Type_temp{prop:Req} = True
        ?Transit_Type_temp{prop:FontColor} = 65793
        ?Transit_Type_temp{prop:Color} = 16777215
    End ! If ?Transit_Type_temp{prop:Req} = True
    ?Transit_Type_temp{prop:Trn} = 0
    ?Transit_Type_temp{prop:FontStyle} = font:Bold
    ?name_group{prop:Font,3} = -1
    ?name_group{prop:Color} = 15066597
    ?name_group{prop:Trn} = 0
    ?Title_temp:Prompt{prop:FontColor} = -1
    ?Title_temp:Prompt{prop:Color} = 15066597
    ?Title_temp:Prompt:2{prop:FontColor} = -1
    ?Title_temp:Prompt:2{prop:Color} = 15066597
    ?Title_temp:Prompt:3{prop:FontColor} = -1
    ?Title_temp:Prompt:3{prop:Color} = 15066597
    ?Title_temp:Prompt:4{prop:FontColor} = -1
    ?Title_temp:Prompt:4{prop:Color} = 15066597
    If ?Title_temp{prop:ReadOnly} = True
        ?Title_temp{prop:FontColor} = 65793
        ?Title_temp{prop:Color} = 15066597
    Elsif ?Title_temp{prop:Req} = True
        ?Title_temp{prop:FontColor} = 65793
        ?Title_temp{prop:Color} = 8454143
    Else ! If ?Title_temp{prop:Req} = True
        ?Title_temp{prop:FontColor} = 65793
        ?Title_temp{prop:Color} = 16777215
    End ! If ?Title_temp{prop:Req} = True
    ?Title_temp{prop:Trn} = 0
    ?Title_temp{prop:FontStyle} = font:Bold
    If ?initial_temp{prop:ReadOnly} = True
        ?initial_temp{prop:FontColor} = 65793
        ?initial_temp{prop:Color} = 15066597
    Elsif ?initial_temp{prop:Req} = True
        ?initial_temp{prop:FontColor} = 65793
        ?initial_temp{prop:Color} = 8454143
    Else ! If ?initial_temp{prop:Req} = True
        ?initial_temp{prop:FontColor} = 65793
        ?initial_temp{prop:Color} = 16777215
    End ! If ?initial_temp{prop:Req} = True
    ?initial_temp{prop:Trn} = 0
    ?initial_temp{prop:FontStyle} = font:Bold
    If ?surname_temp{prop:ReadOnly} = True
        ?surname_temp{prop:FontColor} = 65793
        ?surname_temp{prop:Color} = 15066597
    Elsif ?surname_temp{prop:Req} = True
        ?surname_temp{prop:FontColor} = 65793
        ?surname_temp{prop:Color} = 8454143
    Else ! If ?surname_temp{prop:Req} = True
        ?surname_temp{prop:FontColor} = 65793
        ?surname_temp{prop:Color} = 16777215
    End ! If ?surname_temp{prop:Req} = True
    ?surname_temp{prop:Trn} = 0
    ?surname_temp{prop:FontStyle} = font:Bold
    ?Prompt20{prop:FontColor} = -1
    ?Prompt20{prop:Color} = 15066597
    ?Company_Name:Prompt{prop:FontColor} = -1
    ?Company_Name:Prompt{prop:Color} = 15066597
    If ?Company_Name{prop:ReadOnly} = True
        ?Company_Name{prop:FontColor} = 65793
        ?Company_Name{prop:Color} = 15066597
    Elsif ?Company_Name{prop:Req} = True
        ?Company_Name{prop:FontColor} = 65793
        ?Company_Name{prop:Color} = 8454143
    Else ! If ?Company_Name{prop:Req} = True
        ?Company_Name{prop:FontColor} = 65793
        ?Company_Name{prop:Color} = 16777215
    End ! If ?Company_Name{prop:Req} = True
    ?Company_Name{prop:Trn} = 0
    ?Company_Name{prop:FontStyle} = font:Bold
    ?Postcode:Prompt{prop:FontColor} = -1
    ?Postcode:Prompt{prop:Color} = 15066597
    If ?Postcode{prop:ReadOnly} = True
        ?Postcode{prop:FontColor} = 65793
        ?Postcode{prop:Color} = 15066597
    Elsif ?Postcode{prop:Req} = True
        ?Postcode{prop:FontColor} = 65793
        ?Postcode{prop:Color} = 8454143
    Else ! If ?Postcode{prop:Req} = True
        ?Postcode{prop:FontColor} = 65793
        ?Postcode{prop:Color} = 16777215
    End ! If ?Postcode{prop:Req} = True
    ?Postcode{prop:Trn} = 0
    ?Postcode{prop:FontStyle} = font:Bold
    ?Address_Line1:Prompt{prop:FontColor} = -1
    ?Address_Line1:Prompt{prop:Color} = 15066597
    If ?Address_Line1{prop:ReadOnly} = True
        ?Address_Line1{prop:FontColor} = 65793
        ?Address_Line1{prop:Color} = 15066597
    Elsif ?Address_Line1{prop:Req} = True
        ?Address_Line1{prop:FontColor} = 65793
        ?Address_Line1{prop:Color} = 8454143
    Else ! If ?Address_Line1{prop:Req} = True
        ?Address_Line1{prop:FontColor} = 65793
        ?Address_Line1{prop:Color} = 16777215
    End ! If ?Address_Line1{prop:Req} = True
    ?Address_Line1{prop:Trn} = 0
    ?Address_Line1{prop:FontStyle} = font:Bold
    If ?Address_Line2{prop:ReadOnly} = True
        ?Address_Line2{prop:FontColor} = 65793
        ?Address_Line2{prop:Color} = 15066597
    Elsif ?Address_Line2{prop:Req} = True
        ?Address_Line2{prop:FontColor} = 65793
        ?Address_Line2{prop:Color} = 8454143
    Else ! If ?Address_Line2{prop:Req} = True
        ?Address_Line2{prop:FontColor} = 65793
        ?Address_Line2{prop:Color} = 16777215
    End ! If ?Address_Line2{prop:Req} = True
    ?Address_Line2{prop:Trn} = 0
    ?Address_Line2{prop:FontStyle} = font:Bold
    If ?Address_Line3{prop:ReadOnly} = True
        ?Address_Line3{prop:FontColor} = 65793
        ?Address_Line3{prop:Color} = 15066597
    Elsif ?Address_Line3{prop:Req} = True
        ?Address_Line3{prop:FontColor} = 65793
        ?Address_Line3{prop:Color} = 8454143
    Else ! If ?Address_Line3{prop:Req} = True
        ?Address_Line3{prop:FontColor} = 65793
        ?Address_Line3{prop:Color} = 16777215
    End ! If ?Address_Line3{prop:Req} = True
    ?Address_Line3{prop:Trn} = 0
    ?Address_Line3{prop:FontStyle} = font:Bold
    ?Telephone_Number:Prompt{prop:FontColor} = -1
    ?Telephone_Number:Prompt{prop:Color} = 15066597
    If ?Telephone_Number{prop:ReadOnly} = True
        ?Telephone_Number{prop:FontColor} = 65793
        ?Telephone_Number{prop:Color} = 15066597
    Elsif ?Telephone_Number{prop:Req} = True
        ?Telephone_Number{prop:FontColor} = 65793
        ?Telephone_Number{prop:Color} = 8454143
    Else ! If ?Telephone_Number{prop:Req} = True
        ?Telephone_Number{prop:FontColor} = 65793
        ?Telephone_Number{prop:Color} = 16777215
    End ! If ?Telephone_Number{prop:Req} = True
    ?Telephone_Number{prop:Trn} = 0
    ?Telephone_Number{prop:FontStyle} = font:Bold
    ?Fax_Number:Prompt{prop:FontColor} = -1
    ?Fax_Number:Prompt{prop:Color} = 15066597
    If ?Fax_Number{prop:ReadOnly} = True
        ?Fax_Number{prop:FontColor} = 65793
        ?Fax_Number{prop:Color} = 15066597
    Elsif ?Fax_Number{prop:Req} = True
        ?Fax_Number{prop:FontColor} = 65793
        ?Fax_Number{prop:Color} = 8454143
    Else ! If ?Fax_Number{prop:Req} = True
        ?Fax_Number{prop:FontColor} = 65793
        ?Fax_Number{prop:Color} = 16777215
    End ! If ?Fax_Number{prop:Req} = True
    ?Fax_Number{prop:Trn} = 0
    ?Fax_Number{prop:FontStyle} = font:Bold
    ?Delivery_address_Group{prop:Font,3} = -1
    ?Delivery_address_Group{prop:Color} = 15066597
    ?Delivery_address_Group{prop:Trn} = 0
    ?Prompt22{prop:FontColor} = -1
    ?Prompt22{prop:Color} = 15066597
    ?Company_Name:Prompt:2{prop:FontColor} = -1
    ?Company_Name:Prompt:2{prop:Color} = 15066597
    If ?Company_Name_Delivery{prop:ReadOnly} = True
        ?Company_Name_Delivery{prop:FontColor} = 65793
        ?Company_Name_Delivery{prop:Color} = 15066597
    Elsif ?Company_Name_Delivery{prop:Req} = True
        ?Company_Name_Delivery{prop:FontColor} = 65793
        ?Company_Name_Delivery{prop:Color} = 8454143
    Else ! If ?Company_Name_Delivery{prop:Req} = True
        ?Company_Name_Delivery{prop:FontColor} = 65793
        ?Company_Name_Delivery{prop:Color} = 16777215
    End ! If ?Company_Name_Delivery{prop:Req} = True
    ?Company_Name_Delivery{prop:Trn} = 0
    ?Company_Name_Delivery{prop:FontStyle} = font:Bold
    ?Postcode:Prompt:2{prop:FontColor} = -1
    ?Postcode:Prompt:2{prop:Color} = 15066597
    If ?Postcode_Delivery{prop:ReadOnly} = True
        ?Postcode_Delivery{prop:FontColor} = 65793
        ?Postcode_Delivery{prop:Color} = 15066597
    Elsif ?Postcode_Delivery{prop:Req} = True
        ?Postcode_Delivery{prop:FontColor} = 65793
        ?Postcode_Delivery{prop:Color} = 8454143
    Else ! If ?Postcode_Delivery{prop:Req} = True
        ?Postcode_Delivery{prop:FontColor} = 65793
        ?Postcode_Delivery{prop:Color} = 16777215
    End ! If ?Postcode_Delivery{prop:Req} = True
    ?Postcode_Delivery{prop:Trn} = 0
    ?Postcode_Delivery{prop:FontStyle} = font:Bold
    ?Address_Line1:Prompt:2{prop:FontColor} = -1
    ?Address_Line1:Prompt:2{prop:Color} = 15066597
    If ?Address_Line1_Delivery{prop:ReadOnly} = True
        ?Address_Line1_Delivery{prop:FontColor} = 65793
        ?Address_Line1_Delivery{prop:Color} = 15066597
    Elsif ?Address_Line1_Delivery{prop:Req} = True
        ?Address_Line1_Delivery{prop:FontColor} = 65793
        ?Address_Line1_Delivery{prop:Color} = 8454143
    Else ! If ?Address_Line1_Delivery{prop:Req} = True
        ?Address_Line1_Delivery{prop:FontColor} = 65793
        ?Address_Line1_Delivery{prop:Color} = 16777215
    End ! If ?Address_Line1_Delivery{prop:Req} = True
    ?Address_Line1_Delivery{prop:Trn} = 0
    ?Address_Line1_Delivery{prop:FontStyle} = font:Bold
    If ?Address_Line2_Delivery{prop:ReadOnly} = True
        ?Address_Line2_Delivery{prop:FontColor} = 65793
        ?Address_Line2_Delivery{prop:Color} = 15066597
    Elsif ?Address_Line2_Delivery{prop:Req} = True
        ?Address_Line2_Delivery{prop:FontColor} = 65793
        ?Address_Line2_Delivery{prop:Color} = 8454143
    Else ! If ?Address_Line2_Delivery{prop:Req} = True
        ?Address_Line2_Delivery{prop:FontColor} = 65793
        ?Address_Line2_Delivery{prop:Color} = 16777215
    End ! If ?Address_Line2_Delivery{prop:Req} = True
    ?Address_Line2_Delivery{prop:Trn} = 0
    ?Address_Line2_Delivery{prop:FontStyle} = font:Bold
    If ?Address_Line3_Delivery{prop:ReadOnly} = True
        ?Address_Line3_Delivery{prop:FontColor} = 65793
        ?Address_Line3_Delivery{prop:Color} = 15066597
    Elsif ?Address_Line3_Delivery{prop:Req} = True
        ?Address_Line3_Delivery{prop:FontColor} = 65793
        ?Address_Line3_Delivery{prop:Color} = 8454143
    Else ! If ?Address_Line3_Delivery{prop:Req} = True
        ?Address_Line3_Delivery{prop:FontColor} = 65793
        ?Address_Line3_Delivery{prop:Color} = 16777215
    End ! If ?Address_Line3_Delivery{prop:Req} = True
    ?Address_Line3_Delivery{prop:Trn} = 0
    ?Address_Line3_Delivery{prop:FontStyle} = font:Bold
    ?Telephone_Number:Prompt:2{prop:FontColor} = -1
    ?Telephone_Number:Prompt:2{prop:Color} = 15066597
    If ?Telephone_Delivery{prop:ReadOnly} = True
        ?Telephone_Delivery{prop:FontColor} = 65793
        ?Telephone_Delivery{prop:Color} = 15066597
    Elsif ?Telephone_Delivery{prop:Req} = True
        ?Telephone_Delivery{prop:FontColor} = 65793
        ?Telephone_Delivery{prop:Color} = 8454143
    Else ! If ?Telephone_Delivery{prop:Req} = True
        ?Telephone_Delivery{prop:FontColor} = 65793
        ?Telephone_Delivery{prop:Color} = 16777215
    End ! If ?Telephone_Delivery{prop:Req} = True
    ?Telephone_Delivery{prop:Trn} = 0
    ?Telephone_Delivery{prop:FontStyle} = font:Bold
    ?collection_address_group{prop:Font,3} = -1
    ?collection_address_group{prop:Color} = 15066597
    ?collection_address_group{prop:Trn} = 0
    ?Prompt21{prop:FontColor} = -1
    ?Prompt21{prop:Color} = 15066597
    If ?Company_Name_Collection{prop:ReadOnly} = True
        ?Company_Name_Collection{prop:FontColor} = 65793
        ?Company_Name_Collection{prop:Color} = 15066597
    Elsif ?Company_Name_Collection{prop:Req} = True
        ?Company_Name_Collection{prop:FontColor} = 65793
        ?Company_Name_Collection{prop:Color} = 8454143
    Else ! If ?Company_Name_Collection{prop:Req} = True
        ?Company_Name_Collection{prop:FontColor} = 65793
        ?Company_Name_Collection{prop:Color} = 16777215
    End ! If ?Company_Name_Collection{prop:Req} = True
    ?Company_Name_Collection{prop:Trn} = 0
    ?Company_Name_Collection{prop:FontStyle} = font:Bold
    If ?Postcode_Collection{prop:ReadOnly} = True
        ?Postcode_Collection{prop:FontColor} = 65793
        ?Postcode_Collection{prop:Color} = 15066597
    Elsif ?Postcode_Collection{prop:Req} = True
        ?Postcode_Collection{prop:FontColor} = 65793
        ?Postcode_Collection{prop:Color} = 8454143
    Else ! If ?Postcode_Collection{prop:Req} = True
        ?Postcode_Collection{prop:FontColor} = 65793
        ?Postcode_Collection{prop:Color} = 16777215
    End ! If ?Postcode_Collection{prop:Req} = True
    ?Postcode_Collection{prop:Trn} = 0
    ?Postcode_Collection{prop:FontStyle} = font:Bold
    If ?Address_Line1_Collection{prop:ReadOnly} = True
        ?Address_Line1_Collection{prop:FontColor} = 65793
        ?Address_Line1_Collection{prop:Color} = 15066597
    Elsif ?Address_Line1_Collection{prop:Req} = True
        ?Address_Line1_Collection{prop:FontColor} = 65793
        ?Address_Line1_Collection{prop:Color} = 8454143
    Else ! If ?Address_Line1_Collection{prop:Req} = True
        ?Address_Line1_Collection{prop:FontColor} = 65793
        ?Address_Line1_Collection{prop:Color} = 16777215
    End ! If ?Address_Line1_Collection{prop:Req} = True
    ?Address_Line1_Collection{prop:Trn} = 0
    ?Address_Line1_Collection{prop:FontStyle} = font:Bold
    If ?Address_Line2_Collection{prop:ReadOnly} = True
        ?Address_Line2_Collection{prop:FontColor} = 65793
        ?Address_Line2_Collection{prop:Color} = 15066597
    Elsif ?Address_Line2_Collection{prop:Req} = True
        ?Address_Line2_Collection{prop:FontColor} = 65793
        ?Address_Line2_Collection{prop:Color} = 8454143
    Else ! If ?Address_Line2_Collection{prop:Req} = True
        ?Address_Line2_Collection{prop:FontColor} = 65793
        ?Address_Line2_Collection{prop:Color} = 16777215
    End ! If ?Address_Line2_Collection{prop:Req} = True
    ?Address_Line2_Collection{prop:Trn} = 0
    ?Address_Line2_Collection{prop:FontStyle} = font:Bold
    If ?Address_Line3_Collection{prop:ReadOnly} = True
        ?Address_Line3_Collection{prop:FontColor} = 65793
        ?Address_Line3_Collection{prop:Color} = 15066597
    Elsif ?Address_Line3_Collection{prop:Req} = True
        ?Address_Line3_Collection{prop:FontColor} = 65793
        ?Address_Line3_Collection{prop:Color} = 8454143
    Else ! If ?Address_Line3_Collection{prop:Req} = True
        ?Address_Line3_Collection{prop:FontColor} = 65793
        ?Address_Line3_Collection{prop:Color} = 16777215
    End ! If ?Address_Line3_Collection{prop:Req} = True
    ?Address_Line3_Collection{prop:Trn} = 0
    ?Address_Line3_Collection{prop:FontStyle} = font:Bold
    If ?Telephone_Collection{prop:ReadOnly} = True
        ?Telephone_Collection{prop:FontColor} = 65793
        ?Telephone_Collection{prop:Color} = 15066597
    Elsif ?Telephone_Collection{prop:Req} = True
        ?Telephone_Collection{prop:FontColor} = 65793
        ?Telephone_Collection{prop:Color} = 8454143
    Else ! If ?Telephone_Collection{prop:Req} = True
        ?Telephone_Collection{prop:FontColor} = 65793
        ?Telephone_Collection{prop:Color} = 16777215
    End ! If ?Telephone_Collection{prop:Req} = True
    ?Telephone_Collection{prop:Trn} = 0
    ?Telephone_Collection{prop:FontStyle} = font:Bold
    ?Tab5{prop:Color} = 15066597
    ?Prompt37{prop:FontColor} = -1
    ?Prompt37{prop:Color} = 15066597
    ?Prompt38{prop:FontColor} = -1
    ?Prompt38{prop:Color} = 15066597
    ?print_bookin_label_temp{prop:Font,3} = -1
    ?print_bookin_label_temp{prop:Color} = 15066597
    ?print_bookin_label_temp{prop:Trn} = 0
    ?print_job_label_temp{prop:Font,3} = -1
    ?print_job_label_temp{prop:Color} = 15066597
    ?print_job_label_temp{prop:Trn} = 0
    ?Print_Job_Card_Temp{prop:Font,3} = -1
    ?Print_Job_Card_Temp{prop:Color} = 15066597
    ?Print_Job_Card_Temp{prop:Trn} = 0
    ?Print_Job_Receipt_Temp{prop:Font,3} = -1
    ?Print_Job_Receipt_Temp{prop:Color} = 15066597
    ?Print_Job_Receipt_Temp{prop:Trn} = 0
    ?Print_Despatch_Notes_Temp{prop:Font,3} = -1
    ?Print_Despatch_Notes_Temp{prop:Color} = 15066597
    ?Print_Despatch_Notes_Temp{prop:Trn} = 0
    ?Tab4{prop:Color} = 15066597
    ?Prompt33{prop:FontColor} = -1
    ?Prompt33{prop:Color} = 15066597
    ?Prompt34{prop:FontColor} = -1
    ?Prompt34{prop:Color} = 15066597
    ?use_batch_temp{prop:Font,3} = -1
    ?use_batch_temp{prop:Color} = 15066597
    ?use_batch_temp{prop:Trn} = 0
    ?Option5:Radio1{prop:Font,3} = -1
    ?Option5:Radio1{prop:Color} = 15066597
    ?Option5:Radio1{prop:Trn} = 0
    ?Option5:Radio2{prop:Font,3} = -1
    ?Option5:Radio2{prop:Color} = 15066597
    ?Option5:Radio2{prop:Trn} = 0
    ?Option5:Radio3{prop:Font,3} = -1
    ?Option5:Radio3{prop:Color} = 15066597
    ?Option5:Radio3{prop:Trn} = 0
    ?Batch_Message{prop:FontColor} = -1
    ?Batch_Message{prop:Color} = 15066597
    If ?batch_number_temp{prop:ReadOnly} = True
        ?batch_number_temp{prop:FontColor} = 65793
        ?batch_number_temp{prop:Color} = 15066597
    Elsif ?batch_number_temp{prop:Req} = True
        ?batch_number_temp{prop:FontColor} = 65793
        ?batch_number_temp{prop:Color} = 8454143
    Else ! If ?batch_number_temp{prop:Req} = True
        ?batch_number_temp{prop:FontColor} = 65793
        ?batch_number_temp{prop:Color} = 16777215
    End ! If ?batch_number_temp{prop:Req} = True
    ?batch_number_temp{prop:Trn} = 0
    ?batch_number_temp{prop:FontStyle} = font:Bold
    ?String2{prop:FontColor} = -1
    ?String2{prop:Color} = 15066597
    ?Tab6:2{prop:Color} = 15066597
    ?Prompt42{prop:FontColor} = -1
    ?Prompt42{prop:Color} = 15066597
    ?Fields_Group{prop:Font,3} = -1
    ?Fields_Group{prop:Color} = 15066597
    ?Fields_Group{prop:Trn} = 0
    ?ESN_temp:Prompt{prop:FontColor} = -1
    ?ESN_temp:Prompt{prop:Color} = 15066597
    If ?ESN_temp:2{prop:ReadOnly} = True
        ?ESN_temp:2{prop:FontColor} = 65793
        ?ESN_temp:2{prop:Color} = 15066597
    Elsif ?ESN_temp:2{prop:Req} = True
        ?ESN_temp:2{prop:FontColor} = 65793
        ?ESN_temp:2{prop:Color} = 8454143
    Else ! If ?ESN_temp:2{prop:Req} = True
        ?ESN_temp:2{prop:FontColor} = 65793
        ?ESN_temp:2{prop:Color} = 16777215
    End ! If ?ESN_temp:2{prop:Req} = True
    ?ESN_temp:2{prop:Trn} = 0
    ?ESN_temp:2{prop:FontStyle} = font:Bold
    ?BouncerText{prop:FontColor} = -1
    ?BouncerText{prop:Color} = 15066597
    ?msn:prompt{prop:FontColor} = -1
    ?msn:prompt{prop:Color} = 15066597
    If ?MSN_temp{prop:ReadOnly} = True
        ?MSN_temp{prop:FontColor} = 65793
        ?MSN_temp{prop:Color} = 15066597
    Elsif ?MSN_temp{prop:Req} = True
        ?MSN_temp{prop:FontColor} = 65793
        ?MSN_temp{prop:Color} = 8454143
    Else ! If ?MSN_temp{prop:Req} = True
        ?MSN_temp{prop:FontColor} = 65793
        ?MSN_temp{prop:Color} = 16777215
    End ! If ?MSN_temp{prop:Req} = True
    ?MSN_temp{prop:Trn} = 0
    ?MSN_temp{prop:FontStyle} = font:Bold
    ?tmp:ProductCode:Prompt{prop:FontColor} = -1
    ?tmp:ProductCode:Prompt{prop:Color} = 15066597
    If ?tmp:ProductCode{prop:ReadOnly} = True
        ?tmp:ProductCode{prop:FontColor} = 65793
        ?tmp:ProductCode{prop:Color} = 15066597
    Elsif ?tmp:ProductCode{prop:Req} = True
        ?tmp:ProductCode{prop:FontColor} = 65793
        ?tmp:ProductCode{prop:Color} = 8454143
    Else ! If ?tmp:ProductCode{prop:Req} = True
        ?tmp:ProductCode{prop:FontColor} = 65793
        ?tmp:ProductCode{prop:Color} = 16777215
    End ! If ?tmp:ProductCode{prop:Req} = True
    ?tmp:ProductCode{prop:Trn} = 0
    ?tmp:ProductCode{prop:FontStyle} = font:Bold
    ?Prompt26{prop:FontColor} = -1
    ?Prompt26{prop:Color} = 15066597
    If ?Model_Number_temp:2{prop:ReadOnly} = True
        ?Model_Number_temp:2{prop:FontColor} = 65793
        ?Model_Number_temp:2{prop:Color} = 15066597
    Elsif ?Model_Number_temp:2{prop:Req} = True
        ?Model_Number_temp:2{prop:FontColor} = 65793
        ?Model_Number_temp:2{prop:Color} = 8454143
    Else ! If ?Model_Number_temp:2{prop:Req} = True
        ?Model_Number_temp:2{prop:FontColor} = 65793
        ?Model_Number_temp:2{prop:Color} = 16777215
    End ! If ?Model_Number_temp:2{prop:Req} = True
    ?Model_Number_temp:2{prop:Trn} = 0
    ?Model_Number_temp:2{prop:FontStyle} = font:Bold
    ?Prompt27{prop:FontColor} = -1
    ?Prompt27{prop:Color} = 15066597
    If ?Unit_Type_temp:2{prop:ReadOnly} = True
        ?Unit_Type_temp:2{prop:FontColor} = 65793
        ?Unit_Type_temp:2{prop:Color} = 15066597
    Elsif ?Unit_Type_temp:2{prop:Req} = True
        ?Unit_Type_temp:2{prop:FontColor} = 65793
        ?Unit_Type_temp:2{prop:Color} = 8454143
    Else ! If ?Unit_Type_temp:2{prop:Req} = True
        ?Unit_Type_temp:2{prop:FontColor} = 65793
        ?Unit_Type_temp:2{prop:Color} = 16777215
    End ! If ?Unit_Type_temp:2{prop:Req} = True
    ?Unit_Type_temp:2{prop:Trn} = 0
    ?Unit_Type_temp:2{prop:FontStyle} = font:Bold
    ?Prompt43{prop:FontColor} = -1
    ?Prompt43{prop:Color} = 15066597
    If ?accessories_temp{prop:ReadOnly} = True
        ?accessories_temp{prop:FontColor} = 65793
        ?accessories_temp{prop:Color} = 15066597
    Elsif ?accessories_temp{prop:Req} = True
        ?accessories_temp{prop:FontColor} = 65793
        ?accessories_temp{prop:Color} = 8454143
    Else ! If ?accessories_temp{prop:Req} = True
        ?accessories_temp{prop:FontColor} = 65793
        ?accessories_temp{prop:Color} = 16777215
    End ! If ?accessories_temp{prop:Req} = True
    ?accessories_temp{prop:Trn} = 0
    ?accessories_temp{prop:FontStyle} = font:Bold
    ?Order_Number_Temp:Prompt{prop:FontColor} = -1
    ?Order_Number_Temp:Prompt{prop:Color} = 15066597
    If ?Order_Number_Temp:2{prop:ReadOnly} = True
        ?Order_Number_Temp:2{prop:FontColor} = 65793
        ?Order_Number_Temp:2{prop:Color} = 15066597
    Elsif ?Order_Number_Temp:2{prop:Req} = True
        ?Order_Number_Temp:2{prop:FontColor} = 65793
        ?Order_Number_Temp:2{prop:Color} = 8454143
    Else ! If ?Order_Number_Temp:2{prop:Req} = True
        ?Order_Number_Temp:2{prop:FontColor} = 65793
        ?Order_Number_Temp:2{prop:Color} = 16777215
    End ! If ?Order_Number_Temp:2{prop:Req} = True
    ?Order_Number_Temp:2{prop:Trn} = 0
    ?Order_Number_Temp:2{prop:FontStyle} = font:Bold
    ?Prompt36{prop:FontColor} = -1
    ?Prompt36{prop:Color} = 15066597
    If ?Internal_Location_Temp:2{prop:ReadOnly} = True
        ?Internal_Location_Temp:2{prop:FontColor} = 65793
        ?Internal_Location_Temp:2{prop:Color} = 15066597
    Elsif ?Internal_Location_Temp:2{prop:Req} = True
        ?Internal_Location_Temp:2{prop:FontColor} = 65793
        ?Internal_Location_Temp:2{prop:Color} = 8454143
    Else ! If ?Internal_Location_Temp:2{prop:Req} = True
        ?Internal_Location_Temp:2{prop:FontColor} = 65793
        ?Internal_Location_Temp:2{prop:Color} = 16777215
    End ! If ?Internal_Location_Temp:2{prop:Req} = True
    ?Internal_Location_Temp:2{prop:Trn} = 0
    ?Internal_Location_Temp:2{prop:FontStyle} = font:Bold
    ?Prompt25{prop:FontColor} = -1
    ?Prompt25{prop:Color} = 15066597
    If ?Fault_Description_temp:2{prop:ReadOnly} = True
        ?Fault_Description_temp:2{prop:FontColor} = 65793
        ?Fault_Description_temp:2{prop:Color} = 15066597
    Elsif ?Fault_Description_temp:2{prop:Req} = True
        ?Fault_Description_temp:2{prop:FontColor} = 65793
        ?Fault_Description_temp:2{prop:Color} = 8454143
    Else ! If ?Fault_Description_temp:2{prop:Req} = True
        ?Fault_Description_temp:2{prop:FontColor} = 65793
        ?Fault_Description_temp:2{prop:Color} = 16777215
    End ! If ?Fault_Description_temp:2{prop:Req} = True
    ?Fault_Description_temp:2{prop:Trn} = 0
    ?Fault_Description_temp:2{prop:FontStyle} = font:Bold
    ?Mobile_Number:Prompt{prop:FontColor} = -1
    ?Mobile_Number:Prompt{prop:Color} = 15066597
    If ?Mobile_Number_temp:2{prop:ReadOnly} = True
        ?Mobile_Number_temp:2{prop:FontColor} = 65793
        ?Mobile_Number_temp:2{prop:Color} = 15066597
    Elsif ?Mobile_Number_temp:2{prop:Req} = True
        ?Mobile_Number_temp:2{prop:FontColor} = 65793
        ?Mobile_Number_temp:2{prop:Color} = 8454143
    Else ! If ?Mobile_Number_temp:2{prop:Req} = True
        ?Mobile_Number_temp:2{prop:FontColor} = 65793
        ?Mobile_Number_temp:2{prop:Color} = 16777215
    End ! If ?Mobile_Number_temp:2{prop:Req} = True
    ?Mobile_Number_temp:2{prop:Trn} = 0
    ?Mobile_Number_temp:2{prop:FontStyle} = font:Bold
    ?tmp:dop:Prompt{prop:FontColor} = -1
    ?tmp:dop:Prompt{prop:Color} = 15066597
    If ?tmp:dop:2{prop:ReadOnly} = True
        ?tmp:dop:2{prop:FontColor} = 65793
        ?tmp:dop:2{prop:Color} = 15066597
    Elsif ?tmp:dop:2{prop:Req} = True
        ?tmp:dop:2{prop:FontColor} = 65793
        ?tmp:dop:2{prop:Color} = 8454143
    Else ! If ?tmp:dop:2{prop:Req} = True
        ?tmp:dop:2{prop:FontColor} = 65793
        ?tmp:dop:2{prop:Color} = 16777215
    End ! If ?tmp:dop:2{prop:Req} = True
    ?tmp:dop:2{prop:Trn} = 0
    ?tmp:dop:2{prop:FontStyle} = font:Bold
    ?Prompt28{prop:FontColor} = -1
    ?Prompt28{prop:Color} = 15066597
    If ?Charge_Type_temp:2{prop:ReadOnly} = True
        ?Charge_Type_temp:2{prop:FontColor} = 65793
        ?Charge_Type_temp:2{prop:Color} = 15066597
    Elsif ?Charge_Type_temp:2{prop:Req} = True
        ?Charge_Type_temp:2{prop:FontColor} = 65793
        ?Charge_Type_temp:2{prop:Color} = 8454143
    Else ! If ?Charge_Type_temp:2{prop:Req} = True
        ?Charge_Type_temp:2{prop:FontColor} = 65793
        ?Charge_Type_temp:2{prop:Color} = 16777215
    End ! If ?Charge_Type_temp:2{prop:Req} = True
    ?Charge_Type_temp:2{prop:Trn} = 0
    ?Charge_Type_temp:2{prop:FontStyle} = font:Bold
    ?Prompt31{prop:FontColor} = -1
    ?Prompt31{prop:Color} = 15066597
    If ?Repair_Type_temp:2{prop:ReadOnly} = True
        ?Repair_Type_temp:2{prop:FontColor} = 65793
        ?Repair_Type_temp:2{prop:Color} = 15066597
    Elsif ?Repair_Type_temp:2{prop:Req} = True
        ?Repair_Type_temp:2{prop:FontColor} = 65793
        ?Repair_Type_temp:2{prop:Color} = 8454143
    Else ! If ?Repair_Type_temp:2{prop:Req} = True
        ?Repair_Type_temp:2{prop:FontColor} = 65793
        ?Repair_Type_temp:2{prop:Color} = 16777215
    End ! If ?Repair_Type_temp:2{prop:Req} = True
    ?Repair_Type_temp:2{prop:Trn} = 0
    ?Repair_Type_temp:2{prop:FontStyle} = font:Bold
    ?Prompt35{prop:FontColor} = -1
    ?Prompt35{prop:Color} = 15066597
    If ?warranty_charge_type_temp:2{prop:ReadOnly} = True
        ?warranty_charge_type_temp:2{prop:FontColor} = 65793
        ?warranty_charge_type_temp:2{prop:Color} = 15066597
    Elsif ?warranty_charge_type_temp:2{prop:Req} = True
        ?warranty_charge_type_temp:2{prop:FontColor} = 65793
        ?warranty_charge_type_temp:2{prop:Color} = 8454143
    Else ! If ?warranty_charge_type_temp:2{prop:Req} = True
        ?warranty_charge_type_temp:2{prop:FontColor} = 65793
        ?warranty_charge_type_temp:2{prop:Color} = 16777215
    End ! If ?warranty_charge_type_temp:2{prop:Req} = True
    ?warranty_charge_type_temp:2{prop:Trn} = 0
    ?warranty_charge_type_temp:2{prop:FontStyle} = font:Bold
    ?repair_type_warranty_temp:Prompt{prop:FontColor} = -1
    ?repair_type_warranty_temp:Prompt{prop:Color} = 15066597
    If ?repair_type_warranty_temp:2{prop:ReadOnly} = True
        ?repair_type_warranty_temp:2{prop:FontColor} = 65793
        ?repair_type_warranty_temp:2{prop:Color} = 15066597
    Elsif ?repair_type_warranty_temp:2{prop:Req} = True
        ?repair_type_warranty_temp:2{prop:FontColor} = 65793
        ?repair_type_warranty_temp:2{prop:Color} = 8454143
    Else ! If ?repair_type_warranty_temp:2{prop:Req} = True
        ?repair_type_warranty_temp:2{prop:FontColor} = 65793
        ?repair_type_warranty_temp:2{prop:Color} = 16777215
    End ! If ?repair_type_warranty_temp:2{prop:Req} = True
    ?repair_type_warranty_temp:2{prop:Trn} = 0
    ?repair_type_warranty_temp:2{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
CheckRequiredFields     Routine
    If ForceTransitType('B')
        ?Transit_Type_Temp{prop:color} = 0BDFFFFH
        ?Transit_Type_Temp{prop:Req} = 1
    Else !If ForceTransitType('B')
        ?Transit_Type_Temp{prop:color} = color:white
        ?Transit_Type_Temp{prop:Req} = 0
    End !If ForceTransitType('B')

    If ForceIMEI('B')
        ?ESN_Temp{prop:color} = 0BDFFFFH
        ?ESN_Temp{prop:Req} = 1
        ?ESN_Temp:2{prop:color} = 0BDFFFFH
        ?ESN_Temp:2{prop:Req} = 1

    Else !If ForceIMEI('B')
        ?ESN_Temp{prop:color} = color:white
        ?ESN_Temp{prop:Req} = 0
        ?ESN_Temp:2{prop:color} = color:white
        ?ESN_Temp:2{prop:Req} = 0
    End !If ForceIMEI('B')

    ! Start Change 2941 BE(06/11/03)
    !If ForceMSN(job:Manufacturer,'B')
    If ForceMSN(job:Manufacturer,job:Workshop,'B')
    ! End Change 2941 BE(06/11/03)
        ?MSN_temp{prop:color} = 0BDFFFFH
        ?MSN_temp{prop:Req} = 1
    Else !If ForceMSN('B')
        ?MSN_temp{prop:color} = color:white
        ?MSN_temp{prop:Req} = 0
    End !If ForceMSN('B')

    If ForceModelNumber('B')
        ?Model_Number_Temp{prop:color} = 0BDFFFFH
        ?Model_Number_Temp{prop:Req} = 1
        ?Model_Number_Temp:2{prop:color} = 0BDFFFFH
        ?Model_Number_Temp:2{prop:Req} = 1
    Else !If ForceModelNumber('B')
        ?Model_Number_Temp{prop:color} = color:white
        ?Model_Number_Temp{prop:Req} = 0
        ?Model_Number_Temp:2{prop:color} = color:white
        ?Model_Number_Temp:2{prop:Req} = 0
    End !If ForceModelNumber('B')

    If ForceUnitType('B')
        ?Unit_Type_Temp{prop:color} = 0BDFFFFH
        ?Unit_Type_Temp{prop:Req} = 1
        ?Unit_Type_Temp:2{prop:color} = 0BDFFFFH
        ?Unit_Type_Temp:2{prop:Req} = 1
    Else !If ForceUnitType{'B')
        ?Unit_Type_Temp{prop:color} = color:white
        ?Unit_Type_Temp{prop:Req} = 0
        ?Unit_Type_Temp:2{prop:color} = color:white
        ?Unit_Type_Temp:2{prop:Req} = 0
    End !If ForceUnitType{'B')

    If ForceColour('B')
        ?Colour_Temp{prop:color} = 0BDFFFFH
        ?Colour_Temp{prop:Req} = 1
    Else !If ForceColour('B')
        ?Colour_Temp{prop:color} = color:white
        ?Colour_Temp{prop:Req} = 0
    End !If ForceColour('B')

    If ForceDOP(Transit_Type_Temp,Manufacturer_Temp,'YES','B')
        ?tmp:DOP{prop:color} = 0BDFFFFH
        ?tmp:DOP{prop:Req} = 1
        ?tmp:DOP:2{prop:color} = 0BDFFFFH
        ?tmp:DOP:2{prop:Req} = 1
    Else !If ForceDOP(job:Transit_Type,job:Manufacturer,job:Warranty_Job,'B')
        ?tmp:DOP{prop:color} = color:white
        ?tmp:DOP{prop:Req} = 0
        ?tmp:DOP:2{prop:color} = color:white
        ?tmp:DOP:2{prop:Req} = 0
    End !If ForceDOP(job:Transit_Type,job:Manufacturer,job:Warranty_Job,'B')

    If ForceMobileNumber('B')
        ?Mobile_Number_Temp:2{prop:color} = 0BDFFFFH
        ?Mobile_Number_Temp:2{prop:Req} = 1
    Else !If ForceMobileNumber('B')
        ?Mobile_Number_Temp:2{prop:color} = color:white
        ?Mobile_Number_Temp:2{prop:Req} = 0
    End !If ForceMobileNumber('B')

    If ForceLocation(Transit_Type_Temp,'YES','B')
        ?Internal_Location_Temp{prop:color} = 0BDFFFFH
        ?Internal_Location_Temp{prop:Req} = 1
        ?Internal_Location_Temp:2{prop:color} = 0BDFFFFH
        ?Internal_Location_Temp:2{prop:Req} = 1
    Else !If ForceLocation(job:Transit_Type,job:Workshop,'B')
        ?Internal_Location_Temp{prop:color} = color:white
        ?Internal_Location_Temp{prop:Req} = 0
        ?Internal_Location_Temp:2{prop:color} = color:white
        ?Internal_Location_Temp:2{prop:Req} = 0
    End !If ForceLocation(job:Transit_Type,job:Workshop,'B')

    If ForceOrderNumber(Account_Number_Temp,'B')
        ?Order_Number_Temp{prop:color} = 0BDFFFFH
        ?Order_Number_Temp{prop:Req} = 1
        ?Order_Number_Temp:2{prop:color} = 0BDFFFFH
        ?Order_Number_Temp:2{prop:Req} = 1
    Else !If ForceOrderNumber(job:Account_Number,'B')
        ?Order_Number_Temp{prop:color} = color:white
        ?Order_Number_Temp{prop:Req} = 0
        ?Order_Number_Temp:2{prop:color} = color:white
        ?Order_Number_Temp:2{prop:Req} = 0
    End !If ForceOrderNumber(job:Account_Number,'B')

    If ForceCustomerName(Account_Number_Temp,'B')
        ?Initial_Temp{prop:color} = 0BDFFFFH
        ?Surname_Temp{prop:color} = 0BDFFFFH
        ?Title_Temp{prop:color} = 0BDFFFFH
        ?Surname_Temp{prop:Req} = 1
    Else !If ForceCustomerName(job:Account_Number,'B')
        ?Initial_Temp{prop:color} = color:white
        ?Surname_Temp{prop:color} = color:white
        ?Title_Temp{prop:color} = color:white
        ?Surname_Temp{prop:Req} = 0
    End !If ForceCustomerName(job:Account_Number,'B')

    ! Start Change 2645 BE(17/06/03)
    IF (ForceFaultDescription('B')) THEN
        ?Fault_Description_Temp{prop:color} = 0BDFFFFH
        ?Fault_Description_Temp{prop:Req} = 1
        ?Fault_Description_Temp:2{prop:color} = 0BDFFFFH
        ?Fault_Description_Temp:2{prop:Req} = 1
    ELSE
        ?Fault_Description_Temp{prop:color} = color:white
        ?Fault_Description_Temp{prop:Req} = 0
        ?Fault_Description_Temp:2{prop:color} = color:white
        ?Fault_Description_Temp:2{prop:Req} = 0
    END
    ! Start Change 2645 BE(17/06/03)

    ! Start Change 4765 BE(26/10/2004)
    IF (ForceIncomingCourier('B')) THEN
        ?Consignment_Number_Temp{prop:color} = 0BDFFFFH
        ?Consignment_Number_Temp{prop:Req} = 1
        ?Consignment_Date_Temp{prop:color} = 0BDFFFFH
        ?Consignment_Date_Temp{prop:Req} = 1
    ELSE
        ?Consignment_Number_Temp{prop:color} = color:white
        ?Consignment_Number_Temp{prop:Req} = 0
        ?Consignment_Date_Temp{prop:color} = color:white
        ?Consignment_Date_Temp{prop:Req} = 0
    END
    ! End Change 4765 BE(26/10/2004)

    ! Cannot insert multiple jobs if you are not allowed to book "live" bouncers - TrkBs: 6503 (DBH: 24-10-2005)
    If Clip(GETINI('BOUNCER','StopLive',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
        ?Insert_Multiple{Prop:Hide} = True
    Else     ! If Clip(GETINI('BOUNCER','StopLive',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
        ?Insert_Multiple{Prop:Hide} = False
    End ! If Clip(GETINI('BOUNCER','StopLive',,CLIP(PATH())&'\SB2KDEF.INI')) = 1

Clear_Fields        Routine
    If order_number_tick_temp <> 'Y'
        order_number_temp = ''
    End
    If internal_location_tick_temp <> 'Y'
        internal_location_temp = ''
    End
    If mobile_number_tick_temp <> 'Y'
        mobile_number_temp = ''
    End
    If model_number_tick_temp <> 'Y'
        model_number_temp = ''
        manufacturer_temp = ''
    End
    If unit_type_tick_temp <> 'Y'
        unit_type_temp = ''
        enable(?lookup_unit_Type:2)
    End
    If esn_tick_temp <> 'Y'
        esn_temp = ''
    End
    If msn_tick_temp <> 'Y'
        msn_temp = ''
    End
    If fault_description_tick_temp <> 'Y'
        fault_description_temp = ''
    End
    If intermittent_fault_tick_temp <> 'Y'
        intermittent_fault_temp = ''
    End
    If physical_damage_tick_temp <> 'Y'
        physical_damage_temp = ''
    End
    If charge_type_tick_temp <> 'Y'
        charge_type_temp = ''
    End
    If warranty_job_tick_temp <> 'Y'
        warranty_charge_type_temp = ''
    End
    If repair_type_tick_temp <> 'Y'
        repair_type_temp = ''
    End

    If repair_type_warranty_tick_temp <> 'Y'
        repair_type_warranty_temp = ''
    End !If repair_type_warranty_tick_temp <> 'Y'


    If tmp:DOPTick <> 1
        tmp:dop = ''
    End !If tmp:DOPTick <> 1

    ! Start Change 4765 BE(26/10/2004)
    IF (Consignment_Number_Tick_Temp <> 1) THEN
        Consignment_Number_Temp = ''
    END

    IF (Consignment_Date_Tick_Temp <> 1) THEN
        Consignment_Date_Temp = ''
    END
    ! End Change 4765 BE(26/10/2004)

    ?BouncerText{prop:Text} = ''
Check_MSN   Routine
        access:modelnum.clearkey(mod:model_number_key)
        mod:model_number = model_number_temp
        if access:modelnum.fetch(mod:model_number_key) = Level:benign
            access:manufact.clearkey(man:manufacturer_key)
            man:manufacturer    = mod:manufacturer
            If access:manufact.tryfetch(man:manufacturer_key) = Level:Benign
                If man:use_msn <> 'YES'
                    msn_temp = ''
                    Hide(?msn_temp)
                    Hide(?msn:prompt)
                Else!If man:use_msn <> 'YES'
                    Unhide(?msn_temp)
                    Unhide(?msn:prompt)
                    If (Len(Clip(msn_temp)) < mod:msn_length_from Or Len(Clip(msn_temp)) > mod:msn_length_to) and msn_temp <> ''
                        beep(beep:systemhand)  ;  yield()
                        message('The M.S.N. entered should be between '&|
                                clip(mod:msn_length_from)&' And '&clip(mod:msn_length_to)&' characters '&|
                                'in length.', |
                                'ServiceBase 2000', icon:hand)
                        Select(?msn_temp)
                        error_temp = 1
                    End !If Len(job:esn) < mod:esn_length_from Or Len(job:esn) > mod:esn_length_to
                End!If man:use_msn <> 'YES'
            End!If access:manufact.tryfetch(man:manufacturer_key) = Level:Benign
            

        end !if access:modelnum.fetch(mod:model_number_key) = Level:benign
Check_For_Despatch        Routine
    Set(defaults)
    access:defaults.next()

    despatch# = 0

    if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'
        despatch# = 1
    Else!if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        if access:subtracc.fetch(sub:account_number_key) = Level:Benign
            access:tradeacc.clearkey(tra:account_number_key)
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                If tra:use_sub_accounts = 'YES'
                    If job:chargeable_job = 'YES'
                        If sub:despatch_invoiced_jobs <> 'YES' And sub:despatch_paid_jobs <> 'YES'
                            despatch# = 1
                        End!If sub:despatch_invoiced_jobs <> 'YES'
                    Else!If job:chargeable_job = 'YES'
                        despatch# = 1
                    End!If job:chargeable_job = 'YES'
                Else!If tra:use_sub_accounts = 'YES'
                    If job:chargeable_job = 'YES'
                        If tra:despatch_invoiced_jobs <> 'YES' And tra:despatch_paid_jobs <> 'YES'
                            despatch# = 1
                        End!If sub:despatch_invoiced_jobs <> 'YES'
                    Else!If job:chargeable_job = 'YES'
                        despatch# = 1
                    End!If job:chargeable_job = 'YES'
                End!If tra:use_sub_accounts = 'YES'
            end!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
        end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign
    End!if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'

    If despatch# = 1
        If job:despatched <> 'YES'
            job:despatched = 'REA'
            job:despatch_type = 'JOB'
            job:current_courier = job:courier
        End!If job:despatched <> 'YES'
    End
QA_Group        Routine
    Set(defaults)
    access:defaults.next()
    If def:qa_required = 'YES' And job:date_completed <> ''
!        Unhide(?qa_group)
    Else
!        Hide(?qa_group)
    End
check_force_fault_codes    Routine
    force_fault_codes_temp = 'NO'
    If job:chargeable_job = 'YES'
        required# = 0
        access:chartype.clearkey(cha:charge_type_key)
        cha:charge_type = job:charge_type
        If access:chartype.fetch(cha:charge_type_key) = Level:Benign
            If cha:force_warranty = 'YES'
                force_fault_codes_temp = 'YES'
            End
        end !if
    End!If job:chargeable = 'YES'
    If job:warranty_job = 'YES' And force_fault_codes_temp <> 'YES'
        access:chartype.clearkey(cha:charge_type_key)
        cha:charge_type = job:warranty_charge_type
        If access:chartype.fetch(cha:charge_type_key) = Level:Benign
            If cha:force_warranty = 'YES'
                force_fault_codes_temp = 'YES'
            End
        end !if
    End!If job:warranty_job = 'YES'
Select_Fields       Routine
    select# = 0

    If tmp:doptick = 1
        ?tmp:dop:2{prop:skip} = 1
        ?tmp:dop:2{prop:readonly} = 1
        ?tmp:dop:2{prop:color} = color:gray
    Else
        select# = 12
    End!If tmp:doptick = 1
    If repair_type_Warranty_tick_temp = 'Y'
        ?repair_type_warranty_temp:2{prop:skip} = 1
        ?repair_type_warranty_temp:2{prop:readonly} = 1
        ?repair_type_warranty_temp:2{prop:color} = color:gray
        ?lookup_warranty_repair_type{prop:skip} = 1
    Else
        select# = 11
    End!If repair_type_Warraty_tick_temp = 'Y'
    If warranty_job_tick_temp = 'Y'
        ?lookup_warranty_charge_type:2{prop:skip} = 1
        ?lookup_warranty_charge_type:2{prop:readonly} = 1
        ?warranty_charge_type_temp:2{prop:skip} = 1
        ?warranty_charge_type_temp:2{prop:color} = color:gray
    Else
        select# = 10
    End
    If repair_type_tick_temp = 'Y'
        ?repair_type_temp:2{prop:skip} = 1
        ?repair_type_temp:2{prop:readonly} = 1
        ?lookup_repair_type:2{prop:skip} = 1
        ?repair_type_temp:2{prop:color} = color:gray
    Else
        select# = 9
    End
    If charge_type_tick_temp = 'Y'
        ?charge_type_temp:2{prop:skip} = 1
        ?charge_type_temp:2{prop:readonly} = 1
        ?lookup_charge_type:2{prop:skip} = 1
        ?charge_type_temp:2{prop:color} = color:gray
    Else
        select# = 8
    End
    If fault_description_tick_temp = 'Y'
        ?fault_description_temp:2{prop:skip} = 1
        ?fault_description_temp:2{prop:readonly} = 1
        ?fault_description_text_2{prop:skip} = 1
        ?fault_description_temp:2{prop:color} = color:gray
    Else
        select# = 7
    End
    If internal_location_tick_temp = 'Y'
        ?internal_location_temp:2{prop:skip} = 1
        ?internal_location_temp:2{prop:readonly} = 1
        ?lookup_internal_location:2{prop:skip} = 1
        ?internal_location_temp:2{prop:color} = color:gray
    Else
        select# = 6
    End
    If order_number_tick_temp = 'Y'
        ?order_number_temp:2{prop:skip} = 1
        ?order_number_temp:2{prop:readonly} = 1
        ?order_number_temp:2{prop:color} = color:gray
    Else
        select# = 5
    End
    If unit_type_tick_temp = 'Y'
        ?unit_type_temp:2{prop:skip} = 1
        ?unit_type_temp:2{prop:readonly} = 1
        ?lookup_unit_Type:2{prop:skip} = 1
        ?unit_type_temp:2{prop:color} = color:gray
    Else
        select# = 4
    End

    If model_number_tick_temp = 'Y'
        ?model_number_temp:2{prop:skip} = 1
        ?model_number_temp:2{prop:readonly} = 1
        ?lookup_model_number:2{prop:skip} = 1
        ?model_number_temp:2{prop:color} = color:gray
    Else
        select# = 3
    End

    If msn_tick_temp = 'Y'
        ?msn_temp{prop:skip} = 1
        ?msn_temp{prop:readonly} = 1
        ?msn_temp{prop:color} = color:gray
    Else
        select# = 2
    End

    If esn_tick_temp = 'Y'
        ?esn_temp:2{prop:skip} = 1
        ?esn_temp:2{prop:readonly} = 1
        ?esn_temp:2{prop:color} = color:gray
    else
        select# = 1
    End

    Case select#
        Of 1
            Select(?esn_temp:2)
        Of 2
            Select(?msn_temp)
        Of 3
            Select(?model_number_temp:2)
        Of 4
            Select(?unit_Type_temp:2)
        Of 5
            Select(?order_number_temp:2)
        Of 6
            Select(?internal_location_temp:2)
        Of 7
            Select(?fault_description_temp:2)
        Of 8
            Select(?charge_Type_temp:2)
        Of 9
            Select(?repair_type_temp:2)
        Of 10
            Select(?warranty_charge_type_temp:2)
        Of 11
            Select(?repair_type_warranty_temp:2)
        of 12
            Select(?tmp:dop:2)
    End!Case select#
Transit_type_bit        Routine
    access:trantype.clearkey(trt:transit_type_key)
    trt:transit_type    = transit_type_temp
    If access:trantype.fetch(trt:transit_type_key) = Level:Benign
        If trt:collection_address <> 'YES'
            Clear(address_collection_group)
            Hide(?collection_address_group)
        Else
            Unhide(?collection_address_group)
        End
        If trt:delivery_address <> 'YES'
            Clear(address_delivery_group)
            Hide(?delivery_address_group)
        Else
            Unhide(?delivery_address_group)
        End
    End
    If trt:location <> 'YES'
        !Hide(?location_group)
        ?Internal_Location_Temp{prop:req} = 0
        ?Internal_Location_Temp:2{prop:req} = 0
    Else
        !If job:workshop = 'YES'
            !Unhide(?location_group)
        !End
        If trt:force_location = 'YES' !And job:workshop = 'YES'
            ?Internal_Location_Temp{prop:req} = 1
            ?Internal_Location_Temp:2{prop:req} = 1
        Else!If trt:force_location = 'YES'
            ?Internal_Location_Temp{prop:req} = 0
            ?Internal_Location_Temp:2{prop:req} = 0
        End!If trt:force_location = 'YES'
    End
Trade_Account_Bit       Routine
    stop# = 0
    if sub:stop_account = 'YES'
        stop# = 1
    Else !if sub:stop:account = 'YES'
        tra:account_number    = sub:main_account_number
        if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
            If tra:stop_account = 'YES'
                stop# = 1
            Else !If tra:stop_account = 'YES'

                If tra:use_sub_accounts <> 'YES'
                    If tra:use_customer_address <> 'YES'
                        Postcode        = tra:postcode
                        Company_Name    = tra:company_name
                        Address_Line1   = tra:address_Line1
                        Address_Line2   = tra:address_Line2
                        Address_Line3   = tra:address_line3
                        Telephone_Number= tra:telephone_number
                        Fax_Number      = tra:fax_number
                    End!If tra:use_customer_address <> 'YES'
                    
                    If tra:use_delivery_address = 'YES'
                        postcode_delivery   = tra:postcode
                        company_name_delivery   = tra:company_name
                        address_line1_delivery  = tra:address_line1
                        address_line2_delivery  = tra:address_line2
                        address_line3_delivery  = tra:address_line3
                        telephone_delivery      = tra:telephone_number
                    End !If tra:use_delivery_address = 'YES'
                    If tra:use_collection_address = 'YES'
                        job:postcode_collection = tra:postcode
                        job:company_name_collection = tra:company_Name
                        job:address_line1_collection    = tra:address_line1
                        job:address_line2_collection    = tra:address_line2
                        job:address_line3_collection    = tra:address_line3
                        job:telephone_collection    = tra:telephone_number
                    End!If tra:use_collection_address = 'YES'

                Else!If tra:use_sub_accounts <> 'YES'
                    If tra:invoice_sub_accounts = 'YES'
                        If sub:use_customer_address <> 'YES'
                            Postcode        = sub:postcode
                            Company_Name    = sub:company_name
                            Address_Line1   = sub:address_Line1
                            Address_Line2   = sub:address_Line2
                            Address_Line3   = sub:address_line3
                            Telephone_Number= sub:telephone_number
                            Fax_Number      = sub:fax_number
                        End!If sub:use_customer_address <> 'YES'
                    Else!If tra:invoice_sub_accounts = 'YES'
                        If tra:use_customer_address <> 'YES'
                            Postcode        = tra:postcode
                            Company_Name    = tra:company_name
                            Address_Line1   = tra:address_Line1
                            Address_Line2   = tra:address_Line2
                            Address_Line3   = tra:address_line3
                            Telephone_Number= tra:telephone_number
                            Fax_Number      = tra:fax_number
                        End!If tra:use_customer_address <> 'YES'
                    End!If tra:invoice_sub_accounts = 'YES'
                    If sub:use_delivery_address = 'YES'
                        postcode_delivery      = sub:postcode
                        company_name_delivery  = sub:company_name
                        address_line1_delivery = sub:address_line1
                        address_line2_delivery = sub:address_line2
                        address_line3_delivery = sub:address_line3
                        telephone_delivery     = sub:telephone_number
                    End!If sub:use_delivery_address = 'YES'
                    If tra:use_collection_address = 'YES'
                        job:postcode_collection = sub:postcode
                        job:company_name_collection = sub:company_Name
                        job:address_line1_collection    = sub:address_line1
                        job:address_line2_collection    = sub:address_line2
                        job:address_line3_collection    = sub:address_line3
                        job:telephone_collection    = sub:telephone_number
                    End!If tra:use_collection_address = 'YES'

                End!If tra:use_sub_accounts <> 'YES'
            End !If tra:stop_account = 'YES'
        End !if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
    End !if sub:stop:account = 'YES'

    !Stop
    If stop# = 1
        Case MessageEx('The selected Trade Account is On Stop.'&|
          '<13,10>'&|
          '<13,10>Please report this issue to your System Supervisor Immediately.','ServiceBase 2000',|
                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            Of 1 ! &OK Button
        End!Case MessageEx
        Account_Number_Temp = ''
        Display()
    Else
        Do Trade_account_Status
    End
Check_Unit_Type     Routine
    If mod:specify_unit_type = 'YES'
        unit_type_tick_temp = 'Y'
        unit_type_temp = mod:unit_type
        Disable(?unit_type_tick_temp)
        Disable(?unit_type_temp)
        Disable(?lookup_unit_type)
        Unhide(?unit_type_temp)
    Else
        Enable(?unit_Type_tick_temp)
        Enable(?unit_type_temp)
        Enable(?lookup_unit_type)
    End

    access:manufact.clearkey(man:manufacturer_key)
    man:manufacturer    = manufacturer_temp
    If access:manufact.fetch(man:manufacturer_key) = Level:Benign
        If man:use_msn <> 'YES'
            msn_temp = ''
            Hide(?msn_temp)
            Hide(?msn:prompt)
        End
    End
    Display()

Check_Unit_Type:2       Routine
    If mod:specify_unit_type = 'YES'
        unit_type_temp = mod:unit_type
        Disable(?unit_type_temp:2)
        Disable(?lookup_unit_type:2)
    Else
        Enable(?lookup_unit_type:2)
        Enable(?unit_type_temp:2)
    End

    access:manufact.clearkey(man:manufacturer_key)
    man:manufacturer    = manufacturer_temp
    If access:manufact.fetch(man:manufacturer_key) = Level:Benign
        If man:use_msn <> 'YES'
            msn_temp = ''
            Hide(?msn_temp)
            Hide(?msn:prompt)
        End
    End
    Display()

Create_Job      Routine
    tmp:JobCreated = 0
    get(jobs,0)
    if access:jobs.primerecord() = level:benign
        !call the status routine
        GetStatus(0,1,'JOB')
        job:batch_number                  = batch_number_temp
        job:order_number                  = order_number_temp
        job:location                      = internal_location_temp
        If internal_location_temp <> ''
            access:locinter.clearkey(loi:location_key)
            loi:location    = internal_location_temp
            If access:locinter.fetch(loi:location_key) = Level:Benign
                If loi:allocate_spaces = 'YES'
                    If loi:current_spaces <= 0
                        Case MessageEx('There are no more spaces available for the selected Internal Location.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        access:jobs.cancelautoinc()
                        Select(?internal_location_temp:2)
                        Exit
                    Else   !If loi:current_spaces <= 0
                        loi:current_spaces -= 1
                        If loi:current_spaces <= 0
                            loi:current_spaces = 0
                            loi:location_available = 'NO'
                        End
                        access:locinter.update()
                    End   !If loi:current_spaces <= 0
                End
                !call the status routine
                job:workshop                      = 'YES'
            End
        Else
            job:workshop = ''
        End

        job:mobile_number                 = mobile_number_temp
        job:model_number                  = model_number_temp
        access:modelnum.clearkey(mod:model_number_key)
        mod:model_number = job:model_number
        if access:modelnum.fetch(mod:model_number_key) = Level:Benign
            If mod:product_Type <> ''
                job:fault_code1 = mod:product_type
            End!If job:product_Type <> ''
            job:Manufacturer    = mod:Manufacturer
        end
        job:ProductCode                 = tmp:ProductCode
        job:colour                        = colour_temp
        job:unit_type                     = unit_type_temp
        job:esn                           = esn_temp
        If msn_temp <> ''
            job:msn                       = msn_temp
        Else!If msn_temp <> ''
            job:msn                       = 'N/A'
        End!If msn_temp <> ''
        If intermittent_fault_temp
            job:intermittent_fault            = intermittent_fault_temp
        Else
            job:intermittent_fault = 'NO'
        End
        If physical_damage_temp
            job:physical_damage               = physical_damage_temp
        Else
            job:physical_damage = 'NO'
        End
        If charge_type_temp <> ''
            job:chargeable_job = 'YES'
            job:charge_type                   = charge_type_temp
            job:repair_type                   = repair_type_temp
        End
        If warranty_charge_type_temp <> ''
            job:warranty_job = 'YES'
            job:warranty_charge_type          = warranty_charge_Type_temp
            job:repair_type_warranty          = repair_type_warranty_temp
        End

        ! Start Change 4765 BE(26/10/2004)
        IF (Consignment_Number_Temp <> '') THEN
            job:Incoming_Consignment_Number = Consignment_Number_Temp
        END

        IF (Consignment_Date_Temp <> '') THEN
            job:Incoming_Date = Consignment_Date_Temp
        END
        ! End Change 4765 BE(26/10/2004)

        job:account_number                = account_number_temp
        despatch# = 0
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        if access:subtracc.fetch(sub:account_number_key) = level:benign
            access:tradeacc.clearkey(tra:account_number_key) 
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = level:benign
                IF tra:invoice_sub_accounts = 'YES'
                    job:courier_cost        = sub:courier_cost
                    job:courier_cost_warranty = sub:courier_cost
                Else!IF tra:invoice_sub_accounts = 'YES'
                    job:courier_cost        = tra:courier_cost
                    job:courier_cost_warranty = sub:courier_cost
                End!IF tra:invoice_sub_accounts = 'YES'
                if tra:use_sub_accounts = 'YES'
                    job:courier = sub:courier_outgoing
                    job:incoming_courier = sub:courier_incoming
                    job:exchange_courier = job:courier
                    job:loan_courier = job:courier
                    If sub:despatch_paid_jobs <> 'YES' and sub:despatch_invoiced_jobs <> 'YES'
                        despatch# = 1
                    End!If sub:despatch_paid_jobs <> 'YES' and sub:despatch_invoiced_jobs <> 'YES'
                else!if tra:use_sub_accounts = 'YES'
                    job:courier = tra:courier_outgoing
                    job:incoming_courier = tra:courier_incoming
                    job:exchange_courier = job:courier
                    job:loan_courier = job:courier
                    If tra:despatch_paid_jobs <> 'YES' and tra:despatch_invoiced_jobs <> 'YES'
                        despatch# = 1
                    End!If tra:despatch_paid_jobs <> 'YES' and tra:despatch_invoiced_jobs <> 'YES'
                end!if tra:use_sub_accounts = 'YES'
                If tra:Force_Estimate = 'YES'
                    job:Estimate_If_Over    = tra:Estimate_If_Over
                    job:Estimate            = 'YES'
                End!If tra:Force_Estimate = 'YES'
            end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
        end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
        job:transit_type                  = transit_type_temp

        access:trantype.clearkey(trt:transit_type_key)
        trt:transit_type = job:transit_type
        if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
            IF trt:InWorkshop = 'YES'
                job:workshop = 'YES'
            End!IF trt:InWorkshop = 'YES'
            GetStatus(Sub(trt:initial_status,1,3),1,'JOB')

            GetStatus(Sub(trt:exchangestatus,1,3),1,'EXC')

            GetStatus(Sub(trt:loanstatus,1,3),1,'LOA')
            If job:Location = '' And trt:InternalLocation <> ''
                job:Location    = trt:InternalLocation
            End !If job:Location = '' And trt:InternalLocation <> ''
        end!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
        job:dop                           = tmp:dop
        JOB:Postcode                      = postcode
        JOB:Company_Name                  = company_name
        JOB:Address_Line1                 = Address_Line1
        JOB:Address_Line2                 = Address_Line2
        JOB:Address_Line3                 = Address_Line3
        JOB:Telephone_Number              = Telephone_Number
        JOB:Fax_Number                    = Fax_Number

        JOB:Postcode_Collection           = Postcode_Collection
        JOB:Company_Name_Collection       = Company_Name_Collection
        JOB:Address_Line1_Collection      = Address_Line1_Collection
        JOB:Address_Line2_Collection      = Address_Line2_Collection
        JOB:Address_Line3_Collection      = Address_Line3_Collection
        JOB:Telephone_Collection          = Telephone_Collection

        JOB:Postcode_Delivery             = Postcode_Delivery
        JOB:Address_Line1_Delivery        = Address_Line1_Delivery
        JOB:Company_Name_Delivery         = Company_Name_Delivery
        JOB:Address_Line2_Delivery        = Address_Line2_Delivery
        JOB:Address_Line3_Delivery        = Address_Line3_Delivery
        JOB:Telephone_Delivery            = Telephone_Delivery

        access:users.clearkey(use:password_key)
        use:password    =glo:password
        If access:users.fetch(use:password_key) = Level:Benign
            job:who_booked                    = use:user_code
        End
        job:title                         = title_temp
        job:initial                       = initial_temp
        job:surname                       = surname_temp

        If job:warranty_job = 'YES'
            Case job:manufacturer
                Of 'ALCATEL'
                        job:edi = 'EDI'
                Of 'BOSCH'
                        job:edi = 'EDI'
                Of 'ERICSSON'
                        job:edi = 'EDI'
                Of 'MAXON'
                        job:edi = 'EDI'
                Of 'MOTOROLA'
                        job:edi = 'EDI'
                Of 'NEC'
                        job:edi = 'EDI'
                Of 'NOKIA'
                        job:edi = 'EDI'
                Of 'SIEMENS'
                        job:edi = 'EDI'
                Of 'SAMSUNG'
                        job:edi = 'EDI'
                Of 'MITSUBISHI'
                        job:edi = 'EDI'
                Of 'TELITAL'
                        job:edi = 'EDI'
                OF 'SAGEM'
                        job:edi = 'EDI'
                OF 'PANASONIC'
                        job:edi = 'EDI'
                OF 'SONY'
                        job:edi = 'EDI'
                Else
                    job:edi = 'XXX'
            End
        end
        If date_completed_temp <> ''

            job:time_completed = Clock()
            job:date_completed = date_completed_temp
            job:completed   = 'YES'
            If job:edi = 'EDI'
                access:chartype.clearkey(cha:charge_type_key)
                cha:charge_type = job:warranty_charge_type
                if access:chartype.fetch(cha:charge_type_key)
                    job:edi = 'NO'
                Else!if access:chartype.fetch(cha:charge_type_key)
                    If cha:exclude_edi <> 'YES'
                        job:edi = 'NO'
                    End
                end!if access:chartype.fetch(cha:charge_type_key)
            End!If job:edi = 'EDI'
            !call the status routine
            GetStatus(705,1,'JOB') !completed

            If def:qa_required <> 'YES'
                If despatch# = 1
                    job:current_courier = job:courier
                    job:despatch_Type = 'JOB'
                    job:despatched = 'REA'
                    !call the status routine
                    GetStatus(810,1,'JOB') !ready to despatch

                End!If despatch# = 1
            End!If def:qa_required <> 'YES'
            check_for_bouncers_temp = 1
            saved_ref_number_temp   = job:ref_number
            saved_esn_temp  = job:esn
            saved_msn_temp  = job:msn
        End!If date_completed_temp <> ''
!Accessories
        save_jactmp_id = access:jobactmp.savefile()
        set(jactmp:accessory_key)
        loop
            if access:jobactmp.next()
               break
            end !if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            get(jobacc,0)
            if access:jobacc.primerecord() = Level:Benign
                jac:ref_number = job:ref_number
                jac:accessory  = jactmp:accessory
                access:jobacc.tryinsert()
            end!if access:jobacc.primerecord() = Level:Benign
        end !loop
        access:jobactmp.restorefile(save_jactmp_id)

        save_jactmp_id = access:jobactmp.savefile()
        set(jactmp:accessory_key)
        loop
            if access:jobactmp.next()
               break
            end !if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            Delete(jobactmp)
        end !loop
        access:jobactmp.restorefile(save_jactmp_id)

!Fault Codes
        job:Fault_Code1         =  glo:FaultCode1
        job:Fault_Code2         =  glo:FaultCode2
        job:Fault_Code3         =  glo:FaultCode3
        job:Fault_Code4         =  glo:FaultCode4
        job:Fault_Code5         =  glo:FaultCode5
        job:Fault_code6         =  glo:FaultCode6
        job:Fault_Code7         =  glo:FaultCode7
        job:Fault_code8         =  glo:FaultCode8
        job:Fault_Code9         =  glo:FaultCode9
        job:Fault_Code10        =  glo:FaultCode10
        job:Fault_Code11        =  glo:FaultCode11
        job:Fault_Code12        =  glo:FaultCode12


        access:jobnotes.primerecord()
        jbn:refnumber  = job:ref_number
        jbn:fault_description             = fault_description_temp
        !Check for estimate
        If job:Estimate = 'YES' and job:Chargeable_Job = 'YES'
            ! Start Change 3922 BE(02/03/04)
            !GetStatus(505,1,'JOB')
            Total_Price('E', vat", total", balance")
            Total_Price('C', vat2", total2", balance2")
            total$ = total" + total2"
            IF ((job:estimate_if_over = 0.0) OR (total$ > job:estimate_if_over)) THEN
                GetStatus(505,1,'JOB')
            END
            ! End Change 3922 BE(02/03/04)
        End !If job:Estimate = 'YES' and job:Chargeable_Job = 'YES'
        access:jobnotes.insert()

        !Jobse prime!
        get(jobse,0)
        if access:jobse.primerecord() = level:benign
          jobe:RefNumber = job:Ref_Number
          jobe:FaultCode13        =  glo:FaultCode13
          jobe:FaultCode14        =  glo:FaultCode14
          jobe:FaultCode15        =  glo:FaultCode15
          jobe:FaultCode16        =  glo:FaultCode16
          jobe:FaultCode17        =  glo:FaultCode17
          jobe:FaultCode18        =  glo:FaultCode18
          jobe:FaultCode19        =  glo:FaultCode19
          jobe:FaultCode20        =  glo:FaultCode20
          jobe:TraFaultCode1      =  glo:TradeFaultCode1
          jobe:TraFaultCode2      =  glo:TradeFaultCode2
          jobe:TraFaultCode3      =  glo:TradeFaultCode3
          jobe:TraFaultCode4      =  glo:TradeFaultCOde4
          jobe:TraFaultCOde5      =  glo:TradeFaultCode5
          jobe:TraFaultCode6      =  glo:TradeFaultCode6
          jobe:TraFaultCode7      =  glo:TradeFaultCode7
          jobe:TraFaultCode8      =  glo:TradeFaultCode8
          jobe:TraFaultCode9      =  glo:TradeFaultCode9
          jobe:TraFaultCode10     =  glo:TradeFaultCode10
          jobe:TraFaultCode11     =  glo:TradeFaultCode11
          jobe:TraFaultCode12     =  glo:TradeFaultCode12
          If job:Workshop = 'YES'
            jobe:InWorkshopDate = Today()
            jobe:InWorkshopTime = Clock()
          End !If job:Workshop = 'YES'
          ! Fill in Repair Completed Date/Time - TrkBs: 6502 (DBH: 21-10-2005)
          If date_completed_temp <> ''
              jobe:CompleteRepairType = 2
              jobe:CompleteRepairDate = TODAY()
              jobe:CompleteRepairTime = CLOCK()
          End ! If date_completed_temp <> ''
          if access:jobse.tryinsert()
            access:jobse.cancelautoinc()
          END
        END

        glo:ErrorText = ''
        CompulsoryFieldCheck('B')

        If glo:ErrorText <> ''
            glo:errortext = 'You cannot book the job due to the following error(s): <13,10>' & Clip(glo:errortext)
            Error_Text
            glo:errortext = ''
            !Delete Children
            Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
            jbn:RefNumber = job:Ref_Number
            If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
                !Found
                Delete(JOBNOTES)
            Else!If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                !Found
                Delete(JOBSE)
            Else!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
            Access:JOBS.Cancelautoinc()
        Else !If glo:ErrorText <> ''
            if access:jobs.tryinsert()
                !Delete Children
                Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
                jbn:RefNumber = job:Ref_Number
                If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
                    !Found
                    Delete(JOBNOTES)
                Else!If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = job:Ref_Number
                If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                    !Found
                    Delete(JOBSE)
                Else!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                access:jobs.cancelautoinc()
            Else!if access:jobs.tryinsert()
                Clear(glo:G_Select1)
                Clear(glo:G_Select1)
                tmp:JobCreated = 1
                get(audit,0)
                if access:audit.primerecord() = level:benign
                    aud:notes         = 'UNIT DETAILS: ' & Clip(job:manufacturer) & ' ' & Clip(job:model_number) & ' - ' & Clip(job:unit_type) & |
                                        '<13,10>E.S.N. / I.M.E.I.: ' & Clip(job:esn) & |
                                        '<13,10>M.S.N.: ' & Clip(job:msn)
                    If job:Chargeable_job = 'YES'
                        aud:Notes   = Clip(aud:Notes) & '<13,10>CHARGEABLE CHARGE TYPE: ' & Clip(job:Charge_Type)
                    End !If job:Chargeable_job = 'YES'
                    If job:Warranty_Job = 'YES'
                        aud:Notes   = Clip(aud:Notes) & '<13,10>WARRANTY CHARGE TYPE: ' & Clip(job:Warranty_Charge_Type)
                    End !If job:Warranty_Job = 'YES'
                    count# = 0
                    save_jac_id = access:jobacc.savefile()
                    access:jobacc.clearkey(jac:ref_number_key)
                    jac:ref_number = job:ref_number
                    set(jac:ref_number_key,jac:ref_number_key)
                    loop
                        if access:jobacc.next()
                           break
                        end !if
                        if jac:ref_number <> job:ref_number      |
                            then break.  ! end if
                        count# += 1
                        aud:notes = Clip(aud:notes) & '<13,10>ACCESSORY ' & Clip(count#) & ': ' & Clip(jac:accessory)
                    end !loop
                    access:jobacc.restorefile(save_jac_id)

                    aud:ref_number    = job:ref_number
                    aud:date          = Today()
                    aud:time          = Clock()
                    access:users.clearkey(use:password_key)
                    use:password =glo:password
                    access:users.fetch(use:password_key)
                    aud:user = use:user_code
                    aud:action        = 'MULTIPLE NEW JOB BOOKING INITIAL ENTRY'
                    if access:audit.insert()
                        access:audit.cancelautoinc()
                    end
                end!if access:audit.primerecord() = level:benign

                glo:Pointer = job:Ref_Number
                Add(glo:Queue)

                ! Start Change 3389 BE(11/11/03)
                IF (print_bookin_label_temp) THEN
                    Bookin_JobLabel(job:ref_number)
                END
                ! End Change 3389 BE(11/11/03)

                glo:select1 = job:Ref_number
                glo:preview = ''
                If print_job_card_temp = 'Y'
                    Job_Card
                End
                If print_job_receipt_temp = 'Y'
                    Job_Receipt
                End
                If print_despatch_notes_temp = 'Y'
                    Despatch_Note
                End
                If print_job_label_temp = 'YES'
                    if def:use_job_label = 'YES'
                        set(defaults)
                        access:defaults.next()
                        case def:label_printer_type
                            of 'TEC B-440 / B-442'
                                If job:bouncer = 'B'
                                    thermal_labels('SE')
                                Else
                                    thermal_labels('')
                                End
                            of 'TEC B-452'
                                thermal_labels_b452
                        end!case def:label_printer_type
                    end!if def:use_loan_exchange_label = 'YES'
                End
                glo:select1 = ''
                Do update_accessories
                Include('bouncer.inc')

            End!if access:jobs.tryinsert()
        End !if access:jobs.tryinsert()
    end!if access:jobs.primerecord() = level:benign
Trade_Account_Status        Routine  ! Trade Account Status Display
    Hide(?name_group)
    access:subtracc.clearkey(sub:account_number_key)
    sub:account_number = account_number_temp
    if access:subtracc.fetch(sub:account_number_key) = Level:Benign
        access:tradeacc.clearkey(tra:account_number_key)
        tra:account_number = sub:main_account_Number
        if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
            If tra:use_contact_name = 'YES'
                Unhide(?name_group)
            End
        end !if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
    end !if access:subtracc.fetch(sub:account_number_key) = Level:Benign


getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
Check_Defaults      Routine
!    error_temp = 0
!
!    if def:force_mobile_number = 'B' And error_temp = 0 and def:show_mobile_number <> 'YES'
!        if Mobile_Number_temp  = ''
!            beep(beep:systemhand)  ;  yield()
!                message('Before you can complete booking you must enter information in '&|
!                'the Mobile Number field.', |
!                'ServiceBase 2000', icon:hand)
!            select(?mobile_number_temp:2)
!            error_temp = 1
!        end ! if job:mobile_number  = '
!    end !if def:force_mobile_number = 'B'
!
!    if def:force_model_number = 'B' and error_temp = 0
!        if model_number_temp  = ''
!            beep(beep:systemhand)  ;  yield()
!                message('Before you can complete booking you must enter information in '&|
!                'the Model Number field.', |
!                'ServiceBase 2000', icon:hand)
!            select(?model_number_temp:2)
!            error_temp =1
!        end ! if job:model_number  = '
!    end !if def:force_model_number = 'B'
!
!    if def:force_unit_type = 'B' and error_temp = 0
!        if unit_type_temp  = ''
!            beep(beep:systemhand)  ;  yield()
!                message('Before you can complete booking you must enter information in '&|
!                'the Unit Type field.', |
!                'ServiceBase 2000', icon:hand)
!            select(?unit_type_temp:2)
!            error_temp = 1
!        end ! if job:unit_type  = '
!    end !if def:force_unit_type = 'B'
!
!    if def:force_fault_description = 'B' and error_temp = 0
!        if fault_description_temp  = ''
!            beep(beep:systemhand)  ;  yield()
!                message('Before you can complete booking you must enter information in '&|
!                'the Fault Description field.', |
!                'ServiceBase 2000', icon:hand)
!            select(?fault_description_temp:2)
!            error_temp = 1
!        end ! if job:fault_description  = '
!    end !if def:force_fault_description = 'B'
!
!    if def:force_esn = 'B' and error_temp = 0
!        if esn_temp  = ''
!            beep(beep:systemhand)  ;  yield()
!                message('Before you can complete booking you must enter information in '&|
!                'the E.S.N. / I.M.E.I. field.', |
!                'ServiceBase 2000', icon:hand)
!            select(?esn_temp:2)
!            error_temp = 1
!        end ! if job:esn  = '
!    end !if def:force_esn = 'B'
!
!    if def:force_msn = 'B' and error_temp = 0
!        access:manufact.clearkey(man:manufacturer_key)
!        man:manufacturer = job:manufacturer
!        if access:manufact.fetch(man:manufacturer_key) = Level:Benign
!            If man:use_msn = 'YES'
!                if msn_temp  = ''
!                    beep(beep:systemhand)  ;  yield()
!                        message('Before you can complete booking you must enter information in '&|
!                        'the M.S.N. field.', |
!                        'ServiceBase 2000', icon:hand)
!                    select(?msn_temp)
!                    error_temp = 1
!                end ! if job:msn  = '
!            End !If man:use_msn = 'YES'
!        end !if access:manufact.fetch(man:manufacturer_key) = Level:Benign
!    end !if def:force_msn = 'B'
!
!    if def:force_job_type = 'B' and error_temp = 0
!        if Charge_Type_temp  = '' And warranty_charge_type_temp = ''
!            beep(beep:systemhand)  ;  yield()
!                message('Before you can complete booking you must enter information in '&|
!                'the Job Type field.', |
!                'ServiceBase 2000', icon:hand)
!            error_temp = 1
!        end ! if job:job_type  = '
!    end !if def:force_job_type = 'B'
!
!    !Added Neil 11/07/01!
!    IF ?Internal_Location_Temp:2{prop:req} = 1
!      if Internal_Location_Temp  = ''
!            beep(beep:systemhand)  ;  yield()
!                message('Before you can complete booking you must enter information in '&|
!                'the Location field.', |
!                'ServiceBase 2000', icon:hand)
!            select(?Internal_Location_Temp:2)
!            error_temp = 1
!        end ! if job:repair_type  = '
!    end ! IF Internal_Location_Temp:2{prop:req} = 1
!
!
!
!    if def:force_repair_type = 'B' and error_temp = 0
!        if repair_type_temp  = '' And repair_type_warranty_temp = ''
!            beep(beep:systemhand)  ;  yield()
!                message('Before you can complete booking you must enter information in '&|
!                'the Repair Type field.', |
!                'ServiceBase 2000', icon:hand)
!            select(?repair_type_temp:2)
!            error_temp = 1
!        end ! if job:repair_type  = '
!    end !if def:force_repair_type = 'B'
!
!    If esn_temp <> ''
!    
!        access:modelnum.clearkey(mod:model_number_key)
!        mod:model_number = model_number_temp
!        if access:modelnum.fetch(mod:model_number_key) = Level:benign
!            If Len(Clip(esn_temp)) < mod:esn_length_from Or Len(Clip(esn_temp)) > mod:esn_length_to
!                beep(beep:systemhand)  ;  yield()
!                message('The E.S.N. / I.M.E.I. entered should be between '&|
!                        clip(mod:esn_length_from)&' And '&clip(mod:esn_length_to)&' characters '&|
!                        'in length.', |
!                        'ServiceBase 2000', icon:hand)
!                Select(?esn_temp:2)
!                error_temp = 1
!            End !If Len(job:esn) < mod:esn_length_from Or Len(job:esn) > mod:esn_length_to
!        end !if access:modelnum.fetch(mod:model_number_key) = Level:benign
!
!    End!If esn_temp <> ''
!
!    If msn_temp <> ''
!
!        Do check_msn
!
!    End!If esn_temp <> ''
Update_accessories      Routine
    count# = 0
    accessories_temp = ''
    save_jactmp_id = access:jobactmp.savefile()
    set(jactmp:accessory_key)
    loop
        if access:jobactmp.next()
           break
        end !if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if
        accessories_temp = jactmp:accessory
        count# += 1
    end !loop
    access:jobactmp.restorefile(save_jactmp_id)

    If count# > 1
        accessories_temp = Clip(count#)
    End
    Display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Multiple_Job_Wizard',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Multiple_Job_Wizard',1)
    SolaceViewVars('ValidateMobileNumber',ValidateMobileNumber,'Multiple_Job_Wizard',1)
    SolaceViewVars('temp_255string',temp_255string,'Multiple_Job_Wizard',1)
    SolaceViewVars('tmp:do_next',tmp:do_next,'Multiple_Job_Wizard',1)
    SolaceViewVars('force_fault_codes_temp',force_fault_codes_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('error_temp',error_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('number_jobs_temp',number_jobs_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Account_Number_temp',Account_Number_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Transit_Type_temp',Transit_Type_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Collection_Group:save_par_id',Address_Collection_Group:save_par_id,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Collection_Group:date_error_temp',Address_Collection_Group:date_error_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Collection_Group:save_job_id',Address_Collection_Group:save_job_id,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Collection_Group:check_for_bouncers_temp',Address_Collection_Group:check_for_bouncers_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Collection_Group:saved_ref_number_temp',Address_Collection_Group:saved_ref_number_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Collection_Group:saved_esn_temp',Address_Collection_Group:saved_esn_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Collection_Group:saved_msn_temp',Address_Collection_Group:saved_msn_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Collection_Group:time_completed_temp',Address_Collection_Group:time_completed_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Collection_Group:Postcode_Collection',Address_Collection_Group:Postcode_Collection,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Collection_Group:save_wpr_id',Address_Collection_Group:save_wpr_id,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Collection_Group:Company_Name_Collection',Address_Collection_Group:Company_Name_Collection,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Collection_Group:save_maf_id',Address_Collection_Group:save_maf_id,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Collection_Group:Address_Line1_Collection',Address_Collection_Group:Address_Line1_Collection,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Collection_Group:Address_Line2_Collection',Address_Collection_Group:Address_Line2_Collection,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Collection_Group:Address_Line3_Collection',Address_Collection_Group:Address_Line3_Collection,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Collection_Group:Telephone_Collection',Address_Collection_Group:Telephone_Collection,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Group:Postcode',Address_Group:Postcode,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Group:Company_Name',Address_Group:Company_Name,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Group:Address_Line1',Address_Group:Address_Line1,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Group:Address_Line2',Address_Group:Address_Line2,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Group:Address_Line3',Address_Group:Address_Line3,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Group:Telephone_Number',Address_Group:Telephone_Number,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Group:Fax_Number',Address_Group:Fax_Number,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Delivery_Group:Postcode_Delivery',Address_Delivery_Group:Postcode_Delivery,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Delivery_Group:Company_Name_Delivery',Address_Delivery_Group:Company_Name_Delivery,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Delivery_Group:Address_Line1_Delivery',Address_Delivery_Group:Address_Line1_Delivery,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Delivery_Group:Address_Line2_Delivery',Address_Delivery_Group:Address_Line2_Delivery,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Delivery_Group:Address_Line3_Delivery',Address_Delivery_Group:Address_Line3_Delivery,'Multiple_Job_Wizard',1)
    SolaceViewVars('Address_Delivery_Group:Telephone_Delivery',Address_Delivery_Group:Telephone_Delivery,'Multiple_Job_Wizard',1)
    SolaceViewVars('TabNumber',TabNumber,'Multiple_Job_Wizard',1)
    SolaceViewVars('MaxTabs',MaxTabs,'Multiple_Job_Wizard',1)
    SolaceViewVars('Order_Number_Tick_Temp',Order_Number_Tick_Temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Order_Number_Temp',Order_Number_Temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Internal_Location_Tick_Temp',Internal_Location_Tick_Temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Internal_Location_Temp',Internal_Location_Temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Mobile_Number_temp',Mobile_Number_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Mobile_Number_Tick_Temp',Mobile_Number_Tick_Temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Model_Number_temp',Model_Number_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('model_number_tick_temp',model_number_tick_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Unit_Type_temp',Unit_Type_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('unit_type_tick_temp',unit_type_tick_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('ESN_temp',ESN_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('esn_tick_temp',esn_tick_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('MSN_temp',MSN_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('msn_tick_temp',msn_tick_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Fault_Description_temp',Fault_Description_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('fault_description_tick_temp',fault_description_tick_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Intermittent_Fault_temp',Intermittent_Fault_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('intermittent_fault_tick_temp',intermittent_fault_tick_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Charge_Type_temp',Charge_Type_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('charge_type_tick_temp',charge_type_tick_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Repair_Type_temp',Repair_Type_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('repair_type_tick_temp',repair_type_tick_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Physical_Damage_temp',Physical_Damage_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('physical_damage_tick_temp',physical_damage_tick_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('manufacturer_temp',manufacturer_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('number_temp',number_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('use_batch_temp',use_batch_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('batch_number_temp',batch_number_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('auto_complete_temp',auto_complete_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('date_completed_temp',date_completed_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('auto_despatch_temp',auto_despatch_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('courier_temp',courier_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Title_temp',Title_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('initial_temp',initial_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('surname_temp',surname_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Print_Job_Receipt_Temp',Print_Job_Receipt_Temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Print_Job_Card_Temp',Print_Job_Card_Temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Print_Despatch_Notes_Temp',Print_Despatch_Notes_Temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('warranty_job_tick_temp',warranty_job_tick_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('warranty_charge_type_temp',warranty_charge_type_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('print_job_label_temp',print_job_label_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('repair_type_warranty_tick_temp',repair_type_warranty_tick_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('repair_type_warranty_temp',repair_type_warranty_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('accessories_temp',accessories_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Colour_Temp',Colour_Temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('colour_tick_temp',colour_tick_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('tmp:DOPTick',tmp:DOPTick,'Multiple_Job_Wizard',1)
    SolaceViewVars('tmp:dop',tmp:dop,'Multiple_Job_Wizard',1)
    SolaceViewVars('save_jac_id',save_jac_id,'Multiple_Job_Wizard',1)
    SolaceViewVars('tmp:JobCreated',tmp:JobCreated,'Multiple_Job_Wizard',1)
    SolaceViewVars('tmp:ProductCode',tmp:ProductCode,'Multiple_Job_Wizard',1)
    SolaceViewVars('print_bookin_label_temp',print_bookin_label_temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Consignment_Number_Tick_Temp',Consignment_Number_Tick_Temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Consignment_Date_Tick_Temp',Consignment_Date_Tick_Temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Consignment_Number_Temp',Consignment_Number_Temp,'Multiple_Job_Wizard',1)
    SolaceViewVars('Consignment_Date_Temp',Consignment_Date_Temp,'Multiple_Job_Wizard',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2:2;  SolaceCtrlName = '?Prompt2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt47;  SolaceCtrlName = '?Prompt47';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Order_Number_Tick_Temp;  SolaceCtrlName = '?Order_Number_Tick_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Order_Number_Temp;  SolaceCtrlName = '?Order_Number_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Internal_Location_Tick_Temp;  SolaceCtrlName = '?Internal_Location_Tick_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Internal_Location_Temp;  SolaceCtrlName = '?Internal_Location_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Location;  SolaceCtrlName = '?Lookup_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?model_number_tick_temp;  SolaceCtrlName = '?model_number_tick_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Model_Number_temp;  SolaceCtrlName = '?Model_Number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?lookup_Model_Number:3;  SolaceCtrlName = '?lookup_Model_Number:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?colour_tick_temp;  SolaceCtrlName = '?colour_tick_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Colour_Temp;  SolaceCtrlName = '?Colour_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Colour;  SolaceCtrlName = '?Lookup_Colour';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?unit_type_tick_temp;  SolaceCtrlName = '?unit_type_tick_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Unit_Type_temp;  SolaceCtrlName = '?Unit_Type_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Unit_Type;  SolaceCtrlName = '?Lookup_Unit_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?esn_tick_temp;  SolaceCtrlName = '?esn_tick_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ESN_temp;  SolaceCtrlName = '?ESN_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?fault_description_tick_temp;  SolaceCtrlName = '?fault_description_tick_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Fault_Description_Text;  SolaceCtrlName = '?Fault_Description_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Fault_Description_temp;  SolaceCtrlName = '?Fault_Description_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DOPTick;  SolaceCtrlName = '?tmp:DOPTick';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:dop;  SolaceCtrlName = '?tmp:dop';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Consignment_Number_Tick_Temp;  SolaceCtrlName = '?Consignment_Number_Tick_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Consignment_Number_Temp;  SolaceCtrlName = '?Consignment_Number_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Consignment_Date_Tick_Temp;  SolaceCtrlName = '?Consignment_Date_Tick_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopConsignmentDate;  SolaceCtrlName = '?PopConsignmentDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Consignment_Date_Temp;  SolaceCtrlName = '?Consignment_Date_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ApplyDefault;  SolaceCtrlName = '?ApplyDefault';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab6;  SolaceCtrlName = '?Tab6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt40;  SolaceCtrlName = '?Prompt40';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt41;  SolaceCtrlName = '?Prompt41';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?charge_type_tick_temp;  SolaceCtrlName = '?charge_type_tick_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Charge_Type;  SolaceCtrlName = '?Lookup_Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Charge_Type_temp;  SolaceCtrlName = '?Charge_Type_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?repair_type_tick_temp;  SolaceCtrlName = '?repair_type_tick_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Repair_Type;  SolaceCtrlName = '?Lookup_Repair_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Repair_Type_temp;  SolaceCtrlName = '?Repair_Type_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?warranty_job_tick_temp;  SolaceCtrlName = '?warranty_job_tick_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?lookup_warranty_charge_type;  SolaceCtrlName = '?lookup_warranty_charge_type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?warranty_charge_type_temp;  SolaceCtrlName = '?warranty_charge_type_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?repair_type_warranty_tick_temp;  SolaceCtrlName = '?repair_type_warranty_tick_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Repair_Type_Warranty;  SolaceCtrlName = '?Lookup_Repair_Type_Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?repair_type_warranty_temp;  SolaceCtrlName = '?repair_type_warranty_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?auto_complete_temp;  SolaceCtrlName = '?auto_complete_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?date_completed_temp;  SolaceCtrlName = '?date_completed_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?auto_despatch_temp;  SolaceCtrlName = '?auto_despatch_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Courier;  SolaceCtrlName = '?Lookup_Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?courier_temp;  SolaceCtrlName = '?courier_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt23;  SolaceCtrlName = '?Prompt23';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Account_Number_temp;  SolaceCtrlName = '?Account_Number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Account;  SolaceCtrlName = '?Lookup_Account';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt24;  SolaceCtrlName = '?Prompt24';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Transit_Type_temp;  SolaceCtrlName = '?Transit_Type_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Transit_Type;  SolaceCtrlName = '?Lookup_Transit_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?name_group;  SolaceCtrlName = '?name_group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Title_temp:Prompt;  SolaceCtrlName = '?Title_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Title_temp:Prompt:2;  SolaceCtrlName = '?Title_temp:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Title_temp:Prompt:3;  SolaceCtrlName = '?Title_temp:Prompt:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Title_temp:Prompt:4;  SolaceCtrlName = '?Title_temp:Prompt:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Title_temp;  SolaceCtrlName = '?Title_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?initial_temp;  SolaceCtrlName = '?initial_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?surname_temp;  SolaceCtrlName = '?surname_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt20;  SolaceCtrlName = '?Prompt20';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Company_Name:Prompt;  SolaceCtrlName = '?Company_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Company_Name;  SolaceCtrlName = '?Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Postcode:Prompt;  SolaceCtrlName = '?Postcode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Postcode;  SolaceCtrlName = '?Postcode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Address_Line1:Prompt;  SolaceCtrlName = '?Address_Line1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Address_Line1;  SolaceCtrlName = '?Address_Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Address_Line2;  SolaceCtrlName = '?Address_Line2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Address_Line3;  SolaceCtrlName = '?Address_Line3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Telephone_Number:Prompt;  SolaceCtrlName = '?Telephone_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Telephone_Number;  SolaceCtrlName = '?Telephone_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Fax_Number:Prompt;  SolaceCtrlName = '?Fax_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Fax_Number;  SolaceCtrlName = '?Fax_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delivery_address_Group;  SolaceCtrlName = '?Delivery_address_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt22;  SolaceCtrlName = '?Prompt22';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Company_Name:Prompt:2;  SolaceCtrlName = '?Company_Name:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Company_Name_Delivery;  SolaceCtrlName = '?Company_Name_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Postcode:Prompt:2;  SolaceCtrlName = '?Postcode:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Postcode_Delivery;  SolaceCtrlName = '?Postcode_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delivery_Button;  SolaceCtrlName = '?Delivery_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Address_Line1:Prompt:2;  SolaceCtrlName = '?Address_Line1:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Address_Line1_Delivery;  SolaceCtrlName = '?Address_Line1_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Address_Line2_Delivery;  SolaceCtrlName = '?Address_Line2_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Address_Line3_Delivery;  SolaceCtrlName = '?Address_Line3_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Telephone_Number:Prompt:2;  SolaceCtrlName = '?Telephone_Number:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Telephone_Delivery;  SolaceCtrlName = '?Telephone_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?collection_address_group;  SolaceCtrlName = '?collection_address_group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt21;  SolaceCtrlName = '?Prompt21';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Company_Name_Collection;  SolaceCtrlName = '?Company_Name_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Postcode_Collection;  SolaceCtrlName = '?Postcode_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Collection_Clear;  SolaceCtrlName = '?Collection_Clear';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Address_Line1_Collection;  SolaceCtrlName = '?Address_Line1_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Address_Line2_Collection;  SolaceCtrlName = '?Address_Line2_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Address_Line3_Collection;  SolaceCtrlName = '?Address_Line3_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Telephone_Collection;  SolaceCtrlName = '?Telephone_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt37;  SolaceCtrlName = '?Prompt37';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt38;  SolaceCtrlName = '?Prompt38';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?print_bookin_label_temp;  SolaceCtrlName = '?print_bookin_label_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?print_job_label_temp;  SolaceCtrlName = '?print_job_label_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Print_Job_Card_Temp;  SolaceCtrlName = '?Print_Job_Card_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Print_Job_Receipt_Temp;  SolaceCtrlName = '?Print_Job_Receipt_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Print_Despatch_Notes_Temp;  SolaceCtrlName = '?Print_Despatch_Notes_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt33;  SolaceCtrlName = '?Prompt33';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt34;  SolaceCtrlName = '?Prompt34';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use_batch_temp;  SolaceCtrlName = '?use_batch_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option5:Radio1;  SolaceCtrlName = '?Option5:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option5:Radio2;  SolaceCtrlName = '?Option5:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option5:Radio3;  SolaceCtrlName = '?Option5:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Batch_Message;  SolaceCtrlName = '?Batch_Message';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?batch_number_temp;  SolaceCtrlName = '?batch_number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String2;  SolaceCtrlName = '?String2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab6:2;  SolaceCtrlName = '?Tab6:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt42;  SolaceCtrlName = '?Prompt42';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Fields_Group;  SolaceCtrlName = '?Fields_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ESN_temp:Prompt;  SolaceCtrlName = '?ESN_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ESN_temp:2;  SolaceCtrlName = '?ESN_temp:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BouncerText;  SolaceCtrlName = '?BouncerText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?msn:prompt;  SolaceCtrlName = '?msn:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MSN_temp;  SolaceCtrlName = '?MSN_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ProductCode:Prompt;  SolaceCtrlName = '?tmp:ProductCode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupProductCode;  SolaceCtrlName = '?LookupProductCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ProductCode;  SolaceCtrlName = '?tmp:ProductCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt26;  SolaceCtrlName = '?Prompt26';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Model_Number:2;  SolaceCtrlName = '?Lookup_Model_Number:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Model_Number_temp:2;  SolaceCtrlName = '?Model_Number_temp:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt27;  SolaceCtrlName = '?Prompt27';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_unit_Type:2;  SolaceCtrlName = '?Lookup_unit_Type:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Unit_Type_temp:2;  SolaceCtrlName = '?Unit_Type_temp:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt43;  SolaceCtrlName = '?Prompt43';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Accessories;  SolaceCtrlName = '?Lookup_Accessories';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?accessories_temp;  SolaceCtrlName = '?accessories_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Order_Number_Temp:Prompt;  SolaceCtrlName = '?Order_Number_Temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Order_Number_Temp:2;  SolaceCtrlName = '?Order_Number_Temp:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt36;  SolaceCtrlName = '?Prompt36';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Internal_Location:2;  SolaceCtrlName = '?Lookup_Internal_Location:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Internal_Location_Temp:2;  SolaceCtrlName = '?Internal_Location_Temp:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt25;  SolaceCtrlName = '?Prompt25';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?fault_description_text_2;  SolaceCtrlName = '?fault_description_text_2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Fault_Description_temp:2;  SolaceCtrlName = '?Fault_Description_temp:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Mobile_Number:Prompt;  SolaceCtrlName = '?Mobile_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Mobile_Number_temp:2;  SolaceCtrlName = '?Mobile_Number_temp:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:dop:Prompt;  SolaceCtrlName = '?tmp:dop:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:dop:2;  SolaceCtrlName = '?tmp:dop:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt28;  SolaceCtrlName = '?Prompt28';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Charge_Type:2;  SolaceCtrlName = '?Lookup_Charge_Type:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Charge_Type_temp:2;  SolaceCtrlName = '?Charge_Type_temp:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt31;  SolaceCtrlName = '?Prompt31';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Repair_Type:2;  SolaceCtrlName = '?Lookup_Repair_Type:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Repair_Type_temp:2;  SolaceCtrlName = '?Repair_Type_temp:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt35;  SolaceCtrlName = '?Prompt35';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Warranty_Charge_Type:2;  SolaceCtrlName = '?Lookup_Warranty_Charge_Type:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?warranty_charge_type_temp:2;  SolaceCtrlName = '?warranty_charge_type_temp:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?repair_type_warranty_temp:Prompt;  SolaceCtrlName = '?repair_type_warranty_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Warranty_Repair_Type;  SolaceCtrlName = '?Lookup_Warranty_Repair_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?repair_type_warranty_temp:2;  SolaceCtrlName = '?repair_type_warranty_temp:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Fault_Codes;  SolaceCtrlName = '?Fault_Codes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?save_Button;  SolaceCtrlName = '?save_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert_Multiple;  SolaceCtrlName = '?Insert_Multiple';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Save_Finish_Button;  SolaceCtrlName = '?Save_Finish_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1;  SolaceCtrlName = '?Image1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VSBackButton;  SolaceCtrlName = '?VSBackButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VSNextButton;  SolaceCtrlName = '?VSNextButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Finish;  SolaceCtrlName = '?Finish';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, Window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Multiple_Job_Wizard')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Multiple_Job_Wizard')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:ESNMODEL.Open
  Relate:EXCHANGE.Open
  Relate:JOBACTMP.Open
  Relate:JOBBATCH.Open
  Relate:JOBS2_ALIAS.Open
  Relate:MULTIDEF.Open
  Relate:REPTYDEF.Open
  Relate:STAHEAD.Open
  Relate:TRANTYPE.Open
  Access:JOBS.UseFile
  Access:USERS.UseFile
  Access:TRADEACC.UseFile
  Access:JOBSTAGE.UseFile
  Access:STATUS.UseFile
  Access:LOCINTER.UseFile
  Access:CHARTYPE.UseFile
  Access:REPAIRTY.UseFile
  Access:UNITTYPE.UseFile
  Access:SUBTRACC.UseFile
  Access:MODELNUM.UseFile
  Access:MODELCOL.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBSE.UseFile
  Access:MANUFACT.UseFile
  Access:MANFAULT.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:TRAFAULT.UseFile
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  Do RecolourWindow
    Wizard4.Init(?Sheet1, |                           ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Finish, |                       ! OK button
                     ?Close, |                        ! Cancel button
                     1, |                             ! Skip hidden tabs
                     1, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  ?date_completed_temp{Prop:Alrt,255} = MouseLeft2
  IF ?Account_Number_temp{Prop:Tip} AND ~?Lookup_Account{Prop:Tip}
     ?Lookup_Account{Prop:Tip} = 'Select ' & ?Account_Number_temp{Prop:Tip}
  END
  IF ?Account_Number_temp{Prop:Msg} AND ~?Lookup_Account{Prop:Msg}
     ?Lookup_Account{Prop:Msg} = 'Select ' & ?Account_Number_temp{Prop:Msg}
  END
  IF ?Transit_Type_temp{Prop:Tip} AND ~?Lookup_Transit_Type{Prop:Tip}
     ?Lookup_Transit_Type{Prop:Tip} = 'Select ' & ?Transit_Type_temp{Prop:Tip}
  END
  IF ?Transit_Type_temp{Prop:Msg} AND ~?Lookup_Transit_Type{Prop:Msg}
     ?Lookup_Transit_Type{Prop:Msg} = 'Select ' & ?Transit_Type_temp{Prop:Msg}
  END
  IF ?Internal_Location_Temp{Prop:Tip} AND ~?Lookup_Location{Prop:Tip}
     ?Lookup_Location{Prop:Tip} = 'Select ' & ?Internal_Location_Temp{Prop:Tip}
  END
  IF ?Internal_Location_Temp{Prop:Msg} AND ~?Lookup_Location{Prop:Msg}
     ?Lookup_Location{Prop:Msg} = 'Select ' & ?Internal_Location_Temp{Prop:Msg}
  END
  IF ?Model_Number_temp{Prop:Tip} AND ~?lookup_Model_Number:3{Prop:Tip}
     ?lookup_Model_Number:3{Prop:Tip} = 'Select ' & ?Model_Number_temp{Prop:Tip}
  END
  IF ?Model_Number_temp{Prop:Msg} AND ~?lookup_Model_Number:3{Prop:Msg}
     ?lookup_Model_Number:3{Prop:Msg} = 'Select ' & ?Model_Number_temp{Prop:Msg}
  END
  IF ?Colour_Temp{Prop:Tip} AND ~?Lookup_Colour{Prop:Tip}
     ?Lookup_Colour{Prop:Tip} = 'Select ' & ?Colour_Temp{Prop:Tip}
  END
  IF ?Colour_Temp{Prop:Msg} AND ~?Lookup_Colour{Prop:Msg}
     ?Lookup_Colour{Prop:Msg} = 'Select ' & ?Colour_Temp{Prop:Msg}
  END
  IF ?Unit_Type_temp{Prop:Tip} AND ~?Lookup_Unit_Type{Prop:Tip}
     ?Lookup_Unit_Type{Prop:Tip} = 'Select ' & ?Unit_Type_temp{Prop:Tip}
  END
  IF ?Unit_Type_temp{Prop:Msg} AND ~?Lookup_Unit_Type{Prop:Msg}
     ?Lookup_Unit_Type{Prop:Msg} = 'Select ' & ?Unit_Type_temp{Prop:Msg}
  END
  IF ?Order_Number_Tick_Temp{Prop:Checked} = True
    UNHIDE(?Order_Number_Temp)
  END
  IF ?Order_Number_Tick_Temp{Prop:Checked} = False
    Order_Number_Temp = ''
    HIDE(?Order_Number_Temp)
  END
  IF ?Internal_Location_Tick_Temp{Prop:Checked} = True
    UNHIDE(?Internal_Location_Temp)
    UNHIDE(?Lookup_Location)
  END
  IF ?Internal_Location_Tick_Temp{Prop:Checked} = False
    Internal_Location_Temp = ''
    HIDE(?Internal_Location_Temp)
    HIDE(?Lookup_Location)
  END
  IF ?model_number_tick_temp{Prop:Checked} = True
    Colour_Temp = ''
    colour_tick_temp = ''
    UNHIDE(?Model_Number_temp)
    UNHIDE(?repair_type_tick_temp)
    UNHIDE(?lookup_Model_Number:3)
    UNHIDE(?repair_type_warranty_tick_temp)
    UNHIDE(?colour_tick_temp)
  END
  IF ?model_number_tick_temp{Prop:Checked} = False
    Model_Number_temp = ''
    colour_tick_temp = 'N'
    Colour_Temp = ''
    HIDE(?Model_Number_temp)
    HIDE(?repair_type_tick_temp)
    HIDE(?Repair_Type_temp)
    HIDE(?lookup_Model_Number:3)
    HIDE(?repair_type_warranty_tick_temp)
    HIDE(?colour_tick_temp)
  END
  IF ?colour_tick_temp{Prop:Checked} = True
    UNHIDE(?Lookup_Colour)
    UNHIDE(?Colour_Temp)
  END
  IF ?colour_tick_temp{Prop:Checked} = False
    HIDE(?Lookup_Colour)
    HIDE(?Colour_Temp)
  END
  IF ?unit_type_tick_temp{Prop:Checked} = True
    UNHIDE(?Unit_Type_temp)
    UNHIDE(?Lookup_Unit_Type)
  END
  IF ?unit_type_tick_temp{Prop:Checked} = False
    Unit_Type_temp = ''
    HIDE(?Unit_Type_temp)
    HIDE(?Lookup_Unit_Type)
  END
  IF ?esn_tick_temp{Prop:Checked} = True
    UNHIDE(?ESN_temp)
  END
  IF ?esn_tick_temp{Prop:Checked} = False
    ESN_temp = ''
    HIDE(?ESN_temp)
  END
  IF ?fault_description_tick_temp{Prop:Checked} = True
    UNHIDE(?Fault_Description_temp)
    UNHIDE(?Fault_Description_Text)
  END
  IF ?fault_description_tick_temp{Prop:Checked} = False
    Fault_Description_temp = ''
    HIDE(?Fault_Description_temp)
    HIDE(?Fault_Description_Text)
  END
  IF ?tmp:DOPTick{Prop:Checked} = True
    UNHIDE(?tmp:dop)
  END
  IF ?tmp:DOPTick{Prop:Checked} = False
    tmp:dop = ''
    HIDE(?tmp:dop)
  END
  IF ?Consignment_Number_Tick_Temp{Prop:Checked} = True
    UNHIDE(?Consignment_Number_Temp)
  END
  IF ?Consignment_Number_Tick_Temp{Prop:Checked} = False
    HIDE(?Consignment_Number_Temp)
  END
  IF ?Consignment_Date_Tick_Temp{Prop:Checked} = True
    UNHIDE(?Consignment_Date_Temp)
    UNHIDE(?PopConsignmentDate)
  END
  IF ?Consignment_Date_Tick_Temp{Prop:Checked} = False
    HIDE(?Consignment_Date_Temp)
    HIDE(?PopConsignmentDate)
  END
  IF ?charge_type_tick_temp{Prop:Checked} = True
    UNHIDE(?Charge_Type_temp)
    UNHIDE(?Lookup_Charge_Type)
  END
  IF ?charge_type_tick_temp{Prop:Checked} = False
    Charge_Type_temp = ''
    HIDE(?Charge_Type_temp)
    HIDE(?Lookup_Charge_Type)
  END
  IF ?repair_type_tick_temp{Prop:Checked} = True
    UNHIDE(?Repair_Type_temp)
    UNHIDE(?Lookup_Repair_Type)
  END
  IF ?repair_type_tick_temp{Prop:Checked} = False
    Repair_Type_temp = ''
    HIDE(?Repair_Type_temp)
    HIDE(?Lookup_Repair_Type)
  END
  IF ?warranty_job_tick_temp{Prop:Checked} = True
    UNHIDE(?warranty_charge_type_temp)
    UNHIDE(?lookup_warranty_charge_type)
  END
  IF ?warranty_job_tick_temp{Prop:Checked} = False
    warranty_charge_type_temp = ''
    HIDE(?warranty_charge_type_temp)
    HIDE(?lookup_warranty_charge_type)
  END
  IF ?repair_type_warranty_tick_temp{Prop:Checked} = True
    UNHIDE(?repair_type_warranty_temp)
    UNHIDE(?Lookup_Repair_Type_Warranty)
  END
  IF ?repair_type_warranty_tick_temp{Prop:Checked} = False
    HIDE(?Lookup_Repair_Type_Warranty)
    HIDE(?repair_type_warranty_temp)
  END
  IF ?auto_complete_temp{Prop:Checked} = True
    UNHIDE(?date_completed_temp)
    UNHIDE(?PopCalendar)
  END
  IF ?auto_complete_temp{Prop:Checked} = False
    date_completed_temp = ''
    HIDE(?date_completed_temp)
    HIDE(?PopCalendar)
  END
  IF ?auto_despatch_temp{Prop:Checked} = True
    UNHIDE(?courier_temp)
    UNHIDE(?Lookup_Courier)
  END
  IF ?auto_despatch_temp{Prop:Checked} = False
    courier_temp = ''
    HIDE(?courier_temp)
    HIDE(?Lookup_Courier)
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:ESNMODEL.Close
    Relate:EXCHANGE.Close
    Relate:JOBACTMP.Close
    Relate:JOBBATCH.Close
    Relate:JOBS2_ALIAS.Close
    Relate:MULTIDEF.Close
    Relate:REPTYDEF.Close
    Relate:STAHEAD.Close
    Relate:TRANTYPE.Close
  END
  Remove(jobactmp)
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Multiple_Job_Wizard',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Available_Locations
      BrowseMODELNUM
      Pick_Model_Colour
      BrowseUNITTYPE
      Browse_Sub_Accounts
      PickTransitTypes
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard4.Validate()
        DISABLE(Wizard4.NextControl())
     ELSE
        ENABLE(Wizard4.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?Fault_Description_Text
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Fault_Description_Text, Accepted)
      glo:select1 = manufacturer_temp
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Fault_Description_Text, Accepted)
    OF ?Lookup_Charge_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Charge_Type, Accepted)
      glo:select1 = 'NO'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Charge_Type, Accepted)
    OF ?Lookup_Repair_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Repair_Type, Accepted)
      glo:select1 = model_number_temp
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Repair_Type, Accepted)
    OF ?lookup_warranty_charge_type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lookup_warranty_charge_type, Accepted)
      glo:select1 = 'YES'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lookup_warranty_charge_type, Accepted)
    OF ?Lookup_Repair_Type_Warranty
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Repair_Type_Warranty, Accepted)
      glo:select1 = model_number_temp
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Repair_Type_Warranty, Accepted)
    OF ?Lookup_Courier
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Courier, Accepted)
      case globalresponse
          of requestcompleted
              courier_temp = cou:courier
              Select(?+2)
          of requestcancelled
              courier_temp = ''
              Select(?-1)
      end!case globalreponse
      display(?courier_temp)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Courier, Accepted)
    OF ?fault_description_text_2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?fault_description_text_2, Accepted)
      glo:select1 = manufacturer_temp
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?fault_description_text_2, Accepted)
    OF ?Lookup_Charge_Type:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Charge_Type:2, Accepted)
      glo:select1 = 'NO'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Charge_Type:2, Accepted)
    OF ?Lookup_Repair_Type:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Repair_Type:2, Accepted)
      glo:select1 = model_number_temp
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Repair_Type:2, Accepted)
    OF ?Lookup_Warranty_Charge_Type:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Warranty_Charge_Type:2, Accepted)
      glo:select1 = 'YES'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Warranty_Charge_Type:2, Accepted)
    OF ?Lookup_Warranty_Repair_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Warranty_Repair_Type, Accepted)
      glo:select1 = model_number_temp
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Warranty_Repair_Type, Accepted)
    OF ?Fault_Codes
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Fault_Codes, Accepted)
         !May be losing the Manufacturer somewhere, so I'll get it again
          Access:MODELNUM.ClearKey(mod:Model_Number_Key)
          mod:Model_Number = Model_Number_Temp
          If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
              !Found
              glo:Select4 = mod:Manufacturer
          Else!If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
      !    Fault_Codes_Temp_Window
          ! Start Change 4340 BE(6/4/2004)
          !GenericFaultCodes(0,0,Account_Number_Temp,mod:Manufacturer,|
          !                    Warranty_Charge_Type_Temp,Repair_Type_Warranty_Temp,0,|
          !                    glo:Select13,glo:Select14,glo:Select15,glo:Select16,|
          !                    glo:Select17,glo:Select18,glo:Select19,glo:Select20,|
          !                    glo:Select21,glo:Select22,glo:Select23,glo:Select24,|
          !                    glo:Select31,glo:Select32,glo:Select33,glo:Select34,|
          !                    glo:Select35,glo:Select36,glo:Select37,glo:Select38,|
          !                    glo:Select39,glo:Select40,glo:Select41,glo:Select42)
      
          GenericFaultCodes(0,1,Account_Number_Temp,mod:Manufacturer,|
                              Warranty_Charge_Type_Temp,Repair_Type_Warranty_Temp,0)
          ! End Change 4340 BE(6/4/2004)
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Fault_Codes, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Order_Number_Tick_Temp
      IF ?Order_Number_Tick_Temp{Prop:Checked} = True
        UNHIDE(?Order_Number_Temp)
      END
      IF ?Order_Number_Tick_Temp{Prop:Checked} = False
        Order_Number_Temp = ''
        HIDE(?Order_Number_Temp)
      END
      ThisWindow.Reset
    OF ?Internal_Location_Tick_Temp
      IF ?Internal_Location_Tick_Temp{Prop:Checked} = True
        UNHIDE(?Internal_Location_Temp)
        UNHIDE(?Lookup_Location)
      END
      IF ?Internal_Location_Tick_Temp{Prop:Checked} = False
        Internal_Location_Temp = ''
        HIDE(?Internal_Location_Temp)
        HIDE(?Lookup_Location)
      END
      ThisWindow.Reset
    OF ?Internal_Location_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Internal_Location_Temp, Accepted)
      loi:location_available = 'YES'
      IF Internal_Location_Temp OR ?Internal_Location_Temp{Prop:Req}
        loi:Location = Internal_Location_Temp
        !Save Lookup Field Incase Of error
        look:Internal_Location_Temp        = Internal_Location_Temp
        IF Access:LOCINTER.TryFetch(loi:Location_Available_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            Internal_Location_Temp = loi:Location
          ELSE
            !Restore Lookup On Error
            Internal_Location_Temp = look:Internal_Location_Temp
            SELECT(?Internal_Location_Temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Internal_Location_Temp, Accepted)
    OF ?Lookup_Location
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Location, Accepted)
      loi:location_available = 'YES'
      loi:Location = Internal_Location_Temp
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          Internal_Location_Temp = loi:Location
          Select(?+1)
      ELSE
          Select(?Internal_Location_Temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?Internal_Location_Temp)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Location, Accepted)
    OF ?model_number_tick_temp
      IF ?model_number_tick_temp{Prop:Checked} = True
        Colour_Temp = ''
        colour_tick_temp = ''
        UNHIDE(?Model_Number_temp)
        UNHIDE(?repair_type_tick_temp)
        UNHIDE(?lookup_Model_Number:3)
        UNHIDE(?repair_type_warranty_tick_temp)
        UNHIDE(?colour_tick_temp)
      END
      IF ?model_number_tick_temp{Prop:Checked} = False
        Model_Number_temp = ''
        colour_tick_temp = 'N'
        Colour_Temp = ''
        HIDE(?Model_Number_temp)
        HIDE(?repair_type_tick_temp)
        HIDE(?Repair_Type_temp)
        HIDE(?lookup_Model_Number:3)
        HIDE(?repair_type_warranty_tick_temp)
        HIDE(?colour_tick_temp)
      END
      ThisWindow.Reset
    OF ?Model_Number_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Model_Number_temp, Accepted)
      IF Model_Number_temp OR ?Model_Number_temp{Prop:Req}
        mod:Model_Number = Model_Number_temp
        !Save Lookup Field Incase Of error
        look:Model_Number_temp        = Model_Number_temp
        IF Access:MODELNUM.TryFetch(mod:Model_Number_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            Model_Number_temp = mod:Model_Number
          ELSE
            !Restore Lookup On Error
            Model_Number_temp = look:Model_Number_temp
            SELECT(?Model_Number_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      manufacturer_temp = mod:manufacturer
      Do check_unit_type
      Post(event:accepted,?esn_temp)
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Model_Number_temp, Accepted)
    OF ?lookup_Model_Number:3
      ThisWindow.Update
      mod:Model_Number = Model_Number_temp
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          Model_Number_temp = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?Model_Number_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?Model_Number_temp)
    OF ?colour_tick_temp
      IF ?colour_tick_temp{Prop:Checked} = True
        UNHIDE(?Lookup_Colour)
        UNHIDE(?Colour_Temp)
      END
      IF ?colour_tick_temp{Prop:Checked} = False
        HIDE(?Lookup_Colour)
        HIDE(?Colour_Temp)
      END
      ThisWindow.Reset
    OF ?Colour_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Colour_Temp, Accepted)
      glo:select11   = model_number_temp
      moc:model_number  = model_number_temp
      IF Colour_Temp OR ?Colour_Temp{Prop:Req}
        moc:Colour = Colour_Temp
        moc:Model_Number = model_Number_Temp
        GLO:Select12 = model_number_temp
        !Save Lookup Field Incase Of error
        look:Colour_Temp        = Colour_Temp
        IF Access:MODELCOL.TryFetch(moc:Colour_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            Colour_Temp = moc:Colour
          ELSE
            CLEAR(moc:Model_Number)
            CLEAR(GLO:Select12)
            !Restore Lookup On Error
            Colour_Temp = look:Colour_Temp
            SELECT(?Colour_Temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Colour_Temp, Accepted)
    OF ?Lookup_Colour
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Colour, Accepted)
      case globalresponse
          of requestcompleted
              colour_temp = moc:colour  
              select(?+2)
          of requestcancelled
      !        colour_temp = ''
              select(?-1)
      end!case globalreponse
      display(?colour_temp)
      moc:Colour = Colour_Temp
      GLO:Select12 = model_number_temp
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          Colour_Temp = moc:Colour
          Select(?+1)
      ELSE
          Select(?Colour_Temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?Colour_Temp)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Colour, Accepted)
    OF ?unit_type_tick_temp
      IF ?unit_type_tick_temp{Prop:Checked} = True
        UNHIDE(?Unit_Type_temp)
        UNHIDE(?Lookup_Unit_Type)
      END
      IF ?unit_type_tick_temp{Prop:Checked} = False
        Unit_Type_temp = ''
        HIDE(?Unit_Type_temp)
        HIDE(?Lookup_Unit_Type)
      END
      ThisWindow.Reset
    OF ?Unit_Type_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Unit_Type_temp, Accepted)
      access:unittype.clearkey(uni:unit_type_key)
      uni:unit_type = unit_type_temp
      if access:unittype.fetch(uni:unit_type_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browseunittype
          if globalresponse = requestcompleted
              unit_type_temp = uni:unit_type
          else
              unit_type_temp = ''
              Select(?-1)
          end
          display(?unit_type_temp)
          globalrequest     = saverequest#
      end!if access:unittype.fetch(uni:unit_type_key)
      IF Unit_Type_temp OR ?Unit_Type_temp{Prop:Req}
        uni:Unit_Type = Unit_Type_temp
        !Save Lookup Field Incase Of error
        look:Unit_Type_temp        = Unit_Type_temp
        IF Access:UNITTYPE.TryFetch(uni:Unit_Type_Key)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            Unit_Type_temp = uni:Unit_Type
          ELSE
            !Restore Lookup On Error
            Unit_Type_temp = look:Unit_Type_temp
            SELECT(?Unit_Type_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Unit_Type_temp, Accepted)
    OF ?Lookup_Unit_Type
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Unit_Type, Accepted)
      case globalresponse
          of requestcompleted
              unit_type_temp = uni:unit_type
              Select(?+2)
          of requestcancelled
              unit_type_temp = ''
              Select(?-1)
      end!case globalreponse
      display(?unit_type_temp)
      uni:Unit_Type = Unit_Type_temp
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          Unit_Type_temp = uni:Unit_Type
          Select(?+1)
      ELSE
          Select(?Unit_Type_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?Unit_Type_temp)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Unit_Type, Accepted)
    OF ?esn_tick_temp
      IF ?esn_tick_temp{Prop:Checked} = True
        UNHIDE(?ESN_temp)
      END
      IF ?esn_tick_temp{Prop:Checked} = False
        ESN_temp = ''
        HIDE(?ESN_temp)
      END
      ThisWindow.Reset
    OF ?ESN_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ESN_temp, Accepted)
      If esn_tick_temp = 'Y'
          access:Modelnum.clearkey(mod:model_number_key)
          mod:model_number    = model_number_temp
          If access:modelnum.tryfetch(mod:model_number_key) = Level:Benign
              If Len(Clip(esn_temp)) < mod:esn_length_from Or Len(Clip(esn_temp)) > mod:esn_length_to
                  Case MessageEx('The E.S.N. / I.M.E.I. entered should be between '&clip(mod:esn_length_from)&' and '&Clip(mod:esn_length_to)&' characters in length.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  tmp:do_next = 0
                  Select(?esn_temp)
              End!If Len(Clip(esn_temp)) < mod:esn_length_from Or Len(Clip(esn_temp)) > mod:esn_length_to
          End!If access:modelnum.tryfetch(mod:model_number_key) = Level:Benign
      End!If esn_temp <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ESN_temp, Accepted)
    OF ?fault_description_tick_temp
      IF ?fault_description_tick_temp{Prop:Checked} = True
        UNHIDE(?Fault_Description_temp)
        UNHIDE(?Fault_Description_Text)
      END
      IF ?fault_description_tick_temp{Prop:Checked} = False
        Fault_Description_temp = ''
        HIDE(?Fault_Description_temp)
        HIDE(?Fault_Description_Text)
      END
      ThisWindow.Reset
    OF ?Fault_Description_Text
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Fault_Description_Text
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Fault_Description_Text, Accepted)
      glo:select1 = ''
      If Records(glo:Q_Notes) <> ''
          Loop x$ = 1 To Records(glo:Q_Notes)
              Get(glo:Q_Notes,x$)
              If fault_description_temp = ''
                  fault_description_temp    = Clip(glo:notes_pointer)
              Else !If job:invoice_text = ''
                  fault_description_temp = Clip(fault_description_temp) & '<13,10>' & Clip(glo:notes_pointer)
              End !If job:invoice_text = ''
          End
          Display()
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Fault_Description_Text, Accepted)
    OF ?tmp:DOPTick
      IF ?tmp:DOPTick{Prop:Checked} = True
        UNHIDE(?tmp:dop)
      END
      IF ?tmp:DOPTick{Prop:Checked} = False
        tmp:dop = ''
        HIDE(?tmp:dop)
      END
      ThisWindow.Reset
    OF ?Consignment_Number_Tick_Temp
      IF ?Consignment_Number_Tick_Temp{Prop:Checked} = True
        UNHIDE(?Consignment_Number_Temp)
      END
      IF ?Consignment_Number_Tick_Temp{Prop:Checked} = False
        HIDE(?Consignment_Number_Temp)
      END
      ThisWindow.Reset
    OF ?Consignment_Date_Tick_Temp
      IF ?Consignment_Date_Tick_Temp{Prop:Checked} = True
        UNHIDE(?Consignment_Date_Temp)
        UNHIDE(?PopConsignmentDate)
      END
      IF ?Consignment_Date_Tick_Temp{Prop:Checked} = False
        HIDE(?Consignment_Date_Temp)
        HIDE(?PopConsignmentDate)
      END
      ThisWindow.Reset
    OF ?PopConsignmentDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          Consignment_Date_Temp = TINCALENDARStyle1(Consignment_Date_Temp)
          Display(?Consignment_Date_Temp)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?ApplyDefault
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ApplyDefault, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      browsemultidefaults
      if globalresponse = requestcompleted
          If MUL:OrderNumber <> ''
              order_number_tick_temp = 'Y'
              order_number_temp = mul:OrderNumber
              Post(Event:Accepted,?order_number_tick_temp)
          End!If MUL:OrderNumber <> ''
      
          If MUL:InternalLocation <> ''
              internal_location_tick_temp = 'Y'
              internal_location_temp = MUL:InternalLocation
              Post(Event:Accepted,?internal_location_tick_temp)
          End!If MUL:InternalLocation <> ''
      
          If MUL:ModelNumber <> ''
              model_number_tick_temp = 'Y'
              model_number_temp   = mul:ModelNumber
              Post(Event:Accepted,?model_number_tick_temp)
              Display()
          End!If MUL:ModelNumber <> ''
      
          If MUL:UnitType <> ''
              unit_type_tick_temp = 'Y'
              unit_type_temp = mul:unittype
              Post(Event:Accepted,?unit_type_tick_temp)
          End!If MUL:UnitType <> ''
      
          If MUL:FaultDescription <> ''
              fault_description_tick_temp = 'Y'
              fault_description_temp  = mul:faultdescription
              Post(Event:Accepted,?fault_description_tick_temp)
          End!If MUL:FaultDescription <> ''
      
          If MUL:DOP <> ''
              tmp:DOPTick = 1
              tmp:dop = mul:dop
              Post(Event:Accepted,?tmp:DOPTick)
          End!If MUL:DOP <> ''
      
          If MUL:ChaChargeType <> ''
              charge_type_tick_temp = 'Y'
              Charge_Type_temp = mul:chachargetype
              Post(Event:Accepted,?Charge_Type_tick_temp)
          End!If MUL:ChaChargeType <> ''
      
          If MUL:ChaRepairType <> ''
              repair_type_tick_temp = 'Y'
              Repair_Type_temp = mul:ChaRepairType
              Post(Event:Accepted,?repair_type_tick_temp)
          End!If MUL:ChaRepairType <> ''
      
          If MUL:WarChargeType <> ''
              warranty_job_tick_temp = 'Y'
              warranty_charge_type_temp = mul:WarChargeType
              Post(Event:Accepted,?warranty_job_tick_temp)
          End!If MUL:WarChargeType <> ''
      
          If MUL:WarRepairType <> ''
              repair_type_warranty_tick_temp = 'Y'
              repair_type_warranty_temp = mul:WarRepairType
              Post(Event:Accepted,?repair_type_warranty_tick_temp)
          End!If MUL:WarRepairType <> ''
      
      end
      globalrequest     = saverequest#
      Display()
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ApplyDefault, Accepted)
    OF ?charge_type_tick_temp
      IF ?charge_type_tick_temp{Prop:Checked} = True
        UNHIDE(?Charge_Type_temp)
        UNHIDE(?Lookup_Charge_Type)
      END
      IF ?charge_type_tick_temp{Prop:Checked} = False
        Charge_Type_temp = ''
        HIDE(?Charge_Type_temp)
        HIDE(?Lookup_Charge_Type)
      END
      ThisWindow.Reset
    OF ?Lookup_Charge_Type
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Warranty_Charge_Types
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Charge_Type, Accepted)
      glo:select1 = ''
      case globalresponse
          of requestcompleted
              charge_type_temp = cha:charge_type
              Select(?+2)
          of requestcancelled
              charge_type_temp = ''
              Select(?-1)
      end!case globalreponse
      display(?charge_type_temp)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Charge_Type, Accepted)
    OF ?Charge_Type_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Charge_Type_temp, Accepted)
      access:chartype.clearkey(cha:warranty_key)
      cha:warranty    = 'NO'
      cha:charge_type = charge_type_temp
      if access:chartype.fetch(cha:warranty_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          glo:select1 = 'NO'
          browse_warranty_charge_types
          glo:select1 = ''
          if globalresponse = requestcompleted
              charge_type_temp = cha:charge_type
          else
              charge_type_temp = ''
              Select(?-1)
          end
          display(?charge_type_temp)
          globalrequest     = saverequest#
      end!if access:chartype.fetch(cha:warranty_key)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Charge_Type_temp, Accepted)
    OF ?repair_type_tick_temp
      IF ?repair_type_tick_temp{Prop:Checked} = True
        UNHIDE(?Repair_Type_temp)
        UNHIDE(?Lookup_Repair_Type)
      END
      IF ?repair_type_tick_temp{Prop:Checked} = False
        Repair_Type_temp = ''
        HIDE(?Repair_Type_temp)
        HIDE(?Lookup_Repair_Type)
      END
      ThisWindow.Reset
    OF ?Lookup_Repair_Type
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseREPAIRTY
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Repair_Type, Accepted)
      glo:select1 = ''
      case globalresponse
          of requestcompleted
              repair_type_temp = rep:repair_type
              Select(?+2)
          of requestcancelled
              repair_type_temp = ''
              Select(?-1)
      end!case globalreponse
      display(?repair_type_temp)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Repair_Type, Accepted)
    OF ?Repair_Type_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Repair_Type_temp, Accepted)
      access:repairty.clearkey(rep:model_chargeable_key)
      rep:model_number = model_number_temp
      rep:chargeable   = 'YES'
      rep:repair_type  = repair_type_temp
      if access:repairty.tryfetch(rep:model_chargeable_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          glo:select1 = model_number_temp
          browserepairty
          glo:select1 = ''
          if globalresponse = requestcompleted
              repair_type_temp = rep:repair_type
          else
              repair_type_temp = ''
              Select(?-1)
          end
          display(?repair_type_temp)
          globalrequest     = saverequest#
      end!if access:repairty.fetch(rep:model_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Repair_Type_temp, Accepted)
    OF ?warranty_job_tick_temp
      IF ?warranty_job_tick_temp{Prop:Checked} = True
        UNHIDE(?warranty_charge_type_temp)
        UNHIDE(?lookup_warranty_charge_type)
      END
      IF ?warranty_job_tick_temp{Prop:Checked} = False
        warranty_charge_type_temp = ''
        HIDE(?warranty_charge_type_temp)
        HIDE(?lookup_warranty_charge_type)
      END
      ThisWindow.Reset
    OF ?lookup_warranty_charge_type
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Warranty_Charge_Types
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lookup_warranty_charge_type, Accepted)
      glo:select1 = ''
      case globalresponse
          of requestcompleted
              warranty_charge_type_temp = cha:charge_type
              Select(?+2)
          of requestcancelled
              warranty_charge_type_temp = ''
              Select(?-1)
      end!case globalreponse
      display(?warranty_charge_type_temp)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lookup_warranty_charge_type, Accepted)
    OF ?warranty_charge_type_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?warranty_charge_type_temp, Accepted)
      access:chartype.clearkey(cha:warranty_key)
      cha:warranty    = 'YES'
      cha:charge_type = warranty_charge_type_temp
      if access:chartype.fetch(cha:warranty_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          glo:select1 = 'YES'
          browse_warranty_charge_types
          glo:select1 = ''
          if globalresponse = requestcompleted
              warranty_charge_type_temp = cha:charge_type
          else
              warranty_charge_type_temp = ''
              Select(?-1)
          end
          display(?warranty_charge_type_temp)
          globalrequest     = saverequest#
      end!if access:chartype.fetch(cha:warranty_key)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?warranty_charge_type_temp, Accepted)
    OF ?repair_type_warranty_tick_temp
      IF ?repair_type_warranty_tick_temp{Prop:Checked} = True
        UNHIDE(?repair_type_warranty_temp)
        UNHIDE(?Lookup_Repair_Type_Warranty)
      END
      IF ?repair_type_warranty_tick_temp{Prop:Checked} = False
        HIDE(?Lookup_Repair_Type_Warranty)
        HIDE(?repair_type_warranty_temp)
      END
      ThisWindow.Reset
    OF ?Lookup_Repair_Type_Warranty
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseREPAIRTY_Warranty
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Repair_Type_Warranty, Accepted)
      glo:select1 = ''
      case globalresponse
          of requestcompleted
              repair_type_warranty_temp = rep:repair_type
              Select(?+2)
          of requestcancelled
              repair_type_warranty_temp = ''
              Select(?-1)
      end!case globalreponse
      display(?repair_type_warranty_temp)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Repair_Type_Warranty, Accepted)
    OF ?repair_type_warranty_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?repair_type_warranty_temp, Accepted)
      access:repairty.clearkey(rep:model_warranty_key)
      rep:model_number = model_number_temp
      rep:warranty     = 'YES'
      rep:repair_type  = repair_type_warranty_temp
      if access:repairty.tryfetch(rep:model_warranty_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          glo:select1 = model_number_temp
          browserepairty_warranty
          glo:select1 = ''
          if globalresponse = requestcompleted
              repair_type_warranty_temp = rep:repair_type
          else
              repair_type_warranty_temp = ''
              Select(?-1)
          end
          display(?repair_type_warranty_temp)
          globalrequest     = saverequest#
      end!if access:repairty.fetch(rep:model_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?repair_type_warranty_temp, Accepted)
    OF ?auto_complete_temp
      IF ?auto_complete_temp{Prop:Checked} = True
        UNHIDE(?date_completed_temp)
        UNHIDE(?PopCalendar)
      END
      IF ?auto_complete_temp{Prop:Checked} = False
        date_completed_temp = ''
        HIDE(?date_completed_temp)
        HIDE(?PopCalendar)
      END
      ThisWindow.Reset
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          date_completed_temp = TINCALENDARStyle1(date_completed_temp)
          Display(?date_completed_temp)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?auto_despatch_temp
      IF ?auto_despatch_temp{Prop:Checked} = True
        UNHIDE(?courier_temp)
        UNHIDE(?Lookup_Courier)
      END
      IF ?auto_despatch_temp{Prop:Checked} = False
        courier_temp = ''
        HIDE(?courier_temp)
        HIDE(?Lookup_Courier)
      END
      ThisWindow.Reset
    OF ?courier_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?courier_temp, Accepted)
      access:courier.clearkey(cou:courier_key)
      cou:courier = courier_temp
      if access:courier.fetch(cou:courier_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_courier
          if globalresponse = requestcompleted
              courier_temp = cou:courier
          else
              courier_temp = ''
              Select(?-1)
          end
          display(?courier_temp)
          globalrequest     = saverequest#
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?courier_temp, Accepted)
    OF ?Account_Number_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Account_Number_temp, Accepted)
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number  = account_number_temp
      If access:subtracc.fetch(sub:account_number_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_sub_accounts
          if globalresponse = requestcompleted
              account_number_temp = sub:account_number
              GLO:Select43 = sub:account_number
              Do Trade_Account_Bit
          else
              account_number_temp = ''
          end
          display(?account_number_temp)
          globalrequest     = saverequest#
      Else!If access:subtracc.fetch(sub:account_number_key)
          Do Trade_Account_Bit
      End !If access:subtracc.fetch(sub:account_number_key) = Level:Benign
      Display()
      Do CheckRequiredFields
      IF Account_Number_temp OR ?Account_Number_temp{Prop:Req}
        sub:Account_Number = Account_Number_temp
        GLO:Select43 = sub:Account_Number
        !Save Lookup Field Incase Of error
        look:Account_Number_temp        = Account_Number_temp
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            Account_Number_temp = sub:Account_Number
          ELSE
            CLEAR(GLO:Select43)
            !Restore Lookup On Error
            Account_Number_temp = look:Account_Number_temp
            SELECT(?Account_Number_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Account_Number_temp, Accepted)
    OF ?Lookup_Account
      ThisWindow.Update
      sub:Account_Number = Account_Number_temp
      
      IF SELF.RUN(5,Selectrecord)  = RequestCompleted
          Account_Number_temp = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?Account_Number_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?Account_Number_temp)
    OF ?Transit_Type_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Transit_Type_temp, Accepted)
      IF Transit_Type_temp OR ?Transit_Type_temp{Prop:Req}
        trt:Transit_Type = Transit_Type_temp
        !Save Lookup Field Incase Of error
        look:Transit_Type_temp        = Transit_Type_temp
        IF Access:TRANTYPE.TryFetch(trt:Transit_Type_Key)
          IF SELF.Run(6,SelectRecord) = RequestCompleted
            Transit_Type_temp = trt:Transit_Type
          ELSE
            !Restore Lookup On Error
            Transit_Type_temp = look:Transit_Type_temp
            SELECT(?Transit_Type_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      Do Transit_Type_Bit
      Do CheckRequiredFields
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Transit_Type_temp, Accepted)
    OF ?Lookup_Transit_Type
      ThisWindow.Update
      trt:Transit_Type = Transit_Type_temp
      
      IF SELF.RUN(6,Selectrecord)  = RequestCompleted
          Transit_Type_temp = trt:Transit_Type
          Select(?+1)
      ELSE
          Select(?Transit_Type_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?Transit_Type_temp)
    OF ?Postcode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Postcode, Accepted)
      POstcode_Routine(postcode,address_line1,address_line2,address_line3)
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Postcode, Accepted)
    OF ?Postcode_Delivery
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Postcode_Delivery, Accepted)
      POstcode_routine(postcode_delivery,address_Line1_delivery,address_line2_delivery,address_Line3_Delivery)
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Postcode_Delivery, Accepted)
    OF ?Delivery_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Delivery_Button, Accepted)
      
          !! ** Bryan Harrison (c)1998 **
      
          glo:G_Select1 = ''
      
      glo:select1 = 'DELIVERY'
      If address_delivery_group <> ''
          glo:select2 = '1'
      Else
          glo:select2 = '2'
      End
      If address_collection_group = ''
          glo:select3 = 'COLLECTION'
      End
      Clear_Address_Window()
      Case glo:select1
          Of '1'
              Clear(address_collection_group)
              Select(?postcode_collection)
          Of '2'
              postcode_delivery      = postcode
              company_name_delivery  = company_name
              address_line1_delivery = address_line1
              address_line2_delivery = address_line2
              address_line3_delivery = address_line3
              telephone_delivery     = telephone_number
          Of '3'
              postcode_delivery      = postcode_collection
              company_name_delivery  = company_name_collection
              address_line1_delivery = address_line1_collection
              address_line2_delivery = address_line2_collection
              address_line3_Delivery = address_line3_collection
              telephone_delivery     = telephone_collection
          Of 'CANCEL'
      End
      Select(?postcode_delivery)
      Display()
      glo:select1 = ''
      glo:select2 = ''
      
          !! ** Bryan Harrison (c)1998 **
      
          glo:G_Select1 = ''
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Delivery_Button, Accepted)
    OF ?Postcode_Collection
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Postcode_Collection, Accepted)
      POstcode_routine(postcode_collection,address_Line1_collection,address_line2_collection,address_Line3_collection)
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Postcode_Collection, Accepted)
    OF ?Collection_Clear
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Collection_Clear, Accepted)
      
          !! ** Bryan Harrison (c)1998 **
      
          glo:G_Select1 = ''
      
      glo:select1 = 'COLLECTION'
      If address_collection_group <> ''
          glo:select2 = '1'
      Else
          glo:select2 = '2'
      End
      If address_delivery_group = ''
          glo:select3 = 'DELIVERY'
      End
      Clear_Address_Window()
      Case glo:select1
          Of '1'
              Clear(address_collection_group)
              Select(?postcode_collection)
          Of '2'
              postcode_collection      = postcode
              company_name_collection  = company_name
              address_line1_collection = address_line1
              address_line2_collection = address_line2
              address_line3_collection = address_line3
              telephone_collection     = telephone_number
          Of '4'
              postcode_collection      = postcode_delivery
              company_name_collection  = company_name_delivery
              address_line1_collection = address_line1_delivery
              address_line2_collection = address_line2_delivery
              address_line3_collection = address_line3_delivery
              telephone_collection     = telephone_delivery
          Of 'CANCEL'
      End
      Select(?postcode_collection)
      Display()
      glo:select1 = ''
      glo:select2 = ''
      
          !! ** Bryan Harrison (c)1998 **
      
          glo:G_Select1 = ''
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Collection_Clear, Accepted)
    OF ?use_batch_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use_batch_temp, Accepted)
      Case use_batch_temp
          Of 1
              Unhide(?batch_message)
              unhide(?batch_number_temp)
              ?batch_message{prop:text} = 'A Batch Number will be allocated'
              Hide(?batch_number_temp)
          Of 2
              batch_number_temp = ''
              Unhide(?batch_message)
              Unhide(?batch_number_temp)
              ?batch_message{prop:text} = 'Insert the Batch Number'
              Enable(?batch_number_temp)
          Of 3
              Hide(?batch_Message)
              Hide(?batch_number_temp)
              batch_number_temp = ''
      
      End
      Display()
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use_batch_temp, Accepted)
    OF ?ESN_temp:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ESN_temp:2, Accepted)
      If ~0{prop:acceptall}
          IF Len(Clip(ESN_Temp)) = 18
            !Ericsson IMEI!
            ESN_Temp = Sub(ESN_Temp,4,15)
            !ESN_Entry_Temp = Job:ESN
            Update()
          ELSE!IF Len(Clip(ESN_Temp)) = 18
            !Job:ESN = ESN_Entry_Temp
          END!IF Len(Clip(ESN_Temp)) = 18
      
          Set(DEFAULTS)
          Access:DEFAULTS.Next()
          Bouncers# = 0
          ?BouncerText{prop:Text} = ''
          ! If bouncers found, but ok, check for live bouncers - TrkBs: 6503 (DBH: 21-10-2005)
      
          If job:ESN <> 'N/A'
              Bouncers# = CountBouncer(0,Today(),ESN_Temp)
              If Bouncers# > 0
                  ?BouncerText{prop:Text} = Clip(Bouncers#) & ' Bouncer(s)'
              End ! If Bouncers# > 0
              If def:Allow_Bouncer <> 'YES'
                  If Bouncers# > 0
                      Case MessageEx('The I.M.E.I. Number has been entered onto the system before.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      ESN_Temp = ''
                      Error# = 1
                      ?BouncerText{prop:Text}= ''
                  End ! If Bouncers# > 0
              Else ! If def:Allow_Bouncer <> 'YES'
                  If Clip(GETINI('BOUNCER','StopLive',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
                      If LiveBouncers(0,Today(),ESN_Temp)
                          Case MessageEx('The selected I.M.E.I. is already entered onto a live job. You must complete this previous job before you can continue booking.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          Error# = 1
                          ESN_Temp = ''
                          ?BouncerText{prop:Text} = ''
                      End !If LiveBouncer(job:Ref_Number,job:Date_Booked,job:ESN)
                  End !If Clip(GETINI('BOUNCER','StopLive',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
              End ! If def:Allow_Bouncer <> 'YES'
      
          End ! If job:ESN <> 'N/A'
      
          Display()
      
          If Error# = 0
              If ExchangeAccount(Account_Number_Temp)
                  Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
                  xch:ESN = ESN_Temp
                  If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                      !Found
                      !Write the relevant fields to say this unit is in repair and take completed.
                      !Otherwise the use could click cancel and screw everything up.
                  Else!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                      Case MessageEx('The selected Trade Account is setup to repair Exchange Units, but the entered I.M.E.I. Number cannot be found in Exchange Stock.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      ESN_Temp = ''
                      Select(?ESN_Temp:2)
                      Error# = 1
                  End!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
              Else !ExchangeAccount(job:Account_Number)
                  Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
                  xch:ESN = ESN_Temp
                  If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
                      If xch:Available <> 'DES'
                          Case MessageEx('The entered I.M.E.I. matches that of an entry in the Exchange Database. This unit is not marked as "Despatched" so may still exist in your stock.'&|
                            '<13,10>'&|
                            '<13,10>If you continue, this job will be treated as a normal customer repair and not a "In House Exchange Repair". These "In House" repairs should be booked in using an Account that is marked as "Exchange Account".'&|
                            '<13,10>'&|
                            '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
                                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                              Of 1 ! &Yes Button
                              Of 2 ! &No Button
                                  ESN_Temp = ''
                                  Select(?ESN_Temp:2)
                                  Error# = 1
                          End!Case MessageEx
                      End!Case MessageEx
                  Else! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
                  
      
              End !ExchangeAccount(job:Account_Number)
              If Error# = 0
                  Model_Number_Temp   = IMEIModelRoutine(ESN_Temp,Model_Number_Temp)
                  access:modelnum.clearkey(mod:model_number_key)
                  mod:model_number = model_number_temp
                  if access:modelnum.fetch(mod:model_number_key) = Level:Benign
                      manufacturer_temp = mod:manufacturer
                  end
                  Do check_unit_type:2
                  Display()
      
              End !If Error# = 0
      
              Post(Event:Accepted,?Model_Number_Temp:2)
      
          End ! If Error# = 0
      
      
      End!If ~0{prop:acceptall}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ESN_temp:2, Accepted)
    OF ?MSN_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MSN_temp, Accepted)
          IF LEN(CLIP(MSN_Temp)) = 11 AND manufacturer_temp = 'ERICSSON'
            !Ericsson MSN!
            MSN_Temp = SUB(MSN_Temp,2,10)
            UPDATE()
          END
          Post(Event:Accepted,?Model_Number_Temp:2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MSN_temp, Accepted)
    OF ?LookupProductCode
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseProductCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupProductCode, Accepted)
      Case Globalresponse
          Of requestcompleted
              tmp:ProductCode = prd:ProductCode
              Select(?+1)
          Of requestcancelled
              tmp:ProductCode = ''
              Select(?-1)
      End
      Display(?tmp:ProductCode)
      Post(Event:Accepted,?Model_Number_Temp:2)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupProductCode, Accepted)
    OF ?tmp:ProductCode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ProductCode, Accepted)
      access:ProdCode.clearkey(prd:ProductCodeKey)
      prd:ProductCode           = tmp:ProductCode
      if access:ProdCode.Fetch(prd:ProductCodeKey)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          BrowseProductCodes
          if globalresponse = requestcompleted
              tmp:ProductCode = prd:ProductCode
          else
              tmp:ProductCode = ''
              Select(?-1)
          end
          display(?tmp:ProductCode)
          globalrequest     = saverequest#
      end!if access:locinter.fetch(loi:location_available_key)
      Post(Event:Accepted,?Model_Number_Temp:2)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ProductCode, Accepted)
    OF ?Lookup_Model_Number:2
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseMODELNUM
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Model_Number:2, Accepted)
      Case Globalresponse
          Of requestcompleted
              model_number_temp = mod:model_number
              manufacturer_temp = mod:manufacturer
              Select(?+2)
              Do Check_Unit_Type:2
          Of requestcancelled
              model_number_temp = ''
              Select(?-1)
      End
      Display(?model_number_temp:2)
      Display(?unit_Type_temp:2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Model_Number:2, Accepted)
    OF ?Model_Number_temp:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Model_Number_temp:2, Accepted)
      If CheckLength('IMEI',Model_Number_Temp,ESN_temp)
          ESN_Temp = ''
      End !CheckLength('IMEI',Model_Number_Temp,ESN_temp)
      If ?MSN_Temp{prop:Hide} = 0
          If CheckLength('MSN',Model_Number_Temp,MSN_Temp)
              MSN_Temp = ''
          End !If CheckLength('MSN',Model_Number_Temp,MSN_Temp)
      End !?MSN_Temp{prop:Hide} = 0
      
      error# = 0
      access:modelnum.clearkey(mod:model_number_key)
      mod:model_number = model_number_temp
      if access:modelnum.fetch(mod:model_number_key)
      
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browsemodelnum
          if globalresponse = requestcompleted
              model_number_temp = mod:model_number
              manufacturer_temp = mod:manufacturer
          else
              model_number_temp = ''
              manufacturer_temp = ''
              error# = 1
              Select(?-1)
          end
          display(?model_number_temp:2)
          globalrequest     = saverequest#
      Else!if access:modelnum.fetch(mod:model_number_key)
          manufacturer_temp = mod:manufacturer
      End!if access:modelnum.fetch(mod:model_number_key)
      
      If error# = 0
          Do check_unit_type:2
      End!If error# = 0
      Display()
      
      IF manufacturer_temp <> ''
        If ?tmp:ProductCode{prop:Hide} = 1
            If manufacturer_temp = 'SIEMENS' Or manufacturer_temp = 'NOKIA'  Or manufacturer_temp = 'MOTOROLA'
                GLO:Select13 = tmp:ProductCode
            End!If job:manufacturer = 'SIEMENS' Or job:manufacturer = 'NOKIA' Or job:manufacturer = 'MOTOROLA'
        Else !If ?job:ProductCode{prop:Hide} = 1
            If manufacturer_temp = 'SIEMENS' Or manufacturer_temp = 'NOKIA'  Or manufacturer_temp = 'MOTOROLA'
                GLO:Select13 = tmp:ProductCode
            End!If job:manufacturer = 'SIEMENS' Or job:manufacturer = 'NOKIA' Or job:manufacturer = 'MOTOROLA'
        End !If ?job:ProductCode{prop:Hide} = 1
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Model_Number_temp:2, Accepted)
    OF ?Lookup_unit_Type:2
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseUNITTYPE
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_unit_Type:2, Accepted)
      case globalresponse
          of requestcompleted
              unit_type_temp = uni:unit_type
              Select(?+2)
          of requestcancelled
              unit_type_temp = ''
              Select(?-1)
      end!case globalreponse
      display(?unit_type_temp:2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_unit_Type:2, Accepted)
    OF ?Unit_Type_temp:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Unit_Type_temp:2, Accepted)
      access:unittype.clearkey(uni:unit_type_key)
      uni:unit_type = unit_type_temp
      if access:unittype.fetch(uni:unit_type_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browseunittype
          if globalresponse = requestcompleted
              unit_type_temp = uni:unit_type
          else
              unit_type_temp = ''
              Select(?-1)
          end
          display(?unit_type_temp:2)
          globalrequest     = saverequest#
      end!if access:unittype.fetch(uni:unit_type_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Unit_Type_temp:2, Accepted)
    OF ?Lookup_Accessories
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Accessories, Accepted)
      If model_number_temp = ''
          beep(beep:systemhand)  ;  yield()
          message('You must enter a model number.', |
                  'ServiceBase 2000', icon:hand)
      Else
          count# = 0
          save_jactmp_id = access:jobactmp.savefile()
          set(jactmp:accessory_key)
          loop
              if access:jobactmp.next()
                 break
              end !if
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              count# = 1
              Break
          end !loop
          access:jobactmp.restorefile(save_jactmp_id)
      
          glo:select1  = model_number_temp
      
          If count# = 0
              Rapid_Temp_Accessory_Insert
          Else!If count# = 0
              Browse_temp_accessories
          End!If count# = 0
      
          glo:select1  = ''
      
          Do Update_Accessories
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Accessories, Accepted)
    OF ?Lookup_Internal_Location:2
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Available_Locations
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Internal_Location:2, Accepted)
      Case Globalresponse
          Of requestcompleted
              internal_location_temp = loi:location
              Select(?+2)
          Of requestcancelled
              Select(?-1)
      End!Case Globalrequest
      Display(?internal_location_temp:2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Internal_Location:2, Accepted)
    OF ?Internal_Location_Temp:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Internal_Location_Temp:2, Accepted)
      access:locinter.clearkey(loi:location_available_key)
      loi:location_available = 'YES'
      loi:location           = internal_location_temp
      if access:locinter.fetch(loi:location_available_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_available_locations
          if globalresponse = requestcompleted
              internal_location_temp = loi:location
          else
              internal_location_temp = ''
              Select(?-1)
          end
          display(?internal_location_temp:2)
          globalrequest     = saverequest#
      end!if access:locinter.fetch(loi:location_available_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Internal_Location_Temp:2, Accepted)
    OF ?fault_description_text_2
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Fault_Description_Text
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?fault_description_text_2, Accepted)
      glo:select1 = ''
      If Records(glo:Q_Notes) <> ''
          Loop x$ = 1 To Records(glo:Q_Notes)
              Get(glo:Q_Notes,x$)
              If fault_description_temp = ''
                  fault_description_temp    = Clip(glo:notes_pointer)
              Else !If job:invoice_text = ''
                  fault_description_temp = Clip(fault_description_temp) & '<13,10>' & Clip(glo:notes_pointer)
              End !If job:invoice_text = ''
          End
          Display()
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?fault_description_text_2, Accepted)
    OF ?Mobile_Number_temp:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Mobile_Number_temp:2, Accepted)
      ! Start Change 2821 MobileNumberValidate BE (23/06/03)
       If ~0{prop:acceptall}
          len# = LEN(CLIP(Mobile_Number_temp))
          iy# = 0
          mobile_error# = 0
           LOOP ix# = 1 TO len#
              IF (Mobile_Number_temp[ix#] <> ' ') THEN
                  ! Start Change 2897 BE(04/08/03)
                  !IF ((job:mobile_number[ix#] < '0') OR (Mobile_Number_temp[ix#] > '9')) THEN
                  IF ((Mobile_Number_temp[ix#] < '0') OR (Mobile_Number_temp[ix#] > '9')) THEN
                  ! End Change 2897 BE(04/08/03)
                      mobile_error# = 1
                  END
                  iy# += 1
                  temp_255string[iy#] = Mobile_Number_temp[ix#]
              END
          END
      
          ! Check for Validation error here... (mobile_error#)
          IF ((ValidateMobileNumber) AND (mobile_error# = 1)) THEN
              MessageEx('Mobile Number must be numeric','ServiceBase 2000',|
                      'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,|
                      '',0,beep:systemhand,msgex:samewidths,84,26,0)
              SELECT(?Mobile_Number_temp:2)
              Mobile_Number_Temp = ''
              CYCLE
          ELSE
              IF (iy# > 0) THEN
                  Mobile_Number_temp = temp_255string[1 : iy#]
                  Mobile_Number_temp[1] = UPPER(Mobile_Number_temp[1])
              ELSE
                  Mobile_Number_temp = ''
              END
              Display(?Mobile_Number_temp:2)
          END
       END
       ! End Change MobileNumberValidate BE (23/06/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Mobile_Number_temp:2, Accepted)
    OF ?Lookup_Charge_Type:2
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Warranty_Charge_Types
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Charge_Type:2, Accepted)
      glo:select1 = ''
      case globalresponse
          of requestcompleted
              charge_type_temp = cha:charge_type
              Select(?+2)
          of requestcancelled
              charge_type_temp = ''
              Select(?-1)
      end!case globalreponse
      display(?charge_type_temp:2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Charge_Type:2, Accepted)
    OF ?Charge_Type_temp:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Charge_Type_temp:2, Accepted)
      access:chartype.clearkey(cha:warranty_key)
      cha:warranty    = 'NO'
      cha:charge_type = charge_type_temp
      if access:chartype.fetch(cha:warranty_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          glo:select1 = 'NO'
          browse_warranty_charge_types
          glo:select1 = ''
          if globalresponse = requestcompleted
              charge_type_temp = cha:charge_type
          else
              charge_type_temp = ''
              Select(?-1)
          end
          display(?charge_type_temp:2)
          globalrequest     = saverequest#
      end!if access:chartype.fetch(cha:warranty_key)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Charge_Type_temp:2, Accepted)
    OF ?Lookup_Repair_Type:2
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseREPAIRTY
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Repair_Type:2, Accepted)
      glo:select1 = ''
      case globalresponse
          of requestcompleted
              repair_type_temp = rep:repair_type
              Select(?+2)
          of requestcancelled
              repair_type_temp = ''
              Select(?-1)
      end!case globalreponse
      display(?repair_type_temp:2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Repair_Type:2, Accepted)
    OF ?Repair_Type_temp:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Repair_Type_temp:2, Accepted)
      access:repairty.clearkey(rep:model_chargeable_key)
      rep:model_number = model_number_temp
      rep:chargeable   = 'YES'
      rep:repair_type  = repair_Type_temp
      if access:repairty.tryfetch(rep:model_chargeable_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          glo:select1 = model_number_temp
          browserepairty
          glo:select1 = ''
          if globalresponse = requestcompleted
              repair_type_temp = rep:repair_type
          else
              repair_type_temp = ''
              Select(?-1)
          end
          display(?repair_type_temp:2)
          globalrequest     = saverequest#
      end!if access:repairty.fetch(rep:model_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Repair_Type_temp:2, Accepted)
    OF ?Lookup_Warranty_Charge_Type:2
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Warranty_Charge_Types
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Warranty_Charge_Type:2, Accepted)
      glo:select1 = ''
      case globalresponse
          of requestcompleted
              warranty_charge_type_temp = cha:charge_type
              Select(?+2)
          of requestcancelled
              warranty_charge_type_temp = ''
              Select(?-1)
      end!case globalreponse
      display(?warranty_charge_type_temp:2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Warranty_Charge_Type:2, Accepted)
    OF ?warranty_charge_type_temp:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?warranty_charge_type_temp:2, Accepted)
      access:chartype.clearkey(cha:warranty_key)
      cha:warranty    = 'YES'
      cha:charge_type = warranty_charge_type_temp
      if access:chartype.fetch(cha:warranty_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          glo:select1 = 'YES'
          browse_warranty_charge_types
          glo:select1 = ''
          if globalresponse = requestcompleted
              warranty_charge_type_temp = cha:charge_type
          else
              warranty_charge_type_temp = ''
              Select(?-1)
          end
          display(?warranty_charge_type_temp:2)
          globalrequest     = saverequest#
      end!if access:chartype.fetch(cha:warranty_key)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?warranty_charge_type_temp:2, Accepted)
    OF ?Lookup_Warranty_Repair_Type
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseREPAIRTY_Warranty
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Warranty_Repair_Type, Accepted)
      glo:select1 = ''
      case globalresponse
          of requestcompleted
              repair_type_warranty_temp = rep:repair_type
              Select(?+2)
          of requestcancelled
              repair_type_warranty_temp = ''
              Select(?-1)
      end!case globalreponse
      display(?repair_type_warranty_temp:2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Warranty_Repair_Type, Accepted)
    OF ?repair_type_warranty_temp:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?repair_type_warranty_temp:2, Accepted)
      access:repairty.clearkey(rep:model_warranty_key)
      rep:model_number = model_number_temp
      rep:warranty     = 'YES'
      rep:repair_type  = repair_type_warranty_temp
      if access:repairty.tryfetch(rep:model_warranty_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          glo:select1 = model_number_temp
          browserepairty_warranty
          glo:select1 = ''
          if globalresponse = requestcompleted
              repair_type_warranty_temp = rep:repair_type
          else
              repair_type_warranty_temp = ''
              Select(?-1)
          end
          display(?repair_type_warranty_temp:2)
          globalrequest     = saverequest#
      end!if access:repairty.fetch(rep:model_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?repair_type_warranty_temp:2, Accepted)
    OF ?save_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?save_Button, Accepted)
      Do Check_Defaults
      If error_temp = 0
      
          Do Create_Job
          IF tmp:jobCreated
              Do Clear_Fields
              number_temp += 1
              
              Display()
              thiswindow.reset
      
              Beep(beep:systemasterisk)
              Do Select_Fields
              Do check_msn
          End !IF tmp:jobCreated
      
      EnD!If error_temp = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?save_Button, Accepted)
    OF ?Insert_Multiple
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert_Multiple, Accepted)
      Do Check_Defaults
      If error_temp = 0
          glo:select24 = ''
          glo:select23 = ''
          Multiple_Job_Number
          If glo:select23 = 'Y'
      !Check Location
              error# = 0
              access:locinter.clearkey(loi:location_key)
              loi:location = internal_location_temp
              if access:locinter.fetch(loi:location_key) = Level:Benign
                  If Clip(glo:select24) > Clip(loi:current_spaces)
                      Case MessageEx('The selected location has only ' & Clip(loi:Current_Spaces) & ' free spaces. Do you want to adjust the amount of jobs, or selecte a new location?','ServiceBase 2000',|
                                     'Styles\question.ico','|&Adjust Amount|&Select Location|&Cancel',3,3,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                          Of 1 ! &Adjust Amount Button
                              glo:Select24    = loi:Current_Spaces
                          Of 2 ! &Select Location Button
                              Error# = 1
                          Of 3 ! &Cancel Button
                              Error# = 1
                      End!Case MessageEx
                  End!If glo:select24 > loi:current_spaces
              end!if access:locinter.fetch(loi:location_key) = Level:Benign
      
              If error# = 0
                  Case MessageEx('Are you sure you want to create ' & clip(glo:Select24) & ' jobs?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                          setcursor(cursor:wait)
                          Loop x$ = 1 To glo:select24
                              number_temp += 1
                              Do GetNextRecord2
                              Do Create_Job
                              If ~tmp:JobCreated
                                  Break
                              End !If ~tmp:JobCreated
                              Thiswindow.reset
                          End!Loop x$ = 1 To glo:select24
                          setcursor()
                          If tmp:JobCreated
                              Case MessageEx('Jobs Inserted.','ServiceBase 2000',|
                                             'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                              Do clear_fields
                          End !If tmp:JobCreated
                      Of 2 ! &No Button
                  End!Case MessageEx
              End!If error# = 0
          End
      End!If error_temp = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert_Multiple, Accepted)
    OF ?Save_Finish_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Save_Finish_Button, Accepted)
      Do Check_Defaults
      If error_temp = 0
          Do Create_Job
          If tmp:JobCreated
              If Records(glo:Queue)
                  Case MessageEx('Do you wish to print a report of all Jobs inserted?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                          Multiple_Job_Booking_Report
                      Of 2 ! &No Button
                  End!Case MessageEx
      
              End !If Records(glo:Queue)
              POst(Event:Closewindow)
      
          End !If tmp:JobCreated
      End!If error_temp = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Save_Finish_Button, Accepted)
    OF ?VSBackButton
      ThisWindow.Update
         Wizard4.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VSNextButton, Accepted)
      tmp:do_next = 1
      Case Choice(?Sheet1)
          Of 1
              If order_number_tick_temp = 'Y' and order_number_temp = ''
                  Select(?order_number_temp)
                  tmp:do_next = 0
              End!If order_number_tick_temp = 'Y'
      
              If tmp:do_next = 1 and internal_location_tick_temp = 'Y' and internal_location_temp = ''
                  Select(?internal_location_temp)
                  tmp:do_next = 0
              End!If internal_location_tick_temp 'Y' and internal_location_temp = ''
      
              If tmp:do_next = 1 And model_number_tick_temp = 'Y' and model_number_temp = ''
                  Select(?model_number_temp)
                  tmp:do_next = 0
              End!If tmp:do_next = 1 And model_number_tick_temp = 'Y' and model_number_temp = ''
      
              If tmp:do_next = 1 And colour_tick_temp = 'Y' and colour_temp = ''
                  Select(?colour_temp)
                  tmp:do_next = 0
              End!If tmp:do_next = 1 And colour_tick_temp = 'Y' and colour_temp = ''
      
              If tmp:do_next = 1 And unit_type_tick_temp = 'Y' and unit_type_temp = ''
                  Select(?unit_type_temp)
                  tmp:do_next = 0
              End!If tmp:do_next = 1 And unit_type_tick_temp = 'Y' and unit_type_temp = ''
      
              If tmp:do_next = 1 And esn_tick_temp = 'Y'
                  If esn_temp = ''
                      Select(?esn_temp)
                      tmp:do_next = 0
                  Else!If esn_temp = ''
                      Post(event:accepted,?esn_temp)
                  End!If esn_temp = ''
              End!If tmp:do_next = 1 And esn_tick_temp = 'Y'
      
              If tmp:do_next = 1 And fault_description_tick_temp = 'Y' And fault_description_temp = ''
                  Select(?fault_description_temp)
                  tmp:do_next = 0
              End!If tmp:do_next = 1 And fault_description_tick_temp = 'Y' And fault_description_temp = ''
      
              If tmp:do_next = 1 And tmp:DOPtick = 1 and tmp:dop = ''
                  Select(?tmp:dop)
                  tmp:do_next = 0
              End!If tmp:do_next = 1 And tmp:DOPtick = 1 and tmp:dop = ''
      
              ! Start Change 4765 BE(26/10/2004)
              IF (tmp:do_next =1 AND Consignment_Number_Tick_Temp = 1 AND Consignment_Number_Temp = '') THEN
                  SELECT(?Consignment_Number_Temp)
                  tmp:do_next = 0
              END
      
              IF (tmp:do_next =1 AND Consignment_Date_Tick_Temp = 1 AND Consignment_Date_Temp = '') THEN
                  SELECT(?Consignment_Date_Temp)
                  tmp:do_next = 0
              END
              ! End Change 4765 BE(26/10/2004)
          Of 2
              If charge_type_tick_temp = 'Y' and charge_type_temp = ''
                  Select(?charge_type_temp)
                  tmp:do_next = 0
              End!If charge_type_tick_temp = 'Y' and charge_type_temp = ''
      
              If tmp:do_next = 1 and warranty_job_tick_temp = 'Y' and warranty_charge_type_temp = ''
                  Select(?warranty_charge_type_temp)
                  tmp:do_next = 0
              End!If tmp:do_next = 1 and warranty_job_tick_temp = 'Y' and warraty_charge_type_temp = ''
      
              If tmp:do_next = 1 And repair_type_tick_temp = 'Y' and repair_type_temp = ''
                  Select(?repair_type_temp)
                  tmp:do_next = 0
              End!If tmp:do_next = 1 And repair_type_tick_temp = 'Y' and repair_type_temp = ''
      
              If tmp:do_next = 1 And repair_type_warranty_tick_temp = 'Y' And repair_type_warranty_temp = ''
                  Select(?repair_type_warranty_temp)
                  tmp:do_next = 0
              End!If tmp:do_next = 1 And repair_type_warranty_tick_temp = 'Y' And repair_type_warranty_temp = ''
      
              If tmp:do_next = 1 And auto_complete_temp = 'Y' and date_Completed_temp = ''
                  Select(?date_completed_temp)
                  tmp:do_next = 0
              End!If tmp:do_next = 1 And auto_complete_temp = 'Y' and date_Completed_temp = ''
      
              If tmp:do_next = 1 And auto_despatch_temp = 'Y' and courier_temp = ''
                  Select(?Courier_temp)
                  tmp:do_next = 0
              End!If tmp:do_next = 1 And auto_despatch_temp = 'Y' and courier_temp = ''
      
          Of 3
              If account_number_temp = ''
                  Select(?account_number_temp)
                  tmp:do_next = 0
              End!If account_number_temp = ''
      
              If tmp:do_next = 1 And transit_type_temp = ''
                  Select(?transit_type_temp)
                  tmp:do_next = 0
              End!If tmp:do_next = 1 And transit_type_temp = ''
      End!Case Choice(?Sheet1)
      
      If tmp:do_next = 1
      
         Wizard4.TakeAccepted()
      End!If tmp:do_next = 1
      Do CheckRequiredFields
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VSNextButton, Accepted)
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      unhide(?tab6)
      Select(?Sheet1,6)
      If use_batch_temp = 1
          get(jobbatch,0)
          if access:jobbatch.primerecord() = level:benign
      
              if access:jobbatch.insert()
                  access:jobbatch.cancelautoinc()
              Else
                  batch_number_temp = jbt:batch_number
              end
          end!if access:jobbatch.primerecord() = level:benign
      End!If use_batch_number_temp = 1
      Do Select_Fields
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    OF ?Close
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Close, Accepted)
      Case MessageEx('Are you sure you want to close?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
          
              If Records(glo:Queue)
                  Case MessageEx('Do you wish to print a report of all Jobs inserted?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                          Multiple_Job_Booking_Report
                      Of 2 ! &No Button
                  End!Case MessageEx
              End !Records(glo:Queue)
              Post(Event:CloseWindow)
      Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Close, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Multiple_Job_Wizard')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?date_completed_temp
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Internal_Location_Temp
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Internal_Location_Temp, AlertKey)
      Post(event:accepted,?lookup_location)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Internal_Location_Temp, AlertKey)
    END
  OF ?Model_Number_temp
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Model_Number_temp, AlertKey)
      Post(event:accepted,?lookup_model_number:3)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Model_Number_temp, AlertKey)
    END
  OF ?date_completed_temp
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?date_completed_temp, AlertKey)
      date_completed_temp = Today()
      Display(?date_completed_temp)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?date_completed_temp, AlertKey)
    END
  OF ?Account_Number_temp
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Account_Number_temp, AlertKey)
      Post(event:accepted,?Lookup_account)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Account_Number_temp, AlertKey)
    END
  OF ?Transit_Type_temp
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Transit_Type_temp, AlertKey)
      Post(event:accepted,?Lookup_Transit_Type)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Transit_Type_temp, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Set(defaults)
      access:defaults.next()
      If def:use_job_label = 'YES'
          print_job_label_temp = 'YES'
      Else!If def:use_job_label = 'YES'
          print_job_label_temp = 'NO'
      End!If def:use_job_label = 'YES'
      
      If def:show_mobile_number = 'YES'
      !          Hide(?mobile_number_tick_temp)
      !    Hide(?mobile_number_temp)
          Hide(?mobile_number_temp:2)
          Hide(?mobile_number:prompt)
      Else
      !          UnHide(?mobile_number_tick_temp)
      !    UnHide(?mobile_number_temp)
          UnHide(?mobile_number_temp:2)
          Unhide(?mobile_number:prompt)
      End
      
      If def:HideProduct = 'YES'
          ?tmp:ProductCode{prop:Hide} = 1
          ?tmp:ProductCode:Prompt{prop:Hide} = 1
          ?LookupProductCode{prop:Hide} = 1
      End !def:HideProduct = 'YES'
      
      Clear(glo:G_Select1)
      Clear(glo:G_Select1)
      Clear(glo:Queue)
      Free(glo:Queue)
      
      If GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          ?Colour_Tick_Temp{prop:Text} = Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI'))
      End !GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      
      ! Start Change 2821 BE(24/06/03)
      ValidateMobileNumber = GETINI('VALIDATE','MobileNumber',,CLIP(PATH())&'\SB2KDEF.INI')
      ! End Change 2821 BE(24/06/03)
      Do CheckRequiredFields
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Wizard4.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()


Wizard4.TakeBackEmbed PROCEDURE
   CODE

Wizard4.TakeNextEmbed PROCEDURE
   CODE

Wizard4.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
