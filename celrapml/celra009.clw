

   MEMBER('celrapml.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA009.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CELRA005.INC'),ONCE        !Req'd for module callout resolution
                     END


UpdateMultiDefaults PROCEDURE                         !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::mul:Record  LIKE(mul:RECORD),STATIC
QuickWindow          WINDOW('Update Multiple Booking Defaults'),AT(,,227,255),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('UpdateMultiDefaults'),SYSTEM,GRAY,DOUBLE,MDI
                       SHEET,AT(4,4,220,220),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Description'),AT(8,20),USE(?MUL:Description:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(84,20,124,10),USE(mul:Description),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Description'),TIP('Description'),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),REQ,UPR
                           PROMPT('Model Number'),AT(8,36),USE(?MUL:ModelNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,36,124,10),USE(mul:ModelNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Model Number'),TIP('Model Number'),ALRT(DownKey),ALRT(MouseRight),ALRT(MouseLeft2),ALRT(EnterKey),UPR
                           BUTTON,AT(212,36,10,10),USE(?LookupModelNumber),SKIP,ICON('LIST3.ICO')
                           PROMPT('Unit Type'),AT(8,52),USE(?MUL:UnitType:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,52,124,10),USE(mul:UnitType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Unit Type'),TIP('Unit Type'),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(212,52,10,10),USE(?LookupUnitType),SKIP,ICON('LIST3.ICO')
                           PROMPT('Order Number'),AT(8,68),USE(?MUL:OrderNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,68,124,10),USE(mul:OrderNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Order Number'),TIP('Order Number'),UPR
                           PROMPT('Internal Location'),AT(8,84),USE(?MUL:InternalLocation:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,84,124,10),USE(mul:InternalLocation),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Internal Location'),TIP('Internal Location'),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(212,84,10,10),USE(?LookupLocation),SKIP,ICON('LIST3.ICO')
                           PROMPT('Fault Description'),AT(8,100),USE(?MUL:InternalLocation:Prompt:2),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           TEXT,AT(84,100,124,36),USE(mul:FaultDescription),VSCROLL,FONT(,8,,FONT:bold),UPR,MSG('Fault Description')
                           BUTTON,AT(212,100,10,10),USE(?LookupFaultDescription),SKIP,ICON('List3.ico')
                           PROMPT('Date Of Purchase'),AT(8,144),USE(?MUL:DOP:Prompt)
                           ENTRY(@d6),AT(84,144,64,10),USE(mul:DOP),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Date Of Purchase'),TIP('Date Of Purchase'),UPR
                           PROMPT('Char Charge Type'),AT(8,160),USE(?MUL:ChaChargeType:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,160,124,10),USE(mul:ChaChargeType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Chargeable Charge Type'),TIP('Chargeable Charge Type'),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(212,160,10,10),USE(?LookupChargeType),SKIP,ICON('LIST3.ICO')
                           PROMPT('Char Repair Type'),AT(8,176),USE(?MUL:ChaRepairType:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,176,124,10),USE(mul:ChaRepairType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Chargeable Repair Type'),TIP('Chargeable Repair Type'),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(212,176,10,10),USE(?LookupRepairType),SKIP,ICON('LIST3.ICO')
                           PROMPT('Warr Charge Type'),AT(8,192),USE(?MUL:WarChargeType:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,192,124,10),USE(mul:WarChargeType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Warranty Charge Type'),TIP('Warranty Charge Type'),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(212,192,10,10),USE(?LookupWChargeType),SKIP,ICON('LIST3.ICO')
                           PROMPT('Warr Repair Type'),AT(8,208),USE(?MUL:WarRepairType:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,208,124,10),USE(mul:WarRepairType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Warranty Repair Type'),TIP('Warranty Repair Type'),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),UPR
                           BUTTON,AT(212,208,10,10),USE(?LookupWRepairType),SKIP,ICON('LIST3.ICO')
                         END
                       END
                       BUTTON('&OK'),AT(108,232,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       PANEL,AT(4,228,220,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Cancel'),AT(164,232,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:mul:ModelNumber                Like(mul:ModelNumber)
look:mul:UnitType                Like(mul:UnitType)
look:mul:InternalLocation                Like(mul:InternalLocation)
look:mul:ChaChargeType                Like(mul:ChaChargeType)
look:mul:ChaRepairType                Like(mul:ChaRepairType)
look:mul:WarChargeType                Like(mul:WarChargeType)
look:mul:WarRepairType                Like(mul:WarRepairType)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?MUL:Description:Prompt{prop:FontColor} = -1
    ?MUL:Description:Prompt{prop:Color} = 15066597
    If ?mul:Description{prop:ReadOnly} = True
        ?mul:Description{prop:FontColor} = 65793
        ?mul:Description{prop:Color} = 15066597
    Elsif ?mul:Description{prop:Req} = True
        ?mul:Description{prop:FontColor} = 65793
        ?mul:Description{prop:Color} = 8454143
    Else ! If ?mul:Description{prop:Req} = True
        ?mul:Description{prop:FontColor} = 65793
        ?mul:Description{prop:Color} = 16777215
    End ! If ?mul:Description{prop:Req} = True
    ?mul:Description{prop:Trn} = 0
    ?mul:Description{prop:FontStyle} = font:Bold
    ?MUL:ModelNumber:Prompt{prop:FontColor} = -1
    ?MUL:ModelNumber:Prompt{prop:Color} = 15066597
    If ?mul:ModelNumber{prop:ReadOnly} = True
        ?mul:ModelNumber{prop:FontColor} = 65793
        ?mul:ModelNumber{prop:Color} = 15066597
    Elsif ?mul:ModelNumber{prop:Req} = True
        ?mul:ModelNumber{prop:FontColor} = 65793
        ?mul:ModelNumber{prop:Color} = 8454143
    Else ! If ?mul:ModelNumber{prop:Req} = True
        ?mul:ModelNumber{prop:FontColor} = 65793
        ?mul:ModelNumber{prop:Color} = 16777215
    End ! If ?mul:ModelNumber{prop:Req} = True
    ?mul:ModelNumber{prop:Trn} = 0
    ?mul:ModelNumber{prop:FontStyle} = font:Bold
    ?MUL:UnitType:Prompt{prop:FontColor} = -1
    ?MUL:UnitType:Prompt{prop:Color} = 15066597
    If ?mul:UnitType{prop:ReadOnly} = True
        ?mul:UnitType{prop:FontColor} = 65793
        ?mul:UnitType{prop:Color} = 15066597
    Elsif ?mul:UnitType{prop:Req} = True
        ?mul:UnitType{prop:FontColor} = 65793
        ?mul:UnitType{prop:Color} = 8454143
    Else ! If ?mul:UnitType{prop:Req} = True
        ?mul:UnitType{prop:FontColor} = 65793
        ?mul:UnitType{prop:Color} = 16777215
    End ! If ?mul:UnitType{prop:Req} = True
    ?mul:UnitType{prop:Trn} = 0
    ?mul:UnitType{prop:FontStyle} = font:Bold
    ?MUL:OrderNumber:Prompt{prop:FontColor} = -1
    ?MUL:OrderNumber:Prompt{prop:Color} = 15066597
    If ?mul:OrderNumber{prop:ReadOnly} = True
        ?mul:OrderNumber{prop:FontColor} = 65793
        ?mul:OrderNumber{prop:Color} = 15066597
    Elsif ?mul:OrderNumber{prop:Req} = True
        ?mul:OrderNumber{prop:FontColor} = 65793
        ?mul:OrderNumber{prop:Color} = 8454143
    Else ! If ?mul:OrderNumber{prop:Req} = True
        ?mul:OrderNumber{prop:FontColor} = 65793
        ?mul:OrderNumber{prop:Color} = 16777215
    End ! If ?mul:OrderNumber{prop:Req} = True
    ?mul:OrderNumber{prop:Trn} = 0
    ?mul:OrderNumber{prop:FontStyle} = font:Bold
    ?MUL:InternalLocation:Prompt{prop:FontColor} = -1
    ?MUL:InternalLocation:Prompt{prop:Color} = 15066597
    If ?mul:InternalLocation{prop:ReadOnly} = True
        ?mul:InternalLocation{prop:FontColor} = 65793
        ?mul:InternalLocation{prop:Color} = 15066597
    Elsif ?mul:InternalLocation{prop:Req} = True
        ?mul:InternalLocation{prop:FontColor} = 65793
        ?mul:InternalLocation{prop:Color} = 8454143
    Else ! If ?mul:InternalLocation{prop:Req} = True
        ?mul:InternalLocation{prop:FontColor} = 65793
        ?mul:InternalLocation{prop:Color} = 16777215
    End ! If ?mul:InternalLocation{prop:Req} = True
    ?mul:InternalLocation{prop:Trn} = 0
    ?mul:InternalLocation{prop:FontStyle} = font:Bold
    ?MUL:InternalLocation:Prompt:2{prop:FontColor} = -1
    ?MUL:InternalLocation:Prompt:2{prop:Color} = 15066597
    If ?mul:FaultDescription{prop:ReadOnly} = True
        ?mul:FaultDescription{prop:FontColor} = 65793
        ?mul:FaultDescription{prop:Color} = 15066597
    Elsif ?mul:FaultDescription{prop:Req} = True
        ?mul:FaultDescription{prop:FontColor} = 65793
        ?mul:FaultDescription{prop:Color} = 8454143
    Else ! If ?mul:FaultDescription{prop:Req} = True
        ?mul:FaultDescription{prop:FontColor} = 65793
        ?mul:FaultDescription{prop:Color} = 16777215
    End ! If ?mul:FaultDescription{prop:Req} = True
    ?mul:FaultDescription{prop:Trn} = 0
    ?mul:FaultDescription{prop:FontStyle} = font:Bold
    ?MUL:DOP:Prompt{prop:FontColor} = -1
    ?MUL:DOP:Prompt{prop:Color} = 15066597
    If ?mul:DOP{prop:ReadOnly} = True
        ?mul:DOP{prop:FontColor} = 65793
        ?mul:DOP{prop:Color} = 15066597
    Elsif ?mul:DOP{prop:Req} = True
        ?mul:DOP{prop:FontColor} = 65793
        ?mul:DOP{prop:Color} = 8454143
    Else ! If ?mul:DOP{prop:Req} = True
        ?mul:DOP{prop:FontColor} = 65793
        ?mul:DOP{prop:Color} = 16777215
    End ! If ?mul:DOP{prop:Req} = True
    ?mul:DOP{prop:Trn} = 0
    ?mul:DOP{prop:FontStyle} = font:Bold
    ?MUL:ChaChargeType:Prompt{prop:FontColor} = -1
    ?MUL:ChaChargeType:Prompt{prop:Color} = 15066597
    If ?mul:ChaChargeType{prop:ReadOnly} = True
        ?mul:ChaChargeType{prop:FontColor} = 65793
        ?mul:ChaChargeType{prop:Color} = 15066597
    Elsif ?mul:ChaChargeType{prop:Req} = True
        ?mul:ChaChargeType{prop:FontColor} = 65793
        ?mul:ChaChargeType{prop:Color} = 8454143
    Else ! If ?mul:ChaChargeType{prop:Req} = True
        ?mul:ChaChargeType{prop:FontColor} = 65793
        ?mul:ChaChargeType{prop:Color} = 16777215
    End ! If ?mul:ChaChargeType{prop:Req} = True
    ?mul:ChaChargeType{prop:Trn} = 0
    ?mul:ChaChargeType{prop:FontStyle} = font:Bold
    ?MUL:ChaRepairType:Prompt{prop:FontColor} = -1
    ?MUL:ChaRepairType:Prompt{prop:Color} = 15066597
    If ?mul:ChaRepairType{prop:ReadOnly} = True
        ?mul:ChaRepairType{prop:FontColor} = 65793
        ?mul:ChaRepairType{prop:Color} = 15066597
    Elsif ?mul:ChaRepairType{prop:Req} = True
        ?mul:ChaRepairType{prop:FontColor} = 65793
        ?mul:ChaRepairType{prop:Color} = 8454143
    Else ! If ?mul:ChaRepairType{prop:Req} = True
        ?mul:ChaRepairType{prop:FontColor} = 65793
        ?mul:ChaRepairType{prop:Color} = 16777215
    End ! If ?mul:ChaRepairType{prop:Req} = True
    ?mul:ChaRepairType{prop:Trn} = 0
    ?mul:ChaRepairType{prop:FontStyle} = font:Bold
    ?MUL:WarChargeType:Prompt{prop:FontColor} = -1
    ?MUL:WarChargeType:Prompt{prop:Color} = 15066597
    If ?mul:WarChargeType{prop:ReadOnly} = True
        ?mul:WarChargeType{prop:FontColor} = 65793
        ?mul:WarChargeType{prop:Color} = 15066597
    Elsif ?mul:WarChargeType{prop:Req} = True
        ?mul:WarChargeType{prop:FontColor} = 65793
        ?mul:WarChargeType{prop:Color} = 8454143
    Else ! If ?mul:WarChargeType{prop:Req} = True
        ?mul:WarChargeType{prop:FontColor} = 65793
        ?mul:WarChargeType{prop:Color} = 16777215
    End ! If ?mul:WarChargeType{prop:Req} = True
    ?mul:WarChargeType{prop:Trn} = 0
    ?mul:WarChargeType{prop:FontStyle} = font:Bold
    ?MUL:WarRepairType:Prompt{prop:FontColor} = -1
    ?MUL:WarRepairType:Prompt{prop:Color} = 15066597
    If ?mul:WarRepairType{prop:ReadOnly} = True
        ?mul:WarRepairType{prop:FontColor} = 65793
        ?mul:WarRepairType{prop:Color} = 15066597
    Elsif ?mul:WarRepairType{prop:Req} = True
        ?mul:WarRepairType{prop:FontColor} = 65793
        ?mul:WarRepairType{prop:Color} = 8454143
    Else ! If ?mul:WarRepairType{prop:Req} = True
        ?mul:WarRepairType{prop:FontColor} = 65793
        ?mul:WarRepairType{prop:Color} = 16777215
    End ! If ?mul:WarRepairType{prop:Req} = True
    ?mul:WarRepairType{prop:Trn} = 0
    ?mul:WarRepairType{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateMultiDefaults',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateMultiDefaults',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateMultiDefaults',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MUL:Description:Prompt;  SolaceCtrlName = '?MUL:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mul:Description;  SolaceCtrlName = '?mul:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MUL:ModelNumber:Prompt;  SolaceCtrlName = '?MUL:ModelNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mul:ModelNumber;  SolaceCtrlName = '?mul:ModelNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupModelNumber;  SolaceCtrlName = '?LookupModelNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MUL:UnitType:Prompt;  SolaceCtrlName = '?MUL:UnitType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mul:UnitType;  SolaceCtrlName = '?mul:UnitType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupUnitType;  SolaceCtrlName = '?LookupUnitType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MUL:OrderNumber:Prompt;  SolaceCtrlName = '?MUL:OrderNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mul:OrderNumber;  SolaceCtrlName = '?mul:OrderNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MUL:InternalLocation:Prompt;  SolaceCtrlName = '?MUL:InternalLocation:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mul:InternalLocation;  SolaceCtrlName = '?mul:InternalLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupLocation;  SolaceCtrlName = '?LookupLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MUL:InternalLocation:Prompt:2;  SolaceCtrlName = '?MUL:InternalLocation:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mul:FaultDescription;  SolaceCtrlName = '?mul:FaultDescription';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupFaultDescription;  SolaceCtrlName = '?LookupFaultDescription';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MUL:DOP:Prompt;  SolaceCtrlName = '?MUL:DOP:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mul:DOP;  SolaceCtrlName = '?mul:DOP';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MUL:ChaChargeType:Prompt;  SolaceCtrlName = '?MUL:ChaChargeType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mul:ChaChargeType;  SolaceCtrlName = '?mul:ChaChargeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupChargeType;  SolaceCtrlName = '?LookupChargeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MUL:ChaRepairType:Prompt;  SolaceCtrlName = '?MUL:ChaRepairType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mul:ChaRepairType;  SolaceCtrlName = '?mul:ChaRepairType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupRepairType;  SolaceCtrlName = '?LookupRepairType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MUL:WarChargeType:Prompt;  SolaceCtrlName = '?MUL:WarChargeType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mul:WarChargeType;  SolaceCtrlName = '?mul:WarChargeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupWChargeType;  SolaceCtrlName = '?LookupWChargeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MUL:WarRepairType:Prompt;  SolaceCtrlName = '?MUL:WarRepairType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mul:WarRepairType;  SolaceCtrlName = '?mul:WarRepairType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupWRepairType;  SolaceCtrlName = '?LookupWRepairType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Default'
  OF ChangeRecord
    ActionMessage = 'Changing A Defalult'
  END
  QuickWindow{Prop:Text} = ActionMessage
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateMultiDefaults')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateMultiDefaults')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?MUL:Description:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(mul:Record,History::mul:Record)
  SELF.AddHistoryField(?mul:Description,2)
  SELF.AddHistoryField(?mul:ModelNumber,3)
  SELF.AddHistoryField(?mul:UnitType,4)
  SELF.AddHistoryField(?mul:OrderNumber,6)
  SELF.AddHistoryField(?mul:InternalLocation,7)
  SELF.AddHistoryField(?mul:FaultDescription,8)
  SELF.AddHistoryField(?mul:DOP,9)
  SELF.AddHistoryField(?mul:ChaChargeType,10)
  SELF.AddHistoryField(?mul:ChaRepairType,11)
  SELF.AddHistoryField(?mul:WarChargeType,12)
  SELF.AddHistoryField(?mul:WarRepairType,13)
  SELF.AddUpdateFile(Access:MULTIDEF)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CHARTYPE.Open
  Relate:MULTIDEF.Open
  Access:MODELNUM.UseFile
  Access:UNITTYPE.UseFile
  Access:LOCINTER.UseFile
  Access:REPAIRTY.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MULTIDEF
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  IF ?mul:ModelNumber{Prop:Tip} AND ~?LookupModelNumber{Prop:Tip}
     ?LookupModelNumber{Prop:Tip} = 'Select ' & ?mul:ModelNumber{Prop:Tip}
  END
  IF ?mul:ModelNumber{Prop:Msg} AND ~?LookupModelNumber{Prop:Msg}
     ?LookupModelNumber{Prop:Msg} = 'Select ' & ?mul:ModelNumber{Prop:Msg}
  END
  IF ?mul:UnitType{Prop:Tip} AND ~?LookupUnitType{Prop:Tip}
     ?LookupUnitType{Prop:Tip} = 'Select ' & ?mul:UnitType{Prop:Tip}
  END
  IF ?mul:UnitType{Prop:Msg} AND ~?LookupUnitType{Prop:Msg}
     ?LookupUnitType{Prop:Msg} = 'Select ' & ?mul:UnitType{Prop:Msg}
  END
  IF ?mul:InternalLocation{Prop:Tip} AND ~?LookupLocation{Prop:Tip}
     ?LookupLocation{Prop:Tip} = 'Select ' & ?mul:InternalLocation{Prop:Tip}
  END
  IF ?mul:InternalLocation{Prop:Msg} AND ~?LookupLocation{Prop:Msg}
     ?LookupLocation{Prop:Msg} = 'Select ' & ?mul:InternalLocation{Prop:Msg}
  END
  IF ?mul:ChaChargeType{Prop:Tip} AND ~?LookupChargeType{Prop:Tip}
     ?LookupChargeType{Prop:Tip} = 'Select ' & ?mul:ChaChargeType{Prop:Tip}
  END
  IF ?mul:ChaChargeType{Prop:Msg} AND ~?LookupChargeType{Prop:Msg}
     ?LookupChargeType{Prop:Msg} = 'Select ' & ?mul:ChaChargeType{Prop:Msg}
  END
  IF ?mul:ChaRepairType{Prop:Tip} AND ~?LookupRepairType{Prop:Tip}
     ?LookupRepairType{Prop:Tip} = 'Select ' & ?mul:ChaRepairType{Prop:Tip}
  END
  IF ?mul:ChaRepairType{Prop:Msg} AND ~?LookupRepairType{Prop:Msg}
     ?LookupRepairType{Prop:Msg} = 'Select ' & ?mul:ChaRepairType{Prop:Msg}
  END
  IF ?mul:WarChargeType{Prop:Tip} AND ~?LookupWChargeType{Prop:Tip}
     ?LookupWChargeType{Prop:Tip} = 'Select ' & ?mul:WarChargeType{Prop:Tip}
  END
  IF ?mul:WarChargeType{Prop:Msg} AND ~?LookupWChargeType{Prop:Msg}
     ?LookupWChargeType{Prop:Msg} = 'Select ' & ?mul:WarChargeType{Prop:Msg}
  END
  IF ?mul:WarRepairType{Prop:Tip} AND ~?LookupWRepairType{Prop:Tip}
     ?LookupWRepairType{Prop:Tip} = 'Select ' & ?mul:WarRepairType{Prop:Tip}
  END
  IF ?mul:WarRepairType{Prop:Msg} AND ~?LookupWRepairType{Prop:Msg}
     ?LookupWRepairType{Prop:Msg} = 'Select ' & ?mul:WarRepairType{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:MULTIDEF.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateMultiDefaults',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      BrowseMODELNUM
      BrowseUNITTYPE
      Browse_Available_Locations
      Browse_Warranty_Charge_Types
      BrowseREPAIRTY
      Browse_Warranty_Charge_Types
      BrowseREPAIRTY_Warranty
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?LookupFaultDescription
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupFaultDescription, Accepted)
      Clear(glo:Q_Notes)
      Free(glo:Q_Notes)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupFaultDescription, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?mul:ModelNumber
      IF mul:ModelNumber OR ?mul:ModelNumber{Prop:Req}
        mod:Model_Number = mul:ModelNumber
        !Save Lookup Field Incase Of error
        look:mul:ModelNumber        = mul:ModelNumber
        IF Access:MODELNUM.TryFetch(mod:Model_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            mul:ModelNumber = mod:Model_Number
          ELSE
            !Restore Lookup On Error
            mul:ModelNumber = look:mul:ModelNumber
            SELECT(?mul:ModelNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupModelNumber
      ThisWindow.Update
      mod:Model_Number = mul:ModelNumber
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          mul:ModelNumber = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?mul:ModelNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?mul:ModelNumber)
    OF ?mul:UnitType
      IF mul:UnitType OR ?mul:UnitType{Prop:Req}
        uni:Unit_Type = mul:UnitType
        !Save Lookup Field Incase Of error
        look:mul:UnitType        = mul:UnitType
        IF Access:UNITTYPE.TryFetch(uni:Unit_Type_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            mul:UnitType = uni:Unit_Type
          ELSE
            !Restore Lookup On Error
            mul:UnitType = look:mul:UnitType
            SELECT(?mul:UnitType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupUnitType
      ThisWindow.Update
      uni:Unit_Type = mul:UnitType
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          mul:UnitType = uni:Unit_Type
          Select(?+1)
      ELSE
          Select(?mul:UnitType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?mul:UnitType)
    OF ?mul:InternalLocation
      IF mul:InternalLocation OR ?mul:InternalLocation{Prop:Req}
        loi:Location = mul:InternalLocation
        loi:Location_Available = 'YES'
        !Save Lookup Field Incase Of error
        look:mul:InternalLocation        = mul:InternalLocation
        IF Access:LOCINTER.TryFetch(loi:Location_Available_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            mul:InternalLocation = loi:Location
          ELSE
            CLEAR(loi:Location_Available)
            !Restore Lookup On Error
            mul:InternalLocation = look:mul:InternalLocation
            SELECT(?mul:InternalLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupLocation
      ThisWindow.Update
      loi:Location = mul:InternalLocation
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          mul:InternalLocation = loi:Location
          Select(?+1)
      ELSE
          Select(?mul:InternalLocation)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?mul:InternalLocation)
    OF ?LookupFaultDescription
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Fault_Description_Text
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupFaultDescription, Accepted)
      If Records(glo:Q_Notes) <> ''
          Loop x$ = 1 To Records(glo:Q_Notes)
              Get(glo:Q_Notes,x$)
              If mul:FaultDescription = ''
                  mul:FaultDescription = Clip(glo:notes_pointer)
              Else !If job:engineers_notes = ''
                  mul:FaultDescription = Clip(mul:FaultDescription) & '. ' & Clip(glo:notes_pointer)
              End !If job:engineers_notes = ''
          End
          Display()
      End
      Clear(glo:Q_Notes)
      Free(glo:Q_Notes)
      glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupFaultDescription, Accepted)
    OF ?mul:ChaChargeType
      IF mul:ChaChargeType OR ?mul:ChaChargeType{Prop:Req}
        cha:Charge_Type = mul:ChaChargeType
        cha:Warranty = 'NO'
        GLO:Select1 = 'NO'
        !Save Lookup Field Incase Of error
        look:mul:ChaChargeType        = mul:ChaChargeType
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            mul:ChaChargeType = cha:Charge_Type
          ELSE
            CLEAR(cha:Warranty)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            mul:ChaChargeType = look:mul:ChaChargeType
            SELECT(?mul:ChaChargeType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupChargeType
      ThisWindow.Update
      cha:Charge_Type = mul:ChaChargeType
      GLO:Select1 = 'NO'
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          mul:ChaChargeType = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?mul:ChaChargeType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?mul:ChaChargeType)
    OF ?mul:ChaRepairType
      IF mul:ChaRepairType OR ?mul:ChaRepairType{Prop:Req}
        rep:Repair_Type = mul:ChaRepairType
        rep:Chargeable = 'YES'
        rep:Model_Number = mul:ModelNumber
        GLO:Select1 = mul:ModelNumber
        !Save Lookup Field Incase Of error
        look:mul:ChaRepairType        = mul:ChaRepairType
        IF Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            mul:ChaRepairType = rep:Repair_Type
          ELSE
            CLEAR(rep:Chargeable)
            CLEAR(rep:Model_Number)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            mul:ChaRepairType = look:mul:ChaRepairType
            SELECT(?mul:ChaRepairType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupRepairType
      ThisWindow.Update
      rep:Repair_Type = mul:ChaRepairType
      GLO:Select1 = mul:ModelNumber
      
      IF SELF.RUN(5,Selectrecord)  = RequestCompleted
          mul:ChaRepairType = rep:Repair_Type
          Select(?+1)
      ELSE
          Select(?mul:ChaRepairType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?mul:ChaRepairType)
    OF ?mul:WarChargeType
      IF mul:WarChargeType OR ?mul:WarChargeType{Prop:Req}
        cha:Charge_Type = mul:WarChargeType
        cha:Warranty = 'YES'
        GLO:Select1 = 'YES'
        !Save Lookup Field Incase Of error
        look:mul:WarChargeType        = mul:WarChargeType
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            mul:WarChargeType = cha:Charge_Type
          ELSE
            CLEAR(cha:Warranty)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            mul:WarChargeType = look:mul:WarChargeType
            SELECT(?mul:WarChargeType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupWChargeType
      ThisWindow.Update
      cha:Charge_Type = mul:WarChargeType
      GLO:Select1 = 'YES'
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          mul:WarChargeType = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?mul:WarChargeType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?mul:WarChargeType)
    OF ?mul:WarRepairType
      IF mul:WarRepairType OR ?mul:WarRepairType{Prop:Req}
        rep:Repair_Type = mul:WarRepairType
        rep:Warranty = 'YES'
        rep:Model_Number = MUL:ModelNumber
        GLO:Select1 = mul:ModelNumber
        !Save Lookup Field Incase Of error
        look:mul:WarRepairType        = mul:WarRepairType
        IF Access:REPAIRTY.TryFetch(rep:Model_Warranty_Key)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            mul:WarRepairType = rep:Repair_Type
          ELSE
            CLEAR(rep:Warranty)
            CLEAR(rep:Model_Number)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            mul:WarRepairType = look:mul:WarRepairType
            SELECT(?mul:WarRepairType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupWRepairType
      ThisWindow.Update
      rep:Repair_Type = mul:WarRepairType
      GLO:Select1 = mul:ModelNumber
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          mul:WarRepairType = rep:Repair_Type
          Select(?+1)
      ELSE
          Select(?mul:WarRepairType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?mul:WarRepairType)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateMultiDefaults')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?mul:ModelNumber
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mul:ModelNumber, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Model Number')
             Post(Event:Accepted,?LookupModelNumber)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupModelNumber)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mul:ModelNumber, AlertKey)
    END
  OF ?mul:UnitType
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mul:UnitType, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Unit Type')
             Post(Event:Accepted,?LookupUnitType)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupUnitType)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mul:UnitType, AlertKey)
    END
  OF ?mul:InternalLocation
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mul:InternalLocation, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Location')
             Post(Event:Accepted,?LookupLocation)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupLocation)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mul:InternalLocation, AlertKey)
    END
  OF ?mul:ChaChargeType
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mul:ChaChargeType, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Chargeable Charge Type')
             Post(Event:Accepted,?LookupChargeType)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupChargeType)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mul:ChaChargeType, AlertKey)
    END
  OF ?mul:ChaRepairType
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mul:ChaRepairType, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Chargeable Repair Type')
             Post(Event:Accepted,?LookupRepairType)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupRepairType)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mul:ChaRepairType, AlertKey)
    END
  OF ?mul:WarChargeType
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mul:WarChargeType, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Warranty Charge Type')
             Post(Event:Accepted,?LookupWChargeType)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupWChargeType)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mul:WarChargeType, AlertKey)
    END
  OF ?mul:WarRepairType
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mul:WarRepairType, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Warranty Repair Type')
             Post(Event:Accepted,?LookupWRepairType)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupWRepairType)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mul:WarRepairType, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

