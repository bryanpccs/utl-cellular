

   MEMBER('celrapml.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA002.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CELRA003.INC'),ONCE        !Req'd for module callout resolution
                     END


Update_Engineer_Job PROCEDURE                         !Generated from procedure template - Window

FilesOpened          BYTE
tmp:CommonRef        LONG
engineer_ref_number_temp REAL
main_store_ref_number_temp REAL
engineer_quantity_temp REAL
main_store_quantity_temp REAL
main_store_sundry_temp STRING(3)
engineer_sundry_temp STRING(3)
save_ccp_id          USHORT,AUTO
save_cwp_id          USHORT,AUTO
ref_number_temp      REAL
serial_number_temp   STRING(16)
common_fault_temp    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Rapid Engineer Update'),AT(,,331,107),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE,MDI
                       SHEET,AT(4,4,324,72),USE(?Sheet1),SPREAD
                         TAB('Rapid Engineer Update'),USE(?Tab1)
                           PROMPT('Job Number'),AT(8,20),USE(?ref_number_temp:Prompt),TRN
                           ENTRY(@s8),AT(84,20,64,10),USE(ref_number_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Serial Number'),AT(8,36),USE(?serial_number_temp:Prompt),TRN
                           ENTRY(@s16),AT(84,36,124,10),USE(serial_number_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           GROUP,AT(4,48,324,28),USE(?ButtonGroup),DISABLE
                             BUTTON('&Allocate Job'),AT(8,56,76,16),USE(?AllocateJob),LEFT,ICON('stock_sm.gif')
                             BUTTON('&View Job'),AT(88,56,76,16),USE(?View_Job),LEFT,ICON('spy.gif')
                             BUTTON('Allocate &Common Fault'),AT(168,56,76,16),USE(?CommonFault),LEFT,ICON('fault.gif')
                             BUTTON('Change &Status'),AT(248,56,76,16),USE(?ChangeStatus),LEFT,ICON('1.ico')
                           END
                         END
                       END
                       BUTTON('&Next Job'),AT(212,84,56,16),USE(?OkButton),LEFT,ICON('next.gif')
                       BUTTON('Finish'),AT(268,84,56,16),USE(?CancelButton),LEFT,ICON('thumbs.gif'),STD(STD:Close)
                       PANEL,AT(4,80,324,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?ref_number_temp:Prompt{prop:FontColor} = -1
    ?ref_number_temp:Prompt{prop:Color} = 15066597
    If ?ref_number_temp{prop:ReadOnly} = True
        ?ref_number_temp{prop:FontColor} = 65793
        ?ref_number_temp{prop:Color} = 15066597
    Elsif ?ref_number_temp{prop:Req} = True
        ?ref_number_temp{prop:FontColor} = 65793
        ?ref_number_temp{prop:Color} = 8454143
    Else ! If ?ref_number_temp{prop:Req} = True
        ?ref_number_temp{prop:FontColor} = 65793
        ?ref_number_temp{prop:Color} = 16777215
    End ! If ?ref_number_temp{prop:Req} = True
    ?ref_number_temp{prop:Trn} = 0
    ?ref_number_temp{prop:FontStyle} = font:Bold
    ?serial_number_temp:Prompt{prop:FontColor} = -1
    ?serial_number_temp:Prompt{prop:Color} = 15066597
    If ?serial_number_temp{prop:ReadOnly} = True
        ?serial_number_temp{prop:FontColor} = 65793
        ?serial_number_temp{prop:Color} = 15066597
    Elsif ?serial_number_temp{prop:Req} = True
        ?serial_number_temp{prop:FontColor} = 65793
        ?serial_number_temp{prop:Color} = 8454143
    Else ! If ?serial_number_temp{prop:Req} = True
        ?serial_number_temp{prop:FontColor} = 65793
        ?serial_number_temp{prop:Color} = 16777215
    End ! If ?serial_number_temp{prop:Req} = True
    ?serial_number_temp{prop:Trn} = 0
    ?serial_number_temp{prop:FontStyle} = font:Bold
    ?ButtonGroup{prop:Font,3} = -1
    ?ButtonGroup{prop:Color} = 15066597
    ?ButtonGroup{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Fill_Lists      Routine
Pricing         Routine
stock_history       Routine        !Do The Prime, and Set The Quantity First
    shi:ref_number      = sto:ref_number
    access:users.clearkey(use:password_key)
    use:password        =glo:password
    access:users.fetch(use:password_key)
    shi:user            = use:user_code
    shi:date            = Today()
    shi:transaction_type    = 'DEC'
    shi:despatch_note_number    = par:despatch_note_number
    shi:job_number      = job:ref_number
    shi:purchase_cost   = par:purchase_cost
    shi:sale_cost       = par:sale_cost
    shi:notes           = 'STOCK DECREMENTED'
    access:stohist.insert()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Engineer_Job',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_Engineer_Job',1)
    SolaceViewVars('tmp:CommonRef',tmp:CommonRef,'Update_Engineer_Job',1)
    SolaceViewVars('engineer_ref_number_temp',engineer_ref_number_temp,'Update_Engineer_Job',1)
    SolaceViewVars('main_store_ref_number_temp',main_store_ref_number_temp,'Update_Engineer_Job',1)
    SolaceViewVars('engineer_quantity_temp',engineer_quantity_temp,'Update_Engineer_Job',1)
    SolaceViewVars('main_store_quantity_temp',main_store_quantity_temp,'Update_Engineer_Job',1)
    SolaceViewVars('main_store_sundry_temp',main_store_sundry_temp,'Update_Engineer_Job',1)
    SolaceViewVars('engineer_sundry_temp',engineer_sundry_temp,'Update_Engineer_Job',1)
    SolaceViewVars('save_ccp_id',save_ccp_id,'Update_Engineer_Job',1)
    SolaceViewVars('save_cwp_id',save_cwp_id,'Update_Engineer_Job',1)
    SolaceViewVars('ref_number_temp',ref_number_temp,'Update_Engineer_Job',1)
    SolaceViewVars('serial_number_temp',serial_number_temp,'Update_Engineer_Job',1)
    SolaceViewVars('common_fault_temp',common_fault_temp,'Update_Engineer_Job',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ref_number_temp:Prompt;  SolaceCtrlName = '?ref_number_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ref_number_temp;  SolaceCtrlName = '?ref_number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?serial_number_temp:Prompt;  SolaceCtrlName = '?serial_number_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?serial_number_temp;  SolaceCtrlName = '?serial_number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ButtonGroup;  SolaceCtrlName = '?ButtonGroup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AllocateJob;  SolaceCtrlName = '?AllocateJob';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?View_Job;  SolaceCtrlName = '?View_Job';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CommonFault;  SolaceCtrlName = '?CommonFault';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ChangeStatus;  SolaceCtrlName = '?ChangeStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Engineer_Job')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Engineer_Job')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ref_number_temp:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:COMMONFA.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:USERS_ALIAS.Open
  Access:JOBS.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:COMMONFA.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:USERS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Engineer_Job',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?serial_number_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?serial_number_temp, Accepted)
      Disable(?ButtonGroup)
      error# = 0
      If ref_number_temp = ''
          error# = 1
          Select(?ref_number_temp)
      End!If ref_number_temp = ''
      If serial_number_temp = '' And error# = 0
          error# = 1
          Select(?serial_number_temp)
      End!If serial_number_temp = '' And error# = 0
      If error# = 0
          access:jobs.clearkey(job:ref_number_key)
          job:ref_number   = ref_number_temp
          If access:jobs.fetch(job:ref_number_key)
              beep(beep:systemhand)  ;  yield()
              case message('Unable to find selected Job.', |
                      'ServiceBase 2000', icon:hand, |
                       button:ok, button:ok, 0)
              of button:ok
              end !case
          Else!If access:jobs.fetch(job:ref_number_key)
              If job:date_completed <> ''
                  error# = 1
                  beep(beep:systemhand)  ;  yield()
                  case message('This job has been completed.', |
                          'ServiceBase 2000', icon:hand, |
                           button:ok, button:ok, 0)
                  of button:ok
                  end !case
              End!If job:date_completed <> ''
      
              If job:workshop <> 'YES' and error# = 0
                  beep(beep:systemhand)  ;  yield()
                  case message('This job has not been brought into the Workshop.', |
                          'ServiceBase 2000', icon:hand, |
                           button:ok, button:ok, 0)
                  of button:ok
                  end !case
              End!If job:workshop <> 'YES' and erro# = 0
      
              If serial_number_temp <> job:esn
                  If job:exchange_unit_number <> ''
                      access:exchange.clearkey(xch:ref_number_key)
                      xch:ref_number = job:exchange_unit_number
                      if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                          If xch:esn = serial_number_temp
                              beep(beep:systemhand)  ;  yield()
                              case message('The Serial Number you have entered is the Serial Number of '&|
                                      'the Exchange Unit attached to this job.', |
                                      'ServiceBase 2000', icon:hand, |
                                       button:ok, button:ok, 0)
                              of button:ok
                              end !case
                              error# = 1
                          End!If xch:esn = serial_number_temp
                      End!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                  Else!If job:exchange_unit_number <> ''
                      beep(beep:systemhand)  ;  yield()
                      case message('The Serial Number does not match the selected Job Number.', |
                              'ServiceBase 2000', icon:hand, |
                               button:ok, button:ok, 0)
                      of button:ok
                      end !case
                      error# = 1
                  End!If job:exchange_unit_number <> ''
              End!If serial_number_temp <> job:esn
      
              If error# = 0
                              Enable(?ButtonGroup)
              End!If error# = 0
      
          End!If access:jobs.fetch(job:ref_number_key)
      End!If error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?serial_number_temp, Accepted)
    OF ?AllocateJob
      ThisWindow.Update
      AllocateJob(ref_number_temp)
      ThisWindow.Reset
    OF ?View_Job
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?View_Job, Accepted)
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number = ref_number_temp
      If access:jobs.fetch(job:ref_number_key) = Level:Benign
          saverequest# = globalrequest
          globalrequest = changerecord
          ! Start Change 1744 BE(15/04/03)
          glo:select20 = ''
          ! End Change 1744 BE(15/04/03)
          update_jobs_rapid
          ! Start Change 1744 BE(15/04/03)
          glo:select20 = ''
          ! End Change 1744 BE(15/04/03)
          globalrequest = saverequest#
          Select(?ref_number_temp)
      Else!If access:jobs.fetch(job:ref_number_key) = Level:Benign
          Case MessageEx('Unable To Find Selected Job.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          Select(?ref_number_temp)
      End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?View_Job, Accepted)
    OF ?CommonFault
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CommonFault, Accepted)
      If SecurityCheck('COMMON FAULTS - APPLY TO JOBS')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If SecurityCheck('COMMON FAULTS - APPLY TO JOBS')
          access:jobs.clearkey(job:ref_number_key)
          job:ref_number   = ref_number_temp
          If access:jobs.fetch(job:ref_number_key)
              beep(beep:systemhand)  ;  yield()
              case message('Unable to find selected Job.', |
                      'ServiceBase 2000', icon:hand, |
                       button:ok, button:ok, 0)
              of button:ok
              end !case
          Else!If access:jobs.fetch(job:ref_number_key)
              glo:select1 = job:model_number
              Globalrequest  = selectrecord
              Browse_Common_faults
              case globalresponse
                  of requestcompleted
                      common_fault_temp = com:description
                      tmp:commonref     = com:ref_number
                      CommonFaults(tmp:Commonref)
                      access:jobs.update()
                  of requestcancelled
                      common_fault_temp = ''
                      tmp:commonref   = ''
              end!case globalreponse
              glo:select1 = ''
          End!If access:jobs.fetch(job:ref_number_key)
      
      End!If SecurityCheck('COMMON FAULTS - APPLY TO JOBS')
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CommonFault, Accepted)
    OF ?ChangeStatus
      ThisWindow.Update
      Change_Status(ref_number_temp)
      ThisWindow.Reset
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      ref_number_temp = ''
      serial_number_temp = ''
      Disable(?ButtonGroup)
      Select(?ref_number_temp)
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Engineer_Job')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

