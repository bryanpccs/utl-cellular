  MEMBER('sbrppart.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
SBRPPBC2:DctInit    PROCEDURE
SBRPPBC2:DctKill    PROCEDURE
SBRPPBC2:FilesInit  PROCEDURE
  END

Hide:Access:JOBSTAGE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBSTAGE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANFAUPA CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
ValidateFieldServer    PROCEDURE(UNSIGNED Id,BYTE HandleErrors),BYTE,PROC,DERIVED
                     END


Hide:Relate:MANFAUPA CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOCINTER CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:LOCINTER CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:NOTESFAU CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:NOTESFAU CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANFAULO CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:MANFAULO CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBEXHIS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBEXHIS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ORDPARTS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:ORDPARTS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOCATION CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:LOCATION CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STOESN   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:STOESN   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SUPPLIER CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:SUPPLIER CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SUPVALA  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:SUPVALA  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ESTPARTS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:ESTPARTS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRAEMAIL CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:TRAEMAIL CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SUPVALB  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:SUPVALB  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBS     CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBS     CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MODELNUM CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:MODELNUM CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SUBEMAIL CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:SUBEMAIL CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ORDERS   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:ORDERS   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:UNITTYPE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:UNITTYPE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:REPAIRTY CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:REPAIRTY CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

SBRPPBC2:DctInit PROCEDURE
  CODE
  Relate:JOBSTAGE &= Hide:Relate:JOBSTAGE
  Relate:MANFAUPA &= Hide:Relate:MANFAUPA
  Relate:LOCINTER &= Hide:Relate:LOCINTER
  Relate:NOTESFAU &= Hide:Relate:NOTESFAU
  Relate:MANFAULO &= Hide:Relate:MANFAULO
  Relate:JOBEXHIS &= Hide:Relate:JOBEXHIS
  Relate:ORDPARTS &= Hide:Relate:ORDPARTS
  Relate:LOCATION &= Hide:Relate:LOCATION
  Relate:STOESN &= Hide:Relate:STOESN
  Relate:SUPPLIER &= Hide:Relate:SUPPLIER
  Relate:SUPVALA &= Hide:Relate:SUPVALA
  Relate:ESTPARTS &= Hide:Relate:ESTPARTS
  Relate:TRAEMAIL &= Hide:Relate:TRAEMAIL
  Relate:SUPVALB &= Hide:Relate:SUPVALB
  Relate:JOBS &= Hide:Relate:JOBS
  Relate:MODELNUM &= Hide:Relate:MODELNUM
  Relate:SUBEMAIL &= Hide:Relate:SUBEMAIL
  Relate:ORDERS &= Hide:Relate:ORDERS
  Relate:UNITTYPE &= Hide:Relate:UNITTYPE
  Relate:REPAIRTY &= Hide:Relate:REPAIRTY

SBRPPBC2:FilesInit PROCEDURE
  CODE
  Hide:Relate:JOBSTAGE.Init
  Hide:Relate:MANFAUPA.Init
  Hide:Relate:LOCINTER.Init
  Hide:Relate:NOTESFAU.Init
  Hide:Relate:MANFAULO.Init
  Hide:Relate:JOBEXHIS.Init
  Hide:Relate:ORDPARTS.Init
  Hide:Relate:LOCATION.Init
  Hide:Relate:STOESN.Init
  Hide:Relate:SUPPLIER.Init
  Hide:Relate:SUPVALA.Init
  Hide:Relate:ESTPARTS.Init
  Hide:Relate:TRAEMAIL.Init
  Hide:Relate:SUPVALB.Init
  Hide:Relate:JOBS.Init
  Hide:Relate:MODELNUM.Init
  Hide:Relate:SUBEMAIL.Init
  Hide:Relate:ORDERS.Init
  Hide:Relate:UNITTYPE.Init
  Hide:Relate:REPAIRTY.Init


SBRPPBC2:DctKill PROCEDURE
  CODE
  Hide:Relate:JOBSTAGE.Kill
  Hide:Relate:MANFAUPA.Kill
  Hide:Relate:LOCINTER.Kill
  Hide:Relate:NOTESFAU.Kill
  Hide:Relate:MANFAULO.Kill
  Hide:Relate:JOBEXHIS.Kill
  Hide:Relate:ORDPARTS.Kill
  Hide:Relate:LOCATION.Kill
  Hide:Relate:STOESN.Kill
  Hide:Relate:SUPPLIER.Kill
  Hide:Relate:SUPVALA.Kill
  Hide:Relate:ESTPARTS.Kill
  Hide:Relate:TRAEMAIL.Kill
  Hide:Relate:SUPVALB.Kill
  Hide:Relate:JOBS.Kill
  Hide:Relate:MODELNUM.Kill
  Hide:Relate:SUBEMAIL.Kill
  Hide:Relate:ORDERS.Kill
  Hide:Relate:UNITTYPE.Kill
  Hide:Relate:REPAIRTY.Kill


Hide:Access:JOBSTAGE.Init PROCEDURE
  CODE
  SELF.Init(JOBSTAGE,GlobalErrors)
  SELF.Buffer &= jst:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jst:Ref_Number_Key,'By Ref Number',0)
  SELF.AddKey(jst:Job_Stage_Key,'jst:Job_Stage_Key',0)
  Access:JOBSTAGE &= SELF


Hide:Relate:JOBSTAGE.Init PROCEDURE
  CODE
  Hide:Access:JOBSTAGE.Init
  SELF.Init(Access:JOBSTAGE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSTAGE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSTAGE &= NULL


Hide:Relate:JOBSTAGE.Kill PROCEDURE

  CODE
  Hide:Access:JOBSTAGE.Kill
  PARENT.Kill
  Relate:JOBSTAGE &= NULL


Hide:Access:MANFAUPA.Init PROCEDURE
  CODE
  SELF.Init(MANFAUPA,GlobalErrors)
  SELF.Buffer &= map:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(map:Field_Number_Key,'By Field Number',0)
  Access:MANFAUPA &= SELF


Hide:Relate:MANFAUPA.Init PROCEDURE
  CODE
  Hide:Access:MANFAUPA.Init
  SELF.Init(Access:MANFAUPA,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANFPALO,RI:CASCADE,RI:CASCADE,mfp:Field_Key)
  SELF.AddRelationLink(map:Manufacturer,mfp:Manufacturer)
  SELF.AddRelationLink(map:Field_Number,mfp:Field_Number)
  SELF.AddRelation(Relate:MANUFACT)


Hide:Access:MANFAUPA.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MANFAUPA &= NULL


Hide:Access:MANFAUPA.PrimeFields PROCEDURE

  CODE
  map:Compulsory = 'NO'
  map:RestrictLength = 0
  map:ForceFormat = 0
  PARENT.PrimeFields


Hide:Access:MANFAUPA.ValidateFieldServer PROCEDURE(UNSIGNED Id,BYTE HandleErrors)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateFieldServer(Id,HandleErrors)
  CASE Id
  OF 2
    GlobalErrors.SetField('Fault Code Field Number')
    IF NOT INRANGE(map:Field_Number,1,12)
      ReturnValue = Level:Notify
    END
    IF ReturnValue <> Level:Benign
      IF HandleErrors
        ReturnValue = GlobalErrors.ThrowMessage(Msg:FieldOutOfRange,'1 .. 12')
      END
    END
  END
  RETURN ReturnValue


Hide:Relate:MANFAUPA.Kill PROCEDURE

  CODE
  Hide:Access:MANFAUPA.Kill
  PARENT.Kill
  Relate:MANFAUPA &= NULL


Hide:Access:LOCINTER.Init PROCEDURE
  CODE
  SELF.Init(LOCINTER,GlobalErrors)
  SELF.Buffer &= loi:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(loi:Location_Key,'By Location',0)
  SELF.AddKey(loi:Location_Available_Key,'By Location',0)
  Access:LOCINTER &= SELF


Hide:Relate:LOCINTER.Init PROCEDURE
  CODE
  Hide:Access:LOCINTER.Init
  SELF.Init(Access:LOCINTER,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS,RI:CASCADE,RI:RESTRICT,job:Location_Key)
  SELF.AddRelationLink(loi:Location,job:Location)


Hide:Access:LOCINTER.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOCINTER &= NULL


Hide:Relate:LOCINTER.Kill PROCEDURE

  CODE
  Hide:Access:LOCINTER.Kill
  PARENT.Kill
  Relate:LOCINTER &= NULL


Hide:Access:NOTESFAU.Init PROCEDURE
  CODE
  SELF.Init(NOTESFAU,GlobalErrors)
  SELF.Buffer &= nof:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(nof:Notes_Key,'By Notes',0)
  Access:NOTESFAU &= SELF


Hide:Relate:NOTESFAU.Init PROCEDURE
  CODE
  Hide:Access:NOTESFAU.Init
  SELF.Init(Access:NOTESFAU,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANUFACT)


Hide:Access:NOTESFAU.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:NOTESFAU &= NULL


Hide:Relate:NOTESFAU.Kill PROCEDURE

  CODE
  Hide:Access:NOTESFAU.Kill
  PARENT.Kill
  Relate:NOTESFAU &= NULL


Hide:Access:MANFAULO.Init PROCEDURE
  CODE
  SELF.Init(MANFAULO,GlobalErrors)
  SELF.Buffer &= mfo:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(mfo:Field_Key,'By Field',0)
  SELF.AddKey(mfo:DescriptionKey,'By Description',0)
  Access:MANFAULO &= SELF


Hide:Relate:MANFAULO.Init PROCEDURE
  CODE
  Hide:Access:MANFAULO.Init
  SELF.Init(Access:MANFAULO,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANUFACT)
  SELF.AddRelation(Relate:MANFAULT)


Hide:Access:MANFAULO.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MANFAULO &= NULL


Hide:Relate:MANFAULO.Kill PROCEDURE

  CODE
  Hide:Access:MANFAULO.Kill
  PARENT.Kill
  Relate:MANFAULO &= NULL


Hide:Access:JOBEXHIS.Init PROCEDURE
  CODE
  SELF.Init(JOBEXHIS,GlobalErrors)
  SELF.Buffer &= jxh:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jxh:Ref_Number_Key,'By Date',0)
  SELF.AddKey(jxh:record_number_key,'jxh:record_number_key',1)
  Access:JOBEXHIS &= SELF


Hide:Relate:JOBEXHIS.Init PROCEDURE
  CODE
  Hide:Access:JOBEXHIS.Init
  SELF.Init(Access:JOBEXHIS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBEXHIS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBEXHIS &= NULL


Hide:Relate:JOBEXHIS.Kill PROCEDURE

  CODE
  Hide:Access:JOBEXHIS.Kill
  PARENT.Kill
  Relate:JOBEXHIS &= NULL


Hide:Access:ORDPARTS.Init PROCEDURE
  CODE
  SELF.Init(ORDPARTS,GlobalErrors)
  SELF.Buffer &= orp:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(orp:Order_Number_Key,'By Order Number',0)
  SELF.AddKey(orp:record_number_key,'orp:record_number_key',1)
  SELF.AddKey(orp:Ref_Number_Key,'By Ref Number',0)
  SELF.AddKey(orp:Part_Number_Key,'By Part Number',0)
  SELF.AddKey(orp:Description_Key,'By Description',0)
  SELF.AddKey(orp:Received_Part_Number_Key,'By Part Number',0)
  SELF.AddKey(orp:Account_Number_Key,'orp:Account_Number_Key',0)
  SELF.AddKey(orp:Allocated_Key,'orp:Allocated_Key',0)
  SELF.AddKey(orp:Allocated_Account_Key,'orp:Allocated_Account_Key',0)
  SELF.AddKey(orp:Order_Type_Received,'By Part Number',0)
  SELF.AddKey(orp:PartRecordNumberKey,'By Record Number',0)
  Access:ORDPARTS &= SELF


Hide:Relate:ORDPARTS.Init PROCEDURE
  CODE
  Hide:Access:ORDPARTS.Init
  SELF.Init(Access:ORDPARTS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:ORDERS)
  SELF.AddRelation(Relate:STOCK)


Hide:Access:ORDPARTS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ORDPARTS &= NULL


Hide:Access:ORDPARTS.PrimeFields PROCEDURE

  CODE
  orp:All_Received = 'NO'
  orp:Allocated_To_Sale = 'NO'
  PARENT.PrimeFields


Hide:Relate:ORDPARTS.Kill PROCEDURE

  CODE
  Hide:Access:ORDPARTS.Kill
  PARENT.Kill
  Relate:ORDPARTS &= NULL


Hide:Access:LOCATION.Init PROCEDURE
  CODE
  SELF.Init(LOCATION,GlobalErrors)
  SELF.Buffer &= loc:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(loc:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(loc:Location_Key,'By Location',0)
  SELF.AddKey(loc:Main_Store_Key,'By Location',0)
  SELF.AddKey(loc:ActiveLocationKey,'By Location',0)
  SELF.AddKey(loc:ActiveMainStoreKey,'By Location',0)
  Access:LOCATION &= SELF


Hide:Relate:LOCATION.Init PROCEDURE
  CODE
  Hide:Access:LOCATION.Init
  SELF.Init(Access:LOCATION,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:LOCVALUE,RI:CASCADE,RI:CASCADE,lov:DateKey)
  SELF.AddRelationLink(loc:Location,lov:Location)
  SELF.AddRelation(Relate:LOCSHELF,RI:CASCADE,RI:CASCADE,los:Shelf_Location_Key)
  SELF.AddRelationLink(loc:Location,los:Site_Location)
  SELF.AddRelation(Relate:STOCK,RI:CASCADE,RI:CASCADE,sto:Location_Key)
  SELF.AddRelationLink(loc:Location,sto:Location)


Hide:Access:LOCATION.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOCATION &= NULL


Hide:Access:LOCATION.PrimeFields PROCEDURE

  CODE
  loc:Main_Store = 'NO'
  loc:Active = 0
  PARENT.PrimeFields


Hide:Relate:LOCATION.Kill PROCEDURE

  CODE
  Hide:Access:LOCATION.Kill
  PARENT.Kill
  Relate:LOCATION &= NULL


Hide:Access:STOESN.Init PROCEDURE
  CODE
  SELF.Init(STOESN,GlobalErrors)
  SELF.Buffer &= ste:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(ste:Record_Number_Key,'By Record Number',1)
  SELF.AddKey(ste:Sold_Key,'By Serial Number',0)
  Access:STOESN &= SELF


Hide:Relate:STOESN.Init PROCEDURE
  CODE
  Hide:Access:STOESN.Init
  SELF.Init(Access:STOESN,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STOCK)


Hide:Access:STOESN.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STOESN &= NULL


Hide:Access:STOESN.PrimeFields PROCEDURE

  CODE
  ste:Sold = 'NO'
  PARENT.PrimeFields


Hide:Relate:STOESN.Kill PROCEDURE

  CODE
  Hide:Access:STOESN.Kill
  PARENT.Kill
  Relate:STOESN &= NULL


Hide:Access:SUPPLIER.Init PROCEDURE
  CODE
  SELF.Init(SUPPLIER,GlobalErrors)
  SELF.Buffer &= sup:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(sup:RecordNumberKey,'sup:RecordNumberKey',1)
  SELF.AddKey(sup:AccountNumberKey,'By Account Number',0)
  SELF.AddKey(sup:Company_Name_Key,'By Company Name',0)
  Access:SUPPLIER &= SELF


Hide:Relate:SUPPLIER.Init PROCEDURE
  CODE
  Hide:Access:SUPPLIER.Init
  SELF.Init(Access:SUPPLIER,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:SUPVALA,RI:CASCADE,RI:CASCADE,suva:RunDateKey)
  SELF.AddRelationLink(sup:Company_Name,suva:Supplier)
  SELF.AddRelation(Relate:SUPVALB,RI:CASCADE,RI:CASCADE,suvb:RunDateKey)
  SELF.AddRelationLink(sup:Company_Name,suvb:Supplier)
  SELF.AddRelation(Relate:ORDPEND,RI:CASCADE,RI:None,ope:Supplier_Name_Key)
  SELF.AddRelationLink(sup:Company_Name,ope:Supplier)
  SELF.AddRelation(Relate:ESTPARTS,RI:CASCADE,RI:None,epr:Supplier_Key)
  SELF.AddRelationLink(sup:Company_Name,epr:Supplier)
  SELF.AddRelation(Relate:PARTS,RI:CASCADE,RI:None,par:Supplier_Key)
  SELF.AddRelationLink(sup:Company_Name,par:Supplier)
  SELF.AddRelation(Relate:WARPARTS,RI:CASCADE,RI:None,wpr:Supplier_Key)
  SELF.AddRelationLink(sup:Company_Name,wpr:Supplier)


Hide:Access:SUPPLIER.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SUPPLIER &= NULL


Hide:Relate:SUPPLIER.Kill PROCEDURE

  CODE
  Hide:Access:SUPPLIER.Kill
  PARENT.Kill
  Relate:SUPPLIER &= NULL


Hide:Access:SUPVALA.Init PROCEDURE
  CODE
  SELF.Init(SUPVALA,GlobalErrors)
  SELF.Buffer &= suva:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(suva:RecordNumberKey,'suva:RecordNumberKey',1)
  SELF.AddKey(suva:RunDateKey,'By Run Date',0)
  SELF.AddKey(suva:DateOnly,'suva:DateOnly',0)
  Access:SUPVALA &= SELF


Hide:Relate:SUPVALA.Init PROCEDURE
  CODE
  Hide:Access:SUPVALA.Init
  SELF.Init(Access:SUPVALA,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:SUPPLIER)


Hide:Access:SUPVALA.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SUPVALA &= NULL


Hide:Relate:SUPVALA.Kill PROCEDURE

  CODE
  Hide:Access:SUPVALA.Kill
  PARENT.Kill
  Relate:SUPVALA &= NULL


Hide:Access:ESTPARTS.Init PROCEDURE
  CODE
  SELF.Init(ESTPARTS,GlobalErrors)
  SELF.Buffer &= epr:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(epr:Part_Number_Key,'By Part Number',0)
  SELF.AddKey(epr:record_number_key,'epr:record_number_key',1)
  SELF.AddKey(epr:Part_Ref_Number2_Key,'By Part Ref Number',0)
  SELF.AddKey(epr:Date_Ordered_Key,'By Date Ordered',0)
  SELF.AddKey(epr:Part_Ref_Number_Key,'By Part Ref Number',0)
  SELF.AddKey(epr:Order_Number_Key,'epr:Order_Number_Key',0)
  SELF.AddKey(epr:Supplier_Key,'epr:Supplier_Key',0)
  SELF.AddKey(epr:Order_Part_Key,'By Order Part Number',0)
  Access:ESTPARTS &= SELF


Hide:Relate:ESTPARTS.Init PROCEDURE
  CODE
  Hide:Access:ESTPARTS.Init
  SELF.Init(Access:ESTPARTS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:SUPPLIER)
  SELF.AddRelation(Relate:JOBS)


Hide:Access:ESTPARTS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ESTPARTS &= NULL


Hide:Relate:ESTPARTS.Kill PROCEDURE

  CODE
  Hide:Access:ESTPARTS.Kill
  PARENT.Kill
  Relate:ESTPARTS &= NULL


Hide:Access:TRAEMAIL.Init PROCEDURE
  CODE
  SELF.Init(TRAEMAIL,GlobalErrors)
  SELF.Buffer &= tre:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(tre:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(tre:RecipientKey,'By Recipient Type',0)
  SELF.AddKey(tre:ContactNameKey,'By Contact Name',0)
  Access:TRAEMAIL &= SELF


Hide:Relate:TRAEMAIL.Init PROCEDURE
  CODE
  Hide:Access:TRAEMAIL.Init
  SELF.Init(Access:TRAEMAIL,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRADEACC)


Hide:Access:TRAEMAIL.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRAEMAIL &= NULL


Hide:Relate:TRAEMAIL.Kill PROCEDURE

  CODE
  Hide:Access:TRAEMAIL.Kill
  PARENT.Kill
  Relate:TRAEMAIL &= NULL


Hide:Access:SUPVALB.Init PROCEDURE
  CODE
  SELF.Init(SUPVALB,GlobalErrors)
  SELF.Buffer &= suvb:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(suvb:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(suvb:RunDateKey,'By Run Date',0)
  SELF.AddKey(suvb:LocationKey,'By Location',0)
  SELF.AddKey(suvb:DateOnly,'suvb:DateOnly',0)
  Access:SUPVALB &= SELF


Hide:Relate:SUPVALB.Init PROCEDURE
  CODE
  Hide:Access:SUPVALB.Init
  SELF.Init(Access:SUPVALB,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:SUPPLIER)


Hide:Access:SUPVALB.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SUPVALB &= NULL


Hide:Relate:SUPVALB.Kill PROCEDURE

  CODE
  Hide:Access:SUPVALB.Kill
  PARENT.Kill
  Relate:SUPVALB &= NULL


Hide:Access:JOBS.Init PROCEDURE
  CODE
  SELF.Init(JOBS,GlobalErrors)
  SELF.Buffer &= job:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(job:Ref_Number_Key,'By Job Number',1)
  SELF.AddKey(job:Model_Unit_Key,'By Job Number',0)
  SELF.AddKey(job:EngCompKey,'By Job Number',0)
  SELF.AddKey(job:EngWorkKey,'By Job Number',0)
  SELF.AddKey(job:Surname_Key,'By Surname',0)
  SELF.AddKey(job:MobileNumberKey,'By Mobile Number',0)
  SELF.AddKey(job:ESN_Key,'By E.S.N. / I.M.E.I.',0)
  SELF.AddKey(job:MSN_Key,'By M.S.N.',0)
  SELF.AddKey(job:AccountNumberKey,'By Account Number',0)
  SELF.AddKey(job:AccOrdNoKey,'By Order Number',0)
  SELF.AddKey(job:Model_Number_Key,'By Model Number',0)
  SELF.AddKey(job:Engineer_Key,'By Engineer',0)
  SELF.AddKey(job:Date_Booked_Key,'By Date Booked',0)
  SELF.AddKey(job:DateCompletedKey,'By Date Completed',0)
  SELF.AddKey(job:ModelCompKey,'By Completed Date',0)
  SELF.AddKey(job:By_Status,'By Job Number',0)
  SELF.AddKey(job:StatusLocKey,'By Job Number',0)
  SELF.AddKey(job:Location_Key,'By Location',0)
  SELF.AddKey(job:Third_Party_Key,'By Third Party',0)
  SELF.AddKey(job:ThirdEsnKey,'By ESN',0)
  SELF.AddKey(job:ThirdMsnKey,'By MSN',0)
  SELF.AddKey(job:PriorityTypeKey,'By Priority',0)
  SELF.AddKey(job:Unit_Type_Key,'By Unit Type',0)
  SELF.AddKey(job:EDI_Key,'By Job Number',0)
  SELF.AddKey(job:InvoiceNumberKey,'By Invoice_Number',0)
  SELF.AddKey(job:WarInvoiceNoKey,'By Invoice Number',0)
  SELF.AddKey(job:Batch_Number_Key,'By Job Number',0)
  SELF.AddKey(job:Batch_Status_Key,'By Ref Number',0)
  SELF.AddKey(job:BatchModelNoKey,'By Job Number',0)
  SELF.AddKey(job:BatchInvoicedKey,'By Ref Number',0)
  SELF.AddKey(job:BatchCompKey,'By Job Number',0)
  SELF.AddKey(job:ChaInvoiceKey,'job:ChaInvoiceKey',0)
  SELF.AddKey(job:InvoiceExceptKey,'By Job Number',0)
  SELF.AddKey(job:ConsignmentNoKey,'By Cosignment Number',0)
  SELF.AddKey(job:InConsignKey,'By Consignment Number',0)
  SELF.AddKey(job:ReadyToDespKey,'By Job Number',0)
  SELF.AddKey(job:ReadyToTradeKey,'By Job Number',0)
  SELF.AddKey(job:ReadyToCouKey,'By Job Number',0)
  SELF.AddKey(job:ReadyToAllKey,'By Job Number',0)
  SELF.AddKey(job:DespJobNumberKey,'By Job Number',0)
  SELF.AddKey(job:DateDespatchKey,'By Job Number',0)
  SELF.AddKey(job:DateDespLoaKey,'By Job Number',0)
  SELF.AddKey(job:DateDespExcKey,'By Job Number',0)
  SELF.AddKey(job:ChaRepTypeKey,'By Chargeable Repair Type',0)
  SELF.AddKey(job:WarRepTypeKey,'By Warranty Repair Type',0)
  SELF.AddKey(job:ChaTypeKey,'job:ChaTypeKey',0)
  SELF.AddKey(job:WarChaTypeKey,'job:WarChaTypeKey',0)
  SELF.AddKey(job:Bouncer_Key,'By Job Number',0)
  SELF.AddKey(job:EngDateCompKey,'By Date Completed',0)
  SELF.AddKey(job:ExcStatusKey,'By Job Number',0)
  SELF.AddKey(job:LoanStatusKey,'By Job Number',0)
  SELF.AddKey(job:ExchangeLocKey,'By Job Number',0)
  SELF.AddKey(job:LoanLocKey,'By Job Number',0)
  SELF.AddKey(job:BatchJobKey,'By Job Number',0)
  SELF.AddKey(job:BatchStatusKey,'By Job Number',0)
  Access:JOBS &= SELF


Hide:Relate:JOBS.Init PROCEDURE
  CODE
  Hide:Access:JOBS.Init
  SELF.Init(Access:JOBS,1)
  DO AddRelations_1
  DO AddRelations_2
  DO AddRelations_3

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBSENG,RI:CASCADE,RI:CASCADE,joe:UserCodeKey)
  SELF.AddRelationLink(job:Ref_Number,joe:JobNumber)
  SELF.AddRelation(Relate:AUDSTATS,RI:CASCADE,RI:CASCADE,aus:DateChangedKey)
  SELF.AddRelationLink(job:Ref_Number,aus:RefNumber)
  SELF.AddRelation(Relate:JOBSE,RI:CASCADE,RI:CASCADE,jobe:RefNumberKey)
  SELF.AddRelationLink(job:Ref_Number,jobe:RefNumber)
  SELF.AddRelation(Relate:JOBTHIRD,RI:CASCADE,RI:CASCADE,jot:RefNumberKey)
  SELF.AddRelationLink(job:Ref_Number,jot:RefNumber)
  SELF.AddRelation(Relate:JOBNOTES,RI:CASCADE,RI:CASCADE,jbn:RefNumberKey)
  SELF.AddRelationLink(job:Ref_Number,jbn:RefNumber)
  SELF.AddRelation(Relate:PRIORITY)
  SELF.AddRelation(Relate:LOCINTER)
  SELF.AddRelation(Relate:TRDBATCH,RI:CASCADE,RI:None,trb:JobNumberKey)
  SELF.AddRelationLink(job:Ref_Number,trb:Ref_Number)
  SELF.AddRelation(Relate:CONTHIST,RI:CASCADE,RI:CASCADE,cht:Ref_Number_Key)
  SELF.AddRelationLink(job:Ref_Number,cht:Ref_Number)
  SELF.AddRelation(Relate:TRDPARTY)

AddRelations_2 ROUTINE
  SELF.AddRelation(Relate:REPAIRTY)
  SELF.AddRelation(Relate:JOBPAYMT_ALIAS,RI:CASCADE,RI:CASCADE,jpt_ali:All_Date_Key)
  SELF.AddRelationLink(job:Ref_Number,jpt_ali:Ref_Number)
  SELF.AddRelation(Relate:INVOICE,RI:CASCADE,RI:RESTRICT,inv:Invoice_Number_Key)
  SELF.AddRelationLink(job:Invoice_Number,inv:Invoice_Number)
  SELF.AddRelation(Relate:MANUFACT)
  SELF.AddRelation(Relate:WARPARTS,RI:CASCADE,RI:CASCADE,wpr:Part_Number_Key)
  SELF.AddRelationLink(job:Ref_Number,wpr:Ref_Number)
  SELF.AddRelation(Relate:AUDIT,RI:CASCADE,RI:CASCADE,aud:Ref_Number_Key)
  SELF.AddRelationLink(job:Ref_Number,aud:Ref_Number)
  SELF.AddRelation(Relate:ESTPARTS,RI:CASCADE,RI:CASCADE,epr:Part_Number_Key)
  SELF.AddRelationLink(job:Ref_Number,epr:Ref_Number)
  SELF.AddRelation(Relate:JOBSTAGE,RI:CASCADE,RI:CASCADE,jst:Ref_Number_Key)
  SELF.AddRelationLink(job:Ref_Number,jst:Ref_Number)
  SELF.AddRelation(Relate:USERS)
  SELF.AddRelation(Relate:JOBEXHIS,RI:CASCADE,RI:CASCADE,jxh:Ref_Number_Key)
  SELF.AddRelationLink(job:Ref_Number,jxh:Ref_Number)

AddRelations_3 ROUTINE
  SELF.AddRelation(Relate:JOBLOHIS,RI:CASCADE,RI:CASCADE,jlh:Ref_Number_Key)
  SELF.AddRelationLink(job:Ref_Number,jlh:Ref_Number)
  SELF.AddRelation(Relate:MODELNUM)
  SELF.AddRelation(Relate:SUBTRACC)
  SELF.AddRelation(Relate:PARTS,RI:CASCADE,RI:CASCADE,par:Part_Number_Key)
  SELF.AddRelationLink(job:Ref_Number,par:Ref_Number)
  SELF.AddRelation(Relate:UNITTYPE)
  SELF.AddRelation(Relate:JOBACC,RI:CASCADE,RI:CASCADE,jac:Ref_Number_Key)
  SELF.AddRelationLink(job:Ref_Number,jac:Ref_Number)


Hide:Access:JOBS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBS &= NULL


Hide:Access:JOBS.PrimeFields PROCEDURE

  CODE
  job:date_booked = Today()
  job:time_booked = Clock()
  job:Cancelled = 'NO'
  job:Warranty_Job = 'NO'
  job:Chargeable_Job = 'NO'
  job:Workshop = 'NO'
  job:Insurance = 'NO'
  job:Physical_Damage = 'NO'
  job:Intermittent_Fault = 'NO'
  job:Loan_Status = 'NOT ISSUED'
  job:Exchange_Status = 'NOT ISSUED'
  job:POP = 'NO'
  job:In_Repair = 'NO'
  job:On_Test = 'NO'
  job:Estimate_Ready = 'NO'
  job:QA_Passed = 'NO'
  job:QA_Rejected = 'NO'
  job:QA_Second_Passed = 'NO'
  job:Date_Completed = 0
  job:Time_Completed = 0
  job:Completed = 'NO'
  job:Paid = 'NO'
  job:Paid_Warranty = 'NO'
  job:Ignore_Chargeable_Charges = 'NO'
  job:Ignore_Warranty_Charges = 'NO'
  job:Ignore_Estimate_Charges = 'NO'
  job:Loan_accessory = 'NO'
  job:Exchange_Authorised = 'NO'
  job:Loan_Authorised = 'NO'
  job:Exchange_Accessory = 'NO'
  job:Despatched = 'NO'
  job:Estimate = 'NO'
  job:Estimate_Accepted = 'NO'
  job:Estimate_Rejected = 'NO'
  job:Third_Party_Printed = 'NO'
  job:Invoice_Exception = 'NO'
  PARENT.PrimeFields


Hide:Relate:JOBS.Kill PROCEDURE

  CODE
  Hide:Access:JOBS.Kill
  PARENT.Kill
  Relate:JOBS &= NULL


Hide:Access:MODELNUM.Init PROCEDURE
  CODE
  SELF.Init(MODELNUM,GlobalErrors)
  SELF.Buffer &= mod:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(mod:Model_Number_Key,'By Model Number',0)
  SELF.AddKey(mod:Manufacturer_Key,'By Manufacturer',0)
  SELF.AddKey(mod:Manufacturer_Unit_Type_Key,'By Unit Type',0)
  Access:MODELNUM &= SELF


Hide:Relate:MODELNUM.Init PROCEDURE
  CODE
  Hide:Access:MODELNUM.Init
  SELF.Init(Access:MODELNUM,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:PRODCODE,RI:CASCADE,RI:CASCADE,prd:ModelProductKey)
  SELF.AddRelationLink(mod:Model_Number,prd:ModelNumber)
  SELF.AddRelation(Relate:MODELCOL,RI:CASCADE,RI:CASCADE,moc:Colour_Key)
  SELF.AddRelationLink(mod:Model_Number,moc:Model_Number)
  SELF.AddRelation(Relate:MANUFACT)
  SELF.AddRelation(Relate:REPAIRTY,RI:CASCADE,RI:CASCADE,rep:Repair_Type_Key)
  SELF.AddRelationLink(mod:Manufacturer,rep:Manufacturer)
  SELF.AddRelationLink(mod:Model_Number,rep:Model_Number)
  SELF.AddRelation(Relate:TRDMODEL,RI:CASCADE,RI:RESTRICT,trm:Model_Number_Only_Key)
  SELF.AddRelationLink(mod:Model_Number,trm:Model_Number)
  SELF.AddRelation(Relate:ACCESSOR,RI:CASCADE,RI:None,acr:Accesory_Key)
  SELF.AddRelationLink(mod:Model_Number,acr:Model_Number)
  SELF.AddRelation(Relate:STOMODEL,RI:CASCADE,RI:None,stm:Manufacturer_Key)
  SELF.AddRelationLink(mod:Manufacturer,stm:Manufacturer)
  SELF.AddRelationLink(mod:Model_Number,stm:Model_Number)
  SELF.AddRelation(Relate:JOBS,RI:CASCADE,RI:None,job:Model_Number_Key)
  SELF.AddRelationLink(mod:Model_Number,job:Model_Number)


Hide:Access:MODELNUM.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MODELNUM &= NULL


Hide:Access:MODELNUM.PrimeFields PROCEDURE

  CODE
  mod:UnsupportedExchange = 0
  PARENT.PrimeFields


Hide:Relate:MODELNUM.Kill PROCEDURE

  CODE
  Hide:Access:MODELNUM.Kill
  PARENT.Kill
  Relate:MODELNUM &= NULL


Hide:Access:SUBEMAIL.Init PROCEDURE
  CODE
  SELF.Init(SUBEMAIL,GlobalErrors)
  SELF.Buffer &= sue:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(sue:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(sue:RecipientTypeKey,'By Recipient Type',0)
  SELF.AddKey(sue:ContactNameKey,'By Contact Name',0)
  Access:SUBEMAIL &= SELF


Hide:Relate:SUBEMAIL.Init PROCEDURE
  CODE
  Hide:Access:SUBEMAIL.Init
  SELF.Init(Access:SUBEMAIL,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:SUBTRACC)


Hide:Access:SUBEMAIL.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SUBEMAIL &= NULL


Hide:Relate:SUBEMAIL.Kill PROCEDURE

  CODE
  Hide:Access:SUBEMAIL.Kill
  PARENT.Kill
  Relate:SUBEMAIL &= NULL


Hide:Access:ORDERS.Init PROCEDURE
  CODE
  SELF.Init(ORDERS,GlobalErrors)
  SELF.Buffer &= ord:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(ord:Order_Number_Key,'By Order_Number',1)
  SELF.AddKey(ord:Printed_Key,'By Order Number',0)
  SELF.AddKey(ord:Supplier_Printed_Key,'By Order Number',0)
  SELF.AddKey(ord:Received_Key,'ord:Received_Key',0)
  SELF.AddKey(ord:Supplier_Key,'By Order Number',0)
  SELF.AddKey(ord:Supplier_Received_Key,'By Order Number',0)
  SELF.AddKey(ord:DateKey,'By Date',0)
  Access:ORDERS &= SELF


Hide:Relate:ORDERS.Init PROCEDURE
  CODE
  Hide:Access:ORDERS.Init
  SELF.Init(Access:ORDERS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:ORDPARTS,RI:CASCADE,RI:CASCADE,orp:Order_Number_Key)
  SELF.AddRelationLink(ord:Order_Number,orp:Order_Number)


Hide:Access:ORDERS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ORDERS &= NULL


Hide:Relate:ORDERS.Kill PROCEDURE

  CODE
  Hide:Access:ORDERS.Kill
  PARENT.Kill
  Relate:ORDERS &= NULL


Hide:Access:UNITTYPE.Init PROCEDURE
  CODE
  SELF.Init(UNITTYPE,GlobalErrors)
  SELF.Buffer &= uni:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(uni:Unit_Type_Key,'By Unit Type',0)
  SELF.AddKey(uni:ActiveKey,'By Unit Type',0)
  Access:UNITTYPE &= SELF


Hide:Relate:UNITTYPE.Init PROCEDURE
  CODE
  Hide:Access:UNITTYPE.Init
  SELF.Init(Access:UNITTYPE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STDCHRGE,RI:CASCADE,RI:RESTRICT,sta:Unit_Type_Only_Key)
  SELF.AddRelationLink(uni:Unit_Type,sta:Unit_Type)
  SELF.AddRelation(Relate:TRACHRGE,RI:CASCADE,RI:RESTRICT,trc:Unit_Type_Only_Key)
  SELF.AddRelationLink(uni:Unit_Type,trc:Unit_Type)
  SELF.AddRelation(Relate:SUBCHRGE,RI:CASCADE,RI:RESTRICT,suc:Unit_Type_Only_Key)
  SELF.AddRelationLink(uni:Unit_Type,suc:Unit_Type)
  SELF.AddRelation(Relate:DEFCHRGE,RI:CASCADE,RI:RESTRICT,dec:Unit_Type_Only_Key)
  SELF.AddRelationLink(uni:Unit_Type,dec:Unit_Type)
  SELF.AddRelation(Relate:JOBS,RI:CASCADE,RI:RESTRICT,job:Unit_Type_Key)
  SELF.AddRelationLink(uni:Unit_Type,job:Unit_Type)
  SELF.AddRelation(Relate:USUASSIG,RI:CASCADE,RI:RESTRICT,usu:Unit_Type_Only_Key)
  SELF.AddRelationLink(uni:Unit_Type,usu:Unit_Type)


Hide:Access:UNITTYPE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:UNITTYPE &= NULL


Hide:Access:UNITTYPE.PrimeFields PROCEDURE

  CODE
  uni:Active = 0
  PARENT.PrimeFields


Hide:Relate:UNITTYPE.Kill PROCEDURE

  CODE
  Hide:Access:UNITTYPE.Kill
  PARENT.Kill
  Relate:UNITTYPE &= NULL


Hide:Access:REPAIRTY.Init PROCEDURE
  CODE
  SELF.Init(REPAIRTY,GlobalErrors)
  SELF.Buffer &= rep:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(rep:Repair_Type_Key,'By Repair Type',0)
  SELF.AddKey(rep:Manufacturer_Key,'By Repair Type',0)
  SELF.AddKey(rep:Model_Number_Key,'By Repair Type',0)
  SELF.AddKey(rep:Repair_Type_Only_Key,'By Repair Type',0)
  SELF.AddKey(rep:Model_Chargeable_Key,'By Repair Type',0)
  SELF.AddKey(rep:Model_Warranty_Key,'By Repair Type',0)
  Access:REPAIRTY &= SELF


Hide:Relate:REPAIRTY.Init PROCEDURE
  CODE
  Hide:Access:REPAIRTY.Init
  SELF.Init(Access:REPAIRTY,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANUFACT)
  SELF.AddRelation(Relate:STDCHRGE,RI:CASCADE,RI:CASCADE,sta:Repair_Type_Key)
  SELF.AddRelationLink(rep:Model_Number,sta:Model_Number)
  SELF.AddRelationLink(rep:Repair_Type,sta:Repair_Type)
  SELF.AddRelation(Relate:TRACHRGE,RI:CASCADE,RI:RESTRICT,trc:Model_Repair_Key)
  SELF.AddRelationLink(rep:Model_Number,trc:Model_Number)
  SELF.AddRelationLink(rep:Repair_Type,trc:Repair_Type)
  SELF.AddRelation(Relate:SUBCHRGE,RI:CASCADE,RI:RESTRICT,suc:Model_Repair_Key)
  SELF.AddRelationLink(rep:Model_Number,suc:Model_Number)
  SELF.AddRelationLink(rep:Repair_Type,suc:Repair_Type)
  SELF.AddRelation(Relate:DEFCHRGE,RI:CASCADE,RI:RESTRICT,dec:Repair_Type_Only_Key)
  SELF.AddRelationLink(rep:Repair_Type,dec:Repair_Type)
  SELF.AddRelation(Relate:JOBS,RI:CASCADE,RI:None,job:ChaRepTypeKey)
  SELF.AddRelationLink(rep:Repair_Type,job:Repair_Type)
  SELF.AddRelation(Relate:JOBS_ALIAS,RI:CASCADE,RI:None,job_ali:WarRepTypeKey)
  SELF.AddRelationLink(rep:Repair_Type,job_ali:Repair_Type_Warranty)
  SELF.AddRelation(Relate:MODELNUM)


Hide:Access:REPAIRTY.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:REPAIRTY &= NULL


Hide:Access:REPAIRTY.PrimeFields PROCEDURE

  CODE
  rep:Chargeable = 'YES'
  rep:Warranty = 'YES'
  rep:CompFaultCoding = 0
  rep:ExcludeFromEDI = 0
  rep:ExcludeFromInvoicing = 0
  PARENT.PrimeFields


Hide:Relate:REPAIRTY.Kill PROCEDURE

  CODE
  Hide:Access:REPAIRTY.Kill
  PARENT.Kill
  Relate:REPAIRTY &= NULL

