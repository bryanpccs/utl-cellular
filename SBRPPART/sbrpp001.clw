

   MEMBER('sbrppart.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBRPP001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SBRPP002.INC'),ONCE        !Req'd for module callout resolution
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

tmp:Supplier         STRING(30)
save_tmppen_id       USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_job_id          USHORT,AUTO
save_sup_id          USHORT,AUTO
save_tmpovr_id       USHORT,AUTO
tmp:UserID           STRING(3)
save_shi_id          USHORT,AUTO
tmp:SiteLocation     STRING(30)
save_sto_id          USHORT,AUTO
tmp:ShelfLocation    STRING(30)
tmp:NumberOfWeeks    LONG(1)
re                   LONG
ReportFile           STRING(255)
WindowTitle          STRING(255)
SelectionString      STRING(2000)
CrystalReport        CLASS(CrystalRpt)
                     END
tmp:AccountNumber    STRING(30)
tmp:ModelNumber      STRING(30)
tmp:PartNumber       STRING(30)
tmp:Overdue          BYTE(0)
tmp:Location         STRING(30)
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?tmp:SiteLocation
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:AccountNumber
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?tmp:ModelNumber
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
FDCB1::View:FileDropCombo VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
FDCB3::View:FileDropCombo VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                     END
mo:SelectedTab::Sheet1    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Window
mo:SelectedField  Long
mo:WinType        equate(Win:Window)
window               WINDOW('Pending Parts Report Criteria'),AT(,,231,131),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,224,96),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Account Number'),AT(8,20),USE(?tmp:AccountNumber:Prompt)
                           COMBO(@s30),AT(84,20,124,10),USE(tmp:AccountNumber),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('65L(2)|M@s15@120L(2)|M@s30@'),DROP(10,200),FROM(Queue:FileDropCombo)
                           PROMPT('Model Number'),AT(8,36),USE(?tmp:ModelNumber:Prompt)
                           COMBO(@s30),AT(84,36,124,10),USE(tmp:ModelNumber),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:2)
                           PROMPT('Location'),AT(8,52),USE(?Prompt1:2)
                           COMBO(@s30),AT(84,52,124,10),USE(tmp:SiteLocation),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                           PROMPT('Part Number'),AT(8,68),USE(?tmp:PartNumber:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,68,124,10),USE(tmp:PartNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Part Number'),TIP('Part Number'),UPR
                           BUTTON,AT(212,68,10,10),USE(?LookupPartNumber),SKIP,FONT('Arial',8,,,CHARSET:ANSI),ICON('List3.ico')
                           CHECK('Include Overdue Only'),AT(84,84),USE(tmp:Overdue),MSG('Include Overdue Only'),TIP('Include Overdue Only'),VALUE('1','0')
                         END
                       END
                       BUTTON('&Print'),AT(112,108,56,16),USE(?Button2),LEFT,ICON(ICON:Print1)
                       BUTTON('Cancel'),AT(168,108,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,104,224,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB1                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB3                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
look:tmp:PartNumber                Like(tmp:PartNumber)
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?tmp:AccountNumber:Prompt{prop:FontColor} = -1
    ?tmp:AccountNumber:Prompt{prop:Color} = 15066597
    If ?tmp:AccountNumber{prop:ReadOnly} = True
        ?tmp:AccountNumber{prop:FontColor} = 65793
        ?tmp:AccountNumber{prop:Color} = 15066597
    Elsif ?tmp:AccountNumber{prop:Req} = True
        ?tmp:AccountNumber{prop:FontColor} = 65793
        ?tmp:AccountNumber{prop:Color} = 8454143
    Else ! If ?tmp:AccountNumber{prop:Req} = True
        ?tmp:AccountNumber{prop:FontColor} = 65793
        ?tmp:AccountNumber{prop:Color} = 16777215
    End ! If ?tmp:AccountNumber{prop:Req} = True
    ?tmp:AccountNumber{prop:Trn} = 0
    ?tmp:AccountNumber{prop:FontStyle} = font:Bold
    ?tmp:ModelNumber:Prompt{prop:FontColor} = -1
    ?tmp:ModelNumber:Prompt{prop:Color} = 15066597
    If ?tmp:ModelNumber{prop:ReadOnly} = True
        ?tmp:ModelNumber{prop:FontColor} = 65793
        ?tmp:ModelNumber{prop:Color} = 15066597
    Elsif ?tmp:ModelNumber{prop:Req} = True
        ?tmp:ModelNumber{prop:FontColor} = 65793
        ?tmp:ModelNumber{prop:Color} = 8454143
    Else ! If ?tmp:ModelNumber{prop:Req} = True
        ?tmp:ModelNumber{prop:FontColor} = 65793
        ?tmp:ModelNumber{prop:Color} = 16777215
    End ! If ?tmp:ModelNumber{prop:Req} = True
    ?tmp:ModelNumber{prop:Trn} = 0
    ?tmp:ModelNumber{prop:FontStyle} = font:Bold
    ?Prompt1:2{prop:FontColor} = -1
    ?Prompt1:2{prop:Color} = 15066597
    If ?tmp:SiteLocation{prop:ReadOnly} = True
        ?tmp:SiteLocation{prop:FontColor} = 65793
        ?tmp:SiteLocation{prop:Color} = 15066597
    Elsif ?tmp:SiteLocation{prop:Req} = True
        ?tmp:SiteLocation{prop:FontColor} = 65793
        ?tmp:SiteLocation{prop:Color} = 8454143
    Else ! If ?tmp:SiteLocation{prop:Req} = True
        ?tmp:SiteLocation{prop:FontColor} = 65793
        ?tmp:SiteLocation{prop:Color} = 16777215
    End ! If ?tmp:SiteLocation{prop:Req} = True
    ?tmp:SiteLocation{prop:Trn} = 0
    ?tmp:SiteLocation{prop:FontStyle} = font:Bold
    ?tmp:PartNumber:Prompt{prop:FontColor} = -1
    ?tmp:PartNumber:Prompt{prop:Color} = 15066597
    If ?tmp:PartNumber{prop:ReadOnly} = True
        ?tmp:PartNumber{prop:FontColor} = 65793
        ?tmp:PartNumber{prop:Color} = 15066597
    Elsif ?tmp:PartNumber{prop:Req} = True
        ?tmp:PartNumber{prop:FontColor} = 65793
        ?tmp:PartNumber{prop:Color} = 8454143
    Else ! If ?tmp:PartNumber{prop:Req} = True
        ?tmp:PartNumber{prop:FontColor} = 65793
        ?tmp:PartNumber{prop:Color} = 16777215
    End ! If ?tmp:PartNumber{prop:Req} = True
    ?tmp:PartNumber{prop:Trn} = 0
    ?tmp:PartNumber{prop:FontStyle} = font:Bold
    ?tmp:Overdue{prop:Font,3} = -1
    ?tmp:Overdue{prop:Color} = 15066597
    ?tmp:Overdue{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Cancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:AccountNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CPNDPRTS.Open
  Relate:DEFAULTS.Open
  Relate:JOBS.Open
  Access:STOCK.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  ThisMakeover.SetWindow(Win:Window)
  mo:SelectedTab::Sheet1 = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab::Sheet1,?Sheet1)
  window{prop:buffer} = 1
  IF ?tmp:PartNumber{Prop:Tip} AND ~?LookupPartNumber{Prop:Tip}
     ?LookupPartNumber{Prop:Tip} = 'Select ' & ?tmp:PartNumber{Prop:Tip}
  END
  IF ?tmp:PartNumber{Prop:Msg} AND ~?LookupPartNumber{Prop:Msg}
     ?LookupPartNumber{Prop:Msg} = 'Select ' & ?tmp:PartNumber{Prop:Msg}
  END
  ThisMakeover.SetWindow(Win:Window)
  mo:SelectedTab::Sheet1 = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab::Sheet1,?Sheet1)
  window{prop:buffer} = 1
  FDCB2.Init(tmp:SiteLocation,?tmp:SiteLocation,Queue:FileDropCombo:1.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo:1,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo:1
  FDCB2.AddSortOrder(loc:Location_Key)
  FDCB2.AddField(loc:Location,FDCB2.Q.loc:Location)
  FDCB2.AddField(loc:RecordNumber,FDCB2.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  FDCB1.Init(tmp:AccountNumber,?tmp:AccountNumber,Queue:FileDropCombo.ViewPosition,FDCB1::View:FileDropCombo,Queue:FileDropCombo,Relate:SUBTRACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB1.Q &= Queue:FileDropCombo
  FDCB1.AddSortOrder(sub:Account_Number_Key)
  FDCB1.AddField(sub:Account_Number,FDCB1.Q.sub:Account_Number)
  FDCB1.AddField(sub:Company_Name,FDCB1.Q.sub:Company_Name)
  FDCB1.AddField(sub:RecordNumber,FDCB1.Q.sub:RecordNumber)
  ThisWindow.AddItem(FDCB1.WindowComponent)
  FDCB1.DefaultFill = 0
  FDCB3.Init(tmp:ModelNumber,?tmp:ModelNumber,Queue:FileDropCombo:2.ViewPosition,FDCB3::View:FileDropCombo,Queue:FileDropCombo:2,Relate:MODELNUM,ThisWindow,GlobalErrors,0,1,0)
  FDCB3.Q &= Queue:FileDropCombo:2
  FDCB3.AddSortOrder(mod:Model_Number_Key)
  FDCB3.AddField(mod:Model_Number,FDCB3.Q.mod:Model_Number)
  ThisWindow.AddItem(FDCB3.WindowComponent)
  FDCB3.DefaultFill = 0
  SELF.SetAlerts()
    ThisMakeover.Refresh()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CPNDPRTS.Close
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickPartNumber
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:SiteLocation
      FDCB3.ResetQueue(1)
    OF ?tmp:PartNumber
      IF tmp:PartNumber OR ?tmp:PartNumber{Prop:Req}
        sto:Part_Number = tmp:PartNumber
        sto:Location = tmp:SiteLocation
        GLO:Select1 = tmp:SiteLocation
        !Save Lookup Field Incase Of error
        look:tmp:PartNumber        = tmp:PartNumber
        IF Access:STOCK.TryFetch(sto:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:PartNumber = sto:Part_Number
          ELSE
            CLEAR(sto:Location)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            tmp:PartNumber = look:tmp:PartNumber
            SELECT(?tmp:PartNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupPartNumber
      ThisWindow.Update
      sto:Part_Number = tmp:PartNumber
      GLO:Select1 = tmp:SiteLocation
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:PartNumber = sto:Part_Number
          Select(?+1)
      ELSE
          Select(?tmp:PartNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:PartNumber)
    OF ?Button2
      ThisWindow.Update
          recordspercycle         = 25
          recordsprocessed        = 0
          percentprogress         = 0
          progress:thermometer    = 0
          thiswindow.reset(1)
          open(progresswindow)
      
          ?progress:userstring{prop:text} = 'Running...'
          ?progress:pcttext{prop:text} = '0% Completed'
      
          recordstoprocess    = Records(Jobs)
      
          tmp:UserID = Format(Clock(),@n08)
      
          Set(DEFAULTS)
          Access:DEFAULTS.Next()
          count# = 0
          Setcursor(Cursor:Wait)
      
      
          Save_job_ID = Access:JOBS.SaveFile()
          If tmp:ModelNumber <> ''
              Access:JOBS.ClearKey(job:Model_Unit_Key)
              job:Model_Number = tmp:ModelNumber
              Set(job:Model_Unit_Key,job:Model_Unit_Key)
             
          Else !tmp:ModelNumber <> ''
              If tmp:AccountNumber <> ''    
                  Access:JOBS.ClearKey(job:AccountNumberKey)
                  job:Account_Number = tmp:AccountNumber
                  Set(job:AccountNumberKey,job:AccountNumberKey)
                 
              Else !If tmp:AccountNumber <> ''    
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  Set(job:Ref_Number_Key)
                  
              End !If tmp:AccountNumber <> ''    
          End !tmp:ModelNumber <> ''
          Loop
              If Access:JOBS.NEXT()
                 Break
              End !If
              Do GetNextRecord2
              cancelcheck# += 1
              If cancelcheck# > (RecordsToProcess/100)
                  Do cancelcheck
                  If tmp:cancel = 1
                      Break
                  End!If tmp:cancel = 1
                  cancelcheck# = 0
              End!If cancelcheck# > 50
      
              If tmp:ModelNumber <> ''
                  If job:Model_Number <> tmp:ModelNumber      |
                      Then Break.  ! End If
                  
              Else !If tmp:ModelNumber <> ''
                  If tmp:AccountNumber <> ''        
                      If job:Account_Number <> tmp:AccountNumber      |
                          Then Break.  ! End If            
                  End !If tmp:AccountNumber <> ''        
              End !If tmp:ModelNumber <> ''
      
              Save_par_ID = Access:PARTS.SaveFile()
              Access:PARTS.ClearKey(par:Part_Number_Key)
              par:Ref_Number  = job:Ref_Number
              If tmp:PartNumber <> ''
                  par:Part_Number = tmp:PartNumber
              End !If tmp:PartNumber <> ''
              Set(par:Part_Number_Key,par:Part_Number_Key)
              Loop
                  If Access:PARTS.NEXT()
                     Break
                  End !If
                  If par:Ref_number <> job:Ref_number
                      Break
                  End !If par:Ref_number <> job:Ref_number
      
                  If tmp:PartNumber <> ''
                      If par:Part_Number <> tmp:PartNumber
                          Break
                      End !If par:Part_Number <> tmp:PartNumber
                  End !If tmp:PartNumber <> ''
      
                  ! Start Change BE026 BE(4/2/04)
                  !!If par:Pending_Ref_Number = ''
                  !!    Cycle
                  !!End !If par:Pending_Ref_Number = ''
                  !If par:Order_Number = ''
                  !    Cycle
                  !End !If par:Order_Number <> ''
                  IF ((par:Pending_Ref_Number = 0) AND (par:Order_Number = 0)) THEN
                      CYCLE
                  END
                  ! Start Change BE026 BE(4/2/04)
      
                  If par:Date_Received <> ''
                      Cycle
                  End !If par:Date_Received <> ''
      
                  tmp:Location    = 'NON STOCK'
                  !Only include Pending Parts that haven't been received
                  Access:STOCK.Clearkey(sto:Ref_Number_Key)
                  sto:Ref_number  = par:Part_Ref_Number
                  If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      !Found
                      If tmp:SiteLocation <> ''
                          If sto:Location <> tmp:SiteLocation
                              Cycle
                          End !If sto:Location <> tmp:SiteLocation
                      End !If tmp:SiteLocation <> ''
                      tmp:Location    = sto:Location
                  Else!If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      If tmp:SiteLocation <> ''
                          Cycle
                      End !If tmp:SiteLocation <> ''
                  End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      
                  If tmp:Overdue
                      Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
                      sup:Company_Name = par:Supplier
                      If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                          If par:Date_Ordered >= (Today() - sup:Normal_Supply_Period)
                              Cycle
                          End !If par:Date_Ordered >= (Today() - sup:Normal_Supply_Period)
                      End !If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                  End !If tmp:Overdue
      
                  If Access:CPNDPRTS.Primerecord() = Level:Benign
                      tmppen:UserID           = tmp:UserID
                      tmppen:JobNumber        = job:Ref_Number
                      tmppen:DateBooked       = job:Date_Booked
                      tmppen:AccountNumber    = job:Account_Number
                      tmppen:ModelNumber      = job:Model_Number
                      tmppen:PartNumber       = par:Part_Number
                      tmppen:Description      = par:Description
                      tmppen:Quantity         = par:Quantity
                      tmppen:DateOrdered      = par:Date_Ordered
                      tmppen:Location         = tmp:Location
                      tmppen:Status           = job:Current_Status
                      count# += 1
                      If Access:CPNDPRTS.Tryinsert()
                          Access:CPNDPRTS.Cancelautoinc()
                      End!If Access:CPNDPRTS.Tryinsert()
                  End!If Access:CPNDPRTS.Primerecord() = Level:Benign
                  
              End !Loop
              Access:PARTS.RestoreFile(Save_par_ID)
      
      
              Save_wpr_ID = Access:WARPARTS.SaveFile()
              Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
              wpr:Ref_Number  = job:Ref_Number
              If tmp:PartNumber <> ''
                  wpr:Part_Number = tmp:PartNumber
              End !If tmp:PartNumber <> ''
              Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
              Loop
                  If Access:WARPARTS.NEXT()
                     Break
                  End !If
                  If wpr:Ref_number <> job:Ref_number
                      Break
                  End !If wpr:Ref_number <> job:Ref_number
      
                  If tmp:PartNumber <> ''
                      If wpr:Part_Number <> tmp:PartNumber
                          Break
                      End !If wpr:Part_Number <> tmp:PartNumber
                  End !If tmp:PartNumber <> ''
      
                  ! Start Change BE026 BE(4/2/04)
                  !!If wpr:Pending_Ref_Number = ''
                  !!     Cycle
                  !!End !If wpr:Pending_Ref_Number = ''
                  !If wpr:Order_Number = ''
                  !    Cycle
                  !End !If wpr:Order_Number <> ''
                  IF ((wpr:Pending_Ref_Number = 0) AND (wpr:Order_Number = 0)) THEN
                      CYCLE
                  END
                  ! Start Change BE026 BE(4/2/04)
      
                  If wpr:Date_Received <> ''
                      Cycle
                  End !If wpr:Date_Received <> ''
      
                  tmp:Location = 'NON STOCK'
                  !Only include Pending Parts that haven't been received
                  Access:STOCK.Clearkey(sto:Ref_Number_Key)
                  sto:Ref_number  = wpr:Part_Ref_Number
                  If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      !Found
                      If tmp:SiteLocation <> ''
                          If sto:Location <> tmp:SiteLocation
                              Cycle
                          End !If sto:Location <> tmp:SiteLocation
                      End !If tmp:SiteLocation <> ''
                      tmp:Location = sto:Location
                  Else!If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      If tmp:SiteLocation <> ''
                          Cycle
                      End !If tmp:SiteLocation <> ''
                  End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      
                  If tmp:Overdue
                      Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
                      sup:Company_Name = wpr:Supplier
                      If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                          If wpr:Date_Ordered >= (Today() - sup:Normal_Supply_Period)
                              Cycle
                          End !If wpr:Date_Ordered >= (Today() - sup:Normal_Supply_Period)
                      End !If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                  End !If tmp:Overdue
      
                  If Access:CPNDPRTS.Primerecord() = Level:Benign
                      tmppen:UserID   = tmp:UserID
                      tmppen:JobNumber        = job:Ref_Number
                      tmppen:DateBooked       = job:Date_Booked
                      tmppen:AccountNumber    = job:Account_Number
                      tmppen:ModelNumber      = job:Model_Number
                      tmppen:PartNumber       = wpr:Part_Number
                      tmppen:Description      = wpr:Description
                      tmppen:Quantity         = wpr:Quantity
                      tmppen:DateOrdered      = wpr:Date_Ordered
                      tmppen:Location         = tmp:Location
                      tmppen:Status           = job:Current_Status
                      count# += 1
                      If Access:CPNDPRTS.Tryinsert()
                          Access:CPNDPRTS.Cancelautoinc()
                      End!If Access:CPNDPRTS.Tryinsert()
                  End!If Access:CPNDPRTS.Primerecord() = Level:Benign
                  
              End !Loop
              Access:WARPARTS.RestoreFile(Save_wpr_ID)
      
          End !Loop
          Access:JOBS.RestoreFile(Save_job_ID)
      
          Setcursor()
      !---After Routine
          Do EndPrintRun
          close(progresswindow)
      
          If count# = 0
              Case MessageEx('There are no records that match the selected criteria.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          Else!If count# = 0
              ReportFile = 'PENPARTS.RPT'
              re = CrystalReport.Init(ReportFile)
              CrystalReport.PassStringParameter('Par.UserID',tmp:UserID)
              If tmp:AccountNumber = ''
                  CrystalReport.PassStringParameter('Par.AccountNumber','ALL')
              Else!If tmp:Supplier = ''
                  CrystalReport.PassStringParameter('Par.AccountNumber',tmp:AccountNumber)
              End!If tmp:Supplier = ''
              If tmp:ModelNumber = ''
                  CrystalReport.PassStringParameter('Par.ModelNumber','ALL')
              Else!If tmp:Supplier = ''
                  CrystalReport.PassStringParameter('Par.ModelNumber',tmp:ModelNumber)
              End!If tmp:Supplier = ''
              If tmp:SiteLocation = ''
                  CrystalReport.PassStringParameter('Par.SiteLocation','ALL')
              Else !If tmp:SiteLocation = ''
                  CrystalReport.PassStringParameter('Par.SiteLocation',tmp:SiteLocation)
              End !If tmp:SiteLocation = ''
              If tmp:PartNumber = ''
                  CrystalReport.PassStringParameter('Par.PartNumber','ALL')
              Else !If tmp:SiteLocation = ''
                  CrystalReport.PassStringParameter('Par.PartNumber',tmp:PartNumber)
              End !If tmp:SiteLocation = ''
              If tmp:Overdue
                  CrystalReport.PassStringParameter('Par.Overdue','YES')
              Else !If tmp:Overdue
                  CrystalReport.PassStringParameter('Par.Overdue','NO')
              End !If tmp:Overdue
      
              CrystalReport.PassStringParameter('Par.DefUserName',def:User_Name)
              CrystalReport.PassStringParameter('Par.DefAddressLine1',def:Address_Line1)
              CrystalReport.PassStringParameter('Par.DefAddressLine2',def:Address_Line2)
              CrystalReport.PassStringParameter('Par.DefAddressLine3',def:Address_Line3)
              CrystalReport.PassStringParameter('Par.DefPostcode',def:Postcode)
              CrystalReport.PassStringParameter('Par.DefTelephoneNumber',def:Telephone_Number)
              CrystalReport.PassStringParameter('Par.DefFaxNumber',def:Fax_Number)
              CrystalReport.PassStringParameter('Par.DefEmailAddress',def:EmailAddress)
              re = CrystalReport.Preview(WindowTitle,'MAX','','',TRUE,TRUE,TRUE)
              If re = 0
                  Case MessageEx('An error has occured with this report','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
              End!If re = 0
          End!If count# = 0
      
          Save_tmppen_ID = Access:CPNDPRTS.SaveFile()
          Access:CPNDPRTS.ClearKey(tmppen:JobNumberKey)
          tmppen:UserID    = tmp:UserID
          Set(tmppen:JobNumberKey,tmppen:JobNumberKey)
          Loop
              If Access:CPNDPRTS.NEXT()
                 Break
              End !If
              If tmppen:UserID    <> tmp:UserID      |
                  Then Break.  ! End If
              Delete(CPNDPRTS)
          End !Loop
          Access:CPNDPRTS.RestoreFile(Save_tmppen_ID)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisMakeover.TakeEvent(Win:Window,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      mo:SelectedTab::Sheet1 = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab::Sheet1,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()

