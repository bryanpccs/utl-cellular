

   MEMBER('celraprs.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA002.INC'),ONCE        !Local module procedure declarations
                     END


Return_To_Stock PROCEDURE                             !Generated from procedure template - Window

FilesOpened          BYTE
save_loa_id          USHORT,AUTO
save_xch_id          USHORT,AUTO
job_queue_temp       QUEUE,PRE(jobque)
Job_Number           LONG
Type                 STRING(4)
record_number        LONG
                     END
job_number_temp      REAL
engineer_temp        STRING(3)
engineer_name_temp   STRING(30)
old_status_temp      STRING(30)
Location_temp        STRING(30)
update_text_temp     STRING(100)
tmp:esn              STRING(16)
error_text_temp      STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Rapid Return Loan / Exchange Unit To Stock'),AT(,,327,119),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE,MDI
                       SHEET,AT(4,4,212,84),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Defaults'),USE(?Tab1)
                           PROMPT('Select the I.M.E.I. Number of the Loan/Exchange Unit you wish to make Available.'),AT(8,12,204,24),USE(?Prompt2),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('I.M.E.I. Number'),AT(8,48),USE(?tmp:esn:Prompt)
                           ENTRY(@s16),AT(84,48,124,10),USE(tmp:esn),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           STRING(@s60),AT(8,72,200,12),USE(error_text_temp),FONT(,10,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       SHEET,AT(220,4,104,84),USE(?Sheet2),SPREAD
                         TAB('Units Successfully Updated'),USE(?Tab2)
                           LIST,AT(224,20,96,64),USE(?List1),VSCROLL,FORMAT('45L(2)|M~Unit Number~@s8@16L(2)|M~Type~@s4@'),FROM(job_queue_temp)
                         END
                       END
                       BUTTON('&Finish'),AT(264,96,56,16),USE(?CancelButton),LEFT,ICON('thumbs.gif'),STD(STD:Close)
                       PANEL,AT(4,92,320,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?tmp:esn:Prompt{prop:FontColor} = -1
    ?tmp:esn:Prompt{prop:Color} = 15066597
    If ?tmp:esn{prop:ReadOnly} = True
        ?tmp:esn{prop:FontColor} = 65793
        ?tmp:esn{prop:Color} = 15066597
    Elsif ?tmp:esn{prop:Req} = True
        ?tmp:esn{prop:FontColor} = 65793
        ?tmp:esn{prop:Color} = 8454143
    Else ! If ?tmp:esn{prop:Req} = True
        ?tmp:esn{prop:FontColor} = 65793
        ?tmp:esn{prop:Color} = 16777215
    End ! If ?tmp:esn{prop:Req} = True
    ?tmp:esn{prop:Trn} = 0
    ?tmp:esn{prop:FontStyle} = font:Bold
    ?error_text_temp{prop:FontColor} = -1
    ?error_text_temp{prop:Color} = 15066597
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?List1{prop:FontColor} = 65793
    ?List1{prop:Color}= 16777215
    ?List1{prop:Color,2} = 16777215
    ?List1{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Epassed_update     Routine
    error_text_temp   = 'Exchange Unit: ' & Clip(xch:ref_number) & ' Updated Successfully.'
    ?error_text_temp{prop:fontcolor} = color:navy
    tmp:esn = ''
    Display()
    Select(?tmp:esn)

Lpassed_update     Routine
    error_text_temp   = 'Loan Unit: ' & Clip(loa:ref_number) & ' Updated Successfully.'
    ?error_text_temp{prop:fontcolor} = color:navy
    tmp:esn = ''
    Display()
    Select(?tmp:esn)

Efailed_update     Routine
    error_text_temp   = 'Exchange Unit: ' & Clip(xch:ref_number) & ' Update Failed.'
    ?error_text_temp{prop:fontcolor} = color:red
    Display()

Lfailed_update     Routine
    error_text_temp   = 'Loan Unit: ' & Clip(loa:ref_number) & ' Update Failed.'
    ?error_text_temp{prop:fontcolor} = color:red
    Display()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Return_To_Stock',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Return_To_Stock',1)
    SolaceViewVars('save_loa_id',save_loa_id,'Return_To_Stock',1)
    SolaceViewVars('save_xch_id',save_xch_id,'Return_To_Stock',1)
    SolaceViewVars('job_queue_temp:Job_Number',job_queue_temp:Job_Number,'Return_To_Stock',1)
    SolaceViewVars('job_queue_temp:Type',job_queue_temp:Type,'Return_To_Stock',1)
    SolaceViewVars('job_queue_temp:record_number',job_queue_temp:record_number,'Return_To_Stock',1)
    SolaceViewVars('job_number_temp',job_number_temp,'Return_To_Stock',1)
    SolaceViewVars('engineer_temp',engineer_temp,'Return_To_Stock',1)
    SolaceViewVars('engineer_name_temp',engineer_name_temp,'Return_To_Stock',1)
    SolaceViewVars('old_status_temp',old_status_temp,'Return_To_Stock',1)
    SolaceViewVars('Location_temp',Location_temp,'Return_To_Stock',1)
    SolaceViewVars('update_text_temp',update_text_temp,'Return_To_Stock',1)
    SolaceViewVars('tmp:esn',tmp:esn,'Return_To_Stock',1)
    SolaceViewVars('error_text_temp',error_text_temp,'Return_To_Stock',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:esn:Prompt;  SolaceCtrlName = '?tmp:esn:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:esn;  SolaceCtrlName = '?tmp:esn';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?error_text_temp;  SolaceCtrlName = '?error_text_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List1;  SolaceCtrlName = '?List1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Return_To_Stock')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Return_To_Stock')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Clear(job_queue_temp)
  Free(job_queue_temp)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:STAHEAD.Open
  Access:USERS.UseFile
  Access:EXCHHIST.UseFile
  Access:STATUS.UseFile
  Access:JOBS.UseFile
  Access:LOANHIST.UseFile
  Access:LOAN.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List1{prop:vcr} = TRUE
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:STAHEAD.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Return_To_Stock',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:esn
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:esn, Accepted)
      found# = 0
      set(defaults)
      access:defaults.next()
      setcursor(cursor:wait)
      save_xch_id = access:exchange.savefile()
      access:exchange.clearkey(xch:esn_only_key)
      xch:esn = tmp:esn
      set(xch:esn_only_key,xch:esn_only_key)
      loop
          if access:exchange.next()
             break
          end !if
          if xch:esn <> tmp:esn      |
              then break.  ! end if
      !    If xch:available = 'RTS'
              xch:available = 'AVL'
              get(exchhist,0)
              if access:exchhist.primerecord() = level:benign
                  exh:ref_number   = xch:ref_number
                  exh:date          = today()
                  exh:time          = clock()
                  access:users.clearkey(use:password_key)
                  use:password =glo:password
                  access:users.fetch(use:password_key)
                  exh:user = use:user_code
                  exh:status        = 'UNIT AVAILABLE: RESTOCKED'
                  access:exchhist.insert()
              end!if access:exchhist.primerecord() = level:benign
      
              If xch:job_number <> ''
                  access:jobs.clearkey(job:ref_number_key)
                  job:ref_number  = xch:job_number
                  If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
                      GetStatus(815,1,'JOB') !returned to exchange stock
      
                      job:despatched = 'YES'
                      access:jobs.update()
                  End!If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
                  xch:job_number  = ''
              End!If xch:job_number <> ''
      Compile('***',Debug=1)
          Message('def:use_loan_exchange_label: ' & def:loan_exchange_label,'Debug Message',icon:exclamation)
      ***
              If DEF:Use_Loan_Exchange_Label = 'YES'
                  glo:select1  = xch:ref_number
                  Exchange_Unit_Label
                  glo:select1  = ''
              End!If DEF:Loan_Exchange_Label = 'YES'
              access:exchange.update()
      
              found#  = 1
              jobque:record_number += 1
              jobque:Job_Number    = xch:ref_number
              jobque:type = 'EXCH'
              Add(job_queue_temp)
              Sort(job_queue_temp,-jobque:record_number)
              Do Epassed_update
      
      !    End!If xch:available = 'RTS'
      end !loop
      access:exchange.restorefile(save_xch_id)
      setcursor()
      If found# = 0
          found# = 0
          setcursor(cursor:wait)
          save_loa_id = access:loan.savefile()
          access:loan.clearkey(loa:esn_only_key)
          loa:esn = tmp:esn
          set(loa:esn_only_key,loa:esn_only_key)
          loop
              if access:loan.next()
                 break
              end !if
              if loa:esn <> tmp:esn      |
                  then break.  ! end if
          !    If loa:available = 'RTS'
                  loa:available = 'AVL'
                  get(loanhist,0)
                  if access:loanhist.primerecord() = level:benign
                      loh:ref_number   = loa:ref_number
                      loh:date          = today()
                      loh:time          = clock()
                      access:users.clearkey(use:password_key)
                      use:password =glo:password
                      access:users.fetch(use:password_key)
                      loh:user = use:user_code
                      loh:status        = 'UNIT AVAILABLE: RESTOCKED'
                      access:loanhist.insert()
                  end!if access:loanhist.primerecord() = level:benign
      
                  If loa:job_number <> ''
                      access:jobs.clearkey(job:ref_number_key)
                      job:ref_number  = loa:job_number
                      If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
                          GetStatus(816,0,'LOA') !returned to loan stock
      
                          access:jobs.update()
                      End!If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
                      loa:job_number  = ''
                  End!If loa:job_number <> ''
                  If DEF:Use_Loan_Exchange_Label = 'YES'
                      glo:select1  = loa:ref_number
                      loan_Unit_Label
                      glo:select1  = ''
                  End!If DEF:Loan_Exchange_Label = 'YES'
      
                  access:loan.update()
      
                  found#  = 1
                  jobque:record_number += 1
                  jobque:Job_Number    = loa:ref_number
                  jobque:type = 'LOAN'
                  Add(job_queue_temp)
                  Sort(job_queue_temp,-jobque:record_number)
                  Do LPassed_update
      
          !    End!If loa:available = 'RTS'
          end !loop
          access:loan.restorefile(save_loa_id)
          setcursor()
      End!If found# = 0
      If found# = 0
          Case MessageEx('Unable to find the selected E.S.N. / I.M.E.I. in the Loan or Exchange Stock.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      End!If found# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:esn, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Return_To_Stock')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

