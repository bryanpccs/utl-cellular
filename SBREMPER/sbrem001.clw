

   MEMBER('sbremper.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBREM001.INC'),ONCE        !Local module procedure declarations
                     END


EmployeePerformanceCriteria PROCEDURE                 !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::5:TAGFLAG          BYTE(0)
DASBRW::5:TAGMOUSE         BYTE(0)
DASBRW::5:TAGDISPSTATUS    BYTE(0)
DASBRW::5:QUEUE           QUEUE
Pointer                       LIKE(GLO:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
save_ret_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:tag              STRING(1)
tmp:StartDate        DATE
tmp:EndDate          DATE
local:FileName       STRING(255),STATIC
UserQueue            QUEUE,PRE(tmpque)
UserCode             STRING(3)
Surname              STRING(30)
Forename             STRING(30)
Orders               LONG
OrderValue           REAL
Invoices             LONG
InvoiceValue         REAL
                     END
savepath             STRING(255)
file_name            STRING(255)
tmp:TotalLines       LONG
TotalGroup           GROUP,PRE()
tmp:TotalOrders      LONG
tmp:TotalOrderValue  REAL
tmp:TotalInvoice     LONG
tmp:TotalInvoiceValue REAL
                     END
BRW4::View:Browse    VIEW(USERS)
                       PROJECT(use:Surname)
                       PROJECT(use:Forename)
                       PROJECT(use:User_Code)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:tag                LIKE(tmp:tag)                  !List box control field - type derived from local data
tmp:tag_Icon           LONG                           !Entry's icon ID
use:Surname            LIKE(use:Surname)              !List box control field - type derived from field
use:Forename           LIKE(use:Forename)             !List box control field - type derived from field
use:User_Code          LIKE(use:User_Code)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Employee Performance Export Criteria'),AT(,,219,287),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,212,252),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Select the User'),AT(8,8),USE(?Prompt1)
                           PROMPT('Select the Sales Date Range'),AT(8,200),USE(?Prompt1:2)
                           PROMPT('Start Date'),AT(8,220),USE(?tmp:StartDate:Prompt)
                           ENTRY(@d6),AT(84,220,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(152,220,10,10),USE(?LookupStartDate),SKIP,ICON('Calenda2.ico')
                           PROMPT('End Date'),AT(8,236),USE(?tmp:EndDate:Prompt)
                           ENTRY(@d6),AT(84,236,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(152,236,10,10),USE(?LookupEndDate),SKIP,ICON('Calenda2.ico')
                           LIST,AT(8,24,204,144),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11LI@s1@92L(2)|M~Surname~@s30@120L(2)|M~Forename~@s30@12L(2)|M~User Code~@s3@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(47,67,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(55,99,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON('&Tag'),AT(8,172,56,16),USE(?DASTAG),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(64,172,56,16),USE(?DASTAGAll),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(120,172,56,16),USE(?DASUNTAGALL),LEFT,ICON('UnTag.gif')
                         END
                       END
                       PANEL,AT(4,260,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,264,56,16),USE(?Button9),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,264,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
!Shell Execute Variables
AssocFile    CString(255)
Operation    CString(255)
Param        CString(255) 
Dir          CString(255)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Prompt1:2{prop:FontColor} = -1
    ?Prompt1:2{prop:Color} = 15066597
    ?tmp:StartDate:Prompt{prop:FontColor} = -1
    ?tmp:StartDate:Prompt{prop:Color} = 15066597
    If ?tmp:StartDate{prop:ReadOnly} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 15066597
    Elsif ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 8454143
    Else ! If ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 16777215
    End ! If ?tmp:StartDate{prop:Req} = True
    ?tmp:StartDate{prop:Trn} = 0
    ?tmp:StartDate{prop:FontStyle} = font:Bold
    ?tmp:EndDate:Prompt{prop:FontColor} = -1
    ?tmp:EndDate:Prompt{prop:Color} = 15066597
    If ?tmp:EndDate{prop:ReadOnly} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 15066597
    Elsif ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 8454143
    Else ! If ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 16777215
    End ! If ?tmp:EndDate{prop:Req} = True
    ?tmp:EndDate{prop:Trn} = 0
    ?tmp:EndDate{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::5:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW4.UpdateBuffer
   glo:Queue.Pointer = use:User_Code
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = use:User_Code
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:tag = ''
  END
    Queue:Browse.tmp:tag = tmp:tag
  IF (tmp:tag = '*')
    Queue:Browse.tmp:tag_Icon = 2
  ELSE
    Queue:Browse.tmp:tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW4.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = use:User_Code
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW4.Reset
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::5:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::5:QUEUE = glo:Queue
    ADD(DASBRW::5:QUEUE)
  END
  FREE(glo:Queue)
  BRW4.Reset
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::5:QUEUE.Pointer = use:User_Code
     GET(DASBRW::5:QUEUE,DASBRW::5:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = use:User_Code
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASSHOWTAG Routine
   CASE DASBRW::5:TAGDISPSTATUS
   OF 0
      DASBRW::5:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::5:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::5:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW4.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Cancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('EmployeePerformanceCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:RETSALES.Open
  Relate:USERS.Open
  SELF.FilesOpened = True
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:USERS,SELF)
  OPEN(window)
  SELF.Opened=True
  error# = 1
  Loop x# = 1 To Len(Clip(Command()))
      IF Sub(Command(),x#,1) = '%'
          glo:Password    = Clip(Sub(Command(),x# + 1,30))
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
              error# = 0
          Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          
      End !IF Sub(Command(),x#,1) = '%'
  End !x# = 1 To Len(Clip(Command())
  If error# = 1
      Halt()
  End !error# = 1
  
  tmp:StartDate   = Deformat('1/1/1990',@d6)
  tmp:EndDate     = Today()
  Do RecolourWindow
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  BRW4.Q &= Queue:Browse
  BRW4.AddSortOrder(,use:surname_key)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,use:Surname,1,BRW4)
  BIND('tmp:tag',tmp:tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW4.AddField(tmp:tag,BRW4.Q.tmp:tag)
  BRW4.AddField(use:Surname,BRW4.Q.use:Surname)
  BRW4.AddField(use:Forename,BRW4.Q.use:Forename)
  BRW4.AddField(use:User_Code,BRW4.Q.use:User_Code)
  BRW4.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:RETSALES.Close
    Relate:USERS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Button9
      ThisWindow.Update
      Clear(UserQueue)
      Free(UserQueue)
      Clear(TotalGroup)
      savepath = path()
      glo:File_Name = 'EMPPERF.CSV'
      If not filedialog ('Choose File',glo:File_Name,'CSV Files|*.CSV|All Files|*.*', |
                  file:keepdir + file:noerror + file:longname)
          !Failed
          setpath(savepath)
      else!If not filedialog
          !Found
          setpath(savepath)
          Remove(EXPGEN)
      
          recordspercycle         = 25
          recordsprocessed        = 0
          percentprogress         = 0
          progress:thermometer    = 0
          thiswindow.reset(1)
          open(progresswindow)
      
          ?progress:userstring{prop:text} = 'Running...'
          ?progress:pcttext{prop:text} = '0% Completed'
          recordspercycle         = 25
          recordsprocessed        = 0
          percentprogress         = 0
          progress:thermometer    = 0
      
          recordstoprocess    = Records(USERS)
      
          Setcursor(Cursor:Wait)
          Save_ret_ID = Access:RETSALES.SaveFile()
          Access:RETSALES.ClearKey(ret:Date_Booked_Key)
          ret:date_booked = tmp:StartDate
          Set(ret:Date_Booked_Key,ret:Date_Booked_Key)
          Loop
              If Access:RETSALES.NEXT()
                 Break
              End !If
              If ret:date_booked > tmp:EndDate      |
                  Then Break.  ! End If
      
              Do GetNextRecord2
              cancelcheck# += 1
              If cancelcheck# > (RecordsToProcess/100)
                  Do cancelcheck
                  If tmp:cancel = 1
                      Break
                  End!If tmp:cancel = 1
                  cancelcheck# = 0
              End!If cancelcheck# > 50
      
              !Does the retail user exist in the tag table?
      
              Sort(glo:queue,glo:Pointer)
              glo:Pointer = ret:Who_Booked
              Get(glo:Queue,glo:Pointer)
              If Error()
                  Cycle
              End !If Error()
      
              tmpque:UserCode = ret:Who_Booked
              Get(UserQueue,tmpque:UserCode)
              If Error()
                  Access:USERS.Clearkey(use:User_Code_Key)
                  use:User_Code   = ret:Who_Booked
                  If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                      !Found
                      tmpque:UserCode     = ret:Who_Booked
                      tmpque:Surname      = use:Surname
                      tmpque:Forename     = use:Forename
                      tmpque:Orders       = 1
                      tmpque:OrderValue   = ret:Sub_Total
                      If ret:Invoice_Number <> ''
                          tmpque:Invoices = 1
                          tmpque:InvoiceValue = ret:Invoice_Sub_Total
                      Else !If tmpque:Invoice_Number <>
                          tmpque:Invoices = 0
                          tmpque:InvoiceValue = 0
                      End !If tmpque:Invoice_Number <>
                      Add(UserQueue)
                  End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      
              Else !If Error()
                  tmpque:Orders += 1
                  tmpque:OrderValue += ret:Sub_Total
                  If ret:Invoice_Number <> ''
                      tmpque:Invoices += 1
                      tmpque:InvoiceValue += ret:Invoice_Sub_Total
                  End !If tmpque:Invoice_Number <>
                  Put(UserQueue)
              End !If Error()
          End
          Setcursor()
          Do EndPrintRun
          close(progresswindow)
      
          Access:RETSALES.RestoreFile(Save_ret_ID)
          If Records(UserQueue)
              Access:EXPGEN.Open()
              If Access:EXPGEN.UseFile() = Level:Benign
                  Clear(gen:Record)
                  gen:Line1       = 'Surname,Forename,Orders,Order Value,Invoices,Invoice Value'
                  Access:EXPGEN.Insert()
      
                  recordspercycle         = 25
                  recordsprocessed        = 0
                  percentprogress         = 0
                  progress:thermometer    = 0
                  thiswindow.reset(1)
                  open(progresswindow)
                  Setcursor(cursor:wait)
      
                  ?progress:userstring{prop:text} = 'Exporting...'
                  ?progress:pcttext{prop:text} = '0% Completed'
      
                  recordstoprocess    = Records(UserQueue)
      
                  Sort(UserQueue,tmpque:Surname)
                  Loop x# = 1 To Records(UserQueue)
                      Get(UserQueue,x#)
                      Do GetNextRecord2
                      cancelcheck# += 1
                      If cancelcheck# > (RecordsToProcess/100)
                          Do cancelcheck
                          If tmp:cancel = 1
                              Break
                          End!If tmp:cancel = 1
                          cancelcheck# = 0
                      End!If cancelcheck# > 50
      
                      Clear(gen:Record)
                      gen:Line1       = Clip(tmpque:Surname) & ','
                      gen:Line1       = Clip(gen:Line1) & Clip(tmpque:Forename) & ','
                      gen:Line1       = Clip(gen:Line1) & Clip(tmpque:Orders) & ','
                      gen:Line1       = Clip(gen:Line1) & Clip(tmpque:OrderValue) & ','
                      gen:Line1       = Clip(gen:Line1) & Clip(tmpque:Invoices) & ','
                      gen:Line1       = Clip(gen:Line1) & Clip(tmpque:InvoiceValue)
                      Access:EXPGEN.Insert()
                      !tmp:TotalLines          += 1
                      tmp:TotalOrders         += tmpque:Orders
                      tmp:TotalOrderValue     += tmpque:OrderValue
                      tmp:TotalInvoice        += tmpque:Invoices
                      tmp:TotalInvoiceValue   += tmpque:InvoiceValue
                  End !Loop x# = 1 To Records(UserQueue)
                  Clear(gen:Record)
                  gen:Line1       = 'Totals,'&',' & Clip(tmp:TotalOrders) & ',' & |
                                  Clip(tmp:TotalOrderValue) & ',' & Clip(tmp:TotalInvoice) & ',' & Clip(tmp:TotalInvoiceValue)
                  Access:EXPGEN.Insert()
      
                  Do EndPrintRun
                  close(progresswindow)
                  Access:EXPGEN.Close()
                  Setcursor()
              End !If Access:EXPGEN.UseFile() = Level:Benign
              Case MessageEx('Export Complete','ServiceBase 2000',|
                             'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              AssocFile = glo:File_Name
              Param = ''
              Dir = ''
              Operation = 'Open'
              ShellExecute(GetDesktopWindow(), Operation, AssocFile, Param, Dir, 5)
          Else !If Records(UserQueue)
              Case MessageEx('There are no records that match the criteria.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          End !If Records(UserQueue)
      End!If not filedialog
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::5:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      Post(Event:Accepted,?DASUNTAGALL)
      !Open(EmpLogFile)
      !Set(EmpLogFile,0)
      !Loop
      !    Next(EmpLogFile)
      !    If Error()
      !        Break
      !    End !If Error()
      !    glo:Pointer = emplog:UserCode
      !    Add(glo:Queue)
      !End!End
      !Close(EmpLogFile)
      !Brw4.resetsort(1)
      !
      !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW4.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = use:User_Code
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:tag = ''
    ELSE
      tmp:tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag = '*')
    SELF.Q.tmp:tag_Icon = 2
  ELSE
    SELF.Q.tmp:tag_Icon = 1
  END


BRW4.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW4::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW4::RecordStatus=ReturnValue
  IF BRW4::RecordStatus NOT=Record:OK THEN RETURN BRW4::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = use:User_Code
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::5:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW4::RecordStatus
  RETURN ReturnValue

