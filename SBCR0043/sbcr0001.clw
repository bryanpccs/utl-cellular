

   MEMBER('sbcr0043.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('NetSimp.inc'),ONCE

                     MAP
                       INCLUDE('SBCR0001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SBCR0002.INC'),ONCE        !Req'd for module callout resolution
                     END


CRC_Sub_Trade_Account_Export PROCEDURE                !Generated from procedure template - Window

Parameter_Group      GROUP,PRE(param)
StartRecordNumber    LONG
ENDRecordNumber      LONG
MessageID            STRING(30)
EmailServer          STRING(80)
EmailPort            USHORT
EmailFrom            STRING(100)
EmailTo              STRING(100)
MessageNumber        LONG
RecordNumber         LONG
                     END
Local_Group          GROUP,PRE(LOC)
Account_Name         STRING(100)
AccountNumber        STRING(15)
ApplicationName      STRING('ServiceBase 2000')
CommentText          STRING(100)
DesktopPath          STRING(255)
EmailSubject         STRING(100)
FileName             STRING(255)
HTMLFilename         STRING(255)
InitialRecordNumber  LONG
KeepTrackOfRecordNumber LONG
LastRecordNumber     LONG
MaxHTMLSize          LONG
MessageExTimeout     LONG(500)
Path                 STRING(255)
ProgramName          STRING(100)
SectionName          STRING(100)
Text                 STRING(255)
UserCode             STRING(3)
UserName             STRING(100)
                     END
Excel_Group          GROUP,PRE()
Excel                SIGNED !OLE Automation holder
excel:ColumnName     STRING(100)
excel:ColumnWidth    REAL
excel:DateFormat     STRING('dd mmm yyyy')
excel:OperatingSystem REAL
Excel:Visible        BYTE(0)
                     END
Misc_Group           GROUP,PRE()
OPTION1              SHORT
RecordCount          LONG
Result               BYTE
AccountValue         STRING(15)
AccountTag           STRING(1)
AccountCount         LONG
AccountChange        BYTE
Save_Job_ID          ULONG,AUTO
SAVEPATH             STRING(255)
TotalCharacters      LONG
                     END
Progress_Group       GROUP,PRE()
progress:Text        STRING(100)
progress:NextCancelCheck TIME
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(1)
HeadLastCol          STRING('E')
DataLastCol          STRING('S')
HeadSummaryRow       LONG(9)
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(2048)
                     END
HTML_Queue           QUEUE,PRE(html)
LINE                 STRING(1024)
                     END
Email_Group          GROUP,PRE()
EmailServer          STRING(80)
EmailPort            USHORT
EmailFrom            STRING(255)
EmailTo              STRING(1024)
EmailSubject         STRING(255)
EmailCC              STRING(1024)
EmailBCC             STRING(1024)
EmailFileList        STRING(1024)
EmailMessageText     STRING(16384)
EmailMessageHTML     STRING(1048576)
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG
Count                LONG
                     END
window               WINDOW('Report'),AT(,,209,96),FONT('Tahoma',8,,),CENTER,ICON('PC.ICO'),CENTERED,GRAY,DOUBLE,IMM
                       SHEET,AT(3,2,201,62),USE(?Sheet1),SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           STRING('0 = To Last RecNo'),AT(128,32),USE(?String8)
                           STRING('RecNo From '),AT(8,16,60,12),USE(?String4),LEFT
                           SPIN(@n-14),AT(68,16,56,12),USE(param:StartRecordNumber),TIP('Select the first record number to start the <13,10>report.'),REQ,RANGE(1,999999999),STEP(1)
                           STRING('RecNo To '),AT(8,32,60,12),USE(?String4:2),LEFT
                           SPIN(@n-14),AT(68,32,56,12),USE(param:ENDRecordNumber),TIP('Select the record number to be the last on the<13,10>   report <13,10>   or set to 0 (Zero' &|
   ') to use all records <13,10>   from "RecNo From" onwards.<13,10>'),REQ,RANGE(0,999999999),STEP(1)
                           CHECK('Show Excel'),AT(148,48,52,12),USE(Excel:Visible),HIDE,LEFT,FONT(,,,FONT:bold),TIP('Tick To Show Excel During Report Creation.<13,10>Only Tick if you are experiencing pr' &|
   'oblems <13,10>with Excel reports.'),VALUE('1','0')
                         END
                       END
                       PANEL,AT(4,68,200,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Create Data File'),AT(84,72,56,16),USE(?OkButton),FLAT,LEFT,TIP('Click to create the data file.'),ICON(ICON:Tick),DEFAULT
                       BUTTON('Close'),AT(144,72,56,16),USE(?CancelButton),FLAT,LEFT,TIP('Press to close the criteria form.'),ICON(ICON:Cross)
                     END

SUBTRACCExport     File,Driver('BASIC','/COMMA=9 /ALWAYSQUOTE=OFF'),Pre(subexp),Name(LOCAL:File_Name),Create,Bindable,Thread
Record                  Record
Main_Account_Number         STRING(15)
Account_Number              STRING(15)
Contact_Name                STRING(30)
Branch                      STRING(30)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Postcode                    STRING(15)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Retail_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Stop_Account                STRING(3)
VAT_Number                  STRING(30)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Invoice_Customer_Address    STRING(3)
EuroApplies                 BYTE
                        End
                    End
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Local Data Classes
FTPData              CLASS(NetFTPClientData)          !Generated by NetTalk Extension (Class Definition)

                     END

!Local Data Classes
FTP                  CLASS(NetFTPClientControl)       !Generated by NetTalk Extension (Class Definition)

                     END

    MAP
AppendString    PROCEDURE(STRING, STRING, STRING), STRING
DateToString PROCEDURE(DATE), STRING
GetCustomerName PROCEDURE(), STRING
GetExpectedShipDate PROCEDURE(), STRING!(2)
GetTransactionCode PROCEDURE(), STRING
GetRecordNumber      PROCEDURE(), LONG !

GetEmailServer    PROCEDURE(), STRING
GetEmailPort      PROCEDURE(), LONG
GetEmailFrom      PROCEDURE(), STRING
GetEmailTo        PROCEDURE(), STRING
SetRecordNumber    PROCEDURE()
SetEmailServer    PROCEDURE()! 
SetEmailPort      PROCEDURE()! 
SetEmailFrom      PROCEDURE()! 
SetEmailTo        PROCEDURE()! 
IsMessageHTMLFull   PROCEDURE(), LONG ! BOOL
NumberToBool    PROCEDURE(LONG), STRING
WriteDebug      PROCEDURE( STRING )
WriteTABLE PROCEDURE(LONG)
WriteTD PROCEDURE(STRING)
WriteTH PROCEDURE(STRING)
WriteTR PROCEDURE(LONG)
    END ! MAP
! ProgressWindow Declarations
 
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
 
! Excel EQUATES

!----------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic  EQUATE(0FFFFEFF7h) ! Constants.
xlSolid      EQUATE(        1 ) ! Constants.
xlLeft       EQUATE(0FFFFEFDDh) ! Constants.
xlRight      EQUATE(0FFFFEFC8h) ! Constants.
xlLastCell   EQUATE(       11 ) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!----------------------------------------------------
! Office EQUATES

!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?String8{prop:FontColor} = -1
    ?String8{prop:Color} = 15066597
    ?String4{prop:FontColor} = -1
    ?String4{prop:Color} = 15066597
    If ?param:StartRecordNumber{prop:ReadOnly} = True
        ?param:StartRecordNumber{prop:FontColor} = 65793
        ?param:StartRecordNumber{prop:Color} = 15066597
    Elsif ?param:StartRecordNumber{prop:Req} = True
        ?param:StartRecordNumber{prop:FontColor} = 65793
        ?param:StartRecordNumber{prop:Color} = 8454143
    Else ! If ?param:StartRecordNumber{prop:Req} = True
        ?param:StartRecordNumber{prop:FontColor} = 65793
        ?param:StartRecordNumber{prop:Color} = 16777215
    End ! If ?param:StartRecordNumber{prop:Req} = True
    ?param:StartRecordNumber{prop:Trn} = 0
    ?param:StartRecordNumber{prop:FontStyle} = font:Bold
    ?String4:2{prop:FontColor} = -1
    ?String4:2{prop:Color} = 15066597
    If ?param:ENDRecordNumber{prop:ReadOnly} = True
        ?param:ENDRecordNumber{prop:FontColor} = 65793
        ?param:ENDRecordNumber{prop:Color} = 15066597
    Elsif ?param:ENDRecordNumber{prop:Req} = True
        ?param:ENDRecordNumber{prop:FontColor} = 65793
        ?param:ENDRecordNumber{prop:Color} = 8454143
    Else ! If ?param:ENDRecordNumber{prop:Req} = True
        ?param:ENDRecordNumber{prop:FontColor} = 65793
        ?param:ENDRecordNumber{prop:Color} = 16777215
    End ! If ?param:ENDRecordNumber{prop:Req} = True
    ?param:ENDRecordNumber{prop:Trn} = 0
    ?param:ENDRecordNumber{prop:FontStyle} = font:Bold
    ?Excel:Visible{prop:Font,3} = -1
    ?Excel:Visible{prop:Color} = 15066597
    ?Excel:Visible{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!-----------------------------------------------
OKButton_Pressed                     ROUTINE
    DATA
    CODE
        !------------------------------------------------------------------
        LOC:Filename = ''
        DO LoadFileDialog

        IF CLIP(LOC:Filename) = ''
            EXIT
        END!If                                           

        LOCAL:File_Name = LOC:Filename
        !------------------------------------------------------------------
        !Found
        Remove(SUBTRACCExport)

        Open(SUBTRACCExport)
        If Error()
            Create(SUBTRACCExport)
            Open(SUBTRACCExport)
            If Error()
                Case MessageEx('Cannot create export file.', LOC:ApplicationName,|
                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LOC:MessageExTimeout)
                    Of 1 ! &OK Button
                End!Case MessageEx

                EXIT
            End!If Error()

        End!If Error()
        !##################################################################
        recordspercycle         = 25
        recordsprocessed        = 0
        percentprogress         = 0
        progress:thermometer    = 0
        Progress:HighWaterMark  = 10

        recordstoprocess        = 10 ! 1000 * (tmp:EndDate - tmp:StartDate) + 1

        thiswindow.reset(1)
        open(progresswindow)

        ?progress:userstring{prop:text} = 'Exporting Completed Jobs...'
        ?progress:pcttext{prop:text}    = '0% Completed'
        !------------------------------------------------------------------
        !---Before Routine
        !DO WriteHeaderHTML

        CompCount# = 0
        !------------------------------------------------------------------
        ! Ref_Number_Key KEY( job:Ref_Number ),NOCASE,PRIMARY
        !
        Access:SUBTRACC.ClearKey(sub:RecordNumberKey)
            sub:RecordNumber = param:StartRecordNumber
        Set(sub:RecordNumberKey, sub:RecordNumberKey)

        Loop UNTIL Access:SUBTRACC.NEXT() <> Level:Benign
            !--------------------------------------------------------------
            ?progress:userstring{prop:Text} = 'Done : ' & CompCount# & ' Found: ' & Count#
            Display()

            CompCount# += 1

            Do GetNextRecord2
            Do cancelcheck
            If tmp:cancel = 1
                GOTO Finalize
            End!If tmp:cancel = 1
            !--------------------------------------------------------------
            IF (param:ENDRecordNumber > 0) AND (sub:RecordNumber > param:ENDRecordNumber)
                BREAK
            END !IF
            !==============================================================
            param:RecordNumber = sub:RecordNumber

            DO WriteDataCSV
            !DO WriteDataHTML
            !--------------------------------------------------------------
            Count#     += 1
            !--------------------------------------------------------------
        End !Loop

        ?progress:userstring{prop:Text} = 'Done : ' & CompCount# & ' Found: ' & Count#
        Display()
        !##################################################################
        !---After Routine
        !DO SendMessage
        !DO WriteFooterHTML
        !DO SendFTP
        !------------------------------------------------------------------
Finalize Setcursor()
        !------------------------------------------------------------------
        Do EndPrintRun

        close(progresswindow)
        Close(SUBTRACCExport)
        !------------------------------------------------------------------
        SetRecordNumber()
        !------------------------------------------------------------------
        Case MessageEx('Export Complete.',LOC:ApplicationName,|
                       'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,LOC:MessageExTimeout)
            Of 1 ! &OK Button
        End!Case MessageEx
        !------------------------------------------------------------------
        post(event:closewindow)
        !------------------------------------------------------------------
!-----------------------------------------------
SendMessage     ROUTINE
    DATA
    CODE
        !------------------------------------------------------------------
        IF CLIP(LOC:EmailSubject) = ''
            LOC:EmailSubject = CLIP(LOC:ProgramName) & ' (' & FORMAT(TODAY(), @D8) & ' ' & FORMAT(CLOCK(), @T6) & ') '
        END !IF

        param:MessageNumber += 1
        !------------------------------------------------------------------
        EmailServer      = param:EmailServer
        EmailPort        = param:EmailPort
        EmailFrom        = param:EmailFrom
        EmailTo          = param:EmailTo
        EmailSubject     = CLIP(LOC:EmailSubject) & '[' & param:MessageNumber & ']'
        EmailCC          = ''
        EmailBCC         = ''
        EmailFileList    = '' ! Could be 'c:\test.txt'
        EmailMessageText = ''
        EmailMessageHTML = ''
        !------------------------------------------------------------------
        DO WriteFooterHTML

        LOOP x# = 1 TO RECORDS(HTML_Queue)
            GET(HTML_Queue, x#)
            EmailMessageHTML = CLIP(EmailMessageHTML) & CLIP(html:LINE)
        END !LOOP

        !SETCLIPBOARD(EmailMessageHTML)
        !------------------------------------------------------------------
        !         pEmailServer, pEmailPort, pEmailFrom, pEmailTo, pEmailSubject, pEmailCC, pEmailBcc, pEmailFileList, pEmailMessageText, pEmailMessageHTML
        SendEMail( EmailServer,  EmailPort,  EmailFrom,  EmailTo,  EmailSubject,  EmailCC,  EmailBCC,  EmailFileList,  EmailMessageText,  EmailMessageHTML)
        !------------------------------------------------------------------
        FREE(HTML_Queue)
        TotalCharacters = 0

        DO WriteHeaderHTML
        !------------------------------------------------------------------
!        setcursor (CURSOR:WAIT)
        !------------------------------------------------------------------
SendFTP     ROUTINE
    DATA
    CODE
        !------------------------------------------------------------------
!        setcursor (CURSOR:WAIT)
            FTP.ServerHost               = 'ftp.crc-group.com'
            FTP.User                     = 'anonymous'
            FTP.Password                 = 'anonymous@hotmail.com'
            FTP.BinaryTransfer           = 1
            FTP.ChangeDir('/fromSB/crc')    !          = 'FromSB' ! RemoteFTP.CurrentDirectory
            FTP.ControlPort              = 21
            FTP.OpenNewControlConnection = 1
            FTP.PutFile('FROM name', 'TO name')
!        setcursor
        !------------------------------------------------------------------
!-----------------------------------------------
WriteDataCSV                     ROUTINE
        !------------------------------------------------------------------
        Clear(subexp:Record)
            subexp:Main_Account_Number      = sub:Main_Account_Number
            subexp:Account_Number           = sub:Account_Number
            subexp:Contact_Name             = sub:Contact_Name
            subexp:Branch                   = sub:Branch
            subexp:Company_Name             = sub:Company_Name
            subexp:Address_Line1            = sub:Address_Line1
            subexp:Address_Line2            = sub:Address_Line2
            subexp:Address_Line3            = sub:Address_Line3
            subexp:Postcode                 = sub:Postcode
            subexp:Telephone_Number         = sub:Telephone_Number
            subexp:Fax_Number               = sub:Fax_Number
            subexp:Labour_Discount_Code     = sub:Labour_Discount_Code
            subexp:Retail_Discount_Code     = sub:Retail_Discount_Code
            subexp:Parts_Discount_Code      = sub:Parts_Discount_Code
            subexp:Labour_VAT_Code          = sub:Labour_VAT_Code
            subexp:Retail_VAT_Code          = sub:Retail_VAT_Code
            subexp:Parts_VAT_Code           = sub:Parts_VAT_Code
            subexp:Stop_Account             = sub:Stop_Account
            subexp:VAT_Number               = sub:VAT_Number
            subexp:Retail_Payment_Type      = sub:Retail_Payment_Type
            subexp:Retail_Price_Structure   = sub:Retail_Price_Structure
            subexp:Invoice_Customer_Address = sub:Invoice_Customer_Address
            subexp:EuroApplies              = sub:EuroApplies
        Add(SUBTRACCExport)

        IF (sub:RecordNumber > LOC:LastRecordNumber)
            LOC:LastRecordNumber = sub:RecordNumber
        END !IF
        !------------------------------------------------------------------
!-----------------------------------------------
WriteHeaderHTML         ROUTINE
        !------------------------------------------------------------------
        FREE(HTML_Queue)
        TotalCharacters = 0

        ADD(HTML_Queue)
            CLEAR(HTML_Queue)

            WriteTABLE(False)
                WriteTR(False)
                WriteTH( 'Main Account Number'      )
                WriteTH( 'Account Number'           )
                WriteTH( 'Contact Name'             )
                WriteTH( 'Branch'                   )
                WriteTH( 'Company Name'             )
                WriteTH( 'Address Line1'            )
                WriteTH( 'Address Line2'            )
                WriteTH( 'Address Line3'            )
                WriteTH( 'Postcode'                 )
                WriteTH( 'Telephone Number'         )
                WriteTH( 'Fax Number'               )
                WriteTH( 'Labour Discount Code'     )
                WriteTH( 'Retail Discount Code'     )
                WriteTH( 'Parts Discount Code'      )
                WriteTH( 'Labour VAT Code'          )
                WriteTH( 'Retail VAT Code'          )
                WriteTH( 'Parts VAT Code'           )
                WriteTH( 'Stop Account'             )
                WriteTH( 'VAT Number'               )
                WriteTH( 'Retail Payment Type'      )
                WriteTH( 'Retail Price Structure'   )
                WriteTH( 'Invoice Customer Address' )
                WriteTH( 'Euro Applies'             )
                WriteTR(True)
            !
        PUT(HTML_Queue)

        TotalCharacters += LEN(CLIP(html:LINE))
        !------------------------------------------------------------------
WriteDataHTML         ROUTINE
        !------------------------------------------------------------------
        ADD(HTML_Queue)
            CLEAR(HTML_Queue)

            WriteTR(False)
                WriteTD( CLIP(sub:Main_Account_Number)      )
                WriteTD( CLIP(sub:Account_Number)           )
                WriteTD( CLIP(sub:Contact_Name)             )
                WriteTD( CLIP(sub:Branch)                   )
                WriteTD( CLIP(sub:Company_Name)             )
                WriteTD( CLIP(sub:Address_Line1)            )
                WriteTD( CLIP(sub:Address_Line2)            )
                WriteTD( CLIP(sub:Address_Line3)            )
                WriteTD( CLIP(sub:Postcode)                 )
                WriteTD( CLIP(sub:Telephone_Number)         )
                WriteTD( CLIP(sub:Fax_Number)               )
                WriteTD( CLIP(sub:Labour_Discount_Code)     )
                WriteTD( CLIP(sub:Retail_Discount_Code)     )
                WriteTD( CLIP(sub:Parts_Discount_Code)      )
                WriteTD( CLIP(sub:Labour_VAT_Code)          )
                WriteTD( CLIP(sub:Retail_VAT_Code)          )
                WriteTD( CLIP(sub:Parts_VAT_Code)           )
                WriteTD( CLIP(sub:Stop_Account)             )
                WriteTD( CLIP(sub:VAT_Number)               )
                WriteTD( CLIP(sub:Retail_Payment_Type)      )
                WriteTD( CLIP(sub:Retail_Price_Structure)   )
                WriteTD( CLIP(sub:Invoice_Customer_Address) )
                WriteTD( NumberToBool(sub:EuroApplies)      )
            WriteTR(True)
        PUT(HTML_Queue)

        TotalCharacters += LEN(CLIP(html:LINE))
        !------------------------------------------------------------------
        IF IsMessageHTMLFull()
            DO SendMessage
        END !IF
        !------------------------------------------------------------------
WriteFooterHTML         ROUTINE
        !------------------------------------------------------------------
        ADD(HTML_Queue)
            CLEAR(HTML_Queue)

            WriteTABLE(True)
        PUT(HTML_Queue)

        TotalCharacters += LEN(CLIP(html:LINE))
        !------------------------------------------------------------------
!-----------------------------------------------
CancelCheck                     routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()

        Case MessageEx('Are you sure you want to cancel?',LOC:ApplicationName,|
                       'Styles\question.ico','|&Yes|&No',1,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,LOC:MessageExTimeout)
            Of 1 ! &Yes Button
                tmp:cancel = 1
                CancelPressed = True
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess) * 100

      if percentprogress > 100
        percentprogress = 100
      elsIF percentprogress > 80
        recordstoprocess = 3 * recordstoprocess
      end
    end

    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
    end

    Display()

endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    IF window{Prop:AcceptAll} THEN EXIT.
    DISPLAY()
    !ForceRefresh = False
!-----------------------------------------------
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',1,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,LOC:MessageExTimeout)
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case MessageEx('Attempting to use Workshop Performance Report<10,13>'            & |
                '   without using ' & LOC:ApplicationName & '.<10,13>'                       & |
                '   Start ' & LOC:ApplicationName & ' and run the report from there.<10,13>',  |
                LOC:ApplicationName,                                                           |
                'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LOC:MessageExTimeout)
            Of 1 ! &OK Button
            END!Case MessageEx

            DO SetUpReportDetails ! 16 Apr 2003 John

           POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            !Assert(0,'<13,10>Fetch Error<13,10>')
            Case MessageEx('Unable to find your logged in user details.', |
                    LOC:ApplicationName,                                   |
                    'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LOC:MessageExTimeout)
                Of 1 ! &OK Button
            End!Case MessageEx

            POST(Event:CloseWindow)

           EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = use:Forename

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END !IF

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF
        !-----------------------------------------------
    EXIT
SetUpReportDetails          ROUTINE ! 16 Apr 2003 John
    DATA
RepType STRING('EXPORT REPORTS')
EXEName STRING('SBCR0043')
    CODE
        !-----------------------------------------------
        Access:REPEXTtp.Clearkey(rpt:ReportTypeKey)
            rpt:ReportType = RepType
        IF Access:REPEXTtp.Tryfetch(rpt:ReportTypeKey) <> Level:Benign
            !-------------------------------------------
            Access:REPEXTtp.Clearkey(rpt:ReportTypeKey)
                rpt:ReportType = RepType
            IF Access:REPEXTtp.TryInsert() <> Level:Benign
                Access:REPEXTtp.Close()

                EXIT
            END !IF
            !-------------------------------------------
        END !IF
        !-----------------------------------------------
        Access:REPEXTrp.Clearkey(rex:ReportNameKey)
            rex:ReportType = RepType
            rex:ReportName = CLIP(LOC:ProgramName) & '    (' & CLIP(EXEName) & ')'
        IF Access:REPEXTrp.Tryfetch(rex:ReportNameKey) <> Level:Benign
            !-------------------------------------------
            Access:REPEXTrp.Clearkey(rpt:ReportTypeKey)
                rex:ReportType = RepType
                rex:ReportName = CLIP(LOC:ProgramName) & '    (' & CLIP(EXEName) & ')'
                rex:EXEName    = EXEName & '.EXE'
            IF Access:REPEXTrp.TryInsert() <> Level:Benign
                Access:REPEXTrp.Close()

                EXIT
            END !IF
            !-------------------------------------------
        END !IF
        !-----------------------------------------------
        Access:REPEXTtp.Close()
        Access:REPEXTrp.Close()
        !-----------------------------------------------
    EXIT
GetFileName                             ROUTINE ! Generate default file name
    DATA
Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)
    CODE
        !-----------------------------------------------
        ! Generate default file name
        !
        !-----------------------------------------------
        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
        !
        IF CLIP(LOC:FileName) <> ''
            ! Already generated 
            EXIT
        END !IF

        DeskTopExists = False

        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        LOC:DesktopPath = Desktop
        !-----------------------------------------------
        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
        IF EXISTS(CLIP(ApplicationPath))
            LOC:Path      = CLIP(ApplicationPath) & '\'
            DeskTopExists = True

        ELSE
            Desktop  = CLIP(ApplicationPath)

            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!            ELSE
!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
            END !IF

            IF EXISTS(CLIP(ApplicationPath))
                LOC:Path      = CLIP(ApplicationPath) & '\'
                DeskTopExists = True
            END !IF

        END !IF
        !-----------------------------------------------
        IF DeskTopExists
            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
            Desktop         = CLIP(ApplicationPath)

            IF NOT EXISTS(CLIP(ApplicationPath))
                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!                ELSE
!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
                END !IF
            END !IF

            IF EXISTS(CLIP(ApplicationPath))
                LOC:Path = CLIP(ApplicationPath) & '\'
            END !IF
        END !IF
        !-----------------------------------------------
        LOC:FileName     = SHORTPATH(CLIP(LOC:Path)) & CLIP(LOC:ProgramName) & ' ' & LEFT(FORMAT(TODAY(), @D12))& ' ' & LEFT(FORMAT(CLOCK(), @T5)) & '.tab'
        LOC:HTMLFilename = SHORTPATH(CLIP(LOC:Path)) & CLIP(LOC:ProgramName) & ' ' & LEFT(FORMAT(TODAY(), @D12))& ' ' & LEFT(FORMAT(CLOCK(), @T5)) & '.html'
        !-----------------------------------------------
    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

        IF UPPER(glo:Password) <> 'SCHEDULER'
            OriginalPath = PATH()
            SETPATH(LOC:Path) ! Required for Win98
                !IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
                IF NOT FILEDIALOG('Save File', LOC:Filename, 'Text File (*.tab)|*.tab', |
                    FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)

                    LOC:Filename = ''
                END !IF
            SETPATH(OriginalPath)
        END !IF

        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
PassViaClipboard                                            ROUTINE
    DATA
!ActiveWorkSheet CSTRING(20)
!ActiveCell      CSTRING(20)
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            clip:OriginalValue = CLIPBOARD()
            clip:Saved         = True
        END !IF
        !-----------------------------------------------
        !clip:Value = 'abc' ! <09>def<09>xyz'
        SETCLIPBOARD(CLIP(clip:Value))

        Excel{'ActiveSheet.Paste'} ! Special'}
        !-----------------------------------------------
    EXIT
ResetClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            EXIT
        END !IF

        SETCLIPBOARD(clip:OriginalValue)
        !-----------------------------------------------
    EXIT
!-----------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('CRC_Sub_Trade_Account_Export')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?String8
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?CancelButton,RequestCancelled)
  Relate:REPEXTRP.Open
  Relate:SUBACCAD.Open
  Access:USERS.UseFile
  Access:SUBTRACC.UseFile
  Access:REPEXTTP.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
      LOC:ProgramName   = 'CRC Sub Trade Account Export'               ! Job=2052      Cust=    Date=6 Sep 2002 John
      window{PROP:Text} = CLIP(LOC:ProgramName)
      ?Tab1{PROP:Text}  = CLIP(LOC:ProgramName) & ' Criteria'
      !--------------------------------------------------------------
      debug:Active      = False
      Excel:Visible     = False ! INTEC = ON
      !--------------------------------------------------------------
      LOC:LastRecordNumber  = GetRecordNumber()
      param:ENDRecordNumber = 0
      param:RecordNumber    = LOC:LastRecordNumber
  
      param:StartRecordNumber = (LOC:LastRecordNumber + 1)
      LOC:InitialRecordNumber = LOC:LastRecordNumber
      !--------------------------------------------------------------
      DO GetUserName
      
      IF UPPER(glo:Password) = 'SCHEDULER'
          DISABLE(?OKButton)
          LOC:MessageExTimeout = 50
  
          DO OKButton_Pressed
      ELSE
          LOC:MessageExTimeout = 0
      END !IF
      !--------------------------------------------------------------
  Do RecolourWindow
                                               ! Generated by NetTalk Extension (Start)
  FTPData.ControlConnection &= FTP ! FTP needs _2_ objects. Check the settings on the "Settings" Tab
  FTPData.init(NET:SimpleServer)
  if FTPData.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
                                               ! Generated by NetTalk Extension (Start)
  FTP.DataConnection &= FTPData ! FTP needs _2_ objects. Check the settings on the "Settings" Tab
  FTP.init(NET:SimpleClient)
  if FTP.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
  ! support for CPCS
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  FTPData.Kill()                              ! Generated by NetTalk Extension
  FTP.Kill()                              ! Generated by NetTalk Extension
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:REPEXTRP.Close
    Relate:SUBACCAD.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      DO OKButton_Pressed
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    FTPData.TakeEvent()                 ! Generated by NetTalk Extension
    FTP.TakeEvent()                 ! Generated by NetTalk Extension
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

AppendString    PROCEDURE(IN:First, IN:Second, IN:Separator)!STRING
    CODE
        !------------------------------------------------------------------
        IF CLIP(IN:First) = ''
            RETURN IN:Second

        ELSIF CLIP(IN:Second) = ''
            RETURN IN:First
        END ! IF

        RETURN CLIP(IN:First) & IN:Separator & CLIP(IN:Second)
        !------------------------------------------------------------------
DateToString PROCEDURE(IN:Date)!STRING
    CODE
        !------------------------------------------------------------------
        RETURN LEFT(FORMAT(IN:Date, @D8))
        !------------------------------------------------------------------
GetCustomerName PROCEDURE()!STRING
TEMP STRING(100)
    CODE
        !------------------------------------------------------------------
        ! STRING(30)!(job:Title+job:Initials+job:Surname) / job:Company_Name
        !------------------------------------------------------------------
        TEMP = AppendString( job:Title, job:Initial, ' ')
        TEMP = AppendString(      TEMP, job:Surname, ' ')

        IF CLIP(TEMP) = ''
            RETURN job:Company_Name
        END !IF

        RETURN CLIP(TEMP)
        !------------------------------------------------------------------
GetExpectedShipDate PROCEDURE()!STRING(2)
    CODE
        !------------------------------------------------------------------
        ! STRING('Delivery Not Known')!put "Delivery Not Known" if job:Current_Status IN("Awaiting Spares", "Spares Requested")
        !
        CASE job:Current_Status
        OF 'Awaiting Spares' OROF 'Spares Requested'
            RETURN 'Delivery Not Known'
        ELSE
            RETURN ''
        END !CASE
        !------------------------------------------------------------------
GetTransactionCode PROCEDURE()!STRING(2)
    CODE
        !------------------------------------------------------------------
        CASE job:Current_Status
        OF 'EXCHNGE UNIT SENT'      ! 'EXCH. UNIT DESPATCHED'
            RETURN 'PR'
        OF 'ORIGINAL UNIT RETURNED' !
            RETURN 'RC'
        END !CASE

        IF      job:Unit_Type <> 'ACCESSORY'
            RETURN ''
        ELSIF job:Charge_Type <> 'EXCHANGE'
            RETURN ''
        ELSE
            RETURN 'AC'
        END !IF
        !------------------------------------------------------------------
GetRecordNumber        PROCEDURE()! STRING
    CODE
        !------------------------------------------------------------------
        RETURN GETINI( 'Parameters',  'RecordNumber',                                  '0', '.\sbcr0043.ini')
        !------------------------------------------------------------------

GetEmailServer            PROCEDURE()! STRING
    CODE
        !------------------------------------------------------------------
        RETURN GETINI( 'Parameters',   'EmailServer',                      '192.168.0.229', '.\sbcr0043.ini')
        !------------------------------------------------------------------

GetEmailPort              PROCEDURE()! LONG
    CODE
        !------------------------------------------------------------------
        RETURN GETINI( 'Parameters',     'EmailPort',                                 '25', '.\sbcr0043.ini')
        !------------------------------------------------------------------

GetEmailFrom              PROCEDURE()! STRING
    CODE
        !------------------------------------------------------------------
        RETURN GETINI( 'Parameters',     'EmailFrom',   'j.griffiths@pccontrolsystems.com', '.\sbcr0043.ini')
        !------------------------------------------------------------------

GetEmailTo                PROCEDURE()! STRING
    CODE
        !------------------------------------------------------------------
        RETURN GETINI( 'Parameters',       'EmailTo',   'j.griffiths@pccontrolsystems.com', '.\sbcr0043.ini')
        !------------------------------------------------------------------

SetRecordNumber    PROCEDURE()
    CODE
        !------------------------------------------------------------------
        IF (param:StartRecordNumber  <= LOC:InitialRecordNumber+1) AND (param:RecordNumber => LOC:InitialRecordNumber+1)
            ! NULL
        ELSE
            RETURN
        END !IF

        PUTINI( 'Parameters', 'RecordNumber', param:RecordNumber, '.\sbcr0043.ini')
        !------------------------------------------------------------------

SetEmailServer    PROCEDURE()!
    CODE
        !------------------------------------------------------------------
        PUTINI( 'Parameters',  'EmailServer',    param:EmailServer, '.\sbcr0043.ini')
        !------------------------------------------------------------------

SetEmailPort      PROCEDURE()!
    CODE
        !------------------------------------------------------------------
        PUTINI( 'Parameters',    'EmailPort',      param:EmailPort, '.\sbcr0043.ini')
        !------------------------------------------------------------------

SetEmailFrom      PROCEDURE()!
    CODE
        !------------------------------------------------------------------
        PUTINI( 'Parameters',    'EmailFrom',      param:EmailFrom, '.\sbcr0043.ini')
        !------------------------------------------------------------------

SetEmailTo        PROCEDURE()!
    CODE
        !------------------------------------------------------------------
        PUTINI( 'Parameters',      'EmailTo',        param:EmailTo, '.\sbcr0043.ini')
        !------------------------------------------------------------------
IsMessageHTMLFull   PROCEDURE()! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        IF LOC:MaxHTMLSize = 0
           LOC:MaxHTMLSize = (SIZE(EmailMessageHTML) * 0.95)
           !message('(SIZE(EmailMessageHTML) * 0.8)="' & LOC:MaxHTMLSize & '"')
           !LOC:MaxHTMLSize = (16384 * 0.8)
        END !IF

        IF TotalCharacters > LOC:MaxHTMLSize
            RETURN True
        ELSE
            RETURN False
        END !IF
        !------------------------------------------------------------------
NumberToBool    PROCEDURE(IN:Number)! STRING
    CODE
        !------------------------------------------------------------------
        IF IN:Number = True
            RETURN '1' ! True'
        ELSE
            RETURN '0' ! False'
        END !IF
        !------------------------------------------------------------------
WriteDebug      PROCEDURE( IN:Message )
    CODE
        !-----------------------------------------------
        IF NOT debug:Active
            RETURN
        END !IF

        debug:Count += 1

        PUTINI(CLIP(LOC:ProgramName), debug:Count, IN:Message, 'C:\Debug.ini')
        !-----------------------------------------------
WriteTABLE PROCEDURE(IN:Close)
    CODE
        !------------------------------------------------------------------
        IF IN:Close = True
            html:LINE = CLIP(html:LINE) & ('</TABLE><13,10>')

            LinePrint('</TABLE>', LOC:HTMLFilename)
        ELSE
            html:LINE = CLIP(html:LINE) & ('<TABLE BORDER="1"><13,10>')

            LinePrint('<TABLE BORDER="1">', LOC:HTMLFilename)
        END !IF
        !------------------------------------------------------------------
WriteTD PROCEDURE(IN:Value)
    CODE
        !------------------------------------------------------------------
        html:LINE = CLIP(html:LINE) & '<TD>' & CLIP(IN:Value) & '</TD>'

        LinePrint('<TD>' & CLIP(IN:Value) & '</TD>', LOC:HTMLFilename)
        !------------------------------------------------------------------
WriteTH PROCEDURE(IN:Value)
    CODE
        !------------------------------------------------------------------
        html:LINE = CLIP(html:LINE) & '<TH>' & CLIP(IN:Value) & '</TH>'

        LinePrint('<TH>' & CLIP(IN:Value) & '</TH>', LOC:HTMLFilename)
        !------------------------------------------------------------------
WriteTR PROCEDURE(IN:Close)
    CODE
        !------------------------------------------------------------------
        IF IN:Close = True
            html:LINE = CLIP(html:LINE) & '</TR><13,10>'

            LinePrint('</TR>', LOC:HTMLFilename)
        ELSE
            html:LINE = CLIP(html:LINE) & '<TR>'

            LinePrint('<TR>', LOC:HTMLFilename)
        END !IF
        !------------------------------------------------------------------
